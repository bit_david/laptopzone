<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
body {
    background: grey;
    margin-top: 120px;
    margin-bottom: 120px;
}</style>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">
                            <img src="http://via.placeholder.com/400x90?text=logo">
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-1">Invoice <?php echo $data[0]['INVOICE_ID']; ?></p>
                            <p class="text-muted"><?php echo date('D d-M-Y') ?></p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-4">Client Information</p>
                            <p class="mb-1"><?php echo $data[0]['MERCHANT_NAME']; ?></p>
                            <!-- <p>Acme Inc</p>
                            <p class="mb-1">Berlin, Germany</p>
                            <p class="mb-1">6781 45P</p> -->
                        </div>

                        <div class="col-md-6 text-right">
                            <!-- <p class="font-weight-bold mb-4">Payment Details</p>
                            <p class="mb-1"><span class="text-muted">VAT: </span> 1425782</p>
                            <p class="mb-1"><span class="text-muted">VAT ID: </span> 10253642</p>
                            <p class="mb-1"><span class="text-muted">Payment Type: </span> Root</p>
                            <p class="mb-1"><span class="text-muted">Name: </span> John Doe</p> -->
                        </div>
                    </div>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">Service Name</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Rate</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>

                                        <th class="border-0 text-uppercase small font-weight-bold">Total Charge</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Discount Amount</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Discount Amount%</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Duration</th>
                                         <th class="border-0 text-uppercase small font-weight-bold">From Date</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">To Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data as $res) {?>
                                    <tr>
                                        <td><?php echo $res['SERVICE_DESC']; ?></td>
                                        <td>$<?php echo number_format((float) $res['RATE'], 2, '.', ''); ?></td>
                                        <td><?php echo $res['QUANTITY']; ?></td>

                                        <td>$<?php echo number_format((float) $res['TOTAL_CHARGE'], 2, '.', ''); ?></td>
                                        <td>$<?php echo number_format((float) $res['DIS_AMOUNT'], 2, '.', ''); ?></td>
                                        <td><?php echo number_format((float) $res['DIS_AMOUNT_PERC'], 2, '.', ''); ?>%</td>
                                        <td><?php echo $res['DURATION']; ?></td>
                                          <td><?php echo $res['FROM_DATE']; ?></td>
                                        <td><?php echo $res['TO_DATE']; ?></td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                    <div class="py-3 px-5 text-right">
                            <div class="mb-2">Grand Total</div>
                            <div class="h2 font-weight-light">$<?php echo number_format((float) $invoice_summary[0]['TOTAL'], 2, '.', ''); ?></div>
                        </div>


                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Total Discount</div>
                            <div class="h2 font-weight-light">$<?php echo number_format((float) $invoice_summary[0]['DISCOUNT'], 2, '.', ''); ?></div>
                        </div>
                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Total Charge</div>
                            <div class="h2 font-weight-light">$<?php echo number_format((float) $invoice_summary[0]['SUB_TOTAL'], 2, '.', ''); ?></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="text-light mt-5 mb-5 text-center small">by : <a class="text-light" target="_blank" href="http://www.ecologix.us.com">ecologix.us.com</a></div>

</div>
