
<style media="print"> @page {size: auto; margin: 0; } </style>
<!doctype html>
<html class="no-js" lang="">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>Print Invoice</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
   </head>
   <body>
      <div style="width: 280px !important;margin:0px !important;">
      <div style="width: 138px;float:left;font-size:12px;font-family: arial;"><?php date_default_timezone_set("America/Chicago");
echo $print_date = date("m/d/Y H:i A ");
?></div>
      <div style="width: 138px;float:right;font-size:12px;font-family: arial;"> <b><?php echo 'End Of Day' ?></b> </div>
      <div style="padding: 2px;font-size:12px;font-family: arial;"><?php echo 'Store: ' . @$store_name[0]['STORE_NAME']; ?></div>
      <div style="text-align: center;font-size: 20px; font-weight: bold; font-family: arial;">LaptopZone</div>
      <div style="font-size:12px;font-weight: normal;font-family: arial;text-align: center;"><?php echo @$store_name[0]['ADDRESS']; ?> <br><?php echo @$store_name[0]['CITY_DESC'] ?><br><?php echo trim(str_replace("_", '', @$store_name[0]['PHONE_NO'])) ?></div>
      <br>
      <table>
      <thead>
      <tr>
        <th style="font-size:12px !important;font-family: arial; text-align:left;">Date:</th>
        <td style="font-size:10px !important;font-family: arial;"><?php echo $startDate ?></td>
      </tr>

         <tr style="font-size:12px !important;border-bottom: 1px solid #000 !important;font-family: arial;">
            <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Sales Activity</th>
            <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Sale</th>
            <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Sale Price</th>
         </tr>
      </thead>
      <tbody>
	  <?php foreach ($data as $detail) {?>
	  <tr style="font-size:12px !important;font-family: arial;">
   <td style="width:120px;">
      <?php
// $text = $detail["ITEM_DESC"];
    //  $item_desc = implode("<br/>", str_split($text, 40));
    echo 'Sales Activities' ?>
   </td>
   <!-- <span style="float:right;align:center;margin-right:15px;"> -->
   <td align="center" ><?php echo $price = '$' . number_format((float) @$detail['PRICE'], 2, '.', ','); ?></td>

   <td align="center"><?php $price = 0;
    $tax = 0;
    $tax = ($detail['PRICE'] / 100) * $detail['SALES_TAX_PERC'];
    $price = $detail['PRICE'] + $tax;
    echo $price = '$' . number_format((float) @$price, 2, '.', ',');?></td>
</tr>
	  <?php }?>
<tr style="font-size:12px !important;font-family: arial;">
   <td style="border-top-style: solid; border-top: 1px solid #000 !important;">Total Sales Activity</td>
   <td align="center" style="border-top-style: solid; border-top: 1px solid #000 !important;"><?php $sumPrice = 0;foreach ($data as $detail) {

    $sumPrice += $detail['PRICE'];}
echo '$' . number_format((float) @$sumPrice, 2, '.', ',');?></td>
   <td align="center" style="border-top-style: solid; border-top: 1px solid #000 !important;"><?php $sumPrice = 0;foreach ($data as $detail) {

    $tax = ($detail['PRICE'] / 100) * $detail['SALES_TAX_PERC'];
    $sumPrice += $detail['PRICE'] + $tax;}
echo '$' . number_format((float) @$sumPrice, 2, '.', ',');?></td>
</tr>

<tr style="font-size:12px !important;font-family: arial;">
   <td style="border-top-style: solid; border-top: 1px solid #000 !important;">Cash In Hand</td>
<td style="border-top-style: solid; border-top: 1px solid #000 !important;"> </td>
   <td align="center" style="border-top-style: solid; border-top: 1px solid #000 !important;"><?php

echo '$' . number_format((float) @$todayCash, 2, '.', ',') ?></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
            <th align="left">Sales Adjustments</th>
</tr>

<tr style="font-size:12px !important;font-family: arial;">
   <td align="right">Advance Payments</td>
   <td></td>
   <td  align="center"><?php
//    foreach ($data as $detail) {$advance;
//     $advance_payment = 0;
//     $advance_payment += $advance;}
echo '$' . number_format((float) @$advance, 2, '.', ','); ?></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
   <td align="right">Today Advance Payments</td>
   <td></td>
   <td  align="center"><?php
//    foreach ($data as $detail) {$advance;
//     $advance_payment = 0;
//     $advance_payment += $advance;}
echo '$' . number_format((float) @$today_advance, 2, '.', ','); ?></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
   <td align="right">Discount </td>
   <td></td>
   <td align="center"><?php $sumDisc = 0;foreach ($data as $detail) {

    $sumDisc += $detail['DISC_AMT'];}
echo '$' . number_format((float) @$sumDisc, 2, '.', ',');?></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
   <td align="right">Returns</td>
   <td></td>
   <td align="center"><?php
echo '$' . number_format((float) @$todayReturn, 2, '.', ','); ?></td>
</tr>

<tr style="font-size:12px !important;font-family: arial;">
   <td align="right">Net Sale</td>
   <td></td>
   <td align="center"><?php
$sumPrice = 0;
$sumDis = 0;
foreach ($data as $detail) {

    $tax = ($detail['PRICE'] / 100) * $detail['SALES_TAX_PERC'];
    $sumDis += $detail['DISC_AMT'];
    $sumPrice += $detail['PRICE'] + $tax;}
$netSale = $sumPrice - $sumDis - $advance + $today_advance + $todayCash;
echo '$' . number_format((float) @$netSale, 2, '.', ',');?></td>
</tr>

<tr style="font-size:12px !important;font-family: arial;">
   <td align="right"><b>Total Available For Deposit:</b></td>
   <td></td>
   <td align="center"><?php
$sumPrice = 0;
$sumDis = 0;
foreach ($data as $detail) {

    $tax = ($detail['PRICE'] / 100) * $detail['SALES_TAX_PERC'];
    $sumDis += $detail['DISC_AMT'];
    $sumPrice += $detail['PRICE'] + $tax;}
$netSale = @$sumPrice-@$sumDis-@$advance+@$today_advance+@$todayCash+@$todayReturn;
echo '$' . number_format((float) @$netSale, 2, '.', ',');?></td>
</tr>
<!--  RECEIPT COUNT -->
</tr>
<tr style="font-size:12px !important;font-family: arial;">
<th align="left">Receipt Count</th>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
   <td align="right">Total Sales</td>
   <td></td>
   <td align="center"><?php $sumQty = 0;foreach ($data as $detail) {

    $sumQty += $detail['QTY'];
}
echo $sumQty;?></td>
</tr>
</tr>
<tr style="font-size:12px !important;font-family: arial;">

   <td align="right">Total Returns</td>
   <td>
   </td>
   <td align="center"><?php $totalReturn = 0;foreach ($data as $detail) {

    $totalReturn += @$detail['TOTAL_RETURN'];
}
echo @$totalReturn;?></td>
   <td></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">

   <td align="right">Cash Payments</td>
   <td>
   </td>
   <td align="center"><?php $totalCashCount = 0;
if (isset($data[0]['PAY_MODE']) && !isset($data[0]['TOTAL_CASH'])) {
    foreach ($data as $detail) {

        if ($detail['PAY_MODE'] == 'CASH') {
            $totalCashCount++;
        }

    }
} else {
    $totalCashCount = $data[0]['TOTAL_CASH'];
}
// $totalCashCount = $data[0]['TOTAL_CASH'];
echo @$totalCashCount;?></td>
   <td></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">

   <td align="right">Card Payments</td>
   <td>
   </td>
   <td align="center"><?php $totalCardCount = 0;
if (isset($data[0]['PAY_MODE']) && !isset($data[0]['TOTAL_CARD'])) {
    foreach ($data as $detail) {

        if ($detail['PAY_MODE'] == 'CARD') {
            $totalCardCount++;
        }
    }
} else {
    $totalCardCount = $data[0]['TOTAL_CARD'];
}
// $totalCardCount = $data[0]['TOTAL_CARD'];
echo $totalCardCount;?></td>
   <td></td>
</tr>

<!--  CASH AND CARD -->

<!-- <tr style="font-size:12px !important;font-family: arial;">
<th>Cash/Card</th>
</tr> -->
<!-- <tr style="font-size:12px !important;font-family: arial;">
<td>
   </td>
   <td align="center">Total Cash</td>
   <td>
   </td>
   <td><?php
$totalCash = 0;
$sumPrice = 0;
$sumDix = 0;foreach ($data as $detail) {

    if ($detail['PAY_MODE'] == 'CASH') {
        $tax = ($detail['PRICE'] / 100) * $detail['SALES_TAX_PERC'];
        $sumDis = $detail['DISC_AMT'];
        $sumPrice = $detail['PRICE'] + $tax;
        $totalCash += $sumPrice - $sumDis;
    }
}
$total = @$totalCash-@$advance+@$today_advance;
echo '$' . number_format((float) @$total, 2, '.', ',');?></td>
   <td></td>
</tr>

<tr style="font-size:12px !important;font-family: arial;">
<td>
   </td>
   <td align="center">Total Card</td>
   <td>
   </td>
   <td><?php $totalCard = 0;
$sumPrice = 0;
$sumDix = 0;
foreach ($data as $detail) {

    if ($detail['PAY_MODE'] == 'CARD') {
        $tax = ($detail['PRICE'] / 100) * $detail['SALES_TAX_PERC'];
        $sumDis = $detail['DISC_AMT'];
        $sumPrice = $detail['PRICE'] + $tax;
        $totalCard += $sumPrice - $sumDis;
    }
}
$total = @$totalCard-@$advance+@$today_advance;
echo '$' . number_format((float) @$total, 2, '.', ',');?></td>
   <td></td>
</tr> -->
<!-- Signature -->


</tbody> </table><br>
<div id="textbox" style="margin:5px;font-size: 12px;font-weight: normal;font-family: arial;margin-bottom: 35px;">
  <p class="alignleft" style="float:left; border-top:1px solid black;">Owner Signature</p>
  <p class="alignright" style="float:right; border-top:1px solid black;">Cashier Signature</p>
</div>
<div style="text-align:center;margin:5px;font-size: 12px;font-weight: normal;font-family: arial;margin-bottom: 35px;">End Of Day POS Report!</div>
</div></body> </html>
