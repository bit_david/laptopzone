<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
  <link rel="icon" href="<?php echo base_url('assets/images/favicon-1.ico');?>">
  <?php 
    $hostname = $_SERVER['HTTP_HOST'];       
    $oraserver = "192.168.0.59:8081";
    $oraserver2 = "192.168.0.28";
    $oraserverLive = "71.78.236.22:8081";
    $oraserverLive2 = "71.78.236.20";
    $ecologixserver = "192.168.0.78:8081";
    $ecologixserverLive = "71.78.236.21:8081";
    $localserver = "wizmen-pc:8081";
    $localserver2 = "localhost";

    $server_name = '';
    if($hostname == $oraserver || $hostname == $oraserver2 || $hostname == $oraserverLive || $hostname == $oraserverLive2){
      $server_name .= " - OR";
    }elseif ($hostname == $ecologixserver || $hostname == $ecologixserverLive) {
      $server_name .= " - BG";
    }
  ?>
  <title><?php echo (isset($pageTitle)) ? $pageTitle.$server_name : 'Dashboard '.$server_name; ?> | Laptopzone</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/font-awesome.min.css');?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css');?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css');?>">
  <!-- iCheck -->
  <!-- <link rel="stylesheet" href="<?php //echo base_url('assets/plugins/iCheck/flat/blue.css');?>"> -->
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/morris/morris.css');?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">
    <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/flat/green.css');?>">
    <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/all.css');?>">
  <!-- Date Picker -->  
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css');?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker.css');?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datetimepicker/bootstrap-datetimepicker.css');?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/custom.css');?>">
  <!-- <link rel="stylesheet" href="<?php //echo base_url('assets/plugins/datatables/jquery.dataTables.min.css');?>"> -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/datatables/datatables.min.css');?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/umeditor-dev/themes/default/_css/umeditor.css');?>">
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- image gallery section -->
   <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/image_gallery_viewer.css');?>">
   <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/image_gallery_main.css');?>">
   <!-- //////////////////INPUTABLE DROPDOWN FILES///////////////////////////// -->
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/inputable/jqx.base.css'); ?>"/>
   <!-- /////////////////////////////////////////////// -->
  <!-- image gallery section -->
     <!--=============================================
=            spell check editor block            =
=============================================-->
<!-- <script src="https://cdn.ckeditor.com/ckeditor5/10.0.0/classic/ckeditor.js"></script> -->
<!--=====  End of spell check editor block  ======*/ -->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 