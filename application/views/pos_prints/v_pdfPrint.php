<style media="print">
    @page {
        size: auto;
        margin: 0;
    }
</style>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Print Invoice</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1"> </head>

<body>

    <div style="width: 250px !important;margin:5px !important;">
        <div style="width: 138px;float:left;font-size:12px;font-family: arial;">
            <?php date_default_timezone_set("America/Chicago");
$print_date = date("m/d/Y H:i A ");
$concatDate = date("m/d/Y");
echo @$print_date;
?>
        </div>
        <div style="width: 138px;float:right;font-size:12px;font-family: arial;"> <b>Sale Receipt # <?php echo @$data[0]["DOC_NO"] ?></b> </div>
        <div style="padding: 2px;font-size:12px;font-family: arial;">Store:
            <?php echo @$store_name[0]["STORE_NAME"] ?>
        </div>
        <div style="text-align: center;font-size: 20px; font-weight: bold; font-family: arial;padding: 3px;">LaptopZone</div>
        <div style="font-size:12px;font-weight: normal;font-family: arial;text-align: center;">
            <?php echo @$store_name[0]["ADDRESS"] ?>
                <br>
                <?php echo @$store_name[0]["CITY_DESC"] ?>
                    <br>
                    <?php echo trim(str_replace("_", '', @$store_name[0]["PHONE_NO"])) ?>
        </div>
        <br>
        <table>
            <tr>
                <th style="font-size:11px !important;font-family: arial; text-align:left;">Customer Name:</th>
                <td style="font-size:11px !important;font-family: arial;">
                    <?php echo trim(@$store_name[0]["BUYER_NAME"]) ?>
                </td>
            </tr>
            <tbody>
        </table>
        <br>

        <table>
            <thead>
                <tr style="font-size:10px !important;border-bottom: 1px solid #000 !important;font-family: arial;">
                    <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Item Name</th>
                    <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Qty</th>
                    <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Price</th>
                    <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Total Price</th>
                </tr>
            </thead>
            <tbody>
                <?php $sum_price = 0;
$sum_disc_amt = 0;
$sum_qty = 0;
$total_pay_advance = 0;
foreach ($data as $dataa) {
    $text = $dataa["ITEM_DESC"];
    $price = '$' . number_format((float) @$dataa['DET_PRICE'], 2, '.', ',');
    $item_desc = str_split($text, 40);
    $sum_price = $sum_price + $dataa['DET_PRICE'];

    $sum_disc_amt = $sum_disc_amt + $dataa['DISC_AMT'];
    $sum_qty = $sum_qty + $dataa['QTY'];
    $total_pay_advance = $total_pay_advance + $dataa['ADVANCE_PAYMENT'];
    ?>
                    <tr style="font-size:10px !important;font-family: arial;">
                        <td>

                                <?php echo @$item_desc[0] ?>

                        </td>
                        <td style="text-align: center;"><span><?php echo @$dataa["QTY"] ?></span></td>
                        <td>
                            <?php echo $price ?>
                        </td>
                        <td>
                            <?php echo $price ?>
                        </td>
                    </tr>


                    <?php }?>

                        <?php
$total = $sum_price - $sum_disc_amt;
$pay_mode = @$data[0]['PAY_MODE'];
$sales_tax = ($total / 100) * @$data[0]['DET_SALES_TAX'];
$sales_tax = number_format((float) @$sales_tax, 2, '.', ',');

$receipt_total = $total + $sales_tax;
$receipt_totals = $receipt_total - $total_pay_advance;
$receipt_total = number_format((float) @$receipt_total, 2, '.', ',');
$receipt_totals = number_format((float) @$receipt_totals, 2, '.', ',');
if ($pay_mode == "C") {
    $rcpt_total = "Cash: $" . $receipt_total;

} elseif ($pay_mode == "R") {
    $rcpt_total = "Credit: $" . $receipt_total;
}

// $con = implode($print_date);
$barDate = str_replace('/', '', $concatDate);
// var_dump($barDate);exit;
$str1 = substr($barDate, 0, 4);
$str2 = substr($barDate, 6, 8);
$docDateStr = $str1 . $str2;
?>
 <tr style="font-size:10px !important;font-family: arial;">
                        <td style="border-top-style: solid; border-top: 1px solid #000 !important;text-align:center;"><b> Total Qty </b> </td>
                        <td style="border-top-style: solid; border-top: 1px solid #000 !important; text-align: center;"><span><?php echo $sum_qty ?></span></td>
                        <td style="border-top-style: solid; border-top: 1px solid #000 !important;"><b>Subtotal:</b></td>
                        <td style="border-top-style: solid; border-top: 1px solid #000 !important;">
                            <?php echo '$ ' . $sum_price ?>
                        </td>
                    </tr>
<?php if ($repaire_status == true) {?>

                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td></td>
                                <td>Disc Amt:</td>
                                <td>
                                    <?php echo '$' . $sum_disc_amt ?>
                                </td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td></td>
                                <td><b>Total:</b></td>
                                <td>
                                    <?php echo '$' . $total ?>
                                </td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td>Exempt</td>
                                <td>0 % Tax:</td>
                                <td>+
                                    <?php echo '$' . $sales_tax ?>
                                </td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td colspan="2"><b>GRAND TOTAL:</b></td>
                                <td><b><?php echo '$' . $receipt_total ?></b></td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td>
                                    <div style="width:90px;">
                                        <?php echo $rcpt_total ?>
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td></td>
                                <td>TotalPay:</td>
                                <td>
                                    <?php echo '$' . $receipt_totals ?>
                                </td>
                            </tr>

            </tbody>
        </table>
        <br>

        <div style="text-align:center;margin:5px;font-size: 10px;font-weight: normal;font-family: arial;">Thanks for shopping with us!</div>
        <div style="margin:5px;text-align:center;margin-bottom:10px;font-size: 10px;font-weight: normal;font-family: arial;"><img style="margin-bottom:3px;width: 200px;" alt="pos_barcode" src="<?php echo base_url() ?>assets/barcode/barcode.php?text=<?php echo @$data[0]["DOC_NO"] ?>"/>
            <div style="margin:5px;text-align:center;margin:5px;font-size: 10px;font-weight: normal;font-family: arial;">
                <?php echo @$data[0]["DOC_NO"] . '-' . @$docDateStr ?>
            </div>
        </div>
<?php } else {?>
    <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td></td>
                                <td>Disc Amt:</td>
                                <td>
                                    <?php echo '$' . $sum_disc_amt ?>
                                </td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td></td>
                                <td><b>Total:</b></td>
                                <td>
                                    <?php echo '$' . $total ?>
                                </td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td></td>
                                <td>Adv Pay:</td>
                                <td>
                                    <?php echo '$' . $total_pay_advance ?>
                                </td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td>Exempt</td>
                                <td>0 % Tax:</td>
                                <td>+
                                    <?php echo '$' . $sales_tax ?>
                                </td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td colspan="2"><b>GRAND TOTAL:</b></td>
                                <td><b><?php echo '$' . $receipt_total ?></b></td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td>
                                    <div style="width:90px;">
                                        <?php echo $rcpt_total ?>
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="font-size:10px !important;font-family: arial;">
                                <td></td>
                                <td></td>
                                <td>TotalPay:</td>
                                <td>
                                    <?php echo '$' . $receipt_totals ?>
                                </td>
                            </tr>

            </tbody>
        </table>
        <br>

        <div style="text-align:center;margin:5px;font-size: 10px;font-weight: normal;font-family: arial;">Thanks for shopping with us!</div>
        <div style="margin:5px;text-align:center;margin-bottom:10px;font-size: 10px;font-weight: normal;font-family: arial;"><img style="margin-bottom:3px;width: 200px;" alt="pos_barcode" src="<?php echo base_url() ?>assets/barcode/barcode.php?text=<?php echo @$data[0]["DOC_NO"] ?>"/>
            <div style="margin:5px;text-align:center;margin:5px;font-size: 10px;font-weight: normal;font-family: arial;">
                <?php echo @$data[0]["DOC_NO"] . '-' . @$docDateStr ?>
            </div>
        </div>
 <?php }?>

        </body>

</html>