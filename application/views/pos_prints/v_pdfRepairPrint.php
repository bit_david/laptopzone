<style media="print">
    @page {
        size: auto;
        margin: 0;
    }
</style>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Print Invoice</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1"> </head>

<body>

<?php for ($i = 0; $i < 4; $i++) {
    if ($i === 0) {
        ?>
        <table>
    <tr>
        <th style="font-size:12px !important;font-family: arial; text-align:left;">Customer Name:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['PRODUCT_OWNER'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial; text-align:left;">Created By:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['OWNER_NAME'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial; text-align:left;">Serial No:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['SERIAL_NO'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial;text-align:left;">Expt Delivery Date:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['EXPT_DEL_DATE'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial;text-align:left;">Brand:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['BRAND'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial;text-align:left;">MPN:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['MPN'] ?></td>
    </tr>

    <tr>
        <th style="font-size:12px !important;font-family: arial;text-align:left;">Customer Number:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php trim(str_replace("_", '', @$store_name[0]['CUSTOMER_NUMBER']))?></td>
    </tr>
    <tbody>
</table>
<br>
    <div style="width: 280px !important;margin:0px !important;">
        <div style="width: 138px;float:left;font-size:12px;font-family: arial;">
            <?php date_default_timezone_set("America/Chicago");
        $print_date = date("m/d/Y H:i A ");
        $concatDate = date("m/d/Y");
        echo @$print_date;
        ?>
        </div>
        <div style="width: 138px;float:right;font-size:12px;font-family: arial;"> <b>Sale Receipt # <?php echo @$store_name[0]["LZ_POS_REPAIRE_ID"] ?></b> </div>
        <div style="padding: 2px;font-size:12px;font-family: arial;">Store:
            <?php echo @$store_name[0]["STORE_NAME"] ?>
        </div>
        <div style="text-align: center;font-size: 20px; font-weight: bold; font-family: arial;padding: 3px;">LaptopZone</div>
        <div style="font-size:12px;font-weight: normal;font-family: arial;text-align: center;">
            <?php echo @$store_name[0]["ADDRESS"] ?>
                <br>
                <?php @$store_name[0]["CITY_DESC"]?>
                    <br>
                    <?php echo trim(str_replace("_", '', @$store_name[0]["PHONE_NO"])) ?>
        </div>
        <br>
        <?php echo $header_table ?>
            <table>
                <thead>
                    <tr style="font-size:12px !important;border-bottom: 1px solid #000 !important;font-family: arial;">
                        <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Item Desc</th>
                        <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Price</th>
                        <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Ext Price</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
} else {
        ?>
        <table>
    <tr>
        <th style="font-size:12px !important;font-family: arial; text-align:left;">Customer Name:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['PRODUCT_OWNER'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial; text-align:left;">Created By:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['OWNER_NAME'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial; text-align:left;">Serial No:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['SERIAL_NO'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial;text-align:left;">Expt Delivery Date:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['EXPT_DEL_DATE'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial;text-align:left;">Brand:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['BRAND'] ?></td>
    </tr>
    <tr>
        <th style="font-size:12px !important;font-family: arial;text-align:left;">MPN:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php echo @$store_name[0]['MPN'] ?></td>
    </tr>

    <tr>
        <th style="font-size:12px !important;font-family: arial;text-align:left;">Customer Number:</th>
        <td style="font-size:12px !important;font-family: arial;"><?php trim(str_replace("_", '', @$store_name[0]['CUSTOMER_NUMBER']))?></td>
    </tr>
    <tbody>
</table>
<br>
                        <div style=" width: 280px !important;margin-top:50px !important;">
                            <div style="width: 138px;float:left;font-size:12px;font-family: arial;">
                                <?php date_default_timezone_set("America/Chicago");
        $print_date = date("m/d/Y H:i A ");
        $concatDate = date("m/d/Y");
        echo @$print_date;
        ?>
                            </div>
                            <div style="width: 138px;float:right;font-size:12px;font-family: arial;"> <b>Sale Receipt # <?php echo @$store_name[0]["LZ_POS_REPAIRE_ID"] ?></b> </div>
                            <div style="padding: 2px;font-size:12px;font-family: arial;">Store:
                                <?php echo @$store_name[0]["STORE_NAME"] ?>
                            </div>
                            <div style="text-align: center;font-size: 20px; font-weight: bold; font-family: arial;padding: 3px;">LaptopZone</div>
                            <div style="font-size:12px;font-weight: normal;font-family: arial;text-align: center;">
                                <?php echo @$store_name[0]["ADDRESS"] ?>
                                    <br>
                                    <?php @$store_name[0]["CITY_DESC"]?>
                                        <br>
                                        <?php echo trim(str_replace("_", '', @$store_name[0]["PHONE_NO"])) ?>
                            </div>
                            <br>
                            <?php echo $header_table ?>
                                <table>
                                    <thead>
                                        <tr style="font-size:12px !important;border-bottom: 1px solid #000 !important;font-family: arial;">
                                            <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Item Desc</th>
                                            <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Price</th>
                                            <th style="border-bottom-style: solid; border-bottom: 1px solid #000 !important;">Ext Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
}
}

?>
<?php
$sum_price = 0;
$sum_disc_amt = 0;
$sum_qty = 0;

foreach ($store_name as $data) {
    $text = $data["REPAIRE_DES"];
    $price = '$' . number_format((float) @$data['EXPT_REPAIRE_COST'], 2, '.', ',');
    $item_desc = str_split($text, 40);

    ?>
            <tr style="font-size:12px !important;font-family: arial;">
    <td>
        <div style="width:120px;">' . @$item_desc[0] . '</div>
    </td>
    <td style="text-align:center;">' . $price . '</td>
    <td style="text-align:center;">' . $price . '</td>
</tr>
<?php
$sum_price = $sum_price + $data['EXPT_REPAIRE_COST'];

    $sum_disc_amt = $sum_disc_amt + 0;
}
$total = $sum_price - $sum_disc_amt;
// $pay_mode = @$result['data'][0]['PAY_MODE'];
$sales_tax = ($total / 100) * @$store_name[0]['SALE_TAX'];
$sales_tax = number_format((float) @$sales_tax, 2, '.', ',');
$service_charges = number_format((float) @$store_name[0]['SERVICE_CHARGES']);
$receipt_total = $total + $sales_tax;
$receipt_totals = $receipt_total-@$store_name[0]['ADVANCE_PAYMENT'];
$receipt_total = number_format((float) @$receipt_total, 2, '.', ',');
$receipt_totals = number_format((float) @$receipt_totals, 2, '.', ',');
$rcpt_total = "Credit: $" . $receipt_total;
$barDate = str_replace('/', '', $concatDate);
$str1 = substr($barDate, 0, 4);
$str2 = substr($barDate, 6, 8);
$docDateStr = $str1 . $str2;
?>

<tr style="font-size:12px !important;font-family: arial;">
    <td style="border-top-style: solid; border-top: 1px solid #000 !important;"></td>
    <td style="border-top-style: solid; border-top: 1px solid #000 !important;"><b>Subtotal:</b></td>
    <td style="border-top-style: solid; border-top: 1px solid #000 !important;"><?php echo '$' . $sum_price ?></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
    <td></td>
    <td>Disc Amount:</td>
    <td><?php echo '$' . $sum_disc_amt ?></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
    <td></td>
    <td>Advance Pay:</td>
    <td><?php echo '$' . number_format((float) @$store_name[0]['ADVANCE_PAYMENT'] , 2, '.', ','); ?></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
    <td></td>
    <td>Service Charges:</td>
    <td><?php echo '$' . number_format((float)  @$store_name[0]['SERVICE_CHARGES'] , 2, '.', ','); ?></td>
</tr>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
    <td></td>
    <td><b>Total:</b></td>
    <td><?php echo '$' . number_format((float)  @$total , 2, '.', ',');  ?></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
    <td>Exempt</td>
    <td><?php echo @$store_name[0]['SALE_TAX'] ?> % Tax:</td>
    <td>+ <?php echo '$' . $sales_tax ?></td>
</tr>
<tr>
    <td> </td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
    <td colspan="2"><b>GRAND TOTAL:</b></td>
    <td><b><?php echo '$' . $receipt_total ?></b></td>
</tr>
<tr style="font-size:12px !important;font-family: arial;">
    <td>
        <div style="width:90px;"><?php echo $rcpt_total ?></div>
    </td>
    <td></td>
    <td></td>
    <tr style="font-size:12px !important;font-family: arial;">
        <td></td>
        <td>Total Pay:</td>
        <td><?php echo '$' . $receipt_totals ?></td>
    </tr>
</tr>
</tbody>
</table>
<br>
<div style="text-align:center;margin:5px;font-size: 12px;font-weight: normal;font-family: arial;">Thanks for shopping with us!</div>
<div style="margin:5px;text-align:center;margin:5px;font-size: 12px;font-weight: normal;font-family: arial;"><img style="margin-bottom:3px;width: 200px;" alt="pos_barcode" src="<?php echo base_url() ?>'assets/barcode/barcode.php?text=<?php echo @$store_name[0]["LZ_POS_REPAIRE_ID "] ?>"/>
    <div style="margin:5px;text-align:center;margin:5px;font-size: 12px;font-weight: normal;font-family: arial;"><?php echo @$store_name[0]["LZ_POS_REPAIRE_ID"] . '-' . @$docDateStr ?></div>
</div>
<div style="text-align:left;font-size: 12px;font-weight: normal;font-family: arial;margin-top: 50; margin-bottom:40px;">
    <table>
        <thead>
            <tr style="font-size:12px !important;border-bottom: 1px solid #000 !important;font-family: arial;border-top-style: solid; border-top: 1px solid #000 !important;">
                <th style="float: left;border-top-style: solid; border-top: 1px solid #000 !important;">Store Manager</th>
                <th style="float: right;margin-left: 70;border-top-style: solid;border-top: 1px solid #000 !important;">Customer Signature</th>
            </tr>
        </thead>
    </table>
</div>
</div><span>&#9986;</span>-----------------------------------------------------
'</body> </html>'