<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * m_payouts Model
 */

class m_payouts extends CI_Model
{
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function Search_Ebay_Sale_Payout_Server_Side()
    {

        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $filter = $this->input->post("filter");
        if (!empty($merchId) && isset($merchId['value'])) {
            $merchId = $merchId['value'];
        }

        $account_id = $this->input->post("account_id");
        if (!empty($account_id) && isset($account_id['value'])) {
            $account_id = $account_id['value'];
        }
        $skipDate = $this->input->post('skipDate');

        $searchItem = $this->input->post('searchItem');
        $searchItem = trim(str_replace("  ", ' ', $searchItem));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);

        // var_dump($merchId);
        // exit;
        $requestData = $_REQUEST;

        $searchValue = strtoupper($requestData['search']['value']);
        $searchValue = trim(str_replace("  ", ' ', $searchValue));
        $searchValue = str_replace(array("`,'"), "", $searchValue);
        $searchValue = str_replace(array("'"), "''", $searchValue);
        //$searchValue='254532353361';
        $str = explode(' ', $searchValue);

        $columns = array(
            // datatable column index  => database column name
            0 => 'ORDER_ID',
            1 => 'TRACKING_NUMBER',
            2 => 'ORDER_ID',
            3 => 'EBAY_ITEM_ID',
            4 => 'ITEM_TITLE',
            5 => 'BUYER_NAME',
            6 => 'SALE_DATE',
            7 => 'SALE_PRICE',
            8 => 'REFUND_AMOUNT',
            9 => 'TOTAL_AMOUNT',
            10 => 'QTY',
            11 => 'SALES_TAX_PERC',
            12 => 'DISC_AMT',
            13 => 'TOTAL_SALE_PRICE',
            14 => 'ACC_NAME',
        );
        $pack_inv_detail = " SELECT * from (SELECT
       TRACKING_NUMBER,
       'ebay'  filter,
       ORDER_ID,
       EBAY_ITEM_ID,
       ITEM_TITLE,
       BUYER_NAME,
       TO_DATE(SALE_DATE, 'YYYY-MM-DD HH24:MI:SS') SALE_DATE,
       SALE_PRICE ,
       QTY,
       ''SALES_TAX_PERC,
       ''DISC_AMT,
       TOTAL_SALE_PRICE,
       ACC_NAME,
       MERCHANT_ID,
       decode(return_id,null,'Completed','Return') return_status,
       REFUND_AMOUNT
  FROM (SELECT max(sd.extendedorderid) ORDER_ID,
               is_number(max(sd.paypal_transaction_id)) paypal_transaction_id,
               max(sd.tracking_number) tracking_number,
               max(sd.return_id) return_id,
               max(sd.order_id) ORDER_PACKING_ID, /*max(M.ORDER_PACKING_ID) ORDER_PACKING_ID,*/
               max(sd.extendedorderid) SALES_RECORD_NUMBER,
               max(DT.ORDER_PACKING_DT_ID) ORDER_PACKING_DT_ID,
               MAX(sd.item_id) EBAY_ITEM_ID,
               MAX(sd.item_title) ITEM_TITLE,
               NVL(MAX(sd.SALE_PRICE), 0) SALE_PRICE,
               NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) TOTAL_SALE_PRICE,
               MAX(sd.quantity) QTY,
               NVL(MAX(sd.shippinglabelrate), null) SHIP_COST,
               SUM(NVL(DT.PACKING_COST, 0)) PACKING_COST,
               MAX(NVL(sd.SERVICE_COST, null)) SERVICE_COST,
               MAX(sd.buyer_fullname) BUYER_NAME,
               MAX(TO_CHAR(sd.sale_date, 'dd/mm/YY HH24:MI:SS')) SALE_DATE,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      NVL(MAX(sd.ebay_fee_perc), 0),
                      0) EBAY_FEE,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      (0.07 * (MAX(sd.SALE_PRICE) * MAX(sd.quantity))),
                      0) MARKTPLACE,
               MAX(ACOUNT_DET.ACOUNT_NAME) ACC_NAME,
               MAX(ACOUNT_DET.merchant_id) MERCHANT_ID,
               max(ACOUNT_DET.item_id) item_id,
               MAX(SD.REFUND_AMOUNT) REFUND_AMOUNT
          FROM LZ_SALESLOAD_DET SD,
               LJ_ORDER_PACKING_DT DT,
               (SELECT MM.EBAY_ITEM_ID,
                       max(dd.merchant_id) merchant_id,
                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                       max(mm.item_id) item_id
                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
         WHERE sd.order_id in (select d.order_id
                                 from ebay_list_mt       e,
                                      lj_merhcant_acc_dt a,
                                      lz_salesload_det   d
                                where a.acct_id = e.lz_seller_acct_id
                                  and d.item_id = e.ebay_item_id
                                  and a.merchant_id = '$merchId'
                                group by d.order_id)
           and sd.orderstatus = 'Completed'
           /*and sd.return_id is null*/
           AND is_number(sd.paypal_transaction_id) = 1
           and sd.order_id = dt.order_id(+)
           AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID
           and sd.payout_mt_id is  null
           /*and sd.extendedorderid in('12-04539-69035','21-04557-56524')*/
           AND sd.SALE_DATE >
               TO_DATE('2019-12-30 23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        if (!empty($searchValue)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $pack_inv_detail .= " and (UPPER(sd.item_title) LIKE '%$key%' ";
                    } else {
                        $pack_inv_detail .= " AND UPPER(sd.item_title) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $pack_inv_detail .= " and (UPPER(sd.item_title) LIKE '%$searchValue%' ";
            }

            $pack_inv_detail .= " OR UPPER(sd.item_id) LIKE '%$searchValue%'
            OR UPPER(sd.extendedorderid) LIKE '%$searchValue%'
            OR UPPER(sd.buyer_fullname) LIKE '%$searchValue%'
        )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }
        $pack_inv_detail .= "  GROUP BY sd.ORDER_ID)";

        $pack_inv_detail .= " union all SELECT 'Sold in Pos' TRACKING_NUMBER,
'pos'  filter,
to_char(dm.barcode_id) ORDER_ID,
       '-' EBAY_ITEM_ID,
       dm.item_desc ITEM_TITLE,
       pm.buyer_name,
       TO_DATE(pm.entered_date_time, 'YYYY-MM-DD HH24:MI:SS') SALE_DATE,

       dm.price SALE_PRICE,
       dm.qty,
       to_char(dm.sales_tax_perc) sales_tax_perc,
       to_char(dm.disc_amt)disc_amt,
       (nvl(dm.price, 0) +
       ((nvl(dm.price, 0)) / 100) * (nvl(dm.sales_tax_perc, 0)) -
       (nvl(dm.disc_amt, 0))) TOTAL_SALE_PRICE ,/*NET_AMOUNT*/
       '-'ACC_NAME,
       mt.merchant_id,
       'Completed' return_status,
       0 REFUND_AMOUNT
  from lz_pos_mt pm, lz_pos_det dm,lz_merchant_barcode_dt md,lz_merchant_barcode_mt mt
 where pm.lz_pos_mt_id = dm.lz_pos_mt_id
 and dm.barcode_id = md.barcode_no
 and md.mt_id = mt.mt_id
  and mt.merchant_id = '$merchId'
   /*and dm.barcode_id in
       (select d.barcode_no
          from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
         where m.mt_id = d.mt_id
           and m.merchant_id = '1')*/
   AND pm.deleted_by IS NULL
   AND pm.return_by IS NULL
   and dm.payout_mt_id is  null
   /*and dm.barcode_id in ( 245818,201929)*/

";
        if (!empty($searchValue)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $pack_inv_detail .= " and (UPPER(dm.item_desc) LIKE '%$key%' ";
                    } else {
                        $pack_inv_detail .= " AND UPPER(dm.item_desc) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $pack_inv_detail .= " and (UPPER(dm.item_desc) LIKE '%$searchValue%' ";
            }

            $pack_inv_detail .= " OR UPPER(to_char(dm.barcode_id)) LIKE '%$searchValue%'
            OR UPPER(pm.buyer_name) LIKE '%$searchValue%'
    )";
            // $pack_inv_detail .="  )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }

        if (!empty($filter) && $filter == 'pos' || $filter == 'ebay') {
            $pack_inv_detail .= " )  where filter ='$filter'";
        } else {
            $pack_inv_detail .= " )";
        }

/** Datatable Function Start */
        $query = $this->db->query($pack_inv_detail);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;

        $sqlLength = "SELECT  * FROM (SELECT  q.*, rownum rn FROM  ($pack_inv_detail) q )";

        $sqlLength .= " WHERE   ROWNUM <= " . $requestData['length'] . " AND rn>= " . $requestData['start'];

        // comment Ordering  Datatable

        if (!empty($columns[$requestData['order'][0]['column']])) {
            $sqlLength .= " ORDER BY  " . $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        }
        /** Datatable Function End */
        $sqlLength = $this->db->query($sqlLength);
        $data = array();

        if ($sqlLength->num_rows() > 0) {
            $result = $sqlLength->result_array();
            foreach ($result as $keys => $rows) {
                //$list_price = @$rows['LIST_PRIC'];
                // Chages made by Adil (Fahad)
                $sale_price = '$ ' . number_format((float) @$rows['SALE_PRICE'], 2, '.', '');
                $refund_amount = '$ ' . number_format((float) @$rows['REFUND_AMOUNT'], 2, '.', '');
                $total_amount = number_format((float) @$rows['SALE_PRICE'], 2, '.', '') - number_format((float) @$rows['REFUND_AMOUNT'], 2, '.', '');
                $total_amount = '$ ' . $total_amount;
                $ship_cost = number_format((float) @$rows['SHIP_COST'], 2, '.', '');
                $total_sale_price = '$ ' . number_format((float) @$rows['TOTAL_SALE_PRICE'], 2, '.', '');
                if (!empty($SALES_TAX_PERC)) {
                    $SALES_TAX_PERC = number_format((float) @$rows['SALES_TAX_PERC'], 2, '.', '') . ' %';
                } else {
                    $SALES_TAX_PERC = '-';
                }
                if (!empty($DISC_AMT)) {
                    $DISC_AMT = '$ ' . number_format((float) @$rows['DISC_AMT'], 2, '.', '');
                } else {
                    $DISC_AMT = '-';
                }

                $data[] = array(
                    // "<input type='checkbox' name='payoutOrderId' class='checkbox' id='checkPayout' value=" . @$rows['ORDER_ID'] . " />
                    @$rows['ORDER_ID'],
                    @$rows['TRACKING_NUMBER'] . '<br>' . @$rows['RETURN_STATUS'],
                    @$rows['ORDER_ID'],
                    "<a class='TableImage' href=http://www.ebay.com/itm/" . @$rows['EBAY_ITEM_ID'] . " target='_blank' style='color:#2a90b8;'>
                    " . @$rows['EBAY_ITEM_ID'] . "
                   </a> ",
                    @$rows['ITEM_TITLE'],
                    @$rows['BUYER_NAME'],
                    @$rows['SALE_DATE'],
                    "<span style='float: right;'>" . @$sale_price . "</span>",
                    "<span style='float: right;'>" . @$refund_amount . "</span>",
                    "<span style='float: right;'>" . @$total_amount . "</span>",
                    @$rows['QTY'],
                    "<span style='float: right;'>" . @$SALES_TAX_PERC . "</span>",
                    "<span style='float: right;'>" . @$DISC_AMT . "</span>",
                    // @$rows['SALES_TAX_PERC'],
                    // @$rows['DISC_AMT'],
                    "<span style='float: right;'>" . @$total_sale_price . "</span>",
                    // @$rows['PAYMENT_METHOD'],
                    // "<span style='float: right;'>" . @$total_sale_price . "</span>",
                    // @$rows['ACC_NAME'],
                    @$rows['ACC_NAME'] . "<input type='hidden' name='orderPrice' id='checkPayout" . @$rows['ORDER_ID'] . "' value=" . @$rows['TOTAL_SALE_PRICE'] . " />
                    <input type='hidden' name='orderPrice' id='checkPayoutMerchant" . @$rows['ORDER_ID'] . "' value=" . @$rows['MERCHANT_ID'] . " />
                    <input type='hidden' name='orderQty' id='checkPayoutQty" . @$rows['ORDER_ID'] . "' value=" . @$rows['QTY'] . " />",

                );

            }

            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered),
                // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // $images = $this->get_ebay_pictures($pack_inv_detail);
            // return array('data' => $pack_inv_detail, 'count_data' => $pack_inv_counts, 'status' => true, 'images' => $images['uri']);
        } else {
            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('data' => $pack_inv_detail, 'count_data' => $pack_inv_counts, 'status' => false, 'images' => array());
        }

    }

    public function Get_Merchant_Pos_Invoice_Server_Side()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $mer_id = $this->input->post("merchId");
        if (!empty($mer_id) && isset($mer_id['value'])) {
            $mer_id = $mer_id['value'];
        }
        $account_id = $this->input->post("account_id");
        if (!empty($account_id) && isset($account_id['value'])) {
            $account_id = $account_id['value'];
        }
        $skipDate = $this->input->post("skipDate");

        $searchItem = $this->input->post('searchItem');
        $searchItem = trim(str_replace("  ", ' ', $searchItem));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);

        // var_dump($merchId);
        // exit;
        $requestData = $_REQUEST;

        $searchValue = strtoupper($requestData['search']['value']);
        $searchValue = trim(str_replace("  ", ' ', $searchValue));
        $searchValue = str_replace(array("`,'"), "", $searchValue);
        $searchValue = str_replace(array("'"), "''", $searchValue);
        //$searchValue='254532353361';
        $str = explode(' ', $searchValue);
        $columns = array(
            // datatable column index  => database column name
            0 => 'BARCODE_NO',
            1 => 'PAY_MODE',
            2 => 'ITEM_DESC',
            3 => 'QTY',
            4 => 'CREATED_DATE',
            5 => 'PRICE',
            6 => 'SALES_TAX_PERC',
            7 => 'DISC_AMT',
            8 => 'NET_AMOUNT',
            9 => 'PRICE',
            // 9 => 'PAYOUT',
        );
        $data = "SELECT DECODE(pm.pay_mode, 'R', 'CARD', 'C', 'CASH') PAY_MODE,
        pm.paid_status,
        TO_CHAR(pm.entered_date_time, 'dd/mm/YY HH24:MI:SS') CREATED_DATE,
        dm.barcode_id BARCODE_NO,
        dm.*,
round(round(nvl(dm.price, 0) +
(nvl(dm.price, 0) / 100) * nvl(dm.sales_tax_perc, 0) -
nvl(dm.disc_amt, 0),
2) * (0.05),
2) CHARGES,
(nvl(dm.price, 0)+
                 ((nvl(dm.price, 0)) / 100) * (nvl(dm.sales_tax_perc, 0)) -
                 (nvl(dm.disc_amt, 0))) NET_AMOUNT
        from lz_pos_mt pm, lz_pos_det dm
        where pm.lz_pos_mt_id = dm.lz_pos_mt_id
        and dm.barcode_id in
        (select d.barcode_no
        from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
        where m.mt_id = d.mt_id
        and m.merchant_id = '$mer_id')
        -- AND dm.lz_pos_mt_id = '90'
        AND pm.deleted_by IS NULL
        AND pm.return_by IS NULL ";
        if (empty($skipDate)) {
            $data .= " AND pm.entered_date_time between
            TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
            TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        }
        if (!empty($searchValue)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $data .= " and (UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
                    } else {
                        $data .= " AND UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $data .= " and (UPPER(dm.ITEM_DESC) LIKE '%$searchValue%' ";
            }

            $data .= " OR UPPER(dm.barcode_id) LIKE '%$searchValue%'
          )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }
        $data .= " order by dm.barcode_id asc";
        /** Datatable Function Start */
        $query = $this->db->query($data);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;

        $sqlLength = "SELECT  * FROM (SELECT  q.*, rownum rn FROM  ($data) q )";

        $sqlLength .= " WHERE   ROWNUM <= " . $requestData['length'] . " AND rn>= " . $requestData['start'];

        // comment Ordering  Datatable

        if (!empty($columns[$requestData['order'][0]['column']])) {
            $sqlLength .= " ORDER BY  " . $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        }
        /** Datatable Function End */
        $sqlLength = $this->db->query($sqlLength);
        $data = array();
        if ($sqlLength->num_rows() > 0) {
            // $images = $this->get_pictures($result->result_array());
            $result = $sqlLength->result_array();
            foreach ($result as $keys => $rows) {
                //$list_price = @$rows['LIST_PRIC'];
                // Chages made by Adil (Fahad)
                $price = '$ ' . number_format((float) @$rows['PRICE'], 2, '.', '');
                $sale_tax = number_format((float) @$rows['SALES_TAX_PERC'], 2, '.', '') . ' %';
                $discount_amount = '$ ' . number_format((float) @$rows['DISC_AMT'], 2, '.', '');
                $net_amount = '$ ' . number_format((float) @$rows['NET_AMOUNT'], 2, '.', '');
                $charges = '$ ' . number_format((float) @$rows['CHARGES'], 2, '.', '');

                $data[] = array(
                    @$rows['BARCODE_NO'],
                    @$rows['PAY_MODE'],
                    @$rows['ITEM_DESC'],
                    @$rows['QTY'],
                    @$rows['CREATED_DATE'],
                    "<span style='float: right;'>" . @$price . "</span>",
                    @$sale_tax,
                    "<span style='float: right;'>" . @$discount_amount . "</span>",
                    "<span style='float: right;'>" . @$net_amount . "</span>",
                    "<span style='float: right;'>" . @$net_amount . "</span>",
                    // @$charges,

                );
            }
            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('status' => true, 'data' => $result->result_array(), 'images' => $images['uri']);
        } else {
            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('status' => false, 'data' => array(), 'images' => array());
        }
    }

    public function Get_Merchant_Pos_Invoice_Sum()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $mer_id = $this->input->post("merchId");
        $skipDate = $this->input->post("skipDate");

        if (!empty($mer_id) && isset($mer_id['value'])) {
            $mer_id = $mer_id['value'];
        }

        $account_id = $this->input->post("account_id");
        if (!empty($account_id) && isset($account_id['value'])) {
            $account_id = $account_id['value'];
        }

        $searchItem = strtoupper($this->input->post('searchItem'));
        $searchItem = trim(str_replace("  ", ' ', $searchItem));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);

        // var_dump($merchId);
        // exit;
        //$searchValue='254532353361';
        $str = explode(' ', $searchItem);

        $data = "SELECT
        SUM(round(round(nvl(dm.price, 0) +
        (nvl(dm.price, 0) / 100) * nvl(dm.sales_tax_perc, 0) -
        nvl(dm.disc_amt, 0),
        2) * (0.05),
  2)) SUM_CHARGES,

    SUM((nvl(dm.price, 0) +
    ((nvl(dm.price, 0)) / 100) * (nvl(dm.sales_tax_perc, 0)) -
    (nvl(dm.disc_amt, 0)))) SUM_NET_AMOUNT,
    SUM(dm.qty) SUM_QTY
        from lz_pos_mt pm, lz_pos_det dm
        where pm.lz_pos_mt_id = dm.lz_pos_mt_id
        and dm.barcode_id in
        (select d.barcode_no
        from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
        where m.mt_id = d.mt_id
        and m.merchant_id = '$mer_id')
        -- AND dm.lz_pos_mt_id = '90'
        AND pm.deleted_by IS NULL
        AND pm.return_by IS NULL ";
        if (empty($skipDate)) {
            $data .= " AND pm.entered_date_time between
            TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
            TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        }

        if (!empty($searchItem)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $data .= " and (UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
                    } else {
                        $data .= " AND UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $data .= " and (UPPER(dm.ITEM_DESC) LIKE '%$searchItem%' ";
            }

            $data .= " OR UPPER(dm.barcode_id) LIKE '%$searchItem%'
          )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }
        $data .= " order by dm.barcode_id asc";
        $query = $this->db->query($data);

        if ($query->num_rows() > 0) {
            return array('status' => true, 'data' => $query->result_array());
        } else {

            return array('status' => false, 'data' => $query->result_array());
        }
    }

    public function Search_Ebay_Sale_Payout_Sum()
    {

        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        if (!empty($merchId) && isset($merchId['value'])) {
            $merchId = $merchId['value'];
        }
        $account_id = $this->input->post("account_id");
        if (!empty($account_id) && isset($account_id['value'])) {
            $account_id = $account_id['value'];
        }
        $skipDate = $this->input->post('skipDate');

        $searchItem = strtoupper($this->input->post('searchItem'));
        $searchItem = trim(str_replace("  ", ' ', $searchItem));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);

        // var_dump($merchId);
        // exit;
        //$searchValue='254532353361';
        $str = explode(' ', $searchItem);

        $ebay_payout = "SELECT /*ebay payout amount*/
               sum(nvl(QTY,0)) QTY,
               sum((nvl(TOTAL_SALE_PRICE,0)) - (nvl(refund_amount,0))) Amount,
               sum(nvl(total_order,0)) total_order
          FROM (SELECT
               count(sd.extendedorderid) total_order,
               NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) TOTAL_SALE_PRICE,
               MAX(sd.quantity) QTY,
               NVL(MAX(sd.refund_amount),0) refund_amount
          FROM LZ_SALESLOAD_DET SD,
               LJ_ORDER_PACKING_DT DT,
               (SELECT MM.EBAY_ITEM_ID,
                       max(dd.merchant_id) merchant_id,
                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                       max(mm.item_id) item_id
                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
         WHERE sd.order_id in (select d.order_id
                                 from ebay_list_mt       e,
                                      lj_merhcant_acc_dt a,
                                      lz_salesload_det   d
                                where a.acct_id = e.lz_seller_acct_id
                                  and d.item_id = e.ebay_item_id
                                  and a.merchant_id = '$merchId'
                                group by d.order_id)
           and sd.orderstatus = 'Completed'
           /*and sd.return_id is null*/
           AND is_number(sd.paypal_transaction_id) = 1
           and sd.order_id = dt.order_id(+)
           AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID
           and sd.payout_mt_id is null
           /*and sd.extendedorderid in ('12-04539-69035', '21-04557-56524')*/
           AND sd.SALE_DATE >
               TO_DATE('2019-12-30 23:59:59', 'YYYY-MM-DD HH24:MI:SS')
         GROUP BY sd.ORDER_ID)";

        $ebay_payout = $this->db->query($ebay_payout)->result_array();

        $pos_payout = "select sum(nvl(qty, 0)) qty,
               sum(nvl(TOTAL_SALE_PRICE, 0)) Amount,
               sum(nvl(total_order, 0)) total_order
          from (SELECT dm.qty,
               (nvl(dm.price, 0) +
               ((nvl(dm.price, 0)) / 100) * (nvl(dm.sales_tax_perc, 0)) -
               (nvl(dm.disc_amt, 0))) TOTAL_SALE_PRICE,
               dm.qty total_order
          from lz_pos_mt pm, lz_pos_det dm
         where pm.lz_pos_mt_id = dm.lz_pos_mt_id
           and dm.barcode_id in
               (select d.barcode_no
                  from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
                 where m.mt_id = d.mt_id
                   and m.merchant_id = '$merchId')
           AND pm.deleted_by IS NULL
           AND pm.return_by IS NULL
           and dm.payout_mt_id is null
           /*and dm.barcode_id in (245818, 201929)*/)";

        $pos_payout = $this->db->query($pos_payout)->result_array();

        $ebay_pos_sum = " SELECT sum(nvl(QTY, 0)) QTY,
       sum((nvl(TOTAL_SALE_PRICE, 0)) - (nvl(refund_amount,0))) Amount,
       sum(nvl(total_order, 0)) total_order
  FROM (SELECT count(sd.extendedorderid) total_order,
               NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) TOTAL_SALE_PRICE,
               MAX(sd.quantity) QTY,
               NVL(MAX(sd.refund_amount),0) refund_amount
          FROM LZ_SALESLOAD_DET SD,
               LJ_ORDER_PACKING_DT DT,
               (SELECT MM.EBAY_ITEM_ID,
                       max(dd.merchant_id) merchant_id,
                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                       max(mm.item_id) item_id
                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
         WHERE sd.order_id in (select d.order_id
                                 from ebay_list_mt       e,
                                      lj_merhcant_acc_dt a,
                                      lz_salesload_det   d
                                where a.acct_id = e.lz_seller_acct_id
                                  and d.item_id = e.ebay_item_id
                                  and a.merchant_id = '$merchId'
                                group by d.order_id)
           and sd.orderstatus = 'Completed'
           /*and sd.return_id is null*/
           AND is_number(sd.paypal_transaction_id) = 1
           and sd.order_id = dt.order_id(+)
           and sd.payout_mt_id is null
           AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID
           /*and sd.extendedorderid in ('12-04539-69035', '21-04557-56524')*/
           AND sd.SALE_DATE >
               TO_DATE('2019-12-30 23:59:59', 'YYYY-MM-DD HH24:MI:SS')
         GROUP BY sd.ORDER_ID
        union all
        SELECT dm.qty,
               (nvl(dm.price, 0) +
               ((nvl(dm.price, 0)) / 100) * (nvl(dm.sales_tax_perc, 0)) -
               (nvl(dm.disc_amt, 0) )) TOTAL_SALE_PRICE,
               dm.qty total_order,
               0 refund_amount
          from lz_pos_mt pm, lz_pos_det dm
         where pm.lz_pos_mt_id = dm.lz_pos_mt_id
           and dm.barcode_id in
               (select d.barcode_no
                  from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
                 where m.mt_id = d.mt_id
                   and m.merchant_id = '$merchId')
           AND pm.deleted_by IS NULL
           AND pm.return_by IS NULL
           and dm.payout_mt_id is null
           /*and dm.barcode_id in (245818, 201929)*/) ";

        $ebay_pos_sum = $this->db->query($ebay_pos_sum)->result_array();

        return array('ebay_payout' => $ebay_payout, 'pos_payout' => $pos_payout, 'ebay_pos_sum' => $ebay_pos_sum, 'status' => true);

    }

    public function Save_Payouts()
    {
        $order_id = ($this->input->post('order_id'));
        $orderId = trim(implode(",", ($order_id)));
        $user_id = $this->input->post('user_id');
        $merchant_id = $this->input->post('merchant_id');
        $payoutRemarks = trim($this->input->post('payoutRemarks'));
        $payoutRemarks = trim(str_replace("  ", ' ', $payoutRemarks));
        $payoutRemarks = str_replace(array("`,'"), "", $payoutRemarks);
        $payoutRemarks = str_replace(array("'"), "''", $payoutRemarks);
        $payoutDate = $this->input->post('payoutDate');

        // var_dump($payoutRemarks);
        // exit;
        if (!empty($merchant_id) && $merchant_id['value']) {
            $merchant_id = $merchant_id['value'];
        }
        $this->db->query("call pro_generate_payouts('$orderId', $user_id, $merchant_id,TO_DATE('$payoutDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'$payoutRemarks')");

        return array('status' => true, 'message' => 'Payout Genrated successfully');

    }
    public function Get_Payouts()
    {
        $startDate = $this->input->post("startDate");
        $endDate = $this->input->post("endDate");
        $merchant_id = $this->input->post("merchId");
        if (!empty($merchant_id) && isset($merchant_id['value'])) {
            $merchant_id = $merchant_id['value'];
        }
        $account_id = $this->input->post("account_id");
        if (!empty($account_id) && isset($account_id['value'])) {
            $account_id = $account_id['value'];
        }
        $skipDate = $this->input->post("skipDate");

        $searchItem = $this->input->post('searchItem');
        $searchItem = trim(str_replace("  ", ' ', $searchItem));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);

        // var_dump($merchId);
        // exit;
        // $requestData = $_REQUEST;

        // $searchValue = strtoupper($requestData['search']['value']);
        // $searchValue = trim(str_replace("  ", ' ', $searchValue));
        // $searchValue = str_replace(array("`,'"), "", $searchValue);
        // $searchValue = str_replace(array("'"), "''", $searchValue);

        //$searchValue='254532353361';
        // $str = explode(' ', $searchValue);
        // $columns = array(
        //     // datatable column index  => database column name
        //     0 => 'PAYOUT_MT_ID',
        //     1 => 'PAYOUT_DATE',
        //     2 => 'PAYOUT_BY',
        //     3 => 'MERCHANT',
        //     4 => 'NO_OF_ORDERS',
        //     5 => 'QTY',
        //     6 => 'AMOUNT',
        // );
        $data = "select payout_mt_id payout_mt_id,
       max(payout_date) payout_date,
       max(payout_by) payout_by,
       max(merchant) merchant,
       count(no_of_orders) no_of_orders,
       sum(QTY) QTY,
       sum(Amount) Amount,
       max(remarks) REMARKS
  from (select pm.payout_mt_id,
               pm.payout_date,
               em.first_name payout_by,
               lm.buisness_name merchant,
               /* max(pm.payout_status) payout_status,  */
               /*max(pd.payout_source)payout_source,*/
               pd.order_id no_of_orders,
               de.quantity QTY,
               pm.remarks remarks,
               nvl(de.SALE_PRICE, 0) * nvl(de.quantity, 0) /*TOTAL_SALE_PRICE*/ Amount

          from lj_payout_Mt     pm,
               lj_payout_dt     pd,
               lz_merchant_mt   lm,
               employee_mt      em,
               LZ_SALESLOAD_DET de
         where pm.merchant_id = lm.merchant_id
           and pm.payout_mt_id = pd.payout_mt_id
           and pm.payout_by = em.employee_id
           and pm.payout_mt_id = de.payout_mt_id
           and pd.order_id = de.extendedorderid
           and pd.payout_source = 'ebay'
           and pm.merchant_id = $merchant_id";
        // if (!empty($account_id)) {
        //     $data .= "  and md.acct_id = '$account_id'";
        // }
        // if (empty($skipDate)) {
        //     $data .= " AND pm.payout_date between
        //        TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
        //        TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        // }
        $data .= " /*
         group by pd.payout_mt_id*/
        union all
        select pm.payout_mt_id,
               pm.payout_date,
               em.first_name payout_by,
               lm.buisness_name merchant,
               /* max(pm.payout_status) payout_status,  */
               /*max(pd.payout_source)payout_source,*/
               to_char(pod.qty) no_of_orders,
               pod.qty QTY,
               pm.remarks remarks,
               (nvl(pod.price, 0) + ((nvl(pod.price, 0)) / 100) *
               (nvl(pod.sales_tax_perc, 0)) -
               (nvl(pod.disc_amt, 0))) /*TOTAL_SALE_PRICE*/ Amount

          from lj_payout_Mt   pm,
               lj_payout_dt   pd,
               lz_merchant_mt lm,
               employee_mt    em,
               lz_pos_det     pod,
               lz_pos_mt pom
         where pm.merchant_id = lm.merchant_id
           and pm.payout_mt_id = pd.payout_mt_id
           and pm.payout_by = em.employee_id
           and pm.payout_mt_id = pod.payout_mt_id
           and pd.order_id = pod.barcode_id
           and pm.payout_mt_id = pod.payout_mt_id
           and pod.lz_pos_mt_id = pom.lz_pos_mt_id
            AND pom.deleted_by IS NULL
           AND pom.return_by IS NULL
           and pd.payout_source = 'pos'
           and pm.merchant_id = $merchant_id";
        // if (!empty($account_id)) {
        //     $data .= "  and md.acct_id = '$account_id'";
        // }
        // if (empty($skipDate)) {
        //     $data .= " AND pm.payout_date between
        //        TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
        //        TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        // }

        $data .= " /*group by pd.payout_mt_id*/) group by payout_mt_id ";

        // //         if (!empty($searchValue)) {
        //         //             if (count($str) > 1) {
        //         //                 $i = 1;
        //         //                 foreach ($str as $key) {
        //         //                     if ($i === 1) {
        //         //                         $data .= " and (UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
        //         //                     } else {
        //         //                         $data .= " AND UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
        //         //                     }
        //         //                     $i++;
        //         //                 }
        //         //             } else {
        //         //                 $data .= " and (UPPER(dm.ITEM_DESC) LIKE '%$searchValue%' ";
        //         //             }

// //             $data .= " OR UPPER(dm.barcode_id) LIKE '%$searchValue%'
        //         //   )";
        //         //             //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        //         //         }
        // $data .= " group by pd.payout_mt_id";
        /** Datatable Function Start */
        $query = $this->db->query($data);
        if ($query->num_rows() > 0) {
            return array('status' => true, 'data' => $query->result_array());
        } else {
            return array('status' => false, 'data' => array());
        }

//         $totalData = $query->num_rows();
        //         $totalFiltered = $totalData;

//         $sqlLength = "SELECT  * FROM (SELECT  q.*, rownum rn FROM  ($data) q )";

//         $sqlLength .= " WHERE   ROWNUM <= " . $requestData['length'] . " AND rn>= " . $requestData['start'];

// // comment Ordering  Datatable

//         if (!empty($columns[$requestData['order'][0]['column']])) {
        //             $sqlLength .= " ORDER BY  " . $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        //         }
        // /** Datatable Function End */
        //         $sqlLength = $this->db->query($sqlLength);
        //         $data = array();
        //         if ($sqlLength->num_rows() > 0) {
        //             // $images = $this->get_pictures($result->result_array());
        //             $result = $sqlLength->result_array();
        //             foreach ($result as $keys => $rows) {
        //                 //$list_price = @$rows['LIST_PRIC'];
        //                 // Chages made by Adil (Fahad)
        //                 $amount = '$ ' . number_format((float) @$rows['AMOUNT'], 2, '.', '');

//                 $data[] = array(
        //                     // '<button type="button" class="btn btn-danger" id="deletePayout" value="' . $rows['PAYOUT_MT_ID'] . '"><span class="glyphicon glyphicon-trash"></span></button>',
        //                     '<button type="button" class="btn btn-primary"  data-toggle="modal"
        //                     data-target="#myDetailPayout" id="editPayout" value="' . $rows['PAYOUT_MT_ID'] . '"><span class="glyphicon glyphicon-pencil"></span></button>',
        //                     @$rows['PAYOUT_DATE'],
        //                     @$rows['PAYOUT_BY'],
        //                     @$rows['MERCHANT'],
        //                     @$rows['NO_OF_ORDERS'],
        //                     @$rows['QTY'],
        //                     "<span style='float: right;'>" . @$amount . "</span>",

//                 );
        //             }
        //             return $output = array(
        //                 "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
        //                 "recordsTotal" => intval($totalFiltered),
        //                 "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
        //                 "deferLoading" => intval($totalFiltered),
        //                 "data" => $data,
        //             );
        //             // return array('status' => true, 'data' => $result->result_array(), 'images' => $images['uri']);
        //         } else {
        //             return $output = array(
        //                 "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
        //                 "recordsTotal" => intval($totalFiltered),
        //                 "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
        //                 "deferLoading" => intval($totalFiltered),
        //                 "data" => $data,
        //             );
        //             // return array('status' => false, 'data' => array(), 'images' => array());
        //         }
    }

    public function Delete_Payout()
    {
        $payout_mt_id = $this->input->post('payout_mt_id');
        $this->db->query("call pro_delete_payouts($payout_mt_id)");
        return array('status' => true, 'message' => 'Payout Delete Successfully');
    }

    public function Get_Payout_Detail()
    {
        $payout_mt_id = $this->input->post('payout_mt_id');
        $merchant_id = $this->input->post('merId');
        if (!empty($merchant_id) && isset($merchant_id['value'])) {
            $merchant_id = $merchant_id['value'];
        }
        $data = $this->db->query("SELECT * from (SELECT
        /*TRACKING_NUMBER,*/
        'ebay'  filter,
        ORDER_ID,
        EBAY_ITEM_ID,
        ITEM_TITLE,
        /*BUYER_NAME,*/
        TO_DATE(SALE_DATE, 'YYYY-MM-DD HH24:MI:SS') SALE_DATE,
        SALE_PRICE ,
        QTY,
        ''SALES_TAX_PERC,
        ''DISC_AMT,
        TOTAL_SALE_PRICE,
        ACC_NAME,
        MERCHANT_ID
   FROM (SELECT max(sd.extendedorderid) ORDER_ID,
                is_number(max(sd.paypal_transaction_id)) paypal_transaction_id,
                /*max(sd.tracking_number) tracking_number,*/
                max(sd.order_id) ORDER_PACKING_ID, /*max(M.ORDER_PACKING_ID) ORDER_PACKING_ID,*/
                max(sd.extendedorderid) SALES_RECORD_NUMBER,
                max(DT.ORDER_PACKING_DT_ID) ORDER_PACKING_DT_ID,
                MAX(sd.item_id) EBAY_ITEM_ID,
                MAX(sd.item_title) ITEM_TITLE,
                NVL(MAX(sd.SALE_PRICE), 0) SALE_PRICE,
                NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) TOTAL_SALE_PRICE,
                MAX(sd.quantity) QTY,
                NVL(MAX(sd.shippinglabelrate), null) SHIP_COST,
                SUM(NVL(DT.PACKING_COST, 0)) PACKING_COST,
                MAX(NVL(sd.SERVICE_COST, null)) SERVICE_COST,
                /*MAX(sd.buyer_fullname) BUYER_NAME,*/
                MAX(TO_CHAR(sd.sale_date, 'dd/mm/YY HH24:MI:SS')) SALE_DATE,
                DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                       'dfwonline',
                       NVL(MAX(sd.ebay_fee_perc), 0),
                       0) EBAY_FEE,
                DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                       'dfwonline',
                       (0.07 * (MAX(sd.SALE_PRICE) * MAX(sd.quantity))),
                       0) MARKTPLACE,
                MAX(ACOUNT_DET.ACOUNT_NAME) ACC_NAME,
                MAX(ACOUNT_DET.merchant_id) MERCHANT_ID,
                max(ACOUNT_DET.item_id) item_id
           FROM LZ_SALESLOAD_DET SD,
                LJ_ORDER_PACKING_DT DT,
                (SELECT MM.EBAY_ITEM_ID,
                        max(dd.merchant_id) merchant_id,
                        MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                        MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                        max(mm.item_id) item_id
                   FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                  WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                    AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                  GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
          WHERE sd.order_id in (select d.order_id
                                  from ebay_list_mt       e,
                                       lj_merhcant_acc_dt a,
                                       lz_salesload_det   d
                                 where a.acct_id = e.lz_seller_acct_id
                                   and d.item_id = e.ebay_item_id
                                   and a.merchant_id = '$merchant_id'
                                 group by d.order_id)
            and sd.orderstatus = 'Completed'
            and sd.return_id is null
            AND is_number(sd.paypal_transaction_id) = 1
            and sd.order_id = dt.order_id(+)
            AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID
            and sd.payout_mt_id is not null
            and sd.payout_mt_id = $payout_mt_id
            /*and sd.extendedorderid in('12-04539-69035','21-04557-56524')*/
            AND sd.SALE_DATE >
                TO_DATE('2019-12-30 23:59:59', 'YYYY-MM-DD HH24:MI:SS')
                 GROUP BY sd.ORDER_ID)
                  union all SELECT /*'Sold in Pos' TRACKING_NUMBER,*/
 'pos'  filter,
 to_char(dm.barcode_id) ORDER_ID,
        '-' EBAY_ITEM_ID,
        dm.item_desc ITEM_TITLE,
        /*pm.buyer_name,*/
        TO_DATE(pm.entered_date_time, 'YYYY-MM-DD HH24:MI:SS') SALE_DATE,

        dm.price SALE_PRICE,
        dm.qty,
        to_char(dm.sales_tax_perc) sales_tax_perc,
        to_char(dm.disc_amt)disc_amt,
        (nvl(dm.price, 0) +
        ((nvl(dm.price, 0)) / 100) * (nvl(dm.sales_tax_perc, 0)) -
        (nvl(dm.disc_amt, 0))) TOTAL_SALE_PRICE ,/*NET_AMOUNT*/
        '-'ACC_NAME,
        mt.merchant_id
   from lz_pos_mt pm, lz_pos_det dm,lz_merchant_barcode_dt md,lz_merchant_barcode_mt mt
  where pm.lz_pos_mt_id = dm.lz_pos_mt_id
  and dm.barcode_id = md.barcode_no
  and md.mt_id = mt.mt_id
   and mt.merchant_id = '$merchant_id'
    /*and dm.barcode_id in
        (select d.barcode_no
           from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
          where m.mt_id = d.mt_id
            and m.merchant_id = '1')*/
    AND pm.deleted_by IS NULL
    AND pm.return_by IS NULL
    and dm.payout_mt_id is not null
    and dm.payout_mt_id = $payout_mt_id
    /*and dm.barcode_id in ( 245818,201929)*/
    )");

        if ($data->num_rows() > 0) {
            return array('status' => true, 'data' => $data->result_array());
        } else {
            return array('status' => false, 'data' => array());
        }

    }

}
//         $pack_inv_detail = "SELECT

//     QTY SUM_QTY,
//    TOTAL_SALE_PRICE SUM_TOTAL_SALE_PRICE

// FROM (SELECT
//            SUM(NVL(MAX(sd.SALE_PRICE * sd.quantity), 0)) TOTAL_SALE_PRICE,
//            SUM(MAX(sd.quantity)) QTY,

//            MAX(sd.order_id)
//       FROM LZ_SALESLOAD_DET SD,
//            /*LJ_ORDER_PACKING_MT M,*/
//            LJ_ORDER_PACKING_DT DT,
//            (SELECT MM.EBAY_ITEM_ID,
//                    max(dd.merchant_id)merchant_id,
//                    MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
//                    MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
//                    max(mm.item_id) item_id
//               FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
//              WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+) ";
//         if (!empty($account_id)) {
//             $pack_inv_detail .= " and mm.LZ_SELLER_ACCT_ID = $account_id ";
//         }
//         $pack_inv_detail .= " AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
//              GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
//      WHERE sd.order_id in (select d.order_id
//                              from ebay_list_mt       e,
//                                   lj_merhcant_acc_dt a,
//                                   lz_salesload_det   d
//                             where a.acct_id = e.lz_seller_acct_id
//                               and d.item_id = e.ebay_item_id
//                               and a.merchant_id = '$merchId'
//                             group by d.order_id)
//        /*and sd.order_id = m.order_id(+)*/
//        and sd.orderstatus = 'Completed'
//        AND is_number(sd.paypal_transaction_id) = 1
//        and sd.return_id is null
//        and sd.order_id = dt.order_id(+)
//         AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID
//         AND sd.SALE_DATE >TO_DATE ('2019-12-30 23:59:59', 'YYYY-MM-DD HH24:MI:SS')
//        /* AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+) */
//       ";
//         if (!empty($searchItem)) {
//             if (count($str) > 1) {
//                 $i = 1;
//                 foreach ($str as $key) {
//                     if ($i === 1) {
//                         $pack_inv_detail .= " and (UPPER(sd.item_title) LIKE '%$key%' ";
//                     } else {
//                         $pack_inv_detail .= " AND UPPER(sd.item_title) LIKE '%$key%' ";
//                     }
//                     $i++;
//                 }
//             } else {
//                 $pack_inv_detail .= " and (UPPER(sd.item_title) LIKE '%$searchItem%' ";
//             }

//             $pack_inv_detail .= " OR UPPER(sd.extendedorderid) LIKE '%$searchItem%'
//         OR UPPER(sd.tracking_number) LIKE '%$searchItem%'
//         OR UPPER(sd.order_id) LIKE '%$searchItem%'
//         OR UPPER(ACOUNT_DET.ACOUNT_NAME) LIKE '%$searchItem%'
//         OR UPPER(sd.item_id) LIKE '%$searchItem%' )";
//             //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
//         }

//         if (empty($skipDate)) {
//             $pack_inv_detail .= "  AND sd.SALE_DATE  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";

//         }

//         $pack_inv_detail .= "   GROUP BY sd.ORDER_ID) ";
//         // order by ORDER_ID desc
//         // $pack_inv_detail = $this->db->query($pack_inv_detail)->result_array();

//         $query = $this->db->query($pack_inv_detail);

// if ($query->num_rows() > 0) {
//     $result = $query->result_array();

// return array('data' => $result, 'status' => true);
// } else {

//     return array('data' => $result, 'status' => false);
// }
