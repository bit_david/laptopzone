<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Single Entry Model
 */

class m_monthlyInvoice extends CI_Model
{
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function convert_text($text)
    {

        $t = $text;
        // '\'' => '%27',
        // '~' => '%7E',
        // '_' => '%5F',
        // '/' => '%2F',
        // '\\' => '%5C',
        // '.' => '%2E',
        // '%' => '%25',
        // ':' => '%3A',
        // ' ' => '%20',
        $specChars = array(
            '!' => '%21', '"' => '%22',
            '#' => '%23', '$' => '',
            '&' => '%26', '(' => '%28',
            ')' => '%29', '*' => '%2A', '+' => '%2B',
            ',' => '%2C', '-' => '%2D',
            ';' => '%3B',
            '<' => '%3C', '=' => '%3D', '>' => '%3E',
            '?' => '%3F', '@' => '%40', '[' => '%5B',
            ']' => '%5D', '^' => '%5E',
            '`' => '%60', '{' => '%7B',
            '|' => '%7C', '}' => '%7D',
            ',' => '%E2%80%9A',
        );

        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }

        return $t;
    }
    public function Update_Ship_Cost()
    {
        $ship_cost = $this->input->post('ship_cost');
        $order_id = $this->input->post('order_id');
        $order_packing_id = $this->input->post('order_packing_id');
        // UPDATE LJ_ORDER_PACKING_MT SET SHIPING_LABEL_RATE = '$ship_cost' WHERE ORDER_ID = '$order_id'
        $checkExist = $this->db->query("SELECT dd.order_id FROM  lz_salesload_det dd  WHERE dd.order_id = '$order_packing_id'");
        if ($checkExist->num_rows() > 0) {
            // $update = $this->db->query("UPDATE lz_salesload_det dd SET dd.shippinglabelrate = '$ship_cost' WHERE dd.extendedorderid = '$order_id'");
            $update = $this->db->query("UPDATE lz_salesload_det dd SET dd.shippinglabelrate = '$ship_cost' WHERE dd.order_id = '$order_packing_id'");
            if ($update == true) {
                return array('status' => true, 'message' => 'Shipping Cost Update Successfully');
            } else {
                return array('status' => false, 'message' => 'Shipping Cost Not Update Successfully');
            }
        } else {
            return array('status' => false, 'message' => 'Order  Id ' . $order_id . ' Does not Exist');
        }

    }

    public function Update_Service_Cost()
    {
        $service_cost = $this->input->post('service_cost');
        $order_id = $this->input->post('order_id');
        $order_packing_id = $this->input->post('order_packing_id');
        $checkExist = $this->db->query("SELECT dd.order_id FROM  lz_salesload_det dd  WHERE dd.order_id = '$order_packing_id'");
        if ($checkExist->num_rows() > 0) {
            // $update = $this->db->query("UPDATE lz_salesload_det dd SET dd.SERVICE_COST = '$service_cost' WHERE dd.extendedorderid = '$order_id'");
            $update = $this->db->query("UPDATE lz_salesload_det dd SET dd.SERVICE_COST = '$service_cost' WHERE dd.order_id = '$order_packing_id'");
            if ($update == true) {
                return array('status' => true, 'message' => 'Service Cost Update Successfully');
            } else {
                return array('status' => false, 'message' => 'Service Cost Not Update Successfully');
            }
        } else {
            return array('status' => false, 'message' => 'Order  Id ' . $order_id . ' Does not Exist');
        }
    }

    public function Get_Packing_Order_DropDown()
    {
        $getPackOrder = $this->db->query("SELECT p.PACKING_ID, p.PACKING_COST,
                        p.packing_length||'x'||p.packing_width ||'x'||
                        p.packing_heigth ||'|'|| p.packing_name||'|'||p.packing_type|| '    cost(' ||
   nvl(p.packing_cost, 0)  || ')'  PACKING_NAME
                        from lz_packing_type_mt p");
        if ($getPackOrder) {
            return array('status' => true, 'data' => $getPackOrder->result_array());
        } else {
            return array('status' => false, 'data' => array());
        }
    }

    public function Get_Selected_Packing()
    {
        $order_id = $this->input->post('order_id');
        $data = $this->db->query("SELECT d.order_packing_dt_id, p.packing_id value, d.PACKING_COST cost,
        p.packing_name||'|'||p.packing_type||'|'||
        p.packing_length||'x'||p.packing_width||'x'||
        p.packing_heigth || '    cost(' ||
   nvl(d.packing_cost, 0)  || ')' label from lj_order_packing_dt d, lz_packing_type_mt p where d.packing_id = p.packing_id
   AND d.order_id ='$order_id'");

        if ($data) {
            return array('status' => true, 'data' => $data->result_array());
        } else {
            return array('status' => false, 'data' => array());
        }
    }

    public function Update_Packing_Cost()
    {
        $packings = $this->input->post('packings');
        $rowData = $this->input->post('rowData');
        $user_id = $this->input->post('user_id');

        // $item_id = $rowData->ITEM_ID;
        $item_id = $rowData['ITEM_ID'];
        $qty = $rowData['QTY'];
        $merchant_id = $rowData['MERCHANT_ID'];
        if (empty($merchant_id)) {
            $merchant_id = $this->input->post('merchId');
        }
        $row_order_id = $rowData['ORDER_PACKING_ID'];
        $order_packing_dt_id = $rowData['ORDER_PACKING_DT_ID'];
        $get_order_ids = $this->db->query("SELECT item_id,

        ORDER_ID,
        QTY
   FROM (SELECT sd.ORDER_ID,

                MAX(sd.quantity) QTY,

                max(ACOUNT_DET.item_id) item_id
           FROM LZ_SALESLOAD_DET SD, /*LJ_ORDER_PACKING_MT M,*/
                LJ_ORDER_PACKING_DT DT,
                (SELECT MM.EBAY_ITEM_ID,
                        max(dd.merchant_id) merchant_id,
                        MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                        MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                        max(mm.item_id) item_id
                   FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                  WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                    AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                  GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
          WHERE sd.order_id in (select d.order_id
                                  from ebay_list_mt       e,
                                       lj_merhcant_acc_dt a,
                                       lz_salesload_det   d
                                 where a.acct_id = e.lz_seller_acct_id
                                   and d.item_id = e.ebay_item_id
                                   and a.merchant_id = $merchant_id
                                 group by d.order_id) /*and sd.order_id = m.order_id(+)*/
            and sd.orderstatus = 'Completed'
            and sd.return_id is null
            and sd.order_id = dt.order_id(+)
            AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
            AND sd.INVOICE_ID IS NULL
          GROUP BY sd.ORDER_ID)
         where item_id = '$item_id'
          and QTY = '$qty'
       --   and ORDER_ID = '$row_order_id'

  order by ORDER_ID desc
  ");
        if ($get_order_ids->num_rows() > 0) {
            $get_order_ids = $get_order_ids->result_array();
            foreach ($get_order_ids as $order_data) {
                // $order_id = $order_data['ORDER_ID'];
                $ORDER_ID = $order_data['ORDER_ID'];
                $check_packing = $this->db->query("SELECT pd.order_packing_id from lj_order_packing_dt pd where pd.order_id = '$ORDER_ID'")->result_array();
                if (empty($check_packing)) {
                    foreach ($packings as $packing_data) {
                        $packing_cost = $packing_data['cost'];
                        $packing_id = $packing_data['value'];

                        $this->db->query("INSERT INTO lj_order_packing_dt(ORDER_PACKING_DT_ID, ORDER_ID, PACKING_ID, PACKING_COST,ENTERED_DATE, ENTERED_BY) VALUES (get_single_primary_key('lj_order_packing_dt','ORDER_PACKING_DT_ID'),
                   '$ORDER_ID', '$packing_id', '$packing_cost',sysdate, '$user_id')");

                    }
// return array('status' => false, 'message' => 'packing Already Inserted');
                } else {
                    $ORDER_ID = $order_data['ORDER_ID'];
                    if ($ORDER_ID == $row_order_id) {
                        foreach ($packings as $packing_data) {
                            $packing_cost = $packing_data['cost'];
                            $packing_id = $packing_data['value'];
                            $this->db->query("INSERT INTO lj_order_packing_dt(ORDER_PACKING_DT_ID, ORDER_ID, PACKING_ID, PACKING_COST,ENTERED_DATE, ENTERED_BY) VALUES (get_single_primary_key('lj_order_packing_dt','ORDER_PACKING_DT_ID'),
                            '$ORDER_ID', '$packing_id', '$packing_cost',sysdate, '$user_id')");

                            // $this->db->query("UPDATE lj_order_packing_dt SET PACKING_ID = '$packing_id', PACKING_COST = '$packing_cost', ENTERED_DATE = sysdate, ENTERED_BY = '$user_id' WHERE ORDER_ID = '$ORDER_ID' AND ORDER_PACKING_DT_ID = '$order_packing_dt_id' ");
                        }
                    }
                    // $this->db->query("DELETE FROM lj_order_packing_dt WHERE ORDER_PACKING_DT_ID = '$order_packing_dt_id'");

                }
            }
            return array('status' => true, 'message' => 'packing Cost Update');
        } else {
            return array('status' => false, 'message' => 'packing Cost Not Update');
        }
    }

    public function Delete_Packing()
    {
        $packing = $this->input->post('packing');
        $order_packing_dt_id = $packing['ORDER_PACKING_DT_ID'];
        $delete = $this->db->query("DELETE FROM lj_order_packing_dt WHERE ORDER_PACKING_DT_ID = $order_packing_dt_id");
        if ($delete == true) {
            return array('status' => true, 'message' => 'Packing Delete');
        } else {
            return array('status' => false, 'message' => 'Packing Not Delete');
        }
    }

    public function Get_Invoice_Detail()
    {
        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");

        $data = $this->db->query("SELECT mt.invoice_id,
        'INV-' || LPAD(NVL(mt.invoice_no, 0), 6, '0') INV_CODE,
        mer.buisness_name BUSNIESS_NAME,
        sc.service_desc SERVICE_DESC,
        dt.from_date FROM_DATE,
        dt.to_date TO_DATE,
        nvl(dt.dis_amount,0) DISCOUNT_AMOUNT,
        nvl(dt.dis_amount_perc,0) DISCOUNT_PER,
        nvl(dt.total_charges,0) - nvl(dt.dis_amount,0)    TOTAL_CHARGE,
        null DISCOUNTED_AMOUNT,
        sr.ser_rate_id SER_RATE_ID
        from lj_invoice_mt mt,
        lj_invoice_dt dt,
        lj_service_rate sr,
        lj_services sc,
        lz_merchant_mt mer
        where mt.invoice_id = dt.invoice_id
        and dt.service_rate_id = sr.ser_rate_id(+)
        and sr.service_id = sc.service_id
        and mt.merchant_id = mer.merchant_id
        and mt.merchant_id = '$merchId'
            and mt.INV_FROM_DATE between
            TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
            TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')
        order by dt.INV_DT_ID asc")->result_array();

        if (count($data) > 0) {
            return array('status' => true, 'data' => $data);

        } else {
            return array('status' => false, 'data' => array(), 'message' => 'No Invoice Details');
        }
    }

    public function Delete_Invoice_Detail()
    {
        $ser_rate_id = $this->input->post('ser_rate_id');
        $invoice_id = $this->input->post('invoice_id');
        $delete = $this->db->query("CALL pro_delete_monthly_inovice($invoice_id, $ser_rate_id)");
        if ($delete) {
            return array("status" => true, 'message' => 'Invoice Delete Successfully');
        } else {
            return array("status" => false, 'message' => 'Invoice Not Delete Successfully');
        }

    }

    public function Save_Invoice_Detail()
    {
        $data = $this->input->post('data');
        $user_id = $this->input->post('user_id');
        foreach ($data as $invoice_detail) {
            $invoice_id = $invoice_detail['INVOICE_ID'];
            $ser_rate_id = $invoice_detail['SER_RATE_ID'];
            $dis_amount = $invoice_detail['DISCOUNT_AMOUNT'];
            $dis_per = $invoice_detail['DISCOUNT_PER'];
            $update = $this->db->query("UPDATE LJ_INVOICE_DT SET DIS_AMOUNT = '$dis_amount', DIS_AMOUNT_PERC = '$dis_per' WHERE INVOICE_ID ='$invoice_id' AND SERVICE_RATE_ID ='$ser_rate_id'");
        }
        if ($update == true) {
            return array('status' => true, 'message' => 'Record Updated successfully');
        } else {
            return array('status' => false, 'message' => 'Record Not Updated successfully');
        }
    }
    public function Get_Merchant_Inventory_Prep()
    {
        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $invoice_id = $this->input->post('invoice_id');
        $qr = "SELECT lg.BARCODE_NO,
        decode(BB.EBAY_ITEM_ID, null, 'Not Listed', BB.EBAY_ITEM_ID) EBAY_ITEM_ID,
        decode(S.ITEM_TITLE, null, 'Not Posted', S.ITEM_TITLE) ITEM_TITLE,
        nvl(S.EBAY_PRICE, 0) EBAY_PRICE,
        CM.COND_NAME DEFAULT_COND,
        /*SR.SERVICE_ID,*/
        TO_CHAR(TRUNC(((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60) / 60) -
                24 *
                (TRUNC((((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60) / 60) / 24))) ||
        ' H:' ||
        TO_CHAR(TRUNC((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60) -
                60 *
                (TRUNC(((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60) / 60))) ||
        ' M: ' ||
        TO_CHAR(TRUNC(86400 * (LG.STOP_TIME - LG.START_TIME)) -
                60 * (TRUNC((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60))) || ' S' DURATION,
        ROUND((get_rates_pict.rate / 60) / 60 *
              TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 60 * 60),
              2) CHARGES
   FROM LJ_APPOINTMENT_MT MA,
        LJ_APPOINTMENT_DT DA,
        LJ_SERVICE_RATE SR,
        LJ_SERVICES SV,
        LJ_APPOINTMENT_LOG LG,
        LZ_BARCODE_MT BB,
        LZ_ITEM_SEED S,
        LZ_ITEM_COND_MT CM,
        (select ms.merchant_id, ms.ser_rate_id, ms.rate, ms.excess_qty_rate
           from lj_merchant_service ms
          where ms.merchant_id = '$merchId'
            and ms.ser_rate_id = 3
            and rownum <= 1) get_rates_pict
  WHERE MA.APPOINTMENT_ID = DA.APPOINTMENT_ID
    AND DA.APPOINTMENT_DT_ID = LG.APPOINTMENT_DT_ID(+)
    AND DA.SERVICE_ID = SV.SERVICE_ID
    AND SV.SERVICE_ID = SR.SERVICE_ID
    and sr.ser_rate_id = get_rates_pict.ser_rate_id(+)
    AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+)
    AND BB.ITEM_ID = S.ITEM_ID(+)
    AND BB.CONDITION_ID = S.DEFAULT_COND(+)
    AND LG.BARCODE_NO = BB.BARCODE_NO(+)
    AND S.DEFAULT_COND = CM.ID(+)
    and lg.barcode_no is not null
    AND MA.MERCHANT_ID = '$merchId'";
        if (!empty($invoice_id)) {
            $qr .= " AND LG.INV_ID = '$invoice_id' ";
        } else {
            $qr .= " AND LG.INV_ID IS NULL";
            $qr .= " AND ma.CREATED_DATE between
            TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
            TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        }

        $qr .= " ORDER BY LG.APPOINTMENT_LOG_ID ASC";
        $data = $this->db->query($qr)->result_array();
        if (count($data) > 0) {
            return array('status' => true, 'data' => $data);
        } else {
            return array('status' => false, 'data' => array());
        }

    }

    public function Get_Merchant_Services()
    {
        $mer_id = $_GET['merId'];

        $data = $this->db->query("SELECT max(m.buisness_name) merch_name,
        max(se.service_desc) || ' (' ||
        decode(max(r.service_type),
        '1',
        'per barcde',
        '2',
        'hourly',
        '3',
        'per_order',
        '4',
        'per_bin',
        '5',
        'per_pallet',
        '6',
        'per_cubic_feet')||')' service_desc,
        max(s.ser_rate_id) ser_rate_id,

        max(s.rate) rate
        FROM lj_merchant_service s,
        lz_merchant_mt m,
        lj_service_rate r,
        lj_services se
        where s.merchant_id = m.merchant_id
        and s.ser_rate_id = r.ser_rate_id
        and r.service_id = se.service_id
        and s.ser_rate_id in (10,9,11)
        and s.merchant_id = '$mer_id'
        group by s.merchant_id, s.ser_rate_id
        order by s.merchant_id asc")->result_array();
        if (count($data) > 0) {
            return array('status' => true, 'data' => $data);
        } else {
            return array('status' => false, 'data' => array());
        }
    }

    public function Save_Invoice_Services()
    {
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $startTime = $this->input->post('startTime');
        $endTime = $this->input->post('endTime');
        $merId = $this->input->post('merId');
        $service_id = $this->input->post('service_id');
        if (!empty($service_id)) {
            $service_id = $service_id['value'];
        }
        $lot_id = $this->input->post('lot_id');
        if (!empty($lot_id)) {
            $lot_id = $lot_id['value'];
        }
        $per_hour_rate = $this->input->post('per_hour_rate');
        $remarks = $this->input->post('remarks');
        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = trim(str_replace(array("'"), "''", $remarks));
        $total_charge = $this->input->post('total_charge');
        $user_id = $this->input->post('user_id');
        $duration = $this->input->post('time_diff');
        $manualDuration = $this->input->post('serviceTime');
        if ($manualDuration == '00:00:00') {
            $manualDuration = '';
        }
        $max_query = $this->db->query("SELECT get_single_primary_key('lj_service_mt','LJ_SERVICE_MT_ID') LJ_SERVICE_MT_ID FROM DUAL");
        $rs = $max_query->result_array();
        $lj_service_mt_id = $rs[0]['LJ_SERVICE_MT_ID'];

        $insert_mt = $this->db->query("INSERT INTO lj_service_mt (LJ_SERVICE_MT_ID, MERCHANT_ID, CREATED_DATE, REMARKS, FROM_DATE, TO_DATE) VALUES ('$lj_service_mt_id', '$merId', sysdate, '$remarks', TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS'))");
        if ($insert_mt) {
            $insert_dt = $this->db->query("INSERT INTO lj_service_dt (LJ_SERVICE_DT_ID, LJ_SERVICE_MT_ID, SERVICE_RATE_ID, QUANTITY, RATE, DURATION, START_TIME, STOP_TIME, CREATED_AT, TOTAL_CHARGES, CREATED_BY, LOT_ID) VALUES (get_single_primary_key('lj_service_dt','LJ_SERVICE_DT_ID'), '$lj_service_mt_id', '$service_id', null, ' $per_hour_rate', '$manualDuration',TO_DATE('$startTime', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('$endTime', 'YYYY-MM-DD HH24:MI:SS'), sysdate, $total_charge, '$user_id', '$lot_id' )");
            if ($insert_dt) {
                return array('status' => true, 'message' => 'Reacord Updated successfully');
            } else {
                return array('status' => false, 'message' => 'Reacord Not Updated successfully DT');
            }
        } else {
            return array('status' => false, 'message' => 'Reacord Not Updated successfully MT');
        }
    }
    public function Delete_Invoice_Service()
    {
        $lj_service_dt_id = $this->input->post('lj_service_dt_id');
        $select_mt_id = $this->db->query("SELECT LJ_SERVICE_MT_ID FROM lj_service_dt WHERE LJ_SERVICE_DT_ID = '$lj_service_dt_id'")->result_array();
        if (count($select_mt_id) > 0) {
            $select_mt_id = $select_mt_id[0]['LJ_SERVICE_MT_ID'];
            $delete_dt = $this->db->query("DELETE FROM lj_service_dt WHERE LJ_SERVICE_DT_ID = '$lj_service_dt_id'");
            $delete_mt = $this->db->query("DELETE FROM lj_service_mt WHERE LJ_SERVICE_MT_ID = '$select_mt_id'");
            return array('status' => true, 'message' => 'Record deleted successfully');
        } else {
            return array('status' => false, 'message' => 'Record deleted successfully Please Reload');
        }
    }
    public function Get_Invoice_Other_Services_Detail()
    {
        $service_rate_id = $this->input->post('service_rate_id');
        $mer_id = $this->input->post('merId');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $invoice_id = $this->input->post('invoice_id');
        $data = "SELECT /*s.merchant_id,
        s.created_date,*/
        d.lj_service_dt_id,
        sc.service_desc,
        decode(sr.service_type,
        '1',
        'per barcde',
        '2',
        'hourly',
        '3',
        'per_order',
        '4',
        'per_bin',
        '5',
        'per_pallet',
        '6',
        'per_cubic_feet') ser_type,
        nvl(d.rate, 0) PER_HOUR_RATE,
        d.service_rate_id,
        /*d.lj_service_dt_id,*/
        to_char(d.start_time, 'DD/MM/YYYY HH24:MI:SS') START_TIME,
        to_char( d.stop_time, 'DD/MM/YYYY HH24:MI:SS') STOP_TIME,
        d.duration,
        d.total_charges,
        s.remarks,
        D.INVOICE_ID
        from lj_service_mt s, lj_service_dt d, lj_services sc, lj_service_rate sr
        where s.lj_service_mt_id = d.lj_service_mt_id
        and d.service_rate_id = sr.ser_rate_id
        and sr.service_id = sc.service_id
        AND s.MERCHANT_ID = '$mer_id'
        and d.service_rate_id = '$service_rate_id'
        ";
        if (!empty($invoice_id)) {
            $data .= " AND D.INVOICE_ID = '$invoice_id' ";
        } else {
            $data .= " AND d.invoice_id is null";
            $data .= " AND s.TO_DATE between
            TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
            TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') ";
        }

        $data = $this->db->query($data);
        if ($data->num_rows() > 0) {
            return array('status' => true, 'data' => $data->result_array());
        } else {
            return array('status' => false, 'data' => array());
        }
    }
    public function get_pictures($barcodes)
    {
        $uri = [];
        $base64 = [];

        // $barocde_no = $this->input->post('barocde_no');
        foreach ($barcodes as $barco) {
            $bar = explode(',', $barco['BARCODE_NO']);
            $bar_val = $bar[0];
            $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

            if (count($qry) >= 1) {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $barcode = $qry[0]["FOLDER_NAME"];
                $dir = $master_path . $barcode . "/";
                // var_dump($dir);
                // exit;

            } else {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar_val' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();
                if (count($get_params) === 0) {
                    $get_params = $this->db->query("SELECT MT.UPC F_UPC, MT.MPN F_MPN, C.COND_NAME, DT.BARCODE_NO
                                      FROM LZ_PURCH_ITEM_MT MT, LZ_PURCH_ITEM_DT DT, LZ_ITEM_COND_MT C
                                     WHERE MT.PURCH_MT_ID = DT.PURCH_MT_ID
                                       AND C.ID = MT.CONDITION
                                       AND DT.BARCODE_NO = '$bar_val'")->result_array();
                }
                //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                $cond = @$get_params[0]['COND_NAME'];

                $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);
            // var_dump($dir);
            // exit;

            //var_dump(is_dir($dir));exit;
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            if (is_dir($dir)) {
                // var_dump($dir);exit;
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
                if (!empty($images)) {
                    $i = 0;
                    $count = 0;
                    foreach ($images as $image) {

                        // Get Pic Date of Folder
                        $now = date("Y-m-d"); // or your date as well
                        $fileDate = date("Y-m-d", filemtime($image));
                        $date1 = date_create($now);
                        $date2 = date_create($fileDate);
                        $diff = date_diff($date1, $date2);
                        $diff = $diff->format("%a");
                        // echo $diff;

                        $pathinfo = pathinfo($image);
                        $imagePath = explode('/', $image, 4);
                        // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $image_path = $this->convert_text(@$imagePath[3]);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $image_path);
                        // $uri[$bar_val][$i] = $base_url . $withoutMasterPartUri;
                        $uri[$bar_val]['image'][$i] = "/" . $withoutMasterPartUri;
                        $count = $count + 1;
                        // $uri[$bar_val][$i + 1] = $bar_val;
                        // $uri[$bar_val][$i + 2] = $i + 1;
                        $i++;
                    }
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = $count;
                    $uri[$bar_val][3] = $diff;
                } else {
                    // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                    // $image = $this->convert_text(@$imagePath);
                    $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = 0;
                    $uri[$bar_val][3] = 0;
                }

            } else {
                // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                // $image = $this->convert_text(@$imagePath);
                $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar_val][1] = $bar_val;
                $uri[$bar_val][2] = 0;
                $uri[$bar_val][3] = 0;
            }

            //var_dump($dekitted_pics);exit;

        }
        // var_dump($uri);
        // exit;
        return array('uri' => $uri);
    }
    public function Get_Merchant_Pos_Invoice()
    {
        $mer_id = $this->input->post('merId');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $invoice_id = $this->input->post('invoice_id');
        $data = "SELECT DECODE(pm.pay_mode, 'R', 'CARD', 'C', 'CASH') PAY_MODE,
        pm.paid_status,
        TO_CHAR(pm.entered_date_time, 'dd/mm/YY HH24:MI:SS') CREATED_DATE,
        dm.barcode_id BARCODE_NO,
        dm.*,
round(round(nvl(dm.price, 0) +
(nvl(dm.price, 0) / 100) * nvl(dm.sales_tax_perc, 0) -
nvl(dm.disc_amt, 0),
2) * (0.05),
2) CHARGES,
(nvl(dm.price, 0)+
                 ((nvl(dm.price, 0)) / 100) * (nvl(dm.sales_tax_perc, 0)) -
                 (nvl(dm.disc_amt, 0))) NET_AMOUNT
        from lz_pos_mt pm, lz_pos_det dm
        where pm.lz_pos_mt_id = dm.lz_pos_mt_id
        and dm.barcode_id in
        (select d.barcode_no
        from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
        where m.mt_id = d.mt_id
        and m.merchant_id = '$mer_id')
        -- AND dm.lz_pos_mt_id = '90'
        AND pm.deleted_by IS NULL
        AND pm.return_by IS NULL
        ";
        if (!empty($invoice_id)) {
            $data .= " AND dm.invoice_id = '$invoice_id'";
        } else {
            $data .= " AND dm.invoice_id is null AND pm.entered_date_time between
            TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
            TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        }

        $data .= " order by dm.barcode_id asc";
        $result = $this->db->query($data);
        if ($result->num_rows() > 0) {
            $images = $this->get_pictures($result->result_array());
            return array('status' => true, 'data' => $result->result_array(), 'images' => $images['uri']);
        } else {
            return array('status' => false, 'data' => array(), 'images' => array());
        }
    }
}
