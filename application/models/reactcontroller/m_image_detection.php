
<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Single Entry Model
 */
/***
 *
 * Stripe Payment
 */
require_once './vendor/autoload.php';
\Stripe\Stripe::setApiKey('sk_test_GBoSFpvdORwnqddSO9bA6diO00ifrAdWVC');
class m_image_detection extends CI_Model
{
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function Save_Image_Detect()
    {
        $upc = $this->input->post('upc');
        $upc = trim(str_replace("  ", ' ', $upc));
        $upc = trim(str_replace(array("'"), "''", $upc));
        $mpn = $this->input->post('mpn');
        $mpn = trim(str_replace("  ", ' ', $mpn));
        $mpn = trim(str_replace(array("'"), "''", $mpn));
        $title = $this->input->post('title');
        $title = trim(str_replace("  ", ' ', $title));
        $title = trim(str_replace(array("'"), "''", $title));
        $category_id = $this->input->post('category_id');
        $category_id = trim(str_replace("  ", ' ', $category_id));
        $category_id = trim(str_replace(array("'"), "''", $category_id));
        $brand = $this->input->post('brand');
        $brand = trim(str_replace("  ", ' ', $brand));
        $brand = trim(str_replace(array("'"), "''", $brand));
        if (empty($category_id)) {
            return array('status' => false, 'message' => "Category Id Is Required");
        }
        $category_name = $this->input->post('category_name');
        $category_name = trim(str_replace("  ", ' ', $category_name));
        $category_name = trim(str_replace(":", "\\", $category_name));
        $category_name = trim(str_replace(array("'"), "''", $category_name));
        $ebay_id = $this->input->post('ebay_id');
        $ebay_id = trim(str_replace("  ", ' ', $ebay_id));
        $ebay_id = trim(str_replace(array("'"), "''", $ebay_id));
        $user_id = $this->input->post('user_id');
        $images = $this->input->post('images');
        // exit;
        $get_data = $this->db->query("SELECT * FROM LZ_ITEMTRAIN_MT WHERE UPC = '$upc' OR  MPN = '$mpn' AND ROWNUM = 1")->result_array();
        if (count($get_data) > 0) {
            $train_mt_id = $get_data[0]['TRAIN_MT_ID'];
            $this->db->query("UPDATE LZ_ITEMTRAIN_MT SET STATUS = 0 WHERE TRAIN_MT_ID = '$train_mt_id'");
            foreach ($images as $image) {
                if (isset($image['isSelected']) && $image['isSelected'] == 'true') {
                    $img = $image['src'];
                    $train_dt_id = $this->db->query("SELECT get_single_primary_key('LZ_ITEMTRAIN_DT', 'TRAIN_DT_ID') TRAIN_DT_ID  FROM DUAL")->result_array();
                    $train_dt_id = $train_dt_id[0]['TRAIN_DT_ID'];
                    $this->db->query("INSERT INTO LZ_ITEMTRAIN_DT(TRAIN_DT_ID, TRAIN_MT_ID, IMG_URL, CREATED_AT, CREATED_BY) VALUES ('$train_dt_id', '$train_mt_id', '$img', sysdate, '$user_id')");
                }
            }
            return array('status' => true, 'message' => "Record Inserted Successfully");
        } else {

            $result = $this->db->query("SELECT fun_cat_tree($category_id) root_path from dual")->result_array();
            $category_tree = $result[0]['ROOT_PATH'];

            $train_mt_id = $this->db->query("SELECT get_single_primary_key('LZ_ITEMTRAIN_MT', 'TRAIN_MT_ID') TRAIN_MT_ID  FROM DUAL")->result_array();
            $train_mt_id = $train_mt_id[0]['TRAIN_MT_ID'];
            $insert_mt = $this->db->query("INSERT INTO LZ_ITEMTRAIN_MT(TRAIN_MT_ID, TITLE, UPC, MPN, CATEGORY_ID, CATEGORY_NAME, CATEGORY_TREE, STATUS, INSERTED_DATE, INSERTED_BY, EBAY_ID, BRAND) VALUES ('$train_mt_id', '$title', '$upc', '$mpn', '$category_id', '$category_name', '$category_tree', 0, sysdate, '$user_id', '$ebay_id', '$brand')");
            if ($insert_mt == true) {
                $call_proc = $this->db->query("call pro_create_item($train_mt_id)");
                if ($call_proc) {
                    foreach ($images as $image) {
                        if (isset($image['isSelected']) && $image['isSelected'] == 'true') {
                            $img = $image['src'];
                            $train_dt_id = $this->db->query("SELECT get_single_primary_key('LZ_ITEMTRAIN_DT', 'TRAIN_DT_ID') TRAIN_DT_ID  FROM DUAL")->result_array();
                            $train_dt_id = $train_dt_id[0]['TRAIN_DT_ID'];
                            $this->db->query("INSERT INTO LZ_ITEMTRAIN_DT(TRAIN_DT_ID, TRAIN_MT_ID, IMG_URL, CREATED_AT, CREATED_BY) VALUES ('$train_dt_id', '$train_mt_id', '$img', sysdate, '$user_id')");
                        }
                    }
                    return array('status' => true, 'message' => "Record Inserted Successfully");
                } else {
                    return array('status' => false, 'message' => "DB Functoin Is Not Execute");
                }

            } else {
                return array('status' => false, 'message' => "Record Not Inserted Successfully");
            }

        }

    }
}
?>