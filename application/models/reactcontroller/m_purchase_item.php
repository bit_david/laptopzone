<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Single Entry Model
 */
/***
 *
 * Stripe Payment
 */
require_once './vendor/autoload.php';
\Stripe\Stripe::setApiKey('sk_test_GBoSFpvdORwnqddSO9bA6diO00ifrAdWVC');
class m_purchase_item extends CI_Model
{
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function convert_text($text)
    {

        $t = $text;
        // '\'' => '%27',
        // '~' => '%7E',
        // '_' => '%5F',
        // '/' => '%2F',
        // '\\' => '%5C',
        // '.' => '%2E',
        // '%' => '%25',
        // ':' => '%3A',
        // ' ' => '%20',
        $specChars = array(
            '!' => '%21', '"' => '%22',
            '#' => '%23', '$' => '%24',
            '&' => '%26', '(' => '%28',
            ')' => '%29', '*' => '%2A', '+' => '%2B',
            ',' => '%2C', '-' => '%2D',
            ';' => '%3B',
            '<' => '%3C', '=' => '%3D', '>' => '%3E',
            '?' => '%3F', '@' => '%40', '[' => '%5B',
            ']' => '%5D', '^' => '%5E',
            '`' => '%60', '{' => '%7B',
            '|' => '%7C', '}' => '%7D',
            ',' => '%E2%80%9A',
        );

        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }

        return $t;
    }
    public function Get_Purchase_Items()
    {
        $data = $this->db->query("SELECT distinct (SELECT LISTAGG(d.barcode_no, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
        FROM lz_purch_item_dt d
       where d.purch_mt_id = pit.purch_mt_id) barcode_no,
     case
       when b.barcode_no is null then
        0
       else
        1
     end recoginze,
     (select LISTAGG(PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY pit.purch_mt_id)
        from (SELECT PT.PORTAL_DESC
                FROM LZ_PORTAL_MT         PT,
                     LZ_BARCODE_PORTAL_DT PD,
                     lz_purch_item_dt     d
               where d.purch_mt_id = pit.purch_mt_id
                 AND PT.PORTAL_ID = PD.PORTAL_ID
                 AND PT.ACTIVE_YN = 1
                 AND PD.BARCODE_NO IN (d.barcode_no)
               group by PT.PORTAL_DESC)) PORTAL_DESC,
     (SELECT LISTAGG(d.rowId, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
        FROM lz_purch_item_dt d
       where d.purch_mt_id = pit.purch_mt_id) purch_dt_rowId,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID = pit.inserted_by) PURCHASE_INSERTED_BY,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID = pit.REMARKS_BY) PURCHASE_REMARKS_BY,
     b.lz_manifest_id,
     b.item_id,
     pit.purch_mt_id,
     pit.purchase_no,
     pit.mpn,
     pit.upc,
     pit.imei1,
     pit.imei2,
     pit.cost,
     pit.qty,
     pit.remarks,
     pit.inserted_by,
     pit.update_date,
     TO_CHAR(pit.inserted_date, 'MM/DD/YY HH24:MI:SS') inserted_date,
     pit.condition,
     pit.HISTORY_STATUS,
     pit.LISTER_REMARKS,
     b.ebay_item_id,
     list_price.list_price,
     cm.cond_name,
     (list_price.list_price * pit.qty) total_list_price,
     (pit.cost * pit.qty) total_cost,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID =  dd.ASSIGN_BY) ASSIGN_BY,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID =  dd.ASSIGN_TO) ASSIGN_TO,
     TO_CHAR (dd.ASSIGN_DATE, 'MM/DD/YY HH24:MI:SS') ASSIGN_DATE,
     decode(pit.color,
            '1',
            'red',
            '2',
            'green',
            '3',
            'orange',
            '4',
            'yellow',
            '5',
            'black',
            'white') color,
            (SELECT LISTAGG(pt.PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY d.portal_id) SELECT_PORTAL
        FROM lz_item_list_mt d, lz_portal_mt pt
       where d.barcode_no = dd.barcode_no AND pt.portal_id = d.portal_id) SELECTED_PORTAL,
       bi.BIN_TYPE || '-' || bi.BIN_NO   BIN_NAME
       FROM lz_merchant_barcode_dt d,
lz_purch_item_dt dd,
lz_barcode_mt b,
lz_item_cond_mt cm,
lz_purch_item_mt pit,
lz_item_list_mt lb,
BIN_MT bi,
(select nvl(r.list_price, 0) list_price, r.ebay_item_id
from ebay_list_mt r,
    (select e.ebay_item_id, max(e.list_id) list_id
       from ebay_list_mt e
      group by e.ebay_item_id) ee
where r.list_id = ee.list_id) list_price
WHERE d.barcode_no = dd.barcode_no
AND dd.barcode_no = b.barcode_no(+)
AND d.barcode_no = b.barcode_no(+)
and d.barcode_no = lb.barcode_no(+)
AND pit.purch_mt_id = dd.purch_mt_id
and list_price.ebay_item_id(+) = b.ebay_item_id
AND cm.id = pit.condition
AND b.lz_pos_mt_id is NULL
AND b.item_adj_det_id_for_out IS NULL
--and b.ebay_item_id IS NULL
and (b.hold_status is null or b.hold_status = 0)
and d.bin_id = bi.bin_id(+)
        ORDER BY pit.update_date DESC")->result_array();

        if (count($data) > 0) {
            return array("data" => $data, 'status' => true);
        } else {
            return array("message" => "No Record Found", 'data' => array(), 'status' => false);
        }
    }

    public function Get_Purchase_Images()
    {
        $query = $this->db->query("SELECT PIC.PURCH_MT_ID, PIC.PIC_PATCH from lz_purch_item_pic PIC")->result_array();
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        if (count($query) > 0) {
            // $j = 0;
            // $uri = [];
            foreach ($query as $key => $image) {
                $img = $image['PIC_PATCH'];
                // $purch_mt_id = $image['PURCH_MT_ID'];
                $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $img);
                $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                $query[$key]['PIC_PATCH'] = $base_url . $withoutMasterPartUri;
                // $uri[$purch_mt_id][$j] = $base_url . $withoutMasterPartUri;
            }
            return array('status' => true, 'data' => $query);
        } else {
            return array('status' => false, 'data' => array(), 'message' => 'No Images Found');
        }
    }

    public function Date_Filter_Purch_Item()
    {
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $user_id = $this->input->post('user_id');
        $data = "SELECT distinct  d.tag_id , d.tag_remarks, tm.tag_desc, (SELECT LISTAGG(d.barcode_no, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
        FROM lz_purch_item_dt d
       where d.purch_mt_id = pit.purch_mt_id) barcode_no,
     case
       when b.barcode_no is null then
        0
       else
        1
     end recoginze,
     (select LISTAGG(PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY pit.purch_mt_id)
        from (SELECT PT.PORTAL_DESC
                FROM LZ_PORTAL_MT         PT,
                     LZ_BARCODE_PORTAL_DT PD,
                     lz_purch_item_dt     d
               where d.purch_mt_id = pit.purch_mt_id
                 AND PT.PORTAL_ID = PD.PORTAL_ID
                 AND PT.ACTIVE_YN = 1
                 AND PD.BARCODE_NO IN (d.barcode_no)
               group by PT.PORTAL_DESC)) PORTAL_DESC,
     (SELECT LISTAGG(d.rowId, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
        FROM lz_purch_item_dt d
       where d.purch_mt_id = pit.purch_mt_id) purch_dt_rowId,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID = pit.inserted_by) PURCHASE_INSERTED_BY,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID = pit.REMARKS_BY) PURCHASE_REMARKS_BY,
     b.lz_manifest_id,
     b.item_id,
     pit.purch_mt_id,
     pit.purchase_no,
     pit.mpn,
     pit.upc,
     pit.imei1,
     pit.imei2,
     pit.cost,
     pit.qty,
     pit.remarks,
     pit.inserted_by,
     pit.update_date,
     TO_CHAR(pit.inserted_date, 'MM/DD/YY HH24:MI:SS') inserted_date,
     pit.condition,
     pit.HISTORY_STATUS,
     pit.LISTER_REMARKS,
     b.ebay_item_id,
     list_price.list_price,
     cm.cond_name,
     (list_price.list_price * pit.qty) total_list_price,
     (pit.cost * pit.qty) total_cost,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID =  dd.ASSIGN_BY) ASSIGN_BY,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID =  dd.ASSIGN_TO) ASSIGN_TO,
     TO_CHAR (dd.ASSIGN_DATE, 'MM/DD/YY HH24:MI:SS') ASSIGN_DATE,
     decode(pit.color,
            '1',
            'red',
            '2',
            'green',
            '3',
            'orange',
            '4',
            'yellow',
            '5',
            'black',
            'white') color,
            (SELECT LISTAGG(pt.PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY d.portal_id) SELECT_PORTAL
        FROM lz_item_list_mt d, lz_portal_mt pt
       where d.barcode_no = dd.barcode_no AND pt.portal_id = d.portal_id) SELECTED_PORTAL,
       bi.BIN_TYPE || '-' || bi.BIN_NO   BIN_NAME
FROM lz_merchant_barcode_dt d,
lz_purch_item_dt dd,
lz_barcode_mt b,
lz_item_cond_mt cm,
lz_purch_item_mt pit,
lz_item_list_mt lb,
BIN_MT bi,
lj_barcode_tag_mt tm,
(select nvl(r.list_price, 0) list_price, r.ebay_item_id
from ebay_list_mt r,
    (select e.ebay_item_id, max(e.list_id) list_id
       from ebay_list_mt e
      group by e.ebay_item_id) ee
where r.list_id = ee.list_id) list_price
WHERE d.barcode_no = dd.barcode_no
AND dd.barcode_no = b.barcode_no(+)
AND d.barcode_no = b.barcode_no(+)
and d.barcode_no = lb.barcode_no(+)
AND pit.purch_mt_id = dd.purch_mt_id
and list_price.ebay_item_id(+) = b.ebay_item_id
AND cm.id = pit.condition
AND b.lz_pos_mt_id is NULL
AND b.item_adj_det_id_for_out IS NULL
and (b.discard is null or b.discard =0)
and b.not_found_by is null
and b.order_dt_id is null
and b.extendedorderid is null
and b.order_id is null
and b.sale_record_no is null
and (b.condition_id >0 or b.condition_id is null)
and b.item_adj_det_id_for_in is null
and b.lz_part_issue_mt_id is null
and b.returned_id is null
and b.returned_id_for_in is null
--and b.ebay_item_id IS NULL
and (b.hold_status is null or b.hold_status = 0)
and d.bin_id = bi.bin_id(+)
and d.tag_id = tm.tag_id(+)";
        $userLocaltion = $this->db->query("select e.location from employee_mt e where e.employee_id ='$user_id'")->result_array();
        if ($userLocaltion[0]['LOCATION'] == 'US') {
            $data .= "AND pit.update_date BETWEEN  TO_DATE(' $startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
TO_DATE(' $endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') ORDER BY pit.update_date DESC
";
        } else {
            $data .= "AND pit.inserted_date BETWEEN  TO_DATE(' $startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
    TO_DATE(' $endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') ORDER BY pit.update_date DESC
    ";
        }
        $result = $this->db->query($data)->result_array();
        if (count($result) > 0) {
            return array("data" => $result, 'status' => true);
        } else {
            return array("message" => "No Record Found", 'data' => array(), 'status' => false);
        }
    }

    public function Get_Purchase_Images_DataBase()
    {
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $data = $this->db->query("SELECT PM.PURCH_MT_ID, PIC.PIC_PATCH FROM LZ_PURCH_ITEM_MT PM, LZ_PURCH_ITEM_PIC PIC WHERE PM.PURCH_MT_ID = PIC.PURCH_MT_ID(+) AND PM.inserted_date BETWEEN  TO_DATE(' $startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
        TO_DATE(' $endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') ORDER BY PIC.PURCH_MT_ID DESC")->result_array();
        $images = [];
        foreach ($data as $image) {

            $imageOrginalPath = $image['PIC_PATCH'];
            $purch_mt_id = $image['PURCH_MT_ID'];
            $imagePath = explode('/', $imageOrginalPath, 4);
            // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
            $image_path = $this->convert_text(@$imagePath[3]);
            $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $image_path);
            if (!empty($imageOrginalPath)) {
                $images[$purch_mt_id][0] = '/' . $withoutMasterPartUri;

            } else {
                $images[$purch_mt_id][0] = "/item_pictures/master_pictures/image_not_available.jpg";
            }

        }
        return array('dbImages' => $images);

    }

    public function Get_Db_Barcode_Image()
    {
        $purch_mt_id = $this->input->post('purch_mt_id');
        $data = $this->db->query("SELECT PIC.PIC_PATCH FROM LZ_PURCH_ITEM_PIC PIC WHERE PIC.PURCH_MT_ID = '$purch_mt_id'")->result_array();
        $images = [];
        if (count($data) > 0) {
            $j = 0;
            foreach ($data as $image) {
                $imageOrginalPath = $image['PIC_PATCH'];
                $imagePath = explode('/', $imageOrginalPath, 4);
                // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                $image_path = $this->convert_text(@$imagePath[3]);
                $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $image_path);
                $images[$j] = '/' . $withoutMasterPartUri;
                $j++;
            }
        } else {
            $images[0] = "/item_pictures/master_pictures/image_not_available.jpg";
        }

        return array('dbImages' => $images);

    }

    public function Date_Filter_Purchase_Images()
    {
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $query = $this->db->query("SELECT --b.barcode_no,
        distinct (SELECT LISTAGG(d.barcode_no, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
                    FROM lz_purch_item_dt d
                   where d.purch_mt_id = pit.purch_mt_id) barcode_no,
                 pit.mpn,
                 pit.upc,
                 pit.condition,
                 pit.purch_mt_id,
         pit.update_date
          FROM lz_merchant_barcode_dt d,
               lz_purch_item_dt       dd,
               lz_barcode_mt          b,
               lz_purch_item_mt pit
         WHERE d.barcode_no = dd.barcode_no
           AND b.barcode_no(+) = d.barcode_no
           AND pit.purch_mt_id = dd.purch_mt_id
           AND b.lz_pos_mt_id is NULL
        AND b.item_adj_det_id_for_out IS NULL
        and (b.hold_status is null or  b.hold_status = 0)
           AND pit.update_date BETWEEN  TO_DATE(' $startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
                     TO_DATE(' $endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')  ORDER BY pit.update_date DESC")->result_array();
        $images = $this->get_pictures($query);
        return array('status' => true, 'data' => $images['uri']);
    }

    public function get_pictures($barcodes)
    {
        $uri = [];
        $base64 = [];
        $cond_id = '';
        // $barocde_no = $this->input->post('barocde_no');
        foreach ($barcodes as $barco) {
            $bar = explode(',', $barco['BARCODE_NO']);
            $purchaseUpc = trim($barco['UPC']);
            $purchaseMpn = trim(strtoupper($barco['MPN']));
            $purchaseMpn = trim(str_replace("  ", ' ', $purchaseMpn));
            $purchaseMpn = trim(str_replace(array("'"), "''", $purchaseMpn));

            $bar_val = $bar[0];
            $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

            if (count($qry) >= 1) {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $barcode = $qry[0]["FOLDER_NAME"];
                $dir = $master_path . $barcode . "/";
                $dirThumb = $master_path . $barcode . "/thumb/";
                // var_dump($dir);
                // exit;

            } else {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.ID COND_ID, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar_val' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();
                if (count($get_params) === 0) {
                    $get_params = $this->db->query("SELECT MT.UPC F_UPC, MT.MPN F_MPN, C.ID COND_ID, C.COND_NAME, DT.BARCODE_NO
                                      FROM LZ_PURCH_ITEM_MT MT, LZ_PURCH_ITEM_DT DT, LZ_ITEM_COND_MT C
                                     WHERE MT.PURCH_MT_ID = DT.PURCH_MT_ID
                                       AND C.ID = MT.CONDITION
                                       AND DT.BARCODE_NO = '$bar_val'")->result_array();
                }

                //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                $cond = @$get_params[0]['COND_NAME'];
                $cond_id = @$get_params[0]['COND_ID'];
                $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
                $dirThumb = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/" . "thumb" . "/";
                // var_dump(($dir));
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);
            // var_dump(is_dir($dir));
            // var_dump($purchaseUpc);
            // var_dump($bar_val);
            // exit;
            // if (!is_dir($dir)) {
            //     if (!empty($purchaseUpc)) {
            //         $data = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.CARD_UPC =  '$purchaseUpc' AND CONDITION_ID = '$cond_id'")->result_array();
            //     } else {
            //         $data = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE UPPER(L.CARD_MPN) =  '$purchaseMpn' AND CONDITION_ID = '$cond_id'")->result_array();
            //     }

            //     if (count($data) > 0) {
            //         $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
            //         $path = $path->result_array();
            //         $master_path = $path[0]["MASTER_PATH"];

            //         $barcode = $data[0]["FOLDER_NAME"];
            //         $imageDir = $master_path . $barcode . "/";
            //         // var_dump($imageDir);
            //         if (!is_dir($dir)) {
            //             mkdir($dir, 0755, true);
            //         }
            //         if (!is_dir($dirThumb)) {
            //             mkdir($dirThumb, 0755, true);
            //         }

            //         $images = glob($imageDir . "'*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}'", GLOB_BRACE);
            //         if (!empty($images)) {
            //             foreach ($images as $image) {
            //                 $pathinfo = pathinfo($image);
            //                 $image = $imageDir . $pathinfo['filename'] . '.' . $pathinfo['extension'];
            //                 // var_dump($image);
            //                 $thumb_image = $imageDir . 'thumb/' . $pathinfo['filename'] . '.' . $pathinfo['extension'];
            //                 // var_dump($thumb_image);
            //                 // var_dump($dir . $pathinfo['filename'] . '.' . $pathinfo['extension']);
            //                 copy(@$image, @$dir . @$pathinfo['filename'] . '.' . @$pathinfo['extension']);
            //                 copy(@$thumb_image, @$dirThumb . @$pathinfo['filename'] . '.' . @$pathinfo['extension']);
            //             }
            //             // exit;

            //         }
            //     }

            // }
            //var_dump(is_dir($dir));exit;
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            if (is_dir($dirThumb)) {
                // var_dump($dir);exit;
                $images = glob($dirThumb . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
                if (!empty($images)) {
                    $i = 0;
                    $count = 0;
                    foreach ($images as $image) {

                        // Get Pic Date of Folder
                        $now = date("Y-m-d"); // or your date as well
                        $fileDate = date("Y-m-d", filemtime($image));
                        $date1 = date_create($now);
                        $date2 = date_create($fileDate);
                        $diff = date_diff($date1, $date2);
                        $diff = $diff->format("%a");
                        // echo $diff;

                        $pathinfo = pathinfo($image);
                        $imagePath = explode('/', $image, 4);
                        // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $image_path = $this->convert_text(@$imagePath[3]);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $image_path);
                        // $uri[$bar_val][$i] = $base_url . $withoutMasterPartUri;
                        $uri[$bar_val]['image'][$i] = "/" . $withoutMasterPartUri;
                        $count = $count + 1;
                        // $uri[$bar_val][$i + 1] = $bar_val;
                        // $uri[$bar_val][$i + 2] = $i + 1;
                        $i++;
                    }
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = $count;
                    $uri[$bar_val][3] = $diff;
                } else {
                    // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                    // $image = $this->convert_text(@$imagePath);
                    $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = 0;
                    $uri[$bar_val][3] = 0;
                }

            } else {
                // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                // $image = $this->convert_text(@$imagePath);
                $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar_val][1] = $bar_val;
                $uri[$bar_val][2] = 0;
                $uri[$bar_val][3] = 0;
            }

            //var_dump($dekitted_pics);exit;

        }
        // var_dump($uri);
        // exit;
        return array('uri' => $uri);
    }

    public function Get_Images()
    {
        $uri = [];
        $base64 = [];
        $barcodes = $this->input->post('data');
        $purchaseUpc = '';
        $purchaseMpn = '';
        $cond_id = '';
        // $barocde_no = $this->input->post('barocde_no');
        foreach ($barcodes as $barco) {
            $bar = explode(',', $barco['BARCODE_NO']);
            $bar_val = $bar[0];
            $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

            if (count($qry) >= 1) {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $barcode = $qry[0]["FOLDER_NAME"];
                $dir = $master_path . $barcode . "/";
                $dirThumb = $master_path . $barcode . "/thumb/";
                // var_dump($dir);
                // exit;

            } else {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.ID COND_ID, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar_val' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();
                if (count($get_params) === 0) {
                    $get_params = $this->db->query("SELECT MT.UPC F_UPC, MT.MPN F_MPN, C.ID COND_ID, C.COND_NAME, DT.BARCODE_NO
                                      FROM LZ_PURCH_ITEM_MT MT, LZ_PURCH_ITEM_DT DT, LZ_ITEM_COND_MT C
                                     WHERE MT.PURCH_MT_ID = DT.PURCH_MT_ID
                                       AND C.ID = MT.CONDITION
                                       AND DT.BARCODE_NO = '$bar_val'")->result_array();
                }
                //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                $purchaseUpc = trim(@$upc);
                $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                $purchaseMpn = trim(strtoupper(@$mpn));
                $cond = @$get_params[0]['COND_NAME'];
                $cond_id = @$get_params[0]['COND_ID'];

                $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
                $dirThumb = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/" . "thumb" . "/";
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);
            // var_dump($dir);
            // exit;
            //  Check if UPC or MPN
            // if (!is_dir($dir)) {
            //     if (!empty($purchaseUpc)) {
            //         $data = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.CARD_UPC =  '$purchaseUpc' AND CONDITION_ID = '$cond_id'")->result_array();
            //     } else {
            //         $data = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE UPPER(L.CARD_MPN) =  '$purchaseMpn' AND CONDITION_ID = '$cond_id'")->result_array();
            //     }
            //     if (count($data) > 0) {
            //         $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
            //         $path = $path->result_array();
            //         $master_path = $path[0]["MASTER_PATH"];

            //         $barcode = $data[0]["FOLDER_NAME"];
            //         $imageDir = $master_path . $barcode . "/";
            //         // var_dump($imageDir);
            //         if (!is_dir($dir)) {
            //             mkdir($dir, 0755, true);
            //         }
            //         if (!is_dir($dirThumb)) {
            //             mkdir($dirThumb, 0755, true);
            //         }

            //         $images = glob($imageDir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
            //         if (!empty($images)) {
            //             foreach ($images as $image) {
            //                 $pathinfo = pathinfo($image);
            //                 $image = $imageDir . $pathinfo['filename'] . '.' . $pathinfo['extension'];
            //                 // var_dump($image);
            //                 $thumb_image = $imageDir . 'thumb/' . $pathinfo['filename'] . '.' . $pathinfo['extension'];
            //                 // var_dump($thumb_image);
            //                 // var_dump($dir . $pathinfo['filename'] . '.' . $pathinfo['extension']);
            //                 copy(@$image, @$dir . @$pathinfo['filename'] . '.' . @$pathinfo['extension']);
            //                 copy(@$thumb_image, @$dirThumb . @$pathinfo['filename'] . '.' . @$pathinfo['extension']);
            //             }
            //             // exit;

            //         }
            //     }

            // }
            //var_dump(is_dir($dir));exit;
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            if (is_dir($dir)) {
                // var_dump($dir);exit;
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
                if (!empty($images)) {
                    $i = 0;
                    $count = 0;
                    foreach ($images as $image) {

                        // Get Pic Date of Folder
                        $now = date("Y-m-d"); // or your date as well
                        $fileDate = date("Y-m-d", filemtime($image));
                        $date1 = date_create($now);
                        $date2 = date_create($fileDate);
                        $diff = date_diff($date1, $date2);
                        $diff = $diff->format("%a");

                        $pathinfo = pathinfo($image);
                        $imagePath = explode('/', $image, 4);
                        $image_path = $this->convert_text(@$imagePath[3]);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $image_path);
                        $uri[$bar_val]['image'][$i] = "/" . $withoutMasterPartUri;
                        $count = $count + 1;
                        $i++;
                    }
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = $count;
                    $uri[$bar_val][3] = $diff;
                } else {
                    $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = 0;
                    $uri[$bar_val][3] = 0;
                }

            } else {
                $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar_val][1] = $bar_val;
                $uri[$bar_val][2] = 0;
                $uri[$bar_val][3] = 0;
            }

        }
        // $image = $this->Get_Purchase_Images_DataBase();

        return array('uri' => $uri);
    }

    public function Update_Lister_Remarks()
    {
        $enter_by = $this->input->post('enter_by');
        $lister_remarks = $this->input->post('lister_remarks');
        $lister_remarks = trim(str_replace("  ", ' ', $lister_remarks));
        $lister_remarks = trim(str_replace(array("'"), "''", $lister_remarks));
        $purch_mt_id = $this->input->post('purch_mt_id');
        $update = $this->db->query("UPDATE lz_purch_item_mt SET lister_remarks = '$lister_remarks' , REMARKS_BY = '$enter_by' , REMARKS_DATE = sysdate WHERE PURCH_MT_ID = '$purch_mt_id'");
        if ($update == true) {

            return array('status' => true, 'message' => 'Remarks Updated Successfully');
        } else {
            return array('status' => false, 'message' => 'Remarks Not Updated Successfully');
        }
    }
    public function Print_Purchase_Sticker()
    {
        $barcode = $_GET['barcode'];
        // var_dump(explode(',', $barcode));
        $barcode = explode(',', $barcode);
        $barcode_detail = [];
        $res_des = [];
        if (empty($barcode[1])) {
            $barcode = trim($barcode[0]);
            $data = $this->db->query("SELECT NVL(pim.upc, pim.mpn) UPC_MPN, pim.*,dd.barcode_no
            FROM lz_merchant_barcode_dt d,
                 lz_purch_item_dt       dd,
                 lz_barcode_mt          b,
                 lz_purch_item_mt       pim
           where d.barcode_no = dd.barcode_no
             and b.barcode_no(+) = d.barcode_no
             AND pim.purch_mt_id = dd.purch_mt_id
             /*AND b.lz_pos_mt_id is NULL
         AND b.item_adj_det_id_for_out IS NULL
        and (b.hold_status is null or  b.hold_status = 0)*/
             AND dd.barcode_no = '$barcode'  ORDER BY pim.update_date DESC");
            if ($data->num_rows() > 0) {
                $result = $data->result_array();
                array_push($barcode_detail, $result[0]);
                $res = [];
                if (!empty($result[0]['UPC'])) {
                    $upc = $result[0]['UPC'];
                    $res = $this->db->query("SELECT ITEM_TITLE, UPC_MPN FROM (SELECT ITEM_TITLE, S.F_UPC UPC_MPN FROM LZ_ITEM_SEED S WHERE S.F_UPC = '$upc' ORDER BY S.SEED_ID DESC) WHERE rownum =1")->result_array();
                    $mergeArray = [];
                    foreach ($res as $data) {
                        $data['BARCODE_NO'] = $result[0]['BARCODE_NO'];
                        $mergeArray = $data;
                    }
                    if (count($res) !== 0) {
                        array_push($res_des, $mergeArray);
                    } // $res['UPC_MPN'] = $result[0]['UPC_MPN'];

                }
                if (count($res) === 0) {
                    if (!empty($result[0]['MPN'])) {
                        $mpn = $result[0]['MPN'];
                        $res = $this->db->query("SELECT ITEM_TITLE, UPC_MPN FROM (SELECT ITEM_TITLE, S.F_MPN UPC_MPN FROM LZ_ITEM_SEED S WHERE S.F_MPN = '$mpn' ORDER BY S.SEED_ID DESC) WHERE rownum =1")->result_array();
                        $mergeArray = [];
                        foreach ($res as $data) {
                            $data['BARCODE_NO'] = $result[0]['BARCODE_NO'];
                            $mergeArray = $data;
                        }
                        if (count($res) !== 0) {
                            array_push($res_des, $mergeArray);
                        }
                    }
                }
                if (count($res) === 0) {
                    $res = array();
                    $res_des = [];
                }

                return array('status' => true, 'barcode' => $barcode_detail, 'data' => $res_des);
            } else {
                return array('status' => false, 'barcode' => array(), 'data' => $res_des = array());
            }
        } else {
            $res = [];
            foreach ($barcode as $key => $bar) {
                $barco = trim($bar);
                $data = $this->db->query("SELECT NVL(pim.upc, pim.mpn) UPC_MPN, pim.*,dd.barcode_no
                FROM lz_merchant_barcode_dt d,
                     lz_purch_item_dt       dd,
                     lz_barcode_mt          b,
                     lz_purch_item_mt       pim
               where d.barcode_no = dd.barcode_no
                 and b.barcode_no(+) = d.barcode_no
                 AND pim.purch_mt_id = dd.purch_mt_id
             /*     AND b.lz_pos_mt_id is NULL
         AND b.item_adj_det_id_for_out IS NULL
         and (b.hold_status is null or  b.hold_status = 0)*/
                 AND dd.barcode_no = '$barco'  ORDER BY pim.update_date DESC");

                if ($data->num_rows() > 0) {
                    $result = $data->result_array();
                    array_push($barcode_detail, $result[0]);
                    $res = [];
                    if (!empty($result[0]['UPC'])) {
                        $upc = $result[0]['UPC'];
                        $res = $this->db->query("SELECT ITEM_TITLE, UPC_MPN FROM (SELECT ITEM_TITLE, S.F_UPC UPC_MPN FROM LZ_ITEM_SEED S WHERE S.F_UPC = '$upc' ORDER BY S.SEED_ID DESC) WHERE rownum =1")->result_array();
                        $mergeArray = [];
                        foreach ($res as $data) {
                            $data['BARCODE_NO'] = $result[0]['BARCODE_NO'];
                            $mergeArray = $data;
                        }
                        // var_dump($mergeArray);
                        if (count($res) !== 0) {
                            array_push($res_des, $mergeArray);
                        }
                    }
                    if (count($res) === 0) {
                        if (!empty($result[0]['MPN'])) {
                            $mpn = $result[0]['MPN'];
                            $res = $this->db->query("SELECT ITEM_TITLE, UPC_MPN  FROM (SELECT ITEM_TITLE, S.F_MPN UPC_MPN FROM LZ_ITEM_SEED S WHERE S.F_MPN = '$mpn' ORDER BY S.SEED_ID DESC) WHERE rownum =1")->result_array();
                            $mergeArray = [];
                            foreach ($res as $data) {
                                $data['BARCODE_NO'] = $result[0]['BARCODE_NO'];
                                $mergeArray = $data;
                            }
                            // var_dump($mergeArray);
                            if (count($res) !== 0) {
                                array_push($res_des, $mergeArray);
                            }
                        }
                    }
                    if (count($res) === 0) {
                        $res = array();
                        $res_des = [];
                        // array_push($res_des, $res[0]);
                    }

                    // return array('status' => true, 'barcode' => $data->result_array(), 'data' => $res);
                }
                // else {
                //     return array('status' => false, 'barcode' => array(), 'data' => $res = array());
                // }

            }
            return array('status' => true, 'barcode' => $barcode_detail, 'data' => $res_des);
        }
    }

    public function Save_Color_Info()
    {
        $color = $this->input->post('color_picker');
        $data = $this->input->post('data');
        foreach ($data as $purch) {
            $purch_mt_id = $purch['PURCH_MT_ID'];
            // var_dump($purch_mt_id);
            $this->db->query("UPDATE lz_purch_item_mt SET COLOR = '$color' WHERE PURCH_MT_ID = '$purch_mt_id' ");
        }
        return array('status' => true, 'message' => 'Color Updated Successfully');
    }
    public function Delete_Purchase_Barcode()
    {
        $barcode = trim($this->input->post('barcode'));
        $user_id = $this->input->post('user_id');
        $purch_mt_id = $this->input->post('purch_mt_id');
        $delete = $this->db->query("call PRO_UNPOST_BARCODE('$barcode', '$user_id', '')");
        if ($delete == true) {
            $updateQty = $this->db->query("UPDATE lz_purch_item_mt SET QTY = QTY - 1 WHERE PURCH_MT_ID = '$purch_mt_id'");
            $data = $this->db->query("SELECT distinct  d.tag_id , d.tag_remarks, tm.tag_desc, (SELECT LISTAGG(d.barcode_no, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
            FROM lz_purch_item_dt d
           where d.purch_mt_id = pit.purch_mt_id) barcode_no,
         case
           when b.barcode_no is null then
            0
           else
            1
         end recoginze,
         (select LISTAGG(PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY pit.purch_mt_id)
            from (SELECT PT.PORTAL_DESC
                    FROM LZ_PORTAL_MT         PT,
                         LZ_BARCODE_PORTAL_DT PD,
                         lz_purch_item_dt     d
                   where d.purch_mt_id = pit.purch_mt_id
                     AND PT.PORTAL_ID = PD.PORTAL_ID
                     AND PT.ACTIVE_YN = 1
                     AND PD.BARCODE_NO IN (d.barcode_no)
                   group by PT.PORTAL_DESC)) PORTAL_DESC,
         (SELECT LISTAGG(d.rowId, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
            FROM lz_purch_item_dt d
           where d.purch_mt_id = pit.purch_mt_id) purch_dt_rowId,
         (SELECT USER_NAME
            FROM EMPLOYEE_MT
           WHERE EMPLOYEE_ID = pit.inserted_by) PURCHASE_INSERTED_BY,
         (SELECT USER_NAME
            FROM EMPLOYEE_MT
           WHERE EMPLOYEE_ID = pit.REMARKS_BY) PURCHASE_REMARKS_BY,
         b.lz_manifest_id,
         b.item_id,
         pit.purch_mt_id,
         pit.purchase_no,
         pit.mpn,
         pit.upc,
         pit.imei1,
         pit.imei2,
         pit.cost,
         pit.qty,
         pit.remarks,
         pit.inserted_by,
         pit.update_date,
         TO_CHAR(pit.inserted_date, 'MM/DD/YY HH24:MI:SS') inserted_date,
         pit.condition,
         pit.HISTORY_STATUS,
         pit.LISTER_REMARKS,
         b.ebay_item_id,
         list_price.list_price,
         cm.cond_name,
         (list_price.list_price * pit.qty) total_list_price,
         (pit.cost * pit.qty) total_cost,
         (SELECT USER_NAME
            FROM EMPLOYEE_MT
           WHERE EMPLOYEE_ID =  dd.ASSIGN_BY) ASSIGN_BY,
         (SELECT USER_NAME
            FROM EMPLOYEE_MT
           WHERE EMPLOYEE_ID =  dd.ASSIGN_TO) ASSIGN_TO,
         TO_CHAR (dd.ASSIGN_DATE, 'MM/DD/YY HH24:MI:SS') ASSIGN_DATE,
         decode(pit.color,
                '1',
                'red',
                '2',
                'green',
                '3',
                'orange',
                '4',
                'yellow',
                '5',
                'black',
                'white') color,
                (SELECT LISTAGG(pt.PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY d.portal_id) SELECT_PORTAL
            FROM lz_item_list_mt d, lz_portal_mt pt
           where d.barcode_no = dd.barcode_no AND pt.portal_id = d.portal_id) SELECTED_PORTAL,
           bi.BIN_TYPE || '-' || bi.BIN_NO   BIN_NAME
    FROM lz_merchant_barcode_dt d,
    lz_purch_item_dt dd,
    lz_barcode_mt b,
    lz_item_cond_mt cm,
    lz_purch_item_mt pit,
    lz_item_list_mt lb,
    BIN_MT bi,
    lj_barcode_tag_mt tm,
    (select nvl(r.list_price, 0) list_price, r.ebay_item_id
    from ebay_list_mt r,
        (select e.ebay_item_id, max(e.list_id) list_id
           from ebay_list_mt e
          group by e.ebay_item_id) ee
    where r.list_id = ee.list_id) list_price
    WHERE d.barcode_no = dd.barcode_no
    AND dd.barcode_no = b.barcode_no(+)
    AND d.barcode_no = b.barcode_no(+)
    and d.barcode_no = lb.barcode_no(+)
    AND pit.purch_mt_id = dd.purch_mt_id
    and list_price.ebay_item_id(+) = b.ebay_item_id
    AND cm.id = pit.condition
    AND b.lz_pos_mt_id is NULL
    AND b.item_adj_det_id_for_out IS NULL
    and (b.discard is null or b.discard =0)
    and b.not_found_by is null
    and b.order_dt_id is null
    and b.extendedorderid is null
    and b.order_id is null
    and b.sale_record_no is null
    and (b.condition_id >0 or b.condition_id is null)
    and b.item_adj_det_id_for_in is null
    and b.lz_part_issue_mt_id is null
    and b.returned_id is null
    and b.returned_id_for_in is null
    --and b.ebay_item_id IS NULL
    and (b.hold_status is null or b.hold_status = 0)
    and d.bin_id = bi.bin_id(+)
    and d.tag_id = tm.tag_id(+)
        AND pit.purch_mt_id = '$purch_mt_id'
        ORDER BY pit.update_date DESC")->result_array();
            $images = $this->get_pictures($data);
            return array('status' => true, 'message' => 'Barcode Delete Successfully', 'data' => $data, 'images' => $images['uri']);
        } else {
            return array('status' => false, 'message' => 'Barcode Not Delete Successfully');
        }
    }

    public function Delete_Purchase_All_Barcode()
    {
        // $single_barcode = $this->input->post('barcodes');
        $barcodes = $this->input->post('barcodes');
        $user_id = $this->input->post('user_id');
        $purch_mt_id = $this->input->post('purch_mt_id');
        foreach ($barcodes as $barcode) {
            $barc = trim($barcode);
            $delete = $this->db->query("call PRO_UNPOST_BARCODE('$barc', '$user_id', '')");
        }
        // exit;
        if ($delete == true) {
            $deleteMt = $this->db->query("DELETE FROM lz_purch_item_mt WHERE purch_mt_id = '$purch_mt_id'");
            if ($deleteMt == true) {
                return array('status' => true, 'message' => 'Purchase Delete Successfully');

            } else {
                return array('status' => false, 'message' => 'Purchase Not Delete Successfully');
            }
        } else {
            return array('status' => false, 'message' => 'Purchase Not Delete Successfully');

        }
    }

    public function Un_Post_Purchase_Barcode()
    {
        // $user_id = $this->input->post('user_id');
        // $barcode = $this->input->post('barcode');
        // foreach ($barcodes as $barcode) {
        //     // $delete = $this->db->query("call PRO_UNPOST_BARCODE('$barcode', '$user_id', '')");
        // }
        return array('status' => true, 'message' => 'Barcode UnPost Successfully');
    }
    public function Get_Single_Purchase($purch_mt_id)
    {
        $data = $this->db->query("SELECT distinct  d.tag_id , d.tag_remarks, tm.tag_desc, (SELECT LISTAGG(d.barcode_no, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
        FROM lz_purch_item_dt d
       where d.purch_mt_id = pit.purch_mt_id) barcode_no,
     case
       when b.barcode_no is null then
        0
       else
        1
     end recoginze,
     (select LISTAGG(PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY pit.purch_mt_id)
        from (SELECT PT.PORTAL_DESC
                FROM LZ_PORTAL_MT         PT,
                     LZ_BARCODE_PORTAL_DT PD,
                     lz_purch_item_dt     d
               where d.purch_mt_id = pit.purch_mt_id
                 AND PT.PORTAL_ID = PD.PORTAL_ID
                 AND PT.ACTIVE_YN = 1
                 AND PD.BARCODE_NO IN (d.barcode_no)
               group by PT.PORTAL_DESC)) PORTAL_DESC,
     (SELECT LISTAGG(d.rowId, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
        FROM lz_purch_item_dt d
       where d.purch_mt_id = pit.purch_mt_id) purch_dt_rowId,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID = pit.inserted_by) PURCHASE_INSERTED_BY,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID = pit.REMARKS_BY) PURCHASE_REMARKS_BY,
     b.lz_manifest_id,
     b.item_id,
     pit.purch_mt_id,
     pit.purchase_no,
     pit.mpn,
     pit.upc,
     pit.imei1,
     pit.imei2,
     pit.cost,
     pit.qty,
     pit.remarks,
     pit.inserted_by,
     pit.update_date,
     TO_CHAR(pit.inserted_date, 'MM/DD/YY HH24:MI:SS') inserted_date,
     pit.condition,
     pit.HISTORY_STATUS,
     pit.LISTER_REMARKS,
     b.ebay_item_id,
     list_price.list_price,
     cm.cond_name,
     (list_price.list_price * pit.qty) total_list_price,
     (pit.cost * pit.qty) total_cost,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID =  dd.ASSIGN_BY) ASSIGN_BY,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID =  dd.ASSIGN_TO) ASSIGN_TO,
     TO_CHAR (dd.ASSIGN_DATE, 'MM/DD/YY HH24:MI:SS') ASSIGN_DATE,
     decode(pit.color,
            '1',
            'red',
            '2',
            'green',
            '3',
            'orange',
            '4',
            'yellow',
            '5',
            'black',
            'white') color,
            (SELECT LISTAGG(pt.PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY d.portal_id) SELECT_PORTAL
        FROM lz_item_list_mt d, lz_portal_mt pt
       where d.barcode_no = dd.barcode_no AND pt.portal_id = d.portal_id) SELECTED_PORTAL,
       bi.BIN_TYPE || '-' || bi.BIN_NO   BIN_NAME
FROM lz_merchant_barcode_dt d,
lz_purch_item_dt dd,
lz_barcode_mt b,
lz_item_cond_mt cm,
lz_purch_item_mt pit,
lz_item_list_mt lb,
BIN_MT bi,
lj_barcode_tag_mt tm,
(select nvl(r.list_price, 0) list_price, r.ebay_item_id
from ebay_list_mt r,
    (select e.ebay_item_id, max(e.list_id) list_id
       from ebay_list_mt e
      group by e.ebay_item_id) ee
where r.list_id = ee.list_id) list_price
WHERE d.barcode_no = dd.barcode_no
AND dd.barcode_no = b.barcode_no(+)
AND d.barcode_no = b.barcode_no(+)
and d.barcode_no = lb.barcode_no(+)
AND pit.purch_mt_id = dd.purch_mt_id
and list_price.ebay_item_id(+) = b.ebay_item_id
AND cm.id = pit.condition
AND b.lz_pos_mt_id is NULL
AND b.item_adj_det_id_for_out IS NULL
and (b.discard is null or b.discard =0)
and b.not_found_by is null
and b.order_dt_id is null
and b.extendedorderid is null
and b.order_id is null
and b.sale_record_no is null
and (b.condition_id >0 or b.condition_id is null)
and b.item_adj_det_id_for_in is null
and b.lz_part_issue_mt_id is null
and b.returned_id is null
and b.returned_id_for_in is null
--and b.ebay_item_id IS NULL
and (b.hold_status is null or b.hold_status = 0)
and d.bin_id = bi.bin_id(+)
and d.tag_id = tm.tag_id(+)
    AND pit.purch_mt_id = '$purch_mt_id'
    ORDER BY pit.update_date DESC
    ")->result_array();
        return $data;
    }
    public function Edit_Purchase_Detail()
    {
        $purch_mt_id = $this->input->post('purch_mt_id');
        $upc = $this->input->post('upc');
        $upc = trim(str_replace("  ", ' ', $upc));
        $upc = trim(str_replace(array("'"), "''", $upc));
        $mpn = $this->input->post('mpn');
        $mpn = trim(str_replace("  ", ' ', $mpn));
        $mpn = trim(str_replace(array("'"), "''", $mpn));
        $condition = $this->input->post('condition');
        $condition_value = trim($condition['value']);
        $cost = $this->input->post('cost');
        $cost = trim(str_replace("  ", ' ', $cost));
        $cost = trim(str_replace(array("'"), "''", $cost));
        $cost = trim(str_replace('$ ', '', $cost));
        $cost = trim(str_replace(',', '', $cost));
        $remarks = $this->input->post('remarks');
        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = trim(str_replace(array("'"), "''", $remarks));
        $userId = $this->input->post('userId');
        $lz_manifest_id = $this->input->post('lz_manifest_id');
        $item_id = $this->input->post('item_id');
        $barcodes = $this->input->post('barcodes');
        if (!empty($lz_manifest_id)) {
            $updateSpecialLot = $this->db->query("UPDATE lz_special_lots SET LOT_REMARKS = '$remarks', ITEM_COST = '$cost' WHERE BARCODE_PRV_NO IN ($barcodes)");
            $updateManifest = $this->db->query("UPDATE lz_manifest_det SET po_detail_retial_price = '$cost' WHERE LZ_MANIFEST_ID = '$lz_manifest_id' AND  LAPTOP_ITEM_CODE = (SELECT ITEM_CODE from ITEMS_MT WHERE  ITEM_ID = $item_id)");
            $update = $this->db->query("UPDATE lz_purch_item_mt SET REMARKS = '$remarks', COST = '$cost', UPDATE_BY = '$userId' , UPDATE_DATE = sysdate WHERE PURCH_MT_ID = '$purch_mt_id'");
            if ($update == true) {
                $data = $this->Get_Single_Purchase($purch_mt_id);
                $images = $this->get_pictures($data);
                return array('status' => true, 'message' => 'Updated Successfully', 'data' => $data, 'images' => $images['uri']);
            } else {
                return array('status' => false, 'message' => 'Not Updated Successfully');
            }
        } else {
            $updateSpecialLot = $this->db->query("UPDATE lz_special_lots SET CARD_UPC = '$upc', CARD_MPN='$mpn', CONDITION_ID = '$condition_value', LOT_REMARKS = '$remarks', ITEM_COST = '$cost' WHERE BARCODE_PRV_NO IN ($barcodes)");
            $update = $this->db->query("UPDATE lz_purch_item_mt SET REMARKS = '$remarks', COST = '$cost', MPN = '$mpn', UPC= '$upc', CONDITION= '$condition_value', UPDATE_BY = '$userId' , UPDATE_DATE = sysdate WHERE PURCH_MT_ID = '$purch_mt_id'");
            if ($update == true) {
                $data = $this->Get_Single_Purchase($purch_mt_id);
                $images = $this->get_pictures($data);
                return array('status' => true, 'message' => 'Updated Successfully', 'data' => $data, 'images' => $images['uri']);
            } else {
                return array('status' => false, 'message' => 'Not Updated Successfully');
            }

        }
    }

    public function Get_All_Purchase_Record()
    {
        $user_id = $_GET['user_id'];
        $data = "SELECT distinct  d.tag_id , d.tag_remarks, tm.tag_desc, (SELECT LISTAGG(d.barcode_no, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
        FROM lz_purch_item_dt d
       where d.purch_mt_id = pit.purch_mt_id) barcode_no,
     case
       when b.barcode_no is null then
        0
       else
        1
     end recoginze,
     (select LISTAGG(PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY pit.purch_mt_id)
        from (SELECT PT.PORTAL_DESC
                FROM LZ_PORTAL_MT         PT,
                     LZ_BARCODE_PORTAL_DT PD,
                     lz_purch_item_dt     d
               where d.purch_mt_id = pit.purch_mt_id
                 AND PT.PORTAL_ID = PD.PORTAL_ID
                 AND PT.ACTIVE_YN = 1
                 AND PD.BARCODE_NO IN (d.barcode_no)
               group by PT.PORTAL_DESC)) PORTAL_DESC,
     (SELECT LISTAGG(d.rowId, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
        FROM lz_purch_item_dt d
       where d.purch_mt_id = pit.purch_mt_id) purch_dt_rowId,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID = pit.inserted_by) PURCHASE_INSERTED_BY,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID = pit.REMARKS_BY) PURCHASE_REMARKS_BY,
     b.lz_manifest_id,
     b.item_id,
     pit.purch_mt_id,
     pit.purchase_no,
     pit.mpn,
     pit.upc,
     pit.imei1,
     pit.imei2,
     pit.cost,
     pit.qty,
     pit.remarks,
     pit.inserted_by,
     pit.update_date,
     TO_CHAR(pit.inserted_date, 'MM/DD/YY HH24:MI:SS') inserted_date,
     pit.condition,
     pit.HISTORY_STATUS,
     pit.LISTER_REMARKS,
     b.ebay_item_id,
     list_price.list_price,
     cm.cond_name,
     (list_price.list_price * pit.qty) total_list_price,
     (pit.cost * pit.qty) total_cost,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID =  dd.ASSIGN_BY) ASSIGN_BY,
     (SELECT USER_NAME
        FROM EMPLOYEE_MT
       WHERE EMPLOYEE_ID =  dd.ASSIGN_TO) ASSIGN_TO,
     TO_CHAR (dd.ASSIGN_DATE, 'MM/DD/YY HH24:MI:SS') ASSIGN_DATE,
     decode(pit.color,
            '1',
            'red',
            '2',
            'green',
            '3',
            'orange',
            '4',
            'yellow',
            '5',
            'black',
            'white') color,
            (SELECT LISTAGG(pt.PORTAL_DESC, ', ') WITHIN GROUP(ORDER BY d.portal_id) SELECT_PORTAL
        FROM lz_item_list_mt d, lz_portal_mt pt
       where d.barcode_no = dd.barcode_no AND pt.portal_id = d.portal_id) SELECTED_PORTAL,
       bi.BIN_TYPE || '-' || bi.BIN_NO   BIN_NAME
FROM lz_merchant_barcode_dt d,
lz_purch_item_dt dd,
lz_barcode_mt b,
lz_item_cond_mt cm,
lz_purch_item_mt pit,
lz_item_list_mt lb,
BIN_MT bi,
lj_barcode_tag_mt tm,
(select nvl(r.list_price, 0) list_price, r.ebay_item_id
from ebay_list_mt r,
    (select e.ebay_item_id, max(e.list_id) list_id
       from ebay_list_mt e
      group by e.ebay_item_id) ee
where r.list_id = ee.list_id) list_price
WHERE d.barcode_no = dd.barcode_no
AND dd.barcode_no = b.barcode_no(+)
AND d.barcode_no = b.barcode_no(+)
and d.barcode_no = lb.barcode_no(+)
AND pit.purch_mt_id = dd.purch_mt_id
and list_price.ebay_item_id(+) = b.ebay_item_id
AND cm.id = pit.condition
AND b.lz_pos_mt_id is NULL
AND b.item_adj_det_id_for_out IS NULL
and (b.discard is null or b.discard =0)
and b.not_found_by is null
and b.order_dt_id is null
and b.extendedorderid is null
and b.order_id is null
and b.sale_record_no is null
and (b.condition_id >0 or b.condition_id is null)
and b.item_adj_det_id_for_in is null
and b.lz_part_issue_mt_id is null
and b.returned_id is null
and b.returned_id_for_in is null
and b.ebay_item_id IS NULL
and (b.hold_status is null or b.hold_status = 0)
and d.bin_id = bi.bin_id(+)
and d.tag_id = tm.tag_id(+) ";
        $userLocaltion = $this->db->query("select e.location from employee_mt e where e.employee_id ='$user_id'")->result_array();
        if ($userLocaltion[0]['LOCATION'] == 'US') {
            $data .= "ORDER BY pit.update_date DESC";
        } else {
            $data .= "ORDER BY TO_CHAR(pit.inserted_date, 'MM/DD/YY HH24:MI:SS') DESC";
        }

        $result = $this->db->query($data)->result_array();
        if (count($result) > 0) {
            $images = $this->get_pictures($result);
            return array('status' => true, 'data' => $result, 'images' => $images['uri']);
        } else {
            return array('status' => false, 'data' => array(), 'images' => array(), 'message' => 'No Record Found');
        }
    }

    public function Get_Barcode_MarketPlace()
    {
        $barcode = $this->input->post('barcode');
        $barcode = explode(',', $barcode);
        $marcket_place = [];
        if (empty($barcode[1])) {
            $barcode = trim($barcode[0]);
            //         $data = $this->db->query("SELECT DISTINCT PT.PORTAL_ID, PT.PORTAL_DESC, PD.BARCODE_NO
            //     FROM LZ_PORTAL_MT PT, LZ_BARCODE_PORTAL_DT PD
            //    WHERE PT.PORTAL_ID = PD.PORTAL_ID
            //      AND PT.ACTIVE_YN = 1
            //      AND PD.BARCODE_NO = '$barcode'
            //    ORDER BY PT.PORTAL_ID ASC
            //   ")->result_array();
            $data = $this->db->query("SELECT LISTAGG(PT.PORTAL_DESC, ', ') WITHIN GROUP (ORDER BY PT.PORTAL_ID) PORTAL_DESC
    FROM LZ_PORTAL_MT PT, LZ_BARCODE_PORTAL_DT PD WHERE PT.PORTAL_ID = PD.PORTAL_ID
AND PT.ACTIVE_YN = 1
AND PD.BARCODE_NO = '$barcode'")->result_array();
            if (count($data) > 0) {
                $record = [];
                foreach ($data as $key => $rec) {
                    $rec['BARCODE_NO'] = $barcode;
                    $record = array($rec);
                }
                return array("data" => $record, 'status' => true);
            } else {
                return array("data" => $data, 'status' => false, 'message' => 'No Market Place Found');
            }
        } else {
            foreach ($barcode as $key => $bar) {
                $barco = trim($bar);
                //     $data = $this->db->query("SELECT DISTINCT PT.PORTAL_ID, PT.PORTAL_DESC, PD.BARCODE_NO
                //     FROM LZ_PORTAL_MT PT, LZ_BARCODE_PORTAL_DT PD
                //    WHERE PT.PORTAL_ID = PD.PORTAL_ID
                //      AND PT.ACTIVE_YN = 1
                //      AND PD.BARCODE_NO = '$barco'
                //    ORDER BY PT.PORTAL_ID ASC
                //   ")->result_array();
                $data = $this->db->query("SELECT LISTAGG(PT.PORTAL_DESC, ', ') WITHIN GROUP (ORDER BY PT.PORTAL_ID) PORTAL_DESC
    FROM LZ_PORTAL_MT PT, LZ_BARCODE_PORTAL_DT PD WHERE PT.PORTAL_ID = PD.PORTAL_ID
AND PT.ACTIVE_YN = 1
AND PD.BARCODE_NO = '$barco'")->result_array();
                if (count($data) > 0) {
                    foreach ($data as $record) {
                        $record['BARCODE_NO'] = $barco;
                        array_push($marcket_place, $record);
                    }
                }
            }
            if (count($marcket_place) > 0) {
                return array("data" => $marcket_place, 'status' => true);
            } else {
                return array("data" => $marcket_place, 'status' => false, 'message' => 'No Market Place Found');
            }
        }
    }
    public function Save_Insta_MarketPlace()
    {
        $list_id = $this->input->post('list_id');
        $list_id = trim(str_replace("  ", ' ', $list_id));
        $list_id = trim(str_replace(array("'"), "''", $list_id));
        $user_id = $this->input->post('user_id');
        $list_price = $this->input->post('list_price');
        $list_price = trim(str_replace("  ", ' ', $list_price));
        $list_price = trim(str_replace(array("'"), "''", $list_price));
        $list_url = $this->input->post('instaUrl');
        $list_url = trim(str_replace("  ", ' ', $list_url));
        $list_url = trim(str_replace(array("'"), "''", $list_url));
        $item_desc = $this->input->post('item_desc');
        $item_desc = trim(str_replace("  ", ' ', $item_desc));
        $item_desc = trim(str_replace(array("'"), "''", $item_desc));
        $data = $this->input->post('rowData');
        $barcode = $data['BARCODE_NO'];
        $barcodes = explode(',', $barcode);
        foreach ($barcodes as $bar_val) {
            $insert = $this->db->query("INSERT INTO  lz_item_list_mt (LIST_ID, BARCODE_NO, LIST_DATE, LIST_BY, LISTING_ID, LISTING_URL, ITEM_TITLE, LIST_PRICE, PORTAL_ID ) VALUES (get_single_primary_key('lz_item_list_mt','LIST_ID'), '$bar_val',sysdate, '$user_id', '$list_id','$list_url', '$item_desc', '$list_price', null ");

        }
        if ($insert) {
            return array("status" => true, "message" => 'Reacord Updated Successfully');
        } else {
            return array("status" => false, "message" => 'Reacord Not Updated Successfully');
        }
    }

    public function Save_Fb_MarketPlace()
    {
        $list_id = $this->input->post('list_id');
        $user_id = $this->input->post('user_id');
        $list_price = $this->input->post('list_price');
        $list_url = $this->input->post('fbUrl');
        $item_desc = $this->input->post('item_desc');
        $data = $this->input->post('rowData');
        $barcode = $data['BARCODE_NO'];
        $barcodes = explode(',', $barcode);
        foreach ($barcodes as $bar_val) {
            // $update = $this->db->query("UPDATE LZ_BARCODE_PORTAL_DT SET LIST_ID = '$list_id', LIST_PRICE= '$list_price', LIST_URL = '$list_url', LISTED_BY = '$user_id', LISTED_DATE = sysdate WHERE BARCODE_NO ='$bar_val'");
            $insert = $this->db->query("INSERT INTO  lz_item_list_mt (LIST_ID, BARCODE_NO, LIST_DATE, LIST_BY, LISTING_ID, LISTING_URL, ITEM_TITLE, LIST_PRICE, PORTAL_ID ) VALUES (get_single_primary_key('lz_item_list_mt','LIST_ID'), '$bar_val',sysdate, '$user_id', '$list_id','$list_url', '$item_desc', '$list_price', null ");

        }
        if ($insert) {
            return array("status" => true, "message" => 'Reacord Updated Successfully');
        } else {
            return array("status" => false, "message" => 'Reacord Not Updated Successfully');
        }
    }

    public function Save_MarketPlace_Detail()
    {
        $list_id = $this->input->post('list_id');
        $list_id = trim(str_replace("  ", ' ', $list_id));
        $list_id = trim(str_replace(array("'"), "''", $list_id));
        $user_id = $this->input->post('user_id');
        $list_price = $this->input->post('list_price');
        $list_price = trim(str_replace("  ", ' ', $list_price));
        $list_price = trim(str_replace(array("'"), "''", $list_price));
        $list_url = $this->input->post('list_url');
        $list_url = trim(str_replace("  ", ' ', $list_url));
        $list_url = trim(str_replace(array("'"), "''", $list_url));
        $item_desc = $this->input->post('item_desc');
        $item_desc = trim(str_replace("  ", ' ', $item_desc));
        $item_desc = trim(str_replace(array("'"), "''", $item_desc));
        $data = $this->input->post('rowData');
        $portal_name = strtoupper($this->input->post('portal_name'));
        $portal_id = $this->db->query("SELECT PORTAL_ID FROM LZ_PORTAL_MT WHERE UPPER(PORTAL_DESC) = '$portal_name'")->result_array();
        if (!empty($portal_id)) {
            $portal_id = $portal_id[0]['PORTAL_ID'];
        } else {
            $portal_id = '';
        }
        $barcode = $data['BARCODE_NO'];
        $purch_mt_id = $data['PURCH_MT_ID'];
        $barcodes = explode(',', $barcode);
        foreach ($barcodes as $bar_val) {
            $insert = $this->db->query("INSERT INTO  lz_item_list_mt (LIST_ID, BARCODE_NO, LIST_DATE, LIST_BY, LISTING_ID, LISTING_URL, ITEM_TITLE, LIST_PRICE, PORTAL_ID ) VALUES (get_single_primary_key('lz_item_list_mt','LIST_ID'), '$bar_val',sysdate, '$user_id', '$list_id','$list_url', '$item_desc', '$list_price', '$portal_id' )");

        }

        if ($insert) {
            $data = $this->db->query("SELECT * FROM LZ_ITEM_LIST_MT WHERE BARCODE_NO IN ('$barcode') AND PORTAL_ID = '$portal_id'")->result_array();
            $purch = $this->Get_Single_Purchase($purch_mt_id);
            return array("status" => true, "message" => 'Reacord Updated Successfully', 'data' => $data, 'purch_data' => $purch);
        } else {
            return array("status" => false, "message" => 'Reacord Not Updated Successfully', 'data' => array(), 'purch_data' => array());
        }
    }

    public function Get_Market_Place_Detail()
    {
        $portal_name = strtoupper($this->input->post('portal_name'));
        $data = $this->input->post('data');
        $portal_id = $this->db->query("SELECT PORTAL_ID FROM LZ_PORTAL_MT WHERE UPPER(PORTAL_DESC) = '$portal_name'")->result_array();
        if (!empty($portal_id)) {
            $portal_id = $portal_id[0]['PORTAL_ID'];
        } else {
            $portal_id = '';
        }
        $barcode = $data['BARCODE_NO'];
        $data = $this->db->query("SELECT * FROM LZ_ITEM_LIST_MT WHERE BARCODE_NO IN ('$barcode') AND PORTAL_ID = '$portal_id'")->result_array();
        if ($data) {
            return array('status' => true, 'data' => $data);
        } else {
            return array('status' => false, 'data' => array());
        }
    }
    public function Delete_Market_Place_Barcode()
    {
        $user_id = $this->input->post('user_id');
        $data = $this->input->post('data');
        $list_id = $data['LIST_ID'];
        $delete = $this->db->query("DELETE FROM LZ_ITEM_LIST_MT WHERE LIST_ID = '$list_id'");
        if ($delete) {
            return array('status' => true, 'message' => 'Deleted Successfully');
        } else {
            return array('status' => false, 'message' => 'Deleted  Not Successfully');
        }
    }
    public function Update_Market_Place()
    {
        $user_id = $this->input->post('user_id');
        $cellName = $this->input->post('cellName');
        $cellValue = $this->input->post('cellValue');
        $cellValue = trim(str_replace("  ", ' ', $cellValue));
        $cellValue = trim(str_replace(array("'"), "''", $cellValue));
        $list_id = $this->input->post('list_id');
        if ($cellName == 'LISTING_ID') {
            $update = $this->db->query("UPDATE LZ_ITEM_LIST_MT SET LISTING_ID = '$cellValue' WHERE LIST_ID = '$list_id' ");
        }
        if ($cellName == 'LISTING_URL') {
            $update = $this->db->query("UPDATE LZ_ITEM_LIST_MT SET LISTING_URL = '$cellValue' WHERE LIST_ID = '$list_id' ");
        }
        if ($cellName == 'ITEM_TITLE') {
            $update = $this->db->query("UPDATE LZ_ITEM_LIST_MT SET ITEM_TITLE = '$cellValue' WHERE LIST_ID = '$list_id' ");
        }
        if ($cellName == 'LIST_PRICE') {
            $update = $this->db->query("UPDATE LZ_ITEM_LIST_MT SET LIST_PRICE = '$cellValue' WHERE LIST_ID = '$list_id' ");
        }
        if ($update) {
            return array("status" => true, "message" => 'Reacord Updated Successfully');
        } else {
            return array("status" => false, "message" => 'Reacord Not Updated Successfully');
        }
    }
    // public function Edit_Purchase_Detail()
    // {
    //     $purch_mt_id = $this->input->post('purch_mt_id');
    //     $upc = $this->input->post('upc');
    //     $upc = trim(str_replace("  ", ' ', $upc));
    //     $upc = trim(str_replace(array("'"), "''", $upc));
    //     $mpn = $this->input->post('mpn');
    //     $mpn = trim(str_replace("  ", ' ', $mpn));
    //     $mpn = trim(str_replace(array("'"), "''", $mpn));
    //     $condition = $this->input->post('condition');
    //     $condition_value = trim($condition['value']);
    //     $cost = $this->input->post('cost');
    //     $cost = trim(str_replace("  ", ' ', $cost));
    //     $cost = trim(str_replace(array("'"), "''", $cost));
    //     $cost = trim(str_replace('$ ', '', $cost));
    //     $remarks = $this->input->post('remarks');
    //     $remarks = trim(str_replace("  ", ' ', $remarks));
    //     $remarks = trim(str_replace(array("'"), "''", $remarks));
    //     $userId = $this->input->post('userId');
    //     $history = $this->input->post('history');
    //     $barcodes = $this->input->post('barcodes');
    //     if ($history == 0) {
    //         $updateSpecialLot = $this->db->query("UPDATE lz_special_lots SET CARD_UPC = '$upc', CARD_MPN='$mpn', CONDITION_ID = '$condition_value', LOT_REMARKS = '$remarks', ITEM_COST = '$cost' WHERE BARCODE_PRV_NO IN ($barcodes)");
    //         $update = $this->db->query("UPDATE lz_purch_item_mt SET REMARKS = '$remarks', COST = '$cost', MPN = '$mpn', UPC= '$upc', CONDITION= '$condition_value', UPDATE_BY = '$userId' , UPDATE_DATE = sysdate WHERE PURCH_MT_ID = '$purch_mt_id'");
    //         if ($update == true) {
    //             return array('status' => true, 'message' => 'Remarks Updated Successfully');
    //         } else {
    //             return array('status' => false, 'message' => 'Remarks Not Updated Successfully');
    //         }
    //     } else {
    //         $updateSpecialLot = $this->db->query("UPDATE lz_special_lots SET LOT_REMARKS = '$remarks', ITEM_COST = '$cost' WHERE BARCODE_PRV_NO IN ($barcodes)");
    //         $selectLaptop = $this->db->query("SELECT dd.lz_manifest_det_id from lz_special_lots dd WHERE  dd.BARCODE_PRV_NO IN ($barcodes)")->result_array();
    //         // $lz_manifest_det_id = $selectLaptop[0]['lz_manifest_det_id'];
    //         foreach ($selectLaptop as $id) {
    //             $lz_manifest_det_id = $id['LZ_MANIFEST_DET_ID'];
    //             $this->db->query("UPDATE lz_manifest_det SET po_detail_retial_price = '$cost' WHERE LAPTOP_ZONE_ID = $lz_manifest_det_id");
    //         }
    //         $update = $this->db->query("UPDATE lz_purch_item_mt SET REMARKS = '$remarks', COST = '$cost', UPDATE_BY = '$userId' , UPDATE_DATE = sysdate WHERE PURCH_MT_ID = '$purch_mt_id'");

    //         if ($update == true) {
    //             return array('status' => true, 'message' => 'Remarks Updated Successfully');
    //         } else {
    //             return array('status' => false, 'message' => 'Remarks Not Updated Successfully');
    //         }
    //     }
    //     // , CONDITION = '$condition_value' , MPN = '$mpn', UPC ='$upc'

    // }

}
// SELECT
// distinct
// (SELECT LISTAGG(d.barcode_no, ', ') WITHIN GROUP (ORDER BY d.barcode_no) barco_no
// FROM lz_purch_item_dt d where d.purch_mt_id = pit.purch_mt_id) barcode_no,
// case
// when b.barcode_no is null then
// 0
// else
// 1
// end recoginze,
//  (SELECT  LISTAGG(d.rowId, ', ') WITHIN GROUP (ORDER BY d.barcode_no) barco_no
//  FROM lz_purch_item_dt d where d.purch_mt_id = pit.purch_mt_id) purch_dt_rowId,
//  (SELECT USER_NAME FROM EMPLOYEE_MT WHERE EMPLOYEE_ID = pit.inserted_by) PURCHASE_INSERTED_BY,
//  (SELECT USER_NAME FROM EMPLOYEE_MT WHERE EMPLOYEE_ID = pit.REMARKS_BY) PURCHASE_REMARKS_BY,
//  b.lz_manifest_id,
//         b.item_id,
// pit.purch_mt_id,
// pit.purchase_no,
// pit.mpn,
// pit.upc,
// pit.imei1,
// pit.imei2,
// pit.cost,
// pit.qty,
// pit.remarks,
// pit.inserted_by,
// TO_CHAR(pit.inserted_date, 'MM/DD/YY HH24:MI:SS') inserted_date,
// pit.condition,
// pit.HISTORY_STATUS,
// pit.LISTER_REMARKS,
// b.ebay_item_id,
// list_price.list_price,
// cm.cond_name,
// (list_price.list_price*pit.qty) total_list_price,
// (pit.cost*pit.qty) total_cost,
// decode(pit.color,'1','red','2','green','3','orange','4','yellow','5','black','white')color
// FROM lz_merchant_barcode_dt d,
// lz_purch_item_dt dd,
// lz_barcode_mt b,
// lz_item_cond_mt cm,
// lz_purch_item_mt pit,
// (select nvl(r.list_price, 0) list_price, r.ebay_item_id
// from ebay_list_mt r,
// (select e.ebay_item_id, max(e.list_id) list_id
// from ebay_list_mt e
// group by e.ebay_item_id) ee
// where r.list_id = ee.list_id) list_price
// WHERE d.barcode_no = dd.barcode_no
// AND dd.barcode_no = b.barcode_no(+)
//     AND d.barcode_no = b.barcode_no(+)
// AND pit.purch_mt_id = dd.purch_mt_id
// and list_price.ebay_item_id(+) = b.ebay_item_id
// AND cm.id = pit.condition
// AND b.lz_pos_mt_id is NULL
// AND b.item_adj_det_id_for_out IS NULL
// and b.ebay_item_id IS NULL
// and (b.hold_status is null or  b.hold_status = 0)
// ORDER BY pit.purch_mt_id DESC
