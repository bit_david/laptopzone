<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Lot Model
 */

class m_lot extends CI_Model
{
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function Get_Lots_Detail()
    {
        $lot_items = $this->db->query("SELECT B.BARCODE_NO , DE.ITEM_MT_DESC ITEM_DESC,DE.PO_DETAIL_RETIAL_PRICE COST_PRICE,DE.CONDITIONS_SEG5 FROM LZ_MANIFEST_MT M, LZ_MANIFEST_DET DE, ITEMS_MT I, LZ_BARCODE_MT B WHERE M.MANIFEST_TYPE = 5  AND B.LZ_MANIFEST_ID = M.LZ_MANIFEST_ID AND M.LZ_MANIFEST_ID = DE.LZ_MANIFEST_ID AND B.ITEM_ID = I.ITEM_ID AND B.BARCODE_NO IS NOT NULL order by BARCODE_NO desc ")->result_array();
        if (count($lot_items) > 0) {
            return array('status' => true, 'data' => $lot_items);
        } else {
            return array('status' => true, 'data' => array(), 'message' => "No Lot Found");
        }
    }
    public function Get_Specific_Lot_Detail()
    {
        // $barcode_no = $this->input->post('barcode_no');
        $barcode_no = $_GET['barcode_no'];

        $lot_items_detail = $this->db->query(" SELECT B.BARCODE_NO, B.ITEM_ADJ_DET_ID_FOR_IN  IN_ID, D.ITEM_ADJUSTMENT_ID FROM LZ_BARCODE_MT B, ITEM_ADJUSTMENT_DET D WHERE B.BARCODE_NO = $barcode_no AND B.ITEM_ADJ_DET_ID_FOR_IN = D.ITEM_ADJUSTMENT_DET_ID  ");

        if ($lot_items_detail->num_rows() > 0) {
            $lot_items_detail = $lot_items_detail->result_array();
            $in_id = $lot_items_detail[0]['IN_ID'];
            $item_adjustment_id = $lot_items_detail[0]['ITEM_ADJUSTMENT_ID'];

            $get_detail = $this->db->query(" SELECT DISTINCT B.BARCODE_NO, I.ITEM_ID, I.ITEM_DESC TITLE, D.ITEM_MT_MANUFACTURE BRAND, D.ITEM_MT_MFG_PART_NO    MPN, D.ITEM_MT_UPC UPC, CON.COND_NAME CONDIOTION, D.PO_DETAIL_RETIAL_PRICE COST_PRICE, D.E_BAY_CATA_ID_SEG6     CATEGORY FROM LZ_BARCODE_MT B, ITEMS_MT I, LZ_MANIFEST_DET D, LZ_ITEM_COND_MT CON,ITEM_ADJUSTMENT_DET K WHERE B.ITEM_ID = I.ITEM_ID AND B.CONDITION_ID = CON.ID(+) AND I.ITEM_CODE = D.LAPTOP_ITEM_CODE AND B.LZ_MANIFEST_ID = D.LZ_MANIFEST_ID AND K.ITEM_ADJUSTMENT_ID = $item_adjustment_id AND K.ITEM_ADJUSTMENT_DET_ID = B.ITEM_ADJ_DET_ID_FOR_OUT AND K.ITEM_ADJUSTMENT_DET_ID  <>  $in_id ")->result_array();
            if (count($get_detail) > 0) {
                return array('status' => true, 'data' => $get_detail);
            } else {
                return array('status' => false, 'data' => array(), 'message' => 'No Detail Found');
            }
        } else {
            return array('status' => false, 'data' => array(), 'message' => 'No Record Found');
        }
    }

    public function Get_Print_Lot_Barcode_Detail()
    {
        $get_bar = $_GET['barcode_no'];
        $lot_items = $this->db->query(" SELECT DISTINCT B.BARCODE_NO , I.ITEM_DESC,DE.PO_DETAIL_RETIAL_PRICE COST_PRICE,DE.CONDITIONS_SEG5 FROM LZ_MANIFEST_MT M, LZ_MANIFEST_DET DE, ITEMS_MT I, LZ_BARCODE_MT B WHERE M.MANIFEST_TYPE = 5 AND DE.LAPTOP_ITEM_CODE = I.ITEM_CODE AND I.ITEM_ID = B.ITEM_ID(+) AND M.LZ_MANIFEST_ID = DE.LZ_MANIFEST_ID AND BARCODE_NO = $get_bar order by BARCODE_NO desc ")->result_array();
        if (count($lot_items) > 0) {
            return array('status' => true, 'data' => $lot_items);
        } else {
            return array('status' => false, 'data' => array());
        }
    }
}
