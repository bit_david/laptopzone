<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Single Entry Model
 */

class m_audit_barcode extends CI_Model
{
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        ini_set('MAX_EXECUTION_TIME', '-1');

    }
    public function convert_text($text)
    {

        $t = $text;
        // '\'' => '%27',
        // '~' => '%7E',
        // '_' => '%5F',
        // '/' => '%2F',
        // '\\' => '%5C',
        // '.' => '%2E',
        // '%' => '%25',
        // ':' => '%3A',
        // ' ' => '%20',
        $specChars = array(
            '!' => '%21', '"' => '%22',
            '#' => '%23', '$' => '',
            '&' => '%26', '(' => '%28',
            ')' => '%29', '*' => '%2A', '+' => '%2B',
            ',' => '%2C', '-' => '%2D',
            ';' => '%3B',
            '<' => '%3C', '=' => '%3D', '>' => '%3E',
            '?' => '%3F', '@' => '%40', '[' => '%5B',
            ']' => '%5D', '^' => '%5E',
            '`' => '%60', '{' => '%7B',
            '|' => '%7C', '}' => '%7D',
            ',' => '%E2%80%9A',
        );

        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }

        return $t;
    }

    public function get_pictures($barcodes)
    {
        $uri = [];
        $base64 = [];

        // $barocde_no = $this->input->post('barocde_no');
        foreach ($barcodes as $barco) {
            $bar = explode(',', $barco['BARCODE_NO']);
            $bar_val = $bar[0];
            $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

            if (count($qry) >= 1) {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $barcode = $qry[0]["FOLDER_NAME"];
                $dir = $master_path . $barcode . "/";
                // var_dump($dir);
                // exit;

            } else {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar_val' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();
                if (count($get_params) === 0) {
                    $get_params = $this->db->query("SELECT MT.UPC F_UPC, MT.MPN F_MPN, C.COND_NAME, DT.BARCODE_NO
                                      FROM LZ_PURCH_ITEM_MT MT, LZ_PURCH_ITEM_DT DT, LZ_ITEM_COND_MT C
                                     WHERE MT.PURCH_MT_ID = DT.PURCH_MT_ID
                                       AND C.ID = MT.CONDITION
                                       AND DT.BARCODE_NO = '$bar_val'")->result_array();
                }
                //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                $cond = @$get_params[0]['COND_NAME'];

                $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);
            // var_dump($dir);
            // exit;

            //var_dump(is_dir($dir));exit;
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            if (is_dir($dir)) {
                // var_dump($dir);exit;
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
                if (!empty($images)) {
                    $i = 0;
                    $count = 0;
                    foreach ($images as $image) {

                        // Get Pic Date of Folder
                        $now = date("Y-m-d"); // or your date as well
                        $fileDate = date("Y-m-d", filemtime($image));
                        $date1 = date_create($now);
                        $date2 = date_create($fileDate);
                        $diff = date_diff($date1, $date2);
                        $diff = $diff->format("%a");
                        // echo $diff;

                        $pathinfo = pathinfo($image);
                        $imagePath = explode('/', $image, 4);
                        // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $image_path = $this->convert_text(@$imagePath[3]);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $image_path);
                        // $uri[$bar_val][$i] = $base_url . $withoutMasterPartUri;
                        $uri[$bar_val]['image'][$i] = "/" . $withoutMasterPartUri;
                        $count = $count + 1;
                        // $uri[$bar_val][$i + 1] = $bar_val;
                        // $uri[$bar_val][$i + 2] = $i + 1;
                        $i++;
                    }
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = $count;
                    $uri[$bar_val][3] = $diff;
                } else {
                    // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                    // $image = $this->convert_text(@$imagePath);
                    $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = 0;
                    $uri[$bar_val][3] = 0;
                }

            } else {
                // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                // $image = $this->convert_text(@$imagePath);
                $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar_val][1] = $bar_val;
                $uri[$bar_val][2] = 0;
                $uri[$bar_val][3] = 0;
            }

            //var_dump($dekitted_pics);exit;

        }
        // var_dump($uri);
        // exit;
        return array('uri' => $uri);
    }
    public function Get_Audit_Barcode_detail1()
    {
        $startDate = $_GET['startDate'];
        $endDate = $_GET['endDate'];
        $merchant_id = $_GET['merchant_id'];
        $get_all = isset($_GET['get_all']);

        $data = "SELECT /*b.entry_point,*/
b.barcode_status,
decode(b.barcode_status,
       1,
       'SHIPPED',
       2,
       'SOLD',
       3,
       'LISTED',
       4,
       'POSTED_NOT_LISTED',
       5,
       'UNPOSTED',
       6,
       'UNPOSTED',
       7,
       'PICTURES REQUIRED',
       'not found') STATUS_DESC,

B.BIN_ID,
Bin.BIN_TYPE || '-' || Bin.BIN_NO BIN_NAME,
S.ITEM_TITLE,
S.F_UPC,
S.F_MANUFACTURE,
S.F_MPN,
S.OTHER_NOTES,
S.EBAY_PRICE,
S.SEED_ID,
B.BARCODE_NO,
b.INSERTED_DATE,
mm.MERCHANT_ID,
MM.CONTACT_PERSON MERCHANT_NAME,
 REPLACE(get_picture.thumb_img_url, 'D:/wamp/www/', '') thumb_url,
dd.tag_id
FROM LJ_BARCODE_LOG_MT      B,
(select oi.barcode_no,min(PIC_URL) thumb_img_url  from lz_barcode_pic oi group by  oi.barcode_no) get_picture,

BIN_MT                 BIN,
EMPLOYEE_MT            EM,
lz_merchant_barcode_dt dd,
lz_merchant_barcode_mt dm,
LZ_ITEM_SEED           S,
lz_barcode_mt          bb,
lz_merchant_mt         MM
WHERE B.BIN_ID = BIN.BIN_ID(+)
AND B.INSERTED_BY = EM.EMPLOYEE_ID
and b.barcode_no = dd.barcode_no
and dd.mt_id = dm.mt_id
and dm.merchant_id = MM.MERCHANT_ID
and b.barcode_no = bb.barcode_no(+)
AND bb.ITEM_ID = S.ITEM_ID(+)
AND bb.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+)
AND bb.CONDITION_ID = S.DEFAULT_COND(+)
AND B.BARCODE_NO = get_picture.barcode_no(+)
AND MM.MERCHANT_ID = '$merchant_id'
and b.log_id in
(SELECT MAX(LOG_ID) FROM LJ_BARCODE_LOG_MT group by barcode_no)";

        if ((!$get_all)) {
            $data .= " AND b.INSERTED_DATE between
        TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
        TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') ";
        }

        $data .= " order by b.log_id desc";
        // $data .= " ORDER BY MT.LOG_ID DESC";
        $data = $this->db->query($data);
        if ($data->num_rows() > 0) {
            // $images = $this->get_pictures($data->result_array());
            return array('status' => true, 'data' => $data->result_array(), 'images' => array());
        } else {
            return array('status' => false, 'data' => $data->result_array(), 'images' => array());
        }
    }

    public function Get_Audit_Barcode_Summary()
    {
        $startDate = $_GET['startDate'];
        $endDate = $_GET['endDate'];
        $merchant_id = $_GET['merchant_id'];
        $listFilter = $_GET['listFilterSer'];
        $picFilter = $_GET['picFilterSer'];
        $tagFilter = $_GET['tagFilterSer'];
        $get_all = isset($_GET['get_all']);
        $data = "SELECT COUNT(barcode_status) ALL_BARCODE,
        COUNT(SHIPPED) SHIPPED,
        COUNT(SOLD) SOLD,
        COUNT(LISTED) LISTED,
        COUNT(POSTED_NOT_LISTED) POSTED_NOT_LISTED,
        COUNT(UNPOSTED) UNPOSTED,
        COUNT(UN_POSTED) UN_POSTED,
        COUNT(lz_merchant_barco) lz_merchant_barco,
        COUNT(WITHOUT_TAG) WITHOUT_TAG,
        COUNT(WITH_TAG) WITH_TAG,
        COUNT(WITHOUT_PIC) WITHOUT_PIC,
        COUNT(WITH_PIC) WITH_PIC
   FROM (SELECT (b.barcode_status) barcode_status,
                case
                  when b.barcode_status = 1 then
                   'SHIPPED'
                  else
                   ''
                end SHIPPED,
                case
                  when b.barcode_status = 2 then
                   'SOLD'
                  else
                   ''
                end SOLD,
                case
                  when b.barcode_status = 3 then
                   'LISTED'
                  else
                   ''
                end LISTED,
                case
                  when b.barcode_status = 4 then
                   'POSTED_NOT_LISTED'
                  else
                   ''
                end POSTED_NOT_LISTED,
                case
                  when b.barcode_status = 5 then
                   'UNPOSTED'
                  else
                   ''
                end UNPOSTED,
                case
                  when b.barcode_status = 6 then
                   'UN_POSTED'
                  else
                   ''
                end UN_POSTED,
                case
                  when b.barcode_status = 7 then
                   'lz_merchant_barco'
                  else
                   ''
                end lz_merchant_barco,
                case
                  when dd.tag_id IS NULL then
                   'WITHOUT_TAG'
                  else
                   ''
                end WITHOUT_TAG,
                case
                  when dd.tag_id IS NOT NULL then
                   'WITH_TAG'
                  else
                   ''
                end WITH_TAG,
                case
                 when get_picture.pic_url IS NULL then
                  'WITHOUT_PIC'
                 else
                  ''
               end WITHOUT_PIC,
               case
                 when get_picture.pic_url IS NOT NULL then
                  'WITH_PIC'
                 else
                  ''
               end WITH_PIC
        FROM LJ_BARCODE_LOG_MT      B,
          (select c.barcode_no, pt.NEW_PATH || c.pic_url pic_url

                  from lz_barcode_pic c,
                       lz_pict_path_config pt,
                       (select p.barcode_no, min(p.pic_id) pic_id
                          from lz_barcode_pic p
                         group by p.barcode_no) pp
                 where pp.pic_id = c.pic_id ";
        /*   if ($picFilter == 'WITHOUT_PIC') {
        $data .= " AND c.pic_url IS NULL ";
        } else if ($picFilter == 'WITH_PIC') {
        $data .= " AND c.pic_url IS NOT NULL ";
        } */
        $data .= "  and c.path_id = pt.path_id) get_picture,
        BIN_MT                 BIN,
        EMPLOYEE_MT            EM,
        lz_merchant_barcode_dt dd,
        lz_merchant_barcode_mt dm,
        LZ_ITEM_SEED           S,
        lz_barcode_mt          bb,
        lz_merchant_mt         MM
        WHERE B.BIN_ID = BIN.BIN_ID(+)
        AND B.INSERTED_BY = EM.EMPLOYEE_ID
        and b.barcode_no = dd.barcode_no
        and dd.mt_id = dm.mt_id
        and dm.merchant_id = MM.MERCHANT_ID
        and b.barcode_no = bb.barcode_no(+)
        AND bb.ITEM_ID = S.ITEM_ID(+)
        AND bb.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+)
        AND bb.CONDITION_ID = S.DEFAULT_COND(+)
        AND TO_CHAR(B.BARCODE_NO) = get_picture.barcode_no(+)
        AND MM.MERCHANT_ID = '$merchant_id'";
        /*  if ($listFilter == 'NOT_LISTED') {
        $data .= " AND b.barcode_status = 4 ";

        } else if ($listFilter == 'SHIPPED') {
        $data .= " AND b.barcode_status = 1 ";
        } else if ($listFilter == 'SOLD') {
        $data .= " AND b.barcode_status = 2 ";
        }
        if ($tagFilter == 'WITH_TAG') {
        $data .= " And dd.tag_id IS NOT NULL ";
        } else if ($tagFilter == 'WITHOUT_TAG') {
        $data .= " And dd.tag_id IS NULL ";
        }*/

        $data .= " and b.log_id in
        (SELECT MAX(LOG_ID) FROM LJ_BARCODE_LOG_MT group by barcode_no)";

        if ((!$get_all)) {
            $data .= " AND b.INSERTED_DATE between
                TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
                TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') ";
        }

        $data .= " order by b.barcode_no desc)";
        // $data .= " ORDER BY MT.LOG_ID DESC";
        $query = $this->db->query($data);
        return array('status' => true, 'data' => $query->result_array());
    }
    public function Get_Audit_Barcode_detail()
    {
        $startDate = $_GET['startDate'];
        $endDate = $_GET['endDate'];
        $merchant_id = $_GET['merchant_id'];
        $listFilter = $_GET['listFilterSer'];
        $picFilter = $_GET['picFilterSer'];
        $tagFilter = $_GET['tagFilterSer'];
        $get_all = isset($_GET['get_all']);
        $requestData = $_REQUEST;

        $searchValue = strtoupper($requestData['search']['value']);
        $searchValue = trim(str_replace("  ", ' ', $searchValue));
        $searchValue = str_replace(array("`,'"), "", $searchValue);
        $searchValue = str_replace(array("'"), "''", $searchValue);

        $str = explode(' ', $searchValue);

        $columns = array(
            // datatable column index  => database column name
            0 => 'BARCODE_NO',
            1 => 'BARCODE_NO',
            2 => 'BARCODE_NO',
            3 => 'STATUS_DESC',
            4 => 'BIN_NAME',
            5 => 'ITEM_TITLE',
            6 => 'F_UPC',
            7 => 'F_MPN',
            8 => 'F_MANUFACTURE',
            9 => 'OTHER_NOTES',
            10 => 'ADMIN_REMARKS',
            11 => 'EBAY_PRICE',
            12 => 'MERCHANT_NAME',
            13 => 'INSERTED_DATE',
            14 => 'TAG_ID',
        );

        $data = "SELECT /*b.entry_point,*/
b.barcode_status,
decode(b.barcode_status,
       1,
       'SHIPPED',
       2,
       'SOLD',
       3,
       'LISTED',
       4,
       'POSTED_NOT_LISTED',
       5,
       'UNPOSTED',
       6,
       'UNPOSTED',
       7,
       'lz_merchant_barco',
       'not found') STATUS_DESC,

B.BIN_ID,
Bin.BIN_TYPE || '-' || Bin.BIN_NO BIN_NAME,
S.ITEM_TITLE,
S.F_UPC,
S.F_MANUFACTURE,
S.F_MPN,
S.OTHER_NOTES,
S.EBAY_PRICE,
S.SEED_ID,
B.BARCODE_NO,
b.INSERTED_DATE,
mm.MERCHANT_ID,
MM.CONTACT_PERSON MERCHANT_NAME,
bb.ADMIN_REMARKS,
get_picture.pic_url THUMB_URL,
/*'http://71.78.236.20/item_pictures/master_pictures/image_not_available.jpg' THUMB_URL,
REPLACE(get_picture.thumb_img_url, 'D:/wamp/www/', '') THUMB_URL*/
dd.tag_id
FROM LJ_BARCODE_LOG_MT      B,
  (select c.barcode_no, pt.NEW_PATH || c.pic_url pic_url
          from lz_barcode_pic c,
               lz_pict_path_config pt,
               (select p.barcode_no, min(p.pic_id) pic_id
                  from lz_barcode_pic p
                 group by p.barcode_no) pp
         where pp.pic_id = c.pic_id and c.path_id = pt.path_id) get_picture,
BIN_MT                 BIN,
EMPLOYEE_MT            EM,
lz_merchant_barcode_dt dd,
lz_merchant_barcode_mt dm,
LZ_ITEM_SEED           S,
lz_barcode_mt          bb,
lz_merchant_mt         MM
WHERE B.BIN_ID = BIN.BIN_ID(+)
AND B.INSERTED_BY = EM.EMPLOYEE_ID
and b.barcode_no = dd.barcode_no
and dd.mt_id = dm.mt_id
and dm.merchant_id = MM.MERCHANT_ID
and b.barcode_no = bb.barcode_no(+)
AND bb.ITEM_ID = S.ITEM_ID(+)
AND bb.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+)
AND bb.CONDITION_ID = S.DEFAULT_COND(+)
AND TO_CHAR(B.BARCODE_NO) = get_picture.barcode_no(+)
AND MM.MERCHANT_ID = '$merchant_id'
AND B.ENTRY_POINT = 'LZ Purchase'";
        if ($listFilter == 'NOT_LISTED') {
            $data .= " AND b.barcode_status = 4 ";

        } else if ($listFilter == 'SHIPPED') {
            $data .= " AND b.barcode_status = 1 ";
        } else if ($listFilter == 'SOLD') {
            $data .= " AND b.barcode_status = 2 ";
        }
        if ($tagFilter == 'WITH_TAG') {
            $data .= " And dd.tag_id IS NOT NULL ";
        } else if ($tagFilter == 'WITHOUT_TAG') {
            $data .= " And dd.tag_id IS NULL ";
        }
        if ($picFilter == 'WITHOUT_PIC') {
            $data .= " AND get_picture.pic_url IS NULL ";
        } else if ($picFilter == 'WITH_PIC') {
            $data .= " AND get_picture.pic_url IS NOT NULL ";
        }
        $data .= " and b.log_id in
(SELECT MAX(LOG_ID) FROM LJ_BARCODE_LOG_MT where ENTRY_POINT = 'LZ Purchase' group by barcode_no)";
        if ((!$get_all)) {
            $data .= " AND b.INSERTED_DATE between
        TO_DATE('$startDate " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
        TO_DATE('$endDate " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') ";
        }

        //$data .= " order by b.barcode_no desc";
        $data .= " ORDER BY b.LOG_ID DESC";
        $query = $this->db->query($data);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;

        $sqlLength = "SELECT  * FROM (SELECT  q.*, rownum rn FROM  ($data) q )";

        $sqlLength .= " WHERE   ROWNUM <= " . $requestData['length'] . " AND rn>= " . $requestData['start'];

        if (!empty($columns[$requestData['order'][0]['column']])) {
            $sqlLength .= " ORDER BY  " . $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        }
        $response = $this->db->query($sqlLength);
        $data = array();
        if ($response->num_rows() > 0) {
            $response = $response->result_array();
            $serverSideData = '';
            if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
                $link = "https";
            } else {
                $link = "http";
            }

            // Here append the common URL characters.
            $link .= "://";

            // Append the host(domain name, ip) to the URL.
            $link .= $_SERVER['HTTP_HOST'];
            // var_dump($link);
            foreach ($response as $key => $value) {
                $list_price = '$ ' . number_format((float) @$value['EBAY_PRICE'], 2, '.', '');
                $data[] = [
                    "<button  class='btn btn-warning btn-xs' style='margin: 3px;' id='auditDiscard' title='Discard'
        data-toggle='modal'
        data-target='#myModalEdit'
                    value=" . $value['BARCODE_NO'] . ">
                    <span class='glyphicon glyphicon-edit p-b-5'></span>
                   </button>
                   <a href=" . $link . ":5000/varify?" . @$value['BARCODE_NO'] . " target='_blank' class='btn btn-info btn-xs' role='button'>
                    <span class='glyphicon glyphicon-saved p-b-5'></span>
                   </a>
                   <input type='hidden' id='auditUPC" . $value['BARCODE_NO'] . "' value=" . $value['F_UPC'] . ">
                   <input type='hidden' id='auditMPN" . $value['BARCODE_NO'] . "' value=" . $value['F_MPN'] . ">
                   <input type='hidden' id='auditMN" . $value['BARCODE_NO'] . "' value=" . $value['MERCHANT_NAME'] . ">
                   <input type='hidden' id='auditSeed" . $value['BARCODE_NO'] . "' value=" . $value['SEED_ID'] . ">

                   ",
                    "<a href=https://barcode.tec-it.com/barcode.ashx?data=" . $value['BARCODE_NO'] . " target='_blank'>" .
                    $value['BARCODE_NO'] . "</a>
                    <br/>",
                    "<img src=" . $value['THUMB_URL'] . " alt='Image_Not_Found' style='height: 57px;width: 70px'>",
                    $value['STATUS_DESC'],
                    $value['BIN_NAME'],
                    $value['ITEM_TITLE'],
                    $value['F_UPC'],
                    $value['F_MPN'],
                    $value['F_MANUFACTURE'],
                    "<textarea rows='2'
                    cols='10' id='auditOtherRemarks" . $value['BARCODE_NO'] . "' style='resize:none'
                    >" . $value['OTHER_NOTES'] . "</textarea>
                    <button class='btn btn-warning btn-xs' id='updateAuditRemarks' value=" . $value['BARCODE_NO'] . "> Update </button>",
                    $value['ADMIN_REMARKS'],
                    @$list_price,
                    $value['MERCHANT_NAME'],
                    $value['INSERTED_DATE'],
                    $value['TAG_ID'],

                ];
                //   array_push($serverSideData,$array);

            }

            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered),
                // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // $images = $this->get_pictures($data->result_array());
            // return array('status' => true, 'data' => $serverSideData, 'images' => array());
        } else {
            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered),
                // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('status' => false, 'data' => $data->result_array(), 'images' => array());
        }
    }

    public function Update_Lister_Remarks_Audit_Barcode()
    {
        $user_id = $this->input->post('user_id');
        $lister_remarks = $this->input->post('lister_remarks');
        $lister_remarks = trim(str_replace("  ", ' ', $lister_remarks));
        $lister_remarks = str_replace(array("`,'"), "", $lister_remarks);
        $lister_remarks = str_replace(array("'"), "''", $lister_remarks);
        $data = $this->input->post('data');
        $barcode_no = $data['barcodNo'];

        $update = $this->db->query("UPDATE LZ_ITEM_SEED SET ENTERED_BY = '$user_id' , DATE_TIME = sysdate, OTHER_NOTES = '$lister_remarks' WHERE SEED_ID = (SELECT S.SEED_ID FROM LZ_BARCODE_MT B , LZ_ITEM_SEED S
WHERE B.ITEM_ID = S.ITEM_ID
AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID
AND B.CONDITION_ID = S.DEFAULT_COND
AND B.BARCODE_NO = '$barcode_no'
FETCH FIRST 1 ROW ONLY)");
$update = $this->db->query("UPDATE LZ_SPECIAL_LOTS L SET L.LOT_REMARKS = '$lister_remarks' where l.barcode_prv_no = '$barcode_no'");
$update = $this->db->query("UPDATE LZ_DEKIT_US_DT D SET D.IDENT_REMARKS = '$lister_remarks' where d.barcode_prv_no = '$barcode_no'");
$update = $this->db->query("UPDATE LZ_PURCH_ITEM_MT M SET M.LISTER_REMARKS = '$lister_remarks' , m.remarks_date = sysdate, m.remarks_by='$user_id'
where m.purch_mt_id = (select d.purch_mt_id from lz_purch_item_dt d where d.barcode_no = '$barcode_no' )");
        if ($update == true) {
            return array('status' => true, 'message' => 'Remarks Updated Successfully');
        } else {
            return array('status' => false, 'message' => 'Remarks Updated Failed');
        }
    }



    // public function Update_Lister_Remarks_Audit_Barcode()
    // {
    //     $user_id = $this->input->post('user_id');
    //     $lister_remarks = $this->input->post('lister_remarks');
    //     $lister_remarks = trim(str_replace("  ", ' ', $lister_remarks));
    //     $lister_remarks = str_replace(array("`,'"), "", $lister_remarks);
    //     $lister_remarks = str_replace(array("'"), "''", $lister_remarks);
    //     $data = $this->input->post('data');
    //     $seed_id = $data['SEED_ID'];
    //     if (!empty($seed_id)) {
    //         $update = $this->db->query("UPDATE LZ_ITEM_SEED SET ENTERED_BY = '$user_id' , DATE_TIME = sysdate, OTHER_NOTES = '$lister_remarks' WHERE SEED_ID = '$seed_id'");

    //         if ($update == true) {
    //             return array('status' => true, 'message' => 'Remarks Updated Successfully');
    //         } else {
    //             return array('status' => false, 'message' => 'Remarks Updated Failed');
    //         }
    //     } else {
    //         return array('status' => false, 'message' => 'Remarks Updated Failed Because Item Is Not Posted');
    //     }

    // }

    public function Dicard_Item_Remarks()
    {
        $remarks = $this->input->post('discard_remarks');
        $user_id = $this->input->post('user_id');
        $tag_id = $this->input->post('tag_id');
        $barcode = trim($this->input->post('barcode'), " ");

        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = str_replace(array("`,'"), "", $remarks);
        $remarks = str_replace(array("'"), "''", $remarks);

        $get_barcode = $this->db->query("select * from lz_barcode_mt b where b.barcode_no = '$barcode'")->row();
        if ($get_barcode) {
            $item_id = $get_barcode->ITEM_ID;
            $manifest_id = $get_barcode->LZ_MANIFEST_ID;
            $condition_id = $get_barcode->CONDITION_ID;

            $list_qty = $this->db->query("SELECT COUNT(1) QTY
            FROM LZ_BARCODE_MT BC
           WHERE BC.CONDITION_ID IS NOT NULL
             AND BC.LZ_MANIFEST_ID = $manifest_id
             AND BC.ITEM_ID = $item_id
             AND BC.CONDITION_ID = $condition_id
           GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID");

            if ($list_qty->num_rows() > 1) {
                $qry = $this->db->query("UPDATE lz_merchant_barcode_dt d set d.tag_id ='$tag_id' , d.tag_remarks = '$remarks', d.tag_date = sysdate ,d.tag_by = '$user_id' where d.barcode_no in (
                    select b.barcode_no from lz_barcode_mt b where b.item_id ='$item_id' and b.lz_manifest_id = '$manifest_id' and b.condition_id = '$condition_id'
                    and b.order_id is null
                    and b.extendedorderid is null
                    and b.sale_record_no is null
                )");

            } else {

                $qry = $this->db->query("UPDATE lz_merchant_barcode_dt d set d.tag_id ='$tag_id' , d.tag_remarks = '$remarks', d.tag_date = sysdate ,d.tag_by = '$user_id' where d.barcode_no in ('$barcode' )");

            }
            if ($qry == true) {
                return array("status" => true, 'message' => "Tag Remarks Updated Successfully");
            } else {
                return array("status" => false, 'message' => "Tag Remarks Not Updated Successfully");
            }
        } else {
            return array("status" => false, 'message' => "Tag Remarks Not Updated Successfully Beacuse Item Is Not Posted");
        }

    }

    public function Get_Tages()
    {
        $get_tag_data = $this->db->query('SELECT tag.tag_id,tag.tag_desc from lj_barcode_tag_mt tag  where tag.active_yn = 1')->result_array();
        return array('status' => true, 'data' => $get_tag_data);
    }
}
