<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
} 

/**
 * Single Entry Model
 */
class m_react extends CI_Model
{
  
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function login()
    {

        $userName = strtoupper($this->input->post('userName'));
        $passWord = $this->input->post('passWord');

        $query = $this->db->query("SELECT M.EMPLOYEE_ID,M.USER_NAME,M.LOCATION,(select MT.BUISNESS_NAME from emp_merchant_det DE, lz_merchant_mt MT WHERE DE.MERCHANT_ID = MT.MERCHANT_ID AND DE.EMPLOYEE_ID = M.EMPLOYEE_ID AND ROWNUM <= 1) MER_NAME, (select MT.MERCHANT_ID from emp_merchant_det DE, lz_merchant_mt MT WHERE DE.MERCHANT_ID = MT.MERCHANT_ID AND DE.EMPLOYEE_ID = M.EMPLOYEE_ID AND ROWNUM <= 1) MER_id FROM  EMPLOYEE_MT M WHERE upper(USER_NAME) = '$userName' AND PASS_WORD ='$passWord' AND STATUS =1 ")->result_array();

        if (count($query) == 1) {

            return array('query' => $query, 'login_true' => true);
            //return TRUE;
        } else {
            $query = null;
            return array('query' => $query, 'login_true' => false);
        }

        // return  $userName + $passWord;
    }

    public function getMacro()
    {
        $type_id = $this->input->post('type_id');

        $get_macro = $this->db->query("SELECT * FROM LZ_MACRO_MT WHERE TYPE_ID = $type_id ORDER BY MACRO_ORDER ASC")->result_array();
        return $get_macro;
    }

    public function get_cond_desc()
    {
        $cond_id = $this->input->post('cond_id');
        $get_cond_desc = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT M WHERE M.ID ='$cond_id' ")->result_array();

        if ($get_cond_desc) {
            return array('get_cond_desc' => $get_cond_desc);

        }

    }

    public function get_obj_drop()
    {
        $get_obj = $this->db->query("SELECT O.OBJECT_ID, O.OBJECT_NAME, O.CATEGORY_ID FROM LZ_BD_CAT_GROUP_MT M, LZ_BD_CAT_GROUP_DET D, LZ_BD_OBJECTS_MT O WHERE M.LZ_BD_GROUP_ID = D.LZ_BD_GROUP_ID AND M.LZ_BD_GROUP_ID = O.LZ_BD_GROUP_ID AND D.LZ_BD_GROUP_ID = O.LZ_BD_GROUP_ID AND D.CATEGORY_ID = O.CATEGORY_ID AND M.LZ_BD_GROUP_ID = 7 ");

        if ($get_obj) {
            $get_obj_itm = $get_obj->result_array();
            return array('get_obj_itm' => $get_obj_itm);
        } else {
            return null;
        }

    }

    public function get_obj_drop_sugestion()
    {
        // $get_obj = $this->db->query("SELECT O.OBJECT_ID, O.OBJECT_NAME NICKNAME, O.CATEGORY_ID EMAIL FROM LZ_BD_CAT_GROUP_MT M, LZ_BD_CAT_GROUP_DET D, LZ_BD_OBJECTS_MT O WHERE M.LZ_BD_GROUP_ID = D.LZ_BD_GROUP_ID AND M.LZ_BD_GROUP_ID = O.LZ_BD_GROUP_ID AND D.LZ_BD_GROUP_ID = O.LZ_BD_GROUP_ID AND D.CATEGORY_ID = O.CATEGORY_ID AND M.LZ_BD_GROUP_ID = 7 ");

        // $get_obj = $this->db->query("SELECT OB.OBJECT_NAME NICKNAME,OB.CATEGORY_ID EMAIL FROM LZ_BD_OBJECTS_MT OB WHERE ROWNUM <=1000 order by OB.OBJECT_ID desc");

        $get_obj = $this->db->query("SELECT O.OBJECT_ID, O.OBJECT_NAME NICKNAME, O.CATEGORY_ID  EMAIL FROM LZ_BD_CAT_GROUP_MT M, LZ_BD_CAT_GROUP_DET D, LZ_BD_OBJECTS_MT O WHERE M.LZ_BD_GROUP_ID = D.LZ_BD_GROUP_ID AND M.LZ_BD_GROUP_ID = O.LZ_BD_GROUP_ID AND D.LZ_BD_GROUP_ID = O.LZ_BD_GROUP_ID AND D.CATEGORY_ID = O.CATEGORY_ID AND M.LZ_BD_GROUP_ID = 7 ");

        return $get_obj->result();
        exit;

        // if ($get_obj) {
        //     $get_obj_itm = $get_obj->result_array();
        //     return $get_obj_itm; //array('get_obj_itm' => $get_obj_itm);
        // } else {
        //     return null;
        // }

    }

    public function get_unposted_item()
    {
        $query = $this->db->query("SELECT * FROM (SELECT 'DEKIT ITEM' BAROCDE_TYPE, DECODE(D.LZ_MANIFEST_DET_ID, NULL, 'PENDING', 'POSTED') VERIFY_ITEM, D.LZ_DEKIT_US_DT_ID, D.BARCODE_PRV_NO, BB.BARCODE_NO, BB.EBAY_ITEM_ID, C.COND_NAME, O.OBJECT_NAME, D.PIC_DATE_TIME, D.PIC_BY, D.PIC_NOTES, DECODE(D.MPN_DESCRIPTION, NULL, CA.MPN_DESCRIPTION, D.MPN_DESCRIPTION) MPN_DESC, CA.MPN, CA.UPC, CA.BRAND, TO_CHAR(D.FOLDER_NAME) FOLDER_NAME, D.DEKIT_REMARKS REMARKS, D.LZ_MANIFEST_DET_ID FROM LZ_DEKIT_US_DT   D, LZ_BARCODE_MT BB, LZ_BD_OBJECTS_MT O, LZ_CATALOGUE_MT  CA, LZ_ITEM_COND_MT  C WHERE D.OBJECT_ID = O.OBJECT_ID(+) AND D.CATALOG_MT_ID = CA.CATALOGUE_MT_ID(+) AND D.CONDITION_ID = C.ID(+) AND D.BARCODE_PRV_NO = BB.BARCODE_NO(+) UNION ALL SELECT 'SPECIAL LOT' BAROCDE_TYPE, DECODE(L.LZ_MANIFEST_DET_ID, NULL, 'PENDING', 'POSTED') VERIFY_ITEM, L.SPECIAL_LOT_ID, L.BARCODE_PRV_NO, BB.BARCODE_NO, BB.EBAY_ITEM_ID, C.COND_NAME, OB.OBJECT_NAME, L.PIC_DATE_TIME, L.PIC_BY, L.PIC_NOTES, DECODE(L.MPN_DESCRIPTION, NULL, CA.MPN_DESCRIPTION, L.MPN_DESCRIPTION) MPN_DESC, DECODE(L.CARD_MPN, NULL, CA.MPN, L.CARD_MPN) MPN, DECODE(L.CARD_UPC, NULL, CA.UPC, L.CARD_UPC) UPC, DECODE(L.BRAND, NULL, CA.BRAND, L.BRAND) BRAND, TO_CHAR(L.FOLDER_NAME) FOLDER_NAME, L.LOT_REMARKS REMARKS, L.LZ_MANIFEST_DET_ID FROM LZ_SPECIAL_LOTS  L, LZ_BD_OBJECTS_MT OB, LZ_ITEM_COND_MT  C, LZ_CATALOGUE_MT  CA, LZ_BARCODE_MT BB WHERE L.OBJECT_ID = OB.OBJECT_ID(+) AND L.CONDITION_ID = C.ID(+) AND L.CATALOG_MT_ID = CA.CATALOGUE_MT_ID(+) AND L.BARCODE_PRV_NO = BB.BARCODE_NO(+) /*ORDER BY LZ_MANIFEST_DET_ID  NULLS FIRST*/) WHERE ROWNUM <= 150   and barcode_prv_no in (127758,127645,127834,127640,127835,127715)
          order by barcode_prv_no asc /*and barcode_prv_no = 126153*/ ");

        if ($query) {
            $get_items = $query->result_array();
            return array('get_items' => $get_items);
        } else {
            return null;
        }
    }

    public function add_custom_specifics()
    {
        $cat_id = $this->input->post('cat_id');
        $custom_name = ucfirst($this->input->post('custom_name'));
        $custom_name = trim(str_replace("  ", ' ', $custom_name));
        $custom_name = trim(str_replace(array("'"), "''", $custom_name));

        $custom_value = ucfirst($this->input->post('custom_value'));
        $custom_value = trim(str_replace("  ", ' ', $custom_value));
        $custom_value = trim(str_replace(array("'"), "''", $custom_value));

        $selectionMode = 'FreeText';

        $catalogue_only = 0;

        $maxValue = 1;

        $comma = ',';
        date_default_timezone_set("America/Chicago");
        $current_date = date("Y-m-d H:i:s");
        $current_date = "TO_DATE('" . $current_date . "', 'YYYY-MM-DD HH24:MI:SS')";

        $query = $this->db->query("SELECT SPECIFIC_NAME FROM CATEGORY_SPECIFIC_MT WHERE SPECIFIC_NAME = '$custom_name' AND EBAY_CATEGORY_ID = $cat_id");
        if ($query->num_rows() > 0) {
            return false;
        } else {

            $get_mt_pk = $this->db->query("SELECT get_single_primary_key('CATEGORY_SPECIFIC_MT','MT_ID') SPECIFICS_MT_ID FROM DUAL");
            $get_mt_pk = $get_mt_pk->result_array();
            $specifics_mt_id = $get_mt_pk[0]['SPECIFICS_MT_ID'];

            $ins_mt_qry = "INSERT INTO CATEGORY_SPECIFIC_MT(MT_ID, EBAY_CATEGORY_ID, SPECIFIC_NAME, MARKETPLACE_ID, MAX_VALUE, MIN_VALUE, SELECTION_MODE, UPDATE_DATE, CUSTOM, CATALOGUE_ONLY) VALUES ($specifics_mt_id $comma $cat_id $comma '$custom_name' $comma 1 $comma $maxValue $comma 1 $comma '$selectionMode' $comma $current_date $comma 1 $comma $catalogue_only)";
            $ins_mt_qry = $this->db->query($ins_mt_qry);

            $get_det_pk = $this->db->query("SELECT get_single_primary_key('CATEGORY_SPECIFIC_DET','DET_ID') SPECIFICS_DET_ID FROM DUAL");
            $get_det_pk = $get_det_pk->result_array();
            $specifics_det_id = $get_det_pk[0]['SPECIFICS_DET_ID'];

            $ins_det_qry = "INSERT INTO CATEGORY_SPECIFIC_DET VALUES ($specifics_det_id $comma $specifics_mt_id $comma '$custom_value')";
            $ins_det_qry = $this->db->query($ins_det_qry);
        }
    }

    public function add_specifics()
    {
        $item_id = $this->input->post('item_id');
        $cat_id = $this->input->post('cat_id');
        $spec_name = $this->input->post('spec_name');

        $spec_value = $this->input->post('spec_value');
        //var_dump($spec_value);exit;
        $item_upc = $this->input->post('item_upc');
        $item_mpn = $this->input->post('item_mpn');
        $user_id = 2; // $this->session->userdata('user_id');
        date_default_timezone_set("America/Chicago");
        $current_date = date("Y-m-d H:i:s");
        $current_date = "TO_DATE('" . $current_date . "', 'YYYY-MM-DD HH24:MI:SS')";
        $comma = ',';

        $qry_det = $this->db->query("DELETE FROM LZ_ITEM_SPECIFICS_DET D WHERE D.SPECIFICS_MT_ID IN (SELECT S.SPECIFICS_MT_ID FROM LZ_ITEM_SPECIFICS_MT S WHERE S.CATEGORY_ID = $cat_id AND S.ITEM_ID = $item_id)");

        $qry_mt = $this->db->query("DELETE FROM  LZ_ITEM_SPECIFICS_MT S WHERE S.CATEGORY_ID =  $cat_id AND S.ITEM_ID = $item_id");

        $i = 0;
        foreach ($spec_name as $name) {

            $name = trim(str_replace("  ", ' ', $name));
            $name = trim(str_replace(array("'"), "''", $name));
            if (is_array(@$spec_value[$i])) {
                $value = "ok";
                //var_dump(@$spec_value[$i]);
            } else {
                $value = @$spec_value[$i];
                //var_dump(@$spec_value[$i]);
            }

            if ($value != 'select' && !empty($value)) {
                //var_dump($value);
                // if(!empty($item_upc)){
                //   $query = $this->db->query("SELECT SPECIFICS_MT_ID from LZ_ITEM_SPECIFICS_MT WHERE SPECIFICS_NAME = '$name' AND CATEGORY_ID=$cat_id AND UPC = '$item_upc'");
                //     $rs = $query->result_array();
                // }elseif(!empty($item_mpn)){
                //   $query = $this->db->query("SELECT SPECIFICS_MT_ID from LZ_ITEM_SPECIFICS_MT WHERE SPECIFICS_NAME = '$name' AND CATEGORY_ID=$cat_id AND upper(MPN) = upper('$item_mpn')");
                //     $rs = $query->result_array();
                // }else{
                //   return false;
                //   exit;
                // }

                $get_mt_pk = $this->db->query("SELECT get_single_primary_key('LZ_ITEM_SPECIFICS_MT','SPECIFICS_MT_ID') SPECIFICS_MT_ID FROM DUAL");
                $get_pk = $get_mt_pk->result_array();
                $specifics_mt_id = $get_pk[0]['SPECIFICS_MT_ID'];
                $ins_mt_qty = "INSERT INTO LZ_ITEM_SPECIFICS_MT VALUES ($specifics_mt_id $comma $item_id $comma $cat_id $comma $user_id $comma $current_date $comma '$name' $comma '$item_mpn' $comma '$item_upc')";
                $ins_mt_qty = $this->db->query($ins_mt_qty);
                //}
                if (is_array(@$spec_value[$i])) {
                    foreach (@$spec_value[$i] as $val) {
                        //var_dump(@$spec_value[$i]);
                        $specific_value = trim(str_replace("  ", ' ', $val));
                        $specific_value = trim(str_replace(array("'"), "''", $specific_value));

                        if (!empty($specific_value)) {
                            $get_det_pk = $this->db->query("SELECT get_single_primary_key('LZ_ITEM_SPECIFICS_DET','SPECIFICS_DET_ID') SPECIFICS_DET_ID FROM DUAL");
                            $get_det_pk = $get_det_pk->result_array();
                            $specifics_det_id = $get_det_pk[0]['SPECIFICS_DET_ID'];
                            $ins_det_qty = "INSERT INTO LZ_ITEM_SPECIFICS_DET VALUES ($specifics_det_id $comma $specifics_mt_id $comma '$specific_value')";
                            $ins_det_qty = $this->db->query($ins_det_qty);
                        }
                    }

                } else {
                    // if(!empty(specific_valueue)){
                    $specific_value = trim(str_replace("  ", ' ', $value));
                    $specific_value = trim(str_replace(array("'"), "''", $specific_value));
                    $get_det_pk = $this->db->query("SELECT get_single_primary_key('LZ_ITEM_SPECIFICS_DET','SPECIFICS_DET_ID') SPECIFICS_DET_ID FROM DUAL");
                    $get_det_pk = $get_det_pk->result_array();
                    $specifics_det_id = $get_det_pk[0]['SPECIFICS_DET_ID'];
                    $ins_det_qty = "INSERT INTO LZ_ITEM_SPECIFICS_DET VALUES ($specifics_det_id $comma $specifics_mt_id $comma '$specific_value')";
                    $ins_det_qty = $this->db->query($ins_det_qty);
                    // }
                }
                //}//end main ifelse
            } // end main if
            $i++;
        } //end main foreach
    }

    public function queryData()
    {

        $perameter = $this->input->post('barocde');
        $item_qry = $this->db->query("select m.item_id,m.lz_manifest_id,m.condition_id from lz_barcode_mt m where m.barcode_no=$perameter");
        $item_data = $item_qry->result_array();

        if ($item_qry->num_rows() > 0) {
            $item_det = $this->db->query("SELECT B.UNIT_NO, B.BARCODE_NO IT_BARCODE, D.CONDITIONS_SEG5     ITEM_CONDITION, D.ITEM_MT_DESC, I.ITEM_MT_MANUFACTURE MANUFACTURER, I.ITEM_MT_MFG_PART_NO MFG_PART_NO, D.ITEM_MT_UPC         UPC, D.AVAILABLE_QTY       AVAIL_QTY, B.ITEM_ID, B.LZ_MANIFEST_ID, M.PURCH_REF_NO,  B.CONDITION_ID FROM LZ_BARCODE_MT B, LZ_MANIFEST_DET D, ITEMS_MT I, LZ_MANIFEST_MT M,LZ_ITEM_COND_MT C WHERE B.ITEM_ID = I.ITEM_ID AND B.LZ_MANIFEST_ID = D.LZ_MANIFEST_ID AND D.LAPTOP_ITEM_CODE = I.ITEM_CODE AND M.LZ_MANIFEST_ID = D.LZ_MANIFEST_ID
   AND  UPPER(TRIM(D.CONDITIONS_SEG5)) =UPPER(C.COND_NAME(+)) AND B.LZ_MANIFEST_ID=" . $item_data[0]['LZ_MANIFEST_ID'] . " AND B.ITEM_ID = " . $item_data[0]['ITEM_ID'] . " ORDER BY B.UNIT_NO");
            $item_det = $item_det->result_array();
        }

        // $cat_id = $this->db->query("SELECT DISTINCT DT.E_BAY_CATA_ID_SEG6,DT.BRAND_SEG3 FROM LZ_MANIFEST_DET DT WHERE DT.LAPTOP_ITEM_CODE = (SELECT V.LAPTOP_ITEM_CODE FROM VIEW_LZ_LISTING_REVISED V WHERE V.LZ_MANIFEST_ID = ".$item_det[0]['LZ_MANIFEST_ID']." AND V.ITEM_ID = ".$item_det[0]['ITEM_ID']." AND ROWNUM = 1) AND DT.E_BAY_CATA_ID_SEG6 NOT IN ('N/A', 'Other', 'OTHER', 'other')");

        if (!empty($item_data[0]['CONDITION_ID'])) {
            $condition_id = $item_data[0]['CONDITION_ID'];
        } else {
            $condition_id = $item_det[0]['CONDITION_ID'];
        }
        $cat_id = $this->db->query("SELECT S.CATEGORY_ID E_BAY_CATA_ID_SEG6, S.CATEGORY_NAME BRAND_SEG3 FROM LZ_MANIFEST_DET DT, LZ_ITEM_SEED S , ITEMS_MT I WHERE DT.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID AND DT.LAPTOP_ITEM_CODE = I.ITEM_CODE AND I.ITEM_ID = S.ITEM_ID AND S.ITEM_ID = " . $item_data[0]['ITEM_ID'] . " AND S.LZ_MANIFEST_ID = " . $item_data[0]['LZ_MANIFEST_ID'] . " AND S.DEFAULT_COND = " . $condition_id);

        $cat_id = $cat_id->result_array();
        //var_dump($cat_id);exit;

        if (!empty($cat_id[0]['E_BAY_CATA_ID_SEG6'])) {

            //$mt_id = $this->db->query("SELECT * FROM CATEGORY_SPECIFIC_MT T WHERE T.EBAY_CATEGORY_ID = (SELECT DISTINCT DT.E_BAY_CATA_ID_SEG6 FROM LZ_MANIFEST_DET DT WHERE DT.LAPTOP_ITEM_CODE = (SELECT V.LAPTOP_ITEM_CODE FROM VIEW_LZ_LISTING_REVISED V WHERE V.LZ_MANIFEST_ID = ".$item_det[0]['LZ_MANIFEST_ID']." AND V.ITEM_ID = ".$item_det[0]['ITEM_ID']." AND ROWNUM = 1) AND DT.E_BAY_CATA_ID_SEG6 NOT IN ('N/A', 'Other', 'OTHER', 'other') ) ORDER BY T.SPECIFIC_NAME");
            $mt_id = $this->db->query("SELECT * FROM CATEGORY_SPECIFIC_MT T  WHERE T.EBAY_CATEGORY_ID = " . $cat_id[0]['E_BAY_CATA_ID_SEG6'] . "  ORDER BY T.SPECIFIC_NAME");
            $mt_id = $mt_id->result_array();

            if (!empty($mt_id[0]['EBAY_CATEGORY_ID'])) {
                // $specs_qry = $this->db->query("SELECT Q1.EBAY_CATEGORY_ID, Q1.SPECIFIC_NAME, DET.SPECIFIC_VALUE, Q1.MAX_VALUE, Q1.MIN_VALUE, Q1.SELECTION_MODE,Q1.MT_ID FROM (SELECT * FROM CATEGORY_SPECIFIC_MT MT WHERE MT.EBAY_CATEGORY_ID = (SELECT DISTINCT  DT.E_BAY_CATA_ID_SEG6 FROM LZ_MANIFEST_DET DT WHERE DT.LAPTOP_ITEM_CODE = (SELECT V.LAPTOP_ITEM_CODE FROM VIEW_LZ_LISTING_REVISED V WHERE V.LZ_MANIFEST_ID = ".$item_det[0]['LZ_MANIFEST_ID']." AND V.ITEM_ID = ".$item_det[0]['ITEM_ID']." AND ROWNUM = 1) AND DT.E_BAY_CATA_ID_SEG6 NOT IN ('N/A', 'Other', 'OTHER', 'other' ) AND ROWNUM = 1 )) Q1, CATEGORY_SPECIFIC_DET DET WHERE Q1.MT_ID = DET.MT_ID ORDER BY Q1.SPECIFIC_NAME");

                $specs_qry = $this->db->query("SELECT Q1.EBAY_CATEGORY_ID, Q1.SPECIFIC_NAME, DET.SPECIFIC_VALUE, Q1.MAX_VALUE, Q1.MIN_VALUE, Q1.SELECTION_MODE,Q1.MT_ID FROM (SELECT * FROM CATEGORY_SPECIFIC_MT MT WHERE MT.EBAY_CATEGORY_ID = (SELECT * FROM (SELECT I.CATEGORY_ID FROM LZ_ITEM_SEED I WHERE I.LZ_MANIFEST_ID =" . $item_det[0]['LZ_MANIFEST_ID'] . " AND I.ITEM_ID = " . $item_det[0]['ITEM_ID'] . " ORDER BY I.SEED_ID DESC) WHERE ROWNUM <= 1)) Q1, CATEGORY_SPECIFIC_DET DET WHERE Q1.MT_ID = DET.MT_ID ORDER BY Q1.SPECIFIC_NAME");

                $specs_qry = $specs_qry->result_array();

                $match_qry = "SELECT  D.LZ_TEST_MT_ID FROM LZ_TEST_CHECK_MT M, LZ_TEST_CHECK_DET D WHERE M.LZ_TEST_MT_ID = D.LZ_TEST_MT_ID AND M.LZ_TEST_MT_ID IN (SELECT DISTINCT D.LZ_TEST_MT_ID FROM LZ_CHECKLIST_DET D, LZ_CHECKLIST_MT M WHERE D.CHECKLIST_MT_ID = (SELECT B.CHECKLIST_MT_ID FROM LZ_CATEGORY_CHECKLIST_BIND B WHERE B.CATEGORY_ID = " . $mt_id[0]['EBAY_CATEGORY_ID'] . " AND ROWNUM = 1) AND D.CHECKLIST_MT_ID = M.CHECKLIST_MT_ID) ORDER BY D.LZ_TEST_MT_ID";
                $match_qry = $this->db->query($match_qry);
                $match_qry = $match_qry->result_array();

                $specs_value = "SELECT MT.SPECIFICS_NAME, DT.SPECIFICS_VALUE,CM.MT_ID FROM LZ_ITEM_SPECIFICS_MT MT, LZ_ITEM_SPECIFICS_DET DT,category_specific_mt cm WHERE MT.SPECIFICS_MT_ID = DT.SPECIFICS_MT_ID and upper(cm.specific_name) = upper(mt.specifics_name) and cm.ebay_category_id = mt.category_id AND MT.ITEM_ID = " . $item_det[0]['ITEM_ID'] . "AND MT.CATEGORY_ID = " . $mt_id[0]['EBAY_CATEGORY_ID'] . "  ORDER BY MT.SPECIFICS_NAME";
                $specs_value = $this->db->query($specs_value);
                $specs_value = $specs_value->result_array();

                return array('specs_qry' => $specs_qry, 'mt_id' => $mt_id, 'item_det' => $item_det, 'cat_id' => $cat_id, 'specs_value' => $specs_value, 'match_qry' => $match_qry);
                // return array("specs_value" => $specs_value);
            } else {

                $result['category_id'] = $cat_id[0]['E_BAY_CATA_ID_SEG6'];
                //var_dump($cat_id[0]['E_BAY_CATA_ID_SEG6']);exit;
                $this->load->view('ebay/trading/item_specifics', $result);
                // $result['data'] = $this->m_tester_model->queryData($perameter);
                // $this->load->view('tester_screen/v_tester_result', $result);
            }

        } else { //category id if else
            return array('error_msg' => true, 'item_det' => $item_det);
        }
    }
    public function attribute_value($cat_id, $barcode, $item_mpn, $item_upc, $spec_name, $custom_attribute)
    {
        $comma = ',';
        $query = $this->db->query("SELECT SPECIFIC_VALUE FROM CATEGORY_SPECIFIC_DET D WHERE D.MT_ID = (SELECT M.MT_ID FROM CATEGORY_SPECIFIC_MT M WHERE M.EBAY_CATEGORY_ID = $cat_id AND UPPER(M.SPECIFIC_NAME) = UPPER('$spec_name')) AND UPPER(SPECIFIC_VALUE) = UPPER('$custom_attribute')");
        if ($query->num_rows() > 0) {
            return false;
        } else {
            $get_mt_id = $this->db->query("SELECT M.MT_ID FROM CATEGORY_SPECIFIC_MT M WHERE M.EBAY_CATEGORY_ID=$cat_id AND UPPER(M.SPECIFIC_NAME)=UPPER('$spec_name')");
            $get_mt_id = $get_mt_id->result_array();
            $mt_id = $get_mt_id[0]['MT_ID'];

            $get_det_pk = $this->db->query("SELECT get_single_primary_key('CATEGORY_SPECIFIC_DET','DET_ID') SPECIFICS_DET_ID FROM DUAL");
            $get_det_pk = $get_det_pk->result_array();
            $specifics_det_id = $get_det_pk[0]['SPECIFICS_DET_ID'];

            $ins_det_qry = "INSERT INTO CATEGORY_SPECIFIC_DET VALUES ($specifics_det_id $comma $mt_id $comma '$custom_attribute')";
            $ins_det_qry = $this->db->query($ins_det_qry);
            return $ins_det_qry;
        }
    }
    // public function get_pictures(){

    // $barocde_no = $this->input->post('barocde_no');
    //  //var_dump($barocde_no['barcodePass']);
    //   //var_dump($barocde_no);
    //  $bar_val = $barocde_no['barcodePass'] ;

    //  //var_dump($bar_val);
    //   //$condition = $this->input->post('condition');

    // $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
    // $path = $path->result_array();

    // $master_path = $path[0]["MASTER_PATH"];

    // $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

    // $barcode = $qry[0]["FOLDER_NAME"];

    // $dir = $master_path.$barcode."/";//getBarcodePrv_no
    // // $dir = $master_path.$getBarcodePrv_no."/thumb/";
    // // var_dump($dir);exit;
    // $dir = preg_replace("/[\r\n]*/","",$dir);

    // $mdir = $master_path.$barcode."/";
    // //var_dump($dir);exit;
    // $dekitted_pics = [];
    // $parts = [];
    // $uri = [];

    //  //var_dump(is_dir($dir));exit;
    // if (is_dir($dir)){
    //  // var_dump($dir);exit;
    //  $images = glob($dir."\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}",GLOB_BRACE);
    //  $i=0 ;

    //  foreach($images as $image){
    //    $uri[$i] = $image;
    //    $parts = explode(".", $image);
    //    $img_name = explode("/",$image);
    //    // var_dump($img_name);exit;
    //    $img_n = explode(".",$img_name[4]);
    //    $str = preg_replace('/[^A-Za-z0-9\. -]/', '', $img_name[4]);
    //    $new_string = substr($str,0,1) . "_" . substr($str,1,strlen($str)-1);
    //    // var_dump($new_string);exit;
    //    //$cloudUrl[$i] = "https://res.cloudinary.com/ecologix/image/upload/".$barcode.'/'.$new_string;
    //            // var_dump($cloudUrl);exit;
    //    // var_dump($new_string );exit;
    //            if (is_array($parts) && count($parts) > 1){
    //                $extension = end($parts);
    //                if(!empty($extension)){

    //                 // $live_path = $data['path_query'][0]['LIVE_PATH'];
    //                    $url = $parts['0'].'.'.$extension;

    //                    $url = preg_replace("/[\r\n]*/","",$url);

    //                    // var_dump($url);exit;
    //                    $uri[$i] = $url;
    //                     //var_dump($uri[$i]);exit;
    //                    $img = file_get_contents($url);
    //                     //var_dump($img);exit;
    //                    $img =base64_encode($img);

    //                    $dekitted_pics[$i] = $img;

    //                    $i++;
    //                }
    //            }

    //  }
    // }

    // //var_dump($dekitted_pics);exit;
    // return array('dekitted_pics'=>$dekitted_pics,'parts'=>$parts,'uri'=>$uri);

    // }

    public function update_seed()
    {

        $seedUpc = $this->input->post('seedUpc');
        $seedUpc = trim(str_replace("  ", ' ', $seedUpc));
        $seedUpc = trim(str_replace(array("'"), "''", $seedUpc));

        $seedMpn = $this->input->post('seedMpn');
        $seedMpn = trim(str_replace("  ", ' ', $seedMpn));
        $seedMpn = trim(str_replace(array("'"), "''", $seedMpn));

        $s_mpn = $this->input->post('s_mpn');
        $s_mpn = trim(str_replace("  ", ' ', $s_mpn));
        $s_mpn = trim(str_replace(array("'"), "''", $s_mpn));

        $old_upc = $this->input->post('old_upc');
        $old_mpn = $this->input->post('old_mpn');

        $seedBrand = $this->input->post('seedBrand');
        $seedBrand = trim(str_replace("  ", ' ', $seedBrand));
        $seedBrand = trim(str_replace(array("'"), "''", $seedBrand));

        $epId = $this->input->post('epId');
        $epId = trim(str_replace("  ", ' ', $epId));
        $epId = trim(str_replace(array("'"), "''", $epId));

        $ounce = $this->input->post('ounce');
        $ounce = trim(str_replace("  ", ' ', $ounce));
        $ounce = trim(str_replace(array("'"), "''", $ounce));

        $length = $this->input->post('length');
        $length = trim(str_replace("  ", ' ', $length));
        $length = trim(str_replace(array("'"), "''", $length));

        $width = $this->input->post('width');
        $width = trim(str_replace("  ", ' ', $width));
        $width = trim(str_replace(array("'"), "''", $width));

        $height = $this->input->post('height');
        $height = trim(str_replace("  ", ' ', $height));
        $height = trim(str_replace(array("'"), "''", $height));

        $manifestDetId = $this->input->post('manifestDetId');
        $manifestDetId = trim(str_replace("  ", ' ', $manifestDetId));
        $manifestDetId = trim(str_replace(array("'"), "''", $manifestDetId));

        $itemTitle = $this->input->post('itemTitle');
        $itemTitle = trim(str_replace("  ", ' ', $itemTitle));
        $itemTitle = trim(str_replace(array("'"), "''", $itemTitle));

        $itemDesc = $this->input->post('itemDesc');
        //var_dump($itemDesc);
        $itemDesc = trim(str_replace("  ", ' ', $itemDesc));
        $itemDesc = trim(str_replace(array("'"), "''", $itemDesc));
//var_dump($itemDesc);
        $categId = $this->input->post('categId');
        $categId = trim(str_replace("  ", ' ', $categId));
        $categId = trim(str_replace(array("'"), "''", $categId));

        $categName = $this->input->post('categName');
        $categName = trim(str_replace("  ", ' ', $categName));
        $categName = trim(str_replace(array("'"), "''", $categName));

        $defCondDis = $this->input->post('defCondDis');
        $defCondDis = trim(str_replace("  ", ' ', $defCondDis));
        $defCondDis = trim(str_replace(array("'"), "''", $defCondDis));

        $defCond = $this->input->post('defCond');
        $defCond = trim(str_replace("  ", ' ', $defCond));
        $defCond = trim(str_replace(array("'"), "''", $defCond));

        $hidenCond = $this->input->post('hidenCond');

        $price = $this->input->post('price');
        $price = trim(str_replace("  ", ' ', $price));
        $price = trim(str_replace(array("'"), "''", $price));

        $shipServ = $this->input->post('shipServ');
        $shipServ = trim(str_replace("  ", ' ', $shipServ));
        $shipServ = trim(str_replace(array("'"), "''", $shipServ));

        $bin = $this->input->post('bin');

        $editTemp = $this->input->post('editTemp');
        $editTemp = trim(str_replace("  ", ' ', $editTemp));
        $editTemp = trim(str_replace(array("'"), "''", $editTemp));

        $retDay = $this->input->post('retDay');
        $retDay = trim(str_replace("  ", ' ', $retDay));
        $retDay = trim(str_replace(array("'"), "''", $retDay));

        $retAccept = $this->input->post('retAccept');
        $retAccept = trim(str_replace("  ", ' ', $retAccept));
        $retAccept = trim(str_replace(array("'"), "''", $retAccept));

        $otherNote = $this->input->post('otherNote');
        $otherNote = trim(str_replace("  ", ' ', $otherNote));
        $otherNote = trim(str_replace(array("'"), "''", $otherNote));

        date_default_timezone_set("America/Chicago");
        $created_date = date("Y-m-d H:i:s");
        $created_date = "TO_DATE('" . $created_date . "', 'YYYY-MM-DD HH24:MI:SS')";

        $barocde = $this->input->post('barocde');
        $entered_by = $this->input->post('user_id');
        $ebaysite = $this->input->post("ebaysite");
        $bar_query = $this->db->query("SELECT B.LZ_MANIFEST_ID,B.ITEM_ID,B.CONDITION_ID  FROM LZ_BARCODE_MT B WHERE B.BARCODE_NO =$barocde")->result_array();
        $lz_manifest_id = $bar_query[0]['LZ_MANIFEST_ID'];
        $item_id = $bar_query[0]['ITEM_ID'];

        // var_dump($defCond);
        // var_dump($hidenCond);
        $temp = $this->db->query("SELECT I.TEMPLATE_NAME,I.EBAY_LOCAL,I.CURRENCY,I.LIST_TYPE,I.SHIP_FROM_ZIP_CODE,I.SHIP_FROM_LOC,I.PAYMENT_METHOD,I.PAYPAL_EMAIL,I.DISPATCH_TIME_MAX,I.SHIPPING_PAID_BY FROM LZ_ITEM_TEMPLATE I WHERE I.TEMPLATE_ID = $editTemp ")->result_array();

        $ebay_local = $temp[0]['EBAY_LOCAL'];
        $currency = $temp[0]['CURRENCY'];
        $list_type = $temp[0]['LIST_TYPE'];
        $ship_from_zip_code = $temp[0]['SHIP_FROM_ZIP_CODE'];
        $ship_from_loc = $temp[0]['SHIP_FROM_LOC'];
        $payment_method = $temp[0]['PAYMENT_METHOD'];
        $paypal_email = $temp[0]['PAYPAL_EMAIL'];
        $dispatch_time_max = $temp[0]['DISPATCH_TIME_MAX'];
        $shipping_paid_by = $temp[0]['SHIPPING_PAID_BY'];

        $qry = $this->db->query("SELECT COND_NAME FROM LZ_ITEM_COND_MT WHERE ID in ('$defCond')")->result_array();
        $selectd_cond = $qry[0]['COND_NAME'];
        $qry2 = $this->db->query("SELECT COND_NAME FROM LZ_ITEM_COND_MT WHERE ID in ('$hidenCond')")->result_array();
        $default_cond = $qry2[0]['COND_NAME'];

        $querys = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = 1");
        $master_qrys = $querys->result_array();
        $pic_dir = $master_qrys[0]['MASTER_PATH'];

        //$pic_dir = $this->input->post('pic_dir');// old dir
        // $condition_name =$hidenCond;// $this->input->post('hidenCond');

        // var_dump('def'.$defCond,'hid'.$hidenCond);
        // exit;

        if ($defCond !== $hidenCond) {

            $this->db->query("UPDATE LZ_MANIFEST_DET set WEIGHT = '$ounce' where LAPTOP_ZONE_ID = '$manifestDetId'");
            $this->db->query("UPDATE ITEMS_MT I  SET I.ITEM_LENGTH ='$length' ,I.ITEM_WIDTH = '$width', I.ITEM_HEIGHT ='$height'  WHERE I.ITEM_ID ='$item_id'");

            $this->db->query("UPDATE LZ_BARCODE_MT SET CONDITION_ID = $defCond  WHERE ITEM_ID = $item_id AND LZ_MANIFEST_ID = $lz_manifest_id AND CONDITION_ID = $hidenCond AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL");

            // $this->db->query("UPDATE LZ_BARCODE_MT SET ,CONDITION_ID = $defCond  WHERE ITEM_ID = $item_id AND LZ_MANIFEST_ID = $lz_manifest_id AND  CONDITION_ID = $hidenCond AND LIST_ID IS NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND EBAY_ITEM_ID IS NULL");
            //------------------------------------------------------------------------------------------------

            $this->db->query(" UPDATE LZ_ITEM_SEED SET ITEM_ID = $item_id, ITEM_TITLE = '$itemTitle', EBAY_LOCAL= $ebaysite, ITEM_DESC = '$itemDesc', EBAY_PRICE = '$price', TEMPLATE_ID = '$editTemp',CURRENCY = '$currency',LIST_TYPE = '$list_type',CATEGORY_ID = $categId, SHIP_FROM_ZIP_CODE = $ship_from_zip_code,SHIP_FROM_LOC ='$ship_from_loc', DEFAULT_COND = '$defCond',      DETAIL_COND = '$defCondDis',PAYMENT_METHOD='$payment_method',PAYPAL_EMAIL='$paypal_email', BID_LENGTH = NULL, DISPATCH_TIME_MAX=$dispatch_time_max,/*ship cost aditioncost*/ RETURN_OPTION = '$retAccept', RETURN_DAYS = '$retDay',SHIPPING_PAID_BY='$shipping_paid_by',    SHIPPING_SERVICE = '$shipServ',/*qty*/ LZ_MANIFEST_ID = $lz_manifest_id, CATEGORY_NAME = '$categName',/*item sesc long*/ENTERED_BY = '$entered_by',/*aprov date and by*/ DATE_TIME = $created_date,OTHER_NOTES='$otherNote',/*EPID = 1,*/EPID='$epId', F_UPC ='$seedUpc',F_MPN='$seedMpn',F_MANUFACTURE='$seedBrand',S_MPN = '$s_mpn'  WHERE ITEM_ID = $item_id AND LZ_MANIFEST_ID = $lz_manifest_id AND DEFAULT_COND = $defCond");

            // return true ;
            // exit;
            $chek_b = $this->db->query("SELECT * FROM LZ_BARCODE_MT B WHERE B.BARCODE_NO NOT IN (SELECT O.BARCODE_PRV_NO FROM LZ_SPECIAL_LOTS O UNION ALL SELECT D.BARCODE_PRV_NO FROM LZ_DEKIT_US_DT D) AND B.BARCODE_NO = '$barocde' ")->result_array();

            //if(count($chek_b) >= ){
            if (count($chek_b) >= 1) {

                $cond_update = false;

                if ($old_upc !== $seedUpc || $old_mpn !== $seedMpn) { //upc change

                    $old_mpn = str_replace("/", "_", $old_mpn);
                    $old_mpn = str_replace("\\", "_", $old_mpn);
                    $seedMpn = str_replace("/", "_", $seedMpn);
                    $seedMpn = str_replace("\\", "_", $seedMpn);
                    $old_dir = $pic_dir . $old_upc . '~' . $old_mpn; //.'/'.$condition_disc;
                    $new_dir = $pic_dir . $seedUpc . '~' . $seedMpn; //.'/'.$condition_name;

                    // var_dump('old'.$old_dir,'new'.$new_dir);
                    // exit;
                    if (!is_dir(@$new_dir)) {

                        rename(@$old_dir, @$new_dir);
                    }
                    $old_dir = $new_dir . '/' . $default_cond;
                    $new_dir = $new_dir . '/' . $selectd_cond;

                    if (!is_dir(@$new_dir)) {
                        rename(@$old_dir, @$new_dir);
                        $cond_update = true;
                    }
                    //return true;
                }

                if ($cond_update == false) {

                    $old_mpn = str_replace("/", "_", $old_mpn);
                    $old_mpn = str_replace("\\", "_", $old_mpn);
                    $seedMpn = str_replace("/", "_", $seedMpn);
                    $seedMpn = str_replace("\\", "_", $seedMpn);
                    $old_dir = $pic_dir . $seedUpc . '~' . $seedMpn . '/' . $default_cond;
                    $new_dir = $pic_dir . $old_upc . '~' . $old_mpn . '/' . $selectd_cond;

                    if (!is_dir(@$new_dir)) {
                        // var_dump($old_dir);
                        // exit;
                        rename(@$old_dir, @$new_dir);
                    }
                }
            }

            return true;
        } else {

            $this->db->query("UPDATE LZ_MANIFEST_DET set WEIGHT = '$ounce' where LAPTOP_ZONE_ID = '$manifestDetId'");
            $this->db->query("UPDATE ITEMS_MT I  SET I.ITEM_LENGTH ='$length' ,I.ITEM_WIDTH = '$width', I.ITEM_HEIGHT ='$height'  WHERE I.ITEM_ID ='$item_id'");

            $this->db->query(" UPDATE LZ_ITEM_SEED SET ITEM_ID = $item_id, ITEM_TITLE = '$itemTitle', EBAY_LOCAL= $ebaysite, ITEM_DESC = '$itemDesc', EBAY_PRICE = '$price', TEMPLATE_ID = '$editTemp',CURRENCY = '$currency',LIST_TYPE = '$list_type',CATEGORY_ID = $categId, SHIP_FROM_ZIP_CODE = $ship_from_zip_code,SHIP_FROM_LOC ='$ship_from_loc', DEFAULT_COND = '$defCond',      DETAIL_COND = '$defCondDis',PAYMENT_METHOD='$payment_method',PAYPAL_EMAIL='$paypal_email', BID_LENGTH = NULL, DISPATCH_TIME_MAX=$dispatch_time_max,/*ship cost aditioncost*/ RETURN_OPTION = '$retAccept', RETURN_DAYS = '$retDay',SHIPPING_PAID_BY='$shipping_paid_by',   SHIPPING_SERVICE = '$shipServ',/*qty*/ LZ_MANIFEST_ID = $lz_manifest_id, CATEGORY_NAME = '$categName',/*item sesc long*/ENTERED_BY = '$entered_by',/*aprov date and by*/ DATE_TIME = $created_date,OTHER_NOTES='$otherNote',/*EPID = 1,*/EPID='$epId', F_UPC ='$seedUpc',F_MPN='$seedMpn',F_MANUFACTURE='$seedBrand',S_MPN = '$s_mpn'  WHERE ITEM_ID = $item_id AND LZ_MANIFEST_ID = $lz_manifest_id AND DEFAULT_COND = $defCond");

            $chek_b = $this->db->query("SELECT * FROM LZ_BARCODE_MT B WHERE B.BARCODE_NO NOT IN (SELECT O.BARCODE_PRV_NO FROM LZ_SPECIAL_LOTS O UNION ALL SELECT D.BARCODE_PRV_NO FROM LZ_DEKIT_US_DT D) AND B.BARCODE_NO = '$barocde' ")->result_array();

            //if(count($chek_b) >= ){
            if (count($chek_b) >= 1) {

                if ($old_upc !== $seedUpc || $old_mpn !== $seedMpn) { //upc change

                    $old_mpn = str_replace("/", "_", $old_mpn);
                    $old_mpn = str_replace("\\", "_", $old_mpn);
                    $seedMpn = str_replace("/", "_", $seedMpn);
                    $seedMpn = str_replace("\\", "_", $seedMpn);
                    $old_dir = $pic_dir . $old_upc . '~' . $old_mpn; //.'/'.$condition_disc;
                    $new_dir = $pic_dir . $seedUpc . '~' . $seedMpn; //.'/'.$condition_name;

                    // var_dump('old'.$old_dir,'new'.$new_dir);
                    // exit;
                    if (!is_dir(@$new_dir)) {

                        rename(@$old_dir, @$new_dir);

                    }
                    //return true;
                }
            }
            // }
            return true;
        }

    }

    public function get_avail_cond()
    {

        $catId = $this->input->post('catId');

        $get_cond = $this->db->query("SELECT C.CONDITION_ID,M.COND_NAME FROM LZ_BD_CAT_COND C,LZ_ITEM_COND_MT M WHERE C.CATEGORY_ID = $catId AND C.CONDITION_ID = M.ID")->result_array();
        if (count($get_cond) > 1) {

            return array('get_cond' => $get_cond, 'exist' => true);

        } else {

            $get_cond = $this->db->query('SELECT CC.ID CONDITION_ID,CC.COND_NAME FROM LZ_ITEM_COND_MT CC')->result_array();

            return array('get_cond' => $get_cond, 'exist' => true);

        }

    }

    public function serch_mpn_sys()
    {
        $mpn = strtoupper($this->input->post('passVal'));

        $desc_mpn = "SELECT * FROM (SELECT S.ITEM_TITLE  TITLE, DECODE(S.CATEGORY_ID,NULL,O.CATEGORY_ID,S.CATEGORY_ID) CATE, I.ITEM_MT_MFG_PART_NO MPN, I.ITEM_MT_MANUFACTURE BRAND, I.ITEM_MT_UPC  UPC, I.ITEM_CONDITION COND_NAME, O.OBJECT_NAME OBJECT_NAME FROM LZ_ITEM_SEED S, ITEMS_MT I, LZ_CATALOGUE_MT C, LZ_BD_OBJECTS_MT O WHERE S.ITEM_ID = I.ITEM_ID AND UPPER(I.ITEM_MT_MFG_PART_NO) = UPPER(C.MPN(+)) AND S.CATEGORY_ID = C.CATEGORY_ID(+) AND C.OBJECT_ID = O.OBJECT_ID(+) and UPPER(I.ITEM_MT_MFG_PART_NO) LIKE '%$mpn%' order by s.seed_id desc ) WHERE ROWNUM <=20";

        $desc_mpn_quer = $this->db->query($desc_mpn)->result_array();

        if (count($desc_mpn_quer) < 1) {

            $desc_mpn = " SELECT * FROM (SELECT DECODE(D.MPN_DESCRIPTION, NULL, C.MPN_DESCRIPTION,D.MPN_DESCRIPTION) TITLE, C.CATEGORY_ID CATE, C.MPN, C.BRAND , C.UPC, CO.COND_NAME, OB.OBJECT_NAME FROM LZ_CATALOGUE_MT C, LZ_BD_ESTIMATE_DET D,LZ_ITEM_COND_MT CO,LZ_BD_OBJECTS_MT OB WHERE D.PART_CATLG_MT_ID = C.CATALOGUE_MT_ID AND D.TECH_COND_ID = CO.ID(+) AND C.OBJECT_ID = OB.OBJECT_ID (+) and UPPER(C.MPN) LIKE '%$mpn%'  ) WHERE  ROWNUM <=20 ";
        }

        $desc_mpn_quer = $this->db->query($desc_mpn)->result_array();

        return array('desc_quer' => $desc_mpn_quer, 'exist' => true);

    }

    public function get_upc_title()
    {

        $upc = strtoupper($this->input->post('passVal'));

        $desc_upc = "SELECT * FROM (SELECT S.ITEM_TITLE  TITLE, DECODE(S.CATEGORY_ID,NULL,O.CATEGORY_ID,S.CATEGORY_ID) CATE, I.ITEM_MT_MFG_PART_NO MPN, I.ITEM_MT_MANUFACTURE BRAND, I.ITEM_MT_UPC  UPC, I.ITEM_CONDITION COND_NAME, O.OBJECT_NAME OBJECT_NAME FROM LZ_ITEM_SEED S, ITEMS_MT I, LZ_CATALOGUE_MT C, LZ_BD_OBJECTS_MT O WHERE S.ITEM_ID = I.ITEM_ID AND UPPER(I.ITEM_MT_MFG_PART_NO) = UPPER(C.MPN(+)) AND S.CATEGORY_ID = C.CATEGORY_ID(+) AND C.OBJECT_ID = O.OBJECT_ID(+) and UPPER(ITEM_MT_UPC) LIKE '%$upc%' order by s.seed_id desc ) WHERE  ROWNUM <=20 ";

        $desc_upc_quer = $this->db->query($desc_upc)->result_array();

        if (count($desc_upc_quer) < 1) {

            $desc_upc = " SELECT * FROM (SELECT DECODE(D.MPN_DESCRIPTION, NULL, C.MPN_DESCRIPTION,D.MPN_DESCRIPTION) TITLE, C.CATEGORY_ID CATE, C.MPN, C.BRAND , C.UPC, CO.COND_NAME, OB.OBJECT_NAME FROM LZ_CATALOGUE_MT C, LZ_BD_ESTIMATE_DET D,LZ_ITEM_COND_MT CO,LZ_BD_OBJECTS_MT OB WHERE D.PART_CATLG_MT_ID = C.CATALOGUE_MT_ID AND D.TECH_COND_ID = CO.ID(+) AND C.OBJECT_ID = OB.OBJECT_ID (+) and  UPPER(C.UPC) LIKE '%$upc%' ) WHERE  ROWNUM <=20 ";
        }

        $desc_upc_quer = $this->db->query($desc_upc)->result_array();

        return array('desc_quer' => $desc_upc_quer, 'exist' => true);

    }

    public function serch_desc_sys()
    {
        $get_desc = strtoupper(trim($this->input->post('passVal')));

        $str = explode(' ', $get_desc);

        $desc_sys = "SELECT * FROM (SELECT S.ITEM_TITLE  TITLE, DECODE(S.CATEGORY_ID,NULL,O.CATEGORY_ID,S.CATEGORY_ID) CATE, I.ITEM_MT_MFG_PART_NO MPN, I.ITEM_MT_MANUFACTURE BRAND, I.ITEM_MT_UPC  UPC, I.ITEM_CONDITION COND_NAME, O.OBJECT_NAME OBJECT_NAME FROM LZ_ITEM_SEED S, ITEMS_MT I, LZ_CATALOGUE_MT C, LZ_BD_OBJECTS_MT O WHERE S.ITEM_ID = I.ITEM_ID AND UPPER(I.ITEM_MT_MFG_PART_NO) = UPPER(C.MPN(+)) AND S.CATEGORY_ID = C.CATEGORY_ID(+) AND C.OBJECT_ID = O.OBJECT_ID(+)";

        if (!empty($get_desc)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $desc_sys .= " and UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    } else {
                        $desc_sys .= " AND UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $desc_sys .= " and UPPER(S.ITEM_TITLE) LIKE '%$get_desc%' ";
            }
        }
        $desc_sys .= "  order by s.seed_id desc ) WHERE ROWNUM <=20 ";

        $desc_sys_quer = $this->db->query($desc_sys)->result_array();

        if (count($desc_sys_quer) < 1) {

            $desc_sys = " SELECT * FROM (SELECT DECODE(D.MPN_DESCRIPTION, NULL, C.MPN_DESCRIPTION,D.MPN_DESCRIPTION) TITLE, C.CATEGORY_ID CATE, C.MPN, C.BRAND , C.UPC, CO.COND_NAME, OB.OBJECT_NAME FROM LZ_CATALOGUE_MT C, LZ_BD_ESTIMATE_DET D,LZ_ITEM_COND_MT CO,LZ_BD_OBJECTS_MT OB WHERE D.PART_CATLG_MT_ID = C.CATALOGUE_MT_ID AND D.TECH_COND_ID = CO.ID(+) AND C.OBJECT_ID = OB.OBJECT_ID (+)  ) WHERE  MPN is not null  ";

            if (!empty($get_desc)) {
                if (count($str) > 1) {
                    $i = 1;
                    foreach ($str as $key) {
                        if ($i === 1) {
                            $desc_sys .= " and UPPER(TITLE) LIKE '%$key%' ";
                        } else {
                            $desc_sys .= " AND UPPER(TITLE) LIKE '%$key%' ";
                        }
                        $i++;
                    }
                } else {
                    $desc_sys .= " and UPPER(TITLE) LIKE '%$get_desc%' ";
                }
            }
            $desc_sys .= "  and ROWNUM <=20 ";
        }
        $desc_sys_quer = $this->db->query($desc_sys)->result_array();

        return array('desc_quer' => $desc_sys_quer, 'exist' => true);

    }

    public function itemDiscard()
    {

        $barcodeNo = $this->input->post('barcodeNo');
        //$barcodeNo = trim(str_replace("  ", ' ', $barcodeNo));
        //$barcodeNo = trim(str_replace(array("'"), "''", $barcodeNo));

        $itemType = $this->input->post('itemType');
        $itemType = trim(str_replace("  ", ' ', $itemType));
        $itemType = trim(str_replace(array("'"), "''", $itemType));

        $idArray = $this->input->post('idArray');

        $upcNum = $this->input->post('upcNum');
        $upcNum = trim(str_replace("  ", ' ', $upcNum));
        $upcNum = trim(str_replace(array("'"), "''", $upcNum));

        $mpnName = $this->input->post('mpnName');
        $mpnName = trim(str_replace("  ", ' ', $mpnName));
        $mpnName = trim(str_replace(array("'"), "''", $mpnName));

        $objName = $this->input->post('objName');
        $objName = trim(str_replace("  ", ' ', $objName));
        $objName = trim(str_replace(array("'"), "''", $objName));

        $catId = $this->input->post('catId');
        $catId = trim(str_replace("  ", ' ', $catId));
        $catId = trim(str_replace(array("'"), "''", $catId));

        $brandName = $this->input->post('brandName');
        $brandName = trim(str_replace("  ", ' ', $brandName));
        $brandName = trim(str_replace(array("'"), "''", $brandName));

        $mpnDesc = $this->input->post('mpnDesc');
        $mpnDesc = trim(str_replace("  ", ' ', $mpnDesc));
        $mpnDesc = trim(str_replace(array("'"), "''", $mpnDesc));

        $remarks = $this->input->post('remarks');
        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = trim(str_replace(array("'"), "''", $remarks));

        $avgSold = $this->input->post('avgSold');
        $avgSold = trim(str_replace("  ", ' ', $avgSold));
        $avgSold = trim(str_replace(array("'"), "''", $avgSold));

        $condRadio = $this->input->post('condRadio');
        $userId = $this->input->post('userId');

        date_default_timezone_set("America/Chicago");
        $date = date('Y-m-d H:i:s');
        $insert_date = "TO_DATE('" . $date . "', 'YYYY-MM-DD HH24:MI:SS')";

        $insert_by = $userId;

        if ($itemType == 'DEKIT ITEM') {

            foreach ($barcodeNo as $child_brcd) {
                $update_dekit = $this->db->query("UPDATE LZ_DEKIT_US_DT SET  IDENT_DATE_TIME = $insert_date, IDENTIFIED_BY = $insert_by, IDENT_REMARKS = '$remarks',AVG_SELL_PRICE = '$avgSold', DISCARD_BY =$insert_by ,DISCARD_DATE=$insert_date, DISCARD = 1 WHERE BARCODE_PRV_NO = $child_brcd");
                // if ($update_dekit) {
                //     $this->db->query("call pro_dekiting_us_pk($child_brcd)");
                // }
            }

        } else if ($itemType == 'SPECIAL LOT') {

            foreach ($barcodeNo as $child_brcd) {

                $update_dekit = $this->db->query("UPDATE LZ_SPECIAL_LOTS SET LOT_REMARKS = '$remarks' ,UPDATED_AT = $insert_date, UPDATED_BY = $insert_by,  AVG_SOLD = '$avgSold', DISCARD_BY =$insert_by , DISCARD_DATE =$insert_date, DISCARD = 1 WHERE BARCODE_PRV_NO = $child_brcd"); // if ($update_dekit) {
                //     $this->db->query("call pro_dekiting_us_pk($child_brcd)");
                // }
            }

        }

        if ($update_dekit) {

            return true;
        } else {
            return false;
        }

    }

    public function verify_item()
    {

        $barcodeNo = $this->input->post('barcodeNo');

        //$barcodeNo = trim(str_replace("  ", ' ', $barcodeNo));
        //$barcodeNo = trim(str_replace(array("'"), "''", $barcodeNo));

        $itemType = $this->input->post('itemType');
        $itemType = trim(str_replace("  ", ' ', $itemType));
        $itemType = trim(str_replace(array("'"), "''", $itemType));

        $idArray = $this->input->post('idArray');

        $upcNum = $this->input->post('upcNum');
        $upcNum = trim(str_replace("  ", ' ', $upcNum));
        $upcNum = trim(str_replace(array("'"), "''", $upcNum));

        $mpnName = $this->input->post('mpnName');
        $mpnName = trim(str_replace("  ", ' ', $mpnName));
        $mpnName = trim(str_replace(array("'"), "''", $mpnName));

        $objName = $this->input->post('objName');
        $objName = trim(str_replace("  ", ' ', $objName));
        $objName = trim(str_replace(array("'"), "''", $objName));

        $catId = $this->input->post('catId');
        $catId = trim(str_replace("  ", ' ', $catId));
        $catId = trim(str_replace(array("'"), "''", $catId));

        $brandName = $this->input->post('brandName');
        $brandName = trim(str_replace("  ", ' ', $brandName));
        $brandName = trim(str_replace(array("'"), "''", $brandName));

        $mpnDesc = $this->input->post('mpnDesc');
        $mpnDesc = trim(str_replace("  ", ' ', $mpnDesc));
        $mpnDesc = trim(str_replace(array("'"), "''", $mpnDesc));

        $remarks = $this->input->post('remarks');
        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = trim(str_replace(array("'"), "''", $remarks));

        $avgSold = $this->input->post('avgSold');
        $avgSold = trim(str_replace("  ", ' ', $avgSold));
        $avgSold = trim(str_replace(array("'"), "''", $avgSold));

        $condRadio = $this->input->post('condRadio');
        $userId = $this->input->post('userId');

        date_default_timezone_set("America/Chicago");
        $date = date('Y-m-d H:i:s');
        $insert_date = "TO_DATE('" . $date . "', 'YYYY-MM-DD HH24:MI:SS')";

        $insert_by = $userId;

        $get_merchnt = $this->db->query("SELECT M.MERCHANT_ID FROM LZ_MERCHANT_BARCODE_MT M,LZ_MERCHANT_BARCODE_DT D WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = '$barcodeNo[0]'")->result_array();
        $get_merchnt = $get_merchnt[0]['MERCHANT_ID']; //var_dump($catId);

        $get_obj = $this->db->query("SELECT OB.OBJECT_ID FROM LZ_BD_OBJECTS_MT OB WHERE UPPER(OB.OBJECT_NAME) =upper('$objName') and ob.category_id =$catId ");

        if ($get_obj->num_rows() > 0) {

            $get_obj = $get_obj->result_array();
            $get_obj = $get_obj[0]['OBJECT_ID'];
            $objName = $get_obj;

        } else {
            $obj_id = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_BD_OBJECTS_MT','OBJECT_ID') OBJECT_ID FROM DUAL");
            $get_mt_pk = $obj_id->result_array();
            $object_id = $get_mt_pk[0]['OBJECT_ID'];

            $this->db->query("INSERT INTO LZ_BD_OBJECTS_MT(OBJECT_ID, OBJECT_NAME,INSERT_DATE,INSERT_BY, CATEGORY_ID) VALUES($object_id, '$objName',$insert_date,$insert_by, $catId)");

            $objName = $object_id;
        }

        // dekit posting code start
        if ($itemType == 'DEKIT ITEM') {
            // var_dump($itemType );
            // exit;
            // $get_sedd_param = $this->db->query("SELECT U.LZ_MANIFEST_ID,U.ITEM_ID,U.CONDITION_ID FROM LZ_BARCODE_MT U WHERE U.BARCODE_NO = $barcodeNo")->result_array();

            // if (count($get_sedd_param) > 0) {
            //     $manif_id = $get_sedd_param[0]['LZ_MANIFEST_ID'];
            //     $item_id = $get_sedd_param[0]['ITEM_ID'];
            //     $cond_id = $get_sedd_param[0]['CONDITION_ID'];

            //     return array('exist' => true, 'posted' => true);

            // } else {

            $mpn_check = $this->db->query("SELECT CATALOGUE_MT_ID, OBJECT_ID,MPN FROM LZ_CATALOGUE_MT WHERE UPPER(MPN) = UPPER('$mpnName') AND CATEGORY_ID = $catId");

            if ($mpn_check->num_rows() == 0) {

                $get_mt_pk = $this->db->query("SELECT get_single_primary_key('LZ_CATALOGUE_MT','CATALOGUE_MT_ID') CATALOGUE_MT_ID FROM DUAL");
                //$get_mt_pk = $this->db->query("SELECT MAX(CATALOGUE_MT_ID + 1) CATALOGUE_MT_ID FROM LZ_CATALOGUE_MT");
                $get_pk = $get_mt_pk->result_array();
                $cat_mt_id = $get_pk[0]['CATALOGUE_MT_ID'];

                $mt_qry = $this->db->query("INSERT INTO LZ_CATALOGUE_MT(CATALOGUE_MT_ID, MPN, CATEGORY_ID, INSERTED_DATE, INSERTED_BY,OBJECT_ID,MPN_DESCRIPTION,BRAND,UPC) VALUES($cat_mt_id, '$mpnName', $catId, $insert_date, $insert_by,$objName,'$mpnDesc','$brandName','$upcNum')");
                //echo "yes";
            } else {
                $get_pk = $mpn_check->result_array();
                $cat_mt_id = $get_pk[0]['CATALOGUE_MT_ID'];
                $mpn_description = $get_pk[0]['MPN'];
                $object_id = $get_pk[0]['OBJECT_ID'];
                //$get_brand = $get_pk[0]['BRAND'];
                $this->db->query("UPDATE LZ_CATALOGUE_MT SET BRAND = '$brandName' WHERE CATALOGUE_MT_ID =$cat_mt_id ");
            }

            // code section added by adil asad on jan-19-2018 strat
            //*****************************************************
            // foreach ($idArray as $child_brcd) { coment by adil
            //     $update_dekit = $this->db->query("UPDATE LZ_DEKIT_US_DT SET CATALOG_MT_ID = $cat_mt_id, IDENT_DATE_TIME = $insert_date, IDENTIFIED_BY = $insert_by, IDENT_REMARKS = '$remarks', OBJECT_ID = $objName, CONDITION_ID = $condRadio, MPN_DESCRIPTION = '$mpnDesc' ,AVG_SELL_PRICE = '$avgSold'  WHERE LZ_DEKIT_US_DT_ID = $child_brcd");
            //     if ($update_dekit) {
            //         $this->db->query("call pro_dekiting_us_pk($child_brcd)");
            //     }
            // }
            foreach ($barcodeNo as $child_brcd) {
                $update_dekit = $this->db->query("UPDATE LZ_DEKIT_US_DT SET CATALOG_MT_ID = $cat_mt_id, IDENT_DATE_TIME = $insert_date, IDENTIFIED_BY = $insert_by, IDENT_REMARKS = '$remarks', OBJECT_ID = $objName, CONDITION_ID = $condRadio, MPN_DESCRIPTION = '$mpnDesc' ,AVG_SELL_PRICE = '$avgSold'  WHERE BARCODE_PRV_NO = $child_brcd");
                if ($update_dekit) {
                    $this->db->query("call pro_dekiting_us_pk($child_brcd)");
                }
            }

            return array('exist' => true, 'newly post' => true);
            //}

        } else if ($itemType == 'SPECIAL LOT') {
            // var_dump($itemType );
            // exit;

            // $get_sedd_param = $this->db->query("SELECT U.LZ_MANIFEST_ID,U.ITEM_ID,U.CONDITION_ID FROM LZ_BARCODE_MT U WHERE U.BARCODE_NO = $barcodeNo")->result_array();

            // if (count($get_sedd_param) > 0) {
            //     $manif_id = $get_sedd_param[0]['LZ_MANIFEST_ID'];
            //     $item_id = $get_sedd_param[0]['ITEM_ID'];
            //     $cond_id = $get_sedd_param[0]['CONDITION_ID'];

            //     return array('exist' => true, 'posted' => true);

            // } else {

            // inertion bloc
            $weights = $this->db->query("SELECT O.WEIGHT, O.ITEM_COST,O.CATEGORY_ID FROM LZ_BD_OBJECTS_MT O WHERE O.OBJECT_ID = $objName ")->result_array();
            $item_cost = @$weights[0]['ITEM_COST'];
            $item_weight = @$weights[0]['WEIGHT'];
            $category_id = @$weights[0]['CATEGORY_ID'];

            if (empty($item_cost)) {
                $item_cost = 0;
            }
            if (empty($item_weight)) {
                $item_weight = 0;
            }

            $card_mpn = $mpnName;
            $category_id = $catId;
            $user_id = $insert_by;
            $object_desc = $objName;
            $mpn_description = $mpnDesc;
            $brand_name = $brandName;
            $card_upc = $upcNum;
            $dek = $remarks;
            $cond_item = $condRadio;

            $catalogue_mt_id = '';

            if (!empty($card_mpn)) {
                $check_mpn = $this->db->query("SELECT M.CATALOGUE_MT_ID FROM LZ_CATALOGUE_MT M WHERE UPPER(M.MPN) = UPPER('$card_mpn') AND M.CATEGORY_ID = $category_id")->result_array();
                if (count($check_mpn) == 0) {
                    $qry = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_CATALOGUE_MT', 'CATALOGUE_MT_ID')ID FROM DUAL");
                    $qry = $qry->result_array();
                    $catalogue_mt_id = $qry[0]['ID'];

                    $insert_mpn = $this->db->query("INSERT INTO LZ_CATALOGUE_MT(CATALOGUE_MT_ID, MPN, CATEGORY_ID, INSERTED_DATE, INSERTED_BY, OBJECT_ID, MPN_DESCRIPTION, BRAND, UPC) VALUES($catalogue_mt_id, '$card_mpn', $category_id, $insert_date, $user_id, $object_desc, '$mpn_description', '$brand_name', '$card_upc')");
                } else {
                    $catalogue_mt_id = $check_mpn[0]['CATALOGUE_MT_ID'];
                }

            } else {
                /*==================================================
                =            genrate mpn if mpn is null            =
                ==================================================*/

                $get_catalg_id = $this->db->query("SELECT * FROM (SELECT L.CATALOG_MT_ID FROM LZ_SPECIAL_LOTS L WHERE L.CARD_UPC = '$card_upc' AND L.CATALOG_MT_ID IS NOT NULL ORDER BY L.SPECIAL_LOT_ID DESC) WHERE ROWNUM = 1");

                if ($get_catalg_id->num_rows() > 0) {

                    $get_exist_mpn = $get_catalg_id->result_array();
                    $catalogue_mt_id = $get_exist_mpn[0]['CATALOG_MT_ID'];

                } else {
                    $get_mpn = $this->db->query("SELECT MPN_GENERATION($category_id) as MPN FROM DUAL");
                    $get_mpn = $get_mpn->result_array();
                    $get_mpn = $get_mpn[0]['MPN'];

                    $get_mt_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_CATALOGUE_MT','CATALOGUE_MT_ID') CATALOGUE_MT_ID FROM DUAL");
                    $get_pk = $get_mt_pk->result_array();
                    $catalogue_mt_id = $get_pk[0]['CATALOGUE_MT_ID'];

                    $mt_qry = $this->db->query("INSERT INTO LZ_CATALOGUE_MT(CATALOGUE_MT_ID, MPN, CATEGORY_ID, INSERTED_DATE, INSERTED_BY, OBJECT_ID, MPN_DESCRIPTION, BRAND, UPC) VALUES($catalogue_mt_id, '$card_mpn', $category_id, $insert_date, $user_id, $object_desc, '$mpn_description', '$brand_name', '$card_upc')");

                    //$card_mpn = $get_mpn; // assign newly created part no

                } //get_catalg_id else closing
                /*=====  End of genrate mpn if mpn is null  ===*/
            } //end if else !empty($card_mpn)

            $lots_data = $this->db->query("SELECT SPECIAL_LOT_ID,C.CARD_UPC, UPPER(C.CARD_MPN) CARD_MPN, C.CONDITION_ID, C.OBJECT_ID, C.BRAND,C.FOLDER_NAME,C.BIN_ID FROM LZ_SPECIAL_LOTS C WHERE C.barcode_prv_no = $barcodeNo[0]")->result_array();
            $special_lot_id = $lots_data[0]['SPECIAL_LOT_ID'];
            $lot_upc = $lots_data[0]['CARD_UPC'];
            $lot_mpn = $lots_data[0]['CARD_MPN'];
            $lot_condition_id = $lots_data[0]['CONDITION_ID'];
            $lot_object_id = $lots_data[0]['OBJECT_ID'];
            $lot_brand = $lots_data[0]['BRAND'];
            $folder_name = $lots_data[0]['FOLDER_NAME'];
            $current_bin = $lots_data[0]['BIN_ID'];

            // if(!empty($current_bin)){
            //    = "";
            // }else{
            //    =",BIN_ID =".$bin_rack;
            // }

            if ($lot_upc != '' && $lot_mpn == '') {

                $with_upcs = $this->db->query("SELECT * FROM LZ_SPECIAL_LOTS LS WHERE LS.CARD_UPC = '$card_upc' AND LS.CARD_MPN IS NULL AND LS.LZ_MANIFEST_DET_ID IS NULL")->result_array();
                if (count($with_upcs) > 1) {
                    $insert_est_det = $this->db->query("UPDATE LZ_SPECIAL_LOTS SET OBJECT_ID = $object_desc ,CARD_CATEGORY_ID = '$category_id', LOT_REMARKS = '$dek' , WEIGHT = '$item_weight',CONDITION_ID = $cond_item,CARD_UPC = '$card_upc', CARD_MPN = '$card_mpn', CATALOG_MT_ID = '$catalogue_mt_id', MPN_DESCRIPTION = '$mpn_description', BRAND = '$brand_name', UPDATED_AT = $insert_date, UPDATED_BY = $user_id, ITEM_COST = '$item_cost' , AVG_SOLD = '$avgSold'  WHERE CARD_UPC = '$lot_upc' AND CARD_MPN IS NULL AND LZ_MANIFEST_DET_ID IS NULL");
                    if ($insert_est_det) {
                        $flag = 1;

                    } else {
                        $flag = 0;

                    }

                } else {
                    $insert_est_det = $this->db->query("UPDATE LZ_SPECIAL_LOTS SET OBJECT_ID = $object_desc ,CARD_CATEGORY_ID = '$category_id', LOT_REMARKS = '$dek' , WEIGHT = '$item_weight',CONDITION_ID = $cond_item, CARD_UPC = '$card_upc' ,CARD_MPN = '$card_mpn', CATALOG_MT_ID = '$catalogue_mt_id', MPN_DESCRIPTION = '$mpn_description', BRAND = '$brand_name', UPDATED_AT = $insert_date, UPDATED_BY = $user_id, ITEM_COST = '$item_cost' , AVG_SOLD = '$avgSold' WHERE SPECIAL_LOT_ID = $special_lot_id");
                    if ($insert_est_det) {
                        $flag = 1;
                    } else {
                        $flag = 0;

                    }
                }
            } elseif ($lot_mpn != '' && $lot_upc == '') {

                $with_upcs = $this->db->query("SELECT * FROM LZ_SPECIAL_LOTS LS WHERE LS.CARD_UPC IS NULL AND UPPER(LS.CARD_MPN) = '$card_mpn' AND LS.LZ_MANIFEST_DET_ID IS NULL")->result_array();
                if (count($with_upcs) > 1) {
                    $insert_est_det = $this->db->query("UPDATE LZ_SPECIAL_LOTS SET OBJECT_ID = $object_desc , CARD_CATEGORY_ID = '$category_id',LOT_REMARKS = '$dek' , WEIGHT = '$item_weight',CONDITION_ID = $cond_item,CARD_UPC = '$card_upc', CARD_MPN = '$card_mpn', CATALOG_MT_ID = '$catalogue_mt_id', MPN_DESCRIPTION = '$mpn_description', BRAND = '$brand_name', UPDATED_AT = $insert_date, UPDATED_BY = $user_id, ITEM_COST = '$item_cost'  , AVG_SOLD = '$avgSold'  WHERE CARD_UPC IS NULL AND UPPER(CARD_MPN) = '$lot_mpn' AND LZ_MANIFEST_DET_ID IS NULL");
                    if ($insert_est_det) {
                        $flag = 1;

                    } else {
                        $flag = 0;

                    }

                } else {
                    $insert_est_det = $this->db->query("UPDATE LZ_SPECIAL_LOTS SET OBJECT_ID = $object_desc ,CARD_CATEGORY_ID = '$category_id', LOT_REMARKS = '$dek' , WEIGHT = '$item_weight',CONDITION_ID = $cond_item, CARD_UPC = '$card_upc' ,CARD_MPN = '$card_mpn', CATALOG_MT_ID = '$catalogue_mt_id', MPN_DESCRIPTION = '$mpn_description', BRAND = '$brand_name', UPDATED_AT = $insert_date, UPDATED_BY = $user_id, ITEM_COST = '$item_cost' , AVG_SOLD = '$avgSold' WHERE SPECIAL_LOT_ID = $special_lot_id");
                    if ($insert_est_det) {
                        $flag = 1;
                    } else {
                        $flag = 0;

                    }
                }
            } elseif ($lot_mpn != '' && $lot_upc != '') {

                $with_upcs = $this->db->query("SELECT * FROM LZ_SPECIAL_LOTS LS WHERE LS.CARD_UPC = '$card_upc' AND UPPER(LS.CARD_MPN) = '$card_mpn' AND LS.LZ_MANIFEST_DET_ID IS NULL")->result_array();
                if (count($with_upcs) > 1) {
                    $insert_est_det = $this->db->query("UPDATE LZ_SPECIAL_LOTS SET OBJECT_ID = $object_desc ,CARD_CATEGORY_ID = '$category_id', LOT_REMARKS = '$dek' , WEIGHT = '$item_weight',CONDITION_ID = $cond_item,CARD_UPC = '$card_upc', CARD_MPN = '$card_mpn', CATALOG_MT_ID = '$catalogue_mt_id', MPN_DESCRIPTION = '$mpn_description', BRAND = '$brand_name', UPDATED_AT = $insert_date, UPDATED_BY = $user_id, ITEM_COST = '$item_cost' , AVG_SOLD = '$avgSold' WHERE CARD_UPC = '$lot_upc' AND UPPER(CARD_MPN) = '$lot_mpn'AND LZ_MANIFEST_DET_ID IS NULL");
                    if ($insert_est_det) {
                        $flag = 1;

                    } else {
                        $flag = 0;

                    }

                } else {
                    $insert_est_det = $this->db->query("UPDATE LZ_SPECIAL_LOTS SET OBJECT_ID = $object_desc ,CARD_CATEGORY_ID = '$category_id', LOT_REMARKS = '$dek' , WEIGHT = '$item_weight',CONDITION_ID = $cond_item, CARD_UPC = '$card_upc' ,CARD_MPN = '$card_mpn', CATALOG_MT_ID = '$catalogue_mt_id', MPN_DESCRIPTION = '$mpn_description', BRAND = '$brand_name', UPDATED_AT = $insert_date, UPDATED_BY = $user_id, ITEM_COST = '$item_cost', AVG_SOLD = '$avgSold'  WHERE SPECIAL_LOT_ID = $special_lot_id");
                    if ($insert_est_det) {
                        $flag = 1;
                    } else {
                        $flag = 0;
                    }
                }
            } else {
                $insert_est_det = $this->db->query("UPDATE LZ_SPECIAL_LOTS SET OBJECT_ID = $object_desc , CARD_CATEGORY_ID = '$category_id',LOT_REMARKS = '$dek' , WEIGHT = '$item_weight',CONDITION_ID = $cond_item, CARD_UPC = '$card_upc' ,CARD_MPN = '$card_mpn', CATALOG_MT_ID = '$catalogue_mt_id', MPN_DESCRIPTION = '$mpn_description', BRAND = '$brand_name', UPDATED_AT = $insert_date, UPDATED_BY = $user_id, ITEM_COST = '$item_cost',AVG_SOLD = '$avgSold' WHERE SPECIAL_LOT_ID = $special_lot_id");
                if ($insert_est_det) {
                    $flag = 1;
                } else {
                    $flag = 0;

                }
            }
            //}
            /*================================================================
            =            update all barcode with same folder name            =
            ================================================================*/
            $insert_est_det = $this->db->query("UPDATE LZ_SPECIAL_LOTS SET OBJECT_ID = $object_desc , CARD_CATEGORY_ID = '$category_id',LOT_REMARKS = '$dek' , WEIGHT = '$item_weight',CONDITION_ID = $cond_item, CARD_UPC = '$card_upc' ,CARD_MPN = '$card_mpn', CATALOG_MT_ID = '$catalogue_mt_id', MPN_DESCRIPTION = '$mpn_description', BRAND = '$brand_name', UPDATED_AT = $insert_date, UPDATED_BY = $user_id, ITEM_COST = '$item_cost', AVG_SOLD = '$avgSold' WHERE FOLDER_NAME = $folder_name AND LZ_MANIFEST_DET_ID IS NULL");
            if ($insert_est_det) {
                $flag = 1;
            } else {
                $flag = 0;

            }

            if (!empty($card_upc) and !empty($card_mpn)) {

                $querys = $this->db->query("call PRO_SINGLE_INSERT_LOTS('=''$card_upc''' ,  '=''$card_mpn''', $user_id,'=''$cond_item''','=''$get_merchnt''' ) ");

            } elseif (!empty($card_upc) and empty($card_mpn)) {
                $querys = $this->db->query("call PRO_SINGLE_INSERT_LOTS('=''$card_upc''' , ' IS NULL', $user_id,'=''$cond_item''','=''$get_merchnt''' ) ");

            } elseif (empty($card_upc) and !empty($card_mpn)) {
                $querys = $this->db->query("call PRO_SINGLE_INSERT_LOTS(' IS NULL' , '=''$card_mpn''', $user_id,'=''$cond_item''','=''$get_merchnt''' ) ");

            } elseif (empty($card_upc) and empty($card_mpn)) {
                die('UPC and Mpn is required');
            }

            if ($querys) {
                $flag = 1;
                return array('exist' => true, 'newly post' => true, 'flag' => $flag);
            } else {
                $flag = 0;
                return array('exist' => true, 'newly post' => true, 'flag' => $flag);
            }

            // end insertion block
            //}

        }
        // dekit posting code end
    }
    public function get_verify_item()
    {
        // /117001
        $user_id = $this->input->post('user_id');
        // var_dump($user_id);
        // exit;

        $deleteAllow = array('21', '2', '81');
        $deleAcces = false;
        if (in_array($user_id, $deleteAllow)) {
            $deleAcces = true;
        } else {
            $deleAcces = false;
        }

        $barcode = $this->uri->segment(4);

        $get_items = $this->db->query("SELECT DDD.TAG_ID, DDD.TAG_REMARKS, DDD.TAG_DATE, DDD.TAG_BY, BR.*
        FROM LZ_MERCHANT_BARCODE_DT DDD,
        (SELECT 'DEKIT ITEM' BAROCDE_TYPE,
        DECODE(D.LZ_MANIFEST_DET_ID, NULL, 'PENDING', 'POSTED') VERIFY_ITEM,
        D.LZ_DEKIT_US_DT_ID LZ_DEKIT_US_DT_ID,
        D.BARCODE_PRV_NO,
        C.COND_NAME,
        C.ID,
        O.OBJECT_NAME,
        to_char(O.OBJECT_ID) OBJECT_ID,
        /*D.PIC_DATE_TIME
        D.PIC_BY,
        D.PIC_NOTES,*/
        DECODE(D.MPN_DESCRIPTION,
        NULL,
        CA.MPN_DESCRIPTION,
        D.MPN_DESCRIPTION) MPN_DESC,
        CA.MPN,
        CA.UPC,
        CA.BRAND,
        TO_CHAR(D.FOLDER_NAME) FOLDER_NAME,
        D.IDENT_REMARKS REMARKS,
        D.DEKIT_REMARKS US_REMARKS,
        D.LZ_MANIFEST_DET_ID,

        to_char(CA.CATEGORY_ID) category_id,
        TO_CHAR(D.AVG_SELL_PRICE) AVG_PRIC,
        '-' merchant_name,
        '-' PURCHASE_REMARKS
        FROM LZ_DEKIT_US_DT D,
        LZ_BD_OBJECTS_MT O,
        LZ_CATALOGUE_MT CA,
        LZ_ITEM_COND_MT C
        WHERE D.OBJECT_ID = O.OBJECT_ID(+)
        AND D.CATALOG_MT_ID = CA.CATALOGUE_MT_ID(+)
        AND D.CONDITION_ID = C.ID(+)
        UNION ALL
        SELECT 'SPECIAL LOT' BAROCDE_TYPE,
        DECODE(L.LZ_MANIFEST_DET_ID, NULL, 'PENDING', 'POSTED') VERIFY_ITEM,
        L.SPECIAL_LOT_ID SPECIAL_LOT_ID,
        L.BARCODE_PRV_NO,
        C.COND_NAME,
        C.ID,
        OB.OBJECT_NAME,
        to_char(OB.OBJECT_ID) OBJECT_ID,
        /*L.PIC_DATE_TIME
        L.PIC_BY,
        L.PIC_NOTES,*/
        DECODE(L.MPN_DESCRIPTION,
        NULL,
        CA.MPN_DESCRIPTION,
        L.MPN_DESCRIPTION) MPN_DESC,
        DECODE(L.CARD_MPN, NULL, CA.MPN, L.CARD_MPN) MPN,
        DECODE(L.CARD_UPC, NULL, CA.UPC, L.CARD_UPC) UPC,
        DECODE(L.BRAND, NULL, CA.BRAND, L.BRAND) BRAND,
        TO_CHAR(L.FOLDER_NAME) FOLDER_NAME,
        L.LOT_REMARKS REMARKS,
        L.BARCODE_NOTES US_REMARKS,
        L.LZ_MANIFEST_DET_ID,
        to_char(decode(l.CARD_CATEGORY_ID,
        null,
        CA.CATEGORY_ID,
        l.CARD_CATEGORY_ID)) CATEGORY_ID,

        TO_CHAR(l.AVG_SOLD) AVG_PRIC,
        (select mt.buisness_name
        from lz_merchant_barcode_mt mm,
        lz_merchant_barcode_dt d,
        lz_merchant_mt mt
        where mm.mt_id = d.mt_id
        and mm.merchant_id = mt.merchant_id
        and d.BARCODE_NO = L.barcode_prv_no) merchant_name,
        '-' PURCHASE_REMARKS
        FROM LZ_SPECIAL_LOTS L,
        LZ_BD_OBJECTS_MT OB,
        LZ_ITEM_COND_MT C,
        LZ_CATALOGUE_MT CA
        WHERE L.OBJECT_ID = OB.OBJECT_ID(+)
        AND L.CONDITION_ID = C.ID(+)
        AND L.CATALOG_MT_ID = CA.CATALOGUE_MT_ID(+)
        /* ORDER BY LZ_MANIFEST_DET_ID NULLS FIRST)*/
        union all
        SELECT --b.barcode_no,
        'Purch app' BAROCDE_TYPE,
        'POSTED' VERIFY_ITEM,
        pit.purch_mt_id LZ_DEKIT_US_DT_ID,
        B.BARCODE_NO BARCODE_PRV_NO,

        cm.cond_name,
        pit.condition ID,
        '' OBJECT_NAME,
        '' OBJECT_ID,
        '' MPN_DESC,
        '' MPN,
        '' UPC,
        '' BRAND,
        '' FOLDER_NAME,
        '' REMARKS,
        '' US_REMARKS,
        b.lz_manifest_id LZ_MANIFEST_DET_ID, /*lz_manifest_id */
        '' category_id,
        TO_CHAR(pit.cost) AVG_PRIC,

        '-' merchant_name,
         pit.remarks PURCHASE_REMARKS
        FROM lz_merchant_barcode_dt bdt,
        lz_purch_item_dt dd,
        lz_barcode_mt b,
        lj_barcode_tag_mt tm,
        lz_item_cond_mt cm,
        lz_purch_item_mt pit
        WHERE bdt.barcode_no = dd.barcode_no
        AND b.barcode_no(+) = bdt.barcode_no
        AND pit.purch_mt_id = dd.purch_mt_id
        and bdt.tag_id = tm.tag_id(+)
        AND cm.id = pit.condition
        union all
        select *
        from (SELECT 'Single Entry' BAROCDE_TYPE,
        'POSTED' VERIFY_ITEM,
        s.id LZ_DEKIT_US_DT_ID,
        (SELECT MT.BARCODE_NO
        FROM LZ_BARCODE_MT MT
        WHERE MT.ITEM_ID = MANF.ITEM_ID
        AND MT.LZ_MANIFEST_ID = MANF.LZ_MANIFEST_ID
        AND mt.CONDITION_ID = C.ID
        /*and mt.ebay_item_id is null*/
        and mt.barcode_no = '$barcode') BARCODE_PRV_NO,
        C.COND_NAME,
        C.ID,

        o.object_name OBJECT_NAME,
        to_char(o.object_id) OBJECT_ID, /*'' PIC_DATE_TIME '' PIC_BY, '' PIC_NOTES,*/

        s.item_mt_desc MPN_DESC,
        s.item_mt_mfg_part_no MPN,
        s.item_mt_upc UPC,
        s.item_mt_manufacture BRAND,
        '' FOLDER_NAME,
        '' REMARKS,
        '' US_REMARKS,
        m.lz_manifest_id LZ_MANIFEST_DET_ID, /*lz_manifest_id */
        to_char(s.category_id) category_id,
        TO_CHAR(s.price) AVG_PRIC,
        '-' merchant_name,
        '-' PURCHASE_REMARKS
        FROM LZ_SINGLE_ENTRY S,
        LZ_MANIFEST_MT M,
        LZ_ITEM_COND_MT C,
        LZ_ITEM_SEED I,
        LZ_BD_OBJECTS_MT O,
        (SELECT DISTINCT B.LZ_MANIFEST_ID,
        B.EBAY_ITEM_ID,
        B.ITEM_ID
        FROM LZ_BARCODE_MT B
        WHERE B.LZ_MANIFEST_ID IN
        (SELECT M.LZ_MANIFEST_ID
        FROM LZ_SINGLE_ENTRY S, LZ_MANIFEST_MT M
        WHERE M.SINGLE_ENTRY_ID = S.ID)
        /*AND B.EBAY_ITEM_ID IS NULL*/
        ) MANF
        WHERE M.SINGLE_ENTRY_ID = S.ID
        AND MANF.LZ_MANIFEST_ID = M.LZ_MANIFEST_ID
        AND S.OBJECT_ID = O.OBJECT_ID(+)
        AND S.POS_ONLY = 0
        /*AND UPPER(S.CONDITIONS_SEG5) = UPPER(C.COND_NAME)*/
        AND MANF.ITEM_ID = I.ITEM_ID(+)
        AND MANF.LZ_MANIFEST_ID = I.LZ_MANIFEST_ID(+)
        AND C.ID = I.DEFAULT_COND(+)

        )) BR

        -- WHERE DDD.BARCODE_NO = BARCODE_PRV_NO
        WHERE BARCODE_PRV_NO = DDD.BARCODE_NO (+)
        AND BARCODE_PRV_NO = $barcode")->result_array();

        $get_cond = $this->db->query('SELECT CC.ID CONDITION_ID,CC.COND_NAME FROM LZ_ITEM_COND_MT CC')->result_array();

        $get_tag_data = $this->db->query('SELECT tag.tag_id,tag.tag_desc from lj_barcode_tag_mt tag  where tag.active_yn = 1')->result_array();

        if (count($get_items) >= 1) {
            //var_dump('helllo');

            //$get_items = $query->result_array();
            $barocde_type = $get_items[0]['BAROCDE_TYPE'];

            if ($barocde_type == 'DEKIT ITEM') {

                $run_mast = $this->db->query("SELECT M.BARCODE_NO MASTER_BARCODE FROM LZ_DEKIT_US_MT M ,LZ_DEKIT_US_DT D WHERE M.LZ_DEKIT_US_MT_ID  = D.LZ_DEKIT_US_MT_ID AND D.BARCODE_PRV_NO = '$barcode' ")->result_array();

                $get_master_barc = $run_mast[0]['MASTER_BARCODE'];

                $run_master_bar_query = $this->db->query("SELECT B.BARCODE_NO,C.COND_NAME,I.ITEM_DESC,I.ITEM_MT_MANUFACTURE,I.ITEM_MT_MFG_PART_NO FROM LZ_BARCODE_MT B, ITEMS_MT I,LZ_ITEM_COND_MT C WHERE B.BARCODE_NO = '$get_master_barc' AND B.CONDITION_ID = C.ID(+) AND B.ITEM_ID = I.ITEM_ID")->result_array();

            } else {
                $run_master_bar_query = null;
            }

            $seed_avail = $this->db->query("SELECT B.NOT_FOUND_BY,
            B.DISCARD,
            B.DISCARD_REMARKS,
            B.DISCARD_BY,
            B.HOLD_STATUS,
            B.ORDER_ID,
            B.LZ_POS_MT_ID,
            B.LZ_PART_ISSUE_MT_ID,
            B.EBAY_ITEM_ID,
            b.LZ_DEKIT_US_MT_ID_FOR_OUT,
            b.ITEM_ADJ_DET_ID_FOR_OUT,
            b.CREATED_TRHOUGH,
            b.K2BAY_ORDER_ID,
            b.K2BAY_PRODUCT_ID
       FROM LZ_BARCODE_MT B WHERE B.BARCODE_NO = '$barcode' ")->result_array();
            $not_found = false;
            if (count($seed_avail) >= 1) {
                // count($seed_avail) >= 1
                $no_found_by = '';
                $discard_remarks = '';
                if ($seed_avail[0]['NOT_FOUND_BY'] !== null) {
                    $no_found_by = $seed_avail[0]['NOT_FOUND_BY'];
                    // $discard_remarks = $seed_avail[0]['DISCARD_REMARKS'];
                    $discard_remarks = 'This barcode is marked as not found by Audit person';
                    $not_found = true;

                } else if ($seed_avail[0]['DISCARD'] == 1) {
                    $no_found_by = $seed_avail[0]['DISCARD_BY'];
                    $discard_remarks = $seed_avail[0]['DISCARD_REMARKS'];
                    $discard_remarks = "Barcode Discarded: " . $discard_remarks;
                    $not_found = true;

                } else if ($seed_avail[0]['HOLD_STATUS'] == 1) {
                    $no_found_by = $seed_avail[0]['DISCARD_BY'];
                    $discard_remarks = 'This Barcode is on Hold';
                    $not_found = true;
                } else if ($seed_avail[0]['ORDER_ID'] !== null) {
                    $no_found_by = $seed_avail[0]['ORDER_ID'];
                    $ebay_id = $seed_avail[0]['EBAY_ITEM_ID'];
                    $discard_remarks = "This Barcode is Sold on Ebay Against Order # : " . $no_found_by . " and Item Id:" . $ebay_id;
                    $not_found = true;
                }else if ($seed_avail[0]['K2BAY_ORDER_ID'] !== null) {
                    $no_found_by = $seed_avail[0]['K2BAY_ORDER_ID'];
                    $ebay_id = $seed_avail[0]['K2BAY_PRODUCT_ID'];
                    $discard_remarks = "This Barcode is Sold on K2bay Against Order # : " . $no_found_by . " and Item Id:" . $ebay_id;
                    $not_found = true;
                } else if ($seed_avail[0]['LZ_POS_MT_ID'] !== null) {
                    $no_found_by = $seed_avail[0]['LZ_POS_MT_ID'];
                    $discard_remarks = "This Barcode is Sold in POS. POS ID: " . $no_found_by;
                    $not_found = true;
                } else if ($seed_avail[0]['LZ_PART_ISSUE_MT_ID'] !== null) {
                    $no_found_by = $seed_avail[0]['LZ_PART_ISSUE_MT_ID'];
                    $discard_remarks = "This Barcode is Sold in Service as a Part. Part Issue MT id: " . $no_found_by;
                    $not_found = true;
                } else if ($seed_avail[0]['LZ_DEKIT_US_MT_ID_FOR_OUT'] !== null) {
                    $no_found_by = $seed_avail[0]['LZ_PART_ISSUE_MT_ID'];
                    $discard_remarks = "This Barcode is dekited ! Do not list " . $no_found_by;
                    $not_found = true;

                } else if ($seed_avail[0]['ITEM_ADJ_DET_ID_FOR_OUT'] !== null) {
                    $no_found_by = $seed_avail[0]['LZ_PART_ISSUE_MT_ID'];
                    $discard_remarks = "Barcode is consumed as a lot ! Do not list " . $no_found_by;
                    $not_found = true;

                } else if ($seed_avail[0]['CREATED_TRHOUGH'] !== null) {
                    $no_found_by = $seed_avail[0]['CREATED_TRHOUGH'];
                    $discard_remarks = "Barcode is system Generated ! Do not List " . $no_found_by;
                    $not_found = true;

                }

                return array('get_items' => $get_items, 'get_cond' => $get_cond, 'run_master_bar_query' => $run_master_bar_query, 'exist' => true, 'seed_avail' => true, 'get_tag_data' => $get_tag_data, 'not_found_by' => $no_found_by, 'discard_remarks' => $discard_remarks, 'not_found' => $not_found, 'deleeteAcces' => $deleAcces);
                // return array('get_items' => $get_items, 'get_cond' => $get_cond, 'run_master_bar_query' => $run_master_bar_query, 'exist' => true, 'seed_avail' => true, 'get_tag_data' => $get_tag_data, 'not_found' => false);

            } else {
                return array('get_items' => $get_items, 'get_cond' => $get_cond, 'run_master_bar_query' => $run_master_bar_query, 'exist' => true, 'seed_avail' => false, 'get_tag_data' => $get_tag_data, 'not_found' => false, 'deleeteAcces' => $deleAcces);

            }

        } else {
            $not_found = true;
            $chk_delete = $this->db->query("select cm.user_name from LZ_DELETION_LOG t,employee_mt cm where t.deleted_by = cm.employee_id  and  t.barcode  = '$barcode'  and rownum<=1")->result_array();if (count($chk_delete) > 0) {
                $discard_remarks = "Barcode Is Deleted by ( " . $chk_delete[0]['USER_NAME'] . ' )';

            } else {
                $chk_pics = $this->db->query("select * from lz_merchant_barcode_dt md where md.barcode_no = '$barcode' and md.barcode_no not in (select ll.barcode_prv_no from lz_special_lots ll ) and md.barcode_no not in (select dt.barcode_prv_no  from lz_dekit_us_dt dt) and md.barcode_no not in (select j.barcode_no from lz_barcode_mt j) and md.barcode_no not in (select pd.barcode_no from lz_purch_item_dt pd )")->result_array();
                if (count($chk_pics) < 0) {
                    $discard_remarks = "Picture Required";

                } else {
                    $discard_remarks = "";
                }
            }

            return array('get_items' => $get_items, 'exist' => false, 'not_found' => $not_found, 'discard_remarks' => $discard_remarks, 'deleeteAcces' => $deleAcces);
        }

        return $barcode; //var_dump($barcode);
        //exit;

    }

    public function DeleteBarcodeReact()
    {
        $barcode = $this->input->post('get_barcode');
        $user_id = $this->input->post('user_id');
        $delRemarks = $this->input->post('delRemarks');
        $delRemarks = trim(str_replace("  ", ' ', $delRemarks));
        $delRemarks = str_replace(array("`,'"), "", $delRemarks);
        $delRemarks = str_replace(array("'"), "''", $delRemarks);

        $remarks = 'null';

        $query = $this->db->query("call  Pro_Unpost_barcode($barcode,$user_id,'$delRemarks')");
        if ($query) {
            $deleted = true;
            return array('deletedPrcde' => $deleted, 'message' => 'Barcode is Deleted');
        } else {
            $deleted = true;
            return array('deletedPrcde' => $deleted, 'message' => 'Barcode Not Deleted');
        }

    }

    public function merch_lot_dash()
    {
        $merchId = $this->input->post('merchId');

        $merchLotData = $this->db->query("SELECT LOT_ID,
       MAX(REF_NO) REF_NO,
       MAX(LOT_DESCRIPTION) LOT_DESCRIPTION,
       MAX(CREATION_DATE) CREATION_DATE,
       MAX(LOT_COST) LOT_COST,
       COUNT(LOT_COUNT) LOT_COUNT,
       COUNT(EBAY_ITEM_ID) TOTAL_LISTED,
      -- SUM(LIST_VAL) TOTAL_LIST_AMOUNT,
      NVL(SUM(LIST_VAL), 0) TOTAL_LIST_AMOUNT,
       COUNT(EBAY_ITEM_ID) - COUNT(ORDER_ID) ACTIV_LISTED,
       SUM(LIST_VAL) - SUM(SOLD_VALUE) ACTIV_LIST_AMOUNT,
       COUNT(ORDER_ID) TOTAL_SOLD,
       NVL(SUM(SOLD_VALUE), 0) SOLD_AMOUNT,
       round(NVL(SUM(PIC_CHARGE) + SUM(LIST_CHARGE) + SUM(PACK_PULL_CHARGE), 0),2) LJ_CHARGES,
       NVL(SUM(EXPENSE), 0) EXPENSES,
       NVL(SUM(EXPENSE), 0) +
       round(NVL(SUM(PIC_CHARGE) + SUM(LIST_CHARGE) + SUM(PACK_PULL_CHARGE), 0),2) CHRG_OR_EXPENS,
       /*NVL(SUM(SOLD_VALUE), 0) -
       (NVL(SUM(PIC_CHARGE) + SUM(LIST_CHARGE) + SUM(PACK_PULL_CHARGE), 0) +
        NVL(SUM(EXPENSE), 0)) NET_EARNING*/
        NVL(SUM(LIST_VAL), 0) -
       (NVL(SUM(PIC_CHARGE) + SUM(LIST_CHARGE) + SUM(PACK_PULL_CHARGE), 0) +
        NVL(SUM(EXPENSE), 0)) projected_earning
  FROM (SELECT M.LOT_ID,
               B.EBAY_ITEM_ID,

               M.NO_OF_BARCODE,
               (SELECT NVL(SD.EBAY_PRICE, 0)
                  FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD
                 WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID
                   AND MM.ITEM_ID = SD.ITEM_ID
                   AND MM.CONDITION_ID = SD.DEFAULT_COND
                   AND MM.BARCODE_NO = B.BARCODE_NO
                   AND B.EBAY_ITEM_ID IS NOT NULL) LIST_VAL,
               (SELECT NVL(SD.EBAY_PRICE, 0)
                  FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD
                 WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID
                   AND MM.ITEM_ID = SD.ITEM_ID
                   AND MM.CONDITION_ID = SD.DEFAULT_COND
                   AND MM.BARCODE_NO = B.BARCODE_NO
                   AND B.EBAY_ITEM_ID IS NOT NULL
                   AND B.ORDER_ID IS NOT NULL) SOLD_VALUE,
               (SELECT NVL(BIL.CHARGES, 0)
                  FROM LJ_BARCODE_BILLING BIL
                 WHERE BIL.BARCODE_NO = L.BARCODE_PRV_NO /*B.BARCODE_NO*/
                   AND BIL.SER_RATE_ID = 1) PIC_CHARGE, /*PIC SERVICE CHARGE*/
               (SELECT NVL(BIL.CHARGES, 0)
                  FROM LJ_BARCODE_BILLING BIL, LZ_BARCODE_MT BAR
                 WHERE BIL.BARCODE_NO = B.BARCODE_NO
                   AND BIL.BARCODE_NO = BAR.BARCODE_NO
                   AND BIL.SER_RATE_ID = 2
                   AND BAR.EBAY_ITEM_ID IS NOT NULL) LIST_CHARGE, /*LIST SERVICE CHARGE*/
               (SELECT NVL(BIL.CHARGES, 0)
                  FROM LJ_BARCODE_BILLING BIL, LZ_BARCODE_MT BAR
                 WHERE BIL.BARCODE_NO = B.BARCODE_NO
                   AND BIL.BARCODE_NO = BAR.BARCODE_NO
                   AND BIL.SER_RATE_ID = 6
                   AND BAR.EBAY_ITEM_ID IS NOT NULL) BIN_STORAGE_CHARGE, /*BIN STORAGE SERVICE CHARGE*/
               (SELECT NVL(BIL.CHARGES, 0)
                  FROM LJ_BARCODE_BILLING BIL, LZ_BARCODE_MT BAR
                 WHERE BIL.BARCODE_NO = B.BARCODE_NO
                   AND BIL.BARCODE_NO = BAR.BARCODE_NO
                   AND BIL.SER_RATE_ID = 7
                   AND BAR.EBAY_ITEM_ID IS NOT NULL) PACK_PULL_CHARGE, /*PACK AND PULL STORAGE SERVICE CHARGE*/
               round((SELECT NVL(SUM(OD.PACKING_COST) +
                           NVL(MAX(OM.SHIPING_LABEL_RATE), 0) +
                           NVL(MAX(OM.EBAY_FEE), 0) +
                           NVL((7 / 100) *
                               NVL(MAX(OM.SALE_PRICE * OM.QTY), 0),
                               0),
                           0)
                  FROM LJ_ORDER_PACKING_MT OM, LJ_ORDER_PACKING_DT OD
                 WHERE OM.ORDER_PACKING_ID = OD.ORDER_PACKING_ID
                   AND OM.ORDER_ID = B.ORDER_ID
                 GROUP BY OD.ORDER_PACKING_ID) /
               (SELECT NVL(MAX(OM.QTY), 0)
                  FROM LJ_ORDER_PACKING_MT OM, LJ_ORDER_PACKING_DT OD
                 WHERE OM.ORDER_PACKING_ID = OD.ORDER_PACKING_ID
                   AND OM.ORDER_ID = B.ORDER_ID
                 GROUP BY OD.ORDER_PACKING_ID),2) EXPENSE,
               (SELECT NVL(MAX(OM.QTY), 0)
                  FROM LJ_ORDER_PACKING_MT OM, LJ_ORDER_PACKING_DT OD
                 WHERE OM.ORDER_PACKING_ID = OD.ORDER_PACKING_ID
                   AND OM.ORDER_ID = B.ORDER_ID
                 GROUP BY OD.ORDER_PACKING_ID) QTY,
               MD.REF_NO REF_NO,
               MD.LOT_DESC LOT_DESCRIPTION,
               MD.ASSIGN_DATE CREATION_DATE,
               MD.COST LOT_COST,
               D.BARCODE_NO LOT_COUNT,
               B.ORDER_ID,
               B.SALE_RECORD_NO
          FROM LOT_DEFINATION_MT      MD,
               LZ_MERCHANT_BARCODE_MT M,
               LZ_MERCHANT_BARCODE_DT D,
               LZ_SPECIAL_LOTS        L,
               LZ_BARCODE_MT          B
         WHERE M.MT_ID = D.MT_ID
           AND MD.LOT_ID = M.LOT_ID
           AND D.BARCODE_NO = L.BARCODE_PRV_NO(+)
           AND L.BARCODE_PRV_NO = B.BARCODE_NO(+)
           AND M.MERCHANT_ID = $merchId)
 GROUP BY LOT_ID
")->result_array();

        if (count($merchLotData) >= 1) {
            return array('merchLotData' => $merchLotData, 'exist' => true);
        } else {
            return array('merchLotData' => $merchLotData, 'exist' => false);
        }

    }

    public function merch_lot_detail_view()
    {

        $getSeach = strtoupper($this->input->post('getSeach'));
        $recntSold = strtoupper($this->input->post('recntSold'));

        //$recntSold = strtoupper($this->input->post('recntSold'));
        //var_dump($recntSold);
        // exit;

        $getSeach = trim(str_replace("  ", ' ', $getSeach));
        $getSeach = str_replace(array("`,'"), "", $getSeach);
        $getSeach = str_replace(array("'"), "''", $getSeach);

        $priceOne = strtoupper($this->input->post('priceOne'));
        $priceOne = trim(str_replace("  ", ' ', $priceOne));
        $priceOne = str_replace(array("`,'"), "", $priceOne);
        $priceOne = str_replace(array("'"), "''", $priceOne);

        $priceTwo = strtoupper($this->input->post('priceTwo'));
        $priceTwo = trim(str_replace("  ", ' ', $priceTwo));
        $priceTwo = str_replace(array("`,'"), "", $priceTwo);
        $priceTwo = str_replace(array("'"), "''", $priceTwo);

        $lot_id = ($this->input->post('merLotName'));
        //var_dump($lot_id);
        $merLotName = '';

        if (!empty($lot_id)) {

            // if (is_array($lot_id)) {
            //     if (sizeof($lot_id) > 1) {
            // $cond_exp = '';
            $i = 0;
            foreach ($lot_id as $lot) {
                if (!empty($lot_id[$i + 1])) {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'" . ',';

                } else {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'";
                }
                $i++;

            }

        }

        // if (!empty($merLotSelect)) {
        //     $i = 0;
        //     foreach ($merLotSelect as $lot) {
        //         if (!empty($merLotSelect[$i + 1])) {
        //             $merLotName = $merLotName . $lot['value'] . ',';

        //         } else {
        //             $merLotName = $merLotName . $lot['value'];
        //         }
        //         $i++;

        //     }
        // }

        // $merLotName = '';
        // if (!empty($merLotSelect)) {

        //     if (sizeof($merLotSelect) > 1) {
        //         // $cond_exp = '';
        //         $i = 0;
        //         foreach ($merLotSelect as $lot) {
        //             if (!empty($merLotSelect[$i + 1])) {
        //                 $merLotName = $merLotName . $lot['value'] . ',';

        //             } else {
        //                 $merLotName = $merLotName . $lot['value'];
        //             }
        //             $i++;

        //         }

        //     } else {
        //         $merLotName = $merLotName . $merLotSelect[0]['value'];
        //     }
        // }
        // $merLot Name = '82';
        $merchId = strtoupper($this->input->post('merchId'));
        // $merchId = '9';
        $str = explode(' ', $getSeach);
        //var_dump($getSeach);

        $lot_name = $this->db->query("SELECT M.LOT_ID, M.LOT_DESC  LOT_DESC FROM LOT_DEFINATION_MT M WHERE M.MERCHANT_ID  =$merchId order by M.LOT_ID desc ")->result_array();

        $sqlOne = "SELECT lot_id,
        max(ref_no) ref_no,
        max(created_date) created_date,
        max(lot_desc)  ||' ('||  max(created_date) ||')' lot_description,

        max(nvl(lot_cost,0)) lot_cost,
        count(generated_bar) generated_barc,
        count(generated_bar) - count(picture_barc) pictures_not_done,
        count(picture_barc) picture_done,
        count(POSTED_BARC) verified_barc,
        count(picture_barc) - count(POSTED_BARC) not_verified,
        count(LISTED_BARC) LISTED_BARC,
        sum(nvl(list_pric, 0))-sum(nvl(gg,0)) listed_price,/*need to verify acgain not shwoing corect value chappi*/
        count(POSTED_BARC) - count(LISTED_BARC) - count(POS_SOLD) not_listed,
        count(LISTED_BARC) - count(EBAY_SOLD) active_listed,
        sum(nvl(list_pric, 0)) - sum(nvl(SALE_PRICE, 0)) - sum(nvl(gg,0))/*need to verify  sum(nvl(gg,0)) acgain not shwoing corect value chappi*/ active_listed_value,
        /*sum(nvl(list_pric,0)) - sum(nvl(SALE_PRICE,0)) active_listed_value,*/
        count(awt_orders) awaiting_orders,
       round(sum(nvl(awt_ord_price,0)),2) await_order_value,


       count(EBAY_SOLD) -count(RETURN_BARCO) sold_items,
        sum(nvl(SALE_PRICE, 0)) - sum(nvl(return_amount, 0)) SALE_PRICE,
/*count(EBAY_SOLD) sold_items,
sum(nvl(SALE_PRICE,0)) SALE_PRICE,*/

        count(POS_SOLD) pos_sold,
        sum(nvl(POS_SALE_PRICE,0)) POS_SALE_PRICE,
        count(RETURN_BARCO) return_barc,
        sum(nvl(return_amount,0)) return_amount,
         count(cancel_Item)cancel_Items,
       sum(nvl(cANCEL_AMOUNT,0)) cANCEL_AMOUNT,
        sum(nvl(SALE_PRICE,0)) + sum(nvl(POS_SALE_PRICE,0)) - sum(nvl(return_amount,0)) total_sales,
        sum(nvl(pic_charge,0)) pic_charge,
       sum(nvl(TOTAL_list_chrge,0)) list_charge,
       sum(nvl(get_order_expense,0)) order_Charge

        from (select lo.lot_id,
        lo.ref_no ref_no,
        lo.lot_desc,
        lo.cost lot_cost,
        lo.created_date   created_date,
        d.barcode_no generated_bar,
        l.barcode_prv_no picture_barc,
        l.pic_date_time,
        list_price.list_pric - det.sale_price gg ,
        b.barcode_no posted_barc,
        b.ebay_item_id listed_barc,
        list_price.list_pric,
        awt_ship.orderlineitemid awt_orders,
        awt_ship.awt_ord_price,
         cancel_item.barcode_no cancel_Item,
       cancel_item.NO_OF_TIM_CACEL,
       cancel_item.cANCEL_AMOUNT,
        b.order_id ebay_sold,
        l.folder_name,
        get_sale_expn.total get_order_expense,
        list_charge.TOTAL_list_chrge TOTAL_list_chrge,
        det.sale_price,
        ps.barcode_id pos_sold,
        ps.pos_sale_price,
        b.returned_id return_barco,
        decode(b.returned_id ,null,'',det.sale_price) return_amount,
        /*retur_items.barcode_no return_barco,
        retur_items.return_amount,*/

        end_item.no_of_time_ended,
        end_item.end_barcode,
        get_pic_charge.pic_charge

        from lz_merchant_barcode_mt m,
        lz_merchant_barcode_dt d,
        lot_defination_mt lo,
        lz_special_lots l,
        lz_barcode_mt b,
        lz_salesload_det det,
        lz_item_seed sed,
        lz_item_cond_mt cm,

        (SELECT
                case when PICTURES_DONE > 0 then  round(NVL(UNIQU_QTY_CHARG + REMAIN_QTY_CHARG, 0) /
                             PICTURES_DONE)
                             else PICTURES_DONE
                               end pic_charge,


            folder_name

        FROM (SELECT
        COUNT(L.FOLDER_NAME) PICTURES_DONE,
        round(max(get_rates_pict.rate), 2) rate,
        round(max(get_rates_pict.excess_qty_rate), 2) excess_qty_rate,
        COUNT(DISTINCT L.FOLDER_NAME) UNIQUE_PICS,
        COUNT(L.FOLDER_NAME) -
        COUNT(DISTINCT L.FOLDER_NAME) REMAINING_PICS,
        COUNT(DISTINCT L.FOLDER_NAME) *
        round(max(get_rates_pict.rate), 2) UNIQU_QTY_CHARG,
        (COUNT(L.FOLDER_NAME) -
        COUNT(DISTINCT L.FOLDER_NAME)) *
        round(max(get_rates_pict.excess_qty_rate), 2) REMAIN_QTY_CHARG,
        l.folder_name
        FROM LZ_MERCHANT_BARCODE_MT M,
        LZ_MERCHANT_BARCODE_DT D,
        LZ_SPECIAL_LOTS L,
        (select ms.merchant_id,
        ms.ser_rate_id,
        ms.rate,
        ms.excess_qty_rate
        from lj_merchant_service ms
        where ms.merchant_id = '$merchId'
        and ms.ser_rate_id = 1
        and rownum <= 1) get_rates_pict
        WHERE M.MT_ID = D.MT_ID

        AND D.BARCODE_NO = L.BARCODE_PRV_NO
        and m.merchant_id = get_rates_pict.merchant_id(+)
        AND M.MERCHANT_ID = '$merchId'
        group by l.folder_name)) get_pic_charge,
      ";

        $sqlOne .= " /*sales expense datga */
               (SELECT order_id,
                       round(NVL(SHIPING_COST + PACKING_COST + SERV_COST +
                                 EBAY_FEE + MARKETPLACE_7,
                                 0) / quantity,
                             2) TOTAL
                  FROM (SELECT max(sd.order_id) order_id,
                               max(sd.quantity) quantity,
                               COUNT(sd.ORDER_ID) TOTAL_ORDER,
                               max(NVL(sD.SALE_PRICE, 0) * NVL(sD.Quantity, 0)) SALE_PRI,
                               max(NVL(sd.shippinglabelrate, 0)) SHIPING_COST,
                               max(NVL(GET_PACK_COST.PAC_COST, 0)) PACKING_COST,
                               max(NVL(sD.SERVICE_COST, 0)) SERV_COST,

                               decode(max(acount_Det.acount_name),
                                      'dfwonline',
                                      max(NVL(sd.ebay_fee_perc, 0)),
                                      0) EBAY_FEE,
                               decode(max(acount_Det.acount_name),
                                      'dfwonline',
                                      ROUND(0.07 * NVL(max(NVL(sd.SALE_PRICE, 0) *
                                                           NVL(sd.quantity, 0)),
                                                       0),
                                            2),
                                      0) MARKETPLACE_7
                          FROM LZ_SALESLOAD_DET sd,
                               (SELECT SUM(D.PACKING_COST) PAC_COST, d.order_id
                                  FROM LJ_ORDER_PACKING_DT D
                                 GROUP BY D.order_id) GET_PACK_COST,
                               (SELECT MM.EBAY_ITEM_ID,
                                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME
                                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
                         WHERE sd.order_id in
                               (select d.order_id
                                  from ebay_list_mt       e,
                                       lj_merhcant_acc_dt a,
                                       lz_salesload_det   d
                                 where a.acct_id = e.lz_seller_acct_id
                                   and d.item_id = e.ebay_item_id
                                   and a.merchant_id = '$merchId'
                                 group by d.order_id)
                           and sd.order_id = GET_PACK_COST.order_id(+)
                           and sd.orderstatus = 'Completed'
                           and sd.return_id is null
                           AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
                           AND sd.SALE_DATE BETWEEN
                               TO_DATE('2020-01-01 00:00:00',
                                       'YYYY-MM-DD HH24:MI:SS') AND
                               TO_DATE('2020-02-29 23:59:59',
                                       'YYYY-MM-DD HH24:MI:SS')
                         GROUP BY sd.order_id)

                ) get_sale_expn,";

        $sqlOne .= "/*get list charge */(SELECT
            case when TOTAL_QTY_LIST >0 then
                       NVL(TOTAL_UNIQ_QTY_CHARGE + REMAING_QTY_CHARGE, 0) /
                       TOTAL_QTY_LIST else
                        TOTAL_QTY_LIST
                        end TOTAL_list_chrge,
                       ebay_item_id
          FROM (SELECT SUM(T.LIST_QTY) TOTAL_QTY_LIST,
                       round(max(get_rates_pict.rate), 2) rate,
                       round(max(get_rates_pict.excess_qty_rate), 2) excess_qty_rate,
                       COUNT(DISTINCT T.EBAY_ITEM_ID) TOTAL_UNIQ_QTY,
                       SUM(T.LIST_QTY) - COUNT(DISTINCT T.EBAY_ITEM_ID) REMAING_QTY,
                       COUNT(DISTINCT T.EBAY_ITEM_ID) *
                       round(max(get_rates_pict.rate), 2) TOTAL_UNIQ_QTY_CHARGE,
                       (SUM(T.LIST_QTY) - COUNT(DISTINCT T.EBAY_ITEM_ID)) *
                       round(max(get_rates_pict.excess_qty_rate), 2) REMAING_QTY_CHARGE,
                       t.ebay_item_id
                  FROM EBAY_LIST_MT T,
                       LJ_MERHCANT_ACC_DT DD,
                       LZ_LISTED_ITEM_URL L,
                       (select ms.merchant_id,
                               ms.ser_rate_id,
                               ms.rate,
                               ms.excess_qty_rate
                          from lj_merchant_service ms
                         where ms.merchant_id = '$merchId'
                           and ms.ser_rate_id = 2
                           and rownum <= 1) get_rates_pict
                 WHERE T.EBAY_ITEM_ID = L.EBAY_ID
                   AND DD.ACCT_ID = T.LZ_SELLER_ACCT_ID(+)
                   AND T.LZ_SELLER_ACCT_ID IS NOT NULL

                   and dd.merchant_id = get_rates_pict.merchant_id(+)
                   AND DD.MERCHANT_ID = '$merchId'
                   AND T.STATUS IN ('ADD', 'REVISED')
                  GROUP BY t.ebay_item_id)

        ) list_charge,";

        $sqlOne .= " (select po.barcode_id,
            round(nvl(po.price, 0) + (nvl(po.price, 0) / 100) *
            nvl(po.sales_tax_perc, 0) - nvl(po.disc_amt, 0),
            2) pos_sale_price
            from lz_pos_det po
            where po.barcode_id is not null
            AND po.deleted_by IS NULL
            AND po.return_by IS NULL
            ) ps,
            (select li.ebay_item_id,
            li.ebay_item_desc title,
            li.list_price list_pric /*,
            list_qty_charge.TOTAL listing_charge*/
            from ebay_list_mt li
            where li.list_id in (select max(e.list_id) list_id
            from ebay_list_mt e
            group by e.ebay_item_id)

            ) list_price,
            /*(select k.barcode_no,max(sk.sale_price)/max(sk.quantity) return_amount
            from lj_returned_barcode_mt k,lz_salesload_det sk
            where k.order_id = sk.order_id
            group by k.barcode_no) retur_items,*/

            (select count(ll.barcode_no) no_of_time_ended,
            ll.barcode_no end_barcode
            from LJ_AGINIG_ITEM_LOG ll
            group by ll.barcode_no) end_item,
            (select aw.orderlineitemid,ap.sale_price/ap.quantity awt_ord_price
            from LZ_AWAITING_SHIPMENT aw,lz_salesload_det ap
            where aw.orderlineitemid = ap.order_id) awt_ship,
            (select BARCODE_NO,
               count(BARCODE_NO) no_of_tim_cacel,
               sum(canccel_amount) cancel_amount
          from (select i.barcode_no,
                       u.quantity,
                       u.sale_price,
                       round(u.sale_price / u.quantity, 2) canccel_amount,
                       u.orderstatus,
                       u.order_id,
                       u.cancel_id
                  from lj_cancellation_dt i, lz_salesload_det u
                 where i.order_id = u.order_id(+)
                 order by i.barcode_no asc)
         group by BARCODE_NO) cancel_item

        where m.mt_id = d.mt_id
        and m.lot_id = lo.lot_id
        and l.folder_name = get_pic_charge.folder_name(+)
        and d.barcode_no = l.barcode_prv_no
        and l.barcode_prv_no = b.barcode_no(+)

        and b.barcode_no = cancel_item.barcode_no(+)
        and b.order_id = awt_ship.orderlineitemid(+)
        and b.order_id = det.order_id(+)
        and b.barcode_no = ps.barcode_id(+)

        and b.item_id = sed.item_id(+)
        and b.lz_manifest_id = sed.lz_manifest_id(+)
        and b.condition_id = sed.default_cond(+)
        and sed.default_cond = cm.id(+)
        and b.order_id = get_sale_expn.order_id(+)

        /*and b.barcode_no = retur_items.barcode_no(+)*/
        and b.ebay_item_id = list_price.ebay_item_id(+)
        and b.barcode_no = end_item.end_barcode(+)
        and b.ebay_item_id = list_charge.ebay_item_id(+)
        and m.merchant_id = '$merchId' ";

        if (!empty($getSeach)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $sqlOne .= " and (UPPER(sed.item_title) LIKE '%$key%' ";
                    } else {
                        $sqlOne .= " AND UPPER(sed.item_title) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $sqlOne .= " and (UPPER(sed.item_title) LIKE '%$getSeach%' ";
            }

            $sqlOne .= " OR UPPER(d.BARCODE_NO) LIKE '%$getSeach%'
            OR UPPER(det.extendedorderid) LIKE '%$getSeach%'
            OR UPPER(b.EBAY_ITEM_ID) LIKE '%$getSeach%' )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }
        // var_dump($merLotName);

        if (!empty($lot_id)) {
            $sqlOne .= " and trim(upper(lo.LOT_DESC )) in ($merLotName)  ";

        }

        // if (!empty($merLotName)) {
        //     $sqlOne .= " and lo.lot_id IN ($merLotName) ";
        // }
        $sqlOne .= " order by l.pic_date_time asc) ";

        $sqlOne .= "  GROUP BY LOT_ID order by LOT_ID";

        $lot_detail_query = $this->db->query($sqlOne)->result_array();

//         $sqlTwo = " SELECT COUNT(BARCODE_NO) TOT_BARC,
        //        COUNT(PIC_DATE_TIME) PICTURS_DONE,
        //        COUNT(LZ_MANIFEST_DET_ID) BARCODE_CREATED,
        //        COUNT(EBAY_ITEM_ID) - COUNT(SALE_RECORD_NO) ACTIVE_LISTED,
        //        COUNT(LZ_MANIFEST_DET_ID) - COUNT(EBAY_ITEM_ID) - COUNT(OUTT) NOT_LISTED,
        //        COUNT(SALE_RECORD_NO) SOLD,
        //        ROUND(NVL(SUM(TOTAL_VAL), 0), 2) PROCESED_QTY_VAL, /*round(SUM(nvl(LIST_VAL, 0)) - SUM(nvl(SOLD_VALUE, 0)), 2) ACTIV_LIST_QTY_VAL,*/
        //        case
        //          when COUNT(EBAY_ITEM_ID) - COUNT(SALE_RECORD_NO) <= 0 then
        //           0
        //          else
        //           round(SUM(nvl(new_lis, 0)) - SUM(nvl(SOLD_VALUE, 0)), 2)
        //        end ACTIV_LIST_QTY_VAL,
        //        ROUND(NVL(SUM(NOT_LIST_VAL), 0), 2) ACTIV_NOT_LIST_QTY_VAL,
        //        ROUND(NVL(SUM(SOLD_VALUE), 0), 2) SOLD_QTY_VAL
        //   FROM (select *
        //           from (SELECT D.BARCODE_NO,
        //                        L.BARCODE_PRV_NO,
        //                        B.ITEM_ADJ_DET_ID_FOR_OUT OUTT,
        //                        L.PIC_DATE_TIME,
        //                        L.LZ_MANIFEST_DET_ID,
        //                        B.ITEM_ID,
        //                        B.LZ_MANIFEST_ID,
        //                        (SELECT SD.EBAY_PRICE
        //                           FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD
        //                          WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID
        //                            AND MM.ITEM_ID = SD.ITEM_ID
        //                            AND MM.CONDITION_ID = SD.DEFAULT_COND
        //                            AND MM.BARCODE_NO = B.BARCODE_NO) TOTAL_VAL,
        //                        (SELECT SD.EBAY_PRICE
        //                           FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD
        //                          WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID
        //                            AND MM.ITEM_ID = SD.ITEM_ID
        //                            AND MM.CONDITION_ID = SD.DEFAULT_COND
        //                            AND MM.BARCODE_NO = B.BARCODE_NO
        //                            AND B.EBAY_ITEM_ID IS NOT NULL) LIST_VAL,
        //                        (SELECT SD.EBAY_PRICE
        //                           FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD
        //                          WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID
        //                            AND MM.ITEM_ID = SD.ITEM_ID
        //                            AND MM.CONDITION_ID = SD.DEFAULT_COND
        //                            AND MM.BARCODE_NO = B.BARCODE_NO
        //                            AND mm.EBAY_ITEM_ID IS NULL
        //                            AND MM.ITEM_ADJ_DET_ID_FOR_OUT IS NULL) NOT_LIST_VAL,

//                            decode(B.ORDER_ID,
        //                               null,
        //                               (SELECT SD.EBAY_PRICE
        //                                  FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD
        //                                 WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID
        //                                   AND MM.ITEM_ID = SD.ITEM_ID
        //                                   AND MM.CONDITION_ID = SD.DEFAULT_COND
        //                                   AND MM.BARCODE_NO = B.BARCODE_NO
        //                                   AND B.EBAY_ITEM_ID IS NOT NULL),
        //                               det.sale_price) new_lis,
        //                        det.sale_price SOLD_VALUE,
        //                        B.EBAY_ITEM_ID,
        //                        B.ORDER_ID SALE_RECORD_NO
        //                         FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS L, LZ_BARCODE_MT B, LZ_ITEM_SEED S,lz_salesload_det det/*,
        //                        LJ_ORDER_PACKING_MT    OM*/ WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND B.ITEM_ID = S.ITEM_ID(+) /*and b.order_id = om.order_id(+)*/
        //                    and b.order_id = det.order_id(+) AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND B.CONDITION_ID = S.DEFAULT_COND(+) ";

//         if (!empty($getSeach)) {
        //             if (count($str) > 1) {
        //                 $i = 1;
        //                 foreach ($str as $key) {
        //                     if ($i === 1) {
        //                         $sqlTwo .= " and (UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
        //                     } else {
        //                         $sqlTwo .= " AND UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
        //                     }
        //                     $i++;
        //                 }
        //             } else {
        //                 $sqlTwo .= " and (UPPER(S.ITEM_TITLE) LIKE '%$getSeach%' ";
        //             }

//             $sqlTwo .= " OR UPPER(B.BARCODE_NO) LIKE '%$getSeach%'
        //         OR UPPER(det.order_id) LIKE '%$getSeach%'
        //         OR UPPER(det.buyer_fullname) LIKE '%$getSeach%'
        //         OR UPPER(det.extendedorderid) LIKE '%$getSeach%'
        //            OR UPPER(B.EBAY_ITEM_ID) LIKE '%$getSeach%' )";
        //             //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        //         }

//         if (!empty($merLotName)) {
        //             $sqlTwo .= " and M.lot_id = $merLotName ";
        //         }

//         if (!empty($priceOne) && !empty($priceTwo)) {

//             $sqlTwo .= " AND det.SALE_PRICE BETWEEN  $priceOne AND $priceTwo ";

//         }

//         $sqlTwo .= " AND M.MERCHANT_ID =$merchId";

//         if (!empty($recntSold)) {
        //             $sqlTwo .= "  AND b.order_id is not null
        //            order by det.sale_date desc)
        //            where rownum <=$recntSold )";
        //         } else {
        //             $sqlTwo .= " ))";
        //         }

//         $lot_detail_totals = $this->db->query($sqlTwo)->result_array();
        //         $pos_sum = "SELECT
        //             nvl(sum(max(nvl(dm.qty,0))),0) qty,
        //              sum(max(nvl(dm.price, 0)) +
        //                  (max(nvl(dm.price, 0)) / 100) * max(nvl(dm.sales_tax_perc, 0)) -
        //                  max(nvl(dm.disc_amt, 0))) net_amount
        //               from lz_pos_mt pm, lz_pos_det dm
        //              where pm.lz_pos_mt_id = dm.lz_pos_mt_id
        //                and dm.barcode_id in
        //                    (select d.barcode_no
        //                       from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
        //                      where m.mt_id = d.mt_id
        //                       and m.merchant_id = '$merchId'";

//         if (!empty($merLotName)) {
        //             $pos_sum .= " and M.lot_id = $merLotName ";
        //         }
        //         $pos_sum .= " )";

//         if (!empty($getSeach)) {
        //             if (count($str) > 1) {
        //                 $i = 1;
        //                 foreach ($str as $key) {
        //                     if ($i === 1) {
        //                         $pos_sum .= " and (UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
        //                     } else {
        //                         $pos_sum .= " AND UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
        //                     }
        //                     $i++;
        //                 }
        //             } else {
        //                 $pos_sum .= " and (UPPER(dm.ITEM_DESC) LIKE '%$getSeach%' ";
        //             }

//             $pos_sum .= " OR UPPER(dm.ITEM_DESC) LIKE '%$getSeach%'
        //         OR UPPER(dm.BARCODE_ID) LIKE '%$getSeach%'
        //         OR UPPER(pm.BUYER_NAME) LIKE '%$getSeach%' )";
        //             //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        //         }

//         if (!empty($priceOne) && !empty($priceTwo)) {

//             $pos_sum .= " AND dm.PRICE BETWEEN  $priceOne AND $priceTwo ";

//         }

//         $pos_sum .= " AND pm.deleted_by IS NULL
        //                AND pm.return_by IS NULL
        //              group by dm.barcode_id
        //              order by dm.barcode_id asc";

//         $pos_sum = $this->db->query($pos_sum)->result_array();

//         $return_sum = "SELECT
        //          nvl(sum(nvl(sd.return_qty, 0)),0) ret_qty,
        //        nvl(sum(NVL(sd.SALE_PRICE * sd.return_qty, 0)),0)  return_value

//           FROM LZ_SALESLOAD_DET SD,

//                LJ_ORDER_PACKING_DT DT,
        //                (SELECT MM.EBAY_ITEM_ID,
        //                        max(dd.merchant_id)merchant_id,
        //                        MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
        //                        MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
        //                        max(mm.item_id) item_id
        //                   FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
        //                  WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
        //                    AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
        //                  GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
        //          WHERE sd.order_id in (select d.order_id
        //                                  from ebay_list_mt       e,
        //                                       lj_merhcant_acc_dt a,
        //                                       lz_salesload_det   d
        //                                 where a.acct_id = e.lz_seller_acct_id
        //                                   and d.item_id = e.ebay_item_id
        //                                   and a.merchant_id = '$merchId'
        //                                 group by d.order_id)
        //            /*and sd.order_id = m.order_id(+)*/
        //            --and sd.orderstatus = 'Completed'
        //            and sd.return_id is not null
        //            and sd.order_id = dt.order_id(+)
        //            AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)  ";

//         if (!empty($getSeach)) {
        //             if (count($str) > 1) {
        //                 $i = 1;
        //                 foreach ($str as $key) {
        //                     if ($i === 1) {
        //                         $return_sum .= " and (UPPER(sd.item_title) LIKE '%$key%' ";
        //                     } else {
        //                         $return_sum .= " AND UPPER(sd.item_title) LIKE '%$key%' ";
        //                     }
        //                     $i++;
        //                 }
        //             } else {
        //                 $return_sum .= " and (UPPER(sd.item_title) LIKE '%$getSeach%' ";
        //             }

//             $return_sum .= "OR UPPER(sd.buyer_fullname) LIKE '%$getSeach%'
        //            OR UPPER(sd.order_id) LIKE '%$getSeach%'
        //            OR UPPER(sd.extendedorderid) LIKE '%$getSeach%'
        //            OR UPPER(sd.item_id) LIKE '%$getSeach%' )";

//         }

//         if (!empty($priceOne) && !empty($priceTwo)) {

//             $return_sum .= " AND sd.SALE_PRICE BETWEEN  $priceOne AND $priceTwo";
        //         }
        //$return_sum .= "   GROUP BY sd.ORDER_ID";
        // $return_awaiting = "SELECT count(1) QTY, SUM(SD.SALE_PRICE) SALE_PRICE
        // FROM LZ_SALESLOAD_DET SD,
        // LZ_BARCODE_MT B,
        // EBAY_LIST_MT ELM,
        // LZ_AWAITING_SHIPMENT AW,
        // BIN_MT BI,
        // LZ_RACK_ROWS RO,
        // RACK_MT RAC,
        // LZ_ITEM_COND_MT C,
        // WAREHOUSE_MT WA,
        // LZ_SALESLOAD_MT SM,
        // LZ_SELLER_ACCTS ACC,
        // lz_merchant_mt mr,
        // LJ_MERHCANT_ACC_DT AD,
        // (select p.img_key, p.ebay_id, p.pic_url
        // from LZ_LISTED_ITEM_PIC P
        // where p.pic_id in (SELECT MIN(PIC_id) PIC_id
        // FROM LZ_LISTED_ITEM_PIC P
        // where p.ebay_id is not null
        // and p.img_key is not null
        // GROUP BY EBAY_ID)) pp
        // WHERE B.LIST_ID = ELM.LIST_ID
        // AND SD.SALES_RECORD_NUMBER = B.SALE_RECORD_NO(+)
        // AND SD.ITEM_ID = B.EBAY_ITEM_ID
        // AND SM.LZ_SELLER_ACCT_ID = ACC.LZ_SELLER_ACCT_ID
        // and elm.lz_seller_acct_id = ad.acct_id
        // and aw.orderlineitemid = sd.order_id
        // AND SD.LZ_SALELOAD_ID = SM.LZ_SALELOAD_ID
        // AND B.BIN_ID = BI.BIN_ID(+)
        // AND BI.CURRENT_RACK_ROW_ID = RO.RACK_ROW_ID(+)
        // AND RO.RACK_ID = RAC.RACK_ID(+)
        // AND RAC.WAREHOUSE_ID = WA.WAREHOUSE_ID(+)
        // AND B.CONDITION_ID = C.ID(+)
        // AND SD.ITEM_ID = B.EBAY_ITEM_ID
        // AND SD.ITEM_ID = pp.EBAY_ID(+)
        // AND MR.MERCHANT_ID = AD.MERCHANT_ID
        // AND B.PULLING_ID IS NULL
        // AND SD.UK_FIREBASE_WAITING IS NULL
        // AND B.ITEM_ADJ_DET_ID_FOR_OUT IS NULL
        // AND B.LZ_PART_ISSUE_MT_ID IS NULL
        // AND B.LZ_POS_MT_ID IS NULL
        // AND SD.GNRTD_DC_ID IS NULL
        // AND SD.TRACKING_NUMBER IS NULL
        // AND SD.SHIPPED_ON_DATE IS NULL
        // and sd.orderstatus = 'Completed'
        // and sd.cancel_id is null
        // AND SD.RETURN_ID IS NULL
        // AND mr.MERCHANT_ID =  '$merchId'";
        // $return_sum = $this->db->query($return_sum)->result_array();
        // $return_awaiting = $this->db->query($return_awaiting)->result_array();
        $return_awaiting = [];
        $return_sum = [];
        $lot_detail_totals = [];
        $pos_sum = [];
        if (count($lot_detail_query) >= 1) {
            return array('lot_detail_query' => $lot_detail_query, 'lot_detail_totals' => $lot_detail_totals, 'lot_name' => $lot_name, 'sum_pos' => $pos_sum, 'return_sum' => $return_sum, 'return_awaiting' => $return_awaiting, 'exist' => true);
        } else {
            return array('lot_detail_query' => $lot_detail_query, 'lot_detail_totals' => $lot_detail_totals, 'lot_name' => $lot_name, 'sum_pos' => $pos_sum, 'return_sum' => $return_sum, 'return_awaiting' => $return_awaiting, 'exist' => false);
        }

    }

    public function Get_Lot_All_Record()
    {
        $merchId = $this->input->post('merchId');
        // $merchId = '9';

        $qry = "SELECT sed.item_title,
        cm.cond_name,
        get_pictures.full_img_url,
REPLACE(get_pictures.full_img_url,'D:/wamp/www/','') img_url,
REPLACE(get_pictures.thumb_img_url,'D:/wamp/www/','') thumb_url,
        lo.lot_id,
        lo.ref_no                 ref_no,
        lo.lot_desc,
        l.barcode_prv_no          barcode_no,
        lo.cost                   lot_cost,
        b.ebay_item_id            EBAY_ITEM_ID,
        d.barcode_no              generated_bar,
        l.barcode_prv_no          picture_barc,
        sed.F_UPC                 UPC,
        sed.F_MPN                 MPN,
        sed.category_name       CATEGORY_NAME,
        det.sale_price,
        det.shippinglabelrate ship_price,
        round(det.ebay_fee_perc,2)ebay_fee,
        det.extendedorderid order_id,
        det.buyer_fullname,
        round(det.sale_price * 0.07,2) marketplace_fee,
        det.sale_date,
        '1'  QTY,
                l.pic_date_time,
        b.barcode_no              posted_barc,
        b.ebay_item_id            listed_barc,
        list_price.list_pric,
        cancel_item.barcode_no cancel_Items,
       cancel_item.NO_OF_TIM_CACEL,
       cancel_item.CANCEL_AMOUNT,
        awt_ship.orderlineitemid awt_orders,
               awt_ship.awt_ord_price,
        b.order_id                ebay_sold,
        det.sale_price,
        ps.barcode_id             pos_sold,
        ps.pos_sale_price,
        b.returned_id return_barco,
               decode(b.returned_id ,null,'',det.sale_price) return_amount,
               /*retur_items.barcode_no return_barco,
               retur_items.return_amount,*/
        end_item.no_of_time_ended,
        end_item.end_barcode,
        get_pic_charge.pic_charge
   from lz_merchant_barcode_mt m,
        lz_merchant_barcode_dt d,
        lot_defination_mt lo,
        lz_special_lots l,
        lz_barcode_mt b,
        lz_salesload_det det,
        lz_item_seed sed,
        lz_item_cond_mt cm,
        (SELECT lot_id,
                NVL(UNIQU_QTY_CHARG + REMAIN_QTY_CHARG, 0) pic_charge
           FROM (SELECT m.lot_id,
                        COUNT(L.FOLDER_NAME) PICTURES_DONE,
                        round(max(get_rates_pict.rate), 2) rate,
                        round(max(get_rates_pict.excess_qty_rate), 2) excess_qty_rate,
                        COUNT(DISTINCT L.FOLDER_NAME) UNIQUE_PICS,
                        COUNT(L.FOLDER_NAME) -
                        COUNT(DISTINCT L.FOLDER_NAME) REMAINING_PICS,
                        COUNT(DISTINCT L.FOLDER_NAME) *
                        round(max(get_rates_pict.rate), 2) UNIQU_QTY_CHARG,
                        (COUNT(L.FOLDER_NAME) -
                        COUNT(DISTINCT L.FOLDER_NAME)) *
                        round(max(get_rates_pict.excess_qty_rate), 2) REMAIN_QTY_CHARG
                   FROM LZ_MERCHANT_BARCODE_MT M,
                        LZ_MERCHANT_BARCODE_DT D,
                        LZ_SPECIAL_LOTS L,
                        (select ms.merchant_id,
                                ms.ser_rate_id,
                                ms.rate,
                                ms.excess_qty_rate
                           from lj_merchant_service ms
                          where ms.merchant_id = '$merchId'
                            and ms.ser_rate_id = 1
                            and rownum <= 1) get_rates_pict
                  WHERE M.MT_ID = D.MT_ID
                    and l.INVOICE_ID is null
                    AND D.BARCODE_NO = L.BARCODE_PRV_NO
                    and m.merchant_id = get_rates_pict.merchant_id(+)
                    AND M.MERCHANT_ID = '$merchId'
                  group by m.lot_id)) get_pic_charge,
        (select po.barcode_id,
                round(nvl(po.price, 0) + (nvl(po.price, 0) / 100) *
                      nvl(po.sales_tax_perc, 0) - nvl(po.disc_amt, 0),
                      2) pos_sale_price
           from lz_pos_det po
          where po.barcode_id is not null
          AND po.deleted_by IS NULL
           AND po.return_by IS NULL) ps,
        (select li.ebay_item_id,
                li.ebay_item_desc title,
                li.list_price     list_pric /*, list_qty_charge.TOTAL listing_charge*/
           from ebay_list_mt li
          where li.list_id in (select max(e.list_id) list_id
                                 from ebay_list_mt e
                                group by e.ebay_item_id)) list_price,
        /*(select k.barcode_no,
                max(sk.sale_price) / max(sk.quantity) return_amount
           from lj_returned_barcode_mt k, lz_salesload_det sk
          where k.order_id = sk.order_id
          group by k.barcode_no) retur_items,*/
        (select count(ll.barcode_no) no_of_time_ended,
                ll.barcode_no end_barcode
           from LJ_AGINIG_ITEM_LOG ll
          group by ll.barcode_no) end_item,
          (select aw.orderlineitemid,ap.sale_price/ap.quantity awt_ord_price from LZ_AWAITING_SHIPMENT aw,lz_salesload_det ap
                 where aw.orderlineitemid = ap.order_id) awt_ship,
    (select BARCODE_NO,
               count(BARCODE_NO) no_of_tim_cacel,
               sum(canccel_amount) cancel_amount
          from (select i.barcode_no,
                       u.quantity,
                       u.sale_price,
                       round(u.sale_price / u.quantity, 2) canccel_amount,
                       u.orderstatus,
                       u.order_id,
                       u.cancel_id
                  from lj_cancellation_dt i, lz_salesload_det u
                 where i.order_id = u.order_id(+)
                 order by i.barcode_no asc)
         group by BARCODE_NO) cancel_item,
         (select mt.folder_name,
       mt.base_url || mt.folder_name || '/' || dt.image_name full_img_url,
       mt.base_url || mt.folder_name || '/thumb/' || dt.image_name thumb_img_url
  from lj_barcode_pic_mt mt, lj_barcode_pic_dt dt
 where mt.img_mt_id = dt.img_mt_id
   and dt.img_dt_id in (select min(dt.img_dt_id)
                          from lj_barcode_pic_dt dt
                         group by dt.img_mt_id)
) get_pictures
  where m.mt_id = d.mt_id
    and m.lot_id = lo.lot_id
    and m.lot_id = get_pic_charge.lot_id(+)
    and d.barcode_no = l.barcode_prv_no
    and l.barcode_prv_no = b.barcode_no(+)
    and b.order_id = det.order_id(+)
    and b.barcode_no = ps.barcode_id(+)
    and b.order_id = awt_ship.orderlineitemid(+)
    and b.barcode_no = cancel_item.barcode_no(+)
       and to_char(l.folder_name) = get_pictures.folder_name(+)
    /*and b.barcode_no = retur_items.barcode_no(+)*/
    and b.ebay_item_id = list_price.ebay_item_id(+)
    and b.barcode_no = end_item.end_barcode(+)
    and b.item_id = sed.item_id(+)
    and b.lz_manifest_id = sed.lz_manifest_id(+)
    and b.condition_id = sed.default_cond(+)
    and sed.default_cond = cm.id(+)
              and m.merchant_id = '$merchId'
            --  and lo.lot_id IN (82)
  order by l.pic_date_time asc";
        $qry = $this->db->query($qry);
        if ($qry->num_rows() > 0) {

            return array('status' => true, 'data' => $qry->result_array());
        } else {
            return array('status' => false, 'data' => $qry->array());
        }
    }

    public function get_barcodes_pictures_without_host($barcodes, $conditions)
    {

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();

        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        $dir = "";
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        foreach ($barcodes as $barcode) {

            $bar = $barcode['EBAY_ITEM_ID'];
            $barco = isset($barcode['BARCODE_NO']);

            if (!empty($bar) && empty($barco)) {
                $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE lot.barcode_prv_no in (select mm.barcode_no from lz_barcode_mt mm where mm.ebay_item_id = '$bar') and lot.folder_name is not null and rownum <= 1  ")->result_array();
                $folderName = "";
                if ($getFolder) {
                    $folderName = $getFolder[0]['FOLDER_NAME'];
                } else {
                    $folderName = $bar;
                }

                // $dir = "";
                $barcodePictures = $master_path . $folderName . "/";
                $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

                if (is_dir($barcodePictures)) {
                    $dir = $barcodePictures;
                } else if (is_dir($barcodePicturesThumb)) {
                    $dir = $barcodePicturesThumb;
                }

            } else {

                $bar = $barcode['BARCODE_NO'];
                $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE lot.barcode_prv_no = '$bar' and rownum <= 1  ")->result_array();
                if (count($getFolder) > 0) {
                    $getFolder = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar' ")->result_array();

                    if (count($getFolder) >= 1) {

                        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                        $path = $path->result_array();
                        $master_path = $path[0]["MASTER_PATH"];

                        // $barcode = $qry[0]["FOLDER_NAME"];
                        // $dir = $master_path . $barcode . "/";
                        $folderName = "";
                        if ($getFolder) {
                            $folderName = $getFolder[0]['FOLDER_NAME'];
                        } else {
                            $folderName = $bar;
                        }

                        $barcodePictures = $master_path . $folderName . "/";
                        $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

                        if (is_dir($barcodePictures)) {
                            $dir = $barcodePictures;
                        } else if (is_dir($barcodePicturesThumb)) {
                            $dir = $barcodePicturesThumb;
                        }
                        // var_dump($dir);
                        // // exit;

                    } else {

                        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                        $path = $path->result_array();
                        $master_path = $path[0]["MASTER_PATH"];

                        $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();
                        if (count($get_params) === 0) {
                            $get_params = $this->db->query("SELECT MT.UPC F_UPC, MT.MPN F_MPN, C.COND_NAME, DT.BARCODE_NO
                                              FROM LZ_PURCH_ITEM_MT MT, LZ_PURCH_ITEM_DT DT, LZ_ITEM_COND_MT C
                                             WHERE MT.PURCH_MT_ID = DT.PURCH_MT_ID
                                               AND C.ID = MT.CONDITION
                                               AND DT.BARCODE_NO = '$bar'")->result_array();
                        }
                        //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                        $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                        $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                        $cond = @$get_params[0]['COND_NAME'];

                        $upcPicture = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
                        $upcThumbPicture = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/thumb" . "/";
                        if (is_dir($upcPicture)) {
                            $dir = $upcPicture;
                        } else if (is_dir($upcThumbPicture)) {
                            $dir = $upcThumbPicture;
                        }
                    }

                }
            }
            // var_dump($dir);

            $dir = preg_replace("/[\r\n]*/", "", $dir);

            if (is_dir($dir)) {
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

                if ($images) {
                    $j = 0;
                    foreach ($images as $image) {

                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                        // $uri[$bar][$j] = $base_url . $withoutMasterPartUri;
                        $uri[$bar][$j] = "/" . $withoutMasterPartUri;
                        // if($uri[$barcode['LZ_barcode_ID']]){
                        //     break;
                        // }

                        $j++;
                    }
                } else {
                    $uri[$bar][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar][1] = false;
                }
            } else {
                $uri[$bar][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar][1] = false;
            }
        }
        return array('uri' => $uri);

    }

    public function get_barcodes_pictures($barcodes, $conditions)
    {

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();

        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        $dir = "";
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        foreach ($barcodes as $barcode) {

            $bar = $barcode['EBAY_ITEM_ID'];
            $barco = isset($barcode['BARCODE_NO']);

            if (!empty($bar) && empty($barco)) {
                $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE lot.barcode_prv_no in (select mm.barcode_no from lz_barcode_mt mm where mm.ebay_item_id = '$bar') and lot.folder_name is not null and rownum <= 1  ")->result_array();
                $folderName = "";
                if ($getFolder) {
                    $folderName = $getFolder[0]['FOLDER_NAME'];
                } else {
                    $folderName = $bar;
                }

                // $dir = "";
                $barcodePictures = $master_path . $folderName . "/";
                $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

                if (is_dir($barcodePictures)) {
                    $dir = $barcodePictures;
                } else if (is_dir($barcodePicturesThumb)) {
                    $dir = $barcodePicturesThumb;
                }

            } else {

                $bar = $barcode['BARCODE_NO'];
                $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE lot.barcode_prv_no = '$bar' and rownum <= 1  ")->result_array();
                if (count($getFolder) > 0) {
                    $getFolder = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar' ")->result_array();

                    if (count($getFolder) >= 1) {

                        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                        $path = $path->result_array();
                        $master_path = $path[0]["MASTER_PATH"];

                        // $barcode = $qry[0]["FOLDER_NAME"];
                        // $dir = $master_path . $barcode . "/";
                        $folderName = "";
                        if ($getFolder) {
                            $folderName = $getFolder[0]['FOLDER_NAME'];
                        } else {
                            $folderName = $bar;
                        }

                        $barcodePictures = $master_path . $folderName . "/";
                        $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

                        if (is_dir($barcodePictures)) {
                            $dir = $barcodePictures;
                        } else if (is_dir($barcodePicturesThumb)) {
                            $dir = $barcodePicturesThumb;
                        }
                        // // var_dump($dir);
                        // // exit;

                    } else {

                        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                        $path = $path->result_array();
                        $master_path = $path[0]["MASTER_PATH"];

                        $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();
                        if (count($get_params) === 0) {
                            $get_params = $this->db->query("SELECT MT.UPC F_UPC, MT.MPN F_MPN, C.COND_NAME, DT.BARCODE_NO
                                              FROM LZ_PURCH_ITEM_MT MT, LZ_PURCH_ITEM_DT DT, LZ_ITEM_COND_MT C
                                             WHERE MT.PURCH_MT_ID = DT.PURCH_MT_ID
                                               AND C.ID = MT.CONDITION
                                               AND DT.BARCODE_NO = '$bar'")->result_array();
                        }
                        //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                        $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                        $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                        $cond = @$get_params[0]['COND_NAME'];

                        $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
                    }

                }
            }

            //   if(!is_dir($dir)){
            //     $getBarcodeMt = $this->db->query("SELECT IT.ITEM_MT_UPC UPC, IT.ITEM_MT_MFG_PART_NO MPN FROM LZ_BARCODE_MT MT, ITEMS_MT IT WHERE MT.ITEM_ID = IT.ITEM_ID AND MT.BARCODE_NO = '$bar' ")->result_array();
            //     $upc = "";
            //     $mpn = "";
            //     if($getBarcodeMt){
            //       $upc = $getBarcodeMt[0]['UPC'];
            //       $mpn = $getBarcodeMt[0]['MPN'];
            //     }

            //     $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
            //     $path = $path->result_array();

            //     $upc_mpn_master_path = $path[0]["MASTER_PATH"];

            //     $mpn = str_replace("/","_",$mpn);
            // foreach($conditions as $cond){
            //     $dir = $upc_mpn_master_path.$upc."~".$mpn."/".$cond['COND_NAME']."/";
            //     $dirWithMpn = $upc_mpn_master_path."~".$mpn."/".$cond['COND_NAME']."/";
            //     $dirWithUpc = $upc_mpn_master_path.$upc."~/".$cond['COND_NAME']."/";
            //     if (is_dir($dir)){
            //         $dir = $dir;
            //         break;
            //     }else if(is_dir($dirWithMpn)){
            //         $dir = $dirWithMpn;
            //         break;
            //     }else if(is_dir($dirWithUpc)){
            //         $dir = $dirWithUpc;
            //         break;
            //     }else {
            //         $dir = $upc_mpn_master_path."/".$cond['COND_NAME']."/";
            //     }

            // }
            // if($dir == $upc_mpn_master_path."~/".$cond['COND_NAME']."/"){
            //         $dir = $upc_mpn_master_path."/".$cond['COND_NAME']."/";
            //     }

            //   }
            $dir = preg_replace("/[\r\n]*/", "", $dir);

            if (is_dir($dir)) {
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

                if ($images) {
                    $j = 0;
                    foreach ($images as $image) {

                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                        $uri[$bar][$j] = $base_url . $withoutMasterPartUri;
                        // if($uri[$barcode['LZ_barcode_ID']]){
                        //     break;
                        // }

                        $j++;
                    }
                } else {
                    $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar][1] = false;
                }
            } else {
                $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar][1] = false;
            }
        }
        return array('uri' => $uri);

    }

    public function get_pictures_Dash($barcodes)
    {
        $uri = [];
        $base64 = [];

        // $barocde_no = $this->input->post('barocde_no');
        foreach ($barcodes as $barco) {
            $bar = explode(',', $barco['BARCODE_NO']);
            $bar_val = $bar[0];
            $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

            if (count($qry) >= 1) {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $barcode = $qry[0]["FOLDER_NAME"];
                $dir = $master_path . $barcode . "/";
                // var_dump($dir);
                // exit;

            } else {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar_val' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();
                if (count($get_params) === 0) {
                    $get_params = $this->db->query("SELECT MT.UPC F_UPC, MT.MPN F_MPN, C.COND_NAME, DT.BARCODE_NO
                                      FROM LZ_PURCH_ITEM_MT MT, LZ_PURCH_ITEM_DT DT, LZ_ITEM_COND_MT C
                                     WHERE MT.PURCH_MT_ID = DT.PURCH_MT_ID
                                       AND C.ID = MT.CONDITION
                                       AND DT.BARCODE_NO = '$bar_val'")->result_array();
                }
                //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                $cond = @$get_params[0]['COND_NAME'];

                $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);
            // var_dump($dir);
            // exit;

            //var_dump(is_dir($dir));exit;
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            if (is_dir($dir)) {
                // var_dump($dir);exit;
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
                if (!empty($images)) {
                    $i = 0;
                    $count = 0;
                    foreach ($images as $image) {

                        // Get Pic Date of Folder
                        $now = date("Y-m-d"); // or your date as well
                        $fileDate = date("Y-m-d", filemtime($image));
                        $date1 = date_create($now);
                        $date2 = date_create($fileDate);
                        $diff = date_diff($date1, $date2);
                        $diff = $diff->format("%a");
                        // echo $diff;

                        $pathinfo = pathinfo($image);
                        $imagePath = explode('/', $image, 4);
                        // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $image_path = $this->convert_text(@$imagePath[3]);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $image_path);
                        // $uri[$bar_val][$i] = $base_url . $withoutMasterPartUri;
                        $uri[$bar_val]['image'][$i] = "/" . $withoutMasterPartUri;
                        $count = $count + 1;
                        // $uri[$bar_val][$i + 1] = $bar_val;
                        // $uri[$bar_val][$i + 2] = $i + 1;
                        $i++;
                    }
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = $count;
                    $uri[$bar_val][3] = $diff;
                } else {
                    // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                    // $image = $this->convert_text(@$imagePath);
                    $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = 0;
                    $uri[$bar_val][3] = 0;
                }

            } else {
                // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                // $image = $this->convert_text(@$imagePath);
                $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar_val][1] = $bar_val;
                $uri[$bar_val][2] = 0;
                $uri[$bar_val][3] = 0;
            }

            //var_dump($dekitted_pics);exit;

        }
        // var_dump($uri);
        // exit;
        return array('uri' => $uri);
    }
    public function merch_lot_load_detail()
    {
        $get_param = strtoupper($this->input->post('param'));
        $recntSold = strtoupper($this->input->post('recntSold'));

        $getSeach = strtoupper($this->input->post('getSeach'));
        $getSeach = trim(str_replace("  ", ' ', $getSeach));
        $getSeach = str_replace(array("`,'"), "", $getSeach);
        $getSeach = str_replace(array("'"), "''", $getSeach);

        $priceOne = strtoupper($this->input->post('priceOne'));
        $priceOne = trim(str_replace("  ", ' ', $priceOne));
        $priceOne = str_replace(array("`,'"), "", $priceOne);
        $priceOne = str_replace(array("'"), "''", $priceOne);

        $priceTwo = strtoupper($this->input->post('priceTwo'));
        $priceTwo = trim(str_replace("  ", ' ', $priceTwo));
        $priceTwo = str_replace(array("`,'"), "", $priceTwo);
        $priceTwo = str_replace(array("'"), "''", $priceTwo);

        $merLotName = strtoupper($this->input->post('merLotName'));
        $merchId = strtoupper($this->input->post('merchId'));
        $str = explode(' ', $getSeach);

        $sql = "SELECT EBAY_ITEM_ID,
       MAX(ITEM_TITLE) ITM_TITLE,
       MAX(QTY) SOLD_QTY,
       COUNT(EBAY_ITEM_ID) QTY,
       ROUND(NVL(MAX(SALE_PRICE), 0), 2) SALE_PRICE,
       ROUND(NVL(MAX(QTY) * MAX(SALE_PRICE), 0), 2) TOTAL,
       ROUND(NVL(MAX(SHIPING_LABEL_RATE), 0), 2) SHIPING_PRICE,
       ROUND(NVL(MAX(EBAY_FEE), 0), 2) EBAY_FEE,
       ROUND(NVL(MAX(PACKING_COST), 0), 2) PACKING_COST,
       ROUND(NVL(NVL((7 / 100) * NVL(MAX(SALE_PRICE * QTY), 0), 0), 0), 2) MARKEPLACE_FEE,
       round(0.00, 2) LJ_CHARGES,
       round(ROUND(MAX(EBAY_PRICE), 2) * COUNT(EBAY_ITEM_ID),2)  total_List_,
       MAX(BUYER_NAME) BUY_NAME,
       MAX(SALE_RECRD_NUM) SALE_REC,
       MAX(CHECKOUT_DATE) CHECKOUT_DATE,
       MAX(SALE_DATE) SALE_DATE,
       ROUND(MAX(EBAY_PRICE), 2) EBAY_PRICE,
       MAX(DEFAULT_COND) DEFAULT_COND,
       MAX(F_UPC) F_UPC,
       MAX(F_MPN) F_MPN,
       MAX(F_MANUFACTURE) F_MANUFACTURE,
       MAX(CATEGORY_NAME) CATEGORY_NAME,
       MAX(COND_NAME) COND_NAME
        FROM (SELECT *
          FROM (SELECT M.LOT_ID,
                       S.SEED_ID,
                       B.EBAY_ITEM_ID,
                       det.sale_date ,
                       det.checkout_date,
                       M.NO_OF_BARCODE,
                       B.ORDER_ID,
                       B.SALE_RECORD_NO,
                       det.quantity qty,
                       S.ITEM_TITLE,
                       B.BARCODE_NO,
                       det.SALE_PRICE,
                       det.shipping_charges SHIPING_LABEL_RATE,
                       det.ebay_fee_perc EBAY_FEE,
                       (SELECT NVL(SUM(OD.PACKING_COST), 0)
                          FROM LJ_ORDER_PACKING_MT O, LJ_ORDER_PACKING_DT OD
                         WHERE O.ORDER_PACKING_ID = OD.ORDER_PACKING_ID
                           AND O.ORDER_ID = det.order_id
                         GROUP BY OD.ORDER_PACKING_ID) PACKING_COST,
                       (SELECT NVL(BIL.CHARGES, 0)
                          FROM LJ_BARCODE_BILLING BIL
                         WHERE BIL.BARCODE_NO = L.BARCODE_PRV_NO /*B.BARCODE_NO*/
                           AND BIL.SER_RATE_ID = 1) PIC_CHARGE, /*PIC SERVICE CHARGE*/
                       (SELECT NVL(BIL.CHARGES, 0)
                          FROM LJ_BARCODE_BILLING BIL, LZ_BARCODE_MT BAR
                         WHERE BIL.BARCODE_NO = B.BARCODE_NO
                           AND BIL.BARCODE_NO = BAR.BARCODE_NO
                           AND BIL.SER_RATE_ID = 2
                           AND BAR.EBAY_ITEM_ID IS NOT NULL) LIST_CHARGE,
                       (SELECT NVL(BIL.CHARGES, 0)
                          FROM LJ_BARCODE_BILLING BIL, LZ_BARCODE_MT BAR
                         WHERE BIL.BARCODE_NO = B.BARCODE_NO
                           AND BIL.BARCODE_NO = BAR.BARCODE_NO
                           AND BIL.SER_RATE_ID = 7
                           AND BAR.EBAY_ITEM_ID IS NOT NULL) PACK_PULL_CHARGE,
                       det.buyer_fullname BUYER_NAME,
                       det.extendedorderid/*OM.Order_Id*/ SALE_RECRD_NUM, /*OM.SALE_RECRD_NUM,*/
                       S.EBAY_PRICE,
                       S.DEFAULT_COND,
                       S.F_UPC,
                       S.F_MPN,
                       S.F_MANUFACTURE,
                       S.CATEGORY_NAME,
                       CD.COND_NAME
                  FROM LOT_DEFINATION_MT      MD,
                       LZ_MERCHANT_BARCODE_MT M,
                       LZ_MERCHANT_BARCODE_DT D,
                       LZ_SPECIAL_LOTS        L,
                       LZ_BARCODE_MT          B,
                       LZ_ITEM_SEED           S,
                       /*LJ_ORDER_PACKING_MT    OM,*/
                       lz_salesload_det det,
                       LZ_ITEM_COND_MT        CD
                       WHERE M.MT_ID = D.MT_ID AND MD.LOT_ID = M.LOT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND B.ITEM_ID = S.ITEM_ID(+) AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND B.CONDITION_ID = S.DEFAULT_COND(+)  /*AND B.ORDER_ID = OM.ORDER_ID(+)*/
                   AND B.ORDER_ID = det.ORDER_ID(+) AND S.DEFAULT_COND = CD.ID(+)   and b.ebay_item_id is not null AND M.MERCHANT_ID = $merchId ";
        if (!empty($getSeach)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $sql .= " and (UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    } else {
                        $sql .= " AND UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $sql .= " and (UPPER(S.ITEM_TITLE) LIKE '%$getSeach%' ";
            }

            $sql .= " OR UPPER(B.BARCODE_NO) LIKE '%$getSeach%'
           OR UPPER(det.buyer_fullname) LIKE '%$getSeach%'
           OR UPPER(det.order_id) LIKE '%$getSeach%'
           OR UPPER(det.extendedorderid) LIKE '%$getSeach%'
           OR UPPER(B.EBAY_ITEM_ID) LIKE '%$getSeach%' )";

        }

        if ($get_param == 'ACTIVE') {

            $sql .= " AND B.ORDER_ID IS NULL ";

        } else if ($get_param == 'SOLD') {

            $sql .= " AND B.ORDER_ID IS NOT NULL ";
        }

// if (!empty($recntSold)){
        //     $sqlTwo .= "  AND b.order_id is not null
        //            order by b.order_id  desc)
        //            where rownum <=$recntSold )";
        //     }else{
        //            $sqlTwo .= " ))" ;
        //     }

        //
        if (!empty($merLotName)) {
            $sql .= " and M.lot_id = $merLotName ";
        }

        if (!empty($priceOne) && !empty($priceTwo)) {

            $sql .= " AND det.SALE_PRICE BETWEEN  $priceOne AND $priceTwo";
        }

        if (!empty($recntSold)) {
            $sql .= "  order by det.SALE_DATE desc";
        }

        $sql .= " ) ";

        if (!empty($recntSold)) {
            $sql .= " where rownum<= $recntSold";
        }

        $sql .= " ) GROUP BY EBAY_ITEM_ID, ORDER_ID  ORDER BY  QTY DESC ";

        // $lot_detail_data = $this->db->query($sql)->result_array();

        $pos_query_Detail = "SELECT DECODE(pm.pay_mode, 'R', 'CARD', 'C', 'CASH') PAY_MODE,
               pm.paid_status,
               TO_CHAR(pm.entered_date_time, 'dd/mm/YY HH24:MI:SS' ) CREATED_DATE,
               dm.BARCODE_ID BARCODE_NO,
       dm.qty,
       dm.item_desc,
       DECODE(pm.pay_mode, 'R', 'CARD', 'C', 'CASH') PAY_MODE,
       dm.price,
       dm.sales_tax_perc,
       dm.disc_perc,
       dm.disc_amt,
       dm.repaire_id,
       null EBAY_ITEM_ID,
       nvl(dm.price, 0) +   (nvl(dm.price, 0) / 100) * (nvl(dm.sales_tax_perc, 0)) - (nvl(dm.disc_amt, 0))net_amount
          from lz_pos_mt pm, lz_pos_det dm
         where pm.lz_pos_mt_id = dm.lz_pos_mt_id
         and dm.barcode_id in
              (select d.barcode_no
                 from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
                where m.mt_id = d.mt_id
                  and m.merchant_id = '$merchId'";

        if (!empty($merLotName)) {
            $pos_query_Detail .= " and M.lot_id = $merLotName ";
        }

        $pos_query_Detail .= ") AND pm.deleted_by IS NULL
            AND pm.return_by IS NULL";

        if (!empty($getSeach)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $pos_query_Detail .= " and (UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
                    } else {
                        $pos_query_Detail .= " AND UPPER(dm.ITEM_DESC) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $pos_query_Detail .= " and (UPPER(dm.ITEM_DESC) LIKE '%$getSeach%' ";
            }

            $pos_query_Detail .= " OR UPPER(dm.BARCODE_ID) LIKE '%$getSeach%' )";

        }

        if (!empty($priceOne) && !empty($priceTwo)) {

            $pos_query_Detail .= " AND dm.PRICE BETWEEN  $priceOne AND $priceTwo";
        }

        $pack_inv_detail = "SELECT Return_Id,
item_id,
ret_qty,
return_barcodes,
       MERCHANT_ID,
       TRACKING_NUMBER,
       ORDER_ID, /*ORDER_PACKING_ID,*/
       SALES_RECORD_NUMBER recrd_no,
       EBAY_ITEM_ID,
       ITEM_TITLE,
       sale_price,
       QTY,
       ship_cost,
       PACKING_COST,
       SERVICE_COST,
       EBAY_FEE,
       MARKTPLACE,
       NVL(SHIP_COST + PACKING_COST + SERVICE_COST + EBAY_FEE + MARKTPLACE,
           0) TOTAL_CHARGE

  FROM (SELECT max(SD.Return_Id) Return_Id,max(sd.extendedorderid) ORDER_ID,

               (SELECT LISTAGG(d.barcode_no, ', ') WITHIN GROUP(ORDER BY d.barcode_no) barco_no
        FROM lj_returned_barcode_mt d
       where d.order_id= sd.ORDER_ID) return_barcodes,


               max(sd.tracking_number) tracking_number,
               max(nvl(sd.return_qty,0)) ret_qty,
               /*--max(M.ORDER_PACKING_ID) ORDER_PACKING_ID,*/
               max(sd.extendedorderid) SALES_RECORD_NUMBER,
               MAX(sd.item_id) EBAY_ITEM_ID,
               MAX(sd.item_title) ITEM_TITLE,
               NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) SALE_PRICE,
               MAX(sd.quantity) QTY,
               NVL(MAX(sd.shippinglabelrate),0) SHIP_COST,
               SUM(NVL(DT.PACKING_COST, 0)) PACKING_COST,
               MAX(NVL(sd.SERVICE_COST, 0)) SERVICE_COST,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      NVL(MAX(sd.ebay_fee_perc), 0),
                      0) EBAY_FEE,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      (0.07 * (MAX(sd.SALE_PRICE) * MAX(sd.quantity))),
                      0) MARKTPLACE, /* (0.07 * (MAX(M.SALE_PRICE) * MAX(M.QTY))) MARKTPLACE,*/
               MAX(ACOUNT_DET.ACOUNT_NAME) ACC_NAME,
               MAX(ACOUNT_DET.merchant_id) MERCHANT_ID,
               max(ACOUNT_DET.item_id) item_id
          FROM LZ_SALESLOAD_DET SD,
               /*--LJ_ORDER_PACKING_MT M,*/
               LJ_ORDER_PACKING_DT DT,
               (SELECT MM.EBAY_ITEM_ID,
                       max(dd.merchant_id)merchant_id,
                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                       max(mm.item_id) item_id
                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
         WHERE sd.order_id in (select d.order_id
                                 from ebay_list_mt       e,
                                      lj_merhcant_acc_dt a,
                                      lz_salesload_det   d
                                where a.acct_id = e.lz_seller_acct_id
                                  and d.item_id = e.ebay_item_id
                                  and a.merchant_id = '$merchId'
                                group by d.order_id)
           /*and sd.order_id = m.order_id(+)*/
           --and sd.orderstatus = 'Completed'
           and sd.return_id is not null
           and sd.order_id = dt.order_id(+)
           AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
          ";

        if (!empty($getSeach)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $pack_inv_detail .= " and (UPPER(sd.item_title) LIKE '%$key%' ";
                    } else {
                        $pack_inv_detail .= " AND UPPER(sd.item_title) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $pack_inv_detail .= " and (UPPER(sd.item_title) LIKE '%$getSeach%' ";
            }

            $pack_inv_detail .= "OR UPPER(sd.buyer_fullname) LIKE '%$getSeach%'
           OR UPPER(sd.order_id) LIKE '%$getSeach%'
           OR UPPER(sd.extendedorderid) LIKE '%$getSeach%'
           OR UPPER(sd.item_id) LIKE '%$getSeach%' )";

        }

        if (!empty($priceOne) && !empty($priceTwo)) {

            $pack_inv_detail .= " AND sd.SALE_PRICE BETWEEN  $priceOne AND $priceTwo";
        }
        $pack_inv_detail .= "   GROUP BY sd.ORDER_ID) order by ORDER_ID desc ";

        if ($get_param == 'POS') {
            $pos_query_Detail = $this->db->query($pos_query_Detail)->result_array();
            if (count($pos_query_Detail) > 0) {
                $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();

                $images = $this->get_barcodes_pictures($pos_query_Detail, $conditions);
                // $uri = $this->get_pictures_Dash($pos_query_Detail);
                $images = $images['uri'];

                return array('pos_query_Detail' => $pos_query_Detail, "images" => $images, 'exist' => true);
            } else {
                return array('pos_query_Detail' => $pos_query_Detail, 'exist' => false, "images" => array());
            }

        } else if ($get_param == 'RETURN') {

            $pack_inv_detail = $this->db->query($pack_inv_detail)->result_array();
            if (count($pack_inv_detail) > 0) {
                // $images = $this->get_ebay_pictures($pack_inv_detail);
                $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();

                $images = $this->get_barcodes_pictures($pack_inv_detail, $conditions);
                return array('exist' => true, 'data' => $pack_inv_detail, 'images' => $images['uri']);
            } else {
                return array('exist' => false, 'data' => $pack_inv_detail, "images" => array());
            }

        } else {
            $lot_detail_data = $this->db->query($sql)->result_array();
            if (count($lot_detail_data) >= 1) {
                $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();

                $uri = $this->get_barcodes_pictures($lot_detail_data, $conditions);
                $images = $uri['uri'];

                return array('lot_detail_data' => $lot_detail_data, "images" => $images, 'exist' => true);
            } else {
                return array('lot_detail_data' => $lot_detail_data, 'exist' => false, "images" => array());
            }
        }

    }

    public function merch_inproces_items()
    {
        $get_param = strtoupper($this->input->post('param'));
        $getSeach = strtoupper($this->input->post('getSeach'));
        $getSeach = trim(str_replace("  ", ' ', $getSeach));
        $getSeach = str_replace(array("`,'"), "", $getSeach);
        $getSeach = str_replace(array("'"), "''", $getSeach);

        $priceOne = strtoupper($this->input->post('priceOne'));
        $priceOne = trim(str_replace("  ", ' ', $priceOne));
        $priceOne = str_replace(array("`,'"), "", $priceOne);
        $priceOne = str_replace(array("'"), "''", $priceOne);

        $priceTwo = strtoupper($this->input->post('priceTwo'));
        $priceTwo = trim(str_replace("  ", ' ', $priceTwo));
        $priceTwo = str_replace(array("`,'"), "", $priceTwo);
        $priceTwo = str_replace(array("'"), "''", $priceTwo);

        $merLotName = strtoupper($this->input->post('merLotName'));
        $merchId = strtoupper($this->input->post('merchId'));
        $str = explode(' ', $getSeach);

        $sql = "SELECT B.BARCODE_NO ,B.BARCODE_NO EBAY_ITEM_ID, S.ITEM_TITLE , C.COND_NAME, round(nvl(S.EBAY_PRICE,0),2)EBAY_PRICE, S.F_UPC, S.F_MPN, S.F_MANUFACTURE, L.UPDATED_AT, S.CATEGORY_NAME FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LZ_BARCODE_MT          B, LZ_ITEM_SEED           S, LZ_ITEM_COND_MT        C WHERE M.MT_ID = D.MT_ID AND M.MERCHANT_ID = $merchId AND L.BARCODE_PRV_NO = D.BARCODE_NO AND L.BARCODE_PRV_NO = B.BARCODE_NO AND B.ITEM_ID = S.ITEM_ID(+) AND S.DEFAULT_COND = C.ID(+) AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND B.CONDITION_ID = S.DEFAULT_COND(+) AND B.EBAY_ITEM_ID IS NULL AND B.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND M.MERCHANT_ID = $merchId ";

        if (!empty($getSeach)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $sql .= " and (UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    } else {
                        $sql .= " AND UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $sql .= " and (UPPER(S.ITEM_TITLE) LIKE '%$getSeach%' ";
            }

            $sql .= " OR UPPER(B.BARCODE_NO) LIKE '%$getSeach%' )";

        }

        $sql .= " ORDER BY  B.BARCODE_NO DESC ";

        $lot_detail_data = $this->db->query($sql)->result_array();

        if (count($lot_detail_data) >= 1) {

            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();

            $uri = $this->get_barcodes_pictures($lot_detail_data, $conditions);
            $images = $uri['uri'];

            return array('lot_detail_data' => $lot_detail_data, "images" => $images, 'exist' => true);
        } else {
            return array('lot_detail_data' => $lot_detail_data, 'exist' => false);
        }

    }

    public function merchant_sold_items()
    {

        $get_param = 'SOLD'; //strtoupper($this->input->post('param'));
        $getSeach = strtoupper($this->input->post('getSeach'));
        $getSeach = trim(str_replace("  ", ' ', $getSeach));
        $getSeach = str_replace(array("`,'"), "", $getSeach);
        $getSeach = str_replace(array("'"), "''", $getSeach);

        $priceOne = strtoupper($this->input->post('priceOne'));
        $priceOne = trim(str_replace("  ", ' ', $priceOne));
        $priceOne = str_replace(array("`,'"), "", $priceOne);
        $priceOne = str_replace(array("'"), "''", $priceOne);

        $priceTwo = strtoupper($this->input->post('priceTwo'));
        $priceTwo = trim(str_replace("  ", ' ', $priceTwo));
        $priceTwo = str_replace(array("`,'"), "", $priceTwo);
        $priceTwo = str_replace(array("'"), "''", $priceTwo);

        $merLotName = strtoupper($this->input->post('merLotName'));
        $merchId = strtoupper($this->input->post('merchId'));
        $str = explode(' ', $getSeach);

        $lot_name = $this->db->query("SELECT M.LOT_ID, M.LOT_DESC ||'  ('|| M.REF_NO ||')' LOT_DESC FROM LOT_DEFINATION_MT M WHERE M.MERCHANT_ID  =$merchId ")->result_array();

        $sql = "SELECT EBAY_ITEM_ID, MAX(ITEM_TITLE) ITM_TITLE, MAX(QTY) SOLD_QTY, COUNT(EBAY_ITEM_ID) QTY, MAX(SALE_PRICE) SALE_PRICE, MAX(QTY) * MAX(SALE_PRICE) TOTAL, MAX(SHIPING_LABEL_RATE) SHIPING_PRICE, MAX(EBAY_FEE) EBAY_FEE, MAX(PACKING_COST) PACKING_COST, NVL((7 / 100) * NVL(MAX(SALE_PRICE * QTY), 0), 0) MARKEPLACE_FEE, ROUND(NVL(SUM(NVL(PIC_CHARGE, 0)) + SUM(NVL(LIST_CHARGE, 0)) + SUM(NVL(PACK_PULL_CHARGE, 0)), 0), 2) LJ_CHARGES, MAX(BUYER_NAME) BUY_NAME, MAX(SALE_RECRD_NUM) SALE_REC, MAX(CHECKOUT_DATE) CHECKOUT_DATE, MAX(SALE_DATE) SALE_DATE, MAX(EBAY_PRICE) EBAY_PRICE, MAX(DEFAULT_COND) DEFAULT_COND, MAX(F_UPC) F_UPC, MAX(F_MPN)F_MPN , MAX(F_MANUFACTURE)F_MANUFACTURE, MAX(CATEGORY_NAME)CATEGORY_NAME, MAX(COND_NAME) COND_NAME FROM (SELECT M.LOT_ID, S.SEED_ID, B.EBAY_ITEM_ID, OM.SALE_DATE, OM.CHECKOUT_DATE, M.NO_OF_BARCODE, B.ORDER_ID, B.SALE_RECORD_NO, OM.QTY, S.ITEM_TITLE, B.BARCODE_NO, OM.SALE_PRICE, OM.SHIPING_LABEL_RATE, OM.EBAY_FEE, (SELECT NVL(SUM(OD.PACKING_COST), 0) FROM LJ_ORDER_PACKING_MT O, LJ_ORDER_PACKING_DT OD WHERE O.ORDER_PACKING_ID = OD.ORDER_PACKING_ID AND O.ORDER_ID = OM.ORDER_ID GROUP BY OD.ORDER_PACKING_ID) PACKING_COST, (SELECT NVL(BIL.CHARGES, 0) FROM LJ_BARCODE_BILLING BIL WHERE BIL.BARCODE_NO = L.BARCODE_PRV_NO /*B.BARCODE_NO*/ AND BIL.SER_RATE_ID = 1) PIC_CHARGE, /*PIC SERVICE CHARGE*/ (SELECT NVL(BIL.CHARGES, 0) FROM LJ_BARCODE_BILLING BIL, LZ_BARCODE_MT BAR WHERE BIL.BARCODE_NO = B.BARCODE_NO AND BIL.BARCODE_NO = BAR.BARCODE_NO AND BIL.SER_RATE_ID = 2 AND BAR.EBAY_ITEM_ID IS NOT NULL) LIST_CHARGE, (SELECT NVL(BIL.CHARGES, 0) FROM LJ_BARCODE_BILLING BIL, LZ_BARCODE_MT BAR WHERE BIL.BARCODE_NO = B.BARCODE_NO AND BIL.BARCODE_NO = BAR.BARCODE_NO AND BIL.SER_RATE_ID = 7 AND BAR.EBAY_ITEM_ID IS NOT NULL) PACK_PULL_CHARGE, OM.BUYER_NAME, OM.SALE_RECRD_NUM, S.EBAY_PRICE, S.DEFAULT_COND, S.F_UPC,S.F_MPN, S.F_MANUFACTURE, S.CATEGORY_NAME, CD.COND_NAME FROM LOT_DEFINATION_MT      MD, LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LZ_BARCODE_MT          B, LZ_ITEM_SEED           S, LJ_ORDER_PACKING_MT    OM, LZ_ITEM_COND_MT CD WHERE M.MT_ID = D.MT_ID AND MD.LOT_ID = M.LOT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND B.ITEM_ID = S.ITEM_ID(+) AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND B.CONDITION_ID = S.DEFAULT_COND(+) AND B.ORDER_ID = OM.ORDER_ID(+) AND S.DEFAULT_COND = CD.ID AND M.MERCHANT_ID = $merchId ";

        if (!empty($getSeach)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $sql .= " and (UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    } else {
                        $sql .= " AND UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $sql .= " and (UPPER(S.ITEM_TITLE) LIKE '%$getSeach%' ";
            }

            $sql .= " OR UPPER(B.BARCODE_NO) LIKE '%$getSeach%'
           OR UPPER(OM.BUYER_NAME) LIKE '%$getSeach%'
           OR UPPER(OM.SALE_RECRD_NUM) LIKE '%$getSeach%'
           OR UPPER(B.EBAY_ITEM_ID) LIKE '%$getSeach%' )";

        }

        if ($get_param == 'ACTIVE') {

            $sql .= " AND B.ORDER_ID IS NULL ";

        } else if ($get_param == 'SOLD') {

            $sql .= " AND B.ORDER_ID IS NOT NULL ";
        }

        if (!empty($merLotName)) {
            $sql .= " and M.lot_id = $merLotName ";
        }

        if (!empty($priceOne) && !empty($priceTwo)) {

            $sql .= " AND OM.SALE_PRICE BETWEEN  $priceOne AND $priceTwo";

        }

        $sql .= " ) GROUP BY EBAY_ITEM_ID, ORDER_ID  ORDER BY  QTY DESC ";

        $lot_detail_data = $this->db->query($sql)->result_array();

        if (count($lot_detail_data) >= 1) {
            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();

            $uri = $this->get_barcodes_pictures($lot_detail_data, $conditions);
            $images = $uri['uri'];

            return array('lot_detail_data' => $lot_detail_data, 'lot_name' => $lot_name, "images" => $images, 'exist' => true);
        } else {
            return array('lot_detail_data' => $lot_detail_data, 'lot_name' => $lot_name, 'exist' => false);
        }

    }

    public function merchant_sold_latest()
    {

        $get_param = 'SOLD'; //strtoupper($this->input->post('param'));
        $getSeach = strtoupper($this->input->post('getSeach'));
        $getSeach = trim(str_replace("  ", ' ', $getSeach));
        $getSeach = str_replace(array("`,'"), "", $getSeach);
        $getSeach = str_replace(array("'"), "''", $getSeach);

        $priceOne = strtoupper($this->input->post('priceOne'));
        $priceOne = trim(str_replace("  ", ' ', $priceOne));
        $priceOne = str_replace(array("`,'"), "", $priceOne);
        $priceOne = str_replace(array("'"), "''", $priceOne);

        $priceTwo = strtoupper($this->input->post('priceTwo'));
        $priceTwo = trim(str_replace("  ", ' ', $priceTwo));
        $priceTwo = str_replace(array("`,'"), "", $priceTwo);
        $priceTwo = str_replace(array("'"), "''", $priceTwo);

        $merLotName = strtoupper($this->input->post('merLotName'));
        $merchId = strtoupper($this->input->post('merchId'));
        $str = explode(' ', $getSeach);

        $lot_name = $this->db->query("SELECT M.LOT_ID, M.LOT_DESC ||'  ('|| M.REF_NO ||')' LOT_DESC FROM LOT_DEFINATION_MT M WHERE M.MERCHANT_ID  =$merchId ")->result_array();

        $sql = "SELECT EBAY_ITEM_ID, MAX(ITEM_TITLE) ITM_TITLE, MAX(QTY) SOLD_QTY, COUNT(EBAY_ITEM_ID) QTY, MAX(SALE_PRICE) SALE_PRICE, MAX(QTY) * MAX(SALE_PRICE) TOTAL, MAX(SHIPING_LABEL_RATE) SHIPING_PRICE, MAX(EBAY_FEE) EBAY_FEE, MAX(PACKING_COST) PACKING_COST, NVL((7 / 100) * NVL(MAX(SALE_PRICE * QTY), 0), 0) MARKEPLACE_FEE, ROUND(NVL(SUM(NVL(PIC_CHARGE, 0)) + SUM(NVL(LIST_CHARGE, 0)) + SUM(NVL(PACK_PULL_CHARGE, 0)), 0), 2) LJ_CHARGES, MAX(BUYER_NAME) BUY_NAME, MAX(SALE_RECRD_NUM) SALE_REC, MAX(CHECKOUT_DATE) CHECKOUT_DATE, MAX(SALE_DATE) SALE_DATE, MAX(EBAY_PRICE) EBAY_PRICE, MAX(DEFAULT_COND) DEFAULT_COND, MAX(F_UPC) F_UPC, MAX(F_MPN)F_MPN , MAX(F_MANUFACTURE)F_MANUFACTURE, MAX(CATEGORY_NAME)CATEGORY_NAME, MAX(COND_NAME) COND_NAME FROM (SELECT M.LOT_ID, S.SEED_ID, B.EBAY_ITEM_ID, OM.SALE_DATE, OM.CHECKOUT_DATE, M.NO_OF_BARCODE, B.ORDER_ID, B.SALE_RECORD_NO, OM.QTY, S.ITEM_TITLE, B.BARCODE_NO, OM.SALE_PRICE, OM.SHIPING_LABEL_RATE, OM.EBAY_FEE, (SELECT NVL(SUM(OD.PACKING_COST), 0) FROM LJ_ORDER_PACKING_MT O, LJ_ORDER_PACKING_DT OD WHERE O.ORDER_PACKING_ID = OD.ORDER_PACKING_ID AND O.ORDER_ID = OM.ORDER_ID GROUP BY OD.ORDER_PACKING_ID) PACKING_COST, (SELECT NVL(BIL.CHARGES, 0) FROM LJ_BARCODE_BILLING BIL WHERE BIL.BARCODE_NO = L.BARCODE_PRV_NO /*B.BARCODE_NO*/ AND BIL.SER_RATE_ID = 1) PIC_CHARGE, /*PIC SERVICE CHARGE*/ (SELECT NVL(BIL.CHARGES, 0) FROM LJ_BARCODE_BILLING BIL, LZ_BARCODE_MT BAR WHERE BIL.BARCODE_NO = B.BARCODE_NO AND BIL.BARCODE_NO = BAR.BARCODE_NO AND BIL.SER_RATE_ID = 2 AND BAR.EBAY_ITEM_ID IS NOT NULL) LIST_CHARGE, (SELECT NVL(BIL.CHARGES, 0) FROM LJ_BARCODE_BILLING BIL, LZ_BARCODE_MT BAR WHERE BIL.BARCODE_NO = B.BARCODE_NO AND BIL.BARCODE_NO = BAR.BARCODE_NO AND BIL.SER_RATE_ID = 7 AND BAR.EBAY_ITEM_ID IS NOT NULL) PACK_PULL_CHARGE, OM.BUYER_NAME, OM.SALE_RECRD_NUM, S.EBAY_PRICE, S.DEFAULT_COND, S.F_UPC,S.F_MPN, S.F_MANUFACTURE, S.CATEGORY_NAME, CD.COND_NAME FROM LOT_DEFINATION_MT      MD, LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LZ_BARCODE_MT          B, LZ_ITEM_SEED           S, LJ_ORDER_PACKING_MT    OM, LZ_ITEM_COND_MT CD WHERE M.MT_ID = D.MT_ID AND MD.LOT_ID = M.LOT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND B.ITEM_ID = S.ITEM_ID(+) AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND B.CONDITION_ID = S.DEFAULT_COND(+) AND B.ORDER_ID = OM.ORDER_ID(+) AND S.DEFAULT_COND = CD.ID AND M.MERCHANT_ID = $merchId ";

        if (!empty($getSeach)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $sql .= " and (UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    } else {
                        $sql .= " AND UPPER(S.ITEM_TITLE) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $sql .= " and (UPPER(S.ITEM_TITLE) LIKE '%$getSeach%' ";
            }

            $sql .= " OR UPPER(B.BARCODE_NO) LIKE '%$getSeach%'
           OR UPPER(OM.BUYER_NAME) LIKE '%$getSeach%'
           OR UPPER(OM.SALE_RECRD_NUM) LIKE '%$getSeach%'
           OR UPPER(B.EBAY_ITEM_ID) LIKE '%$getSeach%' )";

        }

        if ($get_param == 'ACTIVE') {

            $sql .= " AND B.ORDER_ID IS NULL ";

        } else if ($get_param == 'SOLD') {

            $sql .= " AND B.ORDER_ID IS NOT NULL ";
        }

        if (!empty($merLotName)) {
            $sql .= " and M.lot_id = $merLotName ";
        }

        if (!empty($priceOne) && !empty($priceTwo)) {

            $sql .= " AND OM.SALE_PRICE BETWEEN  $priceOne AND $priceTwo";

        }

        $sql .= " ) GROUP BY EBAY_ITEM_ID, ORDER_ID  ORDER BY  QTY DESC ";

        $lot_detail_data = $this->db->query($sql)->result_array();

        if (count($lot_detail_data) >= 1) {
            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();

            $uri = $this->get_barcodes_pictures($lot_detail_data, $conditions);
            $images = $uri['uri'];

            return array('lot_detail_data' => $lot_detail_data, 'lot_name' => $lot_name, "images" => $images, 'exist' => true);
        } else {
            return array('lot_detail_data' => $lot_detail_data, 'lot_name' => $lot_name, 'exist' => false);
        }

    }

    public function merchant_dashboard()
    {

        $merchId = $this->input->post('merchId');

        $merhc_dash = $this->db->query("SELECT COUNT(BARCODE_NO) TOT_BARC, COUNT(PIC_DATE_TIME) PICTURS_DONE, COUNT(LZ_MANIFEST_DET_ID) BARCODE_CREATED, COUNT(EBAY_ITEM_ID) - COUNT(SALE_RECORD_NO) ACTIVE_LISTED, COUNT(LZ_MANIFEST_DET_ID) - COUNT(EBAY_ITEM_ID) - COUNT(OUTT) NOT_LISTED, COUNT(SALE_RECORD_NO) SOLD, ROUND(NVL(SUM(TOTAL_VAL), 0), 2) PROCESED_QTY_VAL, ROUND(NVL(SUM(LIST_VAL) - SUM(SOLD_VALUE), 0), 2) ACTIV_LIST_QTY_VAL, ROUND(NVL(SUM(NOT_LIST_VAL), 0), 2) ACTIV_NOT_LIST_QTY_VAL, ROUND(NVL(SUM(SOLD_VALUE), 0), 2) SOLD_QTY_VAL FROM (SELECT D.BARCODE_NO, B.ITEM_ADJ_DET_ID_FOR_OUT OUTT, L.BARCODE_PRV_NO, L.PIC_DATE_TIME, L.LZ_MANIFEST_DET_ID, B.ITEM_ID, B.LZ_MANIFEST_ID, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO) TOTAL_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL) LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NULL AND B.ITEM_ADJ_DET_ID_FOR_OUT IS NULL ) NOT_LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL AND B.ORDER_ID IS NOT NULL) SOLD_VALUE, B.EBAY_ITEM_ID, B.ORDER_ID SALE_RECORD_NO FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LZ_BARCODE_MT          B WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND M.MERCHANT_ID = $merchId /* AND M.LOT_ID = 31*/ ) ")->result_array();

        return array('merhc_dash' => $merhc_dash, 'exist' => true);

    }
    public function mer_active_listed()
    {
        $merchId = $this->input->post('merchId');
        $avail_cat = $this->input->post('avail_cat');

        $merhc_activ_list = "SELECT B.EBAY_ITEM_ID,max(s.seed_id) seed_id,max(s.ebay_price) pric, COUNT(B.EBAY_ITEM_ID) QTY, MAX(S.ITEM_TITLE) ITEM_DESC, MAX(S.F_MANUFACTURE) ITEM_MT_MANUFACTURE, MAX(S.F_MPN) ITEM_MT_MFG_PART_NO, MAX(S.F_UPC) ITEM_MT_UPC, /*MAX(I.ITEM_DESC) ITEM_DESC, MAX(I.ITEM_MT_MANUFACTURE) ITEM_MT_MANUFACTURE, MAX(I.ITEM_MT_MFG_PART_NO) ITEM_MT_MFG_PART_NO, MAX(I.ITEM_MT_UPC) ITEM_MT_UPC,*/ MAX(COND_NAME) CONDITION_ID,MAX(CA.CATEGORY_NAME)  NAME FROM LZ_MERCHANT_BARCODE_MT M, LZ_ITEM_COND_MT        C, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LZ_BARCODE_MT          B, /*ITEMS_MT               I,*/ LZ_ITEM_SEED S,LZ_BD_CATEGORY    CA WHERE M.MT_ID = D.MT_ID AND B.CONDITION_ID = C.ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND B.ITEM_ID = S.ITEM_ID AND B.LZ_MANIFEST_ID =  S.LZ_MANIFEST_ID AND S.CATEGORY_ID = CA.CATEGORY_ID(+) AND B.CONDITION_ID = S.DEFAULT_COND /*AND B.ITEM_ID = I.ITEM_ID(+)*/ AND B.EBAY_ITEM_ID IS NOT NULL AND B.SALE_RECORD_NO IS NULL AND M.MERCHANT_ID = $merchId  ";
        if (!empty($avail_cat)) {
            $merhc_activ_list .= " AND S.CATEGORY_ID = $avail_cat ";
        }

        $merhc_activ_list .= " GROUP BY  B.EBAY_ITEM_ID ";

        $merhc_activ_list = $this->db->query($merhc_activ_list)->result_array();

        $merch_act_list_categry = $this->db->query("SELECT count(s.category_id) cat_id, s.category_id, max(ca.category_name) || ' (' || count(s.category_id) || ')' name FROM LZ_MERCHANT_BARCODE_MT M, LZ_ITEM_COND_MT  C, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS  L, LZ_BARCODE_MT B, LZ_ITEM_SEED  S, lz_bd_category    ca WHERE M.MT_ID = D.MT_ID AND B.CONDITION_ID = C.ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND B.ITEM_ID = S.ITEM_ID AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID AND B.CONDITION_ID = S.DEFAULT_COND AND B.EBAY_ITEM_ID IS NOT NULL AND B.SALE_RECORD_NO IS NULL and s.category_id = ca.category_id(+) AND M.MERCHANT_ID = $merchId GROUP BY s.category_id ")->result_array();

        return array('merhc_activ_list' => $merhc_activ_list, 'merch_act_list_categry' => $merch_act_list_categry, 'exist' => true);

    }
    public function mer_active_not_listed()
    {
        $merchId = $this->input->post('merchId');

        $merhc_activ_not_list = $this->db->query("SELECT D.BARCODE_NO, L.BARCODE_PRV_NO, L.PIC_DATE_TIME, L.LZ_MANIFEST_DET_ID, B.EBAY_ITEM_ID, B.SALE_RECORD_NO, B.ITEM_ID, I.ITEM_DESC, I.ITEM_MT_MANUFACTURE, I.ITEM_MT_MFG_PART_NO, I.ITEM_MT_UPC, I.ITEM_CONDITION, C.COND_NAME CONDITION_ID FROM LZ_MERCHANT_BARCODE_MT M, LZ_ITEM_COND_MT C,LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS L, LZ_BARCODE_MT B, ITEMS_MT  I WHERE M.MT_ID = D.MT_ID AND B.CONDITION_ID = C.ID  AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND B.ITEM_ID = I.ITEM_ID (+) AND B.EBAY_ITEM_ID IS  NULL  and l.lz_manifest_det_id is not null   AND M.MERCHANT_ID = $merchId order by BARCODE_NO desc ")->result_array();

        return array('merhc_activ_not_list' => $merhc_activ_not_list, 'exist' => true);

    }
    // public function mer_active_listed_category(){
    //    $merchId = $this->input->post('merchId');

    //   $merch_act_list_categry = $this->db->query("SELECT count(s.category_id) cat_id, s.category_id, max(ca.category_name) || ' (' || count(s.category_id) || ')' name FROM LZ_MERCHANT_BARCODE_MT M, LZ_ITEM_COND_MT  C, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS  L, LZ_BARCODE_MT B, LZ_ITEM_SEED  S, lz_bd_category_tree    ca WHERE M.MT_ID = D.MT_ID AND B.CONDITION_ID = C.ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND B.ITEM_ID = S.ITEM_ID AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID AND B.CONDITION_ID = S.DEFAULT_COND AND B.EBAY_ITEM_ID IS NOT NULL AND B.SALE_RECORD_NO IS NULL and s.category_id = ca.category_id(+) AND M.MERCHANT_ID = $merchId GROUP BY s.category_id ")->result_array();

    //   return array('merch_act_list_categry' => $merch_act_list_categry,'exist'=>true);

    // }
    public function convert_text($text)
    {

        $t = $text;
        // '\'' => '%27',
        // '~' => '%7E',
        // '_' => '%5F',
        // '/' => '%2F',
        // '\\' => '%5C',
        // '.' => '%2E',
        // '%' => '%25',
        // ':' => '%3A',
        // ' ' => '%20',
        $specChars = array(
            '!' => '%21', '"' => '%22',
            '#' => '%23', '$' => '%24',
            '&' => '%26', '(' => '%28',
            ')' => '%29', '*' => '%2A', '+' => '%2B',
            ',' => '%2C', '-' => '%2D',
            ';' => '%3B',
            '<' => '%3C', '=' => '%3D', '>' => '%3E',
            '?' => '%3F', '@' => '%40', '[' => '%5B',
            ']' => '%5D', '^' => '%5E',
            '`' => '%60', '{' => '%7B',
            '|' => '%7C', '}' => '%7D',
            ',' => '%E2%80%9A',
        );

        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }

        return $t;
    }
    // public function get_pictures()
    // {

    //     $barocde_no = $this->input->post('barocde_no');
    //     $bar_val = $barocde_no;
    //     // var_dump($bar_val);
    //     // exit;
    //     // //var_dump($barocde_no['barcodePass']);
    //     //  var_dump($barocde_no);
    //     //  exit;
    //     //$bar_val = $barocde_no['barcodePass'] ;

    //     $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

    //     if (count($qry) >= 1) {

    //         $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
    //         $path = $path->result_array();
    //         $master_path = $path[0]["MASTER_PATH"];

    //         $barcode = $qry[0]["FOLDER_NAME"];
    //         $dir = $master_path . $barcode . "/";
    //         // var_dump($dir);
    //         // exit;

    //     } else {

    //         $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
    //         $path = $path->result_array();
    //         $master_path = $path[0]["MASTER_PATH"];

    //         $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$barocde_no' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();

    //         //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

    //         $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
    //         $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
    //         $cond = @$get_params[0]['COND_NAME'];

    //         $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";

    //     }

    //     $dir = preg_replace("/[\r\n]*/", "", $dir);
    //     // var_dump($dir);
    //     // exit;

    //     $uri = [];
    //     $base64 = [];

    //     //var_dump(is_dir($dir));exit;
    //     if (is_dir($dir)) {
    //         //var_dump($dir);exit;
    //         $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
    //         $i = 0;
    //         $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
    //         foreach ($images as $image) {
    //             $pathinfo = pathinfo($image);
    //             $imageName = explode("\\", $image);
    //             $imagePath = $imageName[0] . $imageName[1];
    //             // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
    //             $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $imagePath);
    //             $withoutMasterPartUri = $this->convert_text(@$withoutMasterPartUri);
    //             $uri[$i] = $base_url . $withoutMasterPartUri;
    //             $base64[$i]['image'] = $base_url . $withoutMasterPartUri;
    //             // $base64[$i]['img'] = base64_encode(file_get_contents($base_url . $withoutMasterPartUri));
    //             // $exif = exif_read_data($image);
    //             // print_r($exif);
    //             // var_dump($pathinfo);
    //             $ext = $pathinfo['extension'];
    //             $base64[$i]['ext'] = $ext;
    //             $base64[$i]['filename'] = $pathinfo['filename'];
    //             // $base64[$i]['image64'] = base64_encode(file_get_contents($base_url . $withoutMasterPartUri));
    //             $i++;
    //         }
    //     }

    //     //var_dump($dekitted_pics);exit;
    //     return array('uri' => $uri, 'base64' => $base64);

    // }

    public function get_pictures_from_dir()
    {

        $data = $this->input->post('barcodes');
        foreach ($data as $value) {
            $barocde_no = $value['BARCODE_NO'];
            $barocde_no = trim(str_replace("  ", ' ', $barocde_no));
            $barocde_no = trim(str_replace(array("'"), "''", $barocde_no));
            $bar_val = $barocde_no;

            $chek_file = false;

            $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

            if (count($qry) >= 1) {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];
                $path_id = 2;

                $barcode = $qry[0]["FOLDER_NAME"];
                $folder_name = $qry[0]["FOLDER_NAME"];
                $dir = $master_path . $barcode . "/";
                // var_dump($dir);
                // exit;

            } else {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];
                $path_id = 1;
                $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$barocde_no' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();

                //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                $cond = @$get_params[0]['COND_NAME'];

                $folder_name = @$upc . "~" . @$mpn; //$qry[0]["FOLDER_NAME"];

                $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";

            }

            // check barcode pics in db and directory
            $chek_file = false;
            $chek_pic_db = $this->db->query("select jd.img_dt_id,jd.img_mt_id, jm.folder_name, jd.local_url,jm.base_url,pc.MASTER_PATH ,REPLACE(LOCAL_URL,'http://71.78.236.20/item_pictures/','D:/wamp/www/item_pictures/') specfic_url from lj_barcode_pic_mt jm, lj_barcode_pic_dt jd,lz_pict_path_config pc where jm.img_mt_id = jd.img_mt_id and jm.base_url = pc.path_id and jm.folder_name = '$folder_name' order by IMG_DT_ID asc")->result_array();
            if (count($chek_pic_db) > 0) {

                foreach ($chek_pic_db as $pic_url) {
                    $check_url = $pic_url['SPECFIC_URL'];
                    $delet_table_fk = $pic_url['IMG_MT_ID'];
                    //var_dump($check_url);

                    //if no file exit and url in db is wrong then delete all entries to renter
                    if (!file_exists($check_url)) {
                        $chek_file = false;
                        $this->db->query(" delete from lj_barcode_pic_dt s where s.img_mt_id= '$delet_table_fk' ");
                        $this->db->query("delete from lz_barcode_pic u where u.barcode_no = '$barocde_no' ");
                        break;

                    } else {
                        $chek_file = true;
                    }

                }
            } else {
                $chek_file = false;
                // insert into db
            }
            // check barcode pics in db and directory

            $dir = preg_replace("/[\r\n]*/", "", $dir);
            $uri = [];
            $base64 = [];

            //var_dump(is_dir($dir));exit;
            if (is_dir($dir)) {
                //var_dump($dir);exit;
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
                $i = 0;
                $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
                foreach ($images as $image) {
                    $pathinfo = pathinfo($image);
                    $imageName = explode("\\", $image);
                    $imagePath = $imageName[0] . $imageName[1];
                    // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                    $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $imagePath);
                    $withoutMasterPartUri = $this->convert_text(@$withoutMasterPartUri);
                    $uri[$i] = $base_url . $withoutMasterPartUri;
                    $base64[$i]['image'] = $base_url . $withoutMasterPartUri;
                    // $base64[$i]['img'] = base64_encode(file_get_contents($base_url . $withoutMasterPartUri));
                    // $exif = exif_read_data($image);
                    // print_r($exif);
                    // var_dump($pathinfo);
                    $ext = $pathinfo['extension'];
                    $base64[$i]['ext'] = $ext;
                    $base64[$i]['filename'] = $pathinfo['filename'];

                    if ($chek_file == false) {
                        $this->insert_url_in_db($folder_name, $uri[$i], $i, $path_id);
                    }

                    // if()
                    // $base64[$i]['image64'] = base64_encode(file_get_contents($base_url . $withoutMasterPartUri));
                    $i++;
                }
            }
        }

        //var_dump($dekitted_pics);exit;
        return array('status' => true, 'message' => 'Record Updated Successfully');

    }

    public function get_pictures()
    {
        // var_dump('asdasd');
        // exit;

        $barocde_no = $this->input->post('barocde_no');
        $barocde_no = trim(str_replace("  ", ' ', $barocde_no));
        $barocde_no = trim(str_replace(array("'"), "''", $barocde_no));
        $bar_val = $barocde_no;

        $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

        if (count($qry) >= 1) {

            $path = $this->db->query("SELECT  MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
            $path = $path->result_array();
            $master_path = $path[0]["MASTER_PATH"];
            $path_id = 2;

            $barcode = $qry[0]["FOLDER_NAME"];
            $folder_name = $qry[0]["FOLDER_NAME"];
            $dir = $master_path . $barcode . "/";
            // var_dump($dir);
            // exit;

        } else {

            $path = $this->db->query("SELECT  MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
            $path = $path->result_array();
            $master_path = $path[0]["MASTER_PATH"];
            $path_id = 1;
            $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$barocde_no' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();

            //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

            $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
            $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
            $cond = @$get_params[0]['COND_NAME'];

            $folder_name = @$upc . "~" . @$mpn; //$qry[0]["FOLDER_NAME"];

            $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";

        }
        // var_dump($dir);
        // exit;
        // check barcode pics in db and directory
        $chek_file = false;
        $chek_pic_db = $this->db->query("select jd.img_dt_id,jd.img_mt_id, jm.folder_name, jd.local_url,jm.base_url,pc.MASTER_PATH ,REPLACE(LOCAL_URL,'http://71.78.236.20/item_pictures/','D:/wamp/www/item_pictures/') specfic_url from lj_barcode_pic_mt jm, lj_barcode_pic_dt jd,lz_pict_path_config pc where jm.img_mt_id = jd.img_mt_id and jm.base_url = pc.path_id and jm.folder_name = '$folder_name' order by IMG_DT_ID asc")->result_array();
        if (count($chek_pic_db) > 0) {

            foreach ($chek_pic_db as $pic_url) {
                $check_url = $pic_url['SPECFIC_URL'];
                $delet_table_fk = $pic_url['IMG_MT_ID'];
                //var_dump($check_url);

                //if no file exit and url in db is wrong then delete all entries to renter
                if (!file_exists($check_url)) {
                    $chek_file = false;
                    $this->db->query(" delete from lj_barcode_pic_dt s where s.img_mt_id= '$delet_table_fk' ");
                    $this->db->query("delete from lz_barcode_pic u where u.barcode_no = '$barocde_no' ");
                    break;

                } else {
                    $chek_file = true;
                }

            }
        } else {
            $chek_file = false;
            // insert into db
        }
        // check barcode pics in db and directory

        $dir = preg_replace("/[\r\n]*/", "", $dir);
        $uri = [];
        $base64 = [];
        // var_dump($dir);
        // var_dump(is_dir($dir));exit;
        if (is_dir($dir)) {
            //var_dump($dir);exit;
            $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
            $i = 0;

            file:///E:/wamp64/
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            foreach ($images as $image) {
                $pathinfo = pathinfo($image);
                $imageName = explode("\\", $image);
                $imagePath = $imageName[0] . $imageName[1];
                 $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                //$withoutMasterPartUri = str_replace("E:/wamp64/www/", "", $imagePath);
                $withoutMasterPartUri = $this->convert_text(@$withoutMasterPartUri);
                $uri[$i] = $base_url . $withoutMasterPartUri;
                $base64[$i]['image'] = $base_url . $withoutMasterPartUri;
                // $base64[$i]['img'] = base64_encode(file_get_contents($base_url . $withoutMasterPartUri));
                // $exif = exif_read_data($image);
                // print_r($exif);
                 
                $ext = $pathinfo['extension'];
                $base64[$i]['ext'] = $ext;
                $base64[$i]['filename'] = $pathinfo['filename'];

                if ($chek_file == false) {
                    $this->insert_url_in_db($folder_name, $uri[$i], $i, $path_id);
                }

                // if()
                // $base64[$i]['image64'] = base64_encode(file_get_contents($base_url . $withoutMasterPartUri));
                $i++;
            }
        }

        //var_dump($dekitted_pics);exit;
        return array('uri' => $uri, 'base64' => $base64, 'chek_file' => $chek_file);

    }

    public function insert_url_in_db($barcoe_param, $url, $order, $path_id)
    {

        $chek_pic_db = $this->db->query("select IMG_MT_ID from lj_barcode_pic_mt where folder_name = '$barcoe_param' and rownum<=1 ")->result_array();

        if (count($chek_pic_db) > 0) {

            $get_det_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lj_barcode_pic_dt','IMG_DT_ID') IMG_DT_ID FROM DUAL");
            $get_det_pk = $get_det_pk->result_array();
            $get_det_pk = $get_det_pk[0]['IMG_DT_ID'];
            $get_img_mt_id = $chek_pic_db[0]['IMG_MT_ID'];

            $this->db->query("INSERT INTO LJ_BARCODE_PIC_DT (IMG_DT_ID, IMG_MT_ID, LOCAL_URL, PIC_ORDER, INSERTED_BY, INSERTED_DATE, IMAGE_NAME,CHECK_UPLOAD_IMAGE_BLOCK) VALUES ('$get_det_pk','$get_img_mt_id','$url','$order',81,SYSDATE,'','get_pictures')");

        } else {

            $get_img_mt_id = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_BARCODE_PIC_MT','IMG_MT_ID') IMG_mt_ID FROM DUAL");
            $get_img_mt_id = $get_img_mt_id->result_array();
            $get_img_mt_id = $get_img_mt_id[0]['IMG_MT_ID'];

            $get_det_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lj_barcode_pic_dt','IMG_DT_ID') IMG_DT_ID FROM DUAL");
            $get_det_pk = $get_det_pk->result_array();
            $get_det_pk = $get_det_pk[0]['IMG_DT_ID'];

            $this->db->query("INSERT INTO LJ_BARCODE_PIC_MT (IMG_MT_ID, FOLDER_NAME, INSERTED_BY, INSERTED_DATE, BASE_URL) VALUES ($get_img_mt_id, '$barcoe_param','81',sysdate,$path_id) ");
            $this->db->query("INSERT INTO LJ_BARCODE_PIC_DT (IMG_DT_ID, IMG_MT_ID, LOCAL_URL, PIC_ORDER, INSERTED_BY, INSERTED_DATE, IMAGE_NAME,CHECK_UPLOAD_IMAGE_BLOCK) VALUES ('$get_det_pk','$get_img_mt_id','$url','$order',81,SYSDATE,'','get_pictures')");

        }
    }

    public function get_dropdowns()
    {
        //$barocde = 276189;
        $barocde = $this->input->post('barocde');

        // $get_remarks = $this->db->query("SELECT 'Dekit Item => ' ||kK.DEKIT_REMARKS DEKIT_REMARKS , 'Identity Remarks => ' || KK.IDENT_REMARKS IDENT_REMARKS,'Barcode Notes => ' || KK.BARCODE_NOTES  BARCODE_NOTES FROM LZ_DEKIT_US_DT KK where kk.barcode_prv_no  ='$barocde' union all SELECT 'Special Lot => ' ||L.LOT_REMARKS DEKIT_REMARKS,'Identity Remarks => ' ||L.PIC_NOTES IDENT_REMARKS,'Barcode Notes =>' ||L.BARCODE_NOTES FROM LZ_SPECIAL_LOTS L where l.barcode_prv_no  ='$barocde' ")->result_array();
        $get_remarks = $this->db->query("SELECT 'END ITEM => ' || LG.REMARKS || '(EBAY_ID => ' || AL.EBAY_ID || ')  '  || '(' ||M.USER_NAME ||')'  REMARKS, M.USER_NAME END_BY FROM LJ_AGINIG_ITEM_LOG AL, LZ_ENDITEM_LOG LG, EMPLOYEE_MT M WHERE AL.LOG_ID = LG.LOG_ID AND LG.ENDED_BY = M.EMPLOYEE_ID(+)  AND AL.BARCODE_NO ='$barocde'")->result_array();
        $condition_quer = $this->db->query("SELECT C.ID,C.COND_NAME FROM LZ_ITEM_COND_MT C ORDER BY C.ID ASC")->result_array();

        $ship_quer = $this->db->query("SELECT n.SHIPING_NAME,n.DISPLAY_NAME FROM LZ_SHIPING_NAME N WHERE N.ID IN (1,2,3,11,18) ORDER BY N.DISPLAY_ORDER ASC")->result_array();
        //$ship_quer = $this->db->query("SELECT S.SHIPING_NAME FROM LZ_SHIPING_NAME S ORDER BY S.ID ASC")->result_array();

        $temp_data = $this->db->query("SELECT TEMPLATE_ID,TEMPLATE_NAME FROM LZ_ITEM_TEMPLATE ORDER BY TEMPLATE_NAME DESC")->result_array();

        $macro_type = $this->db->query("SELECT T.TYPE_ID,T.TYPE_DESCRIPTION,T.TYPE_ORDER FROM LZ_MACRO_TYPE T ORDER BY T.TYPE_DESCRIPTION  ASC")->result_array();
        $get_sedd_param = $this->db->query("SELECT U.LZ_MANIFEST_ID,U.EBAY_ITEM_ID,U.ITEM_ID,U.CONDITION_ID FROM LZ_BARCODE_MT U WHERE U.BARCODE_NO = $barocde")->result_array();

        if (count($get_sedd_param) > 0) {
            $manif_id = $get_sedd_param[0]['LZ_MANIFEST_ID'];
            $item_id = $get_sedd_param[0]['ITEM_ID'];
            $cond_id = $get_sedd_param[0]['CONDITION_ID'];
            $ebay_item_id = $get_sedd_param[0]['EBAY_ITEM_ID'];

            $seed_data = $this->db->query("SELECT S.SEED_ID,
            S.QC_CHECK,
            S.QC_REMARKS,
            B.BARCODE_NO IT_BARCODE,
            /*DECODE(S.F_MPN, NULL, I.ITEM_MT_MFG_PART_NO, S.F_MPN) MFG_PART_NO, DECODE(S.F_UPC,NULL,D.ITEM_MT_UPC,S.F_UPC) UPC*/
            S.F_MPN MFG_PART_NO,
            S.F_UPC UPC,
            DECODE(S.F_MANUFACTURE, NULL, I.ITEM_MT_MANUFACTURE, S.F_MANUFACTURE) MANUFACTURER,
            I.ITEM_MT_BBY_SKU SKU_NO,
            I.ITEM_CODE LAPTOP_ITEM_CODE,
            s.item_title ITEM_MT_DESC,
            S.ITEM_DESC DESCR,
            D.WEIGHT,
            S.*,
            s.qc_date ||' ('|| m.user_name||')' qc_date,
            R.GENERAL_RULE,
            R.SPECIFIC_RULE,
            BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME,
            C.COND_NAME,
            S.CATEGORY_ID,
            I.ITEM_LENGTH,
            I.ITEM_WIDTH,
            I.ITEM_HEIGHT,
            D.LAPTOP_ZONE_ID
            FROM LZ_ITEM_SEED S,
            LZ_BARCODE_MT B,
            LZ_MANIFEST_DET D,
            ITEMS_MT I,
            LZ_LISTING_RULES R,
            BIN_MT BM,
            LZ_ITEM_COND_MT C,
            employee_mt m
            WHERE B.ITEM_ID = S.ITEM_ID
            AND I.ITEM_ID = S.ITEM_ID
            AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID
            AND D.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID
            AND D.LAPTOP_ITEM_CODE = I.ITEM_CODE
            AND R.ITEM_CONDITION = S.DEFAULT_COND
            AND D.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID
            AND D.LAPTOP_ITEM_CODE = I.ITEM_CODE
            AND BM.BIN_ID = B.BIN_ID
            AND S.DEFAULT_COND = C.ID
            and s.qc_by = m.employee_id(+)
            /*AND S.SEED_ID = 10000000041*/
            AND S.LZ_MANIFEST_ID = $manif_id
            AND S.ITEM_ID = $item_id
            AND S.DEFAULT_COND = $cond_id
            AND ROWNUM = 1")->result_array();
            if (count($seed_data) > 0) {
                $mfg_part_no = $seed_data[0]['MFG_PART_NO'];
                $category_id = $seed_data[0]['CATEGORY_ID'];
            } else {
                $mfg_part_no = "''";
                $category_id = "''";
            }

            $list_qty = $this->db->query("SELECT COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 AND BC.EBAY_ITEM_ID IS NULL AND BC.LIST_ID IS NULL AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL AND BC.ORDER_DT_ID IS NULL AND BC.NOT_FOUND_BY IS NULL AND BC.LZ_DEKIT_US_MT_ID_FOR_OUT IS NULL AND BC.RETURNED_ID IS NULL AND BC.DISCARD = 0 AND BC.CONDITION_ID <> -1 AND BC.LZ_MANIFEST_ID = $manif_id AND BC.ITEM_ID = $item_id  AND BC.CONDITION_ID = $cond_id GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID ");

            if ($list_qty->num_rows() > 0) {
                $list_qty = $list_qty->result_array();
                $list_qty = $list_qty[0]['QTY'];
            } else {
                $list_qty = null;
            }

            $ebay_paypal_qry = $this->db->query("SELECT ITEM_ID,PAYPAL_FEE,EBAY_FEE FROM (SELECT SD.SALES_RECORD_NUMBER, E.ITEM_ID, ROUND((SD.PAYPAL_PER_TRANS_FEE / SD.SALE_PRICE) * 100, 2) PAYPAL_FEE, ROUND((SD.EBAY_FEE_PERC / SD.SALE_PRICE) * 100, 2) EBAY_FEE FROM LZ_SALESLOAD_DET SD, EBAY_LIST_MT E WHERE SD.ITEM_ID = E.EBAY_ITEM_ID AND SD.SALE_PRICE > 0 AND SD.PAYPAL_PER_TRANS_FEE > 0 AND SD.EBAY_FEE_PERC > 0 AND SD.QUANTITY = 1 AND E.ITEM_ID = $item_id ORDER BY SD.SALES_RECORD_NUMBER DESC) WHERE ROWNUM =1");

            if ($ebay_paypal_qry->num_rows() > 0) {
                $ebay_paypal_qry = $ebay_paypal_qry->result_array();
            } else {
                $ebay_paypal_qry = null;
            }

            $ship_fee_qry = $this->db->query(" SELECT * FROM ( SELECT DISTINCT D.SHIP_FEE FROM LZ_MANIFEST_DET D WHERE D.LAPTOP_ITEM_CODE = (SELECT ITEM_CODE FROM ITEMS_MT WHERE ITEM_ID = $item_id) AND D.SHIP_FEE IS NOT NULL ORDER BY D.SHIP_FEE DESC) WHERE ROWNUM =1");

            if ($ship_fee_qry->num_rows() > 0) {
                $ship_fee_qry = $ship_fee_qry->result_array();
            } else {

                $ship_fee_qry = $this->db->query("SELECT O.SHIP_SERV SHIP_FEE,O.WEIGHT FROM LZ_CATALOGUE_MT C, LZ_BD_OBJECTS_MT O WHERE O.OBJECT_ID = C.OBJECT_ID AND C.CATEGORY_ID = '$category_id' AND UPPER(TRIM(C.MPN)) = UPPER(TRIM('$mfg_part_no'))");
                if ($ship_fee_qry->num_rows() > 0) {
                    $ship_fee_qry = $ship_fee_qry->result_array();
                } else {
                    $ship_fee_qry = null;
                }
            }

            $cost_qry = $this->db->query("SELECT D.LZ_MANIFEST_ID, I.ITEM_ID, MAX(D.PO_DETAIL_RETIAL_PRICE) COST_PRICE FROM LZ_MANIFEST_DET D, ITEMS_MT I WHERE D.LAPTOP_ITEM_CODE = I.ITEM_CODE AND I.ITEM_ID = $item_id AND D.LZ_MANIFEST_ID = $manif_id GROUP BY D.LZ_MANIFEST_ID, I.ITEM_ID");
            if ($cost_qry->num_rows() > 0) {
                $cost_qry = $cost_qry->result_array();
            } else {
                $cost_qry = null;
            }

            return array('seed_data' => $seed_data, 'get_remarks' => $get_remarks, 'list_qty' => $list_qty, 'ebay_paypal_qry' => $ebay_paypal_qry, 'ship_fee_qry' => $ship_fee_qry, 'exist' => true, 'condition_quer' => $condition_quer, 'ship_quer' => $ship_quer, 'temp_data' => $temp_data, 'macro_type' => $macro_type, 'cost_qry' => $cost_qry, 'ebay_item_id' => $ebay_item_id);

        } else {

            $seed_data = null;
            $list_qty = null;
            $ebay_paypal_qry = null;
            $ship_fee_qry = null;
            $ebay_item_id = null;

            return array('seed_data' => $seed_data, 'get_remarks' => $get_remarks, 'list_qty' => $list_qty, 'ebay_paypal_qry' => $ebay_paypal_qry, 'ship_fee_qry' => $ship_fee_qry, 'exist' => false, 'condition_quer' => $condition_quer, 'ship_quer' => $ship_quer, 'temp_data' => $temp_data, 'macro_type' => $macro_type, 'ebay_item_id' => $ebay_item_id);
        }

    }

    public function updateReviseStatus($ebay_id, $price)
    {
        $list_rslt = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('EBAY_LIST_MT','LIST_ID') LIST_ID FROM DUAL");
        $rs = $list_rslt->result_array();
        $LIST_ID = $rs[0]['LIST_ID'];
        //$this->session->set_userdata('list_id',$LIST_ID);
        /*========================================================
        =            get require column for insertion            =
        ========================================================*/

        $list_rslt = $this->db->query("SELECT * FROM (SELECT E.* FROM EBAY_LIST_MT E WHERE E.EBAY_ITEM_ID = '$ebay_id' ORDER BY E.LIST_ID DESC) WHERE ROWNUM = 1");
        $rslt_dta = $list_rslt->result_array();
        $LIST_ID = $rs[0]['LIST_ID'];

        /*=====  End of get require column for insertion  ======*/

        $status = "UPDATE";
        $forceRevise = 0;
        //$rslt_dta = $query->result_array();

        //$list_date = date("d/M/Y");// return format Aug/13/2016
        date_default_timezone_set("America/Chicago");
        $list_date = date("Y-m-d H:i:s");
        $list_date = "TO_DATE('" . $list_date . "', 'YYYY-MM-DD HH24:MI:SS')";
        $lister_id = $this->session->userdata('user_id');
        $ebay_item_desc = @$rslt_dta[0]['ITEM_TITLE'];
        $manifest_id = @$rslt_dta[0]['LZ_MANIFEST_ID'];
        $item_id = @$rslt_dta[0]['ITEM_ID'];
        $list_qty = 0;
        $ebay_item_id = $ebay_id;
        $list_price = @$price;
        $remarks = null;
        $single_entry_id = null;
        $salvage_qty = 0.00;
        $entry_type = "L";
        $LZ_SELLER_ACCT_ID = @$rslt_dta[0]['LZ_SELLER_ACCT_ID'];
        $condition_id = @$rslt_dta[0]['ITEM_CONDITION'];
        $seed_id = @$rslt_dta[0]['SEED_ID'];
        //$auth_by_id = $this->session->userdata('auth_by_id');
        $list_qty = 0;

        $insert_query = $this->db->query("INSERT INTO ebay_list_mt (LIST_ID, LZ_MANIFEST_ID, LISTING_NO, ITEM_ID, LIST_DATE, LISTER_ID, EBAY_ITEM_DESC, LIST_QTY, EBAY_ITEM_ID, LIST_PRICE, REMARKS, SINGLE_ENTRY_ID, SALVAGE_QTY, ENTRY_TYPE, LZ_SELLER_ACCT_ID, SEED_ID, STATUS, ITEM_CONDITION, FORCEREVISE)VALUES (" . $LIST_ID . "," . $manifest_id . ", " . $LIST_ID . ", " . $item_id . ", " . $list_date . ", " . $lister_id . ", '" . $ebay_item_desc . "', " . $list_qty . "," . $ebay_item_id . ",'" . $list_price . "',NULL,NULL, NULL, '" . $entry_type . "'," . $LZ_SELLER_ACCT_ID . "," . $seed_id . ",'" . trim($status) . "'," . $condition_id . "," . @$forceRevise . ")");
        if ($insert_query) {
            $update_barcode_qry = "UPDATE LZ_BARCODE_MT SET LIST_ID = $LIST_ID WHERE ITEM_ID= $item_id AND LZ_MANIFEST_ID = $manifest_id AND CONDITION_ID = $condition_id AND EBAY_ITEM_ID IS NOT NULL AND LIST_ID IS NOT NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND HOLD_STATUS = 0";
            $this->db->query($update_barcode_qry);
            $this->db->query("UPDATE LZ_LISTING_ALLOC SET LIST_ID = $LIST_ID WHERE SEED_ID = $seed_id");
            $this->db->query("UPDATE lz_item_seed SET EBAY_PRICE = '$price' WHERE SEED_ID = '$seed_id'");
            return $LIST_ID;
        }

    }

    public function pic_log()
    {
        $user_id = $this->uri->segment(4);

        $data = $this->db->query("SELECT BARCODE_NO , NO_OF_PIC , TO_CHAR(PIC_DATE, 'DD/MM/YYYY HH24:MI:SS') PIC_DATE FROM LZ_PIC_LOG  WHERE PIC_DATE >= TO_DATE(SYSDATE, 'DD-MON-YY') AND PIC_BY = '$user_id'  ORDER BY PIC_DATE DESC ")->result_array();
        return $data;
    }

    public function merch_servic_invoice()
    {

        $merchant_id = $this->input->post('merchant_id');
        //var_dump($merchant_id);
        // if(!empty($merchant_id)){
        //   $
        // }

        $mer_inv = $this->db->query("SELECT DECODE(MAX(SER_NAME), NULL, '-', MAX(SER_NAME)) SERVICE_NAME, MAX(SERV_TYPE) SER_TYPE, CASE WHEN COUNT(EBAY_ITEM_ID) > 0 THEN ROUND(NVL(SUM(CHARGES) / COUNT(TO_CHAR(EBAY_ITEM_ID)), 0), 4) ELSE NVL(SUM(CHARGES) / NULL, 0) END RATE, COUNT(EBAY_ITEM_ID) TOTAL_COUNT, NVL(SUM(CHARGES), 0) TOTAL_CAHRGES, MAX(SER_RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, '-' DURATION FROM (SELECT D.BARCODE_NO, L.BARCODE_PRV_NO, L.PIC_DATE_TIME, BINV.CHARGES, L.LZ_MANIFEST_DET_ID, B.ITEM_ID, B.LZ_MANIFEST_ID, RA.SER_RATE_ID, M.MERCHANT_ID, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO) TOTAL_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL) LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NULL) NOT_LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL AND B.SALE_RECORD_NO IS NOT NULL) SOLD_VALUE, (SELECT SB.SERVICE_DESC FROM LJ_SERVICES SB WHERE SB.SERVICE_ID = RA.SERVICE_ID) SER_NAME, DECODE(RA.SERVICE_TYPE, 1, 'PER_BARC', 2, 'PER_HOUR') SERV_TYPE, B.EBAY_ITEM_ID, B.SALE_RECORD_NO FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LZ_BARCODE_MT          B, LJ_BARCODE_BILLING     BINV, LJ_SERVICE_RATE        RA WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.INVOICE_ID IS NULL AND B.EBAY_ITEM_ID IS NOT NULL AND BINV.SER_RATE_ID = 2 AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND M.MERCHANT_ID = $merchant_id) UNION ALL SELECT DECODE(MAX(SER_NAME), NULL, '-', MAX(SER_NAME)) SERVICE_NAME, MAX(SERV_TYPE) SER_TYPE, ROUND(SUM(CHRG) / COUNT(BARCODE_PRV_NO), 4) RATE, COUNT(FOLDER_NAME) TOTAL_COUNT, (ROUND(SUM(CHRG) / COUNT(BARCODE_PRV_NO), 4)) * COUNT(FOLDER_NAME) TOTAL_CAHRGES, MAX(SER_RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, '-' DURATION FROM (SELECT L.FOLDER_NAME, L.BARCODE_PRV_NO, ROUND(BINV.CHARGES, 2) CHRG, DECODE(RA.SERVICE_TYPE, 1, 'PER_BARC', 2, 'PER_HOUR') SERV_TYPE, RA.SER_RATE_ID, M.MERCHANT_ID, SR.SERVICE_DESC SER_NAME FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LJ_BARCODE_BILLING     BINV, LJ_SERVICE_RATE        RA, LJ_SERVICES            SR, LZ_BARCODE_MT          B WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.SER_RATE_ID = 1 AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND BINV.INVOICE_ID IS NULL AND RA.SERVICE_ID = SR.SERVICE_ID AND M.MERCHANT_ID = $merchant_id AND BINV.INVOICE_ID IS NULL) UNION ALL SELECT MAX(SERVICE_NAME) SERVICE_NAME, MAX(SER_TYPE) SER_TYPE, ROUND(SUM(CHARGES) / SUM(TOTAL_COUNT), 4) RATE, SUM(TOTAL_COUNT) TOTAL_COUNT, SUM(CHARGES) TOTAL_CAHRGES, MAX(RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, '-' DURATION FROM (SELECT MAX(SV.SERVICE_DESC) SERVICE_NAME, DECODE(MAX(RA.SERVICE_TYPE), 1, 'PER_BARC', 2, 'PER_HOUR', 3, 'PER_ORDER') SER_TYPE, NVL(MAX(M.SHIPING_LABEL_RATE), 0) SHIPING_RATE, SUM(PM.PACKING_COST) PACKING_COST,(SUM(nvl(dt.PACKING_COST, 0)) + MAX(nvl(m.SERVICE_COST, 0)) + NVL(MAX(M.SHIPING_LABEL_RATE), 0) + NVL(MAX(M.EBAY_FEE), 0) )+ round(0.07 * (MAX(M.SALE_PRICE) * max(M.QTY)),2) CHARGES, COUNT(DISTINCT M.ORDER_ID) TOTAL_COUNT, MAX(M.SER_RATE_ID) RATE_ID, MAX(M.MERCHANT_ID) MERCHANT_ID FROM LJ_ORDER_PACKING_MT M, LJ_ORDER_PACKING_DT DT, LZ_PACKING_TYPE_MT  PM, lj_service_rate     ra, lj_services         sv WHERE M.ORDER_PACKING_ID = DT.ORDER_PACKING_ID and m.ser_rate_id = ra.ser_rate_id(+) and ra.service_id = sv.service_id(+) AND DT.PACKING_ID = PM.PACKING_ID(+) AND M.MERCHANT_ID = $merchant_id AND M.INVOICE_ID IS NULL GROUP BY dt.order_packing_id) /*UNION ALL SELECT DECODE(MAX(SER_NAME), NULL, '-', MAX(SER_NAME)) SERVICE_NAME, MAX(SERV_TYPE) SER_TYPE, CASE WHEN COUNT(ITEM_ID) > 0 THEN ROUND(NVL(SUM(CHARGES) / COUNT(TO_CHAR(ITEM_ID)), 0), 4) ELSE NVL(SUM(CHARGES) / NULL, 0) END RATE, COUNT(ITEM_ID) TOTAL_COUNT, NVL(SUM(CHARGES), 0) TOTAL_CAHRGES, MAX(SER_RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, '-' DURATION FROM (SELECT D.BARCODE_NO, L.BARCODE_PRV_NO, L.PIC_DATE_TIME, BINV.CHARGES, L.LZ_MANIFEST_DET_ID, B.ITEM_ID, B.LZ_MANIFEST_ID, RA.SER_RATE_ID, M.MERCHANT_ID, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO) TOTAL_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL) LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NULL) NOT_LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL AND B.SALE_RECORD_NO IS NOT NULL) SOLD_VALUE, (SELECT SB.SERVICE_DESC FROM LJ_SERVICES SB WHERE SB.SERVICE_ID = RA.SERVICE_ID) SER_NAME, DECODE(RA.SERVICE_TYPE, 1, 'PER_BARC', 2, 'PER_HOUR') SERV_TYPE, B.EBAY_ITEM_ID, B.SALE_RECORD_NO FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LZ_BARCODE_MT          B, LJ_BARCODE_BILLING     BINV, LJ_SERVICE_RATE        RA WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.INVOICE_ID IS NULL AND BINV.SER_RATE_ID = 6 AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND M.MERCHANT_ID = $merchant_id)*/ UNION ALL SELECT MAX(SERVICE_DESC) SERVICE_NAME, MAX(SERV_TYPE) SER_TYPE, MAX(CHARGES) RATE, count(barcode_no) TOTAL_COUNT, NVL(SUM(TOT_CHARGE), 0) TOTAL_CAHRGES, MAX(SER_RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, TO_CHAR(TRUNC(sum(get_sec) / 3600), 'FM9900') || ' hrs: ' || TO_CHAR(TRUNC(MOD(sum(get_sec), 3600) / 60), 'FM00') || ' min: ' || TO_CHAR(MOD(sum(get_sec), 60), 'FM00') || ' sec' Duration FROM (SELECT SR.CHARGES, SV.SERVICE_DESC, DECODE(SR.SERVICE_TYPE, 1, 'PER_BARC', 2, 'PER_HOUR') SERV_TYPE, SR.SERVICE_ID, (SR.CHARGES / 60) / 60 * TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 60 * 60) TOT_CHARGE, TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 60 * 60) get_sec, SR.SER_RATE_ID, MA.MERCHANT_ID, MA.APPOINTMENT_ID, lg.barcode_no FROM LJ_APPOINTMENT_MT  MA, LJ_APPOINTMENT_DT  DA, LJ_SERVICE_RATE    SR, LJ_SERVICES        SV, LJ_APPOINTMENT_LOG LG WHERE MA.APPOINTMENT_ID = DA.APPOINTMENT_ID AND DA.APPOINTMENT_DT_ID = LG.APPOINTMENT_DT_ID(+) AND LG.INV_ID IS NULL AND DA.SERVICE_ID = SV.SERVICE_ID AND SV.SERVICE_ID = SR.SERVICE_ID AND MA.MERCHANT_ID = $merchant_id ORDER BY LG.APPOINTMENT_LOG_ID ASC) GROUP BY SERVICE_ID, SERVICE_DESC ")->result_array();
        if (count($mer_inv) >= 1) {
            return array('mer_inv' => $mer_inv, 'exist' => true);
        } else {
            return array('mer_inv' => $mer_inv, 'exist' => false);
        }

    }

    public function merch_servic_invoice_barcode()
    {

        $merchant_id = $this->input->post('merchant_id');
        $rate_id = $this->input->post('rate_id');
        // var_dump($merchant_id);
        // var_dump($rate_id );
        //echo $servRateId;

        if ($rate_id == 2) {

            $mer_inv_barcode = $this->db->query("SELECT ROWNUM PK, B.EBAY_ITEM_ID, 'NULL' DURATION,'null' ORDER_ID,'null' SHIPING_RATE,'null' MARKTPLACE,'null' PACKING_COST,'null' SALES_RECORD_NUMBER,'null' eba_fee,'null' QTY,B.BARCODE_NO, SD.ITEM_TITLE, SD.EBAY_PRICE, CO.COND_NAME DEFAULT_COND, L.BARCODE_PRV_NO, L.PIC_DATE_TIME, BINV.CHARGES, L.LZ_MANIFEST_DET_ID, B.ITEM_ID, B.LZ_MANIFEST_ID, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO) TOTAL_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL) LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NULL) NOT_LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL AND B.SALE_RECORD_NO IS NOT NULL) SOLD_VALUE, (SELECT SB.SERVICE_DESC FROM LJ_SERVICES SB WHERE SB.SERVICE_ID = RA.SERVICE_ID) SER_NAME, DECODE(RA.SERVICE_TYPE, 1, 'PER_BARCODE', 2, 'PER_HOUR') SERV_TYPE, RA.SER_RATE_ID, B.EBAY_ITEM_ID, B.SALE_RECORD_NO FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS L, LZ_BARCODE_MT B, LJ_BARCODE_BILLING  BINV, LJ_SERVICE_RATE   RA, LZ_ITEM_SEED   SD,LZ_ITEM_COND_MT CO WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.SER_RATE_ID = $rate_id AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND BINV.INVOICE_ID IS  NULL AND B.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID (+)AND B.ITEM_ID = SD.ITEM_ID(+) AND B.CONDITION_ID = SD.DEFAULT_COND(+) AND SD.DEFAULT_COND = CO.ID(+) AND B.EBAY_ITEM_ID IS NOT NULL AND M.MERCHANT_ID = $merchant_id ")->result_array();

        } elseif ($rate_id == 6) {

            $mer_inv_barcode = $this->db->query("SELECT ROWNUM PK, B.EBAY_ITEM_ID, 'NULL' DURATION,'null' ORDER_ID,'null' SHIPING_RATE,'null' MARKTPLACE,'null' PACKING_COST,'null' SALES_RECORD_NUMBER,'null' eba_fee,'null' QTY,B.BARCODE_NO, SD.ITEM_TITLE, SD.EBAY_PRICE, CO.COND_NAME DEFAULT_COND, L.BARCODE_PRV_NO, L.PIC_DATE_TIME, BINV.CHARGES, L.LZ_MANIFEST_DET_ID, B.ITEM_ID, B.LZ_MANIFEST_ID, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO) TOTAL_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL) LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NULL) NOT_LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL AND B.SALE_RECORD_NO IS NOT NULL) SOLD_VALUE, (SELECT SB.SERVICE_DESC FROM LJ_SERVICES SB WHERE SB.SERVICE_ID = RA.SERVICE_ID) SER_NAME, DECODE(RA.SERVICE_TYPE, 1, 'PER_BARCODE', 2, 'PER_HOUR') SERV_TYPE, RA.SER_RATE_ID, B.EBAY_ITEM_ID, B.SALE_RECORD_NO FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS L, LZ_BARCODE_MT B, LJ_BARCODE_BILLING  BINV, LJ_SERVICE_RATE   RA, LZ_ITEM_SEED   SD,LZ_ITEM_COND_MT CO WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.SER_RATE_ID = $rate_id AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND BINV.INVOICE_ID IS  NULL AND B.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID (+)AND B.ITEM_ID = SD.ITEM_ID(+) AND B.CONDITION_ID = SD.DEFAULT_COND(+) AND SD.DEFAULT_COND = CO.ID(+) /*AND B.EBAY_ITEM_ID IS NOT NULL*/ AND M.MERCHANT_ID = $merchant_id ")->result_array();

        } elseif ($rate_id == 3) {

            $mer_inv_barcode = $this->db->query(" SELECT lg.BARCODE_NO, 'NULL' ORDER_ID, decode(BB.EBAY_ITEM_ID,null,'Not Listed',BB.EBAY_ITEM_ID) EBAY_ITEM_ID, decode(S.ITEM_TITLE,null,'Not Posted',S.ITEM_TITLE) ITEM_TITLE, S.EBAY_PRICE, 'NULL' EBA_FEE, 'NULL' SHIPING_RATE, 'NULL' MARKTPLACE, 'NULL' PACKING_COST, 'NULL' SALES_RECORD_NUMBER, 'NULL' QTY, 'NULL' SALES_RECORD_NUMBER, CM.COND_NAME DEFAULT_COND, SR.SERVICE_ID, /*TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24)) DIFF_IN_HOUR, TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 60) DIFF_IN_MIN, TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 60 * 60) DIFF_IN_SECNDS,*/ TO_CHAR(TRUNC(((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60) / 60) - 24 * (TRUNC((((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60) / 60) / 24))) || ' H:' || TO_CHAR(TRUNC((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60) - 60 * (TRUNC(((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60) / 60))) || ' M: ' || TO_CHAR(TRUNC(86400 * (LG.STOP_TIME - LG.START_TIME)) - 60 * (TRUNC((86400 * (LG.STOP_TIME - LG.START_TIME)) / 60))) || ' S' DURATION, ROUND((SR.CHARGES / 60) / 60 * TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 60 * 60), 2) CHARGES, SR.SER_RATE_ID, MA.MERCHANT_ID, MA.APPOINTMENT_ID, LG.BARCODE_NO, LG.INV_ID FROM LJ_APPOINTMENT_MT  MA, LJ_APPOINTMENT_DT  DA, LJ_SERVICE_RATE    SR, LJ_SERVICES        SV, LJ_APPOINTMENT_LOG LG, LZ_BARCODE_MT      BB, LZ_ITEM_SEED       S, LZ_ITEM_COND_MT    CM WHERE MA.APPOINTMENT_ID = DA.APPOINTMENT_ID AND DA.APPOINTMENT_DT_ID = LG.APPOINTMENT_DT_ID(+) AND DA.SERVICE_ID = SV.SERVICE_ID AND SV.SERVICE_ID = SR.SERVICE_ID AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND(+) AND LG.BARCODE_NO = BB.BARCODE_NO(+) AND S.DEFAULT_COND = CM.ID(+)  and lg.barcode_no is not null AND MA.MERCHANT_ID = $merchant_id AND LG.INV_ID IS NULL ORDER BY LG.APPOINTMENT_LOG_ID ASC ")->result_array();

        } elseif ($rate_id == 5) {

            $mer_inv_barcode = $this->db->query("SELECT M.ORDER_ID BARCODE_NO, 'NULL' DURATION, 'NULL' ORDER_ID, MAX(M.EBAY_ID) EBAY_ITEM_ID, MAX(M.ITEM_TITLE) ITEM_TITLE, NVL(MAX(M.SHIPING_LABEL_RATE), 0) SHIPING_RATE, SUM(PM.PACKING_COST) +max(nvl(m.SERVICE_COST,0)) PACKING_COST,(SUM(nvl(dt.PACKING_COST, 0)) + MAX(nvl(m.SERVICE_COST, 0)) + NVL(MAX(M.SHIPING_LABEL_RATE), 0) + NVL(MAX(M.EBAY_FEE), 0) )+ round(0.07 * (MAX(M.SALE_PRICE) * max(M.QTY)),2) CHARGES, round(0.07 * (MAX(M.SALE_PRICE) * max(M.QTY)),2) MARKTPLACE, MAX(M.EBAY_FEE) EBA_FEE, '' DEFAULT_COND, NVL(MAX(M.SALE_PRICE * M.QTY), 0) EBAY_PRICE, MAX(M.QTY) QTY, MAX(M.SALE_RECRD_NUM) SALES_RECORD_NUMBER FROM LJ_ORDER_PACKING_MT M, LJ_ORDER_PACKING_DT DT, LZ_PACKING_TYPE_MT PM WHERE M.ORDER_PACKING_ID = DT.ORDER_PACKING_ID AND DT.PACKING_ID = PM.PACKING_ID(+) AND M.MERCHANT_ID = $merchant_id AND M.INVOICE_ID IS NULL GROUP BY M.ORDER_ID ")->result_array();
        } elseif ($rate_id == 1) {

            $mer_inv_barcode = $this->db->query("SELECT B.EBAY_ITEM_ID   EBAY_ITEM_ID,'NULL' DURATION, 'null' ORDER_ID,'null' MARKTPLACE,'null' SHIPING_RATE,'null' SALES_RECORD_NUMBER,'null' PACKING_COST,'null' eba_fee,'null' QTY,L.FOLDER_NAME, L.BARCODE_PRV_NO BARCODE_NO, SD.ITEM_TITLE    ITEM_TITLE, SD.EBAY_PRICE    EBAY_PRICE, CO.COND_NAME     DEFAULT_COND, BINV.CHARGES     CHARGES FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LJ_BARCODE_BILLING     BINV, LJ_SERVICE_RATE        RA, LJ_SERVICES            SR, LZ_BARCODE_MT          B, LZ_ITEM_SEED           SD, LZ_ITEM_COND_MT        CO WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.SER_RATE_ID = 1 AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND BINV.INVOICE_ID IS NULL AND RA.SERVICE_ID = SR.SERVICE_ID AND B.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID(+) AND B.ITEM_ID = SD.ITEM_ID(+) AND B.CONDITION_ID = SD.DEFAULT_COND(+) AND SD.DEFAULT_COND = CO.ID(+) AND M.MERCHANT_ID = $merchant_id order by b.EBAY_ITEM_ID asc")->result_array();

        } elseif ($rate_id == 7) {

            $mer_inv_barcode = $this->db->query("SELECT B.EBAY_ITEM_ID ,'NULL' DURATION,EBAY_ITEM_ID,S.ORDER_ID,'null' MARKTPLACE,'null' SHIPING_RATE,'null' SALES_RECORD_NUMBER,'null' QTY,'null' eba_fee,'null' PACKING_COST, L.BARCODE_PRV_NO BARCODE_NO, SD.ITEM_TITLE    ITEM_TITLE, SD.EBAY_PRICE    EBAY_PRICE, CO.COND_NAME     DEFAULT_COND, BINV.CHARGES     CHARGES FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LJ_BARCODE_BILLING     BINV, LJ_SERVICE_RATE        RA, LJ_SERVICES            SR, LZ_BARCODE_MT          B, LZ_ITEM_SEED           SD, LZ_ITEM_COND_MT        CO, LZ_SALESLOAD_DET       S WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.SER_RATE_ID = 7 AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND BINV.INVOICE_ID IS NULL AND RA.SERVICE_ID = SR.SERVICE_ID AND B.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID(+) AND B.ITEM_ID = SD.ITEM_ID(+) AND B.CONDITION_ID = SD.DEFAULT_COND(+) AND SD.DEFAULT_COND = CO.ID(+) AND B.ORDER_ID = S.ORDER_ID /*AND B.SALE_RECORD_NO = S.SALES_RECORD_NUMBER */AND S.ORDER_ID IS NOT NULL AND M.MERCHANT_ID = $merchant_id ORDER BY BINV.CHARGES desc ")->result_array();
        }

        if (count($mer_inv_barcode) >= 1) {
            return array('mer_inv_barcode' => $mer_inv_barcode, 'exist' => true);
        } else {
            return array('mer_inv_barcode' => $mer_inv_barcode, 'exist' => false);
        }

    }

    public function generate_invoic()
    {

        $ids = $this->input->post('ids');
        // var_dump($ids);
        // return $ids;
        $serRateId = $this->input->post('serRateId');
        $merchantId = $this->input->post('merchantId');

        $chek_temp_data = $this->db->query("SELECT T.INV_MT_ID_TEMP FROM LJ_INOVICE_MT_TEMP T WHERE T.SER_RATE_ID = $serRateId AND T.MERCHANT_ID = $merchantId ")->result_array();

        if (count($chek_temp_data) > 0) {
            $get_temp_mt_id = $chek_temp_data[0]['INV_MT_ID_TEMP'];

            $delte_from_det = $this->db->query("DELETE FROM LJ_INOVICE_DT_TEMP WHERE INV_MT_ID_TEMP = $get_temp_mt_id ");
            if ($delte_from_det) {
                $this->db->query("DELETE FROM LJ_INOVICE_MT_TEMP WHERE INV_MT_ID_TEMP = $get_temp_mt_id ");
                $get_pk = $this->db->query(" SELECT GET_SINGLE_PRIMARY_KEY('LJ_INOVICE_MT_TEMP','INV_MT_ID_TEMP') INV_MT_ID_TEMP FROM DUAL")->result_array();
                $get_pk = $get_pk[0]['INV_MT_ID_TEMP'];

                $inser_mt = $this->db->query(" INSERT INTO LJ_INOVICE_MT_TEMP (INV_MT_ID_TEMP, SER_RATE_ID, MERCHANT_ID, CREATED_DATE, CREATED_BY) VALUES ($get_pk,$serRateId,$merchantId, SYSDATE, 2)");

                if ($inser_mt) {
                    $i = 0;
                    foreach ($ids as $barocdes) {

                        $qry = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_INOVICE_DT_TEMP', 'INV_DET_ID_TEMP')INV_DET_ID_TEMP FROM DUAL")->result_array();
                        $lz_dekit_us_dt_id = $qry[0]['INV_DET_ID_TEMP'];

                        $insert_est_det = $this->db->query(" INSERT INTO LJ_INOVICE_DT_TEMP (INV_DET_ID_TEMP, INV_MT_ID_TEMP, TEMP_PARAMETER) VALUES ($lz_dekit_us_dt_id, $get_pk, '$barocdes' )");

                        $i++;
                    } /// end foreach
                    if ($insert_est_det) {
                        return true;
                    }

                }
            }

        } else {

            $get_pk = $this->db->query(" SELECT GET_SINGLE_PRIMARY_KEY('LJ_INOVICE_MT_TEMP','INV_MT_ID_TEMP') INV_MT_ID_TEMP FROM DUAL")->result_array();
            $get_pk = $get_pk[0]['INV_MT_ID_TEMP'];

            $inser_mt = $this->db->query(" INSERT INTO LJ_INOVICE_MT_TEMP (INV_MT_ID_TEMP, SER_RATE_ID, MERCHANT_ID, CREATED_DATE, CREATED_BY) VALUES ($get_pk,$serRateId,$merchantId, SYSDATE, 2)");

            if ($inser_mt) {
                $i = 0;
                foreach ($ids as $barocdes) {

                    $qry = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_INOVICE_DT_TEMP', 'INV_DET_ID_TEMP')INV_DET_ID_TEMP FROM DUAL")->result_array();
                    $lz_dekit_us_dt_id = $qry[0]['INV_DET_ID_TEMP'];

                    $insert_est_det = $this->db->query(" INSERT INTO LJ_INOVICE_DT_TEMP (INV_DET_ID_TEMP, INV_MT_ID_TEMP, TEMP_PARAMETER) VALUES ($lz_dekit_us_dt_id, $get_pk, '$barocdes' )");

                    $i++;
                } /// end foreach
                if ($insert_est_det) {
                    return true;
                }
            }

        }

    }

    public function load_merchant()
    {

        $load_merch = $this->db->query("SELECT M.MERCHANT_ID,M.BUISNESS_NAME FROM LZ_MERCHANT_MT M ORDER BY M.MERCHANT_ID DESC")->result_array();
        return array('load_merch' => $load_merch);
    }
    public function merch_generate_invoice()
    {

        $serId = $this->input->post('serId');
        $merchant_id = $this->input->post('merchant_id');

        $invoice_query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_INVOICE_MT', 'INVOICE_ID') INVID FROM DUAL")->result_array();
        $invoice_id = $invoice_query[0]['INVID'];

        $inser_invoice = $this->db->query("INSERT INTO LJ_INVOICE_MT (INVOICE_ID, MERCHANT_ID, CREATED_DATE, INVOICE_NO, DUE_DATE, REMARKS) VALUES ($invoice_id, $merchant_id, SYSDATE, $invoice_id, SYSDATE, NULL)");

        if ($inser_invoice) {

            $i = 0;
            foreach ($serId as $serviceRateId) {

                $query = $this->db->query("call  pro_generate_inovice($serviceRateId,$merchant_id,$invoice_id)");

                $i++;
            }

            // $query = $this->db->query("");

            if ($query) {
                $mer_inv = $this->db->query("SELECT DECODE(MAX(SER_NAME), NULL, '-', MAX(SER_NAME)) SERVICE_NAME, MAX(SERV_TYPE) SER_TYPE, CASE WHEN COUNT(EBAY_ITEM_ID) > 0 THEN ROUND(NVL(SUM(CHARGES) / COUNT(TO_CHAR(EBAY_ITEM_ID)), 0), 4) ELSE NVL(SUM(CHARGES) / NULL, 0) END RATE, COUNT(EBAY_ITEM_ID) TOTAL_COUNT, NVL(SUM(CHARGES), 0) TOTAL_CAHRGES, MAX(SER_RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, '-' DURATION FROM (SELECT D.BARCODE_NO, L.BARCODE_PRV_NO, L.PIC_DATE_TIME, BINV.CHARGES, L.LZ_MANIFEST_DET_ID, B.ITEM_ID, B.LZ_MANIFEST_ID, RA.SER_RATE_ID, M.MERCHANT_ID, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO) TOTAL_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL) LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NULL) NOT_LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL AND B.SALE_RECORD_NO IS NOT NULL) SOLD_VALUE, (SELECT SB.SERVICE_DESC FROM LJ_SERVICES SB WHERE SB.SERVICE_ID = RA.SERVICE_ID) SER_NAME, DECODE(RA.SERVICE_TYPE, 1, 'PER_BARC', 2, 'PER_HOUR') SERV_TYPE, B.EBAY_ITEM_ID, B.SALE_RECORD_NO FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LZ_BARCODE_MT          B, LJ_BARCODE_BILLING     BINV, LJ_SERVICE_RATE        RA WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.INVOICE_ID IS NULL AND B.EBAY_ITEM_ID IS NOT NULL AND BINV.SER_RATE_ID = 2 AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND M.MERCHANT_ID = $merchant_id) UNION ALL SELECT DECODE(MAX(SER_NAME), NULL, '-', MAX(SER_NAME)) SERVICE_NAME, MAX(SERV_TYPE) SER_TYPE, ROUND(SUM(CHRG) / COUNT(BARCODE_PRV_NO), 4) RATE, COUNT(FOLDER_NAME) TOTAL_COUNT, (ROUND(SUM(CHRG) / COUNT(BARCODE_PRV_NO), 4)) * COUNT(FOLDER_NAME) TOTAL_CAHRGES, MAX(SER_RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, '-' DURATION FROM (SELECT L.FOLDER_NAME, L.BARCODE_PRV_NO, ROUND(BINV.CHARGES, 2) CHRG, DECODE(RA.SERVICE_TYPE, 1, 'PER_BARC', 2, 'PER_HOUR') SERV_TYPE, RA.SER_RATE_ID, M.MERCHANT_ID, SR.SERVICE_DESC SER_NAME FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LJ_BARCODE_BILLING     BINV, LJ_SERVICE_RATE        RA, LJ_SERVICES            SR, LZ_BARCODE_MT          B WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.SER_RATE_ID = 1 AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND BINV.INVOICE_ID IS NULL AND RA.SERVICE_ID = SR.SERVICE_ID AND M.MERCHANT_ID = $merchant_id AND BINV.INVOICE_ID IS NULL) UNION ALL SELECT MAX(SERVICE_NAME) SERVICE_NAME, MAX(SER_TYPE) SER_TYPE, ROUND(SUM(CHARGES) / SUM(TOTAL_COUNT), 4) RATE, SUM(TOTAL_COUNT) TOTAL_COUNT, SUM(CHARGES) TOTAL_CAHRGES, MAX(RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, '-' DURATION FROM (SELECT MAX(SV.SERVICE_DESC) SERVICE_NAME, DECODE(MAX(RA.SERVICE_TYPE), 1, 'PER_BARC', 2, 'PER_HOUR', 3, 'PER_ORDER') SER_TYPE, NVL(MAX(M.SHIPING_LABEL_RATE), 0) SHIPING_RATE, SUM(PM.PACKING_COST) PACKING_COST,(SUM(nvl(dt.PACKING_COST, 0)) + MAX(nvl(m.SERVICE_COST, 0)) + NVL(MAX(M.SHIPING_LABEL_RATE), 0) + NVL(MAX(M.EBAY_FEE), 0) )+ round(0.07 * (MAX(M.SALE_PRICE) * max(M.QTY)),2) CHARGES, COUNT(DISTINCT M.ORDER_ID) TOTAL_COUNT, MAX(M.SER_RATE_ID) RATE_ID, MAX(M.MERCHANT_ID) MERCHANT_ID FROM LJ_ORDER_PACKING_MT M, LJ_ORDER_PACKING_DT DT, LZ_PACKING_TYPE_MT  PM, lj_service_rate     ra, lj_services         sv WHERE M.ORDER_PACKING_ID = DT.ORDER_PACKING_ID and m.ser_rate_id = ra.ser_rate_id(+) and ra.service_id = sv.service_id(+) AND DT.PACKING_ID = PM.PACKING_ID(+) AND M.MERCHANT_ID = $merchant_id AND M.INVOICE_ID IS NULL GROUP BY dt.order_packing_id) /*UNION ALL SELECT DECODE(MAX(SER_NAME), NULL, '-', MAX(SER_NAME)) SERVICE_NAME, MAX(SERV_TYPE) SER_TYPE, CASE WHEN COUNT(ITEM_ID) > 0 THEN ROUND(NVL(SUM(CHARGES) / COUNT(TO_CHAR(ITEM_ID)), 0), 4) ELSE NVL(SUM(CHARGES) / NULL, 0) END RATE, COUNT(ITEM_ID) TOTAL_COUNT, NVL(SUM(CHARGES), 0) TOTAL_CAHRGES, MAX(SER_RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, '-' DURATION FROM (SELECT D.BARCODE_NO, L.BARCODE_PRV_NO, L.PIC_DATE_TIME, BINV.CHARGES, L.LZ_MANIFEST_DET_ID, B.ITEM_ID, B.LZ_MANIFEST_ID, RA.SER_RATE_ID, M.MERCHANT_ID, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO) TOTAL_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL) LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NULL) NOT_LIST_VAL, (SELECT SD.EBAY_PRICE FROM LZ_BARCODE_MT MM, LZ_ITEM_SEED SD WHERE MM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID AND MM.ITEM_ID = SD.ITEM_ID AND MM.CONDITION_ID = SD.DEFAULT_COND AND MM.BARCODE_NO = B.BARCODE_NO AND B.EBAY_ITEM_ID IS NOT NULL AND B.SALE_RECORD_NO IS NOT NULL) SOLD_VALUE, (SELECT SB.SERVICE_DESC FROM LJ_SERVICES SB WHERE SB.SERVICE_ID = RA.SERVICE_ID) SER_NAME, DECODE(RA.SERVICE_TYPE, 1, 'PER_BARC', 2, 'PER_HOUR') SERV_TYPE, B.EBAY_ITEM_ID, B.SALE_RECORD_NO FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LZ_SPECIAL_LOTS        L, LZ_BARCODE_MT          B, LJ_BARCODE_BILLING     BINV, LJ_SERVICE_RATE        RA WHERE M.MT_ID = D.MT_ID AND D.BARCODE_NO = L.BARCODE_PRV_NO(+) AND L.BARCODE_PRV_NO = B.BARCODE_NO(+) AND L.BARCODE_PRV_NO = BINV.BARCODE_NO(+) AND BINV.INVOICE_ID IS NULL AND BINV.SER_RATE_ID = 6 AND BINV.SER_RATE_ID = RA.SER_RATE_ID(+) AND M.MERCHANT_ID = $merchant_id)*/ UNION ALL SELECT MAX(SERVICE_DESC) SERVICE_NAME, MAX(SERV_TYPE) SER_TYPE, MAX(CHARGES) RATE, count(barcode_no) TOTAL_COUNT, NVL(SUM(TOT_CHARGE), 0) TOTAL_CAHRGES, MAX(SER_RATE_ID) RATE_ID, MAX(MERCHANT_ID) MERCHANT_ID, TO_CHAR(TRUNC(sum(get_sec) / 3600), 'FM9900') || ' hrs: ' || TO_CHAR(TRUNC(MOD(sum(get_sec), 3600) / 60), 'FM00') || ' min: ' || TO_CHAR(MOD(sum(get_sec), 60), 'FM00') || ' sec' Duration FROM (SELECT SR.CHARGES, SV.SERVICE_DESC, DECODE(SR.SERVICE_TYPE, 1, 'PER_BARC', 2, 'PER_HOUR') SERV_TYPE, SR.SERVICE_ID, (SR.CHARGES / 60) / 60 * TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 60 * 60) TOT_CHARGE, TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 60 * 60) get_sec, SR.SER_RATE_ID, MA.MERCHANT_ID, MA.APPOINTMENT_ID, lg.barcode_no FROM LJ_APPOINTMENT_MT  MA, LJ_APPOINTMENT_DT  DA, LJ_SERVICE_RATE    SR, LJ_SERVICES        SV, LJ_APPOINTMENT_LOG LG WHERE MA.APPOINTMENT_ID = DA.APPOINTMENT_ID AND DA.APPOINTMENT_DT_ID = LG.APPOINTMENT_DT_ID(+) AND LG.INV_ID IS NULL AND DA.SERVICE_ID = SV.SERVICE_ID AND SV.SERVICE_ID = SR.SERVICE_ID AND MA.MERCHANT_ID = $merchant_id ORDER BY LG.APPOINTMENT_LOG_ID ASC) GROUP BY SERVICE_ID, SERVICE_DESC ")->result_array();

                if (count($mer_inv) >= 1) {return array('mer_inv' => $mer_inv, 'exist' => true);
                } else {
                    return array('mer_inv' => $mer_inv, 'exist' => false);
                }
            } else {
                return false;
            }
        }
    }

    public function load_identification_data()
    {

        $user_id = $this->input->post('user_id');
        $filterData = trim($this->input->post('filterData'));
        //var_dump($filterData);

        //$date_range = $this->input->post('date_range');
        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");

        // var_dump($date_range);
        // var_dump($from);
        // var_dump($to);
        // exit;
        $idntiti_data = "SELECT * FROM (SELECT 'Dekit' DEKIT_ITEM, DC.BRAND, D.LZ_DEKIT_US_DT_ID PK_ID, D.BARCODE_PRV_NO, DECODE(DC.MPN_DESCRIPTION, NULL, D.MPN_DESCRIPTION, DC.MPN_DESCRIPTION) MPN_DESCRIPTION, DC.MPN MPN, DC.UPC, O.OBJECT_NAME, C.COND_NAME, D.AVG_SELL_PRICE, D.DEKIT_REMARKS /*D.IDENT_REMARKS*/ REMARKS, d.allocate_to assign_to, D.discard, BAR.EBAY_ITEM_ID,D.LZ_MANIFEST_DET_ID GET_MANIF_ID,to_char(M.BARCODE_NO)  MAS_BAR,/* M.MASTER_MPN_ID, K.CATALOGUE_MT_ID,  K.MPN_DESCRIPTION,*/ I.ITEM_DESC   mast_mpn_desc,  D.FOLDER_NAME FOLDER_NAME, '-' merchant_name,'DFW' ACCOUNT_NAME , 'BIN-' || MB.BIN_ID BIN_ID   FROM LZ_DEKIT_US_DT   D,LZ_BARCODE_MT BAR, LZ_DEKIT_US_MT   M, LZ_BD_OBJECTS_MT O, LZ_ITEM_COND_MT  C, LZ_CATALOGUE_MT  K, LZ_BARCODE_MT MB, ITEMS_MT I, LZ_CATALOGUE_MT  DC WHERE D.OBJECT_ID = O.OBJECT_ID(+) AND D.CONDITION_ID = C.ID(+)  AND D.BARCODE_PRV_NO = BAR.BARCODE_NO(+) AND M.MASTER_MPN_ID = K.CATALOGUE_MT_ID(+) AND D.LZ_DEKIT_US_MT_ID = M.LZ_DEKIT_US_MT_ID  AND D.PIC_DATE_TIME IS NOT NULL AND D.PIC_BY IS NOT NULL AND D.CATALOG_MT_ID = DC.CATALOGUE_MT_ID(+)   AND M.LZ_MANIFEST_MT_ID IS NOT NULL  AND M.BARCODE_NO = MB.BARCODE_NO AND MB.ITEM_ID = I.ITEM_ID AND D.ALLOCATE_TO = '$user_id'";
        $idntiti_data .= "AND D.ALLOCATE_DATE  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";

        // if (!empty($date_range)) {
        // $fromdate = @$date_range[0];
        // //     $todate = @$rslt[1];
        // //     /*===Convert Date in 24-Apr-2016===*/
        // var_dump($fromdate);
        // $fromdate = date_create($date_range[0]);

        // $from = date_format($fromdate, 'Y-m-d');
        // $from = $date_range;

        // }
        $idntiti_data .= " UNION ALL ";
        //  $idntiti_data .= "SELECT 'Lot' DEKIT_ITEM, DECODE(MT.BRAND, NULL, LL.BRAND, MT.BRAND) BARAND, LL.SPECIAL_LOT_ID PK_ID, LL.BARCODE_PRV_NO, DECODE(MT.MPN_DESCRIPTION, NULL, LL.MPN_DESCRIPTION, MT.MPN_DESCRIPTION) MPN_DESCRIPTION, DECODE(MT.MPN, NULL, LL.CARD_MPN, MT.MPN) MPN, DECODE(MT.UPC, NULL, LL.CARD_UPC, MT.UPC) UPC, OB.OBJECT_NAME, C.COND_NAME, TO_NUMBER(LL.AVG_SOLD) AVG_SOLD, LL.LOT_REMARKS REMARKS, ll.allocate_to assign_to, ll.discard, BAR.EBAY_ITEM_ID ,LL.LZ_MANIFEST_DET_ID GET_MANIF_ID, '' MAS_BAR, '' MAST_MPN_DESC, LL.FOLDER_NAME FOLDER_NAME,(SELECT mt.buisness_name
        //  FROM lz_merchant_barcode_mt mm,
        //       lz_merchant_barcode_dt d,
        //       lz_merchant_mt         mt
        // WHERE mm.mt_id = d.mt_id
        //   AND mm.merchant_id = mt.merchant_id
        //   AND d.BARCODE_NO = ll.barcode_prv_no) merchant_name,  'BIN-' || BAR.BIN_ID BIN_ID  FROM LZ_SPECIAL_LOTS  LL,LZ_BARCODE_MT BAR, LZ_CATALOGUE_MT  MT, LZ_ITEM_COND_MT  C, LZ_BD_OBJECTS_MT OB WHERE LL.CATALOG_MT_ID = MT.CATALOGUE_MT_ID(+)  AND LL.BARCODE_PRV_NO = BAR.BARCODE_NO(+) AND LL.CONDITION_ID = C.ID(+)  AND LL.OBJECT_ID = OB.OBJECT_ID(+) AND LL.PIC_DATE_TIME IS NOT NULL AND LL.PIC_BY IS NOT NULL AND LL.ALLOCATE_TO = '$user_id'  ";
        $idntiti_data .= "SELECT 'Lot' DEKIT_ITEM,
       DECODE(MT.BRAND, NULL, LL.BRAND, MT.BRAND) BARAND,
       LL.SPECIAL_LOT_ID PK_ID,
       LL.BARCODE_PRV_NO,
       DECODE(MT.MPN_DESCRIPTION,
              NULL,
              LL.MPN_DESCRIPTION,
              MT.MPN_DESCRIPTION) MPN_DESCRIPTION,
       DECODE(MT.MPN, NULL, LL.CARD_MPN, MT.MPN) MPN,
       DECODE(MT.UPC, NULL, LL.CARD_UPC, MT.UPC) UPC,
       OB.OBJECT_NAME,
       C.COND_NAME,
       TO_NUMBER(LL.AVG_SOLD) AVG_SOLD,
       LL.LOT_REMARKS REMARKS,
       ll.allocate_to assign_to,
       ll.discard,
       BAR.EBAY_ITEM_ID,
       LL.LZ_MANIFEST_DET_ID GET_MANIF_ID,
       '' MAS_BAR,
       '' MAST_MPN_DESC,
       LL.FOLDER_NAME FOLDER_NAME,
       (SELECT mt.buisness_name
          FROM lz_merchant_barcode_mt mm,
               lz_merchant_barcode_dt d,
               lz_merchant_mt         mt
         WHERE mm.mt_id = d.mt_id
           AND mm.merchant_id = mt.merchant_id
           AND d.BARCODE_NO = ll.barcode_prv_no) merchant_name,
(SELECT ac.account_name--.buisness_name
          FROM lz_merchant_barcode_mt mm,
               lz_merchant_barcode_dt d,
               lz_merchant_mt         mt,
               lj_merhcant_acc_dt ac
         WHERE mm.mt_id = d.mt_id
           AND mm.merchant_id = mt.merchant_id
           and ac.acct_id = d.account_id
           AND d.BARCODE_NO = ll.barcode_prv_no) account_name,
       'BIN-' || BAR.BIN_ID BIN_ID
  FROM LZ_SPECIAL_LOTS  LL,
       LZ_BARCODE_MT    BAR,
       LZ_CATALOGUE_MT  MT,
       LZ_ITEM_COND_MT  C,
       LZ_BD_OBJECTS_MT OB
 WHERE LL.CATALOG_MT_ID = MT.CATALOGUE_MT_ID(+)
   AND LL.BARCODE_PRV_NO = BAR.BARCODE_NO(+)
   AND LL.CONDITION_ID = C.ID(+)
   AND LL.OBJECT_ID = OB.OBJECT_ID(+)
   AND LL.PIC_DATE_TIME IS NOT NULL
   AND LL.PIC_BY IS NOT NULL
   AND LL.ALLOCATE_TO = '$user_id'";
        $idntiti_data .= "AND LL.ALLOC_DATE BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";

        $idntiti_data .= "  )";

        // if (!empty($date_range)) {
        // $fromdate = @$date_range[0];
        // //     $todate = @$rslt[1];
        // //     /*===Convert Date in 24-Apr-2016===*/
        // $fromdate = date_create($date_range[0]);

        // $from = date_format($fromdate, 'Y-m-d');
        // $from = $date_range;
        ////////$idntiti_data .= "AND LL.ALLOC_DATE BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        // }

        if (!empty($filterData) && $filterData != 'All') {

            if (!empty($filterData) && $filterData == 'Dekit') {
                $idntiti_data .= " where DEKIT_ITEM = '$filterData'";
                $idntiti_data .= " AND GET_MANIF_ID IS NULL";
                $idntiti_data .= " AND EBAY_ITEM_ID  IS NOT NULL";
                $idntiti_data .= " AND  DISCARD <> 1";

            } else if (!empty($filterData) && $filterData == 'Lot') {
                $idntiti_data .= " where DEKIT_ITEM = '$filterData'";
                $idntiti_data .= " AND GET_MANIF_ID IS NULL";
                $idntiti_data .= " AND EBAY_ITEM_ID  IS NOT NULL";
                $idntiti_data .= " AND  DISCARD <> 1";
            } else if (!empty($filterData) && $filterData == 'Not Listed') {
                $idntiti_data .= " where EBAY_ITEM_ID is null ";
                $idntiti_data .= " AND GET_MANIF_ID IS not NULL ";
                $idntiti_data .= " AND  DISCARD <> 1";
            } else if (!empty($filterData) && $filterData == 'Discarded') {
                $idntiti_data .= " WHERE DISCARD = 1";
                $idntiti_data .= " AND EBAY_ITEM_ID is null ";
            } else {
                $idntiti_data .= " AND GET_MANIF_ID IS NULL";
                $idntiti_data .= " AND EBAY_ITEM_ID  IS NOT NULL";
                $idntiti_data .= " AND DISCARD <> 1 ";

            }

        } else {
            $idntiti_data .= " Where GET_MANIF_ID IS NULL";
            $idntiti_data .= " AND  DISCARD <> 1";

        }
        //$idntiti_data .= "  )";

        $idntiti_datas = $this->db->query($idntiti_data)->result_array();

        if (count($idntiti_datas) >= 1) {

            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_identiti_bar_pics($idntiti_datas, $conditions);
            $images = $uri['uri'];

            return array('idntiti_data' => $idntiti_datas, "images" => $images, 'exist' => true);
        } else {
            return array('idntiti_data' => $idntiti_datas, 'exist' => false);
        }

    }

    public function get_identiti_bar_pics($barcodes, $conditions)
    {

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();

        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        foreach ($barcodes as $barcode) {

            $bar = $barcode['BARCODE_PRV_NO'];

            if (!empty($bar)) {

                $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE lot.barcode_prv_no = '$bar' and rownum <= 1  ")->result_array();
            }

            $folderName = "";
            if ($getFolder) {
                $folderName = $getFolder[0]['FOLDER_NAME'];
            } else {
                $folderName = $bar;
            }

            $dir = "";
            $barcodePictures = $master_path . $folderName . "/";
            $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

            if (is_dir($barcodePictures)) {
                $dir = $barcodePictures;
            } else if (is_dir($barcodePicturesThumb)) {
                $dir = $barcodePicturesThumb;
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);

            if (is_dir($dir)) {
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

                if ($images) {
                    $j = 0;
                    foreach ($images as $image) {

                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                        $uri[$bar][$j] = $base_url . $withoutMasterPartUri;
                        // if($uri[$barcode['LZ_barcode_ID']]){
                        //     break;
                        // }

                        $j++;
                    }
                } else {
                    $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar][1] = false;
                }
            } else {
                $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar][1] = false;
            }
        }
        return array('uri' => $uri);

    }

    // listing apis query start

    public function uplaod_seed($item_id, $manifest_id, $condition_id, $check_btn, $forceRevise)
    {

        if ($check_btn == 'revise_item' and $forceRevise == 0) {
            $query = $this->db->query("SELECT DET.WEIGHT, LS.ITEM_ID, LS.ITEM_TITLE, LS.ITEM_DESC, LS.EBAY_PRICE, LS.TEMPLATE_ID, LS.EBAY_LOCAL, LS.CURRENCY, LS.LIST_TYPE, LS.CATEGORY_ID, LS.SHIP_FROM_ZIP_CODE, LS.SHIP_FROM_LOC, LS.DEFAULT_COND, LS.DETAIL_COND, LS.PAYMENT_METHOD, LS.PAYPAL_EMAIL, LS.DISPATCH_TIME_MAX, LS.SHIPPING_COST, LS.ADDITIONAL_COST, LS.RETURN_OPTION, LS.RETURN_DAYS, LS.SHIPPING_PAID_BY, LS.SHIPPING_SERVICE, LS.CATEGORY_NAME, LS.LZ_MANIFEST_ID, LM.LOADING_NO, LM.LOADING_DATE, LM.PURCH_REF_NO, LS.F_MANUFACTURE MANUFACTURE,LS.F_MPN PART_NO, I.ITEM_MT_BBY_SKU SKU, LS.F_UPC UPC, LS.DEFAULT_COND ITEM_CONDITION, NULL QUANTITY, R.GENERAL_RULE, R.SPECIFIC_RULE, C.COND_NAME, LS.EPID,LS.S_MPN FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, LZ_MANIFEST_DET DET, ITEMS_MT I, LZ_LISTING_RULES R, LZ_ITEM_COND_MT C, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LM.LZ_MANIFEST_ID = DET.LZ_MANIFEST_ID AND LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND R.ITEM_CONDITION = LS.DEFAULT_COND AND LS.DEFAULT_COND = C.ID AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND = $condition_id AND ROWNUM=1");
        } else {
            $query = $this->db->query("SELECT DET.WEIGHT, LS.ITEM_ID, LS.ITEM_TITLE, LS.ITEM_DESC, LS.EBAY_PRICE, LS.TEMPLATE_ID, LS.EBAY_LOCAL, LS.CURRENCY, LS.LIST_TYPE, LS.CATEGORY_ID, LS.SHIP_FROM_ZIP_CODE, LS.SHIP_FROM_LOC, LS.DEFAULT_COND, LS.DETAIL_COND, LS.PAYMENT_METHOD, LS.PAYPAL_EMAIL, LS.DISPATCH_TIME_MAX, LS.SHIPPING_COST, LS.ADDITIONAL_COST, LS.RETURN_OPTION, LS.RETURN_DAYS, LS.SHIPPING_PAID_BY, LS.SHIPPING_SERVICE, LS.CATEGORY_NAME, LS.LZ_MANIFEST_ID, LM.LOADING_NO, LM.LOADING_DATE, LM.PURCH_REF_NO, LS.F_MANUFACTURE MANUFACTURE,LS.F_MPN PART_NO,  I.ITEM_MT_BBY_SKU  SKU, LS.F_UPC UPC, LS.DEFAULT_COND  ITEM_CONDITION, BCD.QTY  QUANTITY, R.GENERAL_RULE, R.SPECIFIC_RULE, C.COND_NAME,LS.EPID,LS.S_MPN , PC.CATEGORY_NAME PART_CATEGORY FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, LZ_MANIFEST_DET DET, ITEMS_MT I, LZ_LISTING_RULES R, LZ_ITEM_COND_MT C,LJ_PARTS_CATEGORY PC, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 AND BC.EBAY_ITEM_ID IS NULL AND BC.LIST_ID IS NULL AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL AND BC.ORDER_DT_ID IS NULL AND BC.NOT_FOUND_BY IS NULL AND BC.LZ_DEKIT_US_MT_ID_FOR_OUT IS NULL AND BC.RETURNED_ID IS NULL AND BC.DISCARD = 0 AND BC.CONDITION_ID <> -1 GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LM.LZ_MANIFEST_ID = DET.LZ_MANIFEST_ID AND LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND R.ITEM_CONDITION = LS.DEFAULT_COND AND LS.DEFAULT_COND = C.ID AND LS.CATEGORY_ID = PC.CATEGORY_ID(+) AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND = $condition_id AND ROWNUM=1");
            //$query = $this->db->query("SELECT DET.WEIGHT, LS.ITEM_ID, LS.ITEM_TITLE, LS.ITEM_DESC, LS.EBAY_PRICE, LS.TEMPLATE_ID, LS.EBAY_LOCAL, LS.CURRENCY, LS.LIST_TYPE, LS.CATEGORY_ID, LS.SHIP_FROM_ZIP_CODE, LS.SHIP_FROM_LOC, LS.DEFAULT_COND, LS.DETAIL_COND, LS.PAYMENT_METHOD, LS.PAYPAL_EMAIL, LS.DISPATCH_TIME_MAX, LS.SHIPPING_COST, LS.ADDITIONAL_COST, LS.RETURN_OPTION, LS.RETURN_DAYS, LS.SHIPPING_PAID_BY, LS.SHIPPING_SERVICE, LS.CATEGORY_NAME, LS.LZ_MANIFEST_ID, LM.LOADING_NO, LM.LOADING_DATE, LM.PURCH_REF_NO, LS.F_MANUFACTURE MANUFACTURE,LS.F_MPN PART_NO,  I.ITEM_MT_BBY_SKU  SKU, LS.F_UPC UPC, LS.DEFAULT_COND  ITEM_CONDITION, BCD.QTY  QUANTITY, R.GENERAL_RULE, R.SPECIFIC_RULE, C.COND_NAME,LS.EPID FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, LZ_MANIFEST_DET DET, ITEMS_MT I, LZ_LISTING_RULES R, LZ_ITEM_COND_MT C, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 AND BC.EBAY_ITEM_ID IS NULL AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LM.LZ_MANIFEST_ID = DET.LZ_MANIFEST_ID AND LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND R.ITEM_CONDITION = LS.DEFAULT_COND AND LS.DEFAULT_COND = C.ID AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND = $condition_id AND ROWNUM=1");
        }

        return $query->result_array();
    }

    public function uplaod_seed_pic($item_id, $manifest_id, $condition_id, $seed_id)
    {

        // $query = $this->db->query("SELECT trim(I.ITEM_MT_MFG_PART_NO) ITEM_MT_MFG_PART_NO,trim(I.ITEM_MT_UPC) ITEM_MT_UPC FROM ITEMS_MT I WHERE I.ITEM_ID = $item_id");

        $query = $this->db->query("SELECT I.F_UPC ITEM_MT_UPC,I.F_MPN ITEM_MT_MFG_PART_NO FROM LZ_ITEM_SEED I WHERE I.SEED_ID =$seed_id ");
        $result = $query->result_array();
        $mpn = $result[0]['ITEM_MT_MFG_PART_NO'];
        $upc = $result[0]['ITEM_MT_UPC'];
        $it_condition = $condition_id;
        $query = $this->db->query("SELECT COND_NAME FROM LZ_ITEM_COND_MT WHERE ID = $condition_id");
        $result = $query->result_array();
        $it_condition = $result[0]['COND_NAME'];
        $mpn = str_replace('/', '_', @$mpn);

        /*==============================================
        =            Master Picture Check            =
        ==============================================*/
        $query = $this->db->query("SELECT  MASTER_PATH,SPECIFIC_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = 1");
        $master_qry = $query->result_array();
        $master_path = $master_qry[0]['MASTER_PATH'];
        $specific_path = $master_qry[0]['SPECIFIC_PATH'];

        $m_dir = $master_path . @$upc . "~" . @$mpn . "/" . @$it_condition;
        if (is_dir(@$m_dir)) {
            $iterator = new \FilesystemIterator(@$m_dir);
            if (@$iterator->valid()) {
                $m_flag = true;
            } else {
                $m_flag = false;
            }
        } else {
            $m_flag = false;
        }
        /*=====  End of Master Picture Check  ======*/

        /*==============================================
        =            Specific Picture Check            =
        ==============================================*/
        $s_dir = $specific_path . @$upc . "~" . $mpn . "/" . @$it_condition . "/" . $manifest_id;
        // Open a directory, and read its contents
        if (is_dir(@$s_dir)) {
            $iterator = new \FilesystemIterator(@$s_dir);
            if (@$iterator->valid()) {
                $s_flag = true;
            } else {
                $s_flag = false;
            }
        } else {
            $s_flag = false;
        }
        /*=====  End of Specific Picture Check  ======*/
        if ($m_flag && $s_flag) {
            return "B";

        } elseif ($m_flag === true && $s_flag === false) {
            return "M";
        } elseif ($m_flag === false && $s_flag === true) {
            return "S";
        } else {
            die('Error! Item Picture Not Found.');
        }
    } //end function

    public function item_specifics($item_id, $manifest_id, $condition_id)
    {
        // $item_id = 18786;
        // $manifest_id = 13827;
        // $query = $this->db->query("SELECT s.f_upc  I.ITEM_MT_UPC, s.f_mpn I.ITEM_MT_MFG_PART_NO, S.CATEGORY_ID FROM ITEMS_MT I, LZ_ITEM_SEED S WHERE I.ITEM_ID = $item_id AND I.ITEM_ID = S.ITEM_ID AND S.LZ_MANIFEST_ID = $manifest_id AND S.DEFAULT_COND = $condition_id AND ROWNUM = 1");
        $query = $this->db->query("SELECT s.f_upc  ITEM_MT_UPC, s.f_mpn ITEM_MT_MFG_PART_NO, S.CATEGORY_ID FROM ITEMS_MT I, LZ_ITEM_SEED S WHERE I.ITEM_ID = $item_id AND I.ITEM_ID = S.ITEM_ID AND S.LZ_MANIFEST_ID = $manifest_id AND S.DEFAULT_COND = $condition_id AND ROWNUM = 1");

        $result = $query->result_array();

        if ($query->num_rows() > 0) {

            if (!empty($result[0]['ITEM_MT_UPC'])) {
                $where_upc = " AND MT.UPC = '" . $result[0]['ITEM_MT_UPC'] . "'";
            } else {
                //$where_upc = ' ';
                $where_upc = " AND MT.UPC IS NULL";
            }
            if (!empty($result[0]['ITEM_MT_MFG_PART_NO'])) {
                $where_mpn = " AND MT.MPN = '" . $result[0]['ITEM_MT_MFG_PART_NO'] . "'";
            } else {
                //$where_mpn = '';
                $where_mpn = " AND MT.MPN IS NULL";
            }

            $spec_query = $this->db->query("SELECT MT.SPECIFICS_NAME, DT.SPECIFICS_VALUE FROM LZ_ITEM_SPECIFICS_MT MT, LZ_ITEM_SPECIFICS_DET DT WHERE DT.SPECIFICS_MT_ID = MT.SPECIFICS_MT_ID  AND MT.CATEGORY_ID = " . $result[0]['CATEGORY_ID'] . $where_upc . $where_mpn);
            $spec_query = $spec_query->result_array();

        } else {
            $spec_query = "";
        }

        return $spec_query;

        //var_dump($spec_query);exit ;

    }

    public function insert_ebay_id($item_id, $manifest_id, $seed_id, $condition_id, $status, $check_btn, $forceRevise, $account_id, $userId)
    {
        $list_barcode = $this->input->post('list_barcode');

        $list_rslt = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('EBAY_LIST_MT','LIST_ID') LIST_ID FROM DUAL");
        $rs = $list_rslt->result_array();
        $LIST_ID = $rs[0]['LIST_ID'];
        $this->session->set_userdata('list_id', $LIST_ID);
        $ebay_id = $this->session->userdata('ebay_item_id');
        $ebay_account = $this->session->userdata('ebay_account');
        if(!empty($ebay_account)) {
            $get_acc = $this->db->query("select ac.acct_id
            from lz_merchant_barcode_mt mt, lz_merchant_barcode_dt dt , lj_merhcant_acc_dt ac
           where mt.mt_id = dt.mt_id
             and dt.barcode_no = '$list_barcode'
             and ac.merchant_id = mt.merchant_id   
             and LOWER(ac.account_name) = '$ebay_account'");
            $rs = $get_acc->result_array();
            if (count($rs) > 0) {
                $account_id = $rs[0]['ACCT_ID'];
            }
        }else  {
            $get_acc = $this->db->query("SELECT E.LZ_SELLER_ACCT_ID FROM EBAY_LIST_MT E WHERE E.LIST_ID = (SELECT MIN(LIST_ID) FROM EBAY_LIST_MT EE WHERE EE.EBAY_ITEM_ID = '$ebay_id' AND EE.LZ_SELLER_ACCT_ID IS NOT NULL )");
            $rs = $get_acc->result_array();
            if (count($rs) > 0) {
                $account_id = $rs[0]['LZ_SELLER_ACCT_ID'];
            }
        }
       
        if ($check_btn == "revise_item") {
            $status = "UPDATE";

            // if(@$forceRevise === 1){
            //     $query = $this->db->query("SELECT LS.ITEM_TITLE,LS.EBAY_PRICE, BCD.QTY QUANTITY FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, ITEMS_MT I, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0  AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND=$condition_id");
            //     if($query->num_rows() === 0){
            //         $query = $this->db->query("SELECT LS.ITEM_TITLE,LS.EBAY_PRICE, BCD.QTY QUANTITY FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, ITEMS_MT I, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND=$condition_id");
            //     }
            //     $update_barcode_qry = "UPDATE LZ_BARCODE_MT SET EBAY_ITEM_ID=$ebay_id, LIST_ID = $LIST_ID WHERE ITEM_ID= $item_id AND LZ_MANIFEST_ID = $manifest_id AND CONDITION_ID = $condition_id AND EBAY_ITEM_ID IS NULL AND LIST_ID IS NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND HOLD_STATUS = 0";
            // }else{

            //     $query = $this->db->query("SELECT LS.ITEM_TITLE,LS.EBAY_PRICE, BCD.QTY QUANTITY FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, ITEMS_MT I, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 AND BC.EBAY_ITEM_ID IS NOT NULL  AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND=$condition_id");
            //     if($query->num_rows() === 0){
            //         $query = $this->db->query("SELECT LS.ITEM_TITLE,LS.EBAY_PRICE, BCD.QTY QUANTITY FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, ITEMS_MT I, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND=$condition_id");
            //     }

            //     $update_barcode_qry = "UPDATE LZ_BARCODE_MT SET EBAY_ITEM_ID=$ebay_id, LIST_ID = $LIST_ID WHERE ITEM_ID= $item_id AND LZ_MANIFEST_ID = $manifest_id AND CONDITION_ID = $condition_id AND EBAY_ITEM_ID IS NOT NULL AND LIST_ID IS NOT NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND HOLD_STATUS = 0";
            // }
            $query = $this->db->query("SELECT LS.ITEM_TITLE,LS.EBAY_PRICE, BCD.QTY QUANTITY FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, ITEMS_MT I, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 AND BC.EBAY_ITEM_ID IS NOT NULL  AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND=$condition_id");
            if ($query->num_rows() === 0) {
                $query = $this->db->query("SELECT LS.ITEM_TITLE,LS.EBAY_PRICE, BCD.QTY QUANTITY FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, ITEMS_MT I, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND=$condition_id");
            }
            /*============================================
            =            get effected barcode            =
            ============================================*/

            // $affected_barcode_qry = "SELECT LISTAGG(BARCODE_NO, ', ') WITHIN GROUP (ORDER BY BARCODE_NO) BARCODE_NO FROM LZ_BARCODE_MT WHERE ITEM_ID= $item_id AND LZ_MANIFEST_ID = $manifest_id AND CONDITION_ID = $condition_id AND EBAY_ITEM_ID IS NOT NULL AND LIST_ID IS NOT NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND HOLD_STATUS = 0";

            /*=====  End of get effected barcode  ======*/

            $update_barcode_qry = "UPDATE LZ_BARCODE_MT SET EBAY_ITEM_ID='$ebay_id', LIST_ID = $LIST_ID WHERE ITEM_ID= $item_id AND LZ_MANIFEST_ID = $manifest_id AND CONDITION_ID = $condition_id AND EBAY_ITEM_ID IS NOT NULL AND LIST_ID IS NOT NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND HOLD_STATUS = 0";

        } else {
            $query = $this->db->query("SELECT LS.ITEM_TITLE,LS.EBAY_PRICE, BCD.QTY QUANTITY FROM LZ_ITEM_SEED LS, LZ_MANIFEST_MT LM, ITEMS_MT I, (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 AND BC.EBAY_ITEM_ID IS NULL  AND BC.LIST_ID IS NULL AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD WHERE LS.ITEM_ID = I.ITEM_ID AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID AND LS.ITEM_ID = BCD.ITEM_ID AND LS.DEFAULT_COND = BCD.CONDITION_ID AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID AND LS.ITEM_ID = $item_id and LS.LZ_MANIFEST_ID = $manifest_id and LS.DEFAULT_COND=$condition_id");
            /*============================================
            =            get effected barcode            =
            ============================================*/

            // $affected_barcode_qry = "SELECT LISTAGG(BARCODE_NO, ', ') WITHIN GROUP (ORDER BY BARCODE_NO) BARCODE_NO FROM LZ_BARCODE_MT WHERE ITEM_ID= $item_id AND LZ_MANIFEST_ID = $manifest_id AND CONDITION_ID = $condition_id AND EBAY_ITEM_ID IS NULL AND LIST_ID IS NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND HOLD_STATUS = 0";

            /*=====  End of get effected barcode  ======*/
            $update_barcode_qry = "UPDATE LZ_BARCODE_MT SET EBAY_ITEM_ID='$ebay_id', LIST_ID = $LIST_ID WHERE ITEM_ID= $item_id AND LZ_MANIFEST_ID = $manifest_id AND CONDITION_ID = $condition_id AND EBAY_ITEM_ID IS NULL AND LIST_ID IS NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND HOLD_STATUS = 0";
        } //check_btn if else close
        $rslt_dta = $query->result_array();

        //$list_date = date("d/M/Y");// return format Aug/13/2016
        date_default_timezone_set("America/Chicago");
        $list_date = date("Y-m-d H:i:s");
        $list_date = "TO_DATE('" . $list_date . "', 'YYYY-MM-DD HH24:MI:SS')";
        $lister_id = $userId; //$this->session->userdata('user_id');
        $ebay_item_desc = @$rslt_dta[0]['ITEM_TITLE'];
        $ebay_item_desc = trim(str_replace("  ", '', $ebay_item_desc));
        $ebay_item_desc = trim(str_replace(array("'"), "''", $ebay_item_desc));
        if ($check_btn == "revise_item") {
            //$current_qty = $this->session->userdata('current_qty');
            $list_qty = 0;
        } else {
            $list_qty = @$rslt_dta[0]['QUANTITY'];
        }

        $ebay_item_id = $ebay_id;
        $list_price = @$rslt_dta[0]['EBAY_PRICE'];
        $remarks = null;
        $single_entry_id = null;
        $salvage_qty = 0.00;
        $entry_type = "L";
        $LZ_SELLER_ACCT_ID = $account_id;
        $auth_by_id = $this->session->userdata('auth_by_id');
        $list_qty = "'" . $list_qty . "'";

        $insert_query = $this->db->query("INSERT INTO ebay_list_mt (LIST_ID, LZ_MANIFEST_ID, LISTING_NO, ITEM_ID, LIST_DATE, LISTER_ID, EBAY_ITEM_DESC, LIST_QTY, EBAY_ITEM_ID, LIST_PRICE, REMARKS, SINGLE_ENTRY_ID, SALVAGE_QTY, ENTRY_TYPE, LZ_SELLER_ACCT_ID, SEED_ID, STATUS, ITEM_CONDITION, AUTH_BY_ID,FORCEREVISE)VALUES (" . $LIST_ID . "," . $manifest_id . ", " . $LIST_ID . ", " . $item_id . ", " . $list_date . ", " . $lister_id . ", '" . $ebay_item_desc . "', " . $list_qty . ",'" . $ebay_item_id . "','" . $list_price . "',NULL,NULL, NULL, '" . $entry_type . "'," . $LZ_SELLER_ACCT_ID . "," . $seed_id . ",'" . trim($status) . "'," . $condition_id . ", '" . $auth_by_id . "'," . @$forceRevise . ")");
        if ($insert_query) {
            //  $affected_barcode = $this->db->query($affected_barcode_qry)->result_array();
            $this->db->query($update_barcode_qry);
            $this->db->query("UPDATE LZ_LISTING_ALLOC SET LIST_ID = $LIST_ID WHERE SEED_ID = $seed_id");
            //return array('list_id' => $LIST_ID, 'affected_barcode' => $affected_barcode);
            return array('list_id' => $LIST_ID);
        }

    }

    public function insert_ebay_url($userId)
    {
        $ebay_item_id = $this->session->userdata('ebay_item_id');
        //$ebay_item_url = $this->session->userdata('ebay_item_url');
        $ebay_item_url = "https://www.ebay.com/itm/" . $ebay_item_id;
        date_default_timezone_set("America/Chicago");
        $list_date = date("Y-m-d H:i:s");
        $ins_date = "TO_DATE('" . $list_date . "', 'YYYY-MM-DD HH24:MI:SS')";
        $entered_by = $userId; //$this->session->userdata('user_id');
        $comma = ',';
        $query = $this->db->query("SELECT * FROM LZ_LISTED_ITEM_URL WHERE EBAY_ID = $ebay_item_id");
        $result = $query->result_array();
        if ($query->num_rows() == 0) {
            $insert_query = $this->db->query("INSERT INTO LZ_LISTED_ITEM_URL VALUES ($ebay_item_id $comma '$ebay_item_url' $comma $ins_date $comma $entered_by)");
        }
    }

    // listing apis query end
    /// get lz webiste functions
    public function ljw_Brands()
    {

        $get_product_name = $this->input->post('get_product_name');
        $ljw_getobject = $this->db->query("SELECT  DT.BRAND_DT_ID,(SELECT B.IMAGE_URL FROM LZW_BRANDS_MT B WHERE B.BRAND_ID = DT.BRAND_ID) BRAND_URL, (SELECT B.DESCRIPTION FROM LZW_BRANDS_MT B WHERE B.BRAND_ID = DT.BRAND_ID) BRAND_NAME, (SELECT O.OBJECT_NAME FROM LZW_OBJECT_MT O WHERE O.OBJECT_ID = DT.OBJECT_ID) PRODUCT,nvl(DT.ACTIVE,0)ACTIVE FROM LZW_BRANDS_DT DT WHERE DT.OBJECT_ID =$get_product_name ")->result_array();
        foreach ($ljw_getobject as $key => $value) {
            $ljw_getobject[$key]['BRAND_URL_FULL'] = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $value['BRAND_URL'];
        }
        return array('ljw_getobject' => $ljw_getobject, 'exist' => true);

    }

    public function ljw_Series()
    {

        $get_brand_name = $this->input->post('get_brand_name');

        $ljw_Series = $this->db->query("SELECT SD.SERIES_DT_ID, sm.description SERIES_NAME, SD.BRAND_DT_ID, (SELECT BM.DESCRIPTION FROM LZW_BRANDS_DT BT, LZW_BRANDS_MT BM WHERE BM.BRAND_ID = BT.BRAND_ID AND BT.BRAND_DT_ID = SD.BRAND_DT_ID) BRAND_NAME, nvl(sm.ACTIVE, 0) ACTIVE FROM LZW_SERIES_DT SD, lzw_series_mt sm WHERE to_char(SD.BRAND_DT_ID) = '$get_brand_name' and sd.series_id = sm.series_id and sm.active = 0 ")->result_array();

        // $ljw_Series = $this->db->query("SELECT SD.SERIES_DT_ID,(SELECT MT.DESCRIPTION FROM LZW_SERIES_MT MT WHERE MT.SERIES_ID = SD.SERIES_ID) SERIES_NAME, SD.BRAND_DT_ID, (SELECT BM.DESCRIPTION FROM LZW_BRANDS_DT BT,LZW_BRANDS_MT BM WHERE BM.BRAND_ID = BT.BRAND_ID AND BT.BRAND_DT_ID = SD.BRAND_DT_ID ) BRAND_NAME ,nvl(SD.ACTIVE,0) ACTIVE  FROM LZW_SERIES_DT SD WHERE  to_char(SD.BRAND_DT_ID) ='$get_brand_name'")->result_array();

        return array('ljw_Series' => $ljw_Series, 'exist' => true);
    }
    public function ljw_Model()
    {
        $get_series_name = $this->input->post('get_series_name');
        // old query
        $ljw_Model = $this->db->query(" SELECT MDT.MODEL_DT_ID,MD.DESCRIPTION,MD.IMAGE_URL,NVL(MDT.ACTIVE,0) ACTIVE FROM LZW_MODEL_MT MD,LZW_MODEL_DT MDT WHERE MD.MODEL_ID = MDT.MODEL_ID  AND TO_CHAR(MDT.SERIES_DT_ID) ='$get_series_name'  AND MDT.MODEL_DT_ID   IN(SELECT D.MODEL_DT_ID  FROM LZW_CARRIER_DT D WHERE  D.ACTIVE =0 ) ")->result_array();

        foreach ($ljw_Model as $key => $value) {
            $ljw_Model[$key]['IMAGE_URL_FULL'] = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $value['IMAGE_URL'];
        }
        return array('ljw_Model' => $ljw_Model, 'exist' => true);
    }
    public function ljw_Issues()
    {
        $get_product_name = $this->input->post('get_product_name');
        $ljw_Issues = $this->db->query("SELECT SDS.ISSUE_DT_ID, (SELECT SM.DESCIPTION FROM LZW_ISSUES_MT SM WHERE SM.ISSUE_ID =SDS.ISSUE_ID) ISSU_NAME FROM LZW_ISSUES_DT SDS WHERE to_char(SDS.OBJECT_ID) = '$get_product_name'")->result_array();
        return array('ljw_Issues' => $ljw_Issues, 'exist' => true);
    }
    public function ljw_Carrier()
    {
        $model_dt_id = $this->input->post('model_dt_id');
        $ljw_Carrier = $this->db->query("SELECT CD.LZW_CARRIER_DT_ID, C.CARRIER_NAME, C.IMAGE_URL,nvl(CD.ACTIVE,0) ACTIVE
        FROM lzw_carrier C, lzw_carrier_DT CD
       WHERE C.CARRIER_ID = CD.CARRIER_ID

       AND CD.MODEL_DT_ID = '$model_dt_id'
       AND CD.LZW_CARRIER_DT_ID IN (SELECT D.LZW_CARRIER_DT_ID FROM LZW_STORAGE_DT D WHERE  D.ACTIVE =0 ) ORDER BY C.DISPLAY_ORDER ASC ")->result_array();

        foreach ($ljw_Carrier as $key => $value) {
            $ljw_Carrier[$key]['IMAGE_URL_FULL'] = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $value['IMAGE_URL'];
        }
        return array('ljw_Carrier' => $ljw_Carrier, 'exist' => true);
    }
    public function ljw_Storage()
    {
        $carrier_id = $this->input->post('carrier_id');
        $ljw_Storage = $this->db->query("SELECT nvl(SD.SALE_PRICE,0) SALE_PRICE,SD.LZW_STORAGE_DT_ID,SM.STORAGE_DESC ,nvl(SD.ACTIVE,0) ACTIVE FROM lzw_storage_mt SM,lzw_storage_DT SD
        WHERE SM.STORAGE_MT_ID = SD.STORAGE_MT_ID
        and sd.ACTIVE =0
        AND SD.LZW_CARRIER_DT_ID = '$carrier_id'")->result_array();
        return array('ljw_Storage' => $ljw_Storage, 'exist' => true);
    }
    public function ljw_getAnOffer()
    {
        $object_id = $this->input->post('object_id');
        $lzw_questions_mt = $this->db->query("select qm.question_description, qm.questions_mt_id, dg.object_id,NVL((
            SELECT COUNT(AN.QUESTIONS_MT_ID) FROM LZW_ANSWERS AN
            WHERE AN.QUESTIONS_MT_ID = QM.QUESTIONS_MT_ID
            GROUP BY AN.QUESTIONS_MT_ID),0) TOT_ANS
                    from LZW_ANSWERS_DT dg, lzw_questions_mt qm
                    where dg.questions_mt_id = qm.questions_mt_id
        AND OBJECT_ID = '$object_id'")->result_array();
        $lzw_questions_ans = $this->db->query("SELECT a.answers_id,a.questions_mt_id,v.value_desc,a.effective_value,v.default_val FROM LZW_ANSWERS a,LZW_ANSWER_VALUE v WHERE a.answer_values = v.answer_value_id")->result_array();
        return array('lzw_questions_mt' => $lzw_questions_mt, 'lzw_questions_ans' => $lzw_questions_ans, 'exist' => true);
    }
    public function ljw_getAnOfferAdmin()
    {
        $trade_id = $this->input->post('trade_id');
        $result = $this->db->query("SELECT *
                FROM (SELECT 'SELL-' || LPAD(NVL(RM.LZW_TRADE_ID, 0), 6, '0') trade_id,RM.LZW_TRADE_ID,
                            RM.object_id
                        FROM LZW_trade_request RM)
                WHERE trade_id = '$trade_id'")->row();
        $object_id = $result->OBJECT_ID;
        $trade_id = $result->LZW_TRADE_ID;

        $lzw_questions_mt = $this->db->query("select qm.question_description, qm.questions_mt_id, dg.object_id,NVL((
            SELECT COUNT(AN.QUESTIONS_MT_ID) FROM LZW_ANSWERS AN
            WHERE AN.QUESTIONS_MT_ID = QM.QUESTIONS_MT_ID
            GROUP BY AN.QUESTIONS_MT_ID),0) TOT_ANS
                    from LZW_ANSWERS_DT dg, lzw_questions_mt qm
                    where dg.questions_mt_id = qm.questions_mt_id
        AND OBJECT_ID = '$object_id'")->result_array();
        $lzw_questions_ans = $this->db->query("SELECT a.answers_id,a.questions_mt_id,v.value_desc,a.effective_value,v.default_val FROM LZW_ANSWERS a,LZW_ANSWER_VALUE v WHERE a.answer_values = v.answer_value_id")->result_array();
        $lzw_given_ans = $this->db->query("SELECT o.lzw_trade_id,o.answers_id,s.answer_values,
               (SELECT v.value_desc
                  FROM lzw_answer_value v
                 WHERE v.answer_value_id = s.answer_values) ans,
               s.questions_mt_id,
               q.question_description
          FROM lzw_given_ansers o, lzw_answers s, lzw_questions_mt q
         WHERE o.lzw_trade_id = '$trade_id'
           AND o.answers_id = s.answers_id
           AND s.questions_mt_id = q.questions_mt_id")->result_array();

        return array('lzw_questions_mt' => $lzw_questions_mt, 'lzw_questions_ans' => $lzw_questions_ans, 'lzw_given_ans' => $lzw_given_ans, 'exist' => true);
    }

    public function ljw_getAnOfferMobile()
    {
        $object_id = $this->input->post('object_id');
        $lzw_questions_mt = $this->db->query("select qm.questions_mt_id id, qm.question_description description from LZW_ANSWERS_DT dg, lzw_questions_mt qm where dg.questions_mt_id = qm.questions_mt_id AND OBJECT_ID = '$object_id'")->result_array();

        $questions = array();
        foreach ($lzw_questions_mt as $question) {
            $query = "SELECT a.answers_id id, v.value_desc description, a.effective_value,v.default_val IS_DEFAULT" .
                " FROM LZW_ANSWERS a,LZW_ANSWER_VALUE v WHERE a.answer_values = v.answer_value_id AND a.questions_mt_id = " . $question['ID'];
            $question['answers'] = $this->db->query($query)->result_array();

            array_push($questions, $question);
        }

        return $questions;
    }

    public function ljw_SaveRequest()
    {
        $product_name = $this->input->post('product_name');
        $brand_name = $this->input->post('brand_name');
        $series_name = $this->input->post('series_name');
        $model_name = $this->input->post('model_name');
        $issues_name = $this->input->post('issues_name');
        $issuesInput = $this->input->post('issuesInput');
        $emailNumb = $this->input->post('emailNumb');
        $phoneNumb = $this->input->post('phoneNumb');
        $LastName = $this->input->post('LastName');
        $user_id = $this->input->post('user_id');

        if (!empty($user_id)) {

            $get_user = $user_id;
        } else {
            $get_user = 1;
        }
        $LastName = trim(str_replace("  ", ' ', $LastName));
        $LastName = str_replace(array("`,'"), "", $LastName);
        $LastName = str_replace(array("'"), "''", $LastName);
        $yourName = $this->input->post('yourName');
        $yourName = trim(str_replace("  ", ' ', $yourName));
        $yourName = str_replace(array("`,'"), "", $yourName);
        $yourName = str_replace(array("'"), "''", $yourName);
        $enterComents = $this->input->post('enterComents');
        $coments = trim(str_replace("  ", ' ', $enterComents));
        $coments = str_replace(array("`,'"), "", $coments);
        $coments = str_replace(array("'"), "''", $coments);

        if (isset($_FILES['images'])) {
            $images = $_FILES['images'];
        }
        $req_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZW_REPAIRE_MT','REPAIRE_MT_ID') RE_PK FROM DUAL");
        $req_pk = $req_pk->result_array();
        $req_pk_id = $req_pk[0]['RE_PK'];

        $saveRequest = $this->db->query("INSERT INTO LZW_REPAIRE_MT (REPAIRE_MT_ID, F_NAME, L_NAME, EMAIL, CONTACT_NO,CUSTOMER_REMARKS, OBJECT_ID,BRAND_INPUT,SERIES_INPUT,MODAL_INPUT,ISSUE_INPUT,USER_ID )VALUES($req_pk_id, '$yourName', '$LastName', '$emailNumb', '$phoneNumb','$coments','$product_name','$brand_name','$series_name','$model_name','$issuesInput','$get_user') ");

        if ($saveRequest) {

            $req_pk = $this->db->query("SELECT 'REP-' || LPAD(NVL(MT.REPAIRE_MT_ID, 0), 6, '0')   getid FROM LZW_REPAIRE_MT MT WHERE MT.REPAIRE_MT_ID = $req_pk_id ");
            $req_pk = $req_pk->result_array();

            $get_req_id = $req_pk[0]['GETID'];

            if ($get_req_id) {
                foreach ($issues_name as $key) {
                    $req_det_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZW_REPAIRE_DT','REPAIRE_DT_ID') RE_PK FROM DUAL");
                    $req_det_pk = $req_det_pk->result_array();
                    $req_det_pk_id = $req_det_pk[0]['RE_PK'];

                    $this->db->query("INSERT INTO LZW_REPAIRE_DT (REPAIRE_DT_ID, REPAIRE_ID, ISSUE_DT_ID)VALUES($req_det_pk_id,$req_pk_id, '$key' ) ");
                }

                if (isset($_FILES['images'])) {

                    $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 9");
                    $specific_qry = $query->result_array();
                    $specific_path = $specific_qry[0]['MASTER_PATH'];
                    // $specific_path = "D:/wamp/www/item_pictures/repair_pics/";

                    $main_dir = $specific_path . $get_req_id;
                    if (is_dir($main_dir) === false) {
                        mkdir($main_dir);
                    }
                    foreach ($images['name'] as $key => $value) {
                        $_FILES['file']['name'] = $_FILES['images']['name'][$key];
                        $_FILES['file']['type'] = $_FILES['images']['type'][$key];
                        $_FILES['file']['tmp_name'] = $_FILES['images']['tmp_name'][$key];
                        $_FILES['file']['error'] = $_FILES['images']['error'][$key];
                        $_FILES['file']['size'] = $_FILES['images']['size'][$key];
                        $config['upload_path'] = $main_dir;
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        $config['max_size'] = '10000'; // max_size in kb
                        $new_name = $get_req_id . '_repairing';
                        $config['file_name'] = $new_name;
                        //Load upload library
                        $this->load->library('upload', $config);
                        $this->upload->do_upload('file');
                    }

                } // pic code check against null

            }
            return array('get_req_id' => $get_req_id, 'exist' => true);
        } else {
            $get_req_id = '';
            return array('get_req_id' => $get_req_id, 'exist' => false);
        }
    }
    public function ljw_SaveRequestMobile()
    {
        $product_name = trim($this->input->post('product_name'));
        $product_name = trim(str_replace("  ", ' ', $product_name));
        $product_name = str_replace(array("`,'"), "", $product_name);
        $product_name = str_replace(array("'"), "''", $product_name);

        $brand_name = trim($this->input->post('brand_name'));
        $brand_name = trim(str_replace("  ", ' ', $brand_name));
        $brand_name = str_replace(array("`,'"), "", $brand_name);
        $brand_name = str_replace(array("'"), "''", $brand_name);

        $series_name = trim($this->input->post('series_name'));
        $series_name = trim(str_replace("  ", ' ', $series_name));
        $series_name = str_replace(array("`,'"), "", $series_name);
        $series_name = str_replace(array("'"), "''", $series_name);

        $model_name = trim($this->input->post('model_name'));
        $model_name = trim(str_replace("  ", ' ', $model_name));
        $model_name = str_replace(array("`,'"), "", $model_name);
        $model_name = str_replace(array("'"), "''", $model_name);

        $issues_name = trim($this->input->post('issues_name'));
        $issues_name = trim(str_replace("  ", ' ', $issues_name));
        $issues_name = str_replace(array("`,'"), "", $issues_name);
        $issues_name = str_replace(array("'"), "''", $issues_name);

        $issuesInput = trim($this->input->post('issuesInput'));
        $issuesInput = trim(str_replace("  ", ' ', $issuesInput));
        $issuesInput = str_replace(array("`,'"), "", $issuesInput);
        $issuesInput = str_replace(array("'"), "''", $issuesInput);

        $emailNumb = trim($this->input->post('emailNumb'));
        $emailNumb = trim(str_replace("  ", ' ', $emailNumb));
        $emailNumb = str_replace(array("`,'"), "", $emailNumb);
        $emailNumb = str_replace(array("'"), "''", $emailNumb);

        $phoneNumb = trim($this->input->post('phoneNumb'));
        $phoneNumb = trim(str_replace("  ", ' ', $phoneNumb));
        $phoneNumb = str_replace(array("`,'"), "", $phoneNumb);
        $phoneNumb = str_replace(array("'"), "''", $phoneNumb);

        $yourName = trim($this->input->post('yourName'));
        $yourName = trim(str_replace("  ", ' ', $yourName));
        $yourName = str_replace(array("`,'"), "", $yourName);
        $yourName = str_replace(array("'"), "''", $yourName);

        $LastName = "...";
        // $LastName = trim(str_replace("  ", ' ', $yourName));
        // $LastName = str_replace(array("`,'"), "", $yourName);
        // $LastName = str_replace(array("'"), "''", $yourName);

        $enterComents = trim($this->input->post('enterComents'));
        $coments = trim(str_replace("  ", ' ', $enterComents));
        $coments = str_replace(array("`,'"), "", $coments);
        $coments = str_replace(array("'"), "''", $coments);

        $req_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZW_REPAIRE_MT','REPAIRE_MT_ID') RE_PK FROM DUAL");
        $req_pk = $req_pk->result_array();
        $req_pk_id = $req_pk[0]['RE_PK'];

        $saveRequest = $this->db->query("INSERT INTO LZW_REPAIRE_MT (REPAIRE_MT_ID, F_NAME, L_NAME, EMAIL, CONTACT_NO,CUSTOMER_REMARKS, OBJECT_ID,BRAND_INPUT,SERIES_INPUT,MODAL_INPUT,ISSUE_INPUT )VALUES($req_pk_id, '$yourName', '$LastName', '$emailNumb', '$phoneNumb','$coments','$product_name','$brand_name','$series_name','$model_name','$issuesInput') ");

        if ($saveRequest) {

            $req_pk = $this->db->query("SELECT 'REP-' || LPAD(NVL(MT.REPAIRE_MT_ID, 0), 6, '0')   getid FROM LZW_REPAIRE_MT MT WHERE MT.REPAIRE_MT_ID = $req_pk_id ");
            $req_pk = $req_pk->result_array();

            $get_req_id = $req_pk[0]['GETID'];

            if ($get_req_id) {
                $issues = array();
                try {
                    foreach (json_decode($issues_name) as $key => $issue) {

                        array_push($issues, $issue->id);
                    }
                } catch (Exception $e) {
                    print_r($e);
                }

                foreach ($issues as $key) {
                    $req_det_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZW_REPAIRE_DT','REPAIRE_DT_ID') RE_PK FROM DUAL");
                    $req_det_pk = $req_det_pk->result_array();
                    $req_det_pk_id = $req_det_pk[0]['RE_PK'];

                    $this->db->query("INSERT INTO LZW_REPAIRE_DT (REPAIRE_DT_ID, REPAIRE_ID, ISSUE_DT_ID)VALUES($req_det_pk_id,$req_pk_id, '$key' ) ");
                }
            }
            return array('get_req_id' => $get_req_id, 'exist' => true);
        } else {
            $get_req_id = '';
            return array('get_req_id' => $get_req_id, 'exist' => false);
        }
    }
    public function ljw_SaveRequestMobilePics()
    {

        $get_req_id = $this->input->post('get_req_id');
        $req_type = strtoupper(trim($this->input->post('req_type')));

        if ($req_type == 'REP') {

            $path_id = 9;

        } elseif ($req_type == 'REC') {
            $path_id = 8;

        }

        if (isset($_FILES['image'])) {
            $images = $_FILES['image'];

            if ($get_req_id) {
                $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = '$path_id'");
                $specific_qry = $query->result_array();
                $specific_path = $specific_qry[0]['MASTER_PATH'];
                // $specific_path = "D:/wamp/www/item_pictures/repar_pics/";
                $main_dir = $specific_path . $get_req_id;
                if (is_dir($main_dir) === false) {
                    mkdir($main_dir);
                }
                $config['upload_path'] = $main_dir;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['max_size'] = '0'; // max_size in kb
                $config['remove_spaces'] = true;
                $config['file_name'] = $_FILES['image']['name'];

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $error = array('error' => $this->upload->display_errors());
                    // print_r($error);
                    return array('get_req_id' => $get_req_id, 'exist' => false, 'message' => $error);
                } else {
                    return array('get_req_id' => $get_req_id, 'exist' => true, 'message' => 'Successfully uploaded!');
                }

            }
        } else {
            return array('get_req_id' => $get_req_id, 'exist' => false, 'message' => 'You did not select a file to upload');
        } // pic code check against null
    }
    public function save_image_url()
    {
        $this->load->library('image_lib');
        $id = $this->input->post('id');
        $type = $this->input->post('type');
        if (isset($_FILES['image'])) {
            $image = $_FILES['image'];
            if ($id) {
                $server_ip = 'http://' . $_SERVER['SERVER_NAME'] . '/';
                if ($type == 'brand') {
                    $check = $this->db->query("SELECT * from lzw_brands_mt  where BRAND_ID = $id")->row();

                    $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 10");
                    $specific_qry = $query->result_array();
                    $specific_path = $specific_qry[0]['MASTER_PATH'];
                    // $specific_path = "D:/wamp/www/item_pictures/brands/";
                    $main_dir = $specific_path;
                    if (is_dir($main_dir) === false) {
                        mkdir($main_dir);
                    }
                    $config['upload_path'] = $main_dir;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    $config['max_size'] = '0'; // max_size in kb
                    $config['remove_spaces'] = true;
                    $file_name = $_FILES['image']['name'];
                    $config['file_name'] = $_FILES['image']['name'];
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('image')) {
                        $error = array('error' => $this->upload->display_errors());
                        return array('id' => $id, 'exist' => false, 'message' => $error);
                    } else {
                        $image_data = $this->upload->data();
                        $configer = array(
                            'image_library' => 'GD2',
                            'source_image' => $image_data['full_path'],
                            'maintain_ratio' => true,
                            'width' => 100,
                            'height' => 100,
                        );
                        $file_name = 'item_pictures/brands/' . $image_data['file_name'];
                        $this->db->query("UPDATE lzw_brands_mt SET IMAGE_URL = '$file_name'  WHERE BRAND_ID = $id");
                        return array('id' => $id, 'exist' => true, 'message' => 'Successfully uploaded!');
                    }

                } else {
                    $check = $this->db->query("SELECT * from lzw_model_mt  where MODEL_ID = $id")->row();

                    $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 11");
                    $specific_qry = $query->result_array();
                    $specific_path = $specific_qry[0]['MASTER_PATH'];
                    // $specific_path = "D:/wamp/www/item_pictures/models/";
                    $main_dir = $specific_path;
                    if (is_dir($main_dir) === false) {
                        mkdir($main_dir);
                    }
                    $config['upload_path'] = $main_dir;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    $config['max_size'] = '0'; // max_size in kb
                    $config['remove_spaces'] = true;
                    $file_name = $_FILES['image']['name'];
                    $config['file_name'] = $_FILES['image']['name'];
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('image')) {
                        $error = array('error' => $this->upload->display_errors());
                        return array('id' => $id, 'exist' => false, 'message' => $error);
                    } else {
                        $image_data = $this->upload->data();

                        $configer = array(
                            'image_library' => 'GD2',
                            'source_image' => $image_data['full_path'],
                            'maintain_ratio' => true,
                            'width' => 100,
                            'height' => 100,
                        );
                        $this->image_lib->clear();
                        $this->image_lib->initialize($configer);
                        $this->image_lib->resize();

                        $file_name = 'item_pictures/models/' . $image_data['file_name'];
                        $this->db->query("UPDATE lzw_model_mt SET IMAGE_URL = '$file_name'  WHERE MODEL_ID = $id");
                        return array('id' => $id, 'exist' => true, 'message' => 'Successfully uploaded!');
                    }

                }
            }
        } else {
            return array('id' => $id, 'exist' => false, 'message' => 'You did not select a file to upload');
        }
    }
    public function ljw_saveBuySell()
    {

        $product_name = trim($this->input->post('product_name'));
        $brand_name = trim($this->input->post('brand_name'));
        $series_name = trim($this->input->post('series_name'));
        $model_name = trim($this->input->post('model_name'));
        $carrier_name = trim($this->input->post('carrier_name'));
        $storage_name = trim($this->input->post('storage_name'));
        $available_offer = trim($this->input->post('available_offer'));
        $gevin_offer = trim($this->input->post('gevin_offer'));
        $select_option = trim($this->input->post('select_option'));
        $payment_mode = trim($this->input->post('payment_mode'));
        $answer_ids = trim($this->input->post('answer_ids'));
        $paypalEmail = trim($this->input->post('paypalEmail'));

        $payable_to = trim($this->input->post('payable_to'));
        $address1 = trim($this->input->post('address1'));
        $address2 = trim($this->input->post('address2'));
        $cityName = trim($this->input->post('cityName'));
        $stateName = trim($this->input->post('stateName'));
        $zipCode = trim($this->input->post('zipCode'));
        $countryName = trim($this->input->post('countryName'));
        $phone_number = trim($this->input->post('phone_number'));
        $email_address = trim($this->input->post('email_address'));

        $pick_address = trim($this->input->post('pick_address'));
        $pick_area = trim($this->input->post('pick_area'));
        $pick_city = trim($this->input->post('pick_city'));
        $pick_state = trim($this->input->post('pick_state'));
        $pick_zipcode = trim($this->input->post('pick_zipcode'));

        $user_id = $this->input->post('user_id');

        if (!empty($user_id)) {

            $get_user = $user_id;
        } else {
            $get_user = 1;
        }

        $answer_ids = explode(",", $answer_ids);
        if ($payment_mode == 'paypal') {
            $payment_mode = 1;
        } else {
            $payment_mode = 2;
        }
        $trade_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lzw_trade_request','LZW_TRADE_ID') RE_PK FROM DUAL");
        $trade_pk = $trade_pk->result_array();
        $trade_pk_id_temp = $trade_pk[0]['RE_PK'];
        $saveRequest = $this->db->query("INSERT INTO
                lzw_trade_request (
                    LZW_TRADE_ID,
                    F_NAME,
                    L_NAME,
                    EMAIL,
                    CONTACT_NO,
                    OBJECT_ID,
                    AVAILABLE_OFFER,
                    ADDRESS,
                    BRAND_INPUT,
                    SERIES_INPUT,
                    MODAL_INPUT,
                    STORAGE_INPUT,
                    CARRIER_INPUT,
                    GIVEN_OFFER,
                    SELECT_OPTION,
                    PAYMENT_MODE,
                    INSERT_DATE,
                    USER_ID
                     )
                VALUES(
                    $trade_pk_id_temp,
                    'null',
                    'null',
                    'null',
                    'null',
                    '$product_name',
                    '$gevin_offer',
                    'null',
                    '$brand_name',
                    '$series_name',
                    '$model_name',
                    '$storage_name',
                    '$carrier_name',
                    '$available_offer',
                    '$select_option',
                    '$payment_mode',
                    sysdate,
                    '$get_user') ");

        if ($saveRequest) {
            $req_pk = $this->db->query("SELECT 'SELL-' || LPAD(NVL(MT.LZW_TRADE_ID, 0), 6, '0')   getid FROM lzw_trade_request MT WHERE MT.LZW_TRADE_ID = $trade_pk_id_temp");
            $req_pk = $req_pk->result_array();
            $trade_pk_id = $req_pk[0]['GETID'];
            foreach ($answer_ids as $key => $value) {
                $given_ans_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lzw_given_ansers','GIVEN_ANSERS_ID') RE_PK FROM DUAL");
                $given_ans_pk = $given_ans_pk->result_array();
                $given_ans_pk_id = $given_ans_pk[0]['RE_PK'];
                if ($given_ans_pk_id) {
                    $this->db->query("INSERT INTO lzw_given_ansers (
                         GIVEN_ANSERS_ID, ANSWERS_ID, LZW_TRADE_ID)VALUES($given_ans_pk_id,$value,'$trade_pk_id_temp' ) ");
                }
            }
            if ($select_option == 1) {
                $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lzw_dropoffs', 'ID')ID FROM DUAL")->result_array();
                $pk = $query[0]['ID'];
                $stat_query = $this->db->query("INSERT INTO
                    lzw_dropoffs(ID,  REQUEST_ID, CREATED_AT, UPDATED_AT,REQUEST_TYPE)
                        values
                            ($pk,
                            '$trade_pk_id',
                            sysdate,
                            sysdate,
                            '2')");
            } elseif ($select_option == 2) {
                $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lzw_pickups', 'PICK_ID')ID FROM DUAL")->result_array();
                $pk = $query[0]['ID'];
                $stat_query = $this->db->query("INSERT INTO
                        lzw_pickups(PICK_ID,REPAIRE_ID,STREET,CITY,STATE,ZIP,CREATED_AT, UPDATED_AT,REQUEST_TYPE)
                        values
                            ($pk,
                            '$trade_pk_id',
                            '$pick_address',
                            '$pick_city',
                            '$pick_state',
                            '$pick_zipcode',
                            sysdate,
                            sysdate,
                            '2')");
            }
            $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lzw_payments', 'LZW_PAYMENT_ID')ID FROM DUAL")->result_array();
            $pk = $query[0]['ID'];
            $stat_query = $this->db->query("INSERT INTO
                        lzw_payments(
                            LZW_PAYMENT_ID,LZW_TRADE_ID,
                            PAYMENT_MODE,PAYPAL_EMAIL_ADDRESSS,
                            PAYABLE_TO,ADDRESS1,ADDRESS2,
                            CITY,STATE,ZIPCODE,PHONE_NUMBER,EMAIL_ADDRESS)
                        values
                            ($pk,
                            '$trade_pk_id_temp',
                            '$payment_mode',
                            '$paypalEmail',
                            '$payable_to',
                            '$address1',
                            '$address2',
                            '$cityName',
                            '$stateName',
                            '$zipCode',
                            '$phone_number',
                            '$email_address'
                            )");
            return array('trade_pk_id' => $trade_pk_id, 'exist' => true);
        } else {
            $trade_pk_id = '';
            return array('trade_pk_id' => $trade_pk_id, 'exist' => false);
        }
    }
    public function ljw_saveBuySellTech()
    {

        $answer_ids = trim($this->input->post('answer_ids'));
        $trade_id_return = trim($this->input->post('trade_id'));
        $trade_id = trim($this->input->post('trade_id'));

        $remarks = trim($this->input->post('remarks'));
        $answer_ids = explode(",", $answer_ids);

        $result = $this->db->query("SELECT *
                FROM (SELECT 'SELL-' || LPAD(NVL(RM.LZW_TRADE_ID, 0), 6, '0') trade_id,RM.LZW_TRADE_ID,
                            RM.object_id
                        FROM LZW_trade_request RM)
                WHERE trade_id = '$trade_id'")->row();
        $trade_id = $result->LZW_TRADE_ID;

        if ($trade_id) {

            $this->db->query("UPDATE LZW_trade_request SET REMARKS = '$remarks'  WHERE LZW_TRADE_ID = $trade_id");
            foreach ($answer_ids as $key => $value) {
                $tech = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lzw_tech_answers','TECH_ANSERS_ID') RE_PK FROM DUAL");
                $tech = $tech->result_array();
                $tech_id = $tech[0]['RE_PK'];
                if ($tech_id) {
                    $this->db->query("INSERT INTO lzw_tech_answers (
                         TECH_ANSERS_ID, ANSWERS_ID, LZW_TRADE_ID)VALUES($tech_id,$value,'$trade_id' ) ");
                }
            }
            return array('trade_id' => $trade_id_return, 'exist' => true);
        } else {
            $trade_pk_id = '';
            return array('trade_id' => $trade_id_return, 'exist' => false);
        }
    }

    public function ljw_rep_sell_data()
    {

        $searcInput = $this->input->post('searcInput');
        $searcInput = strtoupper(trim(str_replace("  ", ' ', $searcInput)));
        $user_id = $this->input->post('user_id');

        $rep_sell_data = $this->db->query("SELECT *
  FROM (SELECT *
          FROM (SELECT 'REP-' || LPAD(NVL(RM.REPAIRE_MT_ID, 0), 6, '0') REQ_ID,
                       'REP' DATA_SOURCE,
                      '' img_url,
                      '9' PATH_ID ,
                       DECODE((SELECT MOM.DESCRIPTION
                                FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT
                               WHERE MOM.MODEL_ID = MDT.MODEL_ID
                                 AND TO_CHAR(MDT.MODEL_DT_ID) =
                                     RM.MODAL_INPUT),
                              NULL,
                              RM.MODAL_INPUT,
                              (SELECT MOM.DESCRIPTION
                                 FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT
                                WHERE MOM.MODEL_ID = MDT.MODEL_ID
                                  AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME,
                       NVL(OFFER, 0) OFFER,
                        CASE
                         WHEN RM.SELECT_OPTION IS NOT NULL THEN
                          'APPROVED'
                         WHEN NVL(OFFER, 0) >= 1 THEN
                          'PROCESSED'
                         ELSE
                          'NEW'
                       END STATUS,
                       RM.CUSTOMER_REMARKS REMARKS
                  FROM LZW_REPAIRE_MT RM
                  where rm.USER_ID = '$user_id'
                 ORDER BY REPAIRE_MT_ID DESC)
        UNION ALL
        SELECT *
          FROM (SELECT 'SELL-' || LPAD(NVL(RM.LZW_TRADE_ID, 0), 6, '0') REQ_ID,
                       'SELL' DATA_SOURCE,
                       (select m.image_url
                  from lzw_model_mt m
                 where m.model_id in
                       (select dt.model_id
                          from lzw_model_dt dt
                         where to_char(dt.model_dt_id) = rm.modal_input)) img_url,

                        '' PATH_ID ,
                       DECODE((SELECT MOM.DESCRIPTION
                                FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT
                               WHERE MOM.MODEL_ID = MDT.MODEL_ID
                                 AND TO_CHAR(MDT.MODEL_DT_ID) =
                                     RM.MODAL_INPUT),
                              NULL,
                              RM.MODAL_INPUT,
                              (SELECT MOM.DESCRIPTION
                                 FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT
                                WHERE MOM.MODEL_ID = MDT.MODEL_ID
                                  AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME,
                       NVL(GIVEN_OFFER, 0) GIVEN_OFFER,
                      CASE
                         WHEN RM.SELECT_OPTION IS NOT NULL THEN
                          'APPROVED'
                         WHEN NVL(GIVEN_OFFER, 0) >= 1 THEN
                          'PROCESSED'
                         ELSE
                          'NEW'
                       END STATUS,
                       RM.REMARKS REMARKS
                  FROM LZW_TRADE_REQUEST RM
                  where rm.USER_ID = '$user_id'
                 ORDER BY LZW_TRADE_ID DESC)
        UNION ALL
        SELECT *
          FROM (SELECT 'REC-' || LPAD(NVL(R.REC_ID, 0), 6, '0') REQ_ID,
                       'REC' DATA_SOURCE,
                        '' img_url,
                        '8' PATH_ID ,
                       R.REC_FIRST_NAME MODEL_NAME,
                       NVL(R.GIVEN_OFFER, 0) GIVEN_OFFER,
                       CASE
                         WHEN NVL(GIVEN_OFFER, 0) >= 1 THEN
                          'PROCESSED'
                         ELSE
                          'NEW'
                       END STATUS,
                       R.REC_REMARKS REMARKS
                  FROM LZW_REC_PICKUP R
                   where r.USER_ID = '$user_id'
                  ))
        WHERE UPPER(DATA_SOURCE) = '$searcInput'")->result_array();

        if (count($rep_sell_data) > 0) {
            foreach ($rep_sell_data as $key => $value) {

                if (!empty($value['IMG_URL'])) {

                    $rep_sell_data[$key]['IMAGE_URL_FULL'] = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $value['IMG_URL'];
                } else {

                    $uri = $this->get_rep_sell_single_pics($value['REQ_ID'], $value['PATH_ID']);
                    $image = $uri['uri'][0];
                    $rep_sell_data[$key]['IMAGE_URL_FULL'] = $image;
                }
                //var_dump($image);
                // break;
                //$details[0]['IMAGES'] = $uri['uri'];
            }
            return $rep_sell_data; //array('rep_sell_data' => $rep_sell_data);

        } else {
            return $rep_sell_data;
        }

    }
    public function update_tag_remarks()
    {
        $remarks = $this->input->post('tag_remarks');
        $user_id = $this->input->post('user_id');
        $tag_id = $this->input->post('tag_id');
        $barcode = trim($this->input->post('barcode'), " ");

        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = str_replace(array("`,'"), "", $remarks);
        $remarks = str_replace(array("'"), "''", $remarks);

        $get_barcode = $this->db->query("select * from lz_barcode_mt b where b.barcode_no = '$barcode'")->row();
        if ($get_barcode) {
            $item_id = $get_barcode->ITEM_ID;
            $manifest_id = $get_barcode->LZ_MANIFEST_ID;
            $condition_id = $get_barcode->CONDITION_ID;

            $list_qty = $this->db->query("SELECT COUNT(1) QTY
            FROM LZ_BARCODE_MT BC
           WHERE BC.CONDITION_ID IS NOT NULL
             AND BC.LZ_MANIFEST_ID = $manifest_id
             AND BC.ITEM_ID = $item_id
             AND BC.CONDITION_ID = $condition_id
           GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID");

            if ($list_qty->num_rows() > 1) {
                $qry = $this->db->query("UPDATE lz_merchant_barcode_dt d set d.tag_id ='$tag_id' , d.tag_remarks = '$remarks', d.tag_date = sysdate ,d.tag_by = '$user_id' where d.barcode_no in (
                    select b.barcode_no from lz_barcode_mt b where b.item_id ='$item_id' and b.lz_manifest_id = '$manifest_id' and b.condition_id = '$condition_id'
                    and b.order_id is null
                    and b.extendedorderid is null
                    and b.sale_record_no is null
                )");

            } else {

                $qry = $this->db->query("UPDATE lz_merchant_barcode_dt d set d.tag_id ='$tag_id' , d.tag_remarks = '$remarks', d.tag_date = sysdate ,d.tag_by = '$user_id' where d.barcode_no in ('$barcode' )");

            }
            if ($qry == true) {
                return array("status" => true, 'message' => "Tag Remarks Updated Successfully");
            } else {
                return array("status" => false, 'message' => "Tag Remarks Not Updated Successfully");
            }
        } else {
            $qry = $this->db->query("UPDATE lz_merchant_barcode_dt d set d.tag_id ='$tag_id' , d.tag_remarks = '$remarks', d.tag_date = sysdate ,d.tag_by = '$user_id' where d.barcode_no in ('$barcode' )");
            if ($qry == true) {
                return array("status" => true, 'message' => "Tag Remarks Updated Successfully");
            } else {
                return array("status" => false, 'message' => "Tag Remarks Not Updated Successfully");
            }
        }

    }

    public function ljw_rep_sell_counts()
    {

        $user_id = $this->input->post('user_id');

        $rep_count = $this->db->query("SELECT COUNT(*) REPAIR FROM (SELECT 'REP-' || LPAD(NVL(RM.REPAIRE_MT_ID, 0), 6, '0') REQ_ID, 'REP' DATA_SOURCE, DECODE((SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT), NULL, RM.MODAL_INPUT, (SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME, NVL(OFFER, 0) OFFER, CASE WHEN NVL(OFFER, 0) >= 1 THEN 'APPROVED'ELSE 'PENDING'END STATUS FROM LZW_REPAIRE_MT RM where rm.user_id ='$user_id' ORDER BY REPAIRE_MT_ID DESC)")->result_array();

        $sell_count = $this->db->query("SELECT count(*) sell FROM (SELECT 'SELL-' || LPAD(NVL(RM.LZW_TRADE_ID, 0), 6, '0') REQ_ID, 'SELL' DATA_SOURCE, DECODE((SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT), NULL, RM.MODAL_INPUT, (SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME, NVL(GIVEN_OFFER, 0) GIVEN_OFFER, CASE WHEN NVL(GIVEN_OFFER, 0) >= 1 THEN 'APPROVED'ELSE 'PENDING'END STATUS FROM LZW_TRADE_REQUEST RM where rm.user_id ='$user_id' ORDER BY LZW_TRADE_ID DESC)")->result_array();

        $rec_count = $this->db->query("SELECT count(*) rec FROM lzw_rec_pickup where user_id ='$user_id'")->result_array();

        return array('rep_count' => $rep_count[0]["REPAIR"], 'sell_count' => $sell_count[0]["SELL"], 'rec_count' => $rec_count[0]["REC"]);

    }

    public function get_repairs()
    {

        $searcInput = $this->input->post('searcInput');

        $searcInput = strtoupper(trim(str_replace("  ", ' ', $searcInput)));
        $searcInput = str_replace(array("`,'"), "", $searcInput);
        $searcInput = str_replace(array("'"), "''", $searcInput);

        $get_data = $this->db->query("SELECT * FROM (SELECT 'REP-' || LPAD(NVL(RM.REPAIRE_MT_ID, 0), 6, '0') REPAIR_ID, RM.REPAIRE_MT_ID, RM.F_NAME || ' ' || RM.L_NAME F_NAME, RM.L_NAME, RM.EMAIL, RM.CONTACT_NO, RM.CUSTOMER_REMARKS, (SELECT OM.OBJECT_NAME FROM LZW_OBJECT_MT OM WHERE OM.OBJECT_ID = RM.OBJECT_ID) PRODUCT_NAME, /*(SELECT BMM.DESCRIPTION FROM LZW_BRANDS_DT BT, LZW_BRANDS_MT BMM WHERE BMM.BRAND_ID = BT.BRAND_ID AND TO_CHAR(BT.BRAND_DT_ID) = RM.BRAND_INPUT) BRAND_NAME,*/ DECODE((SELECT BMM.DESCRIPTION FROM LZW_BRANDS_DT BT, LZW_BRANDS_MT BMM WHERE BMM.BRAND_ID = BT.BRAND_ID AND TO_CHAR(BT.BRAND_DT_ID) = RM.BRAND_INPUT), NULL, RM.BRAND_INPUT, (SELECT BMM.DESCRIPTION FROM LZW_BRANDS_DT BT, LZW_BRANDS_MT BMM WHERE BMM.BRAND_ID = BT.BRAND_ID AND TO_CHAR(BT.BRAND_DT_ID) = RM.BRAND_INPUT)) BRAND_NAME, /* (SELECT SM.DESCRIPTION FROM LZW_SERIES_MT SM, LZW_SERIES_DT ST WHERE SM.SERIES_ID = ST.SERIES_ID AND TO_CHAR(ST.SERIES_DT_ID) = RM.SERIES_INPUT) SERIES_NAME,*/ DECODE((SELECT SM.DESCRIPTION FROM LZW_SERIES_MT SM, LZW_SERIES_DT ST WHERE SM.SERIES_ID = ST.SERIES_ID AND TO_CHAR(ST.SERIES_DT_ID) = RM.SERIES_INPUT), NULL, RM.SERIES_INPUT, (SELECT SM.DESCRIPTION FROM LZW_SERIES_MT SM, LZW_SERIES_DT ST WHERE SM.SERIES_ID = ST.SERIES_ID AND TO_CHAR(ST.SERIES_DT_ID) = RM.SERIES_INPUT)) SERIES_NAME, /*(SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT) MODEL_NAME,*/ DECODE((SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT), NULL, RM.MODAL_INPUT, (SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME, NVL(OFFER, 0) OFFER, CASE WHEN nvl(OFFER,0) >= 1  then 'Approved' else 'Pending' end status FROM LZW_REPAIRE_MT RM ORDER BY REPAIRE_MT_ID DESC) where REPAIR_ID='$searcInput' ")->result_array();if (count($get_data) > 0) {

            return array('get_data' => $get_data, 'exist' => true);

        } else {
            return array('get_data' => $get_data, 'exist' => false);
        }

    }

    public function saveOptionData()
    {

        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $area = $this->input->post('area');
        $state = $this->input->post('state');
        $zipcode = $this->input->post('zipcode');
        //$getRepareId = TRIM($this->input->post('getRepareId');
        $getRepareId = strtoupper(trim(str_replace("  ", ' ', $this->input->post('getRepareId'))));

        $getradio = $this->input->post('getradio');

        $save = $this->db->query(" UPDATE LZW_REPAIRE_MT SET SELECT_OPTION ='$getradio'  where REPAIRE_MT_ID = (SELECT REP_ID
          FROM (SELECT 'REP-' || LPAD(NVL(RM.REPAIRE_MT_ID, 0), 6, '0') REPAIR_ID,RM.REPAIRE_MT_ID REP_ID
                  FROM LZW_REPAIRE_MT RM) where UPPER(REPAIR_ID)='$getRepareId' )");
        if ($save) {
            $check = $this->db->query("SELECT * FROM lzw_pickups WHERE UPPER(REPAIRE_ID)=UPPER('$getRepareId') ")->result_array();
            if (!$check) {
                $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lzw_pickups', 'PICK_ID')ID FROM DUAL")->result_array();
                $pk = $query[0]['ID'];
                $stat_query = $this->db->query("INSERT INTO
            lzw_pickups(PICK_ID,REPAIRE_ID,STREET,CITY,STATE,ZIP,CREATED_AT, UPDATED_AT,REQUEST_TYPE)
                values
                    ($pk,
                    '$getRepareId',
                    '$address',
                    '$city',
                    '$state',
                    '$zipcode',
                    sysdate,
                    sysdate,
                    '1')");
                if ($stat_query) {
                    return array(
                        "save" => true,
                        "message" => "Successfully Save !");
                } else {
                    return array(
                        "save" => false,
                        "message" => "Updated but no save !");
                }
            } else {
                return array(
                    "save" => true,
                    "message" => "You have already selected this Option!");
            }

        } else {
            return array(
                "save" => false,
                "message" => "Something went wrong !");
        }

    }

// for usamn android app

    public function lz_recyle_form()
    {
        $full_name = html_escape(trim($this->input->post('full_name')));

        $email = html_escape(trim($this->input->post('email')));
        $phone = html_escape(trim($this->input->post('phone')));
        $remarks = html_escape(trim($this->input->post('remarks')));
        $user_id = $this->input->post('user_id');

        if (!empty($user_id)) {

            $get_user = $user_id;
        } else {
            $get_user = 1;
        }

        $images = $_FILES['images'];

        $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lzw_rec_pickup', 'REC_ID')ID FROM DUAL")->result_array();
        $pk = $query[0]['ID'];

        $stat_query = $this->db->query("INSERT INTO
            lzw_rec_pickup(REC_ID,  REC_FIRST_NAME, REC_LAST_NAME, REC_EMAIL, REC_PHONE, REC_REMARKS,REC_INSERTED_DATE,USER_ID)
                values
                    ($pk,
                    '$full_name',
                    '$full_name',
                    '$email',
                    '$phone',
                    '$remarks',
                    sysdate,
                    '$get_user'
                    )");
        if ($stat_query) {
            $req_pk = $this->db->query("SELECT 'REC-' || LPAD(NVL(MT.REC_ID, 0), 6, '0')   getid FROM lzw_rec_pickup MT WHERE MT.REC_ID = $pk");
            $req_pk = $req_pk->result_array();
            $get_req_id = $req_pk[0]['GETID'];

            $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 8");
            $specific_qry = $query->result_array();
            $specific_path = $specific_qry[0]['MASTER_PATH'];
            // $specific_path = "D:/wamp/www/item_pictures/recycle_pics/";
            if (isset($_FILES['images'])) {
                $main_dir = $specific_path . $get_req_id;
                if (is_dir($main_dir) === false) {
                    mkdir($main_dir);
                }
                foreach ($images['name'] as $key => $value) {
                    $_FILES['file']['name'] = $_FILES['images']['name'][$key];
                    $_FILES['file']['type'] = $_FILES['images']['type'][$key];
                    $_FILES['file']['tmp_name'] = $_FILES['images']['tmp_name'][$key];
                    $_FILES['file']['error'] = $_FILES['images']['error'][$key];
                    $_FILES['file']['size'] = $_FILES['images']['size'][$key];
                    $config['upload_path'] = $main_dir;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    $config['max_size'] = '10000'; // max_size in kb
                    $new_name = $pk . '_recycle';
                    $config['file_name'] = $new_name;
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('file');
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function get_rep_rec_detail()
    {

        $searcInput = trim($this->input->post('searcInput'));
        $user_id = trim($this->input->post('user_id'));

        // $details = $this->db->query("SELECT * FROM (SELECT * FROM (SELECT 'REP-' || LPAD(NVL(RM.REPAIRE_MT_ID, 0), 6, '0') REQ_ID, 'REP' DATA_SOURCE, DECODE((SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT), NULL, RM.MODAL_INPUT, (SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME, NVL(OFFER, 0) OFFER, CASE WHEN NVL(OFFER, 0) >= 1 THEN 'APPROVED'ELSE 'PENDING'END STATUS, RM.CUSTOMER_REMARKS REMARKS, '9' PATH_ID, decode(rm.select_option,1,'Drop of',2,'Pic Up',3,'Shipment','-') select_option FROM LZW_REPAIRE_MT RM where rm.user_id ='$user_id' ORDER BY REPAIRE_MT_ID DESC) UNION ALL SELECT * FROM (SELECT 'SELL-' || LPAD(NVL(RM.LZW_TRADE_ID, 0), 6, '0') REQ_ID, 'SELL' DATA_SOURCE, DECODE((SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT), NULL, RM.MODAL_INPUT, (SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME, NVL(GIVEN_OFFER, 0) GIVEN_OFFER, CASE WHEN NVL(GIVEN_OFFER, 0) >= 1 THEN 'APPROVED'ELSE 'PENDING'END STATUS, RM.REMARKS REMARKS, '' PATH_ID ,decode(rm.select_option,1,'Drop of',2,'Pic Up',3,'Shipment','-') select_option FROM LZW_TRADE_REQUEST RM where rm.user_id ='$user_id' ORDER BY LZW_TRADE_ID DESC) UNION ALL SELECT 'REC-' || LPAD(NVL(R.REC_ID, 0), 6, '0') REQ_ID, 'REC' DATA_SOURCE, R.REC_FIRST_NAME MODEL_NAME, R.GIVEN_OFFER, '' STATUS, R.REC_REMARKS REMARKS, '8' PATH_ID,'-' select_option FROM LZW_REC_PICKUP R where r.user_id ='$user_id') WHERE UPPER(REQ_ID) = '$searcInput' ")->result_array();

        $details = $this->db->query("SELECT Q1.*, Q2.* FROM (SELECT * FROM (SELECT TO_CHAR(RM.SELECT_OPTION) SELECTED, 'REP-' || LPAD(NVL(RM.REPAIRE_MT_ID, 0), 6, '0') REQ_ID, 'REP' DATA_SOURCE, DECODE((SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT), NULL, RM.MODAL_INPUT, (SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME, NVL(OFFER, 0) OFFER, CASE WHEN NVL(OFFER, 0) >= 1 THEN 'APPROVED'ELSE 'PENDING'END STATUS, RM.CUSTOMER_REMARKS REMARKS, '9' PATH_ID, DECODE(RM.SELECT_OPTION, 1, 'DROP OF', 2, 'PIC UP', 3, 'SHIPMENT', '-') SELECT_OPTION FROM LZW_REPAIRE_MT RM WHERE RM.USER_ID = '$user_id' ORDER BY REPAIRE_MT_ID DESC) UNION ALL SELECT * FROM (SELECT TO_CHAR(RM.SELECT_OPTION) SELECTED, 'SELL-' || LPAD(NVL(RM.LZW_TRADE_ID, 0), 6, '0') REQ_ID, 'SELL' DATA_SOURCE, DECODE((SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT), NULL, RM.MODAL_INPUT, (SELECT MOM.DESCRIPTION FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT WHERE MOM.MODEL_ID = MDT.MODEL_ID AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME, NVL(GIVEN_OFFER, 0) GIVEN_OFFER, CASE WHEN NVL(GIVEN_OFFER, 0) >= 1 THEN 'APPROVED'ELSE 'PENDING'END STATUS, RM.REMARKS REMARKS, '' PATH_ID, DECODE(RM.SELECT_OPTION, 1, 'DROP OFF', 2, 'PIC UP', 3, 'SHIPMENT', '-') SELECT_OPTION FROM LZW_TRADE_REQUEST RM WHERE RM.USER_ID = '$user_id' ORDER BY LZW_TRADE_ID DESC) UNION ALL SELECT '' SELECTED, 'REC-' || LPAD(NVL(R.REC_ID, 0), 6, '0') REQ_ID, 'REC' DATA_SOURCE, R.REC_FIRST_NAME MODEL_NAME, R.GIVEN_OFFER, '' STATUS, R.REC_REMARKS REMARKS, '8' PATH_ID, '-' SELECT_OPTION FROM LZW_REC_PICKUP R WHERE R.USER_ID = '$user_id' ) Q1, (SELECT '1' OPTIONS, 'DROP' TYPE, D.REQUEST_ID ID, D.CARRRIER_NAME CARIER_NAME, D.TRAKING_NUMBER TRACK_NUMB, '' URL, '' ADDRESS FROM LZW_DROPOFFS D UNION ALL SELECT '2' OPTIONS, 'PIC UP' TYPE, P.REPAIRE_ID ID, '' CARIER_NAME, '' TRACK_NUMB, '' URL, P.STREET || ' ' || P.CITY || ',' || P.STATE || ' ' || P.ZIP ADDRESS FROM LZW_PICKUPS P UNION ALL SELECT '3' OPTIONS, 'SHIP' TYPE, SH.SHIPPABLE_ID ID, '' CARIER_NAME, SH.TRACKING_CODE TRACK_NUMB, SH.LABEL_REMOTE_URL URL, '' ADDRESS FROM STS_STAGE_DUMMY_SHIPMENTS SH) Q2 WHERE UPPER(REQ_ID) = '$searcInput'AND Q1.REQ_ID = Q2.ID(+) AND Q1.SELECTED = Q2.OPTIONS(+)
            ")->result_array();
        if (count($details) >= 1) {
            // $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();

            $path_id = $details[0]['PATH_ID'];
            if ($path_id == null || $path_id == "" || $path_id == 'undefined') {
                $images = array();
            } else {
                $uri = $this->get_rep_sell_pics($details);
                $images = $uri['uri'];
            }

            return array('details' => $details, "images" => $images, 'exist' => true);
        } else {
            $images = '';
            return array('details' => $details, "images" => $images, 'exist' => false);
        }
    }

    public function get_rep_rec_detail_mobile()
    {

        $data = $this->get_rep_rec_detail();

        if ($data["exist"] == true) {

            $details = $data['details'][0];
            $details['IMAGES'] = $data['images'];
            return $details;
        } else {
            header('HTTP/1.1 400 Unprocessable Entity', true, 400);
            $data["message"] = "Record not found";
            return $data;
        }
    }

    public function get_rep_sell_pics($req_id)
    {

        $path_id = $req_id[0]['PATH_ID'];

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = $path_id");
        $path = $path->result_array();

        // $master_path = "D:/wamp/www/item_pictures/repair_pics/";
        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';

        foreach ($req_id as $req_id) {
            // print_r($req_id);
            $get_id = $req_id['REQ_ID'];
            $dir = "";
            $barcodePictures = $master_path . $get_id . "/";

            if (is_dir($barcodePictures)) {
                $dir = $barcodePictures;
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);

            if (is_dir($dir)) {
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

                if ($images) {
                    $j = 0;
                    foreach ($images as $image) {

                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);

                        $uri[$j] = $base_url . $withoutMasterPartUri;
                        $uri[$j] = str_replace("\\", "", $uri[$j]);
                        // if($uri[$barcode['LZ_barcode_ID']]){
                        //     break;
                        // }

                        $j++;
                    }
                } else {
                    $uri = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    $uri = array();
                }
            } else {
                $uri[0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";

            }
        }
        return array('uri' => $uri);

    }

    public function get_rep_sell_single_pics($req_id, $path_id)
    {
        //var_dump('asd');

        $path_id = $path_id; //$req_id[0]['PATH_ID'];

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = $path_id");
        $path = $path->result_array();

        // $master_path = "D:/wamp/www/item_pictures/repair_pics/";
        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';

        //foreach ($req_id as $req_id) {
        // print_r($req_id);
        $get_id = $req_id; //$req_id['REQ_ID'];
        $dir = "";
        $barcodePictures = $master_path . $get_id . "/";

        if (is_dir($barcodePictures)) {
            $dir = $barcodePictures;
        }

        $dir = preg_replace("/[\r\n]*/", "", $dir);

        if (is_dir($dir)) {
            $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

            if ($images) {
                $j = 0;
                foreach ($images as $image) {

                    $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                    $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);

                    $uri[$j] = $base_url . $withoutMasterPartUri;
                    $uri[$j] = str_replace("\\", "", $uri[$j]);
                    // if($uri[$barcode['LZ_barcode_ID']]){
                    //     break;
                    // }

                    $j++;
                }
            } else {
                $uri = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                $uri = false;
            }
        } else {
            $uri[0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";

        }
        //}
        return array('uri' => $uri);

    }

    public function update_carrier_track()
    {

        $req_id = trim($this->input->post('req_id'));
        $carier = trim($this->input->post('carier'));
        $trac_num = trim($this->input->post('trac_num'));

        $update = $this->db->query("UPDATE LZW_DROPOFFS D SET D.CARRRIER_NAME ='$carier' ,D.TRAKING_NUMBER ='$trac_num' WHERE D.REQUEST_ID ='$req_id' ");
        if ($update) {
            return true;
        } else {
            return false;
        }
    }
    public function lzw_hot_items()
    {
        $data = $this->db->query("SELECT * FROM lzw_hot_product_mt")->result_array();
        if ($data) {
            return $data;
        } else {

            return array();
        }
    }

    public function lzw_cancel_req()
    {

        $req_id = trim($this->input->post('trade_id'));

        $result = $this->db->query("

                SELECT *
                FROM (
                        SELECT 'SELL-' || LPAD(NVL(RM.LZW_TRADE_ID, 0), 6, '0') trade_id, RM.LZW_TRADE_ID pk,'SELL' DATA_SOURCE
                          FROM LZW_trade_request RM
                          union all
                          SELECT 'REP-' || LPAD(NVL(sm.REPAIRE_MT_ID, 0), 6, '0') trade_id,sm.repaire_mt_id pk,'REP' DATA_SOURCE
                        FROM lzw_repaire_mt sm

                       )
                WHERE trade_id = '$req_id'")->result_array();
        $trade_id = $result[0]['PK'];
        $data_source = $result[0]['DATA_SOURCE'];
        // 1 for approve 2 for cancel reuest

        if ($data_source === 'SELL') {

            $canc_query = $this->db->query("UPDATE LZW_trade_request SET STATUS = '2'  WHERE LZW_TRADE_ID = '$trade_id'");

        } else if ($data_source === 'REP') {

            $canc_query = $this->db->query("UPDATE lzw_repaire_mt SET STATUS = '2'  WHERE REPAIRE_MT_ID = '$trade_id'");
        }

        if ($canc_query) {
            return true;
        } else {
            return false;
        }

    }

    public function gene_month_invoice()
    {
        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $serviceIds = $this->input->post("serId");

        foreach ($serviceIds as $serviceRateId) {

            $query = $this->db->query("call  pro_generate_monthly_inovice($serviceRateId,$merchId,TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS'),TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS'))");

            //$i++;
        }

        if ($query) {
            return array('true' => true, 'message' => 'invoice Generated Successfully');
        } else {
            return array('false' => false, 'message' => 'invoice Not Generated');

        }
        //return array('true' => true , 'message' => 'invoice Generated Successfully');

    }

    public function serch_invoice_monthly()
    {

        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");

        // var_dump($merchId);
        // exit;

        //$idntiti_data .= "AND D.ALLOCATE_DATE  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        $pic_inv = "SELECT PICTURES_DONE,
       UNIQUE_PICS,
       rate,
       REMAINING_PICS,
       excess_qty_rate,
       UNIQU_QTY_CHARG,
       REMAIN_QTY_CHARG,
       MONTH,
       MONTH_NUMB,
       NVL(UNIQU_QTY_CHARG + REMAIN_QTY_CHARG, 0) TOTAL
  FROM (SELECT COUNT(L.FOLDER_NAME) PICTURES_DONE,
  round(max(get_rates_pict.rate),2) rate,
  round(max(get_rates_pict.excess_qty_rate),2) excess_qty_rate,
               COUNT(DISTINCT L.FOLDER_NAME) UNIQUE_PICS,
               COUNT(L.FOLDER_NAME) - COUNT(DISTINCT L.FOLDER_NAME) REMAINING_PICS,
               COUNT(DISTINCT L.FOLDER_NAME) * round(max(get_rates_pict.rate),2) /* 0.90*/ UNIQU_QTY_CHARG,
               (COUNT(L.FOLDER_NAME) - COUNT(DISTINCT L.FOLDER_NAME)) * round(max(get_rates_pict.excess_qty_rate),2) /*0.10*/ REMAIN_QTY_CHARG,
               TO_CHAR(L.PIC_DATE_TIME, 'MONTH') MONTH,
               TO_CHAR(L.PIC_DATE_TIME,
                       'MONTH',
                       'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE''') MONTH_NUMB
          FROM LZ_MERCHANT_BARCODE_MT M,
               LZ_MERCHANT_BARCODE_DT D,
               LZ_SPECIAL_LOTS L,
               (select ms.merchant_id,
                       ms.ser_rate_id,
                       ms.rate,
                       ms.excess_qty_rate
                  from lj_merchant_service ms
                 where ms.merchant_id = '$merchId'
                   and ms.ser_rate_id = 1
                   and rownum<=1) get_rates_pict
         WHERE M.MT_ID = D.MT_ID
           and l.INVOICE_ID is null
           AND D.BARCODE_NO = L.BARCODE_PRV_NO
           and m.merchant_id = get_rates_pict.merchant_id(+) AND M.MERCHANT_ID = '$merchId'";
        $pic_inv .= " AND L.PIC_DATE_TIME BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";

        $pic_inv .= " GROUP BY TO_CHAR(L.PIC_DATE_TIME, 'MONTH'), TO_CHAR(L.PIC_DATE_TIME, 'MONTH', 'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE''')) ORDER BY MONTH_NUMB ASC ";

        $pic_inv = $this->db->query($pic_inv)->result_array();

        $listing_inv = "SELECT TOTAL_QTY_LIST,
       TOTAL_UNIQ_QTY,
       rate,
       REMAING_QTY,
       excess_qty_rate,
       TOTAL_UNIQ_QTY_CHARGE,
       REMAING_QTY_CHARGE,
       MONTH_NUMB,
       MONTH,
       NVL(TOTAL_UNIQ_QTY_CHARGE + REMAING_QTY_CHARGE, 0) TOTAL
  FROM (SELECT SUM(T.LIST_QTY) TOTAL_QTY_LIST,
    round(max(get_rates_pict.rate),2) rate,
  round(max(get_rates_pict.excess_qty_rate),2) excess_qty_rate,
               COUNT(DISTINCT T.EBAY_ITEM_ID) TOTAL_UNIQ_QTY,
               SUM(T.LIST_QTY) - COUNT(DISTINCT T.EBAY_ITEM_ID) REMAING_QTY,
               COUNT(DISTINCT T.EBAY_ITEM_ID) * round(max(get_rates_pict.rate),2)  TOTAL_UNIQ_QTY_CHARGE,
               (SUM(T.LIST_QTY) - COUNT(DISTINCT T.EBAY_ITEM_ID)) *  round(max(get_rates_pict.excess_qty_rate),2) REMAING_QTY_CHARGE,
               TO_CHAR(T.LIST_DATE, 'MONTH') MONTH,
               TO_CHAR(T.LIST_DATE,
                       'MONTH',
                       'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE''') MONTH_NUMB
          FROM EBAY_LIST_MT T, LJ_MERHCANT_ACC_DT DD, LZ_LISTED_ITEM_URL L,
          (select ms.merchant_id,
                       ms.ser_rate_id,
                       ms.rate,
                       ms.excess_qty_rate
                  from lj_merchant_service ms
                 where ms.merchant_id ='$merchId'
                   and ms.ser_rate_id = 2
                   and rownum<=1) get_rates_pict
         WHERE T.EBAY_ITEM_ID = L.EBAY_ID
           AND DD.ACCT_ID = T.LZ_SELLER_ACCT_ID(+)
           AND T.LZ_SELLER_ACCT_ID IS NOT NULL
           and t.invoice_id is null
           and dd.merchant_id = get_rates_pict.merchant_id(+)
           AND DD.MERCHANT_ID = '$merchId'
            AND T.STATUS IN ('ADD', 'REVISED')";

        $listing_inv .= " AND T.LIST_DATE BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";

        $listing_inv .= " GROUP BY TO_CHAR(T.LIST_DATE, 'MONTH'), TO_CHAR(T.LIST_DATE, 'MONTH', 'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE''')) ORDER BY MONTH_NUMB ASC ";
        $listing_inv = $this->db->query($listing_inv)->result_array();

        $order_inv = "SELECT sum(TOTAL_ORDER) TOTAL_ORDER,
       sum(SALE_PRI)SALE_PRI,
       sum(SHIPING_COST)SHIPING_COST,
       sum(PACKING_COST)PACKING_COST,
       sum(SERV_COST)SERV_COST,
       sum(EBAY_FEE)EBAY_FEE,
       sum(NVL(SHIPING_COST + PACKING_COST + SERV_COST + EBAY_FEE +
           MARKETPLACE_7,
           0)) TOTAL,
       sum(MARKETPLACE_7)MARKETPLACE_7/*,
       '' MONTH,
       '' MONTH_NUMB*/
  FROM (SELECT count(sd.ORDER_ID) TOTAL_ORDER,
               max(NVL(sD.SALE_PRICE, 0) * NVL(sD.Quantity, 0)) SALE_PRI,
               max(NVL(sd.shippinglabelrate, 0)) SHIPING_COST,
               max(NVL(GET_PACK_COST.PAC_COST, 0)) PACKING_COST,
               max(NVL(sD.SERVICE_COST, 0)) SERV_COST,
               decode(max(acount_Det.acount_name),
                      'dfwonline',
                      max(NVL(sd.ebay_fee_perc, 0)),
                      0) EBAY_FEE,
                 decode(max(acount_Det.acount_name),
                      'dfwonline',
                      ROUND(0.07 * NVL(max(NVL(sd.SALE_PRICE, 0) *
                                           NVL(sd.quantity, 0))- max(nvl(sd.refund_amount,0)) ,
                                       0),
                            2),
                      0) MARKETPLACE_7

               /*TO_CHAR(sd.SALE_DATE, 'MONTH') MONTH,
               TO_CHAR(sd.SALE_DATE,
                       'MONTH',
                       'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE''') MONTH_NUMB*/
          FROM LZ_SALESLOAD_DET sd,
               /*LJ_ORDER_PACKING_MT D,*/
               (SELECT SUM(D.PACKING_COST) PAC_COST,d.order_id
                  FROM LJ_ORDER_PACKING_DT D
                 GROUP BY D.order_id) GET_PACK_COST,
               (SELECT MM.EBAY_ITEM_ID,
                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME
                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
         WHERE sd.order_id in (select d.order_id
                                 from ebay_list_mt       e,
                                      lj_merhcant_acc_dt a,
                                      lz_salesload_det   d
                                where a.acct_id = e.lz_seller_acct_id
                                  and d.item_id = e.ebay_item_id
                                  and a.merchant_id = '$merchId'
                                group by d.order_id)
           /*and sd.order_id = d.order_id(+)*/
           and sd.order_id = GET_PACK_COST.order_id(+)/* D.ORDER_PACKING_ID = GET_PACK_COST.ORDER_PACKING_ID(+)*/
           and sd.orderstatus = 'Completed'
           and sd.return_id is null
           AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
           and sd.invoice_id is null";
        $order_inv .= " AND sd.SALE_DATE BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        $order_inv .= "  GROUP BY sd.order_id) /*GROUP BY TO_CHAR(sd.SALE_DATE, 'MONTH'), TO_CHAR(sd.SALE_DATE, 'MONTH', 'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE''')) ORDER BY MONTH_NUMB */  ";

        $order_inv = $this->db->query($order_inv)->result_array();

        $storage_charges = " select total_shipments,
               no_of_box,
               total_palets,
               total_cubic_Feet ||' cubic foot'  total_cubic_Feet, /* total Cubic Feet */
               palete_charg,
               bin_charge,
               total_palete_charge,
               total_bin,
               round(nvl(total_bin_charge + total_palete_charge, 0), 2) total_charges,
               month
          from (select count(sm.shipment_id) total_shipments,
                       sum(nvl(get_det.total_qty, 0)) no_of_box,
                       sum(nvl(get_det.total_palte, 0)) total_palets,
                       /*sum(get_det.lengt) || ' x ' || sum(get_det.widt) || ' x ' ||
                       sum(get_det.heigh)*/
                       round(sum(nvl(get_det.total_cubic_feet, 0)) , 2) total_cubic_Feet,
                       /*round(sum(nvl(get_det.total_cubic_feet, 0)), 2) total_cubic_Feet,*/
                       /* round(((sum(get_det.lengt) * sum(get_det.widt) *
                       sum(get_det.heigh)) / 1728 \*the total number of inches in a cubic foot*\
                       ) * max(nvl(get_det.palete_charg, 0)),
                       2) total_palete_charge,*/

                       round(sum(nvl(get_det.total_cubic_feet, 0)), 2) * max(nvl(get_det.palete_charg, 0)) total_palete_charge,
                       /*round(sum(nvl(get_det.total_cubic_feet, 0)), 2) total_palete_charge,*/
                       max(nvl(get_det.palete_charg, 0)) palete_charg,
                       max(nvl(get_det.bin_charge, 0)) bin_charge,
                       sum(nvl(get_det.total_bin, 0)) total_bin, /*sum(get_det.widt) total_width, --sum(get_det.heigh) total_height,*/
                       sum(nvl(get_det.total_bin, 0)) *
                       max(nvl(get_det.bin_charge, 0)) total_bin_charge,
                       TO_CHAR(sc.from_date, 'MONTH') MONTH,
                       to_char(sc.from_date,
                               'MONTH',
                               'NLS_DATE_LANGUAGE=''numeric date language''') MONTH_NUMB
                  from LJ_SHIPMENT_MT sm,
                       LJ_STORAGE_CHARGES sc,
                       (select b.shipment_id,
                               count(b.box_type) total_qty,
                               count(decode(b.BOX_TYPE, 1, 'palatte')) total_palte,
                               count(decode(b.BOX_TYPE, 2, 'bin')) total_bin,
                               min(decode(b.BOX_TYPE, 1, sr.charges)) palete_charg,
                               min(decode(b.BOX_TYPE, 2, sr.charges)) bin_charge,
                               round(sum(nvl(b.length, 0) * nvl(b.width, 0) *
                                         nvl(b.height, 0) / 1728),
                                     2) total_cubic_feet,
                               sum(nvl(b.length, 0)) lengt,
                               sum(nvl(b.width, 0)) widt,
                               sum(nvl(b.height, 0)) heigh
                          from LJ_SHIPMENT_BOX b, lj_service_rate sr
                         where b.ser_rate_id = sr.ser_rate_id
                         group by b.shipment_id) get_det
                 where sm.shipment_id = sc.shipment_id
                    and sc.INVOICE_ID is null
                   and sm.shipment_id = get_det.shipment_id(+)
                   and sm.merchant_id = '$merchId'";

        $storage_charges .= " AND sc.from_date BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";

        $storage_charges .= "  group by   TO_CHAR(sc.from_date, 'MONTH') ,
                                to_char(sc.from_date,
                                        'MONTH',
                                        'NLS_DATE_LANGUAGE=''numeric date language''') )";

        $storage_charges = $this->db->query($storage_charges)->result_array();

        $inventory_prep = " SELECT MAX(SERVICE_DESC) SERVICE_NAME,
               MAX(SERV_TYPE) SER_TYPE,
               MAX(CHARGES) RATE,
               count(barcode_no) TOTAL_COUNT,
               NVL(SUM(TOT_CHARGE), 0) TOTAL_CAHRGES,
               /*MAX(SER_RATE_ID) RATE_ID,*/
               TO_CHAR(TRUNC(sum(get_sec) / 3600), 'FM9900') || ' hrs: ' ||
               TO_CHAR(TRUNC(MOD(sum(get_sec), 3600) / 60), 'FM00') || ' min: ' ||
               TO_CHAR(MOD(sum(get_sec), 60), 'FM00') || ' sec' Duration
          FROM (SELECT get_rates_pict.rate CHARGES,
                       SV.SERVICE_DESC,
                       DECODE(SR.SERVICE_TYPE, 1, 'PER_BARC', 2, 'PER_HOUR') SERV_TYPE,
                       SR.SERVICE_ID,
                       (get_rates_pict.rate / 3600) *
                       TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 3600) TOT_CHARGE, /* 60 *60 = 3600 total secs in one hour*/
                       TO_NUMBER(((LG.STOP_TIME - LG.START_TIME) * 24) * 3600) get_sec,
                       SR.SER_RATE_ID,
                       MA.MERCHANT_ID,
                       MA.APPOINTMENT_ID,
                       lg.barcode_no
                  FROM LJ_APPOINTMENT_MT MA,
                       LJ_APPOINTMENT_DT DA,
                       LJ_SERVICE_RATE SR,
                       LJ_SERVICES SV,
                       LJ_APPOINTMENT_LOG LG,
                       (select ms.merchant_id,
                               ms.ser_rate_id,
                               ms.rate,
                               ms.excess_qty_rate
                          from lj_merchant_service ms
                         where ms.merchant_id = '$merchId'
                           and ms.ser_rate_id = 3
                           and rownum <= 1) get_rates_pict
                 WHERE MA.APPOINTMENT_ID = DA.APPOINTMENT_ID
                   AND DA.APPOINTMENT_DT_ID = LG.APPOINTMENT_DT_ID(+)
                   AND LG.INV_ID IS NULL
                   AND DA.SERVICE_ID = SV.SERVICE_ID
                   AND SV.SERVICE_ID = SR.SERVICE_ID
                   and da.ser_rate_id = get_rates_pict.ser_rate_id(+)
                   and sr.ser_rate_id = 3
                   AND MA.MERCHANT_ID = '$merchId' ";

        $inventory_prep .= " AND MA.CREATED_DATE BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        $inventory_prep .= " ORDER BY LG.APPOINTMENT_LOG_ID ASC)
         GROUP BY SERVICE_ID, SERVICE_DESC ";

        $inventory_prep = $this->db->query($inventory_prep)->result_array();

        $other_service = "SELECT /*s.merchant_id,
        s.created_date,*/
        sc.service_desc,
        decode(sr.service_type,
        '1',
        'per barcde',
        '2',
        'hourly',
        '3',
        'per_order',
        '4',
        'per_bin',
        '5',
        'per_pallet',
        '6',
        'per_cubic_feet') ser_type,
       nvl(d.rate, 0) PER_HOUR_RATE,
        d.service_rate_id,
        d.lj_service_dt_id,
        /*to_char(d.start_time, 'DD/MM/YYYY HH24:MI:SS') START_TIME,
        to_char( d.stop_time, 'DD/MM/YYYY HH24:MI:SS') STOP_TIME,*/
        '-' START_TIME,
        '-' STOP_TIME,
        d.duration,
        d.total_charges,
        s.remarks
        from lj_service_mt s, lj_service_dt d, lj_services sc, lj_service_rate sr
        where s.lj_service_mt_id = d.lj_service_mt_id
        and d.service_rate_id = sr.ser_rate_id
        and sr.service_id = sc.service_id
        AND s.MERCHANT_ID = '$merchId'
        and d.invoice_id is null ";

        $other_service .= " AND s.TO_DATE BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        // $other_service .= " ORDER BY LG.APPOINTMENT_LOG_ID ASC)
        //  GROUP BY SERVICE_ID, SERVICE_DESC ";

        $other_service = $this->db->query($other_service)->result_array();

        $pos_service = "
             SELECT
             sum(nvl(dm.qty, 0)) qty,
             round(sum(nvl(dm.price, 0)), 2) sale_Price,

             round(sum((nvl(dm.price, 0) / 100) * nvl(dm.sales_tax_perc, 0)),
                   2) sales_tex,
             round(sum(nvl(dm.disc_amt, 0)), 2) discount,
             round(sum(nvl(dm.price, 0) +
                       (nvl(dm.price, 0) / 100) * nvl(dm.sales_tax_perc, 0) -
                       nvl(dm.disc_amt, 0)),
                   2) net_amount,
             round(round(sum(nvl(dm.price, 0) + (nvl(dm.price, 0) / 100) *
                             nvl(dm.sales_tax_perc, 0) - nvl(dm.disc_amt, 0)),
                         2) * (0.05),
                   2) chargesv,
             TO_CHAR(pm.entered_date_time, 'MONTH') MONTH,
             TO_CHAR(pm.entered_date_time,
                     'MONTH',
                     'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE''') MONTH_NUMB
              from lz_pos_mt pm, lz_pos_det dm
             where pm.lz_pos_mt_id = dm.lz_pos_mt_id
               and dm.barcode_id in
                   (select d.barcode_no
                      from lz_merchant_barcode_mt m, lz_merchant_barcode_dt d
                     where m.mt_id = d.mt_id

                       and m.merchant_id = '$merchId')
               AND pm.deleted_by IS NULL
               and dm.invoice_id is null
               AND pm.return_by IS NULL";

        $pos_service .= " AND pm.entered_date_time BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        $pos_service .= "  GROUP BY TO_CHAR(pm.entered_date_time, 'MONTH'),
          TO_CHAR(pm.entered_date_time,
                  'MONTH',
                  'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE''') ";

        $pos_service = $this->db->query($pos_service)->result_array();

        return array('pic_inv' => $pic_inv, 'listing_inv' => $listing_inv, 'order_inv' => $order_inv, 'storage_charges' => $storage_charges, 'inventory_prep' => $inventory_prep, 'other_services' => $other_service, 'pos_service' => $pos_service, 'exist' => true);

    }

    public function serach_invoice_monthly_detail()
    {

        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $invoice_id = $this->input->post("invoice_id");
        $skipDate = $this->input->post('skipDate');

        $pack_inv_detail = "SELECT item_id,
       MERCHANT_ID,
       TRACKING_NUMBER,
       ORDER_ID,
       ORDER_PACKING_ID,
       SALES_RECORD_NUMBER recrd_no,
       EBAY_ITEM_ID,
       ITEM_TITLE,
       get_sale_first sale_price,
       refund_amount,
      /* sale_price,*/
       QTY,
       ship_cost,
       PACKING_COST,
       SERVICE_COST,
       ORDER_PACKING_DT_ID,
       EBAY_FEE,
       MARKTPLACE,
       ACC_NAME,
       ACT_ID,
       NVL(SHIP_COST + PACKING_COST + SERVICE_COST + EBAY_FEE + MARKTPLACE,
           0) TOTAL_CHARGE


  FROM (SELECT max(sd.extendedorderid) ORDER_ID,
               max(sd.tracking_number) tracking_number,
               max(sd.order_id) ORDER_PACKING_ID, /*max(M.ORDER_PACKING_ID) ORDER_PACKING_ID,*/
               max(sd.extendedorderid) SALES_RECORD_NUMBER,
               max(DT.ORDER_PACKING_DT_ID) ORDER_PACKING_DT_ID,
               MAX(sd.item_id) EBAY_ITEM_ID,
               MAX(sd.item_title) ITEM_TITLE,
                NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) get_sale_first,
               NVL(MAX(sd.SALE_PRICE * sd.quantity), 0)- max(nvl(sd.refund_amount,0)) SALE_PRICE,
               MAX(sd.quantity) QTY,
               NVL(MAX(sd.shippinglabelrate), 0) SHIP_COST,
               SUM(NVL(DT.PACKING_COST, 0)) PACKING_COST,
               MAX(NVL(sd.SERVICE_COST, 0)) SERVICE_COST,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      NVL(MAX(sd.ebay_fee_perc), 0),
                      0) EBAY_FEE,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      (0.07 * (MAX(sd.SALE_PRICE) * MAX(sd.quantity) - max(nvl(sd.refund_amount,0)))),
                      0) MARKTPLACE, /* (0.07 * (MAX(M.SALE_PRICE) * MAX(M.QTY))) MARKTPLACE,*/
               MAX(ACOUNT_DET.ACOUNT_NAME) ACC_NAME,
               MAX(ACOUNT_DET.merchant_id) MERCHANT_ID,
               max(ACOUNT_DET.item_id) item_id,
               max(ACOUNT_DET.ACT_ID) ACT_ID,
               max(nvl(sd.refund_amount,0)) refund_amount
          FROM LZ_SALESLOAD_DET SD,
               /*LJ_ORDER_PACKING_MT M,*/
               LJ_ORDER_PACKING_DT DT,
               (SELECT MM.EBAY_ITEM_ID,
                       max(dd.merchant_id)merchant_id,
                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                       max(mm.item_id) item_id
                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
         WHERE sd.order_id in (select d.order_id
                                 from ebay_list_mt       e,
                                      lj_merhcant_acc_dt a,
                                      lz_salesload_det   d
                                where a.acct_id = e.lz_seller_acct_id
                                  and d.item_id = e.ebay_item_id
                                  and a.merchant_id = '$merchId'
                                group by d.order_id)
           /*and sd.order_id = m.order_id(+)*/
           and sd.orderstatus = 'Completed'
           and sd.return_id is null
           and sd.order_id = dt.order_id(+)
           AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
          ";
        if (!empty($invoice_id)) {
            $pack_inv_detail .= " AND sd.INVOICE_ID = '$invoice_id'";
        } else {
            $pack_inv_detail .= " AND sd.INVOICE_ID IS NULL ";
        }
        if (empty($skipDate)) {
            $pack_inv_detail .= "  AND sd.SALE_DATE  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";

        }

        $pack_inv_detail .= "   GROUP BY sd.ORDER_ID) order by ORDER_ID desc ";
        $pack_inv_detail = $this->db->query($pack_inv_detail)->result_array();

        $pack_inv_counts = " SELECT COUNT(MISSING_SHIPNG) SHIPING_COST_MISS,
        COUNT(MISSING_SERVIC_COST) SERVICE_COST_MISS,
        COUNT(MISSING_PACKINGS) PACK_COST_MISSING
   FROM (SELECT sd.ORDER_ID,
                CASE
                  WHEN NVL(MAX(sd.shippinglabelrate), 0) <= 0 THEN
                   'MISSING'
                END MISSING_SHIPNG,

                CASE
                  WHEN MAX(NVL(sd.SERVICE_COST, 0)) <= 0 THEN
                   'MISSING'
                END MISSING_SERVIC_COST,
                CASE
                  WHEN SUM(NVL(DT.PACKING_COST, 0)) <= 0 THEN
                   'MISSING'
                END MISSING_PACKINGS
           FROM LZ_SALESLOAD_DET SD, /*--LJ_ORDER_PACKING_MT M,*/
                LJ_ORDER_PACKING_DT DT,
                (SELECT MM.EBAY_ITEM_ID,
                        max(dd.merchant_id) merchant_id,
                        MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                        MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                        max(mm.item_id) item_id
                   FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                  WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                    AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                  GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
          WHERE sd.order_id in (select d.order_id
                                  from ebay_list_mt       e,
                                       lj_merhcant_acc_dt a,
                                       lz_salesload_det   d
                                 where a.acct_id = e.lz_seller_acct_id
                                   and d.item_id = e.ebay_item_id
                                   and a.merchant_id = '$merchId'
                                 group by d.order_id) /*and sd.order_id = m.order_id(+)*/
            and sd.orderstatus = 'Completed'
            and sd.return_id is null
            and sd.order_id = dt.order_id(+)
            AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
            AND sd.INVOICE_ID IS NULL
            ";
        if (empty($skipDate)) {
            $pack_inv_counts .= "  AND sd.SALE_DATE  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        }
        $pack_inv_counts .= "   GROUP BY sd.ORDER_ID) ";
        $pack_inv_counts = $this->db->query($pack_inv_counts)->result_array();

        if (count($pack_inv_detail) > 0) {
            $images = $this->get_ebay_pictures($pack_inv_detail);
            return array('data' => $pack_inv_detail, 'count_data' => $pack_inv_counts, 'status' => true, 'images' => $images['uri']);
        } else {
            return array('data' => $pack_inv_detail, 'count_data' => $pack_inv_counts, 'status' => false, 'images' => array());
        }

    }
    public function get_barcode_pictures($barcodes)
    {
        $uri = [];
        $base64 = [];

        // $barocde_no = $this->input->post('barocde_no');
        foreach ($barcodes as $barco) {
            $bar = explode(',', $barco['BARCODE_NO']);
            $bar_val = $bar[0];
            $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

            if (count($qry) >= 1) {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $barcode = $qry[0]["FOLDER_NAME"];
                $dir = $master_path . $barcode . "/";
                // var_dump($dir);
                // exit;

            } else {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar_val' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();
                if (count($get_params) === 0) {
                    $get_params = $this->db->query("SELECT MT.UPC F_UPC, MT.MPN F_MPN, C.COND_NAME, DT.BARCODE_NO
                                      FROM LZ_PURCH_ITEM_MT MT, LZ_PURCH_ITEM_DT DT, LZ_ITEM_COND_MT C
                                     WHERE MT.PURCH_MT_ID = DT.PURCH_MT_ID
                                       AND C.ID = MT.CONDITION
                                       AND DT.BARCODE_NO = '$bar_val'")->result_array();
                }
                //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                $cond = @$get_params[0]['COND_NAME'];

                $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);
            // var_dump($dir);
            // exit;

            //var_dump(is_dir($dir));exit;
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            if (is_dir($dir)) {
                // var_dump($dir);exit;
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
                if (!empty($images)) {
                    $i = 0;
                    $count = 0;
                    foreach ($images as $image) {

                        // Get Pic Date of Folder
                        $now = date("Y-m-d"); // or your date as well
                        $fileDate = date("Y-m-d", filemtime($image));
                        $date1 = date_create($now);
                        $date2 = date_create($fileDate);
                        $diff = date_diff($date1, $date2);
                        $diff = $diff->format("%a");
                        // echo $diff;

                        $pathinfo = pathinfo($image);
                        $imagePath = explode('/', $image, 4);
                        // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $image_path = $this->convert_text(@$imagePath[3]);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $image_path);
                        // $uri[$bar_val][$i] = $base_url . $withoutMasterPartUri;
                        $uri[$bar_val]['image'][$i] = "/" . $withoutMasterPartUri;
                        $count = $count + 1;
                        // $uri[$bar_val][$i + 1] = $bar_val;
                        // $uri[$bar_val][$i + 2] = $i + 1;
                        $i++;
                    }
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = $count;
                    $uri[$bar_val][3] = $diff;
                } else {
                    // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                    // $image = $this->convert_text(@$imagePath);
                    $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar_val][1] = $bar_val;
                    $uri[$bar_val][2] = 0;
                    $uri[$bar_val][3] = 0;
                }

            } else {
                // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                // $image = $this->convert_text(@$imagePath);
                $uri[$bar_val]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar_val][1] = $bar_val;
                $uri[$bar_val][2] = 0;
                $uri[$bar_val][3] = 0;
            }

            //var_dump($dekitted_pics);exit;

        }
        // var_dump($uri);
        // exit;
        return array('uri' => $uri);
    }

    public function get_ebay_pictures($data)
    {
        $uri = [];
        $base64 = [];

        // $barocde_no = $this->input->post('barocde_no');
        foreach ($data as $barco) {
            $ebay_id = $barco['EBAY_ITEM_ID'];
            $barcode_no = $this->db->query("SELECT BARCODE_NO from lz_barcode_mt WHERE EBAY_ITEM_ID = '$ebay_id'")->result_array();
            if (count($barcode_no) > 0) {
                $bar_val = $barcode_no[0]['BARCODE_NO'];
            } else {
                $bar_val = '';
            }
            $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

            if (count($qry) >= 1) {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $barcode = $qry[0]["FOLDER_NAME"];
                $dir = $master_path . $barcode . "/";
                // var_dump($dir);
                // exit;

            } else {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar_val' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();
                if (count($get_params) === 0) {
                    $get_params = $this->db->query("SELECT MT.UPC F_UPC, MT.MPN F_MPN, C.COND_NAME, DT.BARCODE_NO
                                      FROM LZ_PURCH_ITEM_MT MT, LZ_PURCH_ITEM_DT DT, LZ_ITEM_COND_MT C
                                     WHERE MT.PURCH_MT_ID = DT.PURCH_MT_ID
                                       AND C.ID = MT.CONDITION
                                       AND DT.BARCODE_NO = '$bar_val'")->result_array();
                }
                //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                $cond = @$get_params[0]['COND_NAME'];

                $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);
            // var_dump($dir);
            // exit;

            //var_dump(is_dir($dir));exit;
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            if (is_dir($dir)) {
                // var_dump($dir);exit;
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
                if (!empty($images)) {
                    $i = 0;
                    $count = 0;
                    foreach ($images as $image) {

                        // Get Pic Date of Folder
                        $now = date("Y-m-d"); // or your date as well
                        $fileDate = date("Y-m-d", filemtime($image));
                        $date1 = date_create($now);
                        $date2 = date_create($fileDate);
                        $diff = date_diff($date1, $date2);
                        $diff = $diff->format("%a");
                        // echo $diff;

                        $pathinfo = pathinfo($image);
                        $imagePath = explode('/', $image, 4);
                        // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $image_path = $this->convert_text(@$imagePath[3]);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $image_path);
                        // $uri[$bar_val][$i] = $base_url . $withoutMasterPartUri;
                        $uri[$ebay_id]['image'][$i] = "/" . $withoutMasterPartUri;
                        $count = $count + 1;
                        // $uri[$bar_val][$i + 1] = $bar_val;
                        // $uri[$bar_val][$i + 2] = $i + 1;
                        $i++;
                    }
                    $uri[$ebay_id][1] = $ebay_id;
                    $uri[$ebay_id][2] = $count;
                    $uri[$ebay_id][3] = $diff;
                } else {
                    // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                    // $image = $this->convert_text(@$imagePath);
                    $uri[$ebay_id]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$ebay_id][1] = $ebay_id;
                    $uri[$ebay_id][2] = 0;
                    $uri[$ebay_id][3] = 0;
                }

            } else {
                // $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                // $imagePath = "/item_pictures/master_pictures/1236455#~sasd/image_not_available.jpg";
                // $image = $this->convert_text(@$imagePath);
                $uri[$ebay_id]['image'][0] = "/item_pictures/master_pictures/image_not_available.jpg";
                $uri[$ebay_id][1] = $ebay_id;
                $uri[$ebay_id][2] = 0;
                $uri[$ebay_id][3] = 0;
            }

            //var_dump($dekitted_pics);exit;

        }
        // var_dump($uri);
        // exit;
        return array('uri' => $uri);
    }
    public function serach_Inv_list_charg_detail()
    {

        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $invoice_id = $this->input->post('invoice_id');
        $skipDate = $this->input->post('skipDate');

        // var_dump($merchId);
        // exit;

        $list_inv_detail = "SELECT EBAY_ITEM_ID,
       TITLE,
       LIST_PRICE,
       QTY,
       CASE
         WHEN QTY > 1 THEN
          ((QTY - 1) * excess_qty_rate ) + rate
         ELSE
          QTY * rate
       END LJ_CHARGE
  FROM (SELECT T.EBAY_ITEM_ID,
   round(max(get_rates_pict.rate),2) rate,
  round(max(get_rates_pict.excess_qty_rate),2) excess_qty_rate,
               MAX(T.EBAY_ITEM_DESC) TITLE,
               SUM(T.LIST_QTY) QTY,
               MAX(T.LIST_PRICE) LIST_PRICE
          FROM EBAY_LIST_MT T, LJ_MERHCANT_ACC_DT DD, LZ_LISTED_ITEM_URL L,
          (select ms.merchant_id,
                       ms.ser_rate_id,
                       ms.rate,
                       ms.excess_qty_rate
                  from lj_merchant_service ms
                 where ms.merchant_id ='$merchId'
                   and ms.ser_rate_id = 2
                   and rownum<=1) get_rates_pict
         WHERE T.EBAY_ITEM_ID = L.EBAY_ID
           AND DD.ACCT_ID = T.LZ_SELLER_ACCT_ID(+)

           AND T.LZ_SELLER_ACCT_ID IS NOT NULL
           and dd.merchant_id = get_rates_pict.merchant_id(+)
           AND DD.MERCHANT_ID = '$merchId'
           AND T.STATUS IN ('ADD', 'REVISED') ";
        if (!empty($invoice_id)) {
            $list_inv_detail .= " and t.invoice_id = '$invoice_id'";
        } else {
            $list_inv_detail .= " and t.invoice_id is null";
        }
        if (empty($skipDate)) {

            $list_inv_detail .= "  AND T.LIST_DATE  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        }

        $list_inv_detail .= "   GROUP BY  T.EBAY_ITEM_ID) ";
        $list_inv_detail = $this->db->query($list_inv_detail)->result_array();
        if (count($list_inv_detail) > 0) {
            $images = $this->get_ebay_pictures($list_inv_detail);
            return array('data' => $list_inv_detail, 'status' => true, 'images' => $images['uri']);
        } else {
            return array('data' => $list_inv_detail, 'status' => false, 'images' => array());
        }

    }

    public function search_Inv_pic_charg_detail()
    {

        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $invoice_id = $this->input->post('invoice_id');
        $skipDate = $this->input->post('skipDate');
        // var_dump($merchId);
        // exit;

        $pic_inv_detail = "SELECT EBAY_ID,
       TITLE,
       LIST_PRICE,
       TOTAL_PICS_QTY TOTAL_PICS,
       CASE
         WHEN TOTAL_PICS_QTY > 1 THEN
          ROUND(((TOTAL_PICS_QTY - 1) * excess_qty_rate ) + rate, 2)
         ELSE
          ROUND(TOTAL_PICS_QTY * rate, 2)
       END LJ_CHARGE,
       BARCODE_NO
  FROM (SELECT MAX(B.EBAY_ITEM_ID) EBAY_ID,
               MAX(S.ITEM_TITLE) TITLE,
               MAX(L.BARCODE_PRV_NO) BARCODE_NO,
               L.FOLDER_NAME,
               MAX(EB_PRI.LIST_PRICE) LIST_PRICE,
               COUNT(L.FOLDER_NAME) TOTAL_PICS_QTY,
               round(max(get_rates_pict.rate),2) rate,
  round(max(get_rates_pict.excess_qty_rate),2) excess_qty_rate
          FROM LZ_MERCHANT_BARCODE_MT M,
               LZ_MERCHANT_BARCODE_DT D,
               LZ_SPECIAL_LOTS L,
               LZ_BARCODE_MT B,
               LZ_ITEM_SEED S,
               (SELECT M.EBAY_ITEM_ID, MAX(M.LIST_PRICE) LIST_PRICE
                  FROM EBAY_LIST_MT M
                 GROUP BY M.EBAY_ITEM_ID) EB_PRI,
                 (select ms.merchant_id,
                       ms.ser_rate_id,
                       ms.rate,
                       ms.excess_qty_rate
                  from lj_merchant_service ms
                 where ms.merchant_id = '$merchId'
                   and ms.ser_rate_id = 1
                   and rownum<=1) get_rates_pict
         WHERE M.MT_ID = D.MT_ID
           AND D.BARCODE_NO = L.BARCODE_PRV_NO

           AND B.EBAY_ITEM_ID = EB_PRI.EBAY_ITEM_ID(+)
           AND L.BARCODE_PRV_NO = B.BARCODE_NO(+)
           AND B.ITEM_ID = S.ITEM_ID(+)
           AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+)
           AND B.CONDITION_ID = S.DEFAULT_COND(+)
           and m.merchant_id = get_rates_pict.merchant_id(+)
           AND M.MERCHANT_ID = '$merchId'  ";
        if (!empty($invoice_id)) {
            $pic_inv_detail .= " and l.INVOICE_ID = '$invoice_id'";
        } else {
            $pic_inv_detail .= " and l.INVOICE_ID is null";
        }
        if (empty($skipDate)) {

            $pic_inv_detail .= "  AND L.PIC_DATE_TIME  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";
        }

        $pic_inv_detail .= "   GROUP BY  L.FOLDER_NAME)  ";
        $pic_inv_detail = $this->db->query($pic_inv_detail)->result_array();
        if (count($pic_inv_detail) > 0) {
            $image = $this->get_barcode_pictures($pic_inv_detail);
            return array('data' => $pic_inv_detail, 'status' => true, 'images' => $image['uri']);
        } else {
            return array('data' => $pic_inv_detail, 'status' => false, 'images' => array());
        }

    }

    public function pic_invoice_barcode_details()
    {

        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $barcode_no = $this->input->post('barcode_no');

        $pic_inv_barc = "SELECT Y.BARCODE_PRV_NO BARCODE_NO
        FROM LZ_SPECIAL_LOTS Y,LZ_MERCHANT_BARCODE_MT M,LZ_MERCHANT_BARCODE_DT D
        WHERE M.MT_ID = D.MT_ID
        AND D.BARCODE_NO = Y.BARCODE_PRV_NO
        AND M.MERCHANT_ID = '$merchId'
        AND Y.FOLDER_NAME IN
        (SELECT LL.FOLDER_NAME
        FROM LZ_SPECIAL_LOTS LL
        WHERE LL.BARCODE_PRV_NO = '$barcode_no')";

        $pic_inv_barc .= "  AND Y.PIC_DATE_TIME  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";

        $pic_inv_barc = $this->db->query($pic_inv_barc)->result_array();

        if (count($pic_inv_barc) > 0) {
            return array('pic_inv_barc' => $pic_inv_barc, 'status' => true);
        } else {
            return array('pic_inv_barc' => $pic_inv_barc, 'status' => false);
        }

    }

    public function list_invoice_barcode_details()
    {

        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $ebay_id = $this->input->post('ebay_id');

        $list_inv_barc = "
            SELECT J.BARCODE_NO
            FROM LZ_BARCODE_MT J
            WHERE J.LIST_ID IN
       (SELECT t.list_id list_id

            FROM EBAY_LIST_MT T, LJ_MERHCANT_ACC_DT DD, LZ_LISTED_ITEM_URL L
            WHERE T.EBAY_ITEM_ID = L.EBAY_ID
            AND DD.ACCT_ID = T.LZ_SELLER_ACCT_ID(+)
            AND T.LZ_SELLER_ACCT_ID IS NOT NULL
            AND DD.MERCHANT_ID = '$merchId'
            AND T.STATUS IN ('ADD', 'REVISED')
            and t.ebay_item_id = '$ebay_id' ";

        $list_inv_barc .= "  AND T.LIST_DATE  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS'))";

        $list_inv_barc = $this->db->query($list_inv_barc)->result_array();

        if (count($list_inv_barc) > 0) {
            return array('list_inv_barc' => $list_inv_barc, 'status' => true);
        } else {
            return array('list_inv_barc' => $list_inv_barc, 'status' => false);
        }

    }

    public function bin_wise_storage_inv_detail()
    {

        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $skipDate = $this->input->post('skipDate');
        $bin_wise_strage_inv_detail = "select *
  from (select TO_CHAR(L.PIC_DATE_TIME, 'MONTH') mon,
  l.bin_id,
  max(bi.bin_type)||'-'||max(bi.bin_no) Active_bins,
               TO_CHAR(L.PIC_DATE_TIME,
                       'MONTH',
                       'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE''') MONTH_NUMB,
               /*count(distinct l.bin_id) Active_bins,*/
               count(l.barcode_prv_no) total_barc,
               max(mer_ser_charg.rate) rate,
               count(sold_items.barcode_prv_no) sold_barcs,
               count(l.barcode_prv_no) - count(sold_items.barcode_prv_no) active_items,
               count(distinct l.bin_id) * max(mer_ser_charg.rate) total_charge
          from lz_merchant_barcode_mt m,
               lz_merchant_barcode_dt d,
               LZ_SPECIAL_LOTS L,
               bin_mt bi,
               (select sr.ser_rate_id,
                       m.merchant_id,
                       m.buisness_name merchant_name,
                       s.service_desc  assigned_service,
                       ms.rate
                  from lj_merchant_service ms,
                       lz_merchant_mt      m,
                       lj_service_rate     sr,
                       lj_services         s
                 where ms.merchant_id = m.merchant_id
                   and ms.ser_rate_id = sr.ser_rate_id
                   and sr.service_id = s.service_id
                   and ms.merchant_id = 2
                   and sr.ser_rate_id = 6) mer_ser_charg,
               (select l.barcode_prv_no
                  from lz_special_lots        l,
                       lz_merchant_barcode_mt m,
                       lz_merchant_barcode_dt d,
                       lz_barcode_mt          b,
                       LZ_SALESLOAD_DET       DE
                 where m.mt_id = d.mt_id
                   and d.barcode_no = l.barcode_prv_no
                   and l.barcode_prv_no = b.barcode_no(+)
                   AND b.order_id = de.order_id(+)
                   and m.merchant_id = '$merchId'";
        if (empty($skipDate)) {
            $bin_wise_strage_inv_detail .= "  and de.sale_Date between /* it must be from and to date*/
                       TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')) sold_items";
        }

        $bin_wise_strage_inv_detail .= "
        where m.mt_id = d.mt_id
           AND D.BARCODE_NO = l.barcode_prv_no
           AND M.MERCHANT_ID = MER_SER_CHARG.MERCHANT_ID(+)
           and l.bin_id = bi.bin_id
           and l.barcode_prv_no = sold_items.barcode_prv_no(+)
           and m.merchant_id = '$merchId' ";
        $bin_wise_strage_inv_detail .= " and l.pic_date_time between
           /*  TO_DATE('2019-03-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS') min date for monthly charge again*/
               TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') ";

        $bin_wise_strage_inv_detail .= " GROUP BY l.bin_id, TO_CHAR(L.PIC_DATE_TIME, 'MONTH'),
                  TO_CHAR(L.PIC_DATE_TIME,
                          'MONTH',
                          'NLS_DATE_LANGUAGE=''NUMERIC DATE LANGUAGE'''))
 order by MONTH_NUMB asc";

        $bin_wise_strage_inv_detail = $this->db->query($bin_wise_strage_inv_detail)->result_array();
        if (count($bin_wise_strage_inv_detail) > 0) {
            return array('data' => $bin_wise_strage_inv_detail, 'status' => true);
        } else {
            return array('data' => $bin_wise_strage_inv_detail, 'status' => false);
        }
    }

    public function Get_Monthly_Storage_Charge()
    {
        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $merchId = $this->input->post("merchId");
        $invoice_id = $this->input->post('invoice_id');
        $skipDate = $this->input->post('skipDate');
        $qry = "SELECT B.SHIPMENT_ID,
               decode(b.box_type, 1, 'palatte', 2, 'bin') box_type,
               sr.charges,
               nvl(b.length, 0) length,
               nvl(b.width, 0) width,
               nvl(b.height, 0) height,
                 round((nvl(b.length, 0) * nvl(b.width, 0) * nvl(b.height, 0)) / 1728 ,2) cubic_feet,
        decode(b.box_type,
                      1,
                       round((nvl(b.length, 0) * nvl(b.width, 0) * nvl(b.height, 0)) / 1728 * sr.charges,2),
                      2,
                      sr.charges) chrages

          from LJ_SHIPMENT_BOX b, lj_service_rate sr, LJ_STORAGE_CHARGES sc,LJ_SHIPMENT_MT sm
         where b.ser_rate_id = sr.ser_rate_id
         and b.shipment_id = sm.shipment_id
         and sm.merchant_id = '$merchId'
         and b.shipment_id = sc.shipment_id
        ";
        if (!empty($invoice_id)) {
            $qry .= " and sc.INVOICE_ID = '$invoice_id'";
        } else {
            $qry .= " and sc.INVOICE_ID is null";
        }
        if (empty($skipDate)) {
            $qry .= " AND sc.from_date BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') ";
        }
        $data = $this->db->query($qry)->result_array();
        if (count($data) > 0) {
            return array('status' => true, 'data' => $data);
        } else {
            return array('status' => false, 'data' => array(), 'message' => 'No Record Found');
        }
    }

    public function merch_invoice_status_detail()
    {
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $merId = $this->input->post('merchId');

        $inv_master_query = $this->db->query("SELECT MAX(MT.INVOICE_ID) INVOICE_ID,MAX(MT.MERCHANT_ID) MERCH_ID, 'INV-' || LPAD(NVL(MT.INVOICE_NO, 0), 6, '0') INV_CODE, MAX(MER.BUISNESS_NAME) MERCHAN_NAME, MAX(MT.INV_FROM_DATE) FROM_DATE, MAX(MT.INV_TO_DATE) TO_DATE, MAX(MT.DUE_DATE) DUE_DATE, SUM(NVL(DT.DIS_AMOUNT,0)) DISCOUNT_AMOUNT,MAX(NVL(REC.RECIEVED_AMOUNT, 0)) PAID_AMOUNT,SUM(NVL(DT.TOTAL_CHARGES, 0)) - SUM(NVL(DT.DIS_AMOUNT, 0)) -
       MAX(NVL(REC.RECIEVED_AMOUNT, 0)) PAYABLE, SUM(NVL(DT.TOTAL_CHARGES,0))  TOTAL_CHARGES, DECODE(MAX(MT.INVOICE_STATUS),0,'Unpaid',1,'PAID') STATUS FROM LJ_INVOICE_MT   MT, LJ_INVOICE_DT   DT, LJ_SERVICE_RATE SR, LJ_SERVICES     SC, LZ_MERCHANT_MT  MER, (SELECT SUM(NVL(PA.AMOUNT_PAID, 0)) RECIEVED_AMOUNT, (PA.INVOICE_ID) INV_ID FROM LJ_PAYMENT_RECEIPT PA GROUP BY PA.INVOICE_ID) REC where mt.invoice_id = dt.invoice_id and dt.service_rate_id = sr.ser_rate_id(+)  AND MT.INVOICE_ID = REC.INV_ID(+) and sr.service_id = sc.service_id and mt.merchant_id = mer.merchant_id and mt.merchant_id = '$merId'/*and mt.INV_FROM_DATE between
               TO_DATE('2020-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
               TO_DATE('2020-01-31 23:59:59', 'YYYY-MM-DD HH24:MI:SS')*/
         group by mt.invoice_no, mt.merchant_id
          order by max(mt.invoice_id) asc ");

        if ($inv_master_query->num_rows() > 0) {
            $mast_data = $inv_master_query->result_array();
            $inv_detail_data = [];
            foreach ($mast_data as $get_mast_data) {
                $invoice_id = $get_mast_data['INVOICE_ID'];
                $merch_id = $get_mast_data['MERCH_ID'];
                $inv_code = $get_mast_data['INV_CODE'];
                $merchan_name = $get_mast_data['MERCHAN_NAME'];
                $from_date = $get_mast_data['FROM_DATE'];
                $to_date = $get_mast_data['TO_DATE'];
                $due_date = $get_mast_data['DUE_DATE'];
                $discount_amount = $get_mast_data['DISCOUNT_AMOUNT'];
                $total_charges = $get_mast_data['TOTAL_CHARGES'];
                $paid_amount = $get_mast_data['PAID_AMOUNT'];
                $payable = $get_mast_data['PAYABLE'];
                $status = $get_mast_data['STATUS'];

                $inv_detail_data_row = $this->db->query("SELECT /*mer.buisness_name BUSNIESS_NAME,*/mt.invoice_id INVOICE_ID, sc.service_desc SERVICE_DESC, dt.from_date FROM_DATE, dt.to_date TO_DATE, nvl(dt.dis_amount, 0) DISCOUNT_AMOUNT, nvl(dt.dis_amount_perc, 0) DISCOUNT_PER, nvl(dt.total_charges, 0) - nvl(dt.dis_amount, 0) TOTAL_CHARGE, null DISCOUNTED_AMOUNT, sr.ser_rate_id SER_RATE_ID from lj_invoice_mt   mt, lj_invoice_dt   dt, lj_service_rate sr, lj_services     sc, lz_merchant_mt  mer where mt.invoice_id = dt.invoice_id and dt.service_rate_id = sr.ser_rate_id(+) and sr.service_id = sc.service_id and mt.merchant_id = mer.merchant_id and mt.merchant_id = '$merch_id'
                       /*and mt.INV_FROM_DATE between TO_DATE('2020-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('2020-01-31 23:59:59', 'YYYY-MM-DD HH24:MI:SS')*/
                         and mt.invoice_id = '$invoice_id' ")->result_array();

                $inv_detail_data[] = array(
                    'invoice_id' => $invoice_id,
                    'inv_code' => $inv_code,
                    'merchan_name' => $merchan_name,
                    'from_date' => $from_date,
                    'to_date' => $to_date,
                    'due_date' => $due_date,
                    'total_charges' => $total_charges,
                    'paid_amount' => $paid_amount,
                    'discount_amount' => $discount_amount,
                    'payable' => $payable,
                    'status' => $status,
                    'detail_data' => $inv_detail_data_row,
                );

            }

            if (!empty($inv_detail_data)) {
                return array('status' => true, 'data' => $inv_detail_data);

            } else {
                return array('status' => false, 'data' => array());

            }
        } else {
            return array('status' => false, 'data' => array());
        }

    }
    public function get_payment_recipt_no()
    {
        $merchant_id = $this->input->post('merchant_id');
        $qry = $this->db->query("SELECT  'Rec' || '-' || LPAD(nvl(max(rec.reciept_no + 1), 1), 6, '0') recipt_no , LPAD(nvl(max(rec.reciept_no + 1), 1), 6, '0') recp_id from lj_payment_receipt rec
                where rec.merchant_id = '$merchant_id'")->row();
        if ($qry) {
            return array('status' => true, 'recipt_no' => $qry);
        } else {
            return array('status' => false, 'recipt_no' => "");
        }

    }
    public function save_payment_recipt()
    {

        $received_by = $this->input->post('received_by');
        $received_by = trim(str_replace("  ", ' ', $received_by));
        $received_by = str_replace(array("`,'"), "", $received_by);
        $received_by = str_replace(array("'"), "''", $received_by);

        $recipt_no = $this->input->post('recipt_no');
        $recipt_no = trim(str_replace("  ", ' ', $recipt_no));
        $recipt_no = str_replace(array("`,'"), "", $recipt_no);
        $recipt_no = str_replace(array("'"), "''", $recipt_no);

        $recipt_ref_no = $this->input->post('recipt_ref_no');
        $recipt_ref_no = trim(str_replace("  ", ' ', $recipt_ref_no));
        $recipt_ref_no = str_replace(array("`,'"), "", $recipt_ref_no);
        $recipt_ref_no = str_replace(array("'"), "''", $recipt_ref_no);

        $merchId = $this->input->post('merchId');

        $currency = $this->input->post('currency');
        $currency = trim(str_replace("  ", ' ', $currency));
        $currency = str_replace(array("`,'"), "", $currency);
        $currency = str_replace(array("'"), "''", $currency);

        $paid_amount = $this->input->post('paid_amount');
        $paid_amount = trim(str_replace("  ", ' ', $paid_amount));
        $paid_amount = str_replace(array("`,'"), "", $paid_amount);
        $paid_amount = str_replace(array("'"), "''", $paid_amount);

        $payment_type = $this->input->post('payment_type');

        $bank_name = $this->input->post('bank_name');
        $bank_name = trim(str_replace("  ", ' ', $bank_name));
        $bank_name = str_replace(array("`,'"), "", $bank_name);
        $bank_name = str_replace(array("'"), "''", $bank_name);

        $bank_account_number = $this->input->post('bank_account_number');
        $bank_account_number = trim(str_replace("  ", ' ', $bank_account_number));
        $bank_account_number = str_replace(array("`,'"), "", $bank_account_number);
        $bank_account_number = str_replace(array("'"), "''", $bank_account_number);

        $check_number = $this->input->post('check_number');
        $check_number = trim(str_replace("  ", ' ', $check_number));
        $check_number = str_replace(array("`,'"), "", $check_number);
        $check_number = str_replace(array("'"), "''", $check_number);

        $remarks = $this->input->post('remarks');
        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = str_replace(array("`,'"), "", $remarks);
        $remarks = str_replace(array("'"), "''", $remarks);

        $date = $this->input->post('startDate');

        $current_date = "TO_DATE('" . $date . "', 'YYYY-MM-DD HH24:MI:SS')";

        $sEmail = $this->input->post('sEmail');
        $sEmail = trim(str_replace("  ", ' ', $sEmail));
        $sEmail = str_replace(array("`,'"), "", $sEmail);
        $sEmail = str_replace(array("'"), "''", $sEmail);

        $rEmail = $this->input->post('rEmail');
        $rEmail = trim(str_replace("  ", ' ', $rEmail));
        $rEmail = str_replace(array("`,'"), "", $rEmail);
        $rEmail = str_replace(array("'"), "''", $rEmail);

        $in_words = $this->input->post('amount_words');
        $in_words = trim(str_replace("  ", ' ', $in_words));
        $in_words = str_replace(array("`,'"), "", $in_words);
        $in_words = str_replace(array("'"), "''", $in_words);

        // $in_words = $this->numberTowords($paid_amount);

        $get_mt_pk = $this->db->query("SELECT get_single_primary_key('lj_payment_receipt','LJ_PAYMENT_ID') LJ_PAYMENT_ID FROM DUAL");
        $get_pk = $get_mt_pk->result_array();
        $pk_id = $get_pk[0]['LJ_PAYMENT_ID'];

        $ins_mt_qry = "INSERT INTO lj_payment_receipt(
                LJ_PAYMENT_ID, MERCHANT_ID, RECIEPT_NO,
                RECIEVE_DATE, AMOUNT_PAID, RECIEVED_BY,
                PAYMENT_TYPE, CHEQUE_NO, REF_NO,
                CURRENCY_ID,REMARKS,ACCOUNT_NO,
                BANK_NAME,PAYMENT_STATUS,AMOUNT_IN_WORDS,SENDER_EMAIL,RECIEVER_EMAIL,ENTERED_DATE )
            VALUES ($pk_id,'$merchId','$recipt_no',$current_date,'$paid_amount', '$received_by' ,'$payment_type', '$check_number','$recipt_ref_no','$currency','$remarks','$bank_account_number','$bank_name',0,'$in_words','$sEmail','$rEmail',sysdate)";
        $ins_mt_qry = $this->db->query($ins_mt_qry);

        if ($ins_mt_qry) {
            return array('status' => true, 'message' => "Payment Recipt Generated");
        } else {
            return array('status' => false, 'message' => "Failed To Generate Recipt");
        }

    }
    public function numberTowords($num)
    {

        $ones = array(0 => "ZERO", 1 => "ONE", 2 => "TWO", 3 => "THREE", 4 => "FOUR", 5 => "FIVE",
            6 => "SIX", 7 => "SEVEN", 8 => "EIGHT", 9 => "NINE", 10 => "TEN", 11 => "ELEVEN", 12 => "TWELVE",
            13 => "THIRTEEN", 14 => "FOURTEEN", 15 => "FIFTEEN", 16 => "SIXTEEN", 17 => "SEVENTEEN", 18 => "EIGHTEEN", 19 => "NINETEEN", "014" => "FOURTEEN",
        );
        $tens = array(0 => "ZERO", 1 => "TEN", 2 => "TWENTY", 3 => "THIRTY", 4 => "FORTY", 5 => "FIFTY", 6 => "SIXTY", 7 => "SEVENTY", 8 => "EIGHTY", 9 => "NINETY");
        $hundreds = array("HUNDRED", "THOUSAND", "MILLION", "BILLION", "TRILLION", "QUARDRILLION"); /*limit t quadrillion */
        $num = number_format($num, 2, ".", ",");
        $num_arr = explode(".", $num);
        $wholenum = $num_arr[0];
        $decnum = $num_arr[1];
        $whole_arr = array_reverse(explode(",", $wholenum));
        krsort($whole_arr, 1);
        $rettxt = "";
        foreach ($whole_arr as $key => $i) {
            while (substr($i, 0, 1) == "0") {
                $i = substr($i, 1, 5);
            }

            if ($i < 20) {
                /* echo "getting:".$i; */
                $rettxt .= $ones[$i];
            } elseif ($i < 100) {
                if (substr($i, 0, 1) != "0") {
                    $rettxt .= $tens[substr($i, 0, 1)];
                }

                if (substr($i, 1, 1) != "0") {
                    $rettxt .= " " . $ones[substr($i, 1, 1)];
                }

            } else {
                if (substr($i, 0, 1) != "0") {
                    $rettxt .= $ones[substr($i, 0, 1)] . " " . $hundreds[0];
                }

                if (substr($i, 1, 1) != "0") {
                    $rettxt .= " " . $tens[substr($i, 1, 1)];
                }

                if (substr($i, 2, 1) != "0") {
                    $rettxt .= " " . $ones[substr($i, 2, 1)];
                }

            }
            if ($key > 0) {
                $rettxt .= " " . $hundreds[$key] . " ";
            }
        }
        if ($decnum > 0) {
            $rettxt .= " and ";
            if ($decnum < 20) {
                $rettxt .= $ones[$decnum];
            } elseif ($decnum < 100) {
                $rettxt .= $tens[substr($decnum, 0, 1)];
                $rettxt .= " " . $ones[substr($decnum, 1, 1)];
            }
        }
        return $rettxt;

    }
    public function get_payment_recipt_data()
    {

        $merchId = $this->input->post('merchId');
        if ($merchId) {
            $merchId = 'and re.merchant_id =' . $merchId;
        } else {
            $merchId = '';
        }
        $payment_recipt_data = $this->db->query("SELECT re.lj_payment_id,'Rec' || '-' || LPAD(NVL(re.reciept_no, 0), 6, '0') receipt_no,
        mer.buisness_name merch_name,
        mer.merchant_id,
        re.ref_no,
        re.payment_type payment_type_id,
        decode(re.payment_type, 1, 'Cash', 2, 'Cheque', 3, 'Paypal',4 , 'Zelle') payment_type,
        re.bank_name,
        re.account_no,
        re.cheque_no,
        re.amount_paid received_amount,
        decode(re.payment_status,0,'Pending','1','Approved',2,'Not Approved') payment_status,
        re.recieve_date,
        em.first_name received_by,
        re.remarks,
        re.amount_in_words,
        re.sender_email,
        re.reciever_email
        from lj_payment_receipt re, lz_merchant_mt mer, employee_mt em

        where re.merchant_id = mer.merchant_id
        $merchId
        and re.recieved_by = em.employee_id order by  re.lj_payment_id desc")->result_array();

        if ($payment_recipt_data) {
            return array('status' => true, 'payment_recipt_data' => $payment_recipt_data);
        } else {
            return array('status' => false, 'payment_recipt_data' => "");
        }

    }
    public function delete_payment_recipt()
    {
        $payment_id = $this->input->post('payment_id');

        $payment_recipt_data = $this->db->query("DELETE FROM lj_payment_receipt WHERE lj_payment_id ='$payment_id' ");

        if ($payment_recipt_data) {
            return array('status' => true, 'message' => "Deleted Successfully");
        } else {
            return array('status' => false, 'message' => "Failed To Delete");
        }

    }
    public function update_payment_recipt_status()
    {
        $payment_id = $this->input->post('payment_id');

        $payment_recipt_data = $this->db->query("UPDATE lj_payment_receipt set PAYMENT_STATUS = '1' where lj_payment_id = '$payment_id'");
        if ($payment_recipt_data) {
            return array('status' => true, 'message' => "Successfully Update Receipt");
        } else {
            return array('status' => false, 'message' => "Failed To Update Receipt");
        }

    }
    public function update_payment_recipt_data()
    {
        $payment_id = $this->input->post('payment_id');
        $paid_amount = $this->input->post('paid_amount');
        $date = $this->input->post('startDate');
        $remarks = $this->input->post('remarks');

        $current_date = "TO_DATE('" . $date . "', 'YYYY-MM-DD HH24:MI:SS')";

        $in_words = $this->input->post('amount_words');
        $in_words = trim(str_replace("  ", ' ', $in_words));
        $in_words = str_replace(array("`,'"), "", $in_words);
        $in_words = str_replace(array("'"), "''", $in_words);

        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = str_replace(array("`,'"), "", $remarks);
        $remarks = str_replace(array("'"), "''", $remarks);

        $payment_recipt_data = $this->db->query("UPDATE lj_payment_receipt set
        RECIEVE_DATE =$current_date, AMOUNT_PAID= '$paid_amount' ,REMARKS = '$remarks', AMOUNT_IN_WORDS =  '$in_words' where lj_payment_id = '$payment_id'");
        if ($payment_recipt_data) {
            return array('status' => true, 'message' => "Successfully Update Receipt");
        } else {
            return array('status' => false, 'message' => "Failed To Update Receipt");
        }

    }
    public function get_tag_data()
    {

        $get_tag_data = $this->db->query('SELECT tag.tag_id,tag.tag_desc from lj_barcode_tag_mt tag  where tag.active_yn = 1')->result_array();
        if ($get_tag_data) {
            return array('status' => true, 'tag_data' => $get_tag_data);
        } else {
            return array('status' => false, 'message' => "No Data Found In Tag remarks DT");
        }

    }
    public function get_tag_remarks_data()
    {

        $tag_id = $this->input->post('tag_id');
        if ($tag_id == 0) {
            $tag_qry = "";
        } else {
            $tag_qry = "and dt.tag_id = $tag_id ";
        }
        $merchId = $this->input->post('merchId');
        if ($merchId == 0) {
            $mer_qry = "";
        } else {
            $mer_qry = "and mt.merchant_id = $merchId";
        }
        $from = $this->input->post("startDate");
        $to = $this->input->post("endDate");
        $get_remarks_data = $this->db->query("SELECT dt.dt_id,
            dt.barcode_no,
            dt.admin_remarks,
            dt.tag_id,
            dt.tag_remarks,
            dt.tag_date,
            dt.tag_by,
            em.user_name,
            (select *
               from (select b.bin_type || '-' || b.bin_no bin_name
                       from lz_loc_trans_log l, bin_mt b
                      where l.loc_trans_id =
                            (select max(loc_trans_id)
                               from lz_loc_trans_log
                              where barcode_no = dt.barcode_no)
                        and b.bin_id = l.new_loc_id
                     union all
                     select b.bin_type || '-' || b.bin_no bin_name
                       from lz_merchant_barcode_dt d, bin_mt b
                      where b.bin_id = d.bin_id
                        and d.barcode_no = dt.barcode_no)
              where rownum = 1) bin_name,
            tag.tag_desc,
            m.buisness_name merchant_name
       from lz_merchant_barcode_dt dt,
            employee_mt            em,
            lj_barcode_tag_mt      tag,
            lz_merchant_barcode_mt mt,
            lz_merchant_mt         m
      where dt.mt_id = mt.mt_id
        and mt.merchant_id = m.merchant_id
        $mer_qry
        $tag_qry
        and dt.tag_by = em.employee_id
        and dt.tag_id = tag.tag_id
        and dt.discard <> 2
        and dt.tag_date between
        TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
        TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')
        -- and ROWNUM <= 100
      order by dt.tag_date desc
     ")->result_array();

        if ($get_remarks_data) {

            return array('status' => true, 'tag_remark_data' => $get_remarks_data);
        } else {
            return array('status' => false, 'message' => "No Data Found");
        }

    }
    public function save_admin_remarks()
    {
        $remarks = $this->input->post('admin_remarks');
        $user_id = $this->input->post('userId');
        $det_id = $this->input->post('det_id');

        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = str_replace(array("`,'"), "", $remarks);
        $remarks = str_replace(array("'"), "''", $remarks);

        $qry = $this->db->query("UPDATE lz_merchant_barcode_dt d set d.admin_id ='$user_id' , d.admin_remarks = '$remarks', d.ADMIN_AT = sysdate  where d.dt_id = $det_id");
        if ($qry == true) {
            return array("status" => true, 'message' => "Admin Remarks Updated Successfully");
        } else {
            return array("status" => false, 'message' => "Admin Remarks Not Updated Successfully");
        }

    }
    public function delete_tag_barcode_remarks()
    {
        $remarks = $this->input->post('admin_remarks');
        $user_id = $this->input->post('userId');
        $det_id = $this->input->post('det_id');

        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = str_replace(array("`,'"), "", $remarks);
        $remarks = str_replace(array("'"), "''", $remarks);

        $qry = $this->db->query("UPDATE lz_merchant_barcode_dt dt set dt.discard = 2,dt.admin_id =$user_id , dt.admin_at = sysdate , dt.admin_remarks = '$remarks'
        where dt.dt_id = $det_id");
        if ($qry == true) {
            return array("status" => true, 'message' => "Deleted Successfully");
        } else {
            return array("status" => false, 'message' => "Failed To Delete Barcode");
        }

    }
    public function custom_order_barrcode()
    {
        $barcode = trim(html_escape($this->input->post('barcode')));
        $qry = $this->db->query("SELECT mt.lz_pos_mt_id from lz_barcode_mt mt where mt.barcode_no = '$barcode'")->row();

        if ($qry->LZ_POS_MT_ID) {

            return array("success" => true, 'message' => "Please enter this barcode in POS");
        } else {
            return array("success" => false, 'message' => "Please enter this barcode in POS");
        }
    }
    public function save_custom_order_barrcode()
    {
        $order_id = trim(html_escape($this->input->post('order_id')));
        $barcodes = $this->input->post('barcodes');

        foreach ($barcodes as $key => $bar) {
            $this->db->query("UPDATE lz_barcode_mt dt set dt.order_dt_id = $order_id where dt.barcode_no = $bar");

        }

        return array("success" => true, 'message' => "Update");

    }
    //   public function Get_Merchant_Returns(){
    //       // $from = $this->input->post("startDate");
    //       // $to = $this->input->post("endDate");
    //       $merchId = $this->input->post("merchId");
    //       $invoice_id = $this->input->post("invoice_id");
    //       // $skipDate = $this->input->post('skipDate');
    //       $pack_inv_detail = "SELECT item_id,
    //      MERCHANT_ID,
    //      TRACKING_NUMBER,
    //      ORDER_ID,
    //      /*ORDER_PACKING_ID,*/
    //      SALES_RECORD_NUMBER recrd_no,
    //      EBAY_ITEM_ID,
    //      ITEM_TITLE,
    //      sale_price,
    //      QTY,
    //      ship_cost,
    //      PACKING_COST,
    //      SERVICE_COST,
    //      EBAY_FEE,
    //      MARKTPLACE,
    //      NVL(SHIP_COST + PACKING_COST + SERVICE_COST + EBAY_FEE + MARKTPLACE,
    //          0) TOTAL_CHARGE
    // FROM (SELECT sd.ORDER_ID,
    //              max(sd.tracking_number) tracking_number,
    //              /*--max(M.ORDER_PACKING_ID) ORDER_PACKING_ID,*/
    //              max(sd.extendedorderid) SALES_RECORD_NUMBER,
    //              MAX(sd.item_id) EBAY_ITEM_ID,
    //              MAX(sd.item_title) ITEM_TITLE,
    //              NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) SALE_PRICE,
    //              MAX(sd.quantity) QTY,
    //              NVL(MAX(sd.shippinglabelrate),0) SHIP_COST,
    //              SUM(NVL(DT.PACKING_COST, 0)) PACKING_COST,
    //              MAX(NVL(sd.SERVICE_COST, 0)) SERVICE_COST,
    //              DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
    //                     'dfwonline',
    //                     NVL(MAX(sd.ebay_fee_perc), 0),
    //                     0) EBAY_FEE,
    //              DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
    //                     'dfwonline',
    //                     (0.07 * (MAX(sd.SALE_PRICE) * MAX(sd.quantity))),
    //                     0) MARKTPLACE, /* (0.07 * (MAX(M.SALE_PRICE) * MAX(M.QTY))) MARKTPLACE,*/
    //              MAX(ACOUNT_DET.ACOUNT_NAME) ACC_NAME,
    //              MAX(ACOUNT_DET.merchant_id) MERCHANT_ID,
    //              max(ACOUNT_DET.item_id) item_id
    //         FROM LZ_SALESLOAD_DET SD,
    //              /*--LJ_ORDER_PACKING_MT M,*/
    //              LJ_ORDER_PACKING_DT DT,
    //              (SELECT MM.EBAY_ITEM_ID,
    //                      max(dd.merchant_id)merchant_id,
    //                      MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
    //                      MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
    //                      max(mm.item_id) item_id
    //                 FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
    //                WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
    //                  AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
    //                GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET
    //        WHERE sd.order_id in (select d.order_id
    //                                from ebay_list_mt       e,
    //                                     lj_merhcant_acc_dt a,
    //                                     lz_salesload_det   d
    //                               where a.acct_id = e.lz_seller_acct_id
    //                                 and d.item_id = e.ebay_item_id
    //                                 and a.merchant_id = '9'
    //                               group by d.order_id)
    //          /*and sd.order_id = m.order_id(+)*/
    //          and sd.orderstatus = 'Completed'
    //          and sd.return_id is null
    //          and sd.order_id = dt.order_id(+)
    //          AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
    //         ";
    //       if (!empty($invoice_id)) {
    //           $pack_inv_detail .= " AND sd.INVOICE_ID = '$invoice_id'";
    //       } else {
    //           $pack_inv_detail .= " AND sd.INVOICE_ID IS NULL ";
    //       }
    //       // if (empty($skipDate)) {
    //       //     $pack_inv_detail .= "  AND sd.SALE_DATE  BETWEEN TO_DATE('$from " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND TO_DATE('$to " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS')";

    //       // }

    //       $pack_inv_detail .= "   GROUP BY sd.ORDER_ID) order by ORDER_ID desc ";
    //       $pack_inv_detail = $this->db->query($pack_inv_detail)->result_array();
    //       if(count($pack_inv_detail) > 0) {
    //           $images = $this->get_ebay_pictures($pack_inv_detail);
    //           return array('exist' => true, 'data' => $pack_inv_detail, 'images' => $images['uri']);
    //       }else {
    //           return array('exist' => false, 'data' => array(), 'images' => array());
    //       }
    //   }

    public function get_pl_report()
    {
        $requestData = $_REQUEST;
        $get_param = strtoupper($this->input->post('param'));
        $from = $this->input->post('startDate');
        $to = $this->input->post('endDate');

        $merchId = strtoupper($this->input->post('merchId'));
        // $merchId = 2;
        $columns = array(
            // datatable column index  => database column name
            0 => 'EBAY_ITEM_ID',
            1 => 'ITEM_TITLE',
            2 => 'BUYER_FULLNAME',
            3 => 'ORDER_ID',
            4 => 'COST',
            5 => 'SALE_PRICE',
            6 => 'QTY',
            7 => 'SHIP_COST',
            8 => 'EBAY_FEE',
            9 => 'PACKING_COST',
            10 => 'PAYPL_FEE',
            11 => 'TOTAL_EXPENSE',
            12 => 'PL',
            13 => 'PL_PERC',
            14 => 'ACC_NAME',
            15 => 'SALE_DATE',
        );
        $sql = "SELECT /*item_id, MERCHANT_ID,*/
        --TRACKING_NUMBER, /*ORDER_PACKING_ID,*/
         SALES_RECORD_NUMBER order_id,
         EBAY_ITEM_ID,
         ITEM_TITLE,
         ROUND(cost,2)COST,
         ROUND(sale_price,2) sale_price,
         QTY,
         ROUND(ship_cost,2) ship_cost,
         ROUND(PACKING_COST,2)PACKING_COST,
         ROUND(SERVICE_COST,2) SERVICE_COST,
         ROUND(EBAY_FEE,2)EBAY_FEE,
         ROUND(paypl_fee,2) paypl_fee ,
         buyer_fullname,

         ROUND(NVL(cost + SHIP_COST + PACKING_COST + SERVICE_COST + EBAY_FEE + paypl_fee,
             0),2) TOTAL_expense,
         ROUND(sale_price -
         NVL(cost + SHIP_COST + PACKING_COST + SERVICE_COST + EBAY_FEE + paypl_fee,
             0),2) pl,
         ROUND(((sale_price -
         NVL(cost + SHIP_COST + PACKING_COST + SERVICE_COST + EBAY_FEE + paypl_fee,
             0)) / sale_price) *100,2 ) PL_PERC,
         sale_date,
         ACC_NAME,
         lz_manifest_id
          FROM (SELECT sd.ORDER_ID,
                       nvl(max(manif_cost.manif_cost),0) manif_cost,
                       nvl(max(manif_cost.manif_cost * sd.quantity)  ,0) cost,
                       max(sd.order_id) get_order_id,
               max(sd.tracking_number) tracking_number,
               max(sd.extendedorderid) SALES_RECORD_NUMBER,
               MAX(sd.item_id) EBAY_ITEM_ID,
               MAX(sd.item_title) ITEM_TITLE,
               NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) SALE_PRICE,
               round(NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) * (0.0225), 2) paypl_fee,

               MAX(sd.quantity) QTY,

               NVL(MAX(sd.shippinglabelrate), 0) SHIP_COST,
               SUM(NVL(DT.PACKING_COST, 0)) PACKING_COST,
               MAX(NVL(sd.SERVICE_COST, 0)) SERVICE_COST,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      NVL(MAX(sd.ebay_fee_perc), 0),
                      0) EBAY_FEE,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      (0.07 * (MAX(sd.SALE_PRICE) * MAX(sd.quantity))),
                      0) MARKTPLACE, /* (0.07 * (MAX(M.SALE_PRICE) * MAX(M.QTY))) MARKTPLACE,*/
               MAX(ACOUNT_DET.ACOUNT_NAME) ACC_NAME,
               MAX(ACOUNT_DET.merchant_id) MERCHANT_ID,
               max(ACOUNT_DET.item_id) item_id,
               max(sd.buyer_fullname) buyer_fullname,
               max(sd.sale_date) sale_date,
               max(manif_cost.lz_manifest_id) lz_manifest_id
          from lz_salesload_det sd,
               LJ_ORDER_PACKING_DT DT,
               (SELECT MM.EBAY_ITEM_ID,
                       max(dd.merchant_id) merchant_id,
                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                       max(mm.item_id) item_id
                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET,
     /*  (select max(mt.cost) cost,

               max(b.order_id) order_id
          from lz_purch_item_dt d, lz_barcode_mt b, lz_purch_item_mt mt
         where d.barcode_no = b.barcode_no
           and d.purch_mt_id = mt.purch_mt_id
              --and b.ebay_item_id = 264599690019
           and b.order_id is not null
         group by b.order_id) barc_cost*/
         ( select /*bh.item_id, bh.barcode_no, bh.lz_manifest_id, i.item_code,*/sum(o.po_detail_retial_price) /COUNT(1) manif_cost,bh.order_id,,max(o.lz_manifest_id) lz_manifest_id
  from lz_barcode_mt bh, items_mt i, lz_manifest_det o
 where /*bh.order_id = '264547503760-2541581429016'
   and */bh.item_id = i.item_id
   and i.item_code = o.laptop_item_code
   AND BH.LZ_MANIFEST_ID = O.LZ_MANIFEST_ID
   and bh.order_id is not null
   group by bh.order_id) manif_cost
 where  sd.orderstatus = 'Completed'
   AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
/* and sd.order_id = barc_cost.order_id(+)*/
   and sd.order_id = manif_cost.order_id(+)
   and sd.order_id = dt.order_id(+)
   and sd.order_id in
       (select d.order_id
          from ebay_list_mt e, lj_merhcant_acc_dt a, lz_salesload_det d
         where a.acct_id = e.lz_seller_acct_id
           and d.item_id = e.ebay_item_id
           and a.merchant_id = 1
         group by d.order_id)
   and sd.return_id is null";
        $searchValue = $requestData['search']['value'];
        $searchValue = trim(str_replace("  ", ' ', $searchValue));
        $searchValue = str_replace(array("`,'"), "", $searchValue);
        $searchValue = str_replace(array("'"), "''", $searchValue);
        $str = explode(' ', $searchValue);
        if (!empty($searchValue)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $sql .= " and ( (sd.item_title) LIKE '%$key%' ";
                        // $sql .= " and (sd.buyer_fullname) LIKE '%$key%' ";
                    } else {
                        $sql .= " AND (sd.item_title) LIKE '%$key%' ";
                        // $sql .= " AND (sd.buyer_fullname) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $sql .= " and ((sd.item_title) LIKE '%$searchValue%' ";
                // $sql .= " AND (sd.buyer_fullname) LIKE '%$searchValue%' ";
            }

            $sql .= " OR (sd.item_id) LIKE '%$searchValue%'
    OR (sd.SERVICE_COST) LIKE '%$searchValue%'
    OR (sd.ORDER_ID) LIKE '%$searchValue%'
    OR (sd.extendedorderid) LIKE '%$searchValue%'
OR (DT.PACKING_COST) LIKE '%$searchValue%'
OR (sd.SALE_PRICE) LIKE '%$searchValue%'
OR (sd.buyer_fullname) LIKE '%$searchValue%'
OR (sd.sale_date) LIKE '%$searchValue%'
OR (sd.ebay_fee_perc) LIKE '%$searchValue%')";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }
        // if (!empty($requestData['search']['value'])) {
        //     $searchValue = trim($requestData['search']['value']);
        //     $searchValue = trim(str_replace("  ", ' ', $searchValue));
        //     $searchValue = str_replace(array("`,'"), "", $searchValue);
        //     $searchValue = str_replace(array("'"), "''", $searchValue);
        //     $searchValue = str_replace(array("."), "", $searchValue);
        //     $sql .= " AND(sd.item_id  LIKE '%" . $searchValue . "%'  OR sd.item_title LIKE TO_CHAR('%" . $searchValue . "%') OR sd.buyer_fullname LIKE TO_CHAR('%" . $searchValue . "%') OR sd.ORDER_ID LIKE '%" . $searchValue . "%' OR sd.quantity LIKE '%" . $searchValue . "%' OR sd.sale_date LIKE '%" . $searchValue . "%' OR (DT.PACKING_COST LIKE '%" . $searchValue . "%' OR sd.SERVICE_COST  LIKE '%" . $searchValue . "%' OR sd.ebay_fee_perc LIKE '%" . $searchValue . "%' OR sd.SALE_PRICE LIKE '%" . $searchValue . "%' ) ";
        // }

        $sql .= " AND sd.SALE_DATE BETWEEN
       TO_DATE('$from 00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
       TO_DATE('$to 23:59:59', 'YYYY-MM-DD HH24:MI:SS')


    GROUP BY sd.ORDER_ID)
 --where cost > 0
 order by COST desc
";
        $query = $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;

        $sqlLength = "SELECT  * FROM (SELECT  q.*, rownum rn FROM  ($sql) q )";

        $sqlLength .= " WHERE   ROWNUM <= " . $requestData['length'] . " AND rn>= " . $requestData['start'];

        if (!empty($columns[$requestData['order'][0]['column']])) {
            $sqlLength .= " ORDER BY  " . $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        }

        /*=====  End of For Oracle 12-c  ======*/
//   $query = $this->db->query($sql);

        $lot_detail_data = $this->db->query($sqlLength)->result_array();
        $data = array();
        if (count($lot_detail_data) >= 1) {

            // $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();

            // $uri = $this->get_barcodes_pictures($lot_detail_data, $conditions);
            // $images = $uri['uri'];
            // '<span style="float: right;"> "$" . number_format((float) @$rows["SALE_PRICE"], 2, ".", ",")</span>',

            // '$' . number_format((float) @$rows['COST'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['SHIP_COST'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['EBAY_FEE'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['PACKING_COST'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['PAYPL_FEE'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['TOTAL_EXPENSE'], 2, '.', ','),
            //         '$ ' . number_format((float) @$rows['PL'], 2, '.', ','),
            //         number_format((float) @$rows['PL_PERC'], 2, '.', ',') . " %",
            foreach ($lot_detail_data as $rows) {
                $sale_price = number_format((float) @$rows['SALE_PRICE'], 2, '.', ',');
                $cost = number_format((float) @$rows['COST'], 2, '.', ',');
                $ship_cost = number_format((float) @$rows['SHIP_COST'], 2, '.', ',');
                $ebay_fee = number_format((float) @$rows['EBAY_FEE'], 2, '.', ',');
                $packing_cost = number_format((float) @$rows['PACKING_COST'], 2, '.', ',');
                $paypl_fee = number_format((float) @$rows['PAYPL_FEE'], 2, '.', ',');
                $total_expense = number_format((float) @$rows['TOTAL_EXPENSE'], 2, '.', ',');
                $pl = number_format((float) @$rows['PL'], 2, '.', ',');
                $pl_perc = number_format((float) @$rows['PL_PERC'], 2, '.', ',');
                $data[] = array(
                    "<a href=http://www.ebay.com/itm/" . @$rows['EBAY_ITEM_ID'] . " target='_blank'>
                    " . @$rows['EBAY_ITEM_ID'] . "
                   </a>",
                    @$rows['ITEM_TITLE'],
                    @$rows['BUYER_FULLNAME'],
                    @$rows['ORDER_ID'],
                    "<span style='float: right;'>$ " . @$cost . "</span>",
                    "<span style='float: right;'>$ " . @$sale_price . "</span>",
                    @$rows['QTY'],
                    "<span style='float: right;'>$ " . @$ship_cost . "</span>",
                    "<span style='float: right;'>$ " . @$ebay_fee . "</span>",
                    "<span style='float: right;'>$ " . @$packing_cost . "</span>",
                    "<span style='float: right;'>$ " . @$paypl_fee . "</span>",
                    "<span style='float: right;'>$ " . @$total_expense . "</span>",
                    "<span style='float: right;'>$ " . @$pl . "</span>",
                    "<span style='float: right;'>" . @$pl_perc . " %</span>",

                    @$rows['ACC_NAME'],
                    @$rows['SALE_DATE'],

                );
            }

            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered),
                // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('lot_detail_data' => $output, "images" => $images, 'status' => true);
        } else {
            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('lot_detail_data' => $lot_detail_data, 'status' => false, "images" => array());
        }

    }
    public function get_best_offers_mt()
    {
        $merchant_id = $this->input->post('mid');
        $user_id = $this->input->post('user_id');
        $check = $this->db->query("SELECT e.role_id from employee_mt e where e.employee_id = $user_id")->row();

        if ($check->ROLE_ID == 1) {
            $qry = $this->db->query("SELECT BO.LJ_OFFER_ID,
            BO.EBAY_ID,
            BO.BUYITNOWPRICE,
            BO.ITEM_TITLE,
            BO.CONDITIONID,
            BO.CONDITIONDISPLAYNAME,
            BO.ACCOUNT_ID,
            BO.CURRENCYID,
            BO.INSERTED_DATE,
            BO.SELLER_NAME,
            MR.ACCT_ID,
            MR.MERCHANT_ID,
            (select max(BOD.OFFER_PRICE) from LJ_BEST_OFFER_ITEMS_DT bod where BO.LJ_OFFER_ID = BOD.LJ_OFFER_ID)OFFER_PRICE
            from LJ_BEST_OFFER_ITEMS_MT BO,
            lj_merhcant_acc_dt MR
            WHERE BO.ACCOUNT_ID = MR.ACCT_ID
            ORDER BY BO.LJ_OFFER_ID DESC")->result_array();
        } else {
            $qry = $this->db->query("SELECT BO.LJ_OFFER_ID,
            BO.EBAY_ID,
            BO.BUYITNOWPRICE,
            BO.ITEM_TITLE,
            BO.CONDITIONID,
            BO.CONDITIONDISPLAYNAME,
            BO.ACCOUNT_ID,
            BO.CURRENCYID,
            BO.INSERTED_DATE,
            BO.SELLER_NAME,
            MR.ACCT_ID,
            MR.MERCHANT_ID,
            (select max(BOD.OFFER_PRICE) from LJ_BEST_OFFER_ITEMS_DT bod where BO.LJ_OFFER_ID = BOD.LJ_OFFER_ID)OFFER_PRICE,
            (select count(BOD.lj_offer_id) from LJ_BEST_OFFER_ITEMS_DT bod where BO.LJ_OFFER_ID = BOD.LJ_OFFER_ID)total_offers
            from LJ_BEST_OFFER_ITEMS_MT BO,
            lj_merhcant_acc_dt MR
            WHERE BO.ACCOUNT_ID = MR.ACCT_ID
            AND MR.MERCHANT_ID = $merchant_id
            ORDER BY BO.LJ_OFFER_ID DESC")->result_array();
        }
        if (count($qry) > 0) {
            return array("data" => $qry, 'status' => true);
        } else {
            return array("data" => array(), 'status' => false);}
    }
    public function get_best_offers_details()
    {
        $offer_id = $this->input->post('offer_id');
        $qry = $this->db->query("SELECT dt.offer_dt_id,
        dt.lj_offer_id,
        dt.bestofferid,
        TO_CHAR(dt.expirationtime, 'dd/mm/YY HH24:MI:SS' ) expirationtime,
        dt.buyer_email,
        dt.buyer_feedbackscore,
        dt.buyer_registrationdate,
        dt.buyer_userid,
        dt.buyer_shippingaddress,
        dt.offer_price,
        dt.offer_status,
        dt.quantity,
        dt.bestoffercodetype,
        dt.offer_role,
        dt.inserted_date from LJ_BEST_OFFER_ITEMS_DT dt WHERE dt.LJ_OFFER_ID = $offer_id order by dt.offer_price desc")->result_array();
        if (count($qry) > 0) {
            return array("data" => $qry, 'status' => true);
        } else {
            return array("data" => array(), 'status' => false);
        }
    }
    public function get_ebay_messages()
    {
        $qry = $this->db->query("SELECT * FROM LJ_MSG_MT MT WHERE REPLIED_BY is null  ORDER BY MT.LJ_MSG_ID DESC ")->result_array();
        if (count($qry) > 0) {
            return array("data" => $qry, "status" => true);
        } else {
            return array("data" => array(), "status" => false);

        }
    }
    public function get_message_attachment()
    {
        $message_id = $this->input->post('messageID');
        $qry = $this->db->query("SELECT * FROM LJ_MSG_DT MT WHERE MESSAGE_ID = $message_id")->result_array();
        if (count($qry) > 0) {
            return array("data" => $qry, "status" => true);
        } else {
            return array("data" => array(), "status" => false);

        }
    }
    public function get_sent_messages()
    {
        $qry = $this->db->query("SELECT * FROM LJ_MSG_MT MT WHERE REPLIED_BY is not null ORDER BY MT.LJ_MSG_ID DESC ")->result_array();
        if (count($qry) > 0) {
            return array("data" => $qry, "status" => true);
        } else {
            return array("data" => array(), "status" => false);

        }
    }
    public function delete_ebay_message()
    {
        $id = $this->input->post('id');

        $data = $this->db->query("DELETE FROM LJ_MSG_MT WHERE LJ_MSG_ID ='$id' ");

        if ($data) {
            return array('status' => true, 'message' => "Deleted Successfully");
        } else {
            return array('status' => false, 'message' => "Failed To Delete");
        }
    }

    public function updateReturnShip()
    {
        $returnShip = $this->input->post('returnShip');
        $returnShip = trim(str_replace("  ", ' ', $returnShip));
        $returnShip = trim(str_replace(array("'"), "''", $returnShip));

        $lz_salesload_det_id = $this->input->post('lz_salesload_det_id');

        $data = $this->db->query("update lz_salesload_det d set d.RETURN_SHIP_AMOUNT = '$returnShip'  where d.lz_salesload_det_id = $lz_salesload_det_id");
        // var_dump($returnShip,$sale_record_no);
        // exit;

        // $data = $this->db->query("DELETE FROM LJ_MSG_MT WHERE LJ_MSG_ID ='$id' ");

        if ($data) {
            return array('status' => true, 'message' => "Updated Successfully");
        } else {
            return array('status' => false, 'message' => "Not Updated ! ERROR");
        }
    }

    public function save_refund_amount()
    {
        $order_id = $this->input->post('order_id');
        $user_id = $this->input->post('user_id');
        $startDate = $this->input->post('startDate');
        $refund_amount = $this->input->post('refund_amount');
        $refund_remarks = $this->input->post('refund_remarks');

        $refund_remarks = trim(str_replace("  ", ' ', $refund_remarks));
        $refund_remarks = trim(str_replace(array("'"), "''", $refund_remarks));

        $ins_date = "TO_DATE('" . $startDate . "', 'YYYY-MM-DD HH24:MI:SS')";

        $data = $this->db->query("UPDATE lz_salesload_det set refund_amount = '$refund_amount',
        refund_remarks = '$refund_remarks' , refund_by = '$user_id', refund_date = $ins_date where order_id = '$order_id'");

        if ($data) {
            return array('status' => true, 'message' => "Refunded Successfully");
        } else {
            return array('status' => false, 'message' => "Failed To Refund");
        }
    }
    public function update_refund_amount()
    {
        $order_id = $this->input->post('order_id');
        $refund_amount = $this->input->post('refund_amount');
        $refund_remarks = $this->input->post('refund_remarks');

        $refund_remarks = trim(str_replace("  ", ' ', $refund_remarks));
        $refund_remarks = trim(str_replace(array("'"), "''", $refund_remarks));

        $data = $this->db->query("UPDATE lz_salesload_det set refund_amount = '$refund_amount',
        refund_remarks = '$refund_remarks' where order_id = '$order_id'");

        if ($data) {
            return array('status' => true, 'message' => "Refund Update Successfully");
        } else {
            return array('status' => false, 'message' => "Failed To Update Refund");
        }
    }
    public function get_partial_refund_data()
    {
        $query = $this->db->query("SELECT DET.ITEM_TITLE,
        DET.SALE_RECORD_NO,
        DET.BUYER_ADDRESS1,
        DET.refund_amount,
        DET.refund_remarks,
        DET.ORDER_ID,
        DET.EXTENDEDORDERID,
        DET.LZ_SALELOAD_ID,
        DET.LZ_SELLER_ACCT_ID,
        DET.USER_ID,
        DET.BUYER_FULLNAME,
        DET.QUANTITY,
        DET.SALE_PRICE,
        DET.TOTAL_PRICE,
        DET.SALE_DATE,
        DET.TRACKING_NUMBER,
        DET.BUYER_ZIP,
        -- DET.BARCODES,
        DET.SELL_ACCT_DESC
    FROM (SELECT DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.refund_amount,
        DET.refund_remarks,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                -- TO_CHAR((SELECT REPLACE(REPLACE(XMLAGG(XMLELEMENT(A, CDT.BARCODE_NO) ORDER BY CDT.BARCODE_NO DESC NULLS LAST)
                --                                .GETCLOBVAL(),
                --                                '<A>',
                --                                ''),
                --                        '</A>',
                --                        ', ') AS BARCODE_NO
                --           FROM LJ_CANCELLATION_DT CDT
                --          WHERE CDT.ORDER_ID = DET.ORDER_ID)) BARCODES,
                AA.SELL_ACCT_DESC
            FROM LZ_SALESLOAD_DET   DET,
                LZ_SALESLOAD_MT    MT,
                LZ_SELLER_ACCTS    AA
            WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
            AND DET.REFUND_AMOUNT is not null
            AND MT.LZ_SELLER_ACCT_ID = AA.LZ_SELLER_ACCT_ID) DET
            GROUP BY DET.ITEM_TITLE,
                DET.SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.refund_amount,
                DET.refund_remarks,
                DET.ORDER_ID,

                DET.EXTENDEDORDERID,
                DET.LZ_SALELOAD_ID,
                DET.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                -- DET.BARCODES,
                DET.SELL_ACCT_DESC")->result_array();
        if (count($query) > 0) {
            return array("records" => $query, "found" => true);
        } else {
            return array("records" => array(), "found" => false);

        }

    }
    public function get_listing_log()
    {
        $to = $this->input->post('startDate');
        $from = $this->input->post('endDate');
        $merchId = strtoupper($this->input->post('merchId'));
        if ($merchId) {
            $qry = $this->db->query("SELECT * from lj_listing_notification n where upper(MERCHANT_NAME) = '$merchId'and  n.TIMESTAMP between
        TO_DATE('$to " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
        TO_DATE('$from " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') and  n.notificationeventname in ('ItemListed','ItemRevised')")->result_array();
        } else {
            $qry = $this->db->query("SELECT * from lj_listing_notification n where
        n.TIMESTAMP between
        TO_DATE('$to " . "00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
        TO_DATE('$from " . "23:59:59', 'YYYY-MM-DD HH24:MI:SS') and
        n.notificationeventname in ('ItemListed','ItemRevised')")->result_array();
        }
        if (count($qry) > 0) {
            return array('data' => $qry, 'status' => true);
        } else {
            return array('data' => array(), 'status' => false);

        }
    }
    public function save_subscriber()
    {
        $email = $this->input->post('email');
        $portal_id = $this->input->post('portal_id');
        $result = $this->db->query("SELECT * from Lj_subscriber_mt WHERE subs_email = '$email' AND subs_portal_id = $portal_id")->result_array();
        if (count($result) > 0) {
            return array('message' => "Successfully Subscribe", 'status' => true);
        } else {
            $get_mt_pk = $this->db->query("SELECT get_single_primary_key('Lj_subscriber_mt','SUBS_ID') SUBS_ID FROM DUAL");
            $get_mt_pk = $get_mt_pk->result_array();
            $subs_id = $get_mt_pk[0]['SUBS_ID'];

            $ins_mt_qry = "INSERT INTO Lj_subscriber_mt(subs_id, subs_email, subs_portal_id) VALUES ($subs_id,'$email',$portal_id)";
            $ins_mt_qry = $this->db->query($ins_mt_qry);
            if ($ins_mt_qry) {
                return array('message' => "Successfully Subscribe", 'status' => true);
            } else {
                return array('message' => "Failed", 'status' => false);
            }
        }
    }
    public function update_qc_info()
    {
        $user_id = $this->input->post('user_id');
        $qc_check = $this->input->post('qc_check');
        $qc_remarks = $this->input->post('qc_remarks');
        $seed_id = $this->input->post('seed_id');

        $qc_remarks = trim(str_replace("  ", ' ', $qc_remarks));
        $qc_remarks = trim(str_replace(array("'"), "''", $qc_remarks));

        $query = $this->db->query("UPDATE lz_item_seed set QC_CHECK = '$qc_check' , QC_REMARKS = '$qc_remarks' , QC_BY = '$user_id' , QC_DATE = SYSDATE   WHERE SEED_ID ='$seed_id'");
        if ($query) {
            return array('message' => "QC Info Updated", 'status' => true);
        } else {
            return array('message' => "Failed To Update QC Info", 'status' => false);

        }
    }

}
