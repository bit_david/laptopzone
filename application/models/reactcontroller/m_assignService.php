<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Single Entry Model
 */

class m_assignService extends CI_Model
{
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function Get_Merchant_Service_DropDown()
    {
        $data = $this->db->query("SELECT --s.service_id,
        r.ser_rate_id,
        --r.service_type,
        s.service_desc || ' (' ||
        DECODE(r.service_type,
               '1',
               'per barcde',
               '2',
               'hourly',
               '3',
               'per_order',
               '4',
               'per_bin',
               '5',
               'per_pallet',
               '6',
               'per_cubic_feet') || ')' ser_name
       /*r.charges rate,
       r.excess_qty_rate,
       r.created_date */
         FROM lj_services s, lj_service_rate r
        WHERE s.service_id = r.service_id
        ORDER BY s.service_id ASC
       ");
        if ($data->num_rows() > 0) {
            return array('status' => true, 'data' => $data->result_array());
        } else {
            return array('status' => false, 'data' => array(), 'message' => 'No Record Found');
        }

    }

    public function Get_Latest_Merchant_Service_Add()
    {
        $data = $this->db->query("SELECT * from (SELECT m.buisness_name    merchant_name,
        s.service_desc     assigned_service,
        ms.merchant_ser_id merchant_ser_id,

        decode(sr.service_type,
               '1',
               'per barcde',
               '2',
               'hourly',
               '3',
               'per_order',
               '4',
               'per_bin',
               '5',
               'per_pallet',
               '6',
               'per_cubic_feet') ser_type,
        ms.rate,
        ms.excess_qty_rate
   FROM lj_merchant_service ms,
        lz_merchant_mt      m,
        lj_service_rate     sr,
        lj_services         s

  WHERE ms.merchant_id = m.merchant_id
    AND ms.ser_rate_id = sr.ser_rate_id
    AND sr.service_id = s.service_id
    ORDER BY ms.merchant_ser_id DESC) WHERE rownum  = 1")->result_array();
        return $data;
    }

    public function Save_Merchant_Services()
    {
        $merchant_id = $this->input->post('merchant_id');
        $service_id = $this->input->post('service_id');
        $service_rate = $this->input->post('service_rate');
        $service_rate = str_replace('$ ', '', $service_rate);
        $created_by = $this->input->post('userId');
        $access_qty_rate = $this->input->post('access_qty_rate');
        $access_qty_rate = str_replace('$ ', '', $access_qty_rate);
        if (!empty($merchant_id)) {
            $mer_id = $merchant_id['value'];
        }

        if (!empty($service_id)) {
            $ser_rate_id = $service_id['value'];
            $ser_name = $service_id['label'];
            $select = $this->db->query("SELECT MERCHANT_SER_ID FROM lj_merchant_service WHERE  MERCHANT_ID = '$mer_id' AND SER_RATE_ID = '$ser_rate_id'");
            if ($select->num_rows() > 0) {
                return array('status' => false, 'message' => 'This Service (' . $ser_name . ') is Already Assigned');
                // $error[] = 'This Service (' . $ser_name . ') is Already Assigned';
            } else {
                $this->db->query("INSERT INTO lj_merchant_service (MERCHANT_SER_ID, MERCHANT_ID, SER_RATE_ID, CREATED_BY, DATED, RATE, EXCESS_QTY_RATE) VALUES (get_single_primary_key('lj_merchant_service','MERCHANT_SER_ID'),'$mer_id', '$ser_rate_id', '$created_by', sysdate, '$service_rate', '$access_qty_rate')");
                $data = $this->Get_Latest_Merchant_Service_Add();
                return array('status' => true, 'message' => 'Service Assigned', 'data' => $data);
            }
        } else {
            return array('status' => false, 'message' => 'Please Select The Service');
        }

    }

    public function Get_Merchant_Service_Detail()
    {
        $data = $this->db->query("SELECT m.buisness_name merchant_name,
        s.service_desc  assigned_service,
        ms.merchant_ser_id merchant_ser_id,

        decode(sr.service_type,
               '1',
               'per barcde',
               '2',
               'hourly',
               '3',
               'per_order',
               '4',
               'per_bin',
               '5',
               'per_pallet',
               '6',
               'per_cubic_feet') ser_type,
        ms.rate,
        ms.excess_qty_rate
   FROM lj_merchant_service ms,
        lz_merchant_mt      m,
        lj_service_rate     sr,
        lj_services         s

  WHERE ms.merchant_id = m.merchant_id
    AND ms.ser_rate_id = sr.ser_rate_id
    AND sr.service_id = s.service_id
 ");
        if ($data->num_rows() > 0) {
            return array('status' => true, 'data' => $data->result_array());
        } else {
            return array('status' => false, 'data' => array());
        }

    }

    public function Delete_Merchant_Service()
    {
        $merchant_ser_id = $this->input->post('merchant_ser_id');

        $delete = $this->db->query("DELETE FROM lj_merchant_service WHERE merchant_ser_id = '$merchant_ser_id'");
        if ($delete) {
            return array('status' => true, 'message' => 'Service Deleted Successfully');
        } else {
            return array('status' => false, 'message' => 'Service Not Deleted Successfully');
        }
    }
}
