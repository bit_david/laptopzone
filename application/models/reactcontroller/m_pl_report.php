<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * m_pl_report  Model
 */

class m_pl_report extends CI_Model
{
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function Get_Summary_Report()
    {
        $from = $this->input->post('startDate');
        $to = $this->input->post('endDate');
        $merchId = strtoupper($this->input->post('merchId'));
        $total_sale_object = '';
        $qry = "SELECT
  NVL(SUM(ROUND(cost, 2)), 0.00) COST,
        NVL(SUM(ROUND(sale_price, 2)), 0.00) sale_price,
       NVL(SUM(QTY), 0.00) QTY,
        NVL(SUM(ROUND(ship_cost, 2)), 0.00) ship_cost,
       NVL(SUM(ROUND(PACKING_COST, 2)), 0.00) PACKING_COST,
        NVL(SUM(ROUND(SERVICE_COST, 2)), 0.00) SERVICE_COST,
        NVL(SUM(ROUND(EBAY_FEE, 2)), 0.00) EBAY_FEE,
        NVL(SUM(ROUND(paypl_fee, 2)), 0.00) paypl_fee,

        NVL(SUM(ROUND(NVL(cost + SHIP_COST + PACKING_COST +
                                 SERVICE_COST + EBAY_FEE + paypl_fee,
                                 0),
                             2)),
                   0.00) TOTAL_expense,

       NVL(SUM(ROUND(sale_price - NVL(cost + SHIP_COST + PACKING_COST +
                                      SERVICE_COST + EBAY_FEE + paypl_fee,
                                      0),
                     2)),
           0.00) pl,
           count(missin_unit_cost) MISSING_UNIT_COST,
       count(mising_ship) MISSING_SHIPPING_COST
       /* SUM(ROUND(((sale_price - NVL(cost + SHIP_COST + PACKING_COST +
                              SERVICE_COST + EBAY_FEE + paypl_fee,
                              0)) / sale_price) * 100,
           2)) PL_PERC*/

         FROM (SELECT sd.ORDER_ID,
                       nvl(max(manif_cost.manif_cost),0) manif_cost,
                       nvl(max(manif_cost.manif_cost * sd.quantity)  ,0) cost,
                       max(sd.order_id) get_order_id,
               max(sd.tracking_number) tracking_number,
               max(sd.extendedorderid) SALES_RECORD_NUMBER,
               MAX(sd.item_id) EBAY_ITEM_ID,
               MAX(sd.item_title) ITEM_TITLE,
               NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) SALE_PRICE,
               round(NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) * (0.0225), 2) paypl_fee,

               MAX(sd.quantity) QTY,

               NVL(MAX(sd.shippinglabelrate), 0) SHIP_COST,
               SUM(NVL(DT.PACKING_COST, 0)) PACKING_COST,
               MAX(NVL(sd.SERVICE_COST, 0)) SERVICE_COST,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      NVL(MAX(sd.ebay_fee_perc), 0),
                      0) EBAY_FEE,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      (0.07 * (MAX(sd.SALE_PRICE) * MAX(sd.quantity))),
                      0) MARKTPLACE, /* (0.07 * (MAX(M.SALE_PRICE) * MAX(M.QTY))) MARKTPLACE,*/
               MAX(ACOUNT_DET.ACOUNT_NAME) ACC_NAME,
               MAX(ACOUNT_DET.merchant_id) MERCHANT_ID,
               max(ACOUNT_DET.item_id) item_id,
               max(sd.buyer_fullname) buyer_fullname,
               max(sd.sale_date) sale_date,
               max(manif_cost.lz_manifest_id) lz_manifest_id,
                case
                 when max(manif_cost.manif_cost) is null or
                      max(manif_cost.manif_cost) = 0 then
                  'cost_miss'
               end missin_unit_cost,

               case
                 when max(sd.shippinglabelrate) is null or
                      max(sd.shippinglabelrate) = 0 then
                  'ship_miss'
               end mising_ship
          from lz_salesload_det sd,
               LJ_ORDER_PACKING_DT DT,
               (SELECT MM.EBAY_ITEM_ID,
                       max(dd.merchant_id) merchant_id,
                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                       max(mm.item_id) item_id
                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET,
     /*  (select max(mt.cost) cost,

               max(b.order_id) order_id
          from lz_purch_item_dt d, lz_barcode_mt b, lz_purch_item_mt mt
         where d.barcode_no = b.barcode_no
           and d.purch_mt_id = mt.purch_mt_id
              --and b.ebay_item_id = 264599690019
           and b.order_id is not null
         group by b.order_id) barc_cost*/
         ( select /*bh.item_id, bh.barcode_no, bh.lz_manifest_id, i.item_code,*/sum(o.po_detail_retial_price) /COUNT(1) manif_cost,bh.order_id,max(o.lz_manifest_id) lz_manifest_id
  from lz_barcode_mt bh, items_mt i, lz_manifest_det o
 where /*bh.order_id = '264547503760-2541581429016'
   and */bh.item_id = i.item_id
   and i.item_code = o.laptop_item_code
   AND BH.LZ_MANIFEST_ID = O.LZ_MANIFEST_ID
   and bh.order_id is not null
   and o.PO_DETAIL_RETIAL_PRICE is not null
   group by bh.order_id) manif_cost
 where  sd.orderstatus = 'Completed'
   AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
/* and sd.order_id = barc_cost.order_id(+)*/
   and sd.order_id = manif_cost.order_id(+)
   and sd.order_id = dt.order_id(+)
   and sd.order_id in
       (select d.order_id
          from ebay_list_mt e, lj_merhcant_acc_dt a, lz_salesload_det d
         where a.acct_id = e.lz_seller_acct_id
           and d.item_id = e.ebay_item_id
           and a.merchant_id = '$merchId'
         group by d.order_id)
   and sd.return_id is null
                  AND sd.SALE_DATE BETWEEN
                  TO_DATE('$from 00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
       TO_DATE('$to 23:59:59', 'YYYY-MM-DD HH24:MI:SS')
                GROUP BY sd.ORDER_ID)
        order by ORDER_ID desc
       ";
        $data = $this->db->query($qry);
/****************************
 *
 *
purch_inv_cost
 *
 *
 *  */
        $purch_inv_cost = "SELECT sum(mt.cost) purchase_inv_cost, COUNT(dt.barcode_no) QTY
        FROM lz_purch_item_mt mt,lz_purch_item_dt dt
       WHERE mt.inserted_date between
       TO_DATE('$from 00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
       TO_DATE('$to 23:59:59', 'YYYY-MM-DD HH24:MI:SS')
             and mt.purch_mt_id = dt.purch_mt_id";
        $purch_inv_cost = $this->db->query($purch_inv_cost);
        if ($purch_inv_cost->num_rows() > 0) {
            $purch_inv = $purch_inv_cost->result_array();
            $purch_inv_cost = $purch_inv[0]['PURCHASE_INV_COST'];
            $purch_inv_qty = $purch_inv[0]['QTY'];

        } else {
            $purch_inv_cost = 0.00;
            $purch_inv_qty = 0;
        }
/*****
 *
 *  Get Total Sale
 *
 *
 */
        $get_total_sale = "SELECT NVL(SUM(ROUND(sale_price, 2)),0) total_sale_price,
 NVL(SUM(ROUND(NVL(cost + SHIP_COST + PACKING_COST + SERVICE_COST +
               EBAY_FEE + paypl_fee,
               0),
           2)),0) cost_of_sales,
           NVL(SUM(ROUND(sale_price - NVL(cost + SHIP_COST + PACKING_COST +
                            SERVICE_COST + EBAY_FEE + paypl_fee,
                            0),
           2)),0) pl,
           NVL(SUM(QTY),0) QTY
FROM (SELECT sd.ORDER_ID,
         nvl(max(manif_cost.manif_cost), 0) unit_cost,
         nvl(max(manif_cost.manif_cost * sd.quantity), 0) cost,
         max(sd.order_id) get_order_id,
         max(sd.tracking_number) tracking_number,
         max(sd.extendedorderid) SALES_RECORD_NUMBER,
         MAX(sd.item_id) EBAY_ITEM_ID,
         MAX(sd.item_title) ITEM_TITLE,
         NVL(MAX(sd.SALE_PRICE), 0) unit_SALE_PRICE,
         NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) SALE_PRICE,
         round(NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) * (0.0225), 2) paypl_fee,
         MAX(sd.quantity) QTY,
         NVL(MAX(sd.shippinglabelrate), 0) SHIP_COST,
         SUM(NVL(DT.PACKING_COST, 0)) PACKING_COST,
         MAX(NVL(sd.SERVICE_COST, 0)) SERVICE_COST,
         DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                'dfwonline',
                NVL(MAX(sd.ebay_fee_perc), 0),
                0) EBAY_FEE,
         DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                'dfwonline',
                (0.07 * (MAX(sd.SALE_PRICE) * MAX(sd.quantity))),
                0) MARKTPLACE,
         MAX(ACOUNT_DET.ACOUNT_NAME) ACC_NAME,
         MAX(ACOUNT_DET.merchant_id) MERCHANT_ID,
         max(ACOUNT_DET.item_id) item_id,
         max(sd.buyer_fullname) buyer_fullname,
         max(sd.sale_date) sale_date,
         max(manif_cost.lz_manifest_id) lz_manifest_id
    from lz_salesload_det sd,
         LJ_ORDER_PACKING_DT DT,
         (SELECT MM.EBAY_ITEM_ID,
                 max(dd.merchant_id) merchant_id,
                 MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                 MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                 max(mm.item_id) item_id
            FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
           WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
             AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
           GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET,
         (select sum(o.po_detail_retial_price) / COUNT(1) manif_cost,
                 bh.order_id,
                 max(o.lz_manifest_id) lz_manifest_id
            from lz_barcode_mt bh, items_mt i, lz_manifest_det o
           where bh.item_id = i.item_id
             and i.item_code = o.laptop_item_code
             AND BH.LZ_MANIFEST_ID = O.LZ_MANIFEST_ID
             and bh.order_id is not null
            and o.PO_DETAIL_RETIAL_PRICE is not null
           group by bh.order_id) manif_cost
   where sd.orderstatus = 'Completed'
     AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
     and sd.order_id = manif_cost.order_id(+)
     and sd.order_id = dt.order_id(+)
     and sd.order_id in
         (select d.order_id
            from ebay_list_mt       e,
                 lj_merhcant_acc_dt a,
                 lz_salesload_det   d
           where a.acct_id = e.lz_seller_acct_id
             and d.item_id = e.ebay_item_id
             and a.merchant_id = '$merchId'
             and d.order_id in (select b.order_id
                                  from lz_purch_item_mt mt,
                                       lz_purch_item_dt dt,
                                       lz_barcode_mt    b
                                 where mt.inserted_date between
                                 TO_DATE('$from 00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
       TO_DATE('$to 23:59:59', 'YYYY-MM-DD HH24:MI:SS')

                                   and mt.purch_mt_id = dt.purch_mt_id

                                   and dt.barcode_no = b.barcode_no

                                   and b.order_id is not null
                                 group by b.order_id)
           group by d.order_id)
     and sd.return_id is null
     AND sd.SALE_DATE BETWEEN
     TO_DATE('$from 00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
       TO_DATE('$to 23:59:59', 'YYYY-MM-DD HH24:MI:SS')

   GROUP BY sd.ORDER_ID)
order by ORDER_ID desc
";
        // if ($get_total_sale->num_rows() > 0) {
        //     $get_total_sale = $get_total_sale->result_array();
        //     $total_sale_price = $get_total_sale[0]['TOTAL_SALE_PRICE'];
        //     $cost_of_sales = $get_total_sale[0]['COST_OF_SALES'];
        //     $pl = $get_total_sale[0]['PL'];

        // } else {
        //     $total_sale_price = 0.00;
        //     $cost_of_sales = 0.00;
        //     $pl = 0.00;
        // }

        $get_total_sale = $this->db->query($get_total_sale);
        return array('status' => true, 'data' => $data->result_array(), 'purch_inv_cost' => $purch_inv_cost, 'get_total_sale' => $get_total_sale->result_array(), 'purch_inv_qty' => $purch_inv_qty);
    }

    public function Get_Pl_Report()
    {
        $requestData = $_REQUEST;
        $get_param = strtoupper($this->input->post('param'));
        $from = $this->input->post('startDate');
        $to = $this->input->post('endDate');
        $filter = $this->input->post('filter');

        $merchId = strtoupper($this->input->post('merchId'));
        // $merchId = 2;
        $columns = array(
            // datatable column index  => database column name
            0 => 'EBAY_ITEM_ID',
            1 => 'ITEM_TITLE',
            2 => 'BUYER_FULLNAME',
            3 => 'ORDER_ID',
            4 => 'UNIT_COST',
            5 => 'QTY',
            6 => 'TOTAL COST',
            7 => 'UNIT_SALE_PRICE',
            8 => 'SALE_PRICE',
            9 => 'SHIP_COST',
            10 => 'EBAY_FEE',
            11 => 'PACKING_COST',
            12 => 'PAYPL_FEE',
            13 => 'TOTAL_EXPENSE',
            14 => 'PL',
            15 => 'PL_PERC',
            16 => 'ACC_NAME',
            17 => 'SALE_DATE',
            18 => 'LZ_MANIFEST_ID',
        );
        $sql = "SELECT /*item_id, MERCHANT_ID,*/
        TRACKING_NUMBER, /*ORDER_PACKING_ID,*/
         SALES_RECORD_NUMBER order_id,
         EBAY_ITEM_ID,
         ITEM_TITLE,
         ROUND(cost,2)COST,
         ROUND(sale_price,2) sale_price,
         QTY,
         ROUND(ship_cost,2) ship_cost,
         ROUND(PACKING_COST,2)PACKING_COST,
         ROUND(SERVICE_COST,2) SERVICE_COST,
         ROUND(EBAY_FEE,2)EBAY_FEE,
         ROUND(paypl_fee,2) paypl_fee ,
         buyer_fullname,

         ROUND(NVL(cost + SHIP_COST + PACKING_COST + SERVICE_COST + EBAY_FEE + paypl_fee,
             0),2) TOTAL_expense,
         ROUND(sale_price -
         NVL(cost + SHIP_COST + PACKING_COST + SERVICE_COST + EBAY_FEE + paypl_fee,
             0),2) pl,
         ROUND(((sale_price -
         NVL(cost + SHIP_COST + PACKING_COST + SERVICE_COST + EBAY_FEE + paypl_fee,
             0)) / sale_price) *100,2 ) PL_PERC,
         sale_date,
         ACC_NAME,
         lz_manifest_id,
         ROUND(UNIT_COST,2) UNIT_COST,
         UNIT_SALE_PRICE
          FROM (SELECT sd.ORDER_ID,
          nvl(max(manif_cost.manif_cost ), 0) unit_cost,
                       nvl(max(manif_cost.manif_cost),0) manif_cost,
                       nvl(max(manif_cost.manif_cost * sd.quantity)  ,0) cost,
                       max(sd.order_id) get_order_id,
               max(sd.tracking_number) tracking_number,
               max(sd.extendedorderid) SALES_RECORD_NUMBER,
               MAX(sd.item_id) EBAY_ITEM_ID,
               MAX(sd.item_title) ITEM_TITLE,
               NVL(MAX(sd.SALE_PRICE), 0) UNIT_SALE_PRICE,
               NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) SALE_PRICE,
               round(NVL(MAX(sd.SALE_PRICE * sd.quantity), 0) * (0.0225), 2) paypl_fee,
               MAX(sd.quantity) QTY,
               NVL(MAX(sd.shippinglabelrate), 0) SHIP_COST,
               SUM(NVL(DT.PACKING_COST, 0)) PACKING_COST,
               MAX(NVL(sd.SERVICE_COST, 0)) SERVICE_COST,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      NVL(MAX(sd.ebay_fee_perc), 0),
                      0) EBAY_FEE,
               DECODE(MAX(ACOUNT_DET.ACOUNT_NAME),
                      'dfwonline',
                      (0.07 * (MAX(sd.SALE_PRICE) * MAX(sd.quantity))),
                      0) MARKTPLACE, /* (0.07 * (MAX(M.SALE_PRICE) * MAX(M.QTY))) MARKTPLACE,*/
               MAX(ACOUNT_DET.ACOUNT_NAME) ACC_NAME,
               MAX(ACOUNT_DET.merchant_id) MERCHANT_ID,
               max(ACOUNT_DET.item_id) item_id,
               max(sd.buyer_fullname) buyer_fullname,
               max(sd.sale_date) sale_date,
               max(manif_cost.lz_manifest_id) lz_manifest_id
          from lz_salesload_det sd,
               LJ_ORDER_PACKING_DT DT,
               (SELECT MM.EBAY_ITEM_ID,
                       max(dd.merchant_id) merchant_id,
                       MAX(MM.LZ_SELLER_ACCT_ID) ACT_ID,
                       MAX(DD.ACCOUNT_NAME) ACOUNT_NAME,
                       max(mm.item_id) item_id
                  FROM EBAY_LIST_MT MM, LJ_MERHCANT_ACC_DT DD
                 WHERE MM.LZ_SELLER_ACCT_ID = DD.ACCT_ID(+)
                   AND MM.LZ_SELLER_ACCT_ID IS NOT NULL
                 GROUP BY MM.EBAY_ITEM_ID) ACOUNT_DET,
     /*  (select max(mt.cost) cost,
 
               max(b.order_id) order_id
          from lz_purch_item_dt d, lz_barcode_mt b, lz_purch_item_mt mt
         where d.barcode_no = b.barcode_no
           and d.purch_mt_id = mt.purch_mt_id
              --and b.ebay_item_id = 264599690019
           and b.order_id is not null
         group by b.order_id) barc_cost*/
         ( select /*bh.item_id, bh.barcode_no, bh.lz_manifest_id, i.item_code,*/sum(o.po_detail_retial_price) /COUNT(1) manif_cost,bh.order_id,max(o.lz_manifest_id) lz_manifest_id
  from lz_barcode_mt bh, items_mt i, lz_manifest_det o
 where /*bh.order_id = '264547503760-2541581429016'
   and */bh.item_id = i.item_id
   and i.item_code = o.laptop_item_code
   AND BH.LZ_MANIFEST_ID = O.LZ_MANIFEST_ID
   and bh.order_id is not null
   and o.PO_DETAIL_RETIAL_PRICE is not null
   group by bh.order_id) manif_cost
 where  sd.orderstatus = 'Completed'
   AND sd.item_id = ACOUNT_DET.EBAY_ITEM_ID(+)
/* and sd.order_id = barc_cost.order_id(+)*/
   and sd.order_id = manif_cost.order_id(+)
   and sd.order_id = dt.order_id(+)
   and sd.order_id in
       (select d.order_id
          from ebay_list_mt e, lj_merhcant_acc_dt a, lz_salesload_det d
         where a.acct_id = e.lz_seller_acct_id
           and d.item_id = e.ebay_item_id
           and a.merchant_id = '$merchId'
         group by d.order_id)
   and sd.return_id is null";
        if ($filter == 'MISSING_UNIT_COST') {
            $sql .= " and (manif_cost.manif_cost is null or manif_cost.manif_cost = 0) ";
        } else if ($filter == 'MISSING_SHIP_COST') {
            $sql .= " and (sd.shippinglabelrate is null or shippinglabelrate=0)";
        }
        $searchValue = $requestData['search']['value'];
        $searchValue = trim(str_replace("  ", ' ', $searchValue));
        $searchValue = str_replace(array("`,'"), "", $searchValue);
        $searchValue = str_replace(array("'"), "''", $searchValue);
        $str = explode(' ', $searchValue);
        if (!empty($searchValue)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $sql .= " and ( (sd.item_title) LIKE '%$key%' ";
                        // $sql .= " and (sd.buyer_fullname) LIKE '%$key%' ";
                    } else {
                        $sql .= " AND (sd.item_title) LIKE '%$key%' ";
                        // $sql .= " AND (sd.buyer_fullname) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $sql .= " and ((sd.item_title) LIKE '%$searchValue%' ";
                // $sql .= " AND (sd.buyer_fullname) LIKE '%$searchValue%' ";
            }

            $sql .= " OR (sd.item_id) LIKE '%$searchValue%'
    OR (sd.SERVICE_COST) LIKE '%$searchValue%'
    OR (sd.ORDER_ID) LIKE '%$searchValue%'
    OR (sd.extendedorderid) LIKE '%$searchValue%'
OR (DT.PACKING_COST) LIKE '%$searchValue%'
OR (sd.SALE_PRICE) LIKE '%$searchValue%'
OR (sd.buyer_fullname) LIKE '%$searchValue%'
OR (sd.sale_date) LIKE '%$searchValue%'
OR (sd.ebay_fee_perc) LIKE '%$searchValue%'
OR(sd.tracking_number)  LIKE '%$searchValue%')";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }

        $sql .= " AND sd.SALE_DATE BETWEEN
       TO_DATE('$from 00:00:00', 'YYYY-MM-DD HH24:MI:SS') AND
       TO_DATE('$to 23:59:59', 'YYYY-MM-DD HH24:MI:SS')


    GROUP BY sd.ORDER_ID)
 /*where cost > 0*/
 order by sale_date desc";
        $query = $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;

        $sqlLength = "SELECT  * FROM (SELECT  q.*, rownum rn FROM  ($sql) q )";

        $sqlLength .= " WHERE   ROWNUM <= " . $requestData['length'] . " AND rn>= " . $requestData['start'];

        if (!empty($columns[$requestData['order'][0]['column']])) {
            $sqlLength .= " ORDER BY  " . $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        }

        /*=====  End of For Oracle 12-c  ======*/
//   $query = $this->db->query($sql);

        $lot_detail_data = $this->db->query($sqlLength)->result_array();
        $data = array();
        if (count($lot_detail_data) >= 1) {
            // '$' . number_format((float) @$rows['COST'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['SHIP_COST'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['EBAY_FEE'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['PACKING_COST'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['PAYPL_FEE'], 2, '.', ','),
            //         '$' . number_format((float) @$rows['TOTAL_EXPENSE'], 2, '.', ','),
            //         '$ ' . number_format((float) @$rows['PL'], 2, '.', ','),
            //         number_format((float) @$rows['PL_PERC'], 2, '.', ',') . " %",

            //     "<a href=http://www.ebay.com/itm/" . @$rows['EBAY_ITEM_ID'] . " target='_blank'>
            //     " . @$rows['EBAY_ITEM_ID'] . "
            //    </a>",
            //     @$rows['ITEM_TITLE'],
            //     @$rows['BUYER_FULLNAME'],
            //     @$rows['ORDER_ID'],
            //     "<span style='float: right;'>$ " . @$cost . "</span>",
            //     "<span style='float: right;'>$ " . @$sale_price . "</span>",
            //     @$rows['QTY'],
            //     "<span style='float: right;'>$ " . @$ship_cost . "</span>",
            //     "<span style='float: right;'>$ " . @$ebay_fee . "</span>",
            //     "<span style='float: right;'>$ " . @$packing_cost . "</span>",
            //     "<span style='float: right;'>$ " . @$paypl_fee . "</span>",
            //     "<span style='float: right;'>$ " . @$total_expense . "</span>",
            //     "<span style='float: right;'>$ " . @$pl . "</span>",
            //     "<span style='float: right;'>" . @$pl_perc . " %</span>",

            foreach ($lot_detail_data as $keys => $rows) {
                $sale_price = number_format((float) @$rows['SALE_PRICE'], 2, '.', ',');
                $unit_sale_price = number_format((float) @$rows['UNIT_SALE_PRICE'], 2, '.', ',');
                $cost = number_format((float) @$rows['COST'], 2, '.', ',');
                $unit_cost = "$ " . number_format((float) @$rows['UNIT_COST'], 2, '.', ',');
                $ship_cost = "$ " . number_format((float) @$rows['SHIP_COST'], 2, '.', ',');
                $ebay_fee = number_format((float) @$rows['EBAY_FEE'], 2, '.', ',');
                $packing_cost = number_format((float) @$rows['PACKING_COST'], 2, '.', ',');
                $paypl_fee = number_format((float) @$rows['PAYPL_FEE'], 2, '.', ',');
                $total_expense = number_format((float) @$rows['TOTAL_EXPENSE'], 2, '.', ',');
                $pl = number_format((float) @$rows['PL'], 2, '.', ',');
                $pl_perc = number_format((float) @$rows['PL_PERC'], 2, '.', ',');
                $data[] = array(
                    "<a href=http://www.ebay.com/itm/" . @$rows['EBAY_ITEM_ID'] . " target='_blank'>
                    " . @$rows['EBAY_ITEM_ID'] . "
                   </a><br>" . @$rows['TRACKING_NUMBER'],
                    @$rows['ITEM_TITLE'],
                    @$rows['BUYER_FULLNAME'],
                    @$rows['ORDER_ID'],
                    "<input type='text' name='unitCost'  id='unitCost' class='form-control' value='" . @$unit_cost . "' />",
                    @$rows['QTY'],
                    "<span style='float: right;'>$ " . @$cost . "</span>",
                    "<span style='float: right;'>$ " . @$unit_sale_price . "</span>",
                    "<span style='float: right;'>$ " . @$sale_price . "</span>",
                    "<input type='text' name='shipCost'  id='shipCost'  class='form-control' value='" . @$ship_cost . "' />",
                    "<span style='float: right;'>$ " . @$ebay_fee . "</span>",
                    // "<input type='text' name='ebayFee'  id='ebayFee'  class='form-control' value='" . @$ebay_fee . "' />",
                    "<span style='float: right;'>$ " . @$packing_cost . "</span>",
                    "<span style='float: right;'>$ " . @$paypl_fee . "</span>",
                    // "<input type='text' name='payplFee'  id='payplFee'  class='form-control' value='" . @$paypl_fee . "' />",
                    "<span style='float: right;'>$ " . @$total_expense . "</span>",
                    "<span style='float: right;'>$ " . @$pl . "</span>",
                    "<span style='float: right;'>" . @$pl_perc . " %</span>",

                    @$rows['ACC_NAME'],
                    @$rows['SALE_DATE'],
                    "<span style='display:none;'>" . @$rows['LZ_MANIFEST_ID'] . "</span>",
                    "<input type='hidden' name='orderId'  id='orderId' class='form-control' value='" . @$rows['ORDER_ID'] . "' />",
                );
            }

            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered),
                // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('lot_detail_data' => $output, "images" => $images, 'status' => true);
        } else {
            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('lot_detail_data' => $lot_detail_data, 'status' => false, "images" => array());
        }

    }
    public function Update_Ship_Cost()
    {
        $ship_cost = $this->input->post('ship_cost');
        $ship_cost = trim(str_replace("  ", ' ', $ship_cost));
        $ship_cost = str_replace(array("`,'"), "", $ship_cost);
        $ship_cost = str_replace(array("'"), "''", $ship_cost);
        $order_id = $this->input->post('order_id');
        $order_id = trim(str_replace("  ", ' ', $order_id));
        $order_id = str_replace(array("`,'"), "", $order_id);
        $order_id = str_replace(array("'"), "''", $order_id);
        // UPDATE LJ_ORDER_PACKING_MT SET SHIPING_LABEL_RATE = '$ship_cost' WHERE ORDER_ID = '$order_id'
        $update = $this->db->query("UPDATE lz_salesload_det dd SET dd.SHIPPINGLABELRATE = '$ship_cost' WHERE dd.extendedorderid = '$order_id'");
        if ($update == true) {
            return array('status' => true, 'message' => 'Shipping Cost Update Successfully');
        } else {
            return array('status' => false, 'message' => 'Shipping Cost Not Update Successfully');
        }
    }
    public function Update_Ebay_Fee()
    {
        $ebay_fee = $this->input->post('ebay_fee');
        $ebay_fee = trim(str_replace("  ", ' ', $ebay_fee));
        $ebay_fee = str_replace(array("`,'"), "", $ebay_fee);
        $ebay_fee = str_replace(array("'"), "''", $ebay_fee);
        $order_id = $this->input->post('order_id');
        $order_id = trim(str_replace("  ", ' ', $order_id));
        $order_id = str_replace(array("`,'"), "", $order_id);
        $order_id = str_replace(array("'"), "''", $order_id);
        // UPDATE LJ_ORDER_PACKING_MT SET SHIPING_LABEL_RATE = '$ship_cost' WHERE ORDER_ID = '$order_id'
        $update = $this->db->query("UPDATE lz_salesload_det dd SET dd.shippinglabelrate = '$ebay_fee' WHERE dd.extendedorderid = '$order_id'");
        if ($update == true) {
            return array('status' => true, 'message' => 'Ebay Fee Update Successfully');
        } else {
            return array('status' => false, 'message' => 'Ebay Fee Not Update Successfully');
        }
    }
    public function Update_Pay_Pal()
    {
        $payplFee = $this->input->post('payplFee');
        $payplFee = trim(str_replace("  ", ' ', $payplFee));
        $payplFee = str_replace(array("`,'"), "", $payplFee);
        $payplFee = str_replace(array("'"), "''", $payplFee);
        $order_id = $this->input->post('order_id');
        $order_id = trim(str_replace("  ", ' ', $order_id));
        $order_id = str_replace(array("`,'"), "", $order_id);
        $order_id = str_replace(array("'"), "''", $order_id);
        // UPDATE LJ_ORDER_PACKING_MT SET SHIPING_LABEL_RATE = '$ship_cost' WHERE ORDER_ID = '$order_id'
        $update = $this->db->query("UPDATE lz_salesload_det dd SET dd.shippinglabelrate = '$payplFee' WHERE dd.extendedorderid = '$order_id'");
        if ($update == true) {
            return array('status' => true, 'message' => 'Pay Pal Update Successfully');
        } else {
            return array('status' => false, 'message' => 'Pay Pal Not Update Successfully');
        }
    }   
    public function Update_Cost()
    {
        $cost = $this->input->post('cost');
        $cost = trim(str_replace("  ", ' ', $cost));
        $cost = str_replace(array("`,'"), "", $cost);
        $cost = str_replace(array("'"), "''", $cost);
        $lz_manifest_id = $this->input->post('lz_manifest_id');
        $lz_manifest_id = trim(str_replace("  ", ' ', $lz_manifest_id));
        $lz_manifest_id = str_replace(array("`,'"), "", $lz_manifest_id);
        $lz_manifest_id = str_replace(array("'"), "''", $lz_manifest_id);
        // UPDATE LJ_ORDER_PACKING_MT SET SHIPING_LABEL_RATE = '$ship_cost' WHERE ORDER_ID = '$order_id'
        $update = $this->db->query("update lz_manifest_det p set p.po_detail_retial_price = '$cost' where p.lz_manifest_id = $lz_manifest_id");
        if ($update == true) {
            return array('status' => true, 'message' => 'Cost Update Successfully');
        } else {
            return array('status' => false, 'message' => 'Cost Not Update Successfully');
        }
    }

}
