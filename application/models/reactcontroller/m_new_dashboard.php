<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * m_new_dashboard  Model
 */

class m_new_dashboard extends CI_Model
{
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function Get_Awaiting_Shippment_Detail()
    {

        $priceOne = strtoupper($this->input->post('priceOne'));
        $priceOne = trim(str_replace("  ", ' ', $priceOne));
        $priceOne = str_replace(array("`,'"), "", $priceOne);
        $priceOne = str_replace(array("'"), "''", $priceOne);

        $priceTwo = strtoupper($this->input->post('priceTwo'));
        $priceTwo = trim(str_replace("  ", ' ', $priceTwo));
        $priceTwo = str_replace(array("`,'"), "", $priceTwo);
        $priceTwo = str_replace(array("'"), "''", $priceTwo);

        $merLotName = strtoupper($this->input->post('merLotName'));
        $merchId = strtoupper($this->input->post('merchId'));
        $mydata = $this->db->query("SELECT sm.lz_seller_acct_id,decode(pp.img_key,
        null,
        null,
        'https://i.ebayimg.com/images/g/' || pp.img_key ||
        '/s-l110.jpg') pic_url,

        ELM.LIST_DATE,
        mr.merchant_id,
        B.BARCODE_NO,
        SD.ITEM_ID,
        B.BIN_ID,
        SD.SALES_RECORD_NUMBER ORDER_NUM,
        SD.ITEM_TITLE,
        SD.USER_ID,
        SD.TRANSACTION_ID,
        SD.SALE_DATE,
        SD.BUYER_FULLNAME,
SD.SALE_PRICE,
        C.COND_NAME,
        SD.QUANTITY,
        SD.TOTAL_PRICE,
        SD.SHIPPING_SERVICE,
        SD.STAMPS_SHIPPING_RATE,
        SM.LZ_SELLER_ACCT_ID,
        ACC.SELL_ACCT_DESC,
        MR.BUISNESS_NAME,
        WA.WAREHOUSE_NO,
        B.CONDITION_ID,
        B.PULLING_ID,
        SD.ORDERSTATUS EBAY_STATUS,
        SD.ORDER_ID,
        SD.SALES_RECORD_NO_GROUP,
        SD.EXTENDEDORDERID,
        SD.PAID_ON_DATE,
        DECODE(BI.BIN_ID, 0, 'NO BIN', 'W' || WA.WAREHOUSE_NO) BIN_LOCATION_WAREHOUSE,
        DECODE(BI.BIN_ID,
        0,
        'NO BIN',
        'W' || WA.WAREHOUSE_NO || '-' || RAC.RACK_NO || '-R' ||
        RO.ROW_NO || '-' || BI.BIN_TYPE || '-' || BI.BIN_NO) BIN_LOCATION
        FROM LZ_SALESLOAD_DET SD,
        LZ_BARCODE_MT B,
        EBAY_LIST_MT ELM,
        LZ_AWAITING_SHIPMENT AW,
        BIN_MT BI,
        LZ_RACK_ROWS RO,
        RACK_MT RAC,
        LZ_ITEM_COND_MT C,
        WAREHOUSE_MT WA,
        LZ_SALESLOAD_MT SM,
        LZ_SELLER_ACCTS ACC,
        lz_merchant_mt mr,
        LJ_MERHCANT_ACC_DT AD,
        (select p.img_key, p.ebay_id, p.pic_url
        from LZ_LISTED_ITEM_PIC P
        where p.pic_id in (SELECT MIN(PIC_id) PIC_id
        FROM LZ_LISTED_ITEM_PIC P
        where p.ebay_id is not null
        and p.img_key is not null
        GROUP BY EBAY_ID)) pp
        WHERE B.LIST_ID = ELM.LIST_ID
        AND SD.SALES_RECORD_NUMBER = B.SALE_RECORD_NO(+)
        AND SD.ITEM_ID = B.EBAY_ITEM_ID
        AND SM.LZ_SELLER_ACCT_ID = ACC.LZ_SELLER_ACCT_ID
        and elm.lz_seller_acct_id = ad.acct_id
        and aw.orderlineitemid = sd.order_id
        AND SD.LZ_SALELOAD_ID = SM.LZ_SALELOAD_ID
        AND B.BIN_ID = BI.BIN_ID(+)
        AND BI.CURRENT_RACK_ROW_ID = RO.RACK_ROW_ID(+)
        AND RO.RACK_ID = RAC.RACK_ID(+)
        AND RAC.WAREHOUSE_ID = WA.WAREHOUSE_ID(+)
        AND B.CONDITION_ID = C.ID(+)
        AND SD.ITEM_ID = B.EBAY_ITEM_ID
        AND SD.ITEM_ID = pp.EBAY_ID(+)
        AND MR.MERCHANT_ID = AD.MERCHANT_ID
        AND B.PULLING_ID IS NULL
        AND SD.UK_FIREBASE_WAITING IS NULL
        AND B.ITEM_ADJ_DET_ID_FOR_OUT IS NULL
        AND B.LZ_PART_ISSUE_MT_ID IS NULL
        AND B.LZ_POS_MT_ID IS NULL
        AND SD.GNRTD_DC_ID IS NULL
        AND SD.TRACKING_NUMBER IS NULL
        AND SD.SHIPPED_ON_DATE IS NULL
        and sd.orderstatus = 'Completed'
        and sd.cancel_id is null
        AND SD.RETURN_ID IS NULL
        AND mr.merchant_id = '$merchId'")->result_array();

        if (count($mydata) > 0) {
            return array('status' => true, 'data' => $mydata);
        } else {

            return array('status' => false, 'data' => array());
        }
    }

    public function Ledger_Trail_Dashboard()
    {
        $barcode = strtoupper($_GET['barcode']);
        $barcode = trim(str_replace("  ", ' ', $barcode));
        $barcode = str_replace(array("`,'"), "", $barcode);
        $barcode = str_replace(array("'"), "''", $barcode);

        $str = explode(' ', $barcode);

        $chek_item_return = $this->db->query("select b.returned_id ret from lz_barcode_mt b where b.barcode_no ='$barcode' and b.returned_id is not null union all select b.returned_id_for_in ret from lz_barcode_mt b where b.barcode_no ='$barcode' and b.returned_id_for_in is not null")->result_array();if (count($chek_item_return) > 0) {
            $chek_item_return = $chek_item_return[0]['RET'];
        } else {
            $chek_item_return = '';
        }

        $data = "select de.lot_desc lot_name,
       case
         when u.returned_id is null and u.ebay_item_id is not null and
              u.order_id is null and u.lz_pos_mt_id is null then
          'Listed Item'
         when u.returned_id is not null and u.ebay_item_id is not null and
              u.lz_pos_mt_id is null then
          'Returned Barcode'
         when u.returned_id is null and u.ebay_item_id is not null and
              u.order_id is not null and u.lz_pos_mt_id is null then
          'sold on Ebay'
         when u.returned_id is null and u.ebay_item_id is null and
              u.order_id is null and u.lz_pos_mt_id is not null then
          'sold in pos'
         when u.barcode_no is  null then
          'Item Not Verified'
          else
            'Not Listed'
       end status,
       ri.remarks,
       l.barcode_prv_no barcode_no,
       case
         when u.returned_id is null and u.ebay_item_id is not null and
              u.order_id is null and u.lz_pos_mt_id is null then
          '' || '' || u.ebay_item_id
         when u.returned_id is not null and u.ebay_item_id is not null and
              u.lz_pos_mt_id is null then
          '' || '' || u.ebay_item_id
         when u.returned_id is null and u.ebay_item_id is not null and
              u.order_id is not null and u.lz_pos_mt_id is null then
          '' || '' || u.ebay_item_id
         when u.returned_id is null and u.ebay_item_id is null and
              u.order_id is null and u.lz_pos_mt_id is not null then
          'sold in pos '
         when u.barcode_no is  null then
          ''
          else
            'Not Listed'
       end EBAY_ITEM_ID,

       /*decode(u.ebay_item_id,
              null,
              'Barcode not listed',
              '' || u.ebay_item_id || '') EBAY_ITEM_ID,*/
       list_price.list_pric,
       list_price.list_date,
       det.extendedorderid,
       decode(det.sale_price ,
              null,
              ps.pos_sale_price,
              det.sale_price ) sale_price,
        decode(u.returned_id ,null,'',det.sale_price)return_amount,

       u.order_id,
       sc.item_title,
       co.cond_name,
       ps.pos_sale_price,
       cancel_item.cANCEL_AMOUNT,
       cancel_item.cancel_status,
       cancel_item.CANCEL_REASON,

       cancel_item.canc_ord_id

      from lz_merchant_barcode_dt md,
           lz_merchant_barcode_mt mt,
           lz_special_lots l,
           lot_defination_mt      de,
           lz_barcode_mt          u,
           lz_salesload_det       det,
           lj_item_returned_mt ri,
           lz_item_seed sc,
           lz_item_cond_mt co,
           (select  li.ebay_item_id,
                   li.list_price     list_pric,
                   li.list_date
              from ebay_list_mt li
             where li.list_id in (select max(e.list_id) list_id
                                    from ebay_list_mt e
                                   group by e.ebay_item_id)) list_price,
            (select po.barcode_id,
               round(nvl(po.price, 0) +
                     (nvl(po.price, 0) / 100) * nvl(po.sales_tax_perc, 0) -
                     nvl(po.disc_amt, 0),
                     2) pos_sale_price
          from lz_pos_det po
         where po.barcode_id is not null
           AND po.deleted_by IS NULL
           AND po.return_by IS NULL) ps,

           (select BARCODE_NO,
               count(BARCODE_NO) no_of_tim_cacel,
               sum(canccel_amount) cancel_amount,
               max(cancel_status) cancel_status,
               max(CANCEL_REASON) CANCEL_REASON,
               max(EXTENDEDORDERID) canc_ord_id
          from (select i.barcode_no,
                       u.quantity,
                       u.sale_price,
                       round(u.sale_price / u.quantity, 2) canccel_amount,
                       u.orderstatus,
                       u.order_id,
                       u.cancel_id,
                       cm.cancel_status,
                       cm.CANCEL_REASON,
                       i.EXTENDEDORDERID
                  from lj_cancellation_dt i,
                       lz_salesload_det   u,
                       lj_cancellation_mt cm
                 where i.order_id = u.order_id(+)
                   and i.cancellation_id = cm.cancellation_id
                 order by i.barcode_no asc)
         group by BARCODE_NO) cancel_item
     where md.mt_id = mt.mt_id
       and md.barcode_no = l.barcode_prv_no(+)
       and l.barcode_prv_no = u.barcode_no(+)
       and mt.lot_id = de.lot_id(+)
       and u.order_id = det.order_id(+)
       and u.returned_id = ri.returned_id(+)
       and u.item_id = sc.item_id(+)
       and u.lz_manifest_id = sc.lz_manifest_id(+)
       and u.condition_id = sc.default_cond(+)
       and sc.default_cond = co.id(+)
       and u.ebay_item_id = list_price.ebay_item_id(+)
       and u.barcode_no =cancel_item.barcode_no(+)
       and u.barcode_no =ps.barcode_id(+)";
        if (!empty($chek_item_return)) {
            $data .= "and u.returned_id = '$chek_item_return'";
        } else {
            $data .= " and md.BARCODE_NO = '$barcode'";
        }

        if (!empty($chek_item_return)) {
            $data .= "union all
        select de.lot_desc lot_name,
               'New Barcode Generated' status,
               ''remarks,
               l.barcode_prv_no barcode_no,
               decode(u.ebay_item_id,
                      null,
                      'Barcode Not Listed',
                      'Relist with' || ' (' || u.ebay_item_id || ')') EBAY_ITEM_ID,
               list_price.list_pric,
               list_price.list_date,
               det.extendedorderid,
               det.sale_price sale_price,

               decode(u.returned_id ,null,'',det.sale_price)return_amount,
               u.order_id,
               sc.item_title,
               co.cond_name,
               ps.pos_sale_price,
               cancel_item.cANCEL_AMOUNT,
               cancel_item.cancel_status,
               cancel_item.CANCEL_REASON,
               cancel_item.canc_ord_id
          from lz_merchant_barcode_dt md,
               lz_merchant_barcode_mt mt,
               lz_special_lots l,
               lot_defination_mt      de,
               lz_barcode_mt          u,
               lz_salesload_det       det,
               lz_item_seed sc,
               lz_item_cond_mt co,
               (select li.ebay_item_id,
                       li.list_price     list_pric,
                       li.list_date
                  from ebay_list_mt li
                 where li.list_id in (select max(e.list_id) list_id
                                        from ebay_list_mt e
                                       group by e.ebay_item_id)) list_price,
                (select po.barcode_id,
               round(nvl(po.price, 0) +
                     (nvl(po.price, 0) / 100) * nvl(po.sales_tax_perc, 0) -
                     nvl(po.disc_amt, 0),
                     2) pos_sale_price
          from lz_pos_det po
         where po.barcode_id is not null
           AND po.deleted_by IS NULL
           AND po.return_by IS NULL) ps,

           (select BARCODE_NO,
               count(BARCODE_NO) no_of_tim_cacel,
               sum(canccel_amount) cancel_amount,
               max(cancel_status) cancel_status,
               max(CANCEL_REASON) CANCEL_REASON,
               max(EXTENDEDORDERID) canc_ord_id
          from (select i.barcode_no,
                       u.quantity,
                       u.sale_price,
                       round(u.sale_price / u.quantity, 2) canccel_amount,
                       u.orderstatus,
                       u.order_id,
                       u.cancel_id,
                       cm.cancel_status,
                       cm.CANCEL_REASON,
                       i.EXTENDEDORDERID
                  from lj_cancellation_dt i,
                       lz_salesload_det   u,
                       lj_cancellation_mt cm
                 where i.order_id = u.order_id(+)
                   and i.cancellation_id = cm.cancellation_id
                 order by i.barcode_no asc)
         group by BARCODE_NO) cancel_item

         where md.mt_id = mt.mt_id
           and md.barcode_no = l.barcode_prv_no(+)
           and l.barcode_prv_no = u.barcode_no(+)
           and mt.lot_id = de.lot_id(+)
           and u.order_id = det.order_id(+)
           and u.item_id = sc.item_id(+)
           and u.lz_manifest_id = sc.lz_manifest_id(+)
           and u.condition_id = sc.default_cond(+)
           and sc.default_cond = co.id(+)
           and u.ebay_item_id = list_price.ebay_item_id(+)
           and u.barcode_no =cancel_item.barcode_no(+)
           and u.barcode_no =ps.barcode_id(+)";

            if (!empty($chek_item_return)) {
                $data .= " and u.returned_id_for_in = '$chek_item_return'";
            } else {
                $data .= " and md.BARCODE_NO = '$barcode'";
            }
        }
        $data = $this->db->query($data);
        if ($data->num_rows() > 0) {
            return array('status' => true, 'data' => $data->result_array());
        } else {
            return array('status' => false, 'data' => array());
        }
    }

    public function Get_Invoice_Detail_Server_Side()
    {
        $merchId = $this->input->post('merchId');
        $lot_id = $this->input->post('lot_id');
        $merLotName = '';

        if (!empty($lot_id)) {

            // if (is_array($lot_id)) {
            //     if (sizeof($lot_id) > 1) {
            // $cond_exp = '';
            $i = 0;
            foreach ($lot_id as $lot) {
                if (!empty($lot_id[$i + 1])) {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'" . ',';

                } else {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'";
                }
                $i++;

            }

        }
        // else {
        //     $merLotName = $merLotName . $lot_id[0]['label'];
        // }
        // } else {
        //     $merLotName = $merLotName . $lot['label'];
        // }

        //svar_dump($merLotName);
        $filter = $this->input->post('filter');
        $requestData = $_REQUEST;

        $searchValue = strtoupper($requestData['search']['value']);
        $searchValue = trim(str_replace("  ", ' ', $searchValue));
        $searchValue = str_replace(array("`,'"), "", $searchValue);
        $searchValue = str_replace(array("'"), "''", $searchValue);
        //$searchValue='254532353361';
        $str = explode(' ', $searchValue);

        $columns = array(
            // datatable column index  => database column name
            0 => 'EBAY_ITEM_ID',
            1 => 'IMG_URL',
            2 => 'BARCODE_NO',
            3 => 'LOT_DESC',
            4 => 'ITEM_TITLE',
            5 => 'COND_NAME',
            6 => 'LIST_PRIC',
            7 => 'UPC',
            8 => 'MPN',
            9 => 'CATEGORY_NAME',
            10 => 'ORDER_ID',
            11 => 'BUYER_FULLNAME',
            12 => 'QTY',
            13 => 'SALE_PRICE',
            14 => 'SHIP_PRICE',
            15 => 'EBAY_FEE',
            16 => 'MARKETPLACE_FEE',
            17 => 'SALE_DATE',
        );

        $qry = "SELECT * from (select sc.item_title,
       co.cond_name,
       de.lot_id,
       get_pictures.full_img_url,
REPLACE(get_pictures.full_img_url,'D:/wamp/www/','') img_url,
REPLACE(get_pictures.thumb_img_url,'D:/wamp/www/','') thumb_url,
       ''REF_NO,
       de.lot_desc LOT_DESC,
       l.barcode_prv_no barcode_no,
       u.barcode_no get_barcode_no,
       '' lot_cost,
       case
when u.returned_id is null and u.ebay_item_id is not null and
u.order_id is null and u.lz_pos_mt_id is null then
'' || '' || u.ebay_item_id
when u.returned_id is not null and u.ebay_item_id is not null and
u.lz_pos_mt_id is null then
'' || '' || u.ebay_item_id
when u.returned_id is null and u.ebay_item_id is not null and
u.order_id is not null and u.lz_pos_mt_id is null then
'' || '' || u.ebay_item_id
when u.returned_id is null and u.ebay_item_id is null and
u.order_id is null and u.lz_pos_mt_id is not null then
'sold in pos '
when u.barcode_no is null then
'Item Not Verified'
when u.barcode_no is not null and u.ebay_item_id is null and
u.order_id is null and u.returned_id is null and
u.lz_pos_mt_id is null then
'Not Listed'
end EBAY_ITEM_ID,
       ''GENERATED_BAR,
       ''PICTURE_BARC,
       sc.f_upc upc,
       sc.f_mpn mpn,
       sc.category_name,
       decode(det.sale_price, null, ps.pos_sale_price, det.sale_price) sale_price,
       det.shippinglabelrate SHIP_PRICE,
       det.ebay_fee_perc EBAY_FEE,
       det.extendedorderid order_id,
       det.buyer_fullname,
       det.sale_date,

       case
       when u.barcode_no is not null and u.ebay_item_id is not null and u.returned_id is null
       and u.order_id is null and u.lz_pos_mt_id is null then
       'Listed Item'
       when u.returned_id is not null and
       u.ebay_item_id is not null and u.lz_pos_mt_id is null then
       'Returned Barcode'
       when u.returned_id is null and u.ebay_item_id is not null and
       u.order_id is not null and u.lz_pos_mt_id is null then
       'sold on Ebay'
       when u.returned_id is null and u.ebay_item_id is null and
       u.order_id is null and u.lz_pos_mt_id is not null then
       'sold in pos'
       when u.barcode_no is null then
       'Item Not Verified'
       when u.barcode_no is not null and u.ebay_item_id is null and
       u.order_id is null and u.returned_id is null and
       u.lz_pos_mt_id is null then
       'Not Listed'
       end status,
       ri.remarks,
       '1' qty,
       u.barcode_no POSTED_BARC,
       u.ebay_item_id LISTED_BARC,
       list_price.LIST_PRIC,
       '' CANCEL_ITEMS,
       ''NO_OF_TIM_CACEL,
       ''CANCEL_AMOUNT,
       ''AWT_ORDERS,
       ''AWT_ORD_PRICE,
       u.order_id ebay_sold,
       ''POS_SOLD,
       ''POS_SALE_PRICE,
       ''RETURN_BARCO,
       ''RETURN_AMOUNT,
       ''NO_OF_TIME_ENDED,
       ''END_BARCODE
       /*,

       list_price.list_date,


       decode(u.returned_id, null, '', det.sale_price) return_amount,
       u.order_id,

       ps.pos_sale_price,
       cancel_item.cANCEL_AMOUNT,
       cancel_item.cancel_status,
       cancel_item.CANCEL_REASON,
       cancel_item.canc_ord_id*/
  from lz_merchant_barcode_dt md,
       lz_merchant_barcode_mt mt,
       lz_special_lots l,
       lot_defination_mt de,
       lz_barcode_mt u,
       lz_salesload_det det,
       lj_item_returned_mt ri,
       lz_item_seed sc,
       lz_item_cond_mt co,
       (select li.ebay_item_id, li.list_price list_pric, li.list_date
          from ebay_list_mt li
         where li.list_id in (select max(e.list_id) list_id
                                from ebay_list_mt e
                               group by e.ebay_item_id)) list_price,
       (select po.barcode_id,
               round(nvl(po.price, 0) +
                     (nvl(po.price, 0) / 100) * nvl(po.sales_tax_perc, 0) -
                     nvl(po.disc_amt, 0),
                     2) pos_sale_price
          from lz_pos_det po
         where po.barcode_id is not null
           AND po.deleted_by IS NULL
           AND po.return_by IS NULL) ps,
       (select BARCODE_NO,
               count(BARCODE_NO) no_of_tim_cacel,
               sum(canccel_amount) cancel_amount,
               max(cancel_status) cancel_status,
               max(CANCEL_REASON) CANCEL_REASON,
               max(EXTENDEDORDERID) canc_ord_id
          from (select i.barcode_no,
                       u.quantity,
                       u.sale_price,
                       round(u.sale_price / u.quantity, 2) canccel_amount,
                       u.orderstatus,
                       u.order_id,
                       u.cancel_id,
                       cm.cancel_status,
                       cm.CANCEL_REASON,
                       i.EXTENDEDORDERID
                  from lj_cancellation_dt i,
                       lz_salesload_det   u,
                       lj_cancellation_mt cm
                 where i.order_id = u.order_id(+)
                   and i.cancellation_id = cm.cancellation_id
                 order by i.barcode_no asc)
         group by BARCODE_NO) cancel_item,

       (select mt.img_mt_id,
               mt.folder_name,
               pp.master_path || mt.folder_name || '/' || dt.image_name full_img_url,
               pp.master_path || mt.folder_name || '/thumb/' ||
               dt.image_name thumb_img_url,
               pp.path_id
          from lj_barcode_pic_mt   mt,
               lj_barcode_pic_dt   dt,
               lz_pict_path_config pp
         where mt.img_mt_id = dt.img_mt_id
           and mt.base_url = pp.path_id
           and dt.img_dt_id in (select min(dt.img_dt_id)
                                  from lj_barcode_pic_dt dt
                                 group by dt.img_mt_id)) get_pictures
 where md.mt_id = mt.mt_id
   and md.barcode_no = l.barcode_prv_no
   and l.barcode_prv_no = u.barcode_no(+)
   and mt.lot_id = de.lot_id(+)
   and u.order_id = det.order_id(+)
   AND TO_CHAR(L.folder_name) = get_pictures.folder_name(+)
   and u.returned_id = ri.returned_id(+)
   and u.item_id = sc.item_id(+)
   and u.lz_manifest_id = sc.lz_manifest_id(+)
   and u.condition_id = sc.default_cond(+)
   and sc.default_cond = co.id(+)
   and u.ebay_item_id = list_price.ebay_item_id(+)
   and u.barcode_no = cancel_item.barcode_no(+)
   and u.barcode_no = ps.barcode_id(+)
   and mt.merchant_id = '$merchId'
   ";
        if (!empty($lot_id)) {
            $qry .= " and trim(upper(de.LOT_DESC )) in ($merLotName)  ";

        }

        if (!empty($searchValue)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $qry .= " and (UPPER(sc.item_title) LIKE '%$key%' ";
                    } else {
                        $qry .= " AND UPPER(sc.item_title) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $qry .= " and (UPPER(sc.item_title) LIKE '%$searchValue%' ";
            }

            $qry .= " OR UPPER(L.barcode_prv_no) LIKE '%$searchValue%'
            OR UPPER(det.extendedorderid) LIKE '%$searchValue%'
            OR UPPER(U.EBAY_ITEM_ID) LIKE '%$searchValue%' )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }

        if ($filter !== 'Verified Item') {
            $qry .= ") WHERE status = '$filter'  ";

        } else {

            $qry .= ") WHERE get_barcode_no IS NOT NULL ";
        }

        // if (empty($searchValue)) {
        // if ($filter == 'Verified Item') {
        //     $qry .= ") WHERE get_barcode_no IS NOT NULL ";
        // } else {
        //     $qry .= ") WHERE status = '$filter'  ";

        // }}

        $query = $this->db->query($qry);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;

        $sqlLength = "SELECT  * FROM (SELECT  q.*, rownum rn FROM  ($qry) q )";

        $sqlLength .= " WHERE   ROWNUM <= " . $requestData['length'] . " AND rn>= " . $requestData['start'];

        if (!empty($columns[$requestData['order'][0]['column']])) {
            $sqlLength .= " ORDER BY  " . $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        }

        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
            $link = "https";
        } else {
            $link = "http";
        }

        // Here append the common URL characters.
        $link .= "://";

        // Append the host(domain name, ip) to the URL.
        $link .= $_SERVER['HTTP_HOST'];
        // var_dump($link);
        $sqlLength = $this->db->query($sqlLength);
        $data = array();
        if ($sqlLength->num_rows() > 0) {
            $result = $sqlLength->result_array();
            foreach ($result as $keys => $rows) {
                //$list_price = @$rows['LIST_PRIC'];
                // Chages made by Adil (Fahad)
                $list_price = number_format((float) @$rows['LIST_PRIC'], 2, '.', '');
                $sale_price = number_format((float) @$rows['SALE_PRICE'], 2, '.', ',');
                $ship_price = number_format((float) @$rows['SHIP_PRICE'], 2, '.', ',');
                $ebay_fee = number_format((float) @$rows['EBAY_FEE'], 2, '.', ',');
                $marketplace_fee = number_format((float) @$rows['MARKETPLACE_FEE'], 2, '.', ',');
                $data[] = array(
                    "<a href=http://www.ebay.com/itm/" . @$rows['EBAY_ITEM_ID'] . " target='_blank'>
                    " . @$rows['EBAY_ITEM_ID'] . "
                   </a>",
                    "<img
                    style='height: 57px;width: 70px'

                    src=" . $link . "/" . @$rows['IMG_URL'] . "

                  />",
                    "<a href=" . $link . ":5000/ledger-trail?" . @$rows['BARCODE_NO'] . " target='_blank'>
                  " . @$rows['BARCODE_NO'] . "
                 </a>",
                    @$rows['LOT_DESC'],
                    @$rows['ITEM_TITLE'],
                    @$rows['COND_NAME'],
                    "<input type='hidden' name='ebay'  class='form-control'  id='ebay" .
                    @$rows['BARCODE_NO'] . "' value='" . @$rows['EBAY_ITEM_ID'] . "' />
                    <input type='number' name='listPrice'   class='form-control'  id='listPrice" .
                    @$rows['BARCODE_NO'] . "' value='" . @$list_price . "' /> <button class='btn btn-primary' type='button' id='reviseButton' value=" .
                    @$rows['BARCODE_NO'] .
                    "> Revise  </button>",
                    @$rows['UPC'],
                    @$rows['MPN'],
                    @$rows['CATEGORY_NAME'],
                    @$rows['ORDER_ID'],
                    @$rows['BUYER_FULLNAME'],
                    @$rows['QTY'],
                    "<span style='float: right;'>$ " . @$sale_price . "</span>",
                    "<span style='float: right;'>$ " . @$ship_price . "</span>",
                    "<span style='float: right;'>$ " . @$ebay_fee . "</span>",
                    "<span style='float: right;'>$ " . @$marketplace_fee . "</span>",
                    @$rows['SALE_DATE'],
                );
            }

            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered),
                // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('status' => true, 'data' => $qry->result_array());
        } else {
            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('status' => false, 'data' => $qry->array());
        }

    }

    /**
     *
     *
     *  Active Listing
     *
     */
    public function Get_Lots()
    {
        $merchant_id = $this->input->post('merchant_id');
        if (!empty($merchant_id)) {
            $merchant_id = $merchant_id['value'];
        } else {
            $merchant_id = '';
        }

        $lot_name = $this->db->query("SELECT M.LOT_ID, M.LOT_DESC LOT_DESC FROM LOT_DEFINATION_MT M WHERE M.MERCHANT_ID  =$merchant_id ")->result_array();
        return array('status' => true, 'data' => $lot_name);
    }

    public function Get_Active_Listing_Detail()
    {
        $lot_id = $this->input->post('merchant_lot');
        $list_filter = $this->input->post('list_filter');
        $merchant_id = $this->input->post('merchant_id');
        $date_filter = $this->input->post('date_filter');
        if (!empty($merchant_id)) {
            if (is_array($merchant_id)) {
                $merchant_id = $merchant_id['value'];

            }
        } else {
            $merchant_id = '';
        }
        $searchItem = $this->input->post('searchItem');
        $searchItem = trim(str_replace("  ", ' ', $searchItem));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);
        $start_date = $this->input->post('startDate');
        $end_date = $this->input->post('endDate');
        $filter = 'Listed Item';

        $requestData = $_REQUEST;

        $merLotName = '';

        if (!empty($lot_id)) {

            // if (is_array($lot_id)) {
            //     if (sizeof($lot_id) > 1) {
            // $cond_exp = '';
            $i = 0;
            foreach ($lot_id as $lot) {
                if (!empty($lot_id[$i + 1])) {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'" . ',';

                } else {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'";
                }
                $i++;

            }

        }

        // if (!empty($lot_id)) {
        //     if (is_array($lot_id)) {
        //         if (sizeof($lot_id) > 1) {
        //             // $cond_exp = '';
        //             $i = 0;
        //             foreach ($lot_id as $lot) {
        //                 if (!empty($lot_id[$i + 1])) {
        //                     $merLotName = $merLotName . $lot['value'] . ',';

        //                 } else {
        //                     $merLotName = $merLotName . $lot['value'];
        //                 }
        //                 $i++;

        //             }

        //         } else {
        //             $merLotName = $merLotName . $lot_id[0]['value'];
        //         }
        //     } else {
        //         $merLotName = $merLotName . $lot['value'];
        //     }
        // }

        $searchValue = strtoupper($requestData['search']['value']);
        $searchValue = trim(str_replace("  ", ' ', $searchValue));
        $searchValue = str_replace(array("`,'"), "", $searchValue);
        $searchValue = str_replace(array("'"), "''", $searchValue);
        //$searchValue='254532353361';
        $str = explode(' ', $searchValue);

        $columns = array(
            // datatable column index  => database column name
            0 => 'EBAY_ITEM_ID / STATUS',
            1 => 'IMG_URL',
            2 => 'BARCODE_NO',
            3 => 'LOT_DESC',
            4 => 'ITEM_TITLE',
            5 => 'COND_NAME',
            6 => 'LIST_PRIC',
            7 => 'LIST_DATE',
            8 => 'MPN',
            9 => 'CATEGORY_NAME',
            10 => 'UPC');

        $qry = "SELECT *
  from (select sc.item_title,
               co.cond_name,
               de.lot_id,
               get_pictures.full_img_url,
               REPLACE(get_pictures.full_img_url, 'D:/wamp/www/', '') img_url,
               REPLACE(get_pictures.thumb_img_url, 'D:/wamp/www/', '') thumb_url,
               '' REF_NO,
               de.lot_desc LOT_DESC,
               l.barcode_prv_no barcode_no,
               TO_CHAR(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') LIST_DATE,
               LIST_PRICE.LIST_DATE LIST_DATEE,
               case
                 when u.returned_id is null and u.ebay_item_id is not null and
                      u.order_id is null and u.lz_pos_mt_id is null then
                  '' || '' || u.ebay_item_id
                 when u.returned_id is not null and
                      u.ebay_item_id is not null and u.lz_pos_mt_id is null then
                  '' || '' || u.ebay_item_id
                 when u.returned_id is null and u.ebay_item_id is not null and
                      u.order_id is not null and u.lz_pos_mt_id is null then
                  '' || '' || u.ebay_item_id
                 when u.returned_id is null and u.ebay_item_id is null and
                      u.order_id is null and u.lz_pos_mt_id is not null then
                  'sold in pos '
                 when u.barcode_no is null then
                  'Item Not Verified'
                 when u.barcode_no is not null and u.ebay_item_id is null and
                      u.order_id is null and u.returned_id is null and
                      u.lz_pos_mt_id is null then
                  'Not Listed'
               end EBAY_ITEM_ID,

               sc.f_upc upc,
               sc.f_mpn mpn,
               sc.category_name,

               case
                 when u.barcode_no is not null and u.ebay_item_id is not null and
                      u.returned_id is null and u.order_id is null and
                      u.lz_pos_mt_id is null then
                  'Listed Item'
                 when u.returned_id is not null and
                      u.ebay_item_id is not null and u.lz_pos_mt_id is null then
                  'Returned Barcode'
                 when u.returned_id is null and u.ebay_item_id is not null and
                      u.order_id is not null and u.lz_pos_mt_id is null then
                  'sold on Ebay'
                 when u.returned_id is null and u.ebay_item_id is null and
                      u.order_id is null and u.lz_pos_mt_id is not null then
                  'sold in pos'
                 when u.barcode_no is null then
                  'Item Not Verified'
                 when u.barcode_no is not null and u.ebay_item_id is null and
                      u.order_id is null and u.returned_id is null and
                      u.lz_pos_mt_id is null then
                  'Not Listed'
               end status,
              /* ri.remarks,*/
               '1' qty,

               list_price.LIST_PRIC
          from lz_merchant_barcode_dt md,
               lz_merchant_barcode_mt mt,
               lz_special_lots l,
               lot_defination_mt de,
               lz_barcode_mt u,
               /*lz_salesload_det det,
               lj_item_returned_mt ri,*/
               lz_item_seed sc,
               lz_item_cond_mt co,
               (select li.ebay_item_id, li.list_price list_pric, li.list_date
                  from ebay_list_mt li
                 where li.list_id in (select max(e.list_id) list_id
                                        from ebay_list_mt e
                                       group by e.ebay_item_id)) list_price,/*,
               (select po.barcode_id,
                       round(nvl(po.price, 0) + (nvl(po.price, 0) / 100) *
                             nvl(po.sales_tax_perc, 0) - nvl(po.disc_amt, 0),
                             2) pos_sale_price
                  from lz_pos_det po
                 where po.barcode_id is not null
                   AND po.deleted_by IS NULL
                   AND po.return_by IS NULL) ps,*/
               /*(select BARCODE_NO,
                       count(BARCODE_NO) no_of_tim_cacel,
                       sum(canccel_amount) cancel_amount,
                       max(cancel_status) cancel_status,
                       max(CANCEL_REASON) CANCEL_REASON,
                       max(EXTENDEDORDERID) canc_ord_id
                  from (select i.barcode_no,
                               u.quantity,
                               u.sale_price,
                               round(u.sale_price / u.quantity, 2) canccel_amount,
                               u.orderstatus,
                               u.order_id,
                               u.cancel_id,
                               cm.cancel_status,
                               cm.CANCEL_REASON,
                               i.EXTENDEDORDERID
                          from lj_cancellation_dt i,
                               lz_salesload_det   u,
                               lj_cancellation_mt cm
                         where i.order_id = u.order_id(+)
                           and i.cancellation_id = cm.cancellation_id
                         order by i.barcode_no asc)
                 group by BARCODE_NO) cancel_item,*/
               (select mt.img_mt_id,
                       mt.folder_name,
                       pp.master_path || mt.folder_name || '/' ||
                       dt.image_name full_img_url,
                       pp.master_path || mt.folder_name || '/thumb/' ||
                       dt.image_name thumb_img_url,
                       pp.path_id
                  from lj_barcode_pic_mt   mt,
                       lj_barcode_pic_dt   dt,
                       lz_pict_path_config pp
                 where mt.img_mt_id = dt.img_mt_id
                   and mt.base_url = pp.path_id
                   and dt.img_dt_id in
                       (select min(dt.img_dt_id)
                          from lj_barcode_pic_dt dt
                         group by dt.img_mt_id)) get_pictures
         where md.mt_id = mt.mt_id
           and md.barcode_no = l.barcode_prv_no
           and l.barcode_prv_no = u.barcode_no(+)
           and mt.lot_id = de.lot_id(+)
           /*and u.order_id = det.order_id(+)*/
           AND TO_CHAR(L.folder_name) = get_pictures.folder_name(+)
           /*and u.returned_id = ri.returned_id(+)*/
           and u.item_id = sc.item_id(+)
           and u.lz_manifest_id = sc.lz_manifest_id(+)
           and u.condition_id = sc.default_cond(+)
           and sc.default_cond = co.id(+)
           and u.ebay_item_id = list_price.ebay_item_id(+)
           /*and u.barcode_no = cancel_item.barcode_no(+)*/
          /* and u.barcode_no = ps.barcode_id(+)*/
   and mt.merchant_id = '$merchant_id'
   ";

        if (!empty($lot_id)) {
            $qry .= " and trim(upper(de.LOT_DESC )) in ($merLotName)  ";

        }

        // if (!empty($lot_id)) {
        //     $qry .= " and mt.lot_id in ($merLotName) ";

        // }

        if (!empty($searchValue)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $qry .= " and (UPPER(sc.item_title) LIKE '%$key%' ";
                    } else {
                        $qry .= " AND UPPER(sc.item_title) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $qry .= " and (UPPER(sc.item_title) LIKE '%$searchValue%' ";
            }

            $qry .= " OR UPPER(L.barcode_prv_no) LIKE '%$searchValue%'

            OR UPPER(U.EBAY_ITEM_ID) LIKE '%$searchValue%' )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }

        if (isset($date_filter)) {
            if ($date_filter == 0) {
                $qry .= " and TO_date(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') =  TO_date(sysdate, 'DD-MM-YYYY HH24:MI:SS') ";
                $qry .= " ) WHERE status IN ('Listed Item','sold on Ebay') ";
            } else if ($date_filter == 1) {
                $qry .= "and  TO_date(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') =  TO_date(sysdate - 1, 'DD-MM-YYYY HH24:MI:SS') ";
                $qry .= " ) WHERE status IN ('Listed Item','sold on Ebay') ";
            } else if ($date_filter == 2) {
                $qry .= " and  TO_date(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') >=  TO_date(sysdate - 7, 'DD-MM-YYYY HH24:MI:SS')";
                $qry .= " ) WHERE status IN ('Listed Item','sold on Ebay') ";
            } else if ($date_filter == 3) {
                $qry .= " and  TO_date(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') >=  TO_date(sysdate - 30, 'DD-MM-YYYY HH24:MI:SS')";
                $qry .= " ) WHERE status IN ('Listed Item','sold on Ebay') ";
            } else {
                $qry .= " ) WHERE status IN ('Listed Item') ";
            }
        } else {

            $qry .= " ) WHERE status IN ('Listed Item') ";
        }

        if (!empty($list_filter)) {
            $filter_value = $list_filter['value'];
            if (@$filter_value == 1) {
                $qry .= " ORDER BY  LIST_DATEE DESC ";
            } else if (@$filter_value == 2) {
                $qry .= " ORDER BY  LIST_PRIC ASC ";
            } else if (@$filter_value == 3) {
                $qry .= " ORDER BY LIST_PRIC DESC ";
            } else if (@$filter_value == 4) {
                $qry .= " ORDER BY  LIST_DATEE ASC ";
            }

        }
        $query = $this->db->query($qry);
        $totalData = $query->num_rows();
        // $totalFiltered = $totalData;

        $sqlLength = "SELECT  * FROM (SELECT  q.*, rownum rn FROM  ($qry) q )";

        $sqlLength .= " WHERE   ROWNUM <= " . $requestData['length'] . " AND rn>= " . $requestData['start'];

        // comment Ordering  Datatable

        // if (!empty($columns[$requestData['order'][0]['column']])) {
        //     $sqlLength .= " ORDER BY  " . $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        // }

        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
            $link = "https";
        } else {
            $link = "http";
        }

        // Here append the common URL characters.
        $link .= "://";

        // Append the host(domain name, ip) to the URL.
        $link .= $_SERVER['HTTP_HOST'];
        // var_dump($link);
        $sqlLength = $this->db->query($sqlLength);
        $data = array();
        $totalFiltered = $sqlLength->num_rows();
        if ($sqlLength->num_rows() > 0) {

            $result = $sqlLength->result_array();

            foreach ($result as $keys => $rows) {
                //$list_price = @$rows['LIST_PRIC'];
                // Chages made by Adil (Fahad)
                $list_price = number_format((float) @$rows['LIST_PRIC'], 2, '.', '');
                $data[] = array(
                    "<a href=http://www.ebay.com/itm/" . @$rows['EBAY_ITEM_ID'] . " target='_blank'>
                    " . @$rows['EBAY_ITEM_ID'] . "
                   </a>  <br/>" . @$rows['STATUS'],
                    "<img
                    style='height: 57px;width: 70px'

                    src=" . $link . "/" . @$rows['THUMB_URL'] . "

                  /> ",
                    "<a href=" . $link . ":5000/ledger-trail?" . @$rows['BARCODE_NO'] . " target='_blank'>
                  " . @$rows['BARCODE_NO'] . "
                 </a>",
                    @$rows['LOT_DESC'],
                    @$rows['ITEM_TITLE'],
                    @$rows['COND_NAME'],
                    "<input type='hidden' name='ebay'  class='form-control'  id='ebay" .
                    @$rows['BARCODE_NO'] . "' value='" . @$rows['EBAY_ITEM_ID'] . "' />
                    <input type='number' name='listPrice'   class='form-control'  id='listPrice" .
                    @$rows['BARCODE_NO'] . "' value='" . @$list_price . "' /> <button class='btn btn-primary' type='button' id='activeReviseButton' value=" .
                    @$rows['BARCODE_NO'] .
                    "> Revise  </button>",
                    @$rows['LIST_DATE'],
                    @$rows['UPC'],
                    @$rows['MPN'],
                    @$rows['CATEGORY_NAME'],

                );
            }

            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('status' => true, 'data' => $qry->result_array());
        } else {
            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('status' => false, 'data' => $qry->array());
        }

    }
    public function Get_Sum_Active_Listing_Detail()
    {
        $lot_id = $this->input->post('merchant_lot');
        $list_filter = $this->input->post('list_filter');
        $merchant_id = $this->input->post('merchant_id');
        if (!empty($merchant_id)) {
            if (is_array($merchant_id)) {
                $merchant_id = $merchant_id['value'];

            }
        } else {
            $merchant_id = '';
        }
        $searchItem = strtoupper($this->input->post('searchItem'));
        $searchItem = trim(str_replace("  ", ' ', $searchItem));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);
        $start_date = $this->input->post('startDate');
        $end_date = $this->input->post('endDate');
        $filter = 'Listed Item';
        $merLotName = '';

        if (!empty($lot_id)) {

            // if (is_array($lot_id)) {
            //     if (sizeof($lot_id) > 1) {
            // $cond_exp = '';
            $i = 0;
            foreach ($lot_id as $lot) {
                if (!empty($lot_id[$i + 1])) {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'" . ',';

                } else {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'";
                }
                $i++;

            }

        }

        // if (!empty($lot_id)) {
        //     if (is_array($lot_id)) {
        //         if (sizeof($lot_id) > 1) {
        //             // $cond_exp = '';
        //             $i = 0;
        //             foreach ($lot_id as $lot) {
        //                 if (!empty($lot_id[$i + 1])) {
        //                     $merLotName = $merLotName . $lot['value'] . ',';

        //                 } else {
        //                     $merLotName = $merLotName . $lot['value'];
        //                 }
        //                 $i++;

        //             }

        //         } else {
        //             $merLotName = $merLotName . $lot_id[0]['value'];
        //         }
        //     } else {
        //         $merLotName = $merLotName . $lot['value'];
        //     }
        // }

        //$searchValue='254532353361';
        $qry = "SELECT count(case
           when TO_date(max(list_price.list_date), 'DD-MM-YYYY HH24:MI:SS') =
                TO_date(sysdate, 'DD-MM-YYYY HH24:MI:SS') then
            to_char(u.barcode_no)
           else
            ''

         end) today_qty,
   nvl(sum(case
             when TO_date(max(list_price.list_date), 'DD-MM-YYYY HH24:MI:SS') =
                  TO_date(sysdate, 'DD-MM-YYYY HH24:MI:SS') then
              to_char(max(list_price.list_pric))
             else
              ''

           end),
       0) today_val,

   count(case
           when TO_date(max(list_price.list_date), 'DD-MM-YYYY HH24:MI:SS') =
                TO_date(sysdate - 1, 'DD-MM-YYYY HH24:MI:SS') then
            to_char(u.barcode_no)
           else
            ''

         end) yesterdy_qty,
   nvl(sum(case
             when TO_date(max(list_price.list_date), 'DD-MM-YYYY HH24:MI:SS') =
                  TO_date(sysdate - 1, 'DD-MM-YYYY HH24:MI:SS') then
              to_char(max(list_price.list_pric))
             else
              ''

           end),
       0) yesterdy_val,

   count(case
           when TO_date(max(list_price.list_date), 'DD-MM-YYYY HH24:MI:SS') >=
                TO_date(sysdate - 7, 'DD-MM-YYYY HH24:MI:SS') then
            to_char(u.barcode_no)
           else
            ''

         end) last_7_day_qty,
   nvl(sum(case
             when TO_date(max(list_price.list_date), 'DD-MM-YYYY HH24:MI:SS') >=
                  TO_date(sysdate - 7, 'DD-MM-YYYY HH24:MI:SS') then
              to_char(max(list_price.list_pric))
             else
              ''

           end),
       0) last_7_day_val,

   count(case
           when TO_date(max(list_price.list_date), 'DD-MM-YYYY HH24:MI:SS') >=
                TO_date(sysdate - 30, 'DD-MM-YYYY HH24:MI:SS') then
            to_char(u.barcode_no)
           else
            ''

         end) last_30_day_qty,
   nvl(sum(case
             when TO_date(max(list_price.list_date), 'DD-MM-YYYY HH24:MI:SS') >=
                  TO_date(sysdate - 30, 'DD-MM-YYYY HH24:MI:SS') then
              to_char(max(list_price.list_pric))
             else
              ''

           end),
       0) last_30_day_val,
   count(case
           when max(u.barcode_no) is not null and
                max(u.ebay_item_id) is not null and max(u.returned_id) is null and
                max(u.order_id) is null and max(u.lz_pos_mt_id) is null then
            to_char(u.barcode_no)
           else
            ''
         end) Active_listed_Qty,
   nvl(sum(case
             when max(u.barcode_no) is not null and
                  max(u.ebay_item_id) is not null and max(u.returned_id) is null and
                  max(u.order_id) is null and max(u.lz_pos_mt_id) is null then
              to_char(max(list_price.list_pric))
             else
              ''
           end),
       0) Active_listed
from lz_merchant_barcode_dt md,
   lz_merchant_barcode_mt mt,
   lz_special_lots l,
   lot_defination_mt de,
   lz_barcode_mt u,

   lj_item_returned_mt ri,
   lz_item_seed sc,
   lz_item_cond_mt co,
   (select li.ebay_item_id, li.list_price list_pric, li.list_date
      from ebay_list_mt li
     where li.list_id in (select max(e.list_id) list_id
                            from ebay_list_mt e
                           group by e.ebay_item_id)) list_price

where md.mt_id = mt.mt_id
and md.barcode_no = l.barcode_prv_no
and l.barcode_prv_no = u.barcode_no(+)
and mt.lot_id = de.lot_id(+)

and u.returned_id = ri.returned_id(+)
and u.item_id = sc.item_id(+)
and u.lz_manifest_id = sc.lz_manifest_id(+)
and u.condition_id = sc.default_cond(+)
and sc.default_cond = co.id(+)
and u.ebay_item_id = list_price.ebay_item_id(+)
and mt.merchant_id = '$merchant_id'
and u.ebay_item_id is not null ";

        if (!empty($lot_id)) {
            $qry .= " and trim(upper(de.LOT_DESC )) in ($merLotName)  ";

        }

        // if (!empty($lot_id)) {
        //     $qry .= " and mt.lot_id in ($merLotName) ";

        // }
        $str = explode(' ', $searchItem);

        if (!empty($searchItem)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $qry .= " and (UPPER(sc.item_title) LIKE '%$key%' ";
                    } else {
                        $qry .= " AND UPPER(sc.item_title) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $qry .= " and (UPPER(sc.item_title) LIKE '%$searchItem%' ";
            }

            $qry .= " OR UPPER(L.barcode_prv_no) LIKE '%$searchItem%'

    OR UPPER(U.EBAY_ITEM_ID) LIKE '%$searchItem%' )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }
        $qry .= " group by u.barcode_no ";
        $data = $this->db->query($qry);
        if ($data->num_rows() > 0) {

            return array('status' => true, 'data' => $data->result_array());
        } else {

            return array('status' => false, 'data' => $data->result_array());
        }

    }

//     public function Get_Sum_Active_Listing_Filter($lot_id, $list_filter, $merchant_id, $searchItem, $filter, $merLotName, $i)
    //     {
    //         $str = explode(' ', $searchItem);

//         $qry = "SELECT NVL(ROUND(SUM(LIST_PRIC), 0),0) LIST_PRICE, NVL(SUM(qty), 0) QTY from (select
    //        TO_CHAR(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') LIST_DATE,
    //        list_pric,
    //        case
    // when u.returned_id is null and u.ebay_item_id is not null and
    // u.order_id is null and u.lz_pos_mt_id is null then
    // '' || '' || u.ebay_item_id
    // when u.returned_id is not null and u.ebay_item_id is not null and
    // u.lz_pos_mt_id is null then
    // '' || '' || u.ebay_item_id
    // when u.returned_id is null and u.ebay_item_id is not null and
    // u.order_id is not null and u.lz_pos_mt_id is null then
    // '' || '' || u.ebay_item_id
    // when u.returned_id is null and u.ebay_item_id is null and
    // u.order_id is null and u.lz_pos_mt_id is not null then
    // 'sold in pos '
    // when u.barcode_no is null then
    // 'Item Not Verified'
    // when u.barcode_no is not null and u.ebay_item_id is null and
    // u.order_id is null and u.returned_id is null and
    // u.lz_pos_mt_id is null then
    // 'Not Listed'
    // end EBAY_ITEM_ID,
    //        decode(det.sale_price, null, ps.pos_sale_price, det.sale_price) sale_price,
    //        det.shippinglabelrate SHIP_PRICE,
    //        det.ebay_fee_perc EBAY_FEE,
    //        det.sale_date,
    //        case
    //        when u.barcode_no is not null and u.ebay_item_id is not null and u.returned_id is null
    //        and u.order_id is null and u.lz_pos_mt_id is null then
    //        'Listed Item'
    //        when u.returned_id is not null and
    //        u.ebay_item_id is not null and u.lz_pos_mt_id is null then
    //        'Returned Barcode'
    //        when u.returned_id is null and u.ebay_item_id is not null and
    //        u.order_id is not null and u.lz_pos_mt_id is null then
    //        'sold on Ebay'
    //        when u.returned_id is null and u.ebay_item_id is null and
    //        u.order_id is null and u.lz_pos_mt_id is not null then
    //        'sold in pos'
    //        when u.barcode_no is null then
    //        'Item Not Verified'
    //        when u.barcode_no is not null and u.ebay_item_id is null and
    //        u.order_id is null and u.returned_id is null and
    //        u.lz_pos_mt_id is null then
    //        'Not Listed'
    //        end status,
    //        '1' qty
    //   from lz_merchant_barcode_dt md,
    //        lz_merchant_barcode_mt mt,
    //        lz_special_lots l,
    //        lot_defination_mt de,
    //        lz_barcode_mt u,
    //        lz_salesload_det det,
    //        lj_item_returned_mt ri,
    //        lz_item_seed sc,
    //        lz_item_cond_mt co,
    //        (select li.ebay_item_id, li.list_price list_pric, li.list_date
    //           from ebay_list_mt li
    //          where li.list_id in (select max(e.list_id) list_id
    //                                 from ebay_list_mt e
    //                                group by e.ebay_item_id)) list_price,
    //        (select po.barcode_id,
    //                round(nvl(po.price, 0) +
    //                      (nvl(po.price, 0) / 100) * nvl(po.sales_tax_perc, 0) -
    //                      nvl(po.disc_amt, 0),
    //                      2) pos_sale_price
    //           from lz_pos_det po
    //          where po.barcode_id is not null
    //            AND po.deleted_by IS NULL
    //            AND po.return_by IS NULL) ps,
    //        (select BARCODE_NO,
    //                count(BARCODE_NO) no_of_tim_cacel,
    //                sum(canccel_amount) cancel_amount,
    //                max(cancel_status) cancel_status,
    //                max(CANCEL_REASON) CANCEL_REASON,
    //                max(EXTENDEDORDERID) canc_ord_id
    //           from (select i.barcode_no,
    //                        u.quantity,
    //                        u.sale_price,
    //                        round(u.sale_price / u.quantity, 2) canccel_amount,
    //                        u.orderstatus,
    //                        u.order_id,
    //                        u.cancel_id,
    //                        cm.cancel_status,
    //                        cm.CANCEL_REASON,
    //                        i.EXTENDEDORDERID
    //                   from lj_cancellation_dt i,
    //                        lz_salesload_det   u,
    //                        lj_cancellation_mt cm
    //                  where i.order_id = u.order_id(+)
    //                    and i.cancellation_id = cm.cancellation_id
    //                  order by i.barcode_no asc)
    //          group by BARCODE_NO) cancel_item,

//        (select mt.img_mt_id,
    //                mt.folder_name,
    //                pp.master_path || mt.folder_name || '/' || dt.image_name full_img_url,
    //                pp.master_path || mt.folder_name || '/thumb/' ||
    //                dt.image_name thumb_img_url,
    //                pp.path_id
    //           from lj_barcode_pic_mt   mt,
    //                lj_barcode_pic_dt   dt,
    //                lz_pict_path_config pp
    //          where mt.img_mt_id = dt.img_mt_id
    //            and mt.base_url = pp.path_id
    //            and dt.img_dt_id in (select min(dt.img_dt_id)
    //                                   from lj_barcode_pic_dt dt
    //                                  group by dt.img_mt_id)) get_pictures
    //  where md.mt_id = mt.mt_id
    //    and md.barcode_no = l.barcode_prv_no
    //    and l.barcode_prv_no = u.barcode_no(+)
    //    and mt.lot_id = de.lot_id(+)
    //    and u.order_id = det.order_id(+)
    //    AND TO_CHAR(L.folder_name) = get_pictures.folder_name(+)
    //    and u.returned_id = ri.returned_id(+)
    //    and u.item_id = sc.item_id(+)
    //    and u.lz_manifest_id = sc.lz_manifest_id(+)
    //    and u.condition_id = sc.default_cond(+)
    //    and sc.default_cond = co.id(+)
    //    and u.ebay_item_id = list_price.ebay_item_id(+)
    //    and u.barcode_no = cancel_item.barcode_no(+)
    //    and u.barcode_no = ps.barcode_id(+)
    //    and mt.merchant_id = '$merchant_id'
    //    ";
    //         if ($i == 0) {
    //             //and  TO_date(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') >=  TO_date(sysdate - 7, 'DD-MM-YYYY HH24:MI:SS')

//             $qry .= " and  TO_date(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') =  TO_date(sysdate, 'DD-MM-YYYY HH24:MI:SS') ";
    //         } else if ($i == 1) {
    //             $qry .= " and  TO_date(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') =  TO_date(sysdate - 1, 'DD-MM-YYYY HH24:MI:SS') ";
    //             //$qry .= " and list_price.list_date >= sysdate-1 ";
    //         } else if ($i == 2) {
    //             $qry .= " and  TO_date(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') >=  TO_date(sysdate - 7, 'DD-MM-YYYY HH24:MI:SS')";
    //             //$qry .= " and list_price.list_date >= sysdate-7 ";
    //         } else if ($i == 3) {
    //             $qry .= " and  TO_date(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') >=  TO_date(sysdate - 30, 'DD-MM-YYYY HH24:MI:SS')";
    //             //$qry .= " and list_price.list_date >= sysdate-30 ";
    //         }
    //         if (!empty($lot_id)) {
    //             $qry .= " and mt.lot_id in ($merLotName) ";

//         }

//         if (!empty($searchItem)) {
    //             if (count($str) > 1) {
    //                 $i = 1;
    //                 foreach ($str as $key) {
    //                     if ($i === 1) {
    //                         $qry .= " and (UPPER(sc.item_title) LIKE '%$key%' ";
    //                     } else {
    //                         $qry .= " AND UPPER(sc.item_title) LIKE '%$key%' ";
    //                     }
    //                     $i++;
    //                 }
    //             } else {
    //                 $qry .= " and (UPPER(sc.item_title) LIKE '%$searchItem%' ";
    //             }

//             $qry .= " OR UPPER(L.barcode_prv_no) LIKE '%$searchItem%'
    //             OR UPPER(det.extendedorderid) LIKE '%$searchItem%'
    //             OR UPPER(U.EBAY_ITEM_ID) LIKE '%$searchItem%' )";
    //             //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
    //         }

//         if ($i !== 4) {
    //             $qry .= ") WHERE status in('Listed Item','sold on Ebay')";
    //         } else {
    //             $qry .= ") WHERE status in('Listed Item')";
    //         }

//         // if ($filter !== 'Verified Item') {
    //         //     $qry .= ") WHERE status = '$filter'  ";

//         // } else {

//         //     $qry .= ") WHERE get_barcode_no IS NOT NULL ";
    //         // }
    //         if (!empty($list_filter)) {
    //             $filter_value = $list_filter['value'];
    //             if (@$filter_value == 1) {
    //                 $qry .= " ORDER BY  LIST_DATE DESC ";
    //             } else if (@$filter_value == 2) {
    //                 $qry .= " ORDER BY  LIST_PRIC ASC ";
    //             } else if (@$filter_value == 3) {
    //                 $qry .= " ORDER BY LIST_PRIC DESC ";
    //             } else if (@$filter_value == 4) {
    //                 $qry .= " ORDER BY  LIST_DATE ASC ";
    //             }

//         }
    //         return $query = $this->db->query($qry)->result_array();
    //     }

    public function Get_Sold_Item_Detail()
    {
        $lot_id = $this->input->post('merchant_lot');
        $merchant_id = $this->input->post('merchant_id');
        if (!empty($merchant_id)) {
            if (is_array($merchant_id)) {
                $merchant_id = $merchant_id['value'];
            }
        } else {
            $merchant_id = '';
        }
        $searchItem = strtoupper($this->input->post('searchItem'));
        $searchItem = trim(str_replace("  ", ' ', $searchItem));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);
        $start_date = $this->input->post('startDate');
        $end_date = $this->input->post('endDate');
        $list_filter = $this->input->post('list_filter');
        $date_filter = $this->input->post('date_filter');
        $filter = 'sold on Ebay';

        $requestData = $_REQUEST;

        $merLotName = '';
        if (!empty($lot_id)) {

            // if (is_array($lot_id)) {
            //     if (sizeof($lot_id) > 1) {
            // $cond_exp = '';
            $i = 0;
            foreach ($lot_id as $lot) {
                if (!empty($lot_id[$i + 1])) {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'" . ',';

                } else {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'";
                }
                $i++;

            }

        }
        // else {
        //     $merLotName = $merLotName . $lot_id[0]['label'];
        // }
        // } else {
        //     $merLotName = $merLotName . $lot['label'];
        // }

        // if (!empty($lot_id)) {
        //     if (is_array($lot_id)) {
        //         if (sizeof($lot_id) > 1) {
        //             // $cond_exp = '';
        //             $i = 0;
        //             foreach ($lot_id as $lot) {
        //                 if (!empty($lot_id[$i + 1])) {
        //                     $merLotName = $merLotName . $lot['value'] . ',';

        //                 } else {
        //                     $merLotName = $merLotName . $lot['value'];
        //                 }
        //                 $i++;

        //             }

        //         } else {
        //             $merLotName = $merLotName . $lot_id[0]['value'];
        //         }
        //     } else {
        //         $merLotName = $merLotName . $lot['value'];
        //     }
        // }

        $searchValue = strtoupper($requestData['search']['value']);
        $searchValue = trim(str_replace("  ", ' ', $searchValue));
        $searchValue = str_replace(array("`,'"), "", $searchValue);
        $searchValue = str_replace(array("'"), "''", $searchValue);
        //$searchValue='254532353361';
        $str = explode(' ', $searchValue);

        $columns = array(
            // datatable column index  => database column name
            0 => 'EBAY_ITEM_ID / STATUS',
            1 => 'IMG_URL',
            2 => 'BARCODE_NO',
            3 => 'LOT_DESC',
            4 => 'ITEM_TITLE',
            5 => 'COND_NAME',
            6 => 'LIST_PRIC',
            7 => 'SALE_DATE',
            8 => 'SALE_PRICE',
            9 => 'ORDER_ID',
            10 => 'BUYER_FULLNAME',
            11 => 'QTY',
            12 => 'SHIP_PRICE',
            13 => 'EBAY_FEE',
            14 => 'MARKETPLACE_FEE',
            15 => 'UPC',
            16 => 'MPN',
            17 => 'CATEGORY_NAME',

        );

        $qry = "SELECT * from (select sc.item_title,
       co.cond_name,
       de.lot_id,
       get_pictures.full_img_url,
REPLACE(get_pictures.full_img_url,'D:/wamp/www/','') img_url,
REPLACE(get_pictures.thumb_img_url,'D:/wamp/www/','') thumb_url,
       ''REF_NO,
       de.lot_desc LOT_DESC,
       l.barcode_prv_no barcode_no,
       u.barcode_no get_barcode_no,
       '' lot_cost,
       TO_CHAR(list_price.list_date, 'DD-MM-YYYY HH24:MI:SS') LIST_DATE,
       case
when u.returned_id is null and u.ebay_item_id is not null and
u.order_id is null and u.lz_pos_mt_id is null then
'' || '' || u.ebay_item_id
when u.returned_id is not null and u.ebay_item_id is not null and
u.lz_pos_mt_id is null then
'' || '' || u.ebay_item_id
when u.returned_id is null and u.ebay_item_id is not null and
u.order_id is not null and u.lz_pos_mt_id is null then
'' || '' || u.ebay_item_id
when u.returned_id is null and u.ebay_item_id is null and
u.order_id is null and u.lz_pos_mt_id is not null then
'sold in pos '
when u.barcode_no is null then
'Item Not Verified'
when u.barcode_no is not null and u.ebay_item_id is null and
u.order_id is null and u.returned_id is null and
u.lz_pos_mt_id is null then
'Not Listed'
end EBAY_ITEM_ID,
       ''GENERATED_BAR,
       ''PICTURE_BARC,
       sc.f_upc upc,
       sc.f_mpn mpn,
       sc.category_name,
       decode(det.sale_price, null, ps.pos_sale_price, det.sale_price) sale_price,
       det.shippinglabelrate SHIP_PRICE,
       det.ebay_fee_perc EBAY_FEE,
       det.extendedorderid order_id,
       det.buyer_fullname,
        decode(
               TO_CHAR(det.sale_date, 'DD-MM-YYYY HH24:MI:SS'),null,TO_CHAR(ps.pos_date, 'DD-MM-YYYY HH24:MI:SS'),TO_CHAR(det.sale_date, 'DD-MM-YYYY HH24:MI:SS') ) SALE_DATE,



       decode(DET.SALE_DATE,null,ps.pos_date,DET.SALE_DATE)
        SALE_DATEE,
       case
       when u.barcode_no is not null and u.ebay_item_id is not null and u.returned_id is null
       and u.order_id is null and u.lz_pos_mt_id is null then
       'Listed Item'
       when u.returned_id is not null and
       u.ebay_item_id is not null and u.lz_pos_mt_id is null then
       'Returned Barcode'
       when u.returned_id is null and u.ebay_item_id is not null and
       u.order_id is not null and u.lz_pos_mt_id is null then
       'sold on Ebay'
       when u.returned_id is null and u.ebay_item_id is null and
       u.order_id is null and u.lz_pos_mt_id is not null then
       'sold in pos'
       when u.barcode_no is null then
       'Item Not Verified'
       when u.barcode_no is not null and u.ebay_item_id is null and
       u.order_id is null and u.returned_id is null and
       u.lz_pos_mt_id is null then
       'Not Listed'
       end status,
       ri.remarks,
       '1' qty,
       u.barcode_no POSTED_BARC,
       u.ebay_item_id LISTED_BARC,
       list_price.LIST_PRIC,
       '' CANCEL_ITEMS,
       ''NO_OF_TIM_CACEL,
       ''CANCEL_AMOUNT,
       ''AWT_ORDERS,
       ''AWT_ORD_PRICE,
       u.order_id ebay_sold,
       ''POS_SOLD,
       ''POS_SALE_PRICE,
       ''RETURN_BARCO,
       ''RETURN_AMOUNT,
       ''NO_OF_TIME_ENDED,
       ''END_BARCODE
  from lz_merchant_barcode_dt md,
       lz_merchant_barcode_mt mt,
       lz_special_lots l,
       lot_defination_mt de,
       lz_barcode_mt u,
       lz_salesload_det det,
       lj_item_returned_mt ri,
       lz_item_seed sc,
       lz_item_cond_mt co,
       (select li.ebay_item_id, li.list_price list_pric, li.list_date
          from ebay_list_mt li
         where li.list_id in (select max(e.list_id) list_id
                                from ebay_list_mt e
                               group by e.ebay_item_id)) list_price,
       (select po.barcode_id,(select max(mt.entered_date_time) from lz_pos_mt mt where mt.lz_pos_mt_id = po.lz_pos_mt_id ) pos_date,
               round(nvl(po.price, 0) +
                     (nvl(po.price, 0) / 100) * nvl(po.sales_tax_perc, 0) -
                     nvl(po.disc_amt, 0),
                     2) pos_sale_price
          from lz_pos_det po
         where po.barcode_id is not null
           AND po.deleted_by IS NULL
           AND po.return_by IS NULL) ps,
       (select BARCODE_NO,
               count(BARCODE_NO) no_of_tim_cacel,
               sum(canccel_amount) cancel_amount,
               max(cancel_status) cancel_status,
               max(CANCEL_REASON) CANCEL_REASON,
               max(EXTENDEDORDERID) canc_ord_id
          from (select i.barcode_no,
                       u.quantity,
                       u.sale_price,
                       round(u.sale_price / u.quantity, 2) canccel_amount,
                       u.orderstatus,
                       u.order_id,
                       u.cancel_id,
                       cm.cancel_status,
                       cm.CANCEL_REASON,
                       i.EXTENDEDORDERID
                  from lj_cancellation_dt i,
                       lz_salesload_det   u,
                       lj_cancellation_mt cm
                 where i.order_id = u.order_id(+)
                   and i.cancellation_id = cm.cancellation_id
                 order by i.barcode_no asc)
         group by BARCODE_NO) cancel_item,

       (select mt.img_mt_id,
               mt.folder_name,
               pp.master_path || mt.folder_name || '/' || dt.image_name full_img_url,
               pp.master_path || mt.folder_name || '/thumb/' ||
               dt.image_name thumb_img_url,
               pp.path_id
          from lj_barcode_pic_mt   mt,
               lj_barcode_pic_dt   dt,
               lz_pict_path_config pp
         where mt.img_mt_id = dt.img_mt_id
           and mt.base_url = pp.path_id
           and dt.img_dt_id in (select min(dt.img_dt_id)
                                  from lj_barcode_pic_dt dt
                                 group by dt.img_mt_id)) get_pictures
 where md.mt_id = mt.mt_id
   and md.barcode_no = l.barcode_prv_no
   and l.barcode_prv_no = u.barcode_no(+)
   and mt.lot_id = de.lot_id(+)
   and u.order_id = det.order_id(+)
   AND TO_CHAR(L.folder_name) = get_pictures.folder_name(+)
   and u.returned_id = ri.returned_id(+)
   and u.item_id = sc.item_id(+)
   and u.lz_manifest_id = sc.lz_manifest_id(+)
   and u.condition_id = sc.default_cond(+)
   and sc.default_cond = co.id(+)
   and u.ebay_item_id = list_price.ebay_item_id(+)
   and u.barcode_no = cancel_item.barcode_no(+)
   and u.barcode_no = ps.barcode_id(+)
   and mt.merchant_id = '$merchant_id'
   ";

        if (!empty($lot_id)) {
            $qry .= " and trim(upper(de.LOT_DESC )) in ($merLotName)  ";

        }

        // if (!empty($lot_id)) {
        //          $qry .= " and trim(upper(de.LOT_DESC )) = trim(upper('Remote ')) /* and (UPPER(de.LOT_DESC) LIKE '%REMOTE%' ) */";

        //  }

        if (!empty($searchValue)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $qry .= " and (UPPER(sc.item_title) LIKE '%$key%' ";
                        // $qry .= " and (UPPER(de.LOT_DESC) LIKE '%$key%' ";
                    } else {
                        $qry .= " AND UPPER(sc.item_title) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $qry .= " and (UPPER(sc.item_title) LIKE '%$searchValue%' ";
            }

            $qry .= " OR UPPER(L.barcode_prv_no) LIKE '%$searchValue%'
            OR UPPER(det.extendedorderid) LIKE '%$searchValue%'
            OR UPPER(mt.lot_id) LIKE '%$searchValue%'
            OR UPPER(U.EBAY_ITEM_ID) LIKE '%$searchValue%' )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }

        if (isset($date_filter)) {
            if ($date_filter == 0) {
                $qry .= " and decode(TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS'),
                   null,
                   TO_date(ps.pos_date, 'DD-MM-YYYY HH24:MI:SS'),
                   TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS'))  =
            TO_date(sysdate , 'DD-MM-YYYY HH24:MI:SS') ) WHERE status IN ('sold on Ebay', 'sold in pos')";
            } else if ($date_filter == 1) {
                $qry .= " and decode(TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS'),
                   null,
                   TO_date(ps.pos_date, 'DD-MM-YYYY HH24:MI:SS'),
                   TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS'))  =
            TO_date(sysdate -1 , 'DD-MM-YYYY HH24:MI:SS') ) WHERE status IN ('sold on Ebay', 'sold in pos')";
            } else if ($date_filter == 2) {
                $qry .= " and decode(TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS'),
                   null,
                   TO_date(ps.pos_date, 'DD-MM-YYYY HH24:MI:SS'),
                   TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS'))  >=
            TO_date(sysdate -7 , 'DD-MM-YYYY HH24:MI:SS') ) WHERE status IN ('sold on Ebay', 'sold in pos')";
            } else if ($date_filter == 3) {
                $qry .= " and decode(TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS'),
                   null,
                   TO_date(ps.pos_date, 'DD-MM-YYYY HH24:MI:SS'),
                   TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS'))  >=
            TO_date(sysdate -30 , 'DD-MM-YYYY HH24:MI:SS') ) WHERE status IN ('sold on Ebay', 'sold in pos')";
            } else if ($date_filter == 4) {
                $qry .= ") WHERE status IN ('sold on Ebay', 'sold in pos')  ";
            } else if ($date_filter == 5) {
                $qry .= " and u.returned_id is not null) ";
            }

        } else {

            if (!empty($searchValue)) {

                $qry .= ") WHERE status IN ('sold on Ebay', 'sold in pos')  ";
            } else {
                $qry .= ") WHERE status IN ('sold on Ebay', 'sold in pos')  ";
            }

        }

        // if ($filter !== 'Verified Item') {
        //     $qry .= ") WHERE status IN ('sold on Ebay', 'sold in pos')  ";

        // } else {

        //     $qry .= ") WHERE get_barcode_no IS NOT NULL ";
        // }

        if (!empty($list_filter)) {
            $filter_value = $list_filter['value'];
            if (@$filter_value == 1) {
                $qry .= " ORDER BY  SALE_DATEE DESC ";
            } else if (@$filter_value == 2) {
                $qry .= " ORDER BY  SALE_PRICE ASC ";
            } else if (@$filter_value == 3) {
                $qry .= " ORDER BY SALE_PRICE DESC ";
            } else if (@$filter_value == 4) {
                $qry .= " ORDER BY  SALE_DATEE ASC ";
            }

        }

        $query = $this->db->query($qry);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;

        $sqlLength = "SELECT  * FROM (SELECT  q.*, rownum rn FROM  ($qry) q )";

        $sqlLength .= " WHERE   ROWNUM <= " . $requestData['length'] . " AND rn >= " . $requestData['start'];

        //  Comment Column Filter

        // if (!empty($columns[$requestData['order'][0]['column']])) {
        //     $sqlLength .= " ORDER BY  " . $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
        // }

        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
            $link = "https";
        } else {
            $link = "http";
        }

        // Here append the common URL characters.
        $link .= "://";

        // Append the host(domain name, ip) to the URL.
        $link .= $_SERVER['HTTP_HOST'];
        // var_dump($link);
        $sqlLength = $this->db->query($sqlLength);
        // var_dump($sqlLength);
        // exit;
        $data = array();
        if ($sqlLength->num_rows() > 0) {
            $result = $sqlLength->result_array();

            foreach ($result as $keys => $rows) {
                //$list_price = @$rows['LIST_PRIC'];
                // Chages made by Adil (Fahad)
                $list_price = number_format((float) @$rows['LIST_PRIC'], 2, '.', '');
                $sale_price = number_format((float) @$rows['SALE_PRICE'], 2, '.', ',');
                $ship_price = number_format((float) @$rows['SHIP_PRICE'], 2, '.', ',');
                $ebay_fee = number_format((float) @$rows['EBAY_FEE'], 2, '.', ',');
                $marketplace_fee = number_format((float) @$rows['MARKETPLACE_FEE'], 2, '.', ',');
                $data[] = array(
                    "<a href=http://www.ebay.com/itm/" . @$rows['EBAY_ITEM_ID'] . " target='_blank'>
                    " . @$rows['EBAY_ITEM_ID'] . "
                   </a> <br/>" . @$rows['STATUS'],
                    "<img
                    style='height: 57px;width: 70px'

                    src=" . $link . "/" . @$rows['THUMB_URL'] . "

                  />",
                    "<a href=" . $link . ":5000/ledger-trail?" . @$rows['BARCODE_NO'] . " target='_blank'>
                  " . @$rows['BARCODE_NO'] . "
                 </a>",
                    @$rows['LOT_DESC'],
                    @$rows['ITEM_TITLE'],
                    @$rows['COND_NAME'],
                    "<span style='float: right;'>$ " . @$list_price . "</span>",
                    // "<input type='hidden' name='ebay'  class='form-control'  id='ebay" .
                    // @$rows['BARCODE_NO'] . "' value='" . @$rows['EBAY_ITEM_ID'] . "' />
                    // <input type='number' name='listPrice'   class='form-control'  id='listPrice" .
                    // @$rows['BARCODE_NO'] . "' value='" . @$list_price . "' /> <button class='btn btn-primary' type='button' id='reviseButton' value=" .
                    // @$rows['BARCODE_NO'] .
                    // "> Revise  </button>",
                    @$rows['SALE_DATE'],
                    "<span style='float: right;'>$ " . @$sale_price . "</span>",
                    @$rows['ORDER_ID'],
                    @$rows['BUYER_FULLNAME'],
                    @$rows['QTY'],

                    "<span style='float: right;'>$ " . @$ship_price . "</span>",
                    "<span style='float: right;'>$ " . @$ebay_fee . "</span>",
                    "<span style='float: right;'>$ " . @$marketplace_fee . "</span>",
                    @$rows['UPC'],
                    @$rows['MPN'],
                    @$rows['CATEGORY_NAME'],

                );
            }

            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered),
                // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('status' => true, 'data' => $qry->result_array());
        } else {
            return $output = array(
                "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval($totalFiltered),
                "recordsFiltered" => intval($totalFiltered), // searching, if there is no searching then totalFiltered = totalData
                "deferLoading" => intval($totalFiltered),
                "data" => $data,
            );
            // return array('status' => false, 'data' => $qry->array());
        }
    }

    public function Get_Sum_Sold_Item_Detail()
    {
        $lot_id = $this->input->post('merchant_lot');
        $merchant_id = $this->input->post('merchant_id');
        if (!empty($merchant_id)) {
            if (is_array($merchant_id)) {
                $merchant_id = $merchant_id['value'];
            }
        } else {
            $merchant_id = '';
        }

        $searchItem = strtoupper($this->input->post('searchItem'));
        $searchItem = trim(str_replace("  ", ' ', $searchItem));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);
        $start_date = $this->input->post('startDate');
        $end_date = $this->input->post('endDate');
        $list_filter = $this->input->post('list_filter');
        $filter = "'sold on Ebay', 'sold in pos'";
        $merLotName = '';

        if (!empty($lot_id)) {

            // if (is_array($lot_id)) {
            //     if (sizeof($lot_id) > 1) {
            // $cond_exp = '';
            $i = 0;
            foreach ($lot_id as $lot) {
                if (!empty($lot_id[$i + 1])) {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'" . ',';

                } else {
                    $merLotName = $merLotName . "'" . strtoupper(trim($lot['label'])) . "'";
                }
                $i++;

            }

        }

        // if (!empty($lot_id)) {
        //     if (is_array($lot_id)) {
        //         if (sizeof($lot_id) > 1) {
        //             // $cond_exp = '';
        //             $i = 0;
        //             foreach ($lot_id as $lot) {
        //                 if (!empty($lot_id[$i + 1])) {
        //                     $merLotName = $merLotName . $lot['value'] . ',';

        //                 } else {
        //                     $merLotName = $merLotName . $lot['value'];
        //                 }
        //                 $i++;

        //             }

        //         } else {
        //             $merLotName = $merLotName . $lot_id[0]['value'];
        //         }
        //     } else {
        //         $merLotName = $merLotName . $lot['value'];
        //     }
        // }
        // $data = [];
        // for ($i = 0; $i <= 4; $i++) {
        //     $result = $this->Get_Sum_Sold_Item_Filter($lot_id, $merLotName, $searchItem, $merchant_id, $list_filter, $filter, $i);
        //     $data[] = $result[0];
        // }

        $data = $this->Get_Sum_Sold_Item_Filter($lot_id, $merLotName, $searchItem, $merchant_id, $list_filter, $filter);
        if (sizeof($data) > 0) {
            return array(
                'status' => true,
                "data" => $data,
            );
        } else {
            return array(
                'status' => false,
                "data" => $data,
            );
        }
    }

    public function Get_Sum_Sold_Item_Filter($lot_id, $merLotName, $searchItem, $merchant_id, $list_filter, $filter)
    {
        $str = explode(' ', $searchItem);

        $qry = "SELECT * FROM (SELECT
        count(case
                when decode(TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'),
                            null,
                            TO_date(max(ps.pos_date), 'DD-MM-YYYY HH24:MI:SS'),
                            TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'))  =
                     TO_date(sysdate , 'DD-MM-YYYY HH24:MI:SS') and
                     max(u.returned_id) is null then
                 to_char(u.barcode_no)
                else
                 ''
              end) today_sale_qty,

        sum(case
              when decode(TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'),
                          null,
                          TO_date(max(ps.pos_date), 'DD-MM-YYYY HH24:MI:SS'),
                          TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'))  =
                   TO_date(sysdate , 'DD-MM-YYYY HH24:MI:SS') and
                   max(u.returned_id) is null then
               to_char(decode(max(det.sale_price),
                              null,
                              max(ps.pos_sale_price),
                              max(det.sale_price)))
              else
               ''
            end) today_sale_price,
        count(case
                when decode(TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'),
                            null,
                            TO_date(max(ps.pos_date), 'DD-MM-YYYY HH24:MI:SS'),
                            TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS')) =
                     TO_date(sysdate - 1, 'DD-MM-YYYY HH24:MI:SS') and
                     max(u.returned_id) is null then
                 to_char(u.barcode_no)
                else
                 ''
              end) yesterday_sale_qty,

        sum(case
              when decode(TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'),
                          null,
                          TO_date(max(ps.pos_date), 'DD-MM-YYYY HH24:MI:SS'),
                          TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS')) =
                   TO_date(sysdate - 1, 'DD-MM-YYYY HH24:MI:SS') and
                   max(u.returned_id) is null then
               to_char(decode(max(det.sale_price),
                              null,
                              max(ps.pos_sale_price),
                              max(det.sale_price)))
              else
               ''
            end) yesterday_sale_price,
        count(case
                when decode(TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'),
                            null,
                            TO_date(max(ps.pos_date), 'DD-MM-YYYY HH24:MI:SS'),
                            TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS')) >=
                     TO_date(sysdate - 7, 'DD-MM-YYYY HH24:MI:SS') and
                     max(u.returned_id) is null then
                 to_char(u.barcode_no)
                else
                 ''
              end) last_7_day_sale_qty,

        sum(case
              when decode(TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'),
                          null,
                          TO_date(max(ps.pos_date), 'DD-MM-YYYY HH24:MI:SS'),
                          TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS')) >=
                   TO_date(sysdate - 7, 'DD-MM-YYYY HH24:MI:SS') and
                   max(u.returned_id) is null then
               to_char(decode(max(det.sale_price),
                              null,
                              max(ps.pos_sale_price),
                              max(det.sale_price)))
              else
               ''
            end) last_7_day_sale_price,
        count(case
                when decode(TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'),
                            null,
                            TO_date(max(ps.pos_date), 'DD-MM-YYYY HH24:MI:SS'),
                            TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS')) >=
                     TO_date(sysdate - 30, 'DD-MM-YYYY HH24:MI:SS') and
                     max(u.returned_id) is null then
                 to_char(u.barcode_no)
                else
                 ''
              end) last_30_day_sale_qty,

        sum(case
              when decode(TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS'),
                          null,
                          TO_date(max(ps.pos_date), 'DD-MM-YYYY HH24:MI:SS'),
                          TO_date(max(det.sale_date), 'DD-MM-YYYY HH24:MI:SS')) >=
                   TO_date(sysdate - 30, 'DD-MM-YYYY HH24:MI:SS') and
                   max(u.returned_id) is null then
               to_char(decode(max(det.sale_price),
                              null,
                              max(ps.pos_sale_price),
                              max(det.sale_price)))
              else
               ''
            end) last_30_day_sale_price,

        count(case
                when max(u.returned_id) is null then
                 to_char(u.barcode_no)
                else
                 ''
              end) total_sale_qty,
        sum(case
              when max(u.returned_id) is null then
               to_char(decode(max(det.sale_price),
                              null,
                              max(ps.pos_sale_price),
                              max(det.sale_price)))
              else
               ''
            end) total_sales,
            count(case
                when max(u.returned_id) is not null then
                 to_char(u.barcode_no)
                else
                 ''
              end) return_qty,

             sum(case
              when max(u.returned_id) is not null then
               to_char(decode(max(det.sale_price),
                              null,
                              max(ps.pos_sale_price),
                              max(det.sale_price)))
              else
               ''
            end) total_return_amount



       /* pos pus ebay */

         from lz_merchant_barcode_dt md,
              lz_merchant_barcode_mt mt,
              lz_special_lots l,
              lot_defination_mt de,
              lz_barcode_mt u,
              lz_salesload_det det,
              lj_item_returned_mt ri,
              lz_item_seed sc,
              lz_item_cond_mt co,
              (select li.ebay_item_id, li.list_price list_pric, li.list_date
                 from ebay_list_mt li
                where li.list_id in (select max(e.list_id) list_id
                                       from ebay_list_mt e
                                      group by e.ebay_item_id)) list_price,
              (select po.barcode_id,
                      (select max(mt.entered_date_time)
                         from lz_pos_mt mt
                        where mt.lz_pos_mt_id = po.lz_pos_mt_id) pos_date,
                      round(nvl(po.price, 0) +
                            (nvl(po.price, 0) / 100) * nvl(po.sales_tax_perc, 0) -
                            nvl(po.disc_amt, 0),
                            2) pos_sale_price
                 from lz_pos_det po
                where po.barcode_id is not null
                  AND po.deleted_by IS NULL
                  AND po.return_by IS NULL) ps
        where md.mt_id = mt.mt_id
          and md.barcode_no = l.barcode_prv_no
          and l.barcode_prv_no = u.barcode_no(+)
          and mt.lot_id = de.lot_id(+)
          and u.order_id = det.order_id(+)

          and u.returned_id = ri.returned_id(+)
          and u.item_id = sc.item_id(+)
          and u.lz_manifest_id = sc.lz_manifest_id(+)
          and u.condition_id = sc.default_cond(+)
          and sc.default_cond = co.id(+)
          and u.ebay_item_id = list_price.ebay_item_id(+)

          and u.barcode_no = ps.barcode_id(+)
          and mt.merchant_id = '$merchant_id'
          and (u.order_id is not null or u.lz_pos_mt_id is not null)
        ";
        // if ($i == 0) {
        //     $qry .= " and TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS') =  TO_date(sysdate, 'DD-MM-YYYY HH24:MI:SS') ";
        // } else if ($i == 1) {
        //     $qry .= " and TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS') =  TO_date(sysdate-1, 'DD-MM-YYYY HH24:MI:SS') ";
        // } else if ($i == 2) {
        //     $qry .= " and TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS') >= TO_date(sysdate - 7, 'DD-MM-YYYY HH24:MI:SS') ";
        // } else if ($i == 3) {
        //     $qry .= " and TO_date(det.sale_date, 'DD-MM-YYYY HH24:MI:SS') >= TO_date(sysdate - 30, 'DD-MM-YYYY HH24:MI:SS')  ";
        // }

        if (!empty($lot_id)) {
            $qry .= " and trim(upper(de.LOT_DESC )) in ($merLotName)  ";

        }

        // if (!empty($lot_id)) {
        //     $qry .= " and mt.lot_id in ($merLotName) ";

        // }

        if (!empty($searchItem)) {
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $qry .= " and (UPPER(sc.item_title) LIKE '%$key%' ";
                    } else {
                        $qry .= " AND UPPER(sc.item_title) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $qry .= " and (UPPER(sc.item_title) LIKE '%$searchItem%' ";
            }

            $qry .= " OR UPPER(L.barcode_prv_no) LIKE '%$searchItem%'
            OR UPPER(det.extendedorderid) LIKE '%$searchItem%'
            OR UPPER(U.EBAY_ITEM_ID) LIKE '%$searchItem%' )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }

        // if ($filter !== 'Verified Item') {
        //     // $qry .= ") WHERE status IN ($filter)  ";
        //     $qry .= " GROUP BY  u.barcode_no)";

        // } else {

        $qry .= "GROUP BY  u.barcode_no)";
        // $qry .= " ) WHERE get_barcode_no IS NOT NULL ";
        //}

        // if (!empty($list_filter)) {
        //     $filter_value = $list_filter['value'];
        //     if (@$filter_value == 1) {
        //         $qry .= " ORDER BY  SALE_DATE DESC ";
        //     } else if (@$filter_value == 2) {
        //         $qry .= " ORDER BY  SALE_PRICE ASC ";
        //     } else if (@$filter_value == 3) {
        //         $qry .= " ORDER BY SALE_PRICE DESC ";
        //     } else if (@$filter_value == 4) {
        //         $qry .= " ORDER BY  SALE_DATE ASC ";
        //     }

        // }

        return $query = $this->db->query($qry)->result_array();
    }

}
