<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Single Entry Model
 */
class m_searchOrder extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function getMerchantAccounts()
    {
        $accounts = $this->db->query("SELECT A.LZ_SELLER_ACCT_ID, A.SELL_ACCT_DESC FROM LZ_SELLER_ACCTS A")->result_array();
        if ($accounts) {
            return array("found" => true, "accounts" => $accounts);
        } else {
            return array("found" => false, "message" => "Something went wrong!");
        }
    }
    public function search_by_ebay_id()
    {
        $ebay_id = $this->input->post('ebay_id');

        $searchResults = $this->db->query("select item_title,
        f_upc,
        f_mpn,
        f_manufacture,
        cond_name,
        ebay_item_id,
        sold_qty,
        barcode_no,
        merchant_name,
        total_qty,
        LAST_LISTER_ID,
        LAST_LISTER_NAME,
        total_qty - sold_qty active_qty,
        qc_check,
        qc_date,
        qc_by,
        seed_id,
        qc_remarks
   from (select s.item_title,
                s.f_upc,
                s.f_mpn,
                s.f_manufacture,
                c.cond_name,
                b.ebay_item_id,
                mm.buisness_name merchant_name,
                nvl(sold_qty, 0) sold_qty,
                em.lister_id LAST_LISTER_ID,
                ee.user_name LAST_LISTER_NAME,
                s.qc_remarks,
        s.qc_check,
        s.qc_date,
        s.qc_by,
        s.seed_id
           from lz_barcode_mt b,
                lz_item_seed s,
                lz_item_cond_mt c,
                ebay_list_mt   em,
                lj_merhcant_acc_dt acc,
                lz_merchant_mt mm,
                employee_mt ee,
               (select l.ebay_item_id, max(l.list_id) list_id
               from ebay_list_mt l , lz_barcode_mt bb
               where bb.list_id = l.list_id
               group by l.ebay_item_id) mx,
                (select sum(dt.quantity) sold_qty, max(dt.item_id) ebay_id
                   from lz_salesload_det dt
                  where dt.item_id = '$ebay_id'
                    and dt.orderstatus = 'Completed'
                    and dt.cancel_id is null
                    and dt.return_id is null)
          where s.item_id = b.item_id
            and b.list_id = em.list_id
            AND b.CREATED_TRHOUGH IS NULL
            and em.lz_seller_acct_id = acc.acct_id
            and em.list_id = mx.list_id
            and em.lister_id = ee.employee_id
            and acc.merchant_id = mm.merchant_id
            and s.lz_manifest_id = b.lz_manifest_id
            and s.default_cond = b.condition_id
            and c.id = b.condition_id
            and b.ebay_item_id = '$ebay_id'
          order by s.date_time desc),
        (SELECT LISTAGG(b.barcode_no, ', ') WITHIN GROUP(ORDER BY b.barcode_no) barcode_no,
                count(1) total_qty
           FROM lz_barcode_mt b
          where b.ebay_item_id = '$ebay_id'
        AND b.CREATED_TRHOUGH IS NULL)

  where rownum = 1")->row();

        $data = $this->db->query("select * from lz_barcode_mt bb where bb.ebay_item_id = '$ebay_id'
      order by bb.lz_manifest_id")->result();

        $lz_ids = array(); //array to store unique emails (the ones we've already used)
        foreach ($data as $value) {

            $id = $value->LZ_MANIFEST_ID;
            if (in_array($id, $lz_ids)) { //If in array, skip iteration
                continue;
            }

            $lz_ids[] = $id; //Add email to "used" emails array
        }

        $data1 = array();
        foreach ($data as $key => $value) {
            $lz_manifest_id = $value->LZ_MANIFEST_ID;
            $data1[$lz_manifest_id][] = $value;
        }

        if ($searchResults) {
            $bar_codes = explode(",", $searchResults->BARCODE_NO);
            return array("found" => true, "searchResult" => $searchResults, 'bar_codes' => $bar_codes, 'block_data' => $data1, 'lz_ids' => $lz_ids);
        } else {
            return array("found" => false, "message" => "Data Not Found");
        }
    }
    public function getCancellationsFromEbay($accout = "")
    {
        $accountId = $this->input->post("accountId");
        if (isset($accountId)) {
            $accountId = $accountId;
        } else {
            $accountId = $accout;
        }

        $ebayCancellations = $this->db->query("SELECT CM.CANCELLATION_ID,
         CM.EBAY_CANCEL_ID,
         BM.BARCODE_NO,
         BM.EBAY_ITEM_ID,
         TM.ITEM_MT_MFG_PART_NO MPN,
         TM.ITEM_MT_UPC UPC,
         TM.ITEM_DESC TITLE,
         TM.ITEM_MT_MANUFACTURE BRAND,
         REPLACE(CM.MARKETPLACE_ID, '_', ' ') MARKETPLACE_ID,
         REPLACE(CM.REQUESTOR_TYPE, '_', ' ') REQUESTOR_TYPE,
         REPLACE(CM.CANCEL_STATE, '_', ' ') CANCEL_STATE,
         REPLACE(CM.CANCEL_STATUS, '_', ' ') CANCEL_STATUS,
         REPLACE(CM.CANCEL_CLOSE_REASON, '_', ' ') CANCEL_CLOSE_REASON,
         REPLACE(CM.PAYMENT_STATUS, '_', ' ') PAYMENT_STATUS,
         REPLACE(CM.CANCEL_REASON, '_', ' ') CANCEL_REASON,
         CM.LEGACY_ORDER_ID,
         CM.CREATED_AT,
         CM.CREATED_BY,
         CM.ACCOUNT_ID,
         ACC.SELL_ACCT_DESC ACCOUNT_NAME,
         DET.SALES_RECORD_NUMBER,
         CASE WHEN DET.SALES_RECORD_NUMBER IS NOT NULL THEN 'APPROVED' ELSE 'PENDING' END STATUS
         FROM LJ_CANCELLATION_MT CM,
         LZ_BARCODE_MT BM,
         ITEMS_MT TM,
         LZ_SELLER_ACCTS ACC,
         LZ_SALESLOAD_DET DET
         WHERE CM.LEGACY_ORDER_ID = DET.EXTENDEDORDERID
         AND BM.ITEM_ID = TM.ITEM_ID
         AND cm.legacy_order_id = BM.Extendedorderid
         AND ACC.LZ_SELLER_ACCT_ID = CM.ACCOUNT_ID
         AND CM.ACCOUNT_ID = $accountId
         ORDER BY CM.CANCELLATION_ID DESC")->result_array();

        if ($ebayCancellations) {
            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_pictures_by_barcode($ebayCancellations, $conditions);
            $images = $uri['uri'];

            return array("found" => true, "cancellations" => $ebayCancellations, "images" => $images);
        } else {
            return array("found" => false, "message" => "No Cancellations Found");
        }
    }
    public function getCancellationsFromEbaySingle($accout, $extended_order_id)
    {
        $accountId = $accout;
        $ebayCancellations = $this->db->query("SELECT CM.CANCELLATION_ID,
         CM.EBAY_CANCEL_ID,
         BM.BARCODE_NO,
         BM.EBAY_ITEM_ID,
         TM.ITEM_MT_MFG_PART_NO MPN,
         TM.ITEM_MT_UPC UPC,
         TM.ITEM_DESC TITLE,
         TM.ITEM_MT_MANUFACTURE BRAND,
         REPLACE(CM.MARKETPLACE_ID, '_', ' ') MARKETPLACE_ID,
         REPLACE(CM.REQUESTOR_TYPE, '_', ' ') REQUESTOR_TYPE,
         REPLACE(CM.CANCEL_STATE, '_', ' ') CANCEL_STATE,
         REPLACE(CM.CANCEL_STATUS, '_', ' ') CANCEL_STATUS,
         REPLACE(CM.CANCEL_CLOSE_REASON, '_', ' ') CANCEL_CLOSE_REASON,
         REPLACE(CM.PAYMENT_STATUS, '_', ' ') PAYMENT_STATUS,
         REPLACE(CM.CANCEL_REASON, '_', ' ') CANCEL_REASON,
         CM.LEGACY_ORDER_ID,
         CM.CREATED_AT,
         CM.CREATED_BY,
         CM.ACCOUNT_ID,
         ACC.SELL_ACCT_DESC ACCOUNT_NAME,
         DET.SALES_RECORD_NUMBER,
         CASE WHEN DET.SALES_RECORD_NUMBER IS NOT NULL THEN 'APPROVED' ELSE 'PENDING' END STATUS
         FROM LJ_CANCELLATION_MT CM,
         LZ_BARCODE_MT BM,
         ITEMS_MT TM,
         LZ_SELLER_ACCTS ACC,
         LZ_SALESLOAD_DET DET
         WHERE CM.LEGACY_ORDER_ID = DET.EXTENDEDORDERID
         AND BM.ITEM_ID = TM.ITEM_ID
         AND cm.legacy_order_id = BM.Extendedorderid
         AND ACC.LZ_SELLER_ACCT_ID = CM.ACCOUNT_ID
         AND CM.LEGACY_ORDER_ID = '$extended_order_id'
         AND CM.ACCOUNT_ID = $accountId
         ORDER BY CM.CANCELLATION_ID DESC")->result_array();

        if ($ebayCancellations) {
            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_pictures_by_barcode($ebayCancellations, $conditions);
            $images = $uri['uri'];

            return array("found" => true, "approve" => true, "cancellations" => $ebayCancellations, "images" => $images);
        } else {
            return array("found" => false, "approve" => false, "message" => "No Cancellations Found");
        }
    }
    public function processCancel()
    {
        $accountId = $this->input->post("accountId");
        $cancel_id = $this->input->post("cancel_id");
        $ebay_cancel_id = $this->input->post("ebay_cancel_id");
        $radioModel = $this->input->post("radioModel");
        $remarks = $this->input->post("Remarks");
        $cancel_by = $this->input->post("userId");
        $bin_id = $this->input->post("bin_id");

        $check_bin_id = $this->db->query("SELECT BIN_ID FROM BIN_MT BB WHERE UPPER(BB.BIN_TYPE) ||'-'||BB.BIN_NO = UPPER('$bin_id')")->row();
        if ($check_bin_id) {
            $bin_id = $check_bin_id->BIN_ID;
        } else {
            $bin_id = 0;
        }

        $qyer = $this->db->query("Call pro_processCancel('$cancel_id',
                                                    '',
                                                  '$radioModel' ,
                                                  '$cancel_by'   ,
                                                  '$remarks'   ,
                                                  '$bin_id' )");

        $qyer1 = $this->db->query("SELECT  CANCEL_ID from LZ_SALESLOAD_DET s where s.CANCEL_ID ='$cancel_id'")->result_array();
        if ($qyer1) {
            return array("status" => true, "data" => $qyer1);
        } else {
            return array("status" => false);
        }
    }

    public function getOrderByID()
    {
        $saleRecordNo = $this->input->post('searchOrder');

        $searchResults = $this->db->query("SELECT LZ_BARCODE_MT_ID,
       BM.BARCODE_NO,
       BM.EBAY_ITEM_ID,
       BM.SALE_RECORD_NO,
       BIN.BIN_TYPE || '-' || BIN.BIN_NO BIN_NAME,
       IT.ITEM_DESC TITLE,
       IT.ITEM_MT_UPC UPC,
       IT.ITEM_MT_MFG_PART_NO MPN,
       IT.ITEM_MT_MANUFACTURE BRAND,
       SD.LZ_SALESLOAD_DET_ID,
       SD.BUYER_FULLNAME,
       SD.BUYER_PHONE_NUMBER,
       SD.BUYER_EMAIL,
       SD.BUYER_ADDRESS1,
       SD.BUYER_ADDRESS2,
       SD.BUYER_CITY,
       SD.BUYER_STATE,
       SD.BUYER_ZIP,
       SD.BUYER_COUNTRY,
       SD.SHIP_TO_ADDRESS1,
       SD.SHIP_TO_ADDRESS2,
       SD.SHIP_TO_CITY,
       SD.SHIP_TO_STATE,
       SD.SHIP_TO_ZIP,
       SD.SHIP_TO_COUNTRY,
       MA.SHIPPING_FULL_NAME,
       MA.SHIPPING_ADDRESS1,
       MA.SHIPPING_ADDRESS2,
       MA.SHIPPING_CITY,
       MA.SHIPPING_STATE,
       MA.SHIPPING_ZIP,
       MA.SHIPPING_COUNTRY,
       SD.QUANTITY,
       SD.MANUAL_BUYER_ADDRESS,
       VD.BARCODE_NO VERIFIED_YN,
       WG.PACK_WEIGHT,
       ACC.ACCOUNT_NAME,
       CASE
                  WHEN SD.TRACKING_NUMBER IS NOT NULL THEN
                   'SHIPPED'
                  ELSE
                   'NOTSHIPPED'
                END ORDER_SHIPPED,
                CASE
                  WHEN BM.PULLING_PRINT_YN = 1 THEN
                   'PRINTED'
                  ELSE
                   'NOTPRINTED'
                   END ORDER_PRINTED,
                   CASE
                  WHEN BM.PULLING_ID IS NOT NULL THEN
                   'PULLED'
                  ELSE
                   'NOTPULLED'
                END ORDER_PULLED,
                CASE
                  WHEN VD.BARCODE_NO IS NOT NULL THEN
                   'VERIFIED'
                  ELSE
                   'NOTVERIFIED'
                END VERIFIED_YN,
       nvl(RETURN_SHIP_AMOUNT,0.00) RETURN_SHIP_AMOUNT
  FROM LZ_BARCODE_MT       BM,
       ITEMS_MT            IT,
       BIN_MT              BIN,
       LZ_SALESLOAD_DET    SD,
       LZ_SALESLOAD_MT     SM,
       LJ_MERHCANT_ACC_DT  ACC,
       LJ_BIN_VERIFY_DT    VD,
       LZ_ITEM_PACK_WEIGHT WG,
       MANUAL_ADDRESS_MT   MA
 WHERE IT.ITEM_ID = BM.ITEM_ID
   AND BM.BIN_ID = BIN.BIN_ID(+)
   AND SM.LZ_SELLER_ACCT_ID = ACC.ACCT_ID(+)
   AND SM.LZ_SALELOAD_ID = SD.LZ_SALELOAD_ID
   AND SD.MANUAL_BUYER_ADDRESS = MA.MANUAL_ADDRESS_ID(+)
   AND IT.ITEM_ID = WG.ITEM_ID(+)
   AND BM.BARCODE_NO = VD.BARCODE_NO(+)
   AND BM.SALE_RECORD_NO = SD.SALES_RECORD_NUMBER
   AND (SD.SALES_RECORD_NUMBER = '$saleRecordNo' OR SD.ORDER_ID = '$saleRecordNo' OR SD.EXTENDEDORDERID = '$saleRecordNo' OR SD.ITEM_ID = '$saleRecordNo' )")->result_array();

        if ($searchResults) {
            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_pictures_by_barcode($searchResults, $conditions);

            $images = $uri['uri'];
            return array("found" => true, "searchResults" => $searchResults, "images" => $images);
        } else {
            return array("found" => false, "message" => "NO DATA FOUND!");
        }

    }
    public function changeAddress()
    {

        $sale_record_no = $this->input->post('sale_record_no');
        $lz_salesload_det_id = $this->input->post('lz_salesload_det_id');

        $full_name = $this->input->post('full_name');
        $full_name = trim(str_replace("  ", ' ', $full_name));
        $full_name = trim(str_replace(array("'"), "''", $full_name));

        $address1 = $this->input->post('address1');
        $address1 = trim(str_replace("  ", ' ', $address1));
        $address1 = trim(str_replace(array("'"), "''", $address1));

        $address2 = $this->input->post('address2');
        $address2 = trim(str_replace("  ", ' ', $address2));
        $address2 = trim(str_replace(array("'"), "''", $address2));

        $buyerCity = $this->input->post('buyerCity');
        $buyerCity = trim(str_replace("  ", ' ', $buyerCity));
        $buyerCity = trim(str_replace(array("'"), "''", $buyerCity));

        $buyerState = $this->input->post('buyerState');
        $buyerState = trim(str_replace("  ", ' ', $buyerState));
        $buyerState = trim(str_replace(array("'"), "''", $buyerState));

        $buyerZip = $this->input->post('buyerZip');
        $buyerZip = trim(str_replace("  ", ' ', $buyerZip));
        $buyerZip = trim(str_replace(array("'"), "''", $buyerZip));

        $buyerCountry = $this->input->post('buyerCountry');
        $buyerCountry = trim(str_replace("  ", ' ', $buyerCountry));
        $buyerCountry = trim(str_replace(array("'"), "''", $buyerCountry));

        $detail_id = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('MANUAL_ADDRESS_MT','LZ_SALELOAD_DET_ID') DET_ID FROM DUAL")->result_array();

        $det_id = $detail_id[0]['DET_ID'];

        $this->db->query("INSERT INTO MANUAL_ADDRESS_MT A (A.MANUAL_ADDRESS_ID,
       A.LZ_SALELOAD_DET_ID,
       A.ORDER_ID,
       A.SHIPPING_FULL_NAME,
       A.SHIPPING_ADDRESS1,
       A.SHIPPING_ADDRESS2,
       A.SHIPPING_CITY,
       A.SHIPPING_STATE,
       A.SHIPPING_ZIP,
       A.SHIPPING_COUNTRY) VALUES (
        $det_id,
        $lz_salesload_det_id,
        $sale_record_no,
        '$full_name',
        '$address1',
        '$address2',
        '$buyerCity',
        '$buyerState',
        '$buyerZip',
        '$buyerCountry'
    )");

        $update = $this->db->query("UPDATE LZ_SALESLOAD_DET SET MANUAL_BUYER_ADDRESS = $det_id WHERE SALES_RECORD_NUMBER = '$sale_record_no' AND LZ_SALESLOAD_DET_ID = $lz_salesload_det_id ");

        if ($update) {
            return array("update" => true, "message" => "Address Changed!");
        } else {
            return array("update" => false, "message" => "Something Went Wrong!");
        }

    }
    public function get_pictures_by_barcode($barcodes, $conditions)
    {

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();

        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        foreach ($barcodes as $barcode) {

            $bar = $barcode['BARCODE_NO'];

            $cancellation_id = @$barcode['CANCELLATION_ID'];

            $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE LOT.BARCODE_PRV_NO = '$bar' ")->result_array();
            $folderName = "";
            if ($getFolder) {
                $folderName = $getFolder[0]['FOLDER_NAME'];
            } else {
                $folderName = $bar;
            }

            $dir = "";
            $barcodePictures = $master_path . $folderName . "/";
            $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

            if (is_dir($barcodePictures)) {
                $dir = $barcodePictures;
            } else if (is_dir($barcodePicturesThumb)) {
                $dir = $barcodePicturesThumb;
            }

            if (!is_dir($dir)) {
                $getBarcodeMt = $this->db->query("SELECT IT.ITEM_MT_UPC UPC, IT.ITEM_MT_MFG_PART_NO MPN FROM LZ_BARCODE_MT MT, ITEMS_MT IT WHERE MT.ITEM_ID = IT.ITEM_ID AND MT.BARCODE_NO = '$bar' ")->result_array();
                $upc = "";
                $mpn = "";
                if ($getBarcodeMt) {
                    $upc = $getBarcodeMt[0]['UPC'];
                    $mpn = $getBarcodeMt[0]['MPN'];
                }

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();

                $upc_mpn_master_path = $path[0]["MASTER_PATH"];

                $mpn = str_replace("/", "_", $mpn);
                foreach ($conditions as $cond) {
                    $dir = $upc_mpn_master_path . $upc . "~" . $mpn . "/" . $cond['COND_NAME'] . "/";
                    $dirWithMpn = $upc_mpn_master_path . "~" . $mpn . "/" . $cond['COND_NAME'] . "/";
                    $dirWithUpc = $upc_mpn_master_path . $upc . "~/" . $cond['COND_NAME'] . "/";
                    if (is_dir($dir)) {
                        $dir = $dir;
                        break;
                    } else if (is_dir($dirWithMpn)) {
                        $dir = $dirWithMpn;
                        break;
                    } else if (is_dir($dirWithUpc)) {
                        $dir = $dirWithUpc;
                        break;
                    } else {
                        $dir = $upc_mpn_master_path . "/" . $cond['COND_NAME'] . "/";
                    }

                }
                if ($dir == $upc_mpn_master_path . "~/" . $cond['COND_NAME'] . "/") {
                    $dir = $upc_mpn_master_path . "/" . $cond['COND_NAME'] . "/";
                }

            }
            $dir = preg_replace("/[\r\n]*/", "", $dir);

            if (is_dir($dir)) {
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

                if ($images) {
                    $j = 0;
                    foreach ($images as $image) {

                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                        if ($cancellation_id) {
                            $uri[$cancellation_id][$j] = $base_url . $withoutMasterPartUri;
                        } else {
                            $uri[$bar][$j] = $base_url . $withoutMasterPartUri;
                        }
                        // if($uri[$barcode['LZ_barcode_ID']]){
                        //     break;
                        // }

                        $j++;
                    }
                } else {
                    if ($cancellation_id) {
                        $uri[$cancellation_id][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                        $uri[$cancellation_id][1] = false;
                    } else {
                        $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                        $uri[$bar][1] = false;
                    }
                }
            } else {
                if ($cancellation_id) {
                    $uri[$cancellation_id][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$cancellation_id][1] = false;
                } else {
                    $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar][1] = false;
                }
            }
        }

        return array('uri' => $uri);
    }
    public function search_barcode_for_assign_market_place()
    {
        $type = $this->input->post('type');
        $single_barcode = $this->input->post('single_barcode');
        $start_barcode = $this->input->post('start_barcode');
        $end_barcode = $this->input->post('end_barcode');
        if ($type == 'single') {
            $qry = "SELECT B.BARCODE_NO BARCODE_PRV_NO,
            TO_CHAR(l.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
            l.LIST_ID,
            l.STATUS,
            '$' || l.LIST_PRICE LIST_PRICE,
            e.user_name lister_name,
            S.SEED_ID,
            M.SINGLE_ENTRY_ID,
            M.LZ_MANIFEST_ID,
            M.LOADING_NO,
            M.LOADING_DATE,
            M.PURCH_REF_NO,
            B.ITEM_ID,
            d.laptop_item_code,
            S.ITEM_TITLE ITEM_MT_DESC,
            s.f_manufacture MANUFACTURER,
            l.list_qty,
            S.F_MPN MFG_PART_NO,
            S.F_UPC UPC,
            BCD.EBAY_ITEM_ID,
            BCD.CONDITION_ID ITEM_CONDITION,
            BCD.QTY QUANTITY,
            '$' || BCD.COST_PRICE COST_PRICE,
            C.COND_NAME,
            (select m.buisness_name
               from lz_merchant_barcode_mt mt,
                    lz_merchant_barcode_dt dt,
                    lz_merchant_mt         m
              where mt.mt_id = dt.mt_id
                and m.merchant_id = mt.merchant_id
                and dt.barcode_no = b.barcode_no) BUISNESS_NAME
       from lz_barcode_mt b,
            ebay_list_mt l,
            employee_mt e,
            lz_manifest_mt m,
            lz_manifest_det d,
            items_mt i,
            lz_item_seed s,
            LZ_ITEM_COND_MT c,
            (SELECT BC.EBAY_ITEM_ID,
                    BC.LZ_MANIFEST_ID,
                    BC.ITEM_ID,
                    BC.CONDITION_ID,
                    COUNT(1) QTY,
                    max(dt.po_detail_retial_price) COST_PRICE
               FROM LZ_BARCODE_MT BC,
                    items_mt i,
                    lz_manifest_det dt,
                    (select item_id, lz_manifest_id, condition_id
                       from lz_barcode_mt
                      where barcode_no = '$single_barcode') bb
              WHERE BC.Item_Id = bb.item_id
                and bc.lz_manifest_id = bb.lz_manifest_id
                and bc.condition_id = bb.condition_id
                and dt.lz_manifest_id = bc.lz_manifest_id
                and dt.laptop_item_code = i.item_code
                and to_char(dt.conditions_seg5) = to_char(bc.condition_id)
                and i.item_id = bc.item_id
              GROUP BY BC.LZ_MANIFEST_ID,
                       BC.ITEM_ID,
                       BC.CONDITION_ID,
                       BC.EBAY_ITEM_ID) bcd
      where b.list_id = l.list_id(+)
        and e.employee_id(+) = l.lister_id
        and m.lz_manifest_id = b.lz_manifest_id
        and d.laptop_item_code = i.item_code
        and d.lz_manifest_id = m.lz_manifest_id
        and b.item_id = i.item_id
        and s.item_id = b.item_id
        and s.lz_manifest_id = b.lz_manifest_id
        and s.default_cond = b.condition_id
        and b.condition_id = c.id
        and bcd.item_id = b.item_id
        and bcd.lz_manifest_id = b.lz_manifest_id
        and bcd.condition_id = b.condition_id
        and b.barcode_no = '$single_barcode'";
        } else {
            $qry = "SELECT B.BARCODE_NO BARCODE_PRV_NO,
        TO_CHAR(l.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
        l.LIST_ID,
        l.STATUS,
        '$' || l.LIST_PRICE LIST_PRICE,
        e.user_name lister_name,
        S.SEED_ID,
        M.SINGLE_ENTRY_ID,
        M.LZ_MANIFEST_ID,
        M.LOADING_NO,
        M.LOADING_DATE,
        M.PURCH_REF_NO,
        B.ITEM_ID,
        d.laptop_item_code,
        S.ITEM_TITLE ITEM_MT_DESC,
        s.f_manufacture MANUFACTURER,
        l.list_qty,
        S.F_MPN MFG_PART_NO,
        S.F_UPC UPC,
        BCD.EBAY_ITEM_ID,
        BCD.CONDITION_ID ITEM_CONDITION,
        BCD.QTY QUANTITY,
        '$' || BCD.COST_PRICE COST_PRICE,
        C.COND_NAME,
        (select m.buisness_name
           from lz_merchant_barcode_mt mt,
                lz_merchant_barcode_dt dt,
                lz_merchant_mt         m
          where mt.mt_id = dt.mt_id
            and m.merchant_id = mt.merchant_id
            and dt.barcode_no = b.barcode_no) BUISNESS_NAME
   from lz_barcode_mt b,
        ebay_list_mt l,
        employee_mt e,
        lz_manifest_mt m,
        lz_manifest_det d,
        items_mt i,
        lz_item_seed s,
        LZ_ITEM_COND_MT c,
        (SELECT BC.EBAY_ITEM_ID,
                BC.LZ_MANIFEST_ID,
                BC.ITEM_ID,
                BC.CONDITION_ID,
                COUNT(1) QTY,
                max(dt.po_detail_retial_price) COST_PRICE
           FROM LZ_BARCODE_MT BC,
                items_mt i,
                lz_manifest_det dt,
                (select item_id, lz_manifest_id, condition_id
                   from lz_barcode_mt
                  where barcode_no between '$start_barcode' and '$end_barcode') bb
          WHERE BC.Item_Id = bb.item_id
            and bc.lz_manifest_id = bb.lz_manifest_id
            and bc.condition_id = bb.condition_id
            and dt.lz_manifest_id = bc.lz_manifest_id
            and dt.laptop_item_code = i.item_code
            and to_char(dt.conditions_seg5) = to_char(bc.condition_id)
            and i.item_id = bc.item_id
          GROUP BY BC.LZ_MANIFEST_ID,
                   BC.ITEM_ID,
                   BC.CONDITION_ID,
                   BC.EBAY_ITEM_ID) bcd
  where b.list_id = l.list_id(+)
    and e.employee_id(+) = l.lister_id
    and m.lz_manifest_id = b.lz_manifest_id
    and d.laptop_item_code = i.item_code
    and d.lz_manifest_id = m.lz_manifest_id
    and b.item_id = i.item_id
    and s.item_id = b.item_id
    and s.lz_manifest_id = b.lz_manifest_id
    and s.default_cond = b.condition_id
    and b.condition_id = c.id
    and bcd.item_id = b.item_id
    and bcd.lz_manifest_id = b.lz_manifest_id
    and bcd.condition_id = b.condition_id
    and b.barcode_no between '$start_barcode' and '$end_barcode'";

        }
        $qry = $this->db->query($qry)->result_array();
        $market_assign = $this->db->query("select mt.portal_id,mt.portal_desc from lz_portal_mt mt where mt.active_yn = 1 order by mt.order_id asc")->result_array();

        if (count($qry) >= 1) {
            $check = $this->db->query("SELECT barcode_no , LISTAGG(portal_id, ',') WITHIN GROUP (ORDER BY portal_id) portal_id
        FROM lz_barcode_portal_dt dt
        where dt.barcode_no = 56395
        group by barcode_no ")->row();
            if ($check) {
                $barcode_assigned = explode(',', $check->PORTAL_ID);
            } else {
                $barcode_assigned = [];
            }
            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_identiti_bar_pics($qry, $conditions);
            $images = $uri['uri'];

            foreach ($qry as $key => $value) {

                $qry[$key]['MFG_PART_NO'] = str_replace('-', ' ', $value['MFG_PART_NO']);
                $current_timestamp = date('m/d/Y');
                $purchase_date = $value['LOADING_DATE'];
                $date1 = date_create($current_timestamp);
                $date2 = date_create($purchase_date);
                $diff = date_diff($date1, $date2);
                $date_rslt = $diff->format("%R%a days");
                $qry[$key]['LOADING_DATE'] = abs($date_rslt) . " Days";
            }
            //   print_r($qyer);
            return array("images" => $images, 'query' => $qry, "exist" => true, 'market_assign' => $market_assign, 'barcode_assigned' => $barcode_assigned);
        } else {
            return array('images' => [], 'query' => [], 'exist' => false, 'market_assign' => []);
        }

    }

    public function assign_market_place_data()
    {

        $market_assign = $this->db->query("select mt.portal_id,mt.portal_desc from lz_portal_mt mt where mt.active_yn = 1 order by mt.order_id asc")->result_array();
        return array("exist" => true, 'market_assign' => $market_assign);
    }

    public function get_identiti_bar_pics($barcodes, $conditions)
    {

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();

        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        // $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        $base_url = 'http://71.78.236.20/';
        foreach ($barcodes as $barcode) {

            $bar = $barcode['BARCODE_PRV_NO'];

            if (!empty($bar)) {

                $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE lot.barcode_prv_no = '$bar' and rownum <= 1  ")->result_array();
            }

            $folderName = "";
            if ($getFolder) {
                $folderName = $getFolder[0]['FOLDER_NAME'];
            } else {
                $folderName = $bar;
            }

            $dir = "";
            $barcodePictures = $master_path . $folderName . "/";
            $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

            if (is_dir($barcodePictures)) {
                $dir = $barcodePictures;
            } else if (is_dir($barcodePicturesThumb)) {
                $dir = $barcodePicturesThumb;
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);

            if (is_dir($dir)) {
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

                if ($images) {
                    $j = 0;
                    foreach ($images as $image) {

                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                        $uri[$bar][$j] = $base_url . $withoutMasterPartUri;
                        // if($uri[$barcode['LZ_barcode_ID']]){
                        //     break;
                        // }

                        $j++;
                    }
                } else {
                    $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar][1] = false;
                }
            } else {
                $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar][1] = false;
            }
        }
        return array('uri' => $uri);
    }
    public function save_market_place_assign()
    {
        $ebay = $this->input->post('ebay');
        $offerUp = $this->input->post('offerUp');
        $facebook = $this->input->post('facebook');
        $instagram = $this->input->post('instagram');
        $barcode_numbers = $this->input->post('barcode_numbers');
        $user_id = $this->input->post('user_id');
        $remarks = $this->input->post('remarks');
        $new_bar = explode(',', $barcode_numbers);
        foreach ($new_bar as $key => $value) {
            if ($ebay) {
                $check = $this->db->query("SELECT * from lz_barcode_portal_dt where barcode_no = '$value' and portal_id = '$ebay'")->row();
                if (count($check) == 0) {

                    $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lz_barcode_portal_dt', 'PORTAL_DT_ID')ID FROM DUAL")->result_array();
                    $pk = $query[0]['ID'];
                    $stat_query = $this->db->query("INSERT INTO
                    lz_barcode_portal_dt(PORTAL_DT_ID,PORTAL_ID,BARCODE_NO,INSERTED_BY,INSERTED_DATE,REMARKS)
                        values
                        ($pk,
                        '$ebay',
                        '$value',
                        '$user_id',
                        sysdate,
                        '$remarks')");
                }
            }
            if ($offerUp) {
                $check = $this->db->query("SELECT * from lz_barcode_portal_dt where barcode_no = '$value' and portal_id = '$offerUp'")->row();
                if (count($check) == 0) {
                    $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lz_barcode_portal_dt', 'PORTAL_DT_ID')ID FROM DUAL")->result_array();
                    $pk = $query[0]['ID'];
                    $stat_query = $this->db->query("INSERT INTO
                    lz_barcode_portal_dt(PORTAL_DT_ID,PORTAL_ID,BARCODE_NO,INSERTED_BY,INSERTED_DATE,REMARKS)
                        values
                        ($pk,
                        '$offerUp',
                        '$value',
                        '$user_id',
                        sysdate,
                        '$remarks')");
                }
            }
            if ($facebook) {
                $check = $this->db->query("SELECT * from lz_barcode_portal_dt where barcode_no = '$value' and portal_id = '$facebook'")->row();
                if (count($check) == 0) {
                    $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lz_barcode_portal_dt', 'PORTAL_DT_ID')ID FROM DUAL")->result_array();
                    $pk = $query[0]['ID'];
                    $stat_query = $this->db->query("INSERT INTO
                    lz_barcode_portal_dt(PORTAL_DT_ID,PORTAL_ID,BARCODE_NO,INSERTED_BY,INSERTED_DATE,REMARKS)
                        values
                        ($pk,
                        '$facebook',
                        '$value',
                        '$user_id',
                        sysdate,
                        '$remarks')");
                }
            }
            if ($instagram) {
                $check = $this->db->query("SELECT * from lz_barcode_portal_dt where barcode_no = '$value' and portal_id = '$instagram'")->row();
                if (count($check) == 0) {
                    $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lz_barcode_portal_dt', 'PORTAL_DT_ID')ID FROM DUAL")->result_array();
                    $pk = $query[0]['ID'];
                    $stat_query = $this->db->query("INSERT INTO
                    lz_barcode_portal_dt(PORTAL_DT_ID,PORTAL_ID,BARCODE_NO,INSERTED_BY,INSERTED_DATE,REMARKS)
                        values
                        ($pk,
                        '$instagram',
                        '$value',
                        '$user_id',
                        sysdate,
                        '$remarks')");
                }
            }
        }
        return array('exist' => true, 'message' => "Update successfully !");

    }

    // adilModel/m_cearchOrder start

    public function get_search_order_record()
    {
        $parameter = trim($this->input->post('searchOrder'));
        $search_type = $this->input->post('search_type');
        $other_by = $this->input->post('other_by');
        $search_extended_order_id = $this->input->post('search_extended_order_id');
        $search_order_id = $this->input->post('search_order_id');
        $search_sale_order = $this->input->post('search_sale_order');
        $ebay_id = $this->input->post('ebay_id');
        $daysCheck = $this->input->post('daysCheck');
        $equalLike = $this->input->post('equalLike');
        if ($daysCheck == 'on') {
            $qryOlder90 = "";
        } else {
            $qryOlder90 = "AND DET.INSERTED_DATE >= sysdate-90 ";
        }
        $parm = '';
        $i = 0;
        if ($equalLike == 'LIKE') {
            if ($search_type == 'on') {
                $tempPara = (explode(" ", $parameter));
                foreach ($tempPara as $key => $value) {
                    if ($i === 0) {
                        $parm .= "(upper(DET.$other_by) LIKE upper('%$value%')";
                    } else {
                        $parm .= "AND upper(DET.$other_by) LIKE upper('%$value%')";
                    }
                    $i++;
                }
                if (count($tempPara) == 1) {
                    $preQuery = $qryOlder90 . "AND " . $parm . ")";
                } else {
                    $preQuery = $qryOlder90 . "AND " . $parm . ")";
                }

                $query = "SELECT
                DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                DET.ITEM_ID,
                DET.SALES_RECORD_NO_GROUP,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                  AND DET.ORDER_ID NOT IN
                      (SELECT ORDER_ID
                         FROM (SELECT D.ORDER_ID,
                                 COUNT(1) QTY_RETURN,
                                 MAX(SD.QUANTITY) QTY_SOLD
                                 FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                WHERE SD.ORDER_ID = D.ORDER_ID
                               GROUP BY D.ORDER_ID)
                        WHERE QTY_RETURN = QTY_SOLD)
                ";
                $query .= $preQuery . " ORDER BY DET.LZ_SALESLOAD_DET_ID)";
                $query = "SELECT * FROM(" . $query . " WHERE ROWNUM <=1000";
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Result Not Found!",
                    );
                }
            } else {
                if ($search_extended_order_id == 'on') {
                    $checkValid = $this->db->query("SELECT EXTENDEDORDERID FROM LZ_SALESLOAD_DET WHERE EXTENDEDORDERID LIKE '%$parameter%' ")->row();
                    if (!$checkValid) {
                        return array(
                            "found" => false,
                            "message" => "Record Not Found Against Extended Order Id!",
                        );
                    }
                    $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE EXTENDEDORDERID = '$parameter'")->row();
                    if ($checkReturn) {
                        return array(
                            "found" => false,
                            "message" => "Return created against this order. Can't be Canceled!",
                        );
                    }

                } elseif ($search_order_id == 'on') {
                    $checkValid = $this->db->query("SELECT TRACKING_NUMBER FROM LZ_SALESLOAD_DET WHERE TRACKING_NUMBER LIKE '%$parameter%' ")->row();
                    if (!$checkValid) {
                        return array(
                            "found" => false,
                            "message" => "Record Not Found Against Tracking Number!",
                        );
                    }
                    $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt bb WHERE bb.order_id =
                         (SELECT d.order_id
                            FROM lz_salesload_det d
                           WHERE d.tracking_number = '$parameter')")->row();
                    if ($checkReturn) {
                        return array(
                            "found" => false,
                            "message" => "Return created against this order. Can't be Canceled!",
                        );
                    }
                } elseif ($search_sale_order == 'on') {
                    $checkValid = $this->db->query("SELECT SALES_RECORD_NUMBER FROM LZ_SALESLOAD_DET WHERE SALES_RECORD_NUMBER LIKE '%$parameter%' ")->row();
                    if (!$checkValid) {
                        return array(
                            "found" => false,
                            "message" => "Record Not Found Against Sale Record Number!",
                        );
                    }
                    $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE SALE_RECORD_NO = '$parameter'")->row();
                    if ($checkReturn) {
                        return array(
                            "found" => false,
                            "message" => "Return created against this order. Can't be Canceled!",
                        );
                    }

                } elseif ($ebay_id == 'on') {
                    $checkValid = $this->db->query("SELECT ITEM_ID FROM LZ_SALESLOAD_DET WHERE ITEM_ID LIKE '%$parameter%' ")->row();
                    if (!$checkValid) {
                        return array(
                            "found" => false,
                            "message" => "Record Not Found Against Item Id!",
                        );
                    }
                    $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE EBAY_ITEM_ID = '$parameter'")->row();
                    if ($checkReturn) {
                        return array(
                            "found" => false,
                            "message" => "Return created against this order. Can't be Canceled!",
                        );
                    }
                }
                if ($search_extended_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.EXTENDEDORDERID LIKE '%$parameter%')";
                } elseif ($search_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.TRACKING_NUMBER LIKE '%$parameter%')";
                } elseif ($search_sale_order == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER LIKE '%$parameter%')";
                } elseif ($ebay_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.ITEM_ID LIKE '%$parameter%')";
                } else {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER LIKE '%$parameter%' OR DET.ORDER_ID LIKE '%$parameter%' OR
                    DET.EXTENDEDORDERID LIKE '%$parameter%')";
                }

                $query = "SELECT
                DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                DET.ITEM_ID,
                DET.SALES_RECORD_NO_GROUP,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                  AND DET.ORDER_ID NOT IN
                      (SELECT ORDER_ID
                         FROM (SELECT D.ORDER_ID,
                                 COUNT(1) QTY_RETURN,
                                 MAX(SD.QUANTITY) QTY_SOLD
                                 FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                WHERE SD.ORDER_ID = D.ORDER_ID
                               GROUP BY D.ORDER_ID)
                        WHERE QTY_RETURN = QTY_SOLD)
                AND ROWNUM <= 1000";
                $query .= $preQuery;
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    // group
                    $group_id = $query[0]['SALES_RECORD_NO_GROUP'];
                    if ($group_id) {
                        $query = "SELECT
                            DET.ITEM_TITLE,
                            DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                            DET.BUYER_ADDRESS1,
                            DET.ORDER_ID,
                            DET.EXTENDEDORDERID,
                            MT.LZ_SALELOAD_ID,
                            MT.LZ_SELLER_ACCT_ID,
                            DET.USER_ID,
                            DET.BUYER_FULLNAME,
                            DET.QUANTITY,
                            DET.SALE_PRICE,
                            DET.TOTAL_PRICE,
                            DET.SALE_DATE,
                            DET.TRACKING_NUMBER,
                            DET.BUYER_ZIP,
                            DET.ITEM_ID,
                            DET.SALES_RECORD_NO_GROUP,
                            (SELECT COUNT(1) QTY_RETURN
                            FROM LJ_CANCELLATION_DT D
                            WHERE D.ORDER_ID = DET.ORDER_ID
                            GROUP BY D.ORDER_ID) QTY_RETURN
                            FROM  LZ_SALESLOAD_DET DET,
                                LZ_SALESLOAD_MT  MT
                            WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                            AND DET.ORDER_ID NOT IN
                                (SELECT ORDER_ID
                                    FROM (SELECT D.ORDER_ID,
                                            COUNT(1) QTY_RETURN,
                                            MAX(SD.QUANTITY) QTY_SOLD
                                            FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                            WHERE SD.ORDER_ID = D.ORDER_ID
                                        GROUP BY D.ORDER_ID)
                                    WHERE QTY_RETURN = QTY_SOLD)
                            AND ROWNUM <= 1000";
                        $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NO_GROUP LIKE '%$group_id%')";
                        $query .= $preQuery;
                        $query = $this->db->query($query)->result_array();
                    }
                    // end group
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Record Not Found",
                    );
                }
            }
        } else {
            if ($search_type == 'on') {
                $tempPara = (explode(" ", $parameter));
                $parm .= "(upper(DET.$other_by) = upper('$parameter'))";
                $preQuery = $qryOlder90 . "AND " . $parm;
                $query = "SELECT
                DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                DET.ITEM_ID,
                DET.SALES_RECORD_NO_GROUP,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                  AND DET.ORDER_ID NOT IN
                      (SELECT ORDER_ID
                         FROM (SELECT D.ORDER_ID,
                                 COUNT(1) QTY_RETURN,
                                 MAX(SD.QUANTITY) QTY_SOLD
                                 FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                WHERE SD.ORDER_ID = D.ORDER_ID
                               GROUP BY D.ORDER_ID)
                        WHERE QTY_RETURN = QTY_SOLD)
                ";
                $query .= $preQuery . " ORDER BY DET.LZ_SALESLOAD_DET_ID)";
                $query = "SELECT * FROM(" . $query . " WHERE ROWNUM <=1000";
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Record Not Found!",
                    );
                }
            } else {
                if ($search_extended_order_id == 'on') {
                    $checkValid = $this->db->query("SELECT EXTENDEDORDERID FROM LZ_SALESLOAD_DET WHERE EXTENDEDORDERID = '$parameter' ")->row();
                    if (!$checkValid) {
                        return array(
                            "found" => false,
                            "message" => "Record Not Found Against Extended Order Id!",
                        );
                    }
                    $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE EXTENDEDORDERID = '$parameter'")->row();
                    if ($checkReturn) {
                        return array(
                            "found" => false,
                            "message" => "Return created against this order. Can't be Canceled!",
                        );
                    }
                } elseif ($search_order_id == 'on') {
                    $checkValid = $this->db->query("SELECT TRACKING_NUMBER FROM LZ_SALESLOAD_DET WHERE TRACKING_NUMBER = '$parameter' ")->row();
                    if (!$checkValid) {
                        return array(
                            "found" => false,
                            "message" => "Record Not Found Against Tracking Number!",
                        );
                    }
                    $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt bb WHERE bb.order_id =
                         (SELECT d.order_id
                            FROM lz_salesload_det d
                           WHERE d.tracking_number = '$parameter')")->row();
                    if ($checkReturn) {
                        return array(
                            "found" => false,
                            "message" => "Return created against this order. Can't be Canceled!",
                        );
                    }
                } elseif ($search_sale_order == 'on') {
                    $checkValid = $this->db->query("SELECT SALES_RECORD_NUMBER FROM LZ_SALESLOAD_DET WHERE SALES_RECORD_NUMBER = '$parameter' ")->row();
                    if (!$checkValid) {
                        return array(
                            "found" => false,
                            "message" => "Record Not Found Against Sale Record Number!",
                        );
                    }
                    $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE SALE_RECORD_NO = '$parameter'")->row();
                    if ($checkReturn) {
                        return array(
                            "found" => false,
                            "message" => "Return created against this order. Can't be Canceled!",
                        );
                    }

                } elseif ($ebay_id == 'on') {
                    $checkValid = $this->db->query("SELECT ITEM_ID FROM LZ_SALESLOAD_DET WHERE ITEM_ID = '$parameter' ")->row();
                    if (!$checkValid) {
                        return array(
                            "found" => false,
                            "message" => "Record Not Found Against Item Id!",
                        );
                    }
                    $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE EBAY_ITEM_ID = '$parameter'")->row();
                    if ($checkReturn) {
                        return array(
                            "found" => false,
                            "message" => "Return created against this order. Can't be Canceled!",
                        );
                    }
                }

                if ($search_extended_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.EXTENDEDORDERID = '$parameter')";
                } elseif ($search_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.TRACKING_NUMBER = '$parameter')";
                } elseif ($search_sale_order == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER = '$parameter')";
                } elseif ($ebay_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.ITEM_ID = '$parameter')";
                } else {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER = '$parameter' OR DET.ORDER_ID = '$parameter' OR
                    DET.EXTENDEDORDERID = '$parameter')";
                }

                $query = "SELECT
                DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                DET.ITEM_ID,
                DET.SALES_RECORD_NO_GROUP,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                  AND DET.ORDER_ID NOT IN
                      (SELECT ORDER_ID
                         FROM (SELECT D.ORDER_ID,
                                 COUNT(1) QTY_RETURN,
                                 MAX(SD.QUANTITY) QTY_SOLD
                                 FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                WHERE SD.ORDER_ID = D.ORDER_ID
                               GROUP BY D.ORDER_ID)
                        WHERE QTY_RETURN = QTY_SOLD)
                ";
                $query .= $preQuery;
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    $group_id = $query[0]['SALES_RECORD_NO_GROUP'];
                    if ($group_id) {
                        $query = "SELECT
                            DET.ITEM_TITLE,
                            DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                            DET.BUYER_ADDRESS1,
                            DET.ORDER_ID,
                            DET.EXTENDEDORDERID,
                            MT.LZ_SALELOAD_ID,
                            MT.LZ_SELLER_ACCT_ID,
                            DET.USER_ID,
                            DET.BUYER_FULLNAME,
                            DET.QUANTITY,
                            DET.SALE_PRICE,
                            DET.TOTAL_PRICE,
                            DET.SALE_DATE,
                            DET.TRACKING_NUMBER,
                            DET.BUYER_ZIP,
                            DET.ITEM_ID,
                            DET.SALES_RECORD_NO_GROUP,
                            (SELECT COUNT(1) QTY_RETURN
                            FROM LJ_CANCELLATION_DT D
                            WHERE D.ORDER_ID = DET.ORDER_ID
                            GROUP BY D.ORDER_ID) QTY_RETURN
                            FROM  LZ_SALESLOAD_DET DET,
                                LZ_SALESLOAD_MT  MT
                            WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                            AND DET.ORDER_ID NOT IN
                                (SELECT ORDER_ID
                                    FROM (SELECT D.ORDER_ID,
                                            COUNT(1) QTY_RETURN,
                                            MAX(SD.QUANTITY) QTY_SOLD
                                            FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                            WHERE SD.ORDER_ID = D.ORDER_ID
                                        GROUP BY D.ORDER_ID)
                                    WHERE QTY_RETURN = QTY_SOLD)
                            AND ROWNUM <= 1000";
                        $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NO_GROUP LIKE '%$group_id%')";
                        $query .= $preQuery;
                        $query = $this->db->query($query)->result_array();
                    }
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Record Not Found!",
                    );
                }
            }
        }
    }
    public function get_search_order_record_account_id()
    {
        $parameter = trim($this->input->post('searchOrder'));
        $search_type = $this->input->post('search_type');
        $other_by = $this->input->post('other_by');
        $search_extended_order_id = $this->input->post('search_extended_order_id');
        $search_order_id = $this->input->post('search_order_id');
        $search_sale_order = $this->input->post('search_sale_order');
        $ebay_id = $this->input->post('ebay_id');
        $daysCheck = $this->input->post('daysCheck');
        $equalLike = $this->input->post('equalLike');
        if ($daysCheck == 'on') {
            $qryOlder90 = "";
        } else {
            $qryOlder90 = "AND DET.INSERTED_DATE >= sysdate-90 ";
        }
        $parm = '';
        $i = 0;
        if ($equalLike == 'LIKE') {
            if ($search_type == 'on') {
                $tempPara = (explode(" ", $parameter));
                foreach ($tempPara as $key => $value) {
                    if ($i === 0) {
                        $parm .= "(upper(DET.$other_by) LIKE upper('%$value%')";
                    } else {
                        $parm .= "AND upper(DET.$other_by) LIKE upper('%$value%')";
                    }
                    $i++;
                }
                if (count($tempPara) == 1) {
                    $preQuery = $qryOlder90 . "AND " . $parm . ")";
                } else {
                    $preQuery = $qryOlder90 . "AND " . $parm . ")";
                }

                $query = "SELECT
                mt.lz_seller_acct_id,
                a.sell_acct_desc,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT ,lz_seller_accts a
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                AND mt.lz_seller_acct_id = a.lz_seller_acct_id
                  AND DET.ORDER_ID NOT IN
                      (SELECT ORDER_ID
                         FROM (SELECT D.ORDER_ID,
                                 COUNT(1) QTY_RETURN,
                                 MAX(SD.QUANTITY) QTY_SOLD
                                 FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                WHERE SD.ORDER_ID = D.ORDER_ID
                               GROUP BY D.ORDER_ID)
                        WHERE QTY_RETURN = QTY_SOLD)
                ";
                $query .= $preQuery . " ORDER BY DET.LZ_SALESLOAD_DET_ID)";
                $query = "SELECT * FROM(" . $query . " WHERE ROWNUM <=1";
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Order already canceled!",
                    );
                }
            } else {

                if ($search_extended_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.EXTENDEDORDERID LIKE '%$parameter%')";
                } elseif ($search_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.TRACKING_NUMBER LIKE '%$parameter%')";
                } elseif ($search_sale_order == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER LIKE '%$parameter%')";
                } elseif ($ebay_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.ITEM_ID LIKE '%$parameter%')";
                } else {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER LIKE '%$parameter%' OR DET.ORDER_ID LIKE '%$parameter%' OR
                    DET.EXTENDEDORDERID LIKE '%$parameter%')";
                }

                $query = "SELECT
                mt.lz_seller_acct_id,
                a.sell_acct_desc,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT ,lz_seller_accts a
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                AND mt.lz_seller_acct_id = a.lz_seller_acct_id
                  AND DET.ORDER_ID NOT IN
                      (SELECT ORDER_ID
                         FROM (SELECT D.ORDER_ID,
                                 COUNT(1) QTY_RETURN,
                                 MAX(SD.QUANTITY) QTY_SOLD
                                 FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                WHERE SD.ORDER_ID = D.ORDER_ID
                               GROUP BY D.ORDER_ID)
                        WHERE QTY_RETURN = QTY_SOLD)
                AND ROWNUM <= 1";
                $query .= $preQuery;
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    // group
                    // $group_id = $query[0]['SALES_RECORD_NO_GROUP'];
                    if ($query) {
                        $query = "SELECT
                            mt.lz_seller_acct_id,
                a.sell_acct_desc,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT ,lz_seller_accts a
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                AND mt.lz_seller_acct_id = a.lz_seller_acct_id
                  AND DET.ORDER_ID NOT IN
                                (SELECT ORDER_ID
                                    FROM (SELECT D.ORDER_ID,
                                            COUNT(1) QTY_RETURN,
                                            MAX(SD.QUANTITY) QTY_SOLD
                                            FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                            WHERE SD.ORDER_ID = D.ORDER_ID
                                        GROUP BY D.ORDER_ID)
                                    WHERE QTY_RETURN = QTY_SOLD)
                            AND ROWNUM <= 1";
                        $preQuery = $qryOlder90;
                        $query .= $preQuery;
                        $query = $this->db->query($query)->result_array();
                    }
                    // end group
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Order already canceled!",
                    );
                }
            }
        } else {
            if ($search_type == 'on') {
                $tempPara = (explode(" ", $parameter));
                $parm .= "(upper(DET.$other_by) = upper('$parameter'))";
                $preQuery = $qryOlder90 . "AND " . $parm;
                $query = "SELECT
                mt.lz_seller_acct_id,
                a.sell_acct_desc,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT ,lz_seller_accts a
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                AND mt.lz_seller_acct_id = a.lz_seller_acct_id
                  AND DET.ORDER_ID NOT IN
                      (SELECT ORDER_ID
                         FROM (SELECT D.ORDER_ID,
                                 COUNT(1) QTY_RETURN,
                                 MAX(SD.QUANTITY) QTY_SOLD
                                 FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                WHERE SD.ORDER_ID = D.ORDER_ID
                               GROUP BY D.ORDER_ID)
                        WHERE QTY_RETURN = QTY_SOLD)
                ";
                $query .= $preQuery . " ORDER BY DET.LZ_SALESLOAD_DET_ID)";
                $query = "SELECT * FROM(" . $query . " WHERE ROWNUM <=1";
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Order already canceled!",
                    );
                }
            } else {

                if ($search_extended_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.EXTENDEDORDERID = '$parameter')";
                } elseif ($search_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.TRACKING_NUMBER = '$parameter')";
                } elseif ($search_sale_order == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER = '$parameter')";
                } elseif ($ebay_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.ITEM_ID = '$parameter')";
                } else {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER = '$parameter' OR DET.ORDER_ID = '$parameter' OR
                    DET.EXTENDEDORDERID = '$parameter')";
                }

                $query = "SELECT
                mt.lz_seller_acct_id,
                a.sell_acct_desc,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT ,lz_seller_accts a
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                AND mt.lz_seller_acct_id = a.lz_seller_acct_id
                  AND DET.ORDER_ID NOT IN
                      (SELECT ORDER_ID
                         FROM (SELECT D.ORDER_ID,
                                 COUNT(1) QTY_RETURN,
                                 MAX(SD.QUANTITY) QTY_SOLD
                                 FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                WHERE SD.ORDER_ID = D.ORDER_ID
                               GROUP BY D.ORDER_ID)
                        WHERE QTY_RETURN = QTY_SOLD)
                ";
                $query .= $preQuery;
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    // $group_id = $query[0]['SALES_RECORD_NO_GROUP'];
                    if ($query) {
                        $query = "SELECT
                            mt.lz_seller_acct_id,
                a.sell_acct_desc,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT ,lz_seller_accts a
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
                AND mt.lz_seller_acct_id = a.lz_seller_acct_id
                  AND DET.ORDER_ID NOT IN
                                (SELECT ORDER_ID
                                    FROM (SELECT D.ORDER_ID,
                                            COUNT(1) QTY_RETURN,
                                            MAX(SD.QUANTITY) QTY_SOLD
                                            FROM LJ_CANCELLATION_DT D, LZ_SALESLOAD_DET SD
                                            WHERE SD.ORDER_ID = D.ORDER_ID
                                        GROUP BY D.ORDER_ID)
                                    WHERE QTY_RETURN = QTY_SOLD)
                            AND ROWNUM <= 1";
                        $preQuery = $qryOlder90;
                        $query .= $preQuery;
                        $query = $this->db->query($query)->result_array();
                    }
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Order already canceled!",
                    );
                }
            }
        }
    }
    public function processCreateCancelation($order_id, $remarks, $cancel_by, $bin_id, $barcode_no)
    {
        $remarks = ucfirst($remarks);
        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = trim(str_replace(array("'"), "''", $remarks));

        $check_bin_id = $this->db->query("SELECT BIN_ID FROM BIN_MT BB WHERE UPPER(BB.BIN_TYPE) ||'-'||BB.BIN_NO = UPPER('$bin_id')")->row();
        $cancel_status = $this->input->post("barcode_status");
        $cancel_id = "";
        if ($check_bin_id) {
            $bin_id = $check_bin_id->BIN_ID;
        } else {
            $bin_id = 0;
        }
        $group_record_number = $this->db->query("SELECT SALES_RECORD_NO_GROUP FROM LZ_SALESLOAD_DET WHERE ORDER_ID ='$order_id'")->row();
        $group_record_number = $group_record_number->SALES_RECORD_NO_GROUP;
        if ($group_record_number) {
            $order_ids = $this->db->query("SELECT ORDER_ID FROM LZ_SALESLOAD_DET WHERE SALES_RECORD_NO_GROUP = '$group_record_number'")->result_array();
            foreach ($order_ids as $key => $value) {
                $order_id_temp = $value['ORDER_ID'];
                //$get_cancel_id = $this->db->query("s")

                $getData = $this->db->query("SELECT DT.ORDER_ID,DT.SALES_RECORD_NUMBER,DT.EXTENDEDORDERID FROM LZ_SALESLOAD_DET DT WHERE DT.ORDER_ID =  '$order_id_temp'")->result_array();
                $sale_record_no = $getData[0]['SALES_RECORD_NUMBER'];
                $extended_order_id = $getData[0]['EXTENDEDORDERID'];
                $get_order_idd = $getData[0]['ORDER_ID'];

                $updateData = $this->db->query("UPDATE LZ_SALESLOAD_DET SET CANCEL_ID = (select mm.cancellation_id from LJ_CANCELLATION_mt mm where mm.legacy_order_id = '$extended_order_id' or  mm.legacy_order_id = '$get_order_idd' ) , RELEASE_DATE = sysdate, RELEASE_BY = '$cancel_by' WHERE ORDER_ID = '$order_id_temp'");

                // $barcode_no        = $this->db->query("SELECT B.BARCODE_NO FROM LZ_BARCODE_MT B
                //         WHERE B.ORDER_ID = '$order_id_temp'")->result_array();
                $barcode_no = explode(",", $barcode_no);
                foreach ($barcode_no as $barcode) {
                    $barcode = $barcode;
                    $this->db->query("INSERT INTO LJ_CANCELLATION_DT CD (CD.CANCELLATION_DT_ID,
                                                            CD.BARCODE_NO,
                                                            CD.sale_order_id,
                                                            CD.extendedorderid,
                                                            CD.ORDER_ID)
                                                            values (
                                                                GET_SINGLE_PRIMARY_KEY('LJ_CANCELLATION_DT','CANCELLATION_DT_ID'),
                                                                '$barcode',
                                                                '$sale_record_no',
                                                                '$extended_order_id',
                                                                '$order_id') ");
                    if ($cancel_status == 'release') {
                        if ($check_bin_id) {
                            $qyer = $this->db->query("UPDATE lz_barcode_mt b
                                        SET b.ebay_item_id    = '',
                                            b.sale_record_no  = '',
                                            b.list_id         = '',
                                            b.pulling_id      = '',
                                            b.bin_id          = '$bin_id',
                                            b.order_id        = '',
                                            b.packing_id      = '',
                                            b.packing_by      = '',
                                            b.packing_date    = '',
                                            b.ebay_sticker    = 0,
                                            b.audit_datetime  = '',
                                            b.audit_by        = '',
                                            b.shopify_list_id = '',
                                            b.extendedorderid = ''
                                    WHERE b.barcode_no = '$barcode'");
                        } else {
                            $qyer = $this->db->query("UPDATE lz_barcode_mt b
                                        SET b.ebay_item_id    = '',
                                            b.sale_record_no  = '',
                                            b.list_id         = '',
                                            b.pulling_id      = '',
                                            b.order_id        = '',
                                            b.packing_id      = '',
                                            b.packing_by      = '',
                                            b.packing_date    = '',
                                            b.ebay_sticker    = 0,
                                            b.audit_datetime  = '',
                                            b.audit_by        = '',
                                            b.shopify_list_id = '',
                                            b.extendedorderid = ''
                                    WHERE b.barcode_no = '$barcode'");
                        }
                    } else {
                        $qyer = $this->db->query("UPDATE lz_barcode_mt b
                                    SET b.condition_id  = -1,
                                        DISCARD_DATE    = sysdate,
                                        DISCARD_BY      = '$cancel_by',
                                        DISCARD         = 1,
                                        DISCARD_REMARKS = '$remarks'
                                WHERE b.barcode_no = '$barcode'");
                    }

                }
            }
        } else {
            // $updateData        = $this->db->query("UPDATE LZ_SALESLOAD_DET SET CANCEL_ID = (select mm.cancellation_id from LJ_CANCELLATION_mt mm where mm.legacy_order_id = (select pd.extendedorderid from lz_salesload_det pd where pd.order_id = '$order_id_temp' ) ),RELEASE_DATE = sysdate, RELEASE_BY = '$cancel_by' WHERE ORDER_ID = '$order_id'");
            $getData = $this->db->query("SELECT DT.ORDER_ID,DT.SALES_RECORD_NUMBER,DT.EXTENDEDORDERID FROM LZ_SALESLOAD_DET DT WHERE DT.ORDER_ID =  '$order_id' ")->result_array();
            $sale_record_no = $getData[0]['SALES_RECORD_NUMBER'];
            $extended_order_id = $getData[0]['EXTENDEDORDERID'];
            $get_order_idd = $getData[0]['ORDER_ID'];

            $updateData = $this->db->query("UPDATE LZ_SALESLOAD_DET SET CANCEL_ID = (select mm.cancellation_id from LJ_CANCELLATION_mt mm where mm.legacy_order_id = '$extended_order_id' or  mm.legacy_order_id = '$get_order_idd' ),RELEASE_DATE = sysdate, RELEASE_BY = '$cancel_by' WHERE ORDER_ID = '$order_id'");
            // $barcode_no        = $this->db->query("SELECT B.BARCODE_NO FROM LZ_BARCODE_MT B
            //             WHERE B.ORDER_ID = '$order_id'")->result_array();
            $barcode_no = explode(",", $barcode_no);
            foreach ($barcode_no as $barcode) {
                $barcode = $barcode;
                $this->db->query("INSERT INTO LJ_CANCELLATION_DT CD (CD.CANCELLATION_DT_ID,
                                                                CD.BARCODE_NO,
                                                                CD.sale_order_id,
                                                                CD.extendedorderid,
                                                                CD.ORDER_ID)
                                                                values (
                                                                    GET_SINGLE_PRIMARY_KEY('LJ_CANCELLATION_DT','CANCELLATION_DT_ID'),
                                                                    '$barcode',
                                                                    '$sale_record_no',
                                                                    '$extended_order_id',
                                                                    '$order_id') ");
                if ($cancel_status == 'release') {
                    if ($check_bin_id) {
                        $qyer = $this->db->query("UPDATE lz_barcode_mt b
                                        SET b.ebay_item_id    = '',
                                            b.sale_record_no  = '',
                                            b.list_id         = '',
                                            b.pulling_id      = '',
                                            b.bin_id          = '$bin_id',
                                            b.order_id        = '',
                                            b.packing_id      = '',
                                            b.packing_by      = '',
                                            b.packing_date    = '',
                                            b.ebay_sticker    = 0,
                                            b.audit_datetime  = '',
                                            b.audit_by        = '',
                                            b.shopify_list_id = '',
                                            b.extendedorderid = ''
                                    WHERE b.barcode_no = '$barcode'");
                    } else {
                        $qyer = $this->db->query("UPDATE lz_barcode_mt b
                                        SET b.ebay_item_id    = '',
                                            b.sale_record_no  = '',
                                            b.list_id         = '',
                                            b.pulling_id      = '',
                                            b.order_id        = '',
                                            b.packing_id      = '',
                                            b.packing_by      = '',
                                            b.packing_date    = '',
                                            b.ebay_sticker    = 0,
                                            b.audit_datetime  = '',
                                            b.audit_by        = '',
                                            b.shopify_list_id = '',
                                            b.extendedorderid = ''
                                    WHERE b.barcode_no = '$barcode'");
                    }
                } else {
                    $qyer = $this->db->query("UPDATE lz_barcode_mt b
                                        SET b.condition_id  = -1,
                                            DISCARD_DATE    = sysdate,
                                            DISCARD_BY      = '$cancel_by',
                                            DISCARD         = 1,
                                            DISCARD_REMARKS = '$remarks'
                                    WHERE b.barcode_no = '$barcode'");
                }

            }
        }

        if ($qyer) {
            return $cancel_status;
        } else {
            return false;
        }
    }
    public function printBarcode()
    {
        $order_id = $this->uri->segment(4);
        return $this->db->query("SELECT B.BARCODE_NO, TO_CHAR(DET.RELEASE_DATE, 'DD-MON-YYYY') RELEASE_DATE FROM LJ_CANCELLATION_DT B, LZ_SALESLOAD_DET DET WHERE B.ORDER_ID = DET.ORDER_ID AND B.ORDER_ID = '$order_id' ")->result_array();
    }
    public function get_bar_codes($order_id)
    {
        $query = $this->db->query("SELECT B.BARCODE_NO,
        B.PULLING_ID,
        B.BIN_ID,
        BB.BIN_TYPE || '-' || BB.BIN_NO BIN_NAME,
        (
        select count(*) from
        (SELECT COUNT(1) CNT
        FROM LZ_BARCODE_MT B
        WHERE B.ORDER_ID = '$order_id'
        GROUP BY B.BIN_ID)
        ) CNT
        FROM LZ_BARCODE_MT B, BIN_MT BB
        WHERE BB.BIN_ID = B.BIN_ID
        AND B.ORDER_ID = '$order_id'")->result_array();
        return $query;
    }
    public function get_cancelled_barcode()
    {
        $order_id = $this->input->post('order_id');
        $query = $this->db->query("SELECT * FROM lj_cancellation_dt WHERE order_id = '$order_id'")->result_array();
        return $query;
    }
    public function get_cancelled_barcode_records()
    {
        $query = $this->db->query("SELECT DET.ITEM_TITLE,
        DET.SALE_RECORD_NO,
        DET.BUYER_ADDRESS1,
        DET.ORDER_ID,
        DET.EXTENDEDORDERID,
        DET.LZ_SALELOAD_ID,
        DET.LZ_SELLER_ACCT_ID,
        DET.USER_ID,
        DET.BUYER_FULLNAME,
        DET.QUANTITY,
        DET.SALE_PRICE,
        DET.TOTAL_PRICE,
        DET.SALE_DATE,
        DET.TRACKING_NUMBER,
        DET.BUYER_ZIP,
        DET.BARCODES,
        DET.SELL_ACCT_DESC
        FROM (SELECT DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                TO_CHAR((SELECT REPLACE(REPLACE(XMLAGG(XMLELEMENT(A, CDT.BARCODE_NO) ORDER BY CDT.BARCODE_NO DESC NULLS LAST)
                                               .GETCLOBVAL(),
                                               '<A>',
                                               ''),
                                       '</A>',
                                       ', ') AS BARCODE_NO
                          FROM LJ_CANCELLATION_DT CDT
                         WHERE CDT.ORDER_ID = DET.ORDER_ID)) BARCODES,
                AA.SELL_ACCT_DESC
            FROM LZ_SALESLOAD_DET   DET,
                LZ_SALESLOAD_MT    MT,
                LJ_CANCELLATION_DT DT,
                LZ_SELLER_ACCTS    AA
            WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID
            AND DT.ORDER_ID = DET.ORDER_ID
            AND MT.LZ_SELLER_ACCT_ID = AA.LZ_SELLER_ACCT_ID) DET
            GROUP BY DET.ITEM_TITLE,
                DET.SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                DET.LZ_SALELOAD_ID,
                DET.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                DET.BARCODES,
                DET.SELL_ACCT_DESC")->result_array();
        return $query;
    }
    public function get_order_ids_by_group_record($group_record_number)
    {
        $query = $this->db->query("SELECT ORDER_ID FROM LZ_SALESLOAD_DET WHERE SALES_RECORD_NO_GROUP = '$group_record_number'")->result_array();
        if ($query) {
            return $query;
        } else {
            return false;
        }
    }
    public function get_search_order_record_refund()
    {
        $parameter = trim($this->input->post('searchOrder'));
        $search_type = $this->input->post('search_type');
        $other_by = $this->input->post('other_by');
        $search_extended_order_id = $this->input->post('search_extended_order_id');
        $search_order_id = $this->input->post('search_order_id');
        $search_sale_order = $this->input->post('search_sale_order');
        $ebay_id = $this->input->post('ebay_id');
        $daysCheck = $this->input->post('daysCheck');
        $equalLike = $this->input->post('equalLike');
        if ($daysCheck == 'on') {
            $qryOlder90 = "";
        } else {
            $qryOlder90 = "AND DET.INSERTED_DATE >= sysdate-90 ";
        }
        $parm = '';
        $i = 0;
        if ($equalLike == 'LIKE') {
            if ($search_type == 'on') {
                $tempPara = (explode(" ", $parameter));
                foreach ($tempPara as $key => $value) {
                    if ($i === 0) {
                        $parm .= "(upper(DET.$other_by) LIKE upper('%$value%')";
                    } else {
                        $parm .= "AND upper(DET.$other_by) LIKE upper('%$value%')";
                    }
                    $i++;
                }
                if (count($tempPara) == 1) {
                    $preQuery = $qryOlder90 . "AND " . $parm . ")";
                } else {
                    $preQuery = $qryOlder90 . "AND " . $parm . ")";
                }

                $query = "SELECT
                DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                DET.ITEM_ID,
                DET.SALES_RECORD_NO_GROUP,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID

                ";
                $query .= $preQuery . " ORDER BY DET.LZ_SALESLOAD_DET_ID)";
                $query = "SELECT * FROM(" . $query . " WHERE ROWNUM <=1000";
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Order not found or older then 90 days!",
                    );
                }
            } else {
                if ($search_extended_order_id == 'on') {
                    $checkValid = $this->db->query("SELECT EXTENDEDORDERID FROM LZ_SALESLOAD_DET WHERE EXTENDEDORDERID LIKE '%$parameter%' ")->row();
                    // if (!$checkValid) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Record Not Found Against Extended Order Id!"
                    //     );
                    // }
                    // $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE EXTENDEDORDERID = '$parameter'")->row();
                    // if ($checkReturn) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Return created against this order. Can't be Canceled!"
                    //     );
                    // }

                } elseif ($search_order_id == 'on') {
                    // $checkValid = $this->db->query("SELECT TRACKING_NUMBER FROM LZ_SALESLOAD_DET WHERE TRACKING_NUMBER LIKE '%$parameter%' ")->row();
                    // if (!$checkValid) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Record Not Found Against Tracking Number!"
                    //     );
                    // }
                    // $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt bb WHERE bb.order_id =
                    //      (SELECT d.order_id
                    //         FROM lz_salesload_det d
                    //        WHERE d.tracking_number = '$parameter')")->row();
                    // if ($checkReturn) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Return created against this order. Can't be Canceled!"
                    //     );
                    // }
                } elseif ($search_sale_order == 'on') {
                    // $checkValid = $this->db->query("SELECT SALES_RECORD_NUMBER FROM LZ_SALESLOAD_DET WHERE SALES_RECORD_NUMBER LIKE '%$parameter%' ")->row();
                    // if (!$checkValid) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Record Not Found Against Sale Record Number!"
                    //     );
                    // }
                    // $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE SALE_RECORD_NO = '$parameter'")->row();
                    // if ($checkReturn) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Return created against this order. Can't be Canceled!"
                    //     );
                    // }

                } elseif ($ebay_id == 'on') {
                    // $checkValid = $this->db->query("SELECT ITEM_ID FROM LZ_SALESLOAD_DET WHERE ITEM_ID LIKE '%$parameter%' ")->row();
                    // if (!$checkValid) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Record Not Found Against Item Id!"
                    //     );
                    // }
                    // $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE EBAY_ITEM_ID = '$parameter'")->row();
                    // if ($checkReturn) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Return created against this order. Can't be Canceled!"
                    //     );
                    // }
                }
                if ($search_extended_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.EXTENDEDORDERID LIKE '%$parameter%')";
                } elseif ($search_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.TRACKING_NUMBER LIKE '%$parameter%')";
                } elseif ($search_sale_order == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER LIKE '%$parameter%')";
                } elseif ($ebay_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.ITEM_ID LIKE '%$parameter%')";
                } else {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER LIKE '%$parameter%' OR DET.ORDER_ID LIKE '%$parameter%' OR
                    DET.EXTENDEDORDERID LIKE '%$parameter%')";
                }

                $query = "SELECT
                DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                DET.ITEM_ID,
                DET.SALES_RECORD_NO_GROUP,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID

                AND ROWNUM <= 1000";
                $query .= $preQuery;
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    // group
                    $group_id = $query[0]['SALES_RECORD_NO_GROUP'];
                    if ($group_id) {
                        $query = "SELECT
                            DET.ITEM_TITLE,
                            DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                            DET.BUYER_ADDRESS1,
                            DET.ORDER_ID,
                            DET.EXTENDEDORDERID,
                            MT.LZ_SALELOAD_ID,
                            MT.LZ_SELLER_ACCT_ID,
                            DET.USER_ID,
                            DET.BUYER_FULLNAME,
                            DET.QUANTITY,
                            DET.SALE_PRICE,
                            DET.TOTAL_PRICE,
                            DET.SALE_DATE,
                            DET.TRACKING_NUMBER,
                            DET.BUYER_ZIP,
                            DET.ITEM_ID,
                            DET.SALES_RECORD_NO_GROUP,
                            (SELECT COUNT(1) QTY_RETURN
                            FROM LJ_CANCELLATION_DT D
                            WHERE D.ORDER_ID = DET.ORDER_ID
                            GROUP BY D.ORDER_ID) QTY_RETURN
                            FROM  LZ_SALESLOAD_DET DET,
                                LZ_SALESLOAD_MT  MT
                            WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID

                            AND ROWNUM <= 1000";
                        $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NO_GROUP LIKE '%$group_id%')";
                        $query .= $preQuery;
                        $query = $this->db->query($query)->result_array();
                    }
                    // end group
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Order not found or older then 90 days!",
                    );
                }
            }
        } else {
            if ($search_type == 'on') {
                $tempPara = (explode(" ", $parameter));
                $parm .= "(upper(DET.$other_by) = upper('$parameter'))";
                $preQuery = $qryOlder90 . "AND " . $parm;
                $query = "SELECT
                DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                DET.ITEM_ID,
                DET.SALES_RECORD_NO_GROUP,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID

                ";
                $query .= $preQuery . " ORDER BY DET.LZ_SALESLOAD_DET_ID)";
                $query = "SELECT * FROM(" . $query . " WHERE ROWNUM <=1000";
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Order not found or older then 90 days!",
                    );
                }
            } else {
                if ($search_extended_order_id == 'on') {
                    $checkValid = $this->db->query("SELECT EXTENDEDORDERID FROM LZ_SALESLOAD_DET WHERE EXTENDEDORDERID = '$parameter' ")->row();
                    // if (!$checkValid) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Record Not Found Against Extended Order Id!"
                    //     );
                    // }
                    // $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE EXTENDEDORDERID = '$parameter'")->row();
                    // if ($checkReturn) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Return created against this order. Can't be Canceled!"
                    //     );
                    // }
                } elseif ($search_order_id == 'on') {
                    // $checkValid = $this->db->query("SELECT TRACKING_NUMBER FROM LZ_SALESLOAD_DET WHERE TRACKING_NUMBER = '$parameter' ")->row();
                    // if (!$checkValid) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Record Not Found Against Tracking Number!"
                    //     );
                    // }
                    // $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt bb WHERE bb.order_id =
                    //      (SELECT d.order_id
                    //         FROM lz_salesload_det d
                    //        WHERE d.tracking_number = '$parameter')")->row();
                    // if ($checkReturn) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Return created against this order. Can't be Canceled!"
                    //     );
                    // }
                } elseif ($search_sale_order == 'on') {
                    // $checkValid = $this->db->query("SELECT SALES_RECORD_NUMBER FROM LZ_SALESLOAD_DET WHERE SALES_RECORD_NUMBER = '$parameter' ")->row();
                    // if (!$checkValid) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Record Not Found Against Sale Record Number!"
                    //     );
                    // }
                    // $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE SALE_RECORD_NO = '$parameter'")->row();
                    // if ($checkReturn) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Return created against this order. Can't be Canceled!"
                    //     );
                    // }

                } elseif ($ebay_id == 'on') {
                    // $checkValid = $this->db->query("SELECT ITEM_ID FROM LZ_SALESLOAD_DET WHERE ITEM_ID = '$parameter' ")->row();
                    // if (!$checkValid) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Record Not Found Against Item Id!"
                    //     );
                    // }
                    // $checkReturn = $this->db->query("SELECT * FROM lj_returned_barcode_mt WHERE EBAY_ITEM_ID = '$parameter'")->row();
                    // if ($checkReturn) {
                    //     return array(
                    //         "found" => false,
                    //         "message" => "Return created against this order. Can't be Canceled!"
                    //     );
                    // }
                }

                if ($search_extended_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.EXTENDEDORDERID = '$parameter')";
                } elseif ($search_order_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.TRACKING_NUMBER = '$parameter')";
                } elseif ($search_sale_order == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER = '$parameter')";
                } elseif ($ebay_id == 'on') {
                    $preQuery = $qryOlder90 . "AND (DET.ITEM_ID = '$parameter')";
                } else {
                    $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NUMBER = '$parameter' OR DET.ORDER_ID = '$parameter' OR
                    DET.EXTENDEDORDERID = '$parameter')";
                }

                $query = "SELECT
                DET.ITEM_TITLE,
                DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                DET.BUYER_ADDRESS1,
                DET.ORDER_ID,
                DET.EXTENDEDORDERID,
                MT.LZ_SALELOAD_ID,
                MT.LZ_SELLER_ACCT_ID,
                DET.USER_ID,
                DET.BUYER_FULLNAME,
                DET.QUANTITY,
                DET.SALE_PRICE,
                DET.TOTAL_PRICE,
                DET.SALE_DATE,
                DET.TRACKING_NUMBER,
                DET.BUYER_ZIP,
                DET.ITEM_ID,
                DET.SALES_RECORD_NO_GROUP,
                (SELECT COUNT(1) QTY_RETURN
                   FROM LJ_CANCELLATION_DT D
                  WHERE D.ORDER_ID = DET.ORDER_ID
                  GROUP BY D.ORDER_ID) QTY_RETURN
                 FROM  LZ_SALESLOAD_DET DET,
                      LZ_SALESLOAD_MT  MT
                WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID

                ";
                $query .= $preQuery;
                $query = $this->db->query($query)->result_array();
                if ($query) {
                    $group_id = $query[0]['SALES_RECORD_NO_GROUP'];
                    if ($group_id) {
                        $query = "SELECT
                            DET.ITEM_TITLE,
                            DET.SALES_RECORD_NUMBER SALE_RECORD_NO,
                            DET.BUYER_ADDRESS1,
                            DET.ORDER_ID,
                            DET.EXTENDEDORDERID,
                            MT.LZ_SALELOAD_ID,
                            MT.LZ_SELLER_ACCT_ID,
                            DET.USER_ID,
                            DET.BUYER_FULLNAME,
                            DET.QUANTITY,
                            DET.SALE_PRICE,
                            DET.TOTAL_PRICE,
                            DET.SALE_DATE,
                            DET.TRACKING_NUMBER,
                            DET.BUYER_ZIP,
                            DET.ITEM_ID,
                            DET.SALES_RECORD_NO_GROUP,
                            (SELECT COUNT(1) QTY_RETURN
                            FROM LJ_CANCELLATION_DT D
                            WHERE D.ORDER_ID = DET.ORDER_ID
                            GROUP BY D.ORDER_ID) QTY_RETURN
                            FROM  LZ_SALESLOAD_DET DET,
                                LZ_SALESLOAD_MT  MT
                            WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID

                            AND ROWNUM <= 1000";
                        $preQuery = $qryOlder90 . "AND (DET.SALES_RECORD_NO_GROUP LIKE '%$group_id%')";
                        $query .= $preQuery;
                        $query = $this->db->query($query)->result_array();
                    }
                    return array(
                        "found" => true,
                        "searchOrderResult" => $query,
                    );
                } else {
                    return array(
                        "found" => false,
                        "message" => "Order not found or older then 90 days!",
                    );
                }
            }
        }
    }

    // adilModel/m_cearchOrder end

}
