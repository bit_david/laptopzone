<?php 

  class m_sync_oracle extends CI_Model
  {

  public function __construct()
  {
      parent::__construct();
      $this->load->database();
    }
public function get_customer()
{
  // $userRole = $_POST['userRole'];
  // if($userRole == '1')
  // {
  $main_query = "Select cmt.customer_id,cmt.customer_code,cmt.company_name from customer_mt cmt";
  // }
  // else
  // {
  //   $sales_person_id = $_POST['sales_person_id'];
  //   $main_query = "Select cmt.customer_id,cmt.customer_code,cmt.company_name from customer_mt cmt where cmt.sales_person_id = ".$sales_person_id;
  // }
  $main_query = $this->db->query($main_query)->result_array();
  return array($main_query);
}

   public function insertData()
  {
  # code...
  
    $mt_ids_array = array();
    // $json_order_data = $_POST['order_data_array'];
    $json_order_data='{"mt_data":"[{\"Barcode\":\"036000291452\",\"Condition\":\"\",\"FolderName\":\"036000291452\",\"Lot_id\":\"0\",\"MPN\":\"\",\"UPC\":\"\",\"pic_DateTime\":\"07\/09\/2018 10:05:31 pm\",\"pic_taker_id\":\"47\",\"sync_status\":\"0\"}]"}';
  //  $json_order_data =  '{"mt_data":"[{\"Barcode\":\"123456\",\"Condition\":\"\",\"FolderName\":\"123456\",\"Lot_id\":\"0\",\"pic_DateTime\":\"31\/08\/2018 06:10:35 pm\",\"pic_taker_id\":\"9\",\"sync_status\":\"0\"},{\"Barcode\":\"1234567890128\",\"Condition\":\"\",\"FolderName\":\"1234567890128\",\"Lot_id\":\"0\",\"MPN\":\"\",\"UPC\":\"\",\"pic_DateTime\":\"31\/08\/2018 06:11:36 pm\",\"pic_taker_id\":\"9\",\"sync_status\":\"0\"}]"}';
    
    $order_data=json_decode($json_order_data);
    
    $order_mt_array=json_decode($order_data->mt_data);
    for($i=0;$i<count($order_mt_array);$i++)
    {
      

        $mysqlstmnt="INSERT INTO lz_special_lots LSL (LSL.special_lot_id,LSL.LOT_ID,LSL.BARCODE_PRV_NO,LSL.CARD_UPC,LSL.FOLDER_NAME,
        LSL.CARD_MPN,LSL.PIC_BY,LSL.PIC_DATE_TIME, LSL.INSERTED_AT,LSL.INSERTED_BY) select  
        2,
        1,
        '".$order_mt_array[$i]->Barcode."',
        '".$order_mt_array[$i]->UPC."',
        '".$order_mt_array[$i]->FolderName."',
        '".$order_mt_array[$i]->MPN."',
        '31',
        TO_DATE('".$order_mt_array[$i]->pic_DateTime."','dd/mm/yyyy hh:mi:ss am'),
        sysdate ,
        '31'
         from dual
        where not exists(select s.barcode_prv_no from lz_special_lots s where s.barcode_prv_no = '".$order_mt_array[$i]->Barcode."')
        ";


        $status_query=$this->db->query($mysqlstmnt);
        if($status_query)
        {
          $mt_id_index['barcode'] = $order_mt_array[$i]->Barcode; 
          $mt_id_index['status'] = 1;
          $mt_ids_array[$i] = $mt_id_index;
          // echo json_encode($status_query);
        }
        else {
          # code...
          $mt_id_index['barcode'] = $order_mt_array[$i]->Barcode; 
          $mt_id_index['status'] = 0;
          $mt_ids_array[$i] = $mt_id_index;
        }
       
    // echo json_encode($status_query); 
    }
    return array($mt_ids_array);

  }
  
public function get_cond()
{
  $main_query = "SELECT M.id,m.cond_name,m.cond_order FROM lz_item_cond_mt M order by m.cond_order";
  $main_query = $this->db->query($main_query)->result_array();
  return $main_query;
}

public function check_lz_barcode_mt($barcode_no)
{
  $main_query = "select m.barcode_no lz_barcode_yn from lz_barcode_mt m where m.barcode_no = '".$barcode_no."'";
  $main_query = $this->db->query($main_query);
  // return $main_query;
  //$lz_barcode_yn = $main_query[0]['LZ_BARCODE_YN'];
  if($main_query->num_rows() > 0)
  {
    $main_query = "select * from lz_barcode_mt m where  LIST_ID IS NULL
    AND SALE_RECORD_NO IS NULL
    AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL
    AND LZ_PART_ISSUE_MT_ID IS NULL
    AND LZ_POS_MT_ID IS NULL
    AND PULLING_ID IS NULL
    AND EBAY_ITEM_ID IS NULL AND m.barcode_no = '".$barcode_no."'";
    $main_query = $this->db->query($main_query);
    if($main_query->num_rows() > 0)
    {
      return 1; // barcode exist but available
    }else{
      return 2;// barcode exist but not available
    }
    // return 5;
  }
  else{
      return 0;//// barcode not exist in lz_barcode_mt
  }

}

public function check_lz_dekit_us_det($barcode_no)
{
  $main_query = "select count(*) lz_dek_dt_yn from lz_dekit_us_dt d where d.barcode_prv_no = '".$barcode_no."'";
  $main_query = $this->db->query($main_query)->result_array();
  return $main_query;
}

public function get_upc_mpn_cond($barcode_no)
{
  $main_query = "select * from (select d.item_mt_upc upc, d.item_mt_mfg_part_no mpn, c.cond_name,b.item_id,b.lz_manifest_id,b.condition_id from items_mt i, lz_manifest_det d, lz_barcode_mt b, lz_item_cond_mt c where b.item_id = i.item_id and b.lz_manifest_id = d.lz_manifest_id and d.laptop_item_code = i.item_code and c.id = b.condition_id and b.barcode_no = '".$barcode_no."'order by d.laptop_zone_id desc) where rownum = 1"; 

  $main_query = $this->db->query($main_query)->result_array();
  return $main_query;
}

public function get_test()

{
  $main_query = $this->db->query("select d.item_mt_upc upc, d.item_mt_mfg_part_no mpn, c.cond_name
  from items_mt i, lz_manifest_det d, lz_barcode_mt b, lz_item_cond_mt c
 where b.item_id = i.item_id
   and b.lz_manifest_id = d.lz_manifest_id
   and d.laptop_item_code = i.item_code
   and c.id = b.condition_id
--   and b.barcode_no = '109382'     --152216
   and d.item_mt_upc is null
   and rownum = 1")->result_array();
  return $main_query;
}

public function get_bin($bin_no)
{
  $bin_id = $this->db->query(" SELECT BIN_ID, BIN_NAME FROM (SELECT B.BIN_ID, B.BIN_TYPE || '-' || B.BIN_NO BIN_NAME FROM BIN_MT B) WHERE BIN_NAME = '$bin_no' ")->result_array();
  
  if(isset($bin_id))
  {
    
    if(empty($bin_id))
    {
      $bin_id = 0;
    }
    else
    {
      $bin_id = $bin_id[0]['BIN_ID'];
    }
  }
  else
  {
    $bin_id = 0;
  }
return $bin_id;
}

public function get_old_bin($barcode_no, $table_name, $column_name)
{
  $bin_id =$this->db->query("select l.bin_id from ".$table_name." l where l.".$column_name."='$barcode_no'")->result_array();
  if(isset($bin_id))
  {
    
    if(empty($bin_id))
    {
      $bin_id = 0;
    }
    else
    {
      $bin_id = $bin_id[0]['BIN_ID'];
      if(empty($bin_id))
      {
        $bin_id = 0;
      }

    }
  }
  else
  {
    $bin_id = 0;
  }
return $bin_id;

}

public function update_bin($barcode_no, $bin_id, $table_name,$column_name)
{
  $query =  $this->db->query("update ".$table_name." l set l.bin_id = ".$bin_id." where l.".$column_name." = '$barcode_no' ");
  
  return $query;
}


public function get_lot_id($barcode_no)
{
  $main_query = "select m.lot_id  from lz_merchant_barcode_mt m , lz_merchant_barcode_dt d
  where m.mt_id = d.mt_id
  and d.barcode_no = '$barcode_no' ";
  $main_query = $this->db->query($main_query)->result_array();
   if(isset($main_query))
  {
    if(empty($main_query))
    {
      $main_query = 0 ;
    }
    else
    {
      $main_query = $main_query[0]['LOT_ID'];
    }
  }
  else
  {
    $main_query = 0;
  }
return $main_query;
  
}

public function get_sync($date)
{
//   $main_query = "select cm.cond_name condition, d.barcode_no, m.upc, m.mpn, m.cost, m.imei1, m.imei2, m.remarks, cm.cond_abrivation cond, (select ITEM_TITLE from (select * from lz_item_seed s 
//   where (s.f_upc = m.upc) or (upper(s.f_mpn) = upper(m.mpn)) order by s.seed_id desc )where rownum =1) ITEM_TITLE, b.ebay_item_id
// from lz_purch_item_mt m, lz_purch_item_dt d, lz_item_cond_mt cm, lz_barcode_mt b
// where  to_date(m.inserted_date, 'dd/mm/YY') = to_date('$date', 'dd/mm/YY')
// and d.purch_mt_id = m.purch_mt_id
// and m.condition = cm.id 
// and d.barcode_no = b.barcode_no(+)
// order by d.barcode_no desc";
// $main_query = "select cm.cond_name       condition,
//        d.barcode_no,
//        m.upc,
//        m.mpn,
//        m.cost,
//        m.imei1,
//        m.imei2,
//        m.remarks,
//        cm.cond_abrivation cond,
//        s.item_title       ITEM_TITLE,
//        b.ebay_item_id
//   from lz_purch_item_mt m,
//        lz_purch_item_dt d,
//        lz_item_cond_mt cm,
//        lz_barcode_mt b,
//        (select max(item_title) item_title, max(f_upc) upc, max(f_mpn) mpn
//           from lz_item_seed s
//          group by s.item_id, s.default_cond) s
//  where to_date(m.inserted_date, 'dd/mm/YY') = to_date('$date', 'dd/mm/YY')
//    and d.purch_mt_id = m.purch_mt_id
//    and (s.upc = m.upc or upper(s.mpn) = upper(m.mpn))
//    and m.condition = cm.id
//    and d.barcode_no = b.barcode_no(+)
//  order by d.barcode_no desc";
 $main_query = "select * from
(select max(cm.cond_name)       condition,
       max(d.barcode_no) barcode_no,
       max(m.upc) upc,
       max(m.mpn) mpn,
       max(m.cost) cost,
       max(m.imei1) imei1,
       max(m.imei2) imei2,
      max( m.remarks) remarks,
       max(cm.cond_abrivation) cond,
       max(s.item_title)       ITEM_TITLE,
       max(b.ebay_item_id) ebay_item_id,
       max(m.inserted_date) inserted_date
  from lz_purch_item_mt m,
       lz_purch_item_dt d,
       lz_item_cond_mt cm,
       lz_barcode_mt b,
       (select max(item_title) item_title, max(f_upc) upc, max(f_mpn) mpn
          from lz_item_seed s
         group by s.item_id, s.default_cond) s
 where m.inserted_date between TO_DATE('$date 00:00:00', 'dd/mm/YY HH24:MI:SS') and TO_DATE('$date 23:59:59', 'dd/mm/YY HH24:MI:SS')
 --to_date(m.inserted_date, 'dd/mm/YY') = to_date('03/07/20', 'dd/mm/YY')
   and d.purch_mt_id = m.purch_mt_id
   --and (s.upc = m.upc or upper(s.mpn) = upper(m.mpn))
   and (m.upc = s.upc(+) and upper(m.mpn) = upper(s.mpn (+)) )
   and m.condition = cm.id
   and d.barcode_no = b.barcode_no(+)
 group by d.barcode_no )
order by inserted_date desc 
";
// echo $main_query; exit;
  $main_query = $this->db->query($main_query)->result_array();
  
return $main_query;
  
}

public function delete_barcode($barcode)
{
  $main_query = $this->db->query("delete from lz_purch_item_dt dt where dt.barcode_no = '$barcode'");
// echo $main_query; exit;
  
return $main_query;
  
}
public function getListedNotFound()
{
  $main_query = "SELECT decode(PP.PIC_URL,
              null,
              'http://71.78.236.20/item_pictures/master_pictures/image_not_available.jpg',
              PP.PIC_URL) PIC_URL,
       E.EBAY_ITEM_DESC,
       E.EBAY_ITEM_ID,
       'https://www.ebay.com/itm/'||E.EBAY_ITEM_ID ebay_url,
       NVL(S.F_UPC, '') UPC,
       B.BARCODE_NO,
       E.LIST_QTY,
       E.LIST_DATE,
       E.LIST_PRICE,
       E.LISTER_ID,
       B.EBAY_STICKER,
       B.BIN_ID,
       BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME,
       EM.USER_NAME LISTER_NAME,
       C.COND_NAME,
       E.LIST_DATE,
       b.barcode_notes,
       s.other_notes seed_remarks
  FROM LZ_BARCODE_MT B,
       EBAY_LIST_MT E,
       BIN_MT BM,
       EMPLOYEE_MT EM,
       LZ_ITEM_COND_MT C,
       LZ_ITEM_SEED S,
       (SELECT P.EBAY_ID,
               DECODE(P.IMG_KEY,
                      NULL,
                      NULL,
                      'https://i.ebayimg.com/images/g/' || p.img_key ||
                      '/s-l110.jpg') PIC_URL,
               P.LOCAL_URL
          FROM LZ_LISTED_ITEM_PIC P
         WHERE P.PIC_ID IN (SELECT MIN(PIC_ID) PIC_ID
                              FROM LZ_LISTED_ITEM_PIC P
                             WHERE P.EBAY_ID IS NOT NULL
                               AND P.IMG_KEY IS NOT NULL
                             GROUP BY EBAY_ID)) PP
 WHERE B.LIST_ID = E.LIST_ID
   AND E.EBAY_ITEM_ID = PP.EBAY_ID(+)
   AND B.BIN_ID = BM.BIN_ID
   AND EM.EMPLOYEE_ID = E.LISTER_ID
   AND E.SEED_ID = S.SEED_ID
   AND C.ID = B.CONDITION_ID
   AND B.NOT_FOUND_BY IS not NULL
   and b.discard = 0
   and b.condition_id <> -1
 ORDER BY E.LIST_DATE DESC
 ";
  $main_query = $this->db->query($main_query)->result_array();
  return $main_query;
}
public function foundBarcode()
{
  $barcode_no = trim($this->input->post('barcode_no'));
  $user_id = trim($this->input->post('user_id'));
  $main_query = "UPDATE LZ_BARCODE_MT
   SET NOT_FOUND_BY    = '',
       NOT_FOUND_DATE  = '',
       AUDIT_BY        = '$user_id',
       AUDIT_DATETIME  = SYSDATE,
       EBAY_STICKER    = 0,
       DISCARD_DATE    = '',
       DISCARD_BY      = '',
       DISCARD         = 0,
       DISCARD_REMARKS = '',
       FOUND_BY = '$user_id',
       FOUND_DATE = sysdate
 WHERE BARCODE_NO = '$barcode_no'
";
  $this->db->query($main_query);
  if($this->db->affected_rows() > 0){
    return 1;
  }else{
    return 0;
  }
  
}
}
?>