<?php
class M_add_werehouse extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add_wer()
    {

        $ware_no = $this->input->post('werehouse_no');
        $ware_desc = $this->input->post('werehouse_desc');
        $ware_desc = trim(str_replace("  ", ' ', $ware_desc));
        $ware_desc = trim(str_replace(array("'"), "''", $ware_desc));

        $ware_loc = $this->input->post('werehouse_loc');
        $ware_loc = trim(str_replace("  ", ' ', $ware_loc));
        $ware_loc = trim(str_replace(array("'"), "''", $ware_loc));
        $merchant_id = $this->input->post('merchant_id');
        if (!empty($merchant_id)) {
            $merchant_id = $merchant_id['value'];
        }
        $checkWareHouse = $this->db->query("SELECT WAREHOUSE_NO FROM WAREHOUSE_MT WHERE WAREHOUSE_NO = '$ware_no'");
        if ($checkWareHouse->num_rows() > 0) {
            $wareHouse = $this->db->query("select Max(WAREHOUSE_NO) WAREHOUSE_NO from warehouse_mt")->result_array();
            $wareHouse = $wareHouse[0]['WAREHOUSE_NO'];
            return array('status' => false, 'message' => 'Add Greater than ' . $wareHouse . ' Warehouse Number');
        } else {
            $war_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('WAREHOUSE_MT', 'WAREHOUSE_ID')ID FROM DUAL")->result_array();
            $war_pk = $war_pk[0]['ID'];

            $sav_war = $this->db->query("INSERT INTO WAREHOUSE_MT(WAREHOUSE_ID,WAREHOUSE_NO,WAREHOUSE_DESC,LOCATION, MERCHANT_ID) VALUES('$war_pk','$ware_no','$ware_desc','$ware_loc', '$merchant_id')");

            $get_data = $this->db->query("SELECT WAREHOUSE_ID FROM WAREHOUSE_MT where WAREHOUSE_ID = (SELECT max(WAREHOUSE_ID) from WAREHOUSE_MT)")->result_array();
            //$data = $this->db->query($get_data)->;
            $sav_war = $get_data[0]['WAREHOUSE_ID'];
            $sav_war = $this->db->query("SELECT w.*,m.contact_person MERCHANT_NAME  FROM  WAREHOUSE_MT W, lz_merchant_mt m  where w.WAREHOUSE_ID = '$sav_war' AND w.MERCHANT_ID = '$merchant_id' AND w.merchant_id = m.merchant_id order by w.WAREHOUSE_ID DESC ")->result_array();
            return array("status" => true, "tableData" => $sav_war);
        }

    }

    public function get_wer()
    {
        $merchant_id = $this->input->post('merchant_id');
        if (!empty($merchant_id)) {
            $merchant_id = $merchant_id['value'];
        }
// var_dump($auto_we_no); exit;
        $qry = $this->db->query("SELECT w.*, m.contact_person MERCHANT_NAME FROM WAREHOUSE_MT W, lz_merchant_mt m where  w.MERCHANT_ID = '$merchant_id' AND w.merchant_id = m.merchant_id
        ")->result_array();
        return $qry;
    }
    public function get_atu_no()
    {
        $auto_we_no = $this->db->query("SELECT max(warehouse_no + 1) WH_NO from WAREHOUSE_MT")->result_array();
        //$auto_we_no = $auto_we_no[0]['WH_NO'];
        return $auto_we_no;
    }

    public function Update_Warehouse_Desc()
    {
        $updated_by = $this->input->post('enter_by');
        $warehouse_desc = $this->input->post('warehouse_desc');
        $warehouse_id = $this->input->post('warehouse_id');

        $update = $this->db->query("UPDATE WAREHOUSE_MT SET WAREHOUSE_DESC = '$warehouse_desc', UPDATED_BY = '$updated_by', UPDATED_DATE = sysdate WHERE WAREHOUSE_ID = '$warehouse_id'");
        if ($update == true) {
            return array('status' => true, 'message' => 'Record Updated successfully');
        } else {
            return array('status' => false, 'message' => 'Record Not Updated successfully');
        }
    }
    public function Update_Warehouse_Location()
    {
        $updated_by = $this->input->post('enter_by');
        $warehouse_location = $this->input->post('warehouse_location');
        $warehouse_id = $this->input->post('warehouse_id');

        $update = $this->db->query("UPDATE WAREHOUSE_MT SET LOCATION = '$warehouse_location', UPDATED_BY = '$updated_by', UPDATED_DATE = sysdate WHERE WAREHOUSE_ID = '$warehouse_id'");
        if ($update == true) {
            return array('status' => true, 'message' => 'Record Updated successfully');
        } else {
            return array('status' => false, 'message' => 'Record Not Updated successfully');
        }
    }

    public function Delete_WareHouse()
    {
        $warehouse_id = $this->input->post('warehouse_id');
        $merchant_id = $this->input->post('merchant_id');
        $getRacks = $this->db->query("SELECT RACK_ID FROM RACK_MT WHERE WAREHOUSE_ID = '$warehouse_id'");
        if ($getRacks->num_rows() > 0) {
            return array('status' => false, 'message' => 'WareHouse Not Deleted Because ' . $getRacks->num_rows() . ' Rack In It');
        } else {
            $this->db->query("DELETE FROM WAREHOUSE_MT WHERE WAREHOUSE_ID = '$warehouse_id'");
            return array('status' => true, 'message' => 'WareHouse Deleted Successfully');
        }
    }

    /********************************
     *   start Screen rack
     *********************************/

    public function dropdown_wer_desc()
    {
        $user_id = $this->input->post('user_id');
        $merchant_id = $this->input->post('mid');
        if ($merchant_id !== '1') {
            $war_house = $this->db->query("SELECT WAREHOUSE_ID,WAREHOUSE_DESC, MERCHANT_ID FROM WAREHOUSE_MT WHERE MERCHANT_ID = '$merchant_id'")->result_array();

        } else {
            $war_house = $this->db->query("SELECT WAREHOUSE_ID,WAREHOUSE_DESC, MERCHANT_ID FROM WAREHOUSE_MT")->result_array();

        }

        return $war_house;
    }
    public function dropdown_rack_type()
    {
        $reack_house = $this->db->query("SELECT RACK_ID,  DECODE(RACK_TYPE, 1, 'CAGE', 2, 'RACK') RACK_TYPE FROM RACK_MT")->result_array();

        return $reack_house;
    }
    public function get_Rack()
    {
        $merchant_id = $this->input->post('mid');
        $rackqry = "SELECT RA.RACK_ID,RA.RACK_NO,WA.WAREHOUSE_DESC,RA.WIDTH,RA.HEIGHT,RA.NO_OF_ROWS,DECODE(RA.RACK_TYPE,1,'CAGE',2,'RACK') RACK_TYPE, MM.BUISNESS_NAME FROM RACK_MT RA ,WAREHOUSE_MT WA, LZ_MERCHANT_MT MM WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID  AND WA.MERCHANT_ID = MM.MERCHANT_ID";
        //  and RA.RACK_ID <>0 AND WA.MERCHANT_ID = '$merchant_id' ORDER BY RACK_ID ASC ")->result_array();
        $rackqry .= " AND RA.RACK_ID <>0 ";
        if ($merchant_id !== '1') {
            $rackqry .= " AND WA.MERCHANT_ID = '$merchant_id' ";
        }
        $rackqry .= " ORDER BY RACK_ID ASC";
        return $this->db->query($rackqry)->result_array();
    }
    public function Get_Rack_By_WareHouse()
    {
        $merchant_id = $this->input->post('mid');
        $warehouse_id = $this->input->post('warehouse_id');
        if (!empty($warehouse_id)) {
            $warehouse_id = $warehouse_id['value'];

            $rackqry = "SELECT RA.RACK_ID, RA.RACK_NO, WA.WAREHOUSE_DESC, RA.WIDTH, RA.HEIGHT, RA.NO_OF_ROWS, DECODE(RA.RACK_TYPE, 1, 'CAGE', 2, 'RACK') RACK_TYPE, MM.BUISNESS_NAME FROM RACK_MT RA, WAREHOUSE_MT WA,LZ_MERCHANT_MT MM WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID AND WA.MERCHANT_ID = MM.MERCHANT_ID";

            $rackqry .= " AND RA.WAREHOUSE_ID = '$warehouse_id' and RA.RACK_ID <>0 ";
            if ($merchant_id !== '1') {
                $rackqry .= " AND WA.MERCHANT_ID = '$merchant_id' ";
            }
            $rackqry .= " ORDER BY RACK_ID ASC";

            return $this->db->query($rackqry)->result_array();

        } else {

            $rackqry = "SELECT RA.RACK_ID, RA.RACK_NO, WA.WAREHOUSE_DESC, RA.WIDTH, RA.HEIGHT, RA.NO_OF_ROWS, DECODE(RA.RACK_TYPE, 1, 'CAGE', 2, 'RACK') RACK_TYPE, MM.BUISNESS_NAME FROM RACK_MT RA, WAREHOUSE_MT WA,LZ_MERCHANT_MT MM WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID AND WA.MERCHANT_ID = MM.MERCHANT_ID";
            $rackqry .= " and RA.RACK_ID <>0 ";
            if ($merchant_id !== '1') {
                $rackqry .= " AND WA.MERCHANT_ID = '$merchant_id' ";
            }
            $rackqry .= " ORDER BY RACK_ID ASC";

            return $this->db->query($rackqry)->result_array();

            // $rackqry = $this->db->query("SELECT RA.RACK_ID,RA.RACK_NO,WA.WAREHOUSE_DESC,RA.WIDTH,RA.HEIGHT,RA.NO_OF_ROWS,DECODE(RA.RACK_TYPE,1,'CAGE',2,'RACK') RACK_TYPE FROM RACK_MT RA ,WAREHOUSE_MT WA WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID and RA.RACK_ID <>0 AND WA.MERCHANT_ID = '$merchant_id' ORDER BY RACK_ID ASC ")->result_array();

            // return $rackqry;
        }

    }
    public function add_rack()
    {

        $WarehouseDescription = $this->input->post('WarehouseDescription');
        $rack_width = $this->input->post('Rackwidth'); //ID
        $rack_height = $this->input->post('Rackheight'); //ID
        $no_of_rows = $this->input->post('NoOfRows'); //ID
        if (isset($_POST['isMobile'])) {
            $warehous_id = $_POST['warehouse_id'];
            $merchant_id = $_POST['merchant_id'];
            $rack_type = $this->input->post('RackTYPE'); //ID
        } else {
            if (!empty($WarehouseDescription)) {
                $warehous_id = $WarehouseDescription['value'];
                $merchant_id = $WarehouseDescription['merId'];
            }

            $rack_type = $this->input->post('RackTYPE'); //ID
            if (!empty($rack_type)) {
                $rack_type = $rack_type['value'];
            }
        }
        $no_of_rack = $this->input->post('NoOfRacks'); //ID
        $bin_array = '';
        $q = 1;
        for ($p = 0; $p < $no_of_rack; $p++) {
            $rack_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('RACK_MT', 'RACK_ID')ID FROM DUAL")->result_array();
            $rack_pk = $rack_pk[0]['ID'];
            $get_val = $this->db->query("SELECT 'K' || '' || NVL(regexp_replace(MAX(RACK_NO), '[^[:digit:]]', '') + 1, 1) NEXT_VAL from RACK_MT r where r.rack_id = (select max(R.RACK_ID) from RACK_MT R, warehouse_mt w  where r.warehouse_id = w.warehouse_id and w.merchant_id ='$merchant_id')")->result_array();

            $get_val = $get_val[0]['NEXT_VAL'];
            $sav_war = $this->db->query("INSERT INTO RACK_MT(RACK_ID,RACK_NO,WAREHOUSE_ID,WIDTH,HEIGHT,NO_OF_ROWS,RACK_TYPE) VALUES('$rack_pk','$get_val','$warehous_id','$rack_width','$rack_height','$no_of_rows','$rack_type')");

            // $get_data = $this->db->query("SELECT RACK_ID FROM RACK_MT where RACK_ID = (SELECT max(RACK_ID) from RACK_MT)")->result_array();
            // //$data = $this->db->query($get_data)->;
            // $get_id = $get_data[0]['RACK_ID'];
            // $rackqry = $this->db->query("SELECT RA.RACK_ID,RA.RACK_NO,WA.WAREHOUSE_DESC,RA.WIDTH,RA.HEIGHT,RA.NO_OF_ROWS,DECODE(RA.RACK_TYPE,1,'CAGE',2,'RACK') RACK_TYPE FROM RACK_MT RA ,WAREHOUSE_MT WA WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID and RA.RACK_ID <>0  and RA.RACK_ID =' $get_id'  ORDER BY RACK_ID ASC ")->result_array();
            for ($j = 1; $j <= $no_of_rows; $j++) { // 2
                $row_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_RACK_ROWS', 'RACK_ROW_ID')ID FROM DUAL")->result_array();
                $row_pk = $row_pk[0]['ID'];

                $this->db->query("INSERT INTO LZ_RACK_ROWS(RACK_ROW_ID,RACK_ID,ROW_NO) values('$row_pk','$rack_pk',$j)");
                //2
            }

            if ($no_of_rack > 0) {
                if ($q >= 1 && $q < $no_of_rack) {
                    $bin_array .= $rack_pk . ",";
                }
                if ($q == $no_of_rack) {
                    $bin_array .= $rack_pk;
                }
            } else {
                $bin_array .= $rack_pk;
            }
            $q++;
        }
  //       $qr = "SELECT RA.RACK_ID,
  //       RA.RACK_NO,
  //       WA.WAREHOUSE_DESC,
  //       RA.WIDTH,
  //       RA.HEIGHT,
  //       RA.NO_OF_ROWS,
  //       DECODE(RA.RACK_TYPE, 1, 'CAGE', 2, 'RACK') RACK_TYPE,
  //       'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO RACK_NAME
  //  FROM RACK_MT RA, WAREHOUSE_MT WA
  // WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID
  //   AND RA.RACK_ID in ($bin_array)
  // ORDER BY RACK_ID ASC";
  $qr = "SELECT RA.RACK_ID,
        RA.RACK_NO,
        WA.WAREHOUSE_DESC,
        RA.WIDTH,
        RA.HEIGHT,
        RA.NO_OF_ROWS,
        DECODE(RA.RACK_TYPE, 1, 'CAGE', 2, 'RACK') RACK_TYPE,
        'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO RACK_NAME,
        m.buisness_name
   FROM RACK_MT RA, WAREHOUSE_MT WA , lz_merchant_mt m
  WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID
  and m.merchant_id = wa.merchant_id
    AND RA.RACK_ID in ($bin_array)
  ORDER BY RACK_ID ASC";
        $data = $this->db->query($qr);
        if ($sav_war == true) {
            return array("status" => true, "data" => $data->result_array());

        } else {
            return array("status" => false, "message" => 'Not Insert Into RACK_MT');
        }
    }

    public function print_sticker()
    {
        // $rack_id = $this->uri->segment(4);
        $rack_id = $_GET['id'];
        // var_dump($rack_id); exit;
        $rack_stick = $this->db->query("SELECT 'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO RACK_NAME FROM RACK_MT RA, WAREHOUSE_MT WA WHERE RACK_ID = $rack_id AND RA.WAREHOUSE_ID = WA.WAREHOUSE_ID ")->result_array();
        return $rack_stick;
    }

    public function Get_Single_Rack_Print()
    {
        $rack_id = $_GET['rack_id'];
        $rack_stick = $this->db->query("SELECT 'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO RACK_NAME FROM RACK_MT RA, WAREHOUSE_MT WA WHERE RACK_ID = $rack_id AND RA.WAREHOUSE_ID = WA.WAREHOUSE_ID ")->result_array();
        return $rack_stick;

    }
    public function Get_All_Rack_Rows_Print()
    {
        $rack_id = $_GET['rack_id'];
        $rack_stick = $this->db->query("SELECT RO.RACK_ROW_ID,'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO ||'-' || 'R' || RO.ROW_NO  RACK_NAME FROM RACK_MT RA, WAREHOUSE_MT WA, LZ_RACK_ROWS RO WHERE RA.RACK_ID = $rack_id AND RA.WAREHOUSE_ID = WA.WAREHOUSE_ID AND RA.RACK_ID = RO.RACK_ID ORDER BY RO.ROW_NO ASC ")->result_array();
        return $rack_stick;

    }

    public function Get_Single_Rack_Print_Mobile()
    {
        // $rack_id = $_GET['rack_id'];
        $rack_id = $this->input->post('rack_id');
        $rack_stick = $this->db->query("SELECT 'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO RACK_NAME FROM RACK_MT RA, WAREHOUSE_MT WA WHERE RACK_ID = $rack_id AND RA.WAREHOUSE_ID = WA.WAREHOUSE_ID ")->result_array();
        return $rack_stick;

    }
    public function Get_All_Rack_Rows_Print_Mobile()
    {
        // $rack_id = $_GET['rack_id'];
        $rack_id = $this->input->post('rack_id');
        $rack_stick = $this->db->query("SELECT RO.RACK_ROW_ID,'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO ||'-' || 'R' || RO.ROW_NO  RACK_NAME FROM RACK_MT RA, WAREHOUSE_MT WA, LZ_RACK_ROWS RO WHERE RA.RACK_ID = $rack_id AND RA.WAREHOUSE_ID = WA.WAREHOUSE_ID AND RA.RACK_ID = RO.RACK_ID ORDER BY RO.ROW_NO ASC ")->result_array();
        return $rack_stick;

    }

    /********************************
     *   start Screen rack
     *********************************/

    /********************************
     *   start Screen add bin
     *********************************/
    public function get_bin()
    {
        $merchant_id = $this->input->post('mid');
        $qry = $this->db->query("SELECT B.PRINT_STATUS,
        B.BIN_ID,
        B.BIN_NO || '-' || B.BIN_TYPE BIN_NAME,
        DECODE(BIN_TYPE,
               'NA',
               'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO || '-' ||
               B.BIN_TYPE || '-' || B.BIN_NO,
               B.BIN_TYPE || '-' || B.BIN_NO) BIN_NO,
               B.BIN_TYPE,
               MM.BUISNESS_NAME
    FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA, LZ_MERCHANT_MT MM
    WHERE BIN_TYPE NOT IN ('No Bin')
    AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
    AND RA.RACK_ID = M.RACK_ID
    AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID
    AND MM.MERCHANT_ID = B.MERCHANT_ID
    AND B.MERCHANT_ID = '$merchant_id'")->result_array();
        return $qry;
    }

    public function Get_Bin_By_Bin_Type()
    {
        $bin_type = $this->input->post('bin_type');
        $merchant_id = $this->input->post('merchant_id');
        $print_sta = $this->input->post('PrintStatus');
        $print_sta = $print_sta['value'];
        if (empty($merchant_id) && empty($bin_type) && $print_sta == 0) {
            $qry = $this->db->query("SELECT B.PRINT_STATUS,
            B.BIN_ID,
            B.BIN_NO || '-' || B.BIN_TYPE BIN_NAME,
            DECODE(BIN_TYPE,
                   'NA',
                   'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO || '-' ||
                   B.BIN_TYPE || '-' || B.BIN_NO,
                   B.BIN_TYPE || '-' || B.BIN_NO) BIN_NO,
                   B.BIN_TYPE,
                   MM.BUISNESS_NAME
        FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA, LZ_MERCHANT_MT MM
        WHERE BIN_TYPE NOT IN ('No Bin')
        AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
        AND RA.RACK_ID = M.RACK_ID
        AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID
        AND MM.MERCHANT_ID = B.MERCHANT_ID")->result_array();
            return $qry;
        } else {
            $qry = "SELECT B.PRINT_STATUS,
            B.BIN_ID,
            B.BIN_NO || '-' || B.BIN_TYPE BIN_NAME,
            DECODE(BIN_TYPE,
                   'NA',
                   'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO || '-' ||
                   B.BIN_TYPE || '-' || B.BIN_NO,
                   B.BIN_TYPE || '-' || B.BIN_NO) BIN_NO,
                   B.BIN_TYPE,
                   MM.BUISNESS_NAME
        FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA, LZ_MERCHANT_MT MM
        WHERE BIN_TYPE NOT IN ('No Bin')
        AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
        AND RA.RACK_ID = M.RACK_ID
        AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID
        AND MM.MERCHANT_ID = B.MERCHANT_ID ";
            if (!empty($merchant_id)) {
                $merchant_id = $merchant_id['value'];
                $merchant_id = (trim($merchant_id));
                $qry .= "AND (B.MERCHANT_ID) = '$merchant_id'";
            }
            if (!empty($bin_type)) {
                $bin_type = $bin_type['value'];
                $bin_type = (trim($bin_type));
                if ($bin_type !== 'ALL') {
                    $qry .= " AND (B.BIN_TYPE) = '$bin_type'";
                }
            }
            if ($print_sta != 0) {
                $qry .= " AND (B.PRINT_STATUS) = '$print_sta'";
            }
            $data = $this->db->query($qry)->result_array();

            return $data;
        }
    }
    //   public function Search_printStatus()
    //   {
    //     $PrintStatus = $this->input->post('PrintStatus');
    //     $bin_type = $this->input->post('Bin_type');

    //     $bin_no = $this->db->query(" SELECT GET_BIN_NO('$bin_type') BIN_NO FROM DUAL ")->result_array();

    //     $bin_no = $bin_no[0]['BIN_NO'];
    //    // var_dump($bin_no); exit;
    //     $qry = $this->db->query("SELECT B.PRINT_STATUS,
    //     B.BIN_ID,
    //     B.BIN_NO || '-' || B.BIN_TYPE BIN_NAME,
    //     DECODE(BIN_TYPE,
    //            'NA',
    //            'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO || '-' ||
    //            B.BIN_TYPE || '-' || B.BIN_NO,
    //            B.BIN_TYPE || '-' || B.BIN_NO) BIN_NO
    // FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA
    // WHERE BIN_TYPE NOT IN ('No Bin')
    // AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
    // AND RA.RACK_ID = M.RACK_ID
    // AND  B.PRINT_STATUS='$PrintStatus'
    // AND B.BIN_TYPE=' $bin_type'
    // AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID")->result_array();
    //     return $qry;
    //   }

    public function dropdowns()
    {

        $war_house = $this->db->query("SELECT WAREHOUSE_ID,WAREHOUSE_DESC FROM WAREHOUSE_MT")->result_array();
        $reack_house = $this->db->query("SELECT RACK_ID,RACK_NO FROM RACK_MT")->result_array();

        return array('war_house' => $war_house, 'reack_house' => $reack_house);
    }
    public function Search_printStatus()
    {

        $bin_type_search = $this->input->post('Bin_type'); //ID
        if (!empty($bin_type_search)) {
            $bin_type_search = $bin_type_search['value'];
        }
        $print_sta = $this->input->post('PrintStatus');

        $merchant_id = $this->input->post('merchant_id');
        // if (!empty($merchant_id)) {
        //     $merchant_id = $merchant_id['value'];
        // }
        // var_dump($bin_type_search, $print_sta);
        // exit;
        // $qry = $this->db->query("SELECT WAREHOUSE_NO,WAREHOUSE_DESC,LOCATION FROM WAREHOUSE_MT order by WAREHOUSE_ID asc ")->result_array();

        // $rackqry = $this->db->query("SELECT RA.RACK_ID,RA.RACK_NO,WA.WAREHOUSE_DESC,RA.WIDTH,RA.HEIGHT,RA.NO_OF_ROWS,DECODE(RA.RACK_TYPE,1,'CAGE',2,'RACK') RACK_TYPE FROM RACK_MT RA ,WAREHOUSE_MT WA WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID and RA.RACK_ID <>0 ORDER BY RACK_ID ASC ")->result_array();

        $binqry = "SELECT B.PRINT_STATUS,
        B.BIN_ID,
        B.BIN_NO || '-' || B.BIN_TYPE BIN_NAME,
        DECODE(BIN_TYPE,
               'NA',
               'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO || '-' ||
               B.BIN_TYPE || '-' || B.BIN_NO,
               B.BIN_TYPE || '-' || B.BIN_NO) BIN_NO,
               B.BIN_TYPE,
               MM.BUISNESS_NAME
    FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA, LZ_MERCHANT_MT MM
    WHERE BIN_TYPE NOT IN ('No Bin')
    AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
    AND RA.RACK_ID = M.RACK_ID
    AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID
    AND MM.MERCHANT_ID = B.MERCHANT_ID ";

        if (!empty($bin_type_search) && $bin_type_search !== 'ALL') {

            $binqry .= " AND  B.BIN_TYPE = '$bin_type_search' ";
        }
        if (!empty($print_sta)) {
            $print_sta = $print_sta['value'];
            if ($print_sta != 0) {
                $binqry .= " AND  B.PRINT_STATUS = '$print_sta'";
            }
        }
        if (!empty($merchant_id)) {
            $merchant_id = $merchant_id['value'];
            $binqry .= " AND B.MERCHANT_ID = '$merchant_id'";
        }

        $binqry .= " ORDER BY BIN_TYPE,BIN_NO ";

        $binqry = $this->db->query($binqry)->result_array();

        // var_dump($this->db->last_query());
        // exit;

        return $binqry;
    }

    public function add_bin()
    {
        $bin_type = $this->input->post('Bin_type');
        $bin_type = trim(str_replace("  ", ' ', $bin_type));
        $bin_type = str_replace(array("`,′"), "", $bin_type);
        $bin_type = str_replace(array("'"), "''", $bin_type);
        $user_id = $this->input->post('user_id');
        $print_status = "";
        $isMobile = trim($this->input->post('isMobile'));
        // var_dump( $bin_type); exit;
        if (!empty($isMobile)) {
            $merchant_id = $this->input->post('merchant_id');
            $print_status = "2";
        } else {
            $print_status = "1";
            $merchant_id = $this->input->post('merchant_id');
            if (!empty($merchant_id)) {
                $merchant_id = $merchant_id['value'];
            }
        }
        $no_bin = $this->input->post('no_of_bins');
        $no_bin = trim(str_replace("  ", ' ', $no_bin));
        $no_bin = str_replace(array("`,′"), "", $no_bin);
        $no_bin = str_replace(array("'"), "''", $no_bin);
        ///var_dump($bin_type,$no_bin ); exit;
        $bin_array = "";
        $j = 1;
        if (!empty($bin_type) && $bin_type != 1) {
            // var_dump($bin_type);exit;
            for ($p = 0; $p < $no_bin; $p++) {
                $bin_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('BIN_MT', 'BIN_ID')ID FROM DUAL")->result_array();
                $bin_pk = $bin_pk[0]['ID'];
                // var_dump($bin_pk);
                $bin_no = $this->db->query(" SELECT GET_BIN_NO('$bin_type') BIN_NO FROM DUAL ")->result_array();

                $bin_no = $bin_no[0]['BIN_NO'];
                $sav_war = $this->db->query("INSERT INTO BIN_MT(BIN_ID,BIN_NO,BIN_TYPE, PRINT_STATUS,MERCHANT_ID, CREATED_BY, CREATED_AT) VALUES('$bin_pk','$bin_no','$bin_type', '$print_status','$merchant_id', '$user_id', sysdate)");

                if ($no_bin > 1) {
                    if ($j >= 1 && $j < $no_bin) {
                        $bin_array .= $bin_pk . ",";
                    }
                    if ($j == $no_bin) {
                        $bin_array .= $bin_pk;
                    }
                } else {
                    $bin_array .= $bin_pk;
                }
                $j++;
            }
        }

        $data = $this->db->query("SELECT B.PRINT_STATUS,
      B.BIN_ID,
      B.BIN_NO || '-' || B.BIN_TYPE BIN_NAME,
      DECODE(BIN_TYPE,
             'NA',
             'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO || '-' ||
             B.BIN_TYPE || '-' || B.BIN_NO,
             B.BIN_TYPE || '-' || B.BIN_NO) BIN_NO,
             B.BIN_TYPE,
               MM.BUISNESS_NAME
 FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA, LZ_MERCHANT_MT MM
WHERE BIN_TYPE NOT IN ('No Bin')
  AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
  AND RA.RACK_ID = M.RACK_ID
  AND B.BIN_ID in ($bin_array)
  AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID
  AND MM.MERCHANT_ID = B.MERCHANT_ID")->result_array();

        return array("status" => true, "data" => $data);

    }

    public function Get_Bin_Type()
    {
        $data = $this->db->query("select b.type_name name,b.type_value value from bin_type_mt B ORDER BY B.BIN_TYPE_ID")->result_array();
        if (count($data) > 0) {
            return array('status' => true, 'data' => $data);
        } else {
            return array('status' => true, 'data' => array(), 'message' => 'No Record Found');
        }
    }

    public function Get_Single_Bin_Print()
    {
        $bin_id = $_GET['bin_id'];
        $rack_bin = $this->db->query("SELECT  B.BIN_TYPE || '-' || B.BIN_NO BIN_NAME ,DECODE(BIN_TYPE, 'NA', 'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO) WARE_NAME FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA WHERE BIN_TYPE NOT IN ('No Bin') AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID AND RA.RACK_ID = M.RACK_ID AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID AND B.BIN_ID =$bin_id  ")->result_array();
        $this->db->query(" UPDATE BIN_MT SET PRINT_STATUS = 2 where bin_id =$bin_id ");
        return $rack_bin;
    }
    public function Get_Single_Bin_Print_Mobile()
    {
        $bin_id = trim($this->input->post('bin_id'));
        $rack_bin = $this->db->query("SELECT  B.BIN_TYPE || '-' || B.BIN_NO BIN_NAME ,DECODE(BIN_TYPE, 'NA', 'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO) WARE_NAME FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA WHERE BIN_TYPE NOT IN ('No Bin') AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID AND RA.RACK_ID = M.RACK_ID AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID AND B.BIN_ID =$bin_id  ")->result_array();
        $this->db->query(" UPDATE BIN_MT SET PRINT_STATUS = 2 where bin_id =$bin_id ");
        return $rack_bin;
    }
    public function Print_Bin_All_React()
    {
        $print_sta = json_decode($_GET['PrintStatus']);
        $print_sta = $print_sta->value;
        $bin_type = json_decode($_GET['Bin_type']);
        $bin_type = $bin_type->value;
        $merchant_id = json_decode($_GET['merchant_id']);
        if (!empty($merchant_id)) {
            $merchant_id = $merchant_id->value;
        }
        $qry = "SELECT  B.BIN_TYPE || '-' || B.BIN_NO BIN_NAME ,DECODE(BIN_TYPE, 'NA', 'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO) WARE_NAME FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA WHERE B.MERCHANT_ID = '$merchant_id' AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID AND RA.RACK_ID = M.RACK_ID AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID ";
        if ($bin_type !== 'ALL') {
            $qry .= " AND B.BIN_type ='$bin_type' ";
        }
        if (!empty($print_sta)) {
            if ($print_sta !== 0) {
                $qry .= " AND  B.PRINT_STATUS = '$print_sta'";
            }
        }

        $qry .= " ORDER BY B.BIN_TYPE,B.BIN_NO   ";
        $rack_bin = $this->db->query($qry)->result_array();
        $qry = "UPDATE BIN_MT B SET B.PRINT_STATUS = 2 ";
        if ($bin_type !== 'ALL' && $print_sta !== 0) {
            $qry .= " WHERE B.BIN_type ='$bin_type' AND B.PRINT_STATUS = '$print_sta' AND B.MERCHANT_ID = '$merchant_id' ";
        } else if ($bin_type == 'ALL' && $print_sta == 0) {
            $qry .= " WHERE B.MERCHANT_ID = '$merchant_id' ";
        } else if ($bin_type !== 'ALL' && $print_sta == 0) {
            $qry .= " WHERE B.BIN_type ='$bin_type' AND B.MERCHANT_ID = '$merchant_id' ";
        } else if ($bin_type == 'ALL' && $print_sta !== 0) {
            $qry .= " WHERE B.PRINT_STATUS = '$print_sta' AND B.MERCHANT_ID = '$merchant_id' ";
        }
        $this->db->query($qry);
        // where  BIN_type ='$bin_type' and PRINT_STATUS = $print_sta  ");
        return $rack_bin;
    }
    public function Print_Bin_All_React_Mobile()
    {
        $print_sta = trim($this->input->post('PrintStatus'));
        $bin_type = trim($this->input->post('Bin_type'));
        $merchant_id = trim($this->input->post('merchant_id'));

        $qry = "SELECT  B.BIN_TYPE || '-' || B.BIN_NO BIN_NAME ,DECODE(BIN_TYPE, 'NA', 'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO) WARE_NAME FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA WHERE B.MERCHANT_ID = '$merchant_id' AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID AND RA.RACK_ID = M.RACK_ID AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID ";
        if ($bin_type !== 'ALL') {
            $qry .= " AND B.BIN_type ='$bin_type' ";
        }
        if (!empty($print_sta)) {
            if ($print_sta !== 0) {
                $qry .= " AND  B.PRINT_STATUS = '$print_sta'";
            }
        }

        $qry .= " ORDER BY B.BIN_TYPE,B.BIN_NO   ";
        $rack_bin = $this->db->query($qry)->result_array();
        $qry = "UPDATE BIN_MT B SET B.PRINT_STATUS = 2 ";
        if ($bin_type !== 'ALL' && $print_sta !== 0) {
            $qry .= " WHERE B.BIN_type ='$bin_type' AND B.PRINT_STATUS = '$print_sta' AND B.MERCHANT_ID = '$merchant_id' ";
        } else if ($bin_type == 'ALL' && $print_sta == 0) {
            $qry .= " WHERE B.MERCHANT_ID = '$merchant_id' ";
        } else if ($bin_type !== 'ALL' && $print_sta == 0) {
            $qry .= " WHERE B.BIN_type ='$bin_type' AND B.MERCHANT_ID = '$merchant_id' ";
        } else if ($bin_type == 'ALL' && $print_sta !== 0) {
            $qry .= " WHERE B.PRINT_STATUS = '$print_sta' AND B.MERCHANT_ID = '$merchant_id' ";
        }
        $this->db->query($qry);
        // where  BIN_type ='$bin_type' and PRINT_STATUS = $print_sta  ");
        return $rack_bin;
    }

    /********************************
     *   end Screen add bin
     *********************************/
    /********************************
     *   start Screen item packing
     *********************************/
    public function get_barcode()
    {

        $barcode = $this->input->post('barcode');
        $barcode = trim(str_replace("  ", ' ', $barcode));
        $barcode = str_replace(array("`,′"), "", $barcode);
        $barcode = str_replace(array("'"), "''", $barcode);
        // var_dump($barcode);
        $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
        $str = strlen($barcode); //if length = 12 its ebay id

        if ($str == 12) {
            $ebY_deta = $this->db->query(" SELECT B.BARCODE_NO,
         CASE
           WHEN DE.SALES_RECORD_NUMBER IS NOT NULL AND
                DE.TRACKING_NUMBER IS NOT NULL THEN
            'SOLD || SHIPPED'
           WHEN DE.SALES_RECORD_NUMBER IS NOT NULL AND
                DE.TRACKING_NUMBER IS NULL THEN
            'SOLD || NOT SHIPPED'
           ELSE
            'AVAILABLE'
         END STATUS,
         B.EBAY_ITEM_ID,
         I.ITEM_DESC DESCR,
         I.ITEM_MT_MANUFACTURE MANUF,
          I.ITEM_MT_MFG_PART_NO MPN,
          I.ITEM_MT_UPC UPC,
         CO.COND_NAME CONDI,
         BI.BIN_ID,
         BI.BIN_TYPE || '-' || BI.BIN_NO BIN_NAME,
         DECODE(ROW_NO,
                0,
                RC.RACK_NO,
                'W' || '' || WA.WAREHOUSE_NO || '-' || RC.RACK_NO || '-R' ||
                RA.ROW_NO) RACK_NAME,
         (SELECT DT.BARCODE_NO
            FROM LJ_BIN_VERIFY_DT DT
           WHERE DT.BARCODE_NO = B.BARCODE_NO
             AND ROWNUM = 1) VERIFIED_YN
    FROM LZ_BARCODE_MT    B,
         LZ_SALESLOAD_DET DE,
         ITEMS_MT         I,
         LZ_ITEM_COND_MT  CO,
         BIN_MT           BI,
         LZ_RACK_ROWS     RA,
         RACK_MT          RC,
         WAREHOUSE_MT     WA
   WHERE B.EBAY_ITEM_ID = $barcode
     AND B.ITEM_ID = I.ITEM_ID
     AND B.BIN_ID = BI.BIN_ID(+)
     AND BI.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
     AND RA.RACK_ID = RC.RACK_ID
     AND B.SALE_RECORD_NO = DE.SALES_RECORD_NUMBER(+)
     AND RC.WAREHOUSE_ID = WA.WAREHOUSE_ID
     AND B.CONDITION_ID = CO.ID(+)
   ")->result_array();

            return array('ebY_deta' => $ebY_deta, 'eby_qyery' => true, 'images' => $conditions);
        } else {

            $bar_query = $this->db->query(" SELECT B.BARCODE_NO FROM LZ_BARCODE_MT  B WHERE B.BARCODE_NO ='$barcode'");

            if ($bar_query->num_rows() > 0) {
                $bar_query = $bar_query->result_array();
                $bar_no = $bar_query[0]['BARCODE_NO'];

                $bar_deta = $this->db->query(" SELECT B.BARCODE_NO,
         CASE
           WHEN DE.SALES_RECORD_NUMBER IS NOT NULL AND
                DE.TRACKING_NUMBER IS NOT NULL THEN
            'SOLD || SHIPPED'
           WHEN DE.SALES_RECORD_NUMBER IS NOT NULL AND
                DE.TRACKING_NUMBER IS NULL THEN
            'SOLD || NOT SHIPPED'
           ELSE
            'AVAILABLE'
         END STATUS,
         B.EBAY_ITEM_ID,
         I.ITEM_DESC DESCR,
         I.ITEM_MT_MANUFACTURE MANUF,
         I.ITEM_MT_MFG_PART_NO MPN,
          I.ITEM_MT_UPC UPC,
         CO.COND_NAME CONDI,
         BI.BIN_ID,
         BI.BIN_TYPE || '-' || BI.BIN_NO BIN_NAME,
         DECODE(ROW_NO,
                0,
                RC.RACK_NO,
                'W' || '' || WA.WAREHOUSE_NO || '-' || RC.RACK_NO || '-R' ||
                RA.ROW_NO) RACK_NAME,
                (SELECT DT.BARCODE_NO
            FROM LJ_BIN_VERIFY_DT DT
           WHERE DT.BARCODE_NO = B.BARCODE_NO
             AND ROWNUM = 1) VERIFIED_YN
    FROM LZ_BARCODE_MT    B,
         ITEMS_MT         I,
         LZ_SALESLOAD_DET DE,
         LZ_ITEM_COND_MT  CO,
         BIN_MT           BI,
         LZ_RACK_ROWS     RA,
         RACK_MT          RC,
         WAREHOUSE_MT     WA
   WHERE B.BARCODE_NO = $bar_no
     AND B.ITEM_ID = I.ITEM_ID
     AND B.SALE_RECORD_NO = DE.SALES_RECORD_NUMBER(+)
     AND B.BIN_ID = BI.BIN_ID(+)
     AND BI.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
     AND RA.RACK_ID = RC.RACK_ID
     AND RC.WAREHOUSE_ID = WA.WAREHOUSE_ID
     AND B.CONDITION_ID = CO.ID(+) ")->result_array();

                return array('bar_deta' => $bar_deta, 'bar_query' => true, 'status' => true); // if barcode valid
            } else {

                $bar_deta = $this->db->query("SELECT B.BARCODE_PRV_NO BARCODE_NO,
         'NULL' EBAY_ITEM_ID,
         'NULL' DESCR,
         'NULL' MANUF,
          NULL UPC,
          NULL MPN,
         CO.COND_NAME CONDI,
         BI.BIN_ID,
         BI.BIN_TYPE || '-' || BI.BIN_NO BIN_NAME,
         DECODE(ROW_NO,
                0,
                RC.RACK_NO,
                'W' || '' || WA.WAREHOUSE_NO || '-' || RC.RACK_NO || '-R' ||
                RA.ROW_NO) RACK_NAME,
                (SELECT DT.BARCODE_NO
            FROM LJ_BIN_VERIFY_DT DT
           WHERE DT.BARCODE_NO = B.BARCODE_PRV_NO
             AND ROWNUM = 1) VERIFIED_YN
    FROM LZ_DEKIT_US_DT  B,
         LZ_ITEM_COND_MT CO,
         BIN_MT          BI,
         LZ_RACK_ROWS    RA,
         RACK_MT         RC,
         WAREHOUSE_MT    WA
   WHERE B.BARCODE_PRV_NO = $barcode
     AND B.BIN_ID = BI.BIN_ID(+)
     AND BI.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
     AND RA.RACK_ID = RC.RACK_ID
     AND RC.WAREHOUSE_ID = WA.WAREHOUSE_ID
     AND B.CONDITION_ID = CO.ID(+)
   ")->result_array();

                return array('bar_deta' => $bar_deta, 'det_bar' => true, 'status' => false);
                //} // if barcode not valid
            }
        } // main if str length

    }

    public function updatePacking()
    {
        $packing_id = $this->input->post('packing');
        $packing_id = trim(str_replace("  ", ' ', $packing_id));
        $packing_id = str_replace(array("`,′"), "", $packing_id);
        $packing_id = str_replace(array("'"), "''", $packing_id);
        $packing_barcode = $this->input->post('barcode');
        $packing_barcode = trim(str_replace("  ", ' ', $packing_barcode));
        $packing_barcode = str_replace(array("`,′"), "", $packing_barcode);
        $packing_barcode = str_replace(array("'"), "''", $packing_barcode);
        $user_id = $this->input->post('userid');
        // var_dump($packing_id, $packing_barcode,$user_id); exit;
        date_default_timezone_set("America/Chicago");
        $date = date('Y-m-d H:i:s');
        $packing_date = "TO_DATE('" . $date . "', 'YYYY-MM-DD HH24:MI:SS')";

        $updatePacking = $this->db->query("UPDATE LZ_BARCODE_MT SET PACKING_ID = '$packing_id', PACKING_BY = '$user_id', PACKING_DATE = sysdate  WHERE BARCODE_NO = '$packing_barcode'");
        if ($updatePacking) {
            return array('status' => 1);
        } else {
            return array('status' => 0);
        }
    }

    public function get_packing_drop()
    {

        // var_dump($rack_id); exit;
        $qry = $this->db->query("SELECT PACKING_ID,
    PACKING_NAME FROM lz_packing_type_mt")->result_array();
        return $qry;
    }

    /********************************
     *   end Screen item packing
     *********************************/

    /********************************
     *   Start Screen Transfer Bin
     *********************************/

    public function Get_Bin_Detail()
    {
        $bin_name = strtoupper(trim($this->input->post('bin_name')));
        $bin_name = trim(str_replace("  ", ' ', $bin_name));
        $bin_name = trim(str_replace(array("'"), "''", $bin_name));
        //var_dump($bin_name); exit;
        // $bind_ids = $this->db->query("SELECT CURRENT_RACK_ROW_ID RACK_ROW_ID, BIN_ID, 'W' || '' || WAREHOUSE_NO || '-' || RACK_NO || ' | ' || DECODE(ROW_NO, NULL, NULL, 0, 'N' || '' || ROW_NO, 'R' || '' || ROW_NO) || ' | ' || BIN_NAME RACK_NAME FROM (SELECT B.BIN_ID, B.BIN_TYPE || '-' || B.BIN_NO BIN_NAME, B.CURRENT_RACK_ROW_ID, RA.RACK_NO, MT.WAREHOUSE_NO, R.ROW_NO FROM BIN_MT B, LZ_RACK_ROWS R, RACK_MT RA, WAREHOUSE_MT MT WHERE B.CURRENT_RACK_ROW_ID = R.RACK_ROW_ID AND RA.WAREHOUSE_ID = MT.WAREHOUSE_ID AND R.RACK_ID = RA.RACK_ID) WHERE UPPER(BIN_NAME) = '$bin_name'")->result_array();
        //  if (count($bind_ids) >0) {
        //   $bin_id    = $bind_ids[0]['BIN_ID'];
        //   if (!empty($bin_id)) {
        //     $transfers = $this->db->query("SELECT B.BARCODE_NO BARCOD, B.BIN_ID, I.ITEM_DESC DESCR, I.ITEM_MT_UPC  UPC, I.ITEM_MT_MFG_PART_NO MPN, MAX_BAR.TRANS_BY_ID TRANS_BY_ID,  TO_CHAR(MAX_BAR.TRANS_DATE_TIME, 'MM-DD-YYYY HH24:MI:SS') AS TRANSFER_DATE FROM LZ_BARCODE_MT B, BIN_MT M, ITEMS_MT I, (SELECT LL.BARCODE_NO, M.FIRST_NAME ||' '||M.LAST_NAME TRANS_BY_ID, LL.TRANS_DATE_TIME FROM LZ_LOC_TRANS_LOG LL,EMPLOYEE_MT M WHERE LL.LOC_TRANS_ID IN (SELECT MAX(L.LOC_TRANS_ID) FROM LZ_LOC_TRANS_LOG L GROUP BY L.BARCODE_NO) AND LL.TRANS_BY_ID = M.EMPLOYEE_ID) MAX_BAR WHERE B.BIN_ID = M.BIN_ID AND B.ITEM_ID = I.ITEM_ID AND B.BARCODE_NO = MAX_BAR.BARCODE_NO(+) AND B.BIN_ID = $bin_id")->result_array();
        //   }else{
        //     $transfers = [];
        //   }

        //  }else{
        //   $transfers = [];
        //  }
        $bind_ids = $this->db->query("SELECT CURRENT_RACK_ROW_ID RACK_ROW_ID,
BIN_ID,
'W' || '' || WAREHOUSE_NO || '-' || RACK_NO || ' | ' ||
DECODE(ROW_NO,
NULL,
NULL,
0,
'N' || '' || ROW_NO,
'R' || '' || ROW_NO) || ' | ' || BIN_NAME RACK_NAME,
merchant_id,
current_rack_row_id
FROM (SELECT B.BIN_ID,
B.BIN_TYPE || '-' || B.BIN_NO BIN_NAME,
B.CURRENT_RACK_ROW_ID,
RA.RACK_NO,
--MT.WAREHOUSE_NO,
R.ROW_NO,
b.merchant_id,
case
when b.merchant_id = 1 and B.CURRENT_RACK_ROW_ID = 0 then
MT.WAREHOUSE_NO
else
get_warehouse.warehouse_id
end WAREHOUSE_NO
/*decode(B.CURRENT_RACK_ROW_ID = 0,MT.WAREHOUSE_NO,get_warehouse.warehouse_id)
get_warehouse.warehouse_id*/ /*,
b.current_rack_row_id*/
FROM BIN_MT B,
LZ_RACK_ROWS R,
RACK_MT RA,
WAREHOUSE_MT MT,
(select min(m.warehouse_no) warehouse_id,
min(m.merchant_id) merrch_id
from warehouse_mt m
where m.MERCHANT_ID =
(SELECT MERCHANT_ID
FROM BIN_MT BM
WHERE UPPER(BM.BIN_TYPE || '-' || BM.BIN_NO) = '$bin_name')) get_warehouse
WHERE B.CURRENT_RACK_ROW_ID = R.RACK_ROW_ID
AND RA.WAREHOUSE_ID = MT.WAREHOUSE_ID
and b.merchant_id = get_warehouse.merrch_id(+)
AND R.RACK_ID = RA.RACK_ID)
WHERE UPPER(BIN_NAME) = '$bin_name'")->result_array();
        if (count($bind_ids) > 0) {
            return array('status' => true, 'bin_detail' => $bind_ids);
        } else {
            return array('status' => false, 'bin_detail' => array(), 'message' => 'No Record Found');
        }

    }
    public function Update_Bin_Location()
    {

        $current_bin_id = $this->input->post('current_bin_id');
        $bin_remarks = $this->input->post('bin_remarks');
        $current_row_id = $this->input->post('current_row_id');
        $new_rack = $this->input->post('new_rack');

        $new_rack = trim($this->input->post('new_rack'));
        $new_rack = str_replace("  ", " ", $new_rack);
        $new_rack = str_replace("'", "''", $new_rack);

        $bin_remarks = trim($this->input->post('bin_remarks'));
        $bin_remark = str_replace("  ", " ", $bin_remarks);
        $bin_remarks = str_replace("'", "''", $bin_remark);
        date_default_timezone_set("America/Chicago");
        $date = date('Y-m-d H:i:s');
        $transfer_date = "TO_DATE('" . $date . "', 'YYYY-MM-DD HH24:MI:SS')";
        $transfer_by_id = $this->input->post('user_id');

        $bindId = $this->db->query("SELECT BIN_ID,BIN_NAME FROM (SELECT B.BIN_ID, DECODE(B.BIN_TYPE, 'TC', 'TC', 'PB', 'PB', 'WB', 'WB', 'AB', 'AB', 'DK', 'DK', 'NA', 'NA') || '-' || B.BIN_NO BIN_NAME FROM BIN_MT B) WHERE BIN_ID = $current_bin_id ")->result_array();

        if (count($bindId) > 0) {
            $new_bin_id = $bindId[0]['BIN_ID'];

            $rack_na = $this->db->query(" SELECT * FROM (SELECT RO.RACK_ROW_ID, 'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO || '-' || 'R' || RO.ROW_NO RACK_NAME FROM RACK_MT RA, WAREHOUSE_MT WA, LZ_RACK_ROWS RO WHERE /*RA.RACK_ID = 10000000013 AND */RA.WAREHOUSE_ID = WA.WAREHOUSE_ID AND RA.RACK_ID = RO.RACK_ID AND RO.RACK_ROW_ID <> 0) WHERE RACK_NAME  = '$new_rack' ")->result_array();
            //  $rack_row_id = $rack_na[0]['RACK_ROW_ID'];

            // var_dump($new_bin_id);
            // EXIT;
            //$bindId = $this->db->query("UPDATE LZ_BARCODE_MT SET BIN_ID = '$new_bin_id' WHERE BARCODE_NO = $current_barcode");
            if (!empty($rack_na)) {
                $rack_row_id = $rack_na[0]['RACK_ROW_ID'];
                $qry = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_BIN_TRANS_LOG','BIN_TRANS_LOG_ID') ID FROM DUAL");
                $rs = $qry->result_array();
                $bin_trans_id = $rs[0]['ID'];
                $updateBin = $this->db->query("INSERT INTO LZ_BIN_TRANS_LOG (BIN_TRANS_LOG_ID, BIN_ID,OLD_RACK_ROW_ID,NEW_RACK_ROW_ID,TRANSFER_DATE,TRANSFER_BY,REMARKS) VALUES($bin_trans_id, $current_bin_id,
      '$current_row_id',$rack_row_id,$transfer_date,$transfer_by_id,'$bin_remarks')");
                if ($updateBin) {
                    $this->db->query(" UPDATE BIN_MT BK SET BK.CURRENT_RACK_ROW_ID = $rack_row_id WHERE BK.BIN_ID  =$new_bin_id  ");

                    return array('status' => true, 'message' => 'Location Updated Successfully');
                } else {
                    return array('status' => false, 'message' => 'Location Not Updated Successfully');
                }

            } else {
                return array('status' => false, 'message' => 'No Rows In This Rack');
            }
        } else {
            return array('status' => false, 'message' => 'No Bin Found ' . $current_bin_id . '');

        }
    }
    //end by danish on 3-3-2018

    public function Get_Rack_Detail()
    {
        $rack_name = strtoupper(trim($this->input->post('rack_name')));
        $rack_name = str_replace("  ", " ", $rack_name);
        $rack_name = str_replace("'", "''", $rack_name);

        $qry = $this->db->query("SELECT *
        from (SELECT 'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO RACK_NAME,
                     ro.row_no,
                     ro.rack_row_id,
                     bin_count.bins_per_row
                FROM RACK_MT RA,
                     WAREHOUSE_MT WA,
                     lz_rack_rows ro,
                     (select count(j.bin_id) bins_per_row,
                             j.current_rack_row_id current_rack_row_id
                        from bin_mt j
                       group by j.current_rack_row_id) bin_count
               WHERE /*ra.RACK_ID = 68
                 AND*/ RA.WAREHOUSE_ID = WA.WAREHOUSE_ID
                 and ra.rack_id = ro.rack_id
                 and ro.rack_row_id = bin_count.current_rack_row_id(+)
                 order by  ro.rack_row_id asc)
       where upper(rack_name) = '$rack_name'");
        if ($qry->num_rows() > 0) {
            $data = $qry->result_array();
            $rack_detail = [];
            foreach ($data as $rack_data) {
                $rack_row_id = $rack_data['RACK_ROW_ID'];
                $rack_row_count = $rack_data['BINS_PER_ROW'];
                $rack_name = $rack_data['RACK_NAME'];
                $row_no = $rack_data['ROW_NO'];

                $row_detail = $this->db->query("SELECT /*B.PRINT_STATUS,*/
                B.BIN_ID,
                B.BIN_TYPE || '-' ||B.BIN_NO BIN_NAME ,
                --B.BIN_NO || '-' || B.BIN_TYPE BIN_NAME,
                /*DECODE(BIN_TYPE,
                'NA',
                'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO || '-' ||
                B.BIN_TYPE || '-' || B.BIN_NO,
                B.BIN_TYPE || '-' || B.BIN_NO) BIN_NO,*/
                B.BIN_TYPE,
                MM.BUISNESS_NAME
                FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA, LZ_MERCHANT_MT MM
                WHERE BIN_TYPE NOT IN ('No Bin')
                AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
                AND RA.RACK_ID = M.RACK_ID
                AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID
                AND MM.MERCHANT_ID = B.MERCHANT_ID
                and b.current_rack_row_id = '$rack_row_id'")->result_array();
                // foreach($row_detail as $bin_detail){
                $rack_detail[] = array(
                    'binsCount' => $rack_row_count,
                    'rankName' => $rack_name,
                    'rowNo' => $row_no,
                    'bins' => $row_detail,
                );

                // }

            }
            return $rack_detail;
        } else {
            header('HTTP/1.1 422 Unprocessable Entity');
            //header( 'HTTP/1.1 422 BAD REQUEST' );
            return array('status' => false, 'message' => 'Invalid Rack Name');
        }
    }
public function get_rack_app()
    {
        $rack_name = strtoupper(trim($this->input->post('rack_name')));
        $rack_name = str_replace("  ", " ", $rack_name);
        $rack_name = str_replace("'", "''", $rack_name);//W1-K1-R1
        $rackqry = "SELECT RR.RACK_ROW_ID
  FROM RACK_MT RA, WAREHOUSE_MT WA, LZ_MERCHANT_MT MM , lz_rack_rows rr 
 WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID
   AND WA.MERCHANT_ID = MM.MERCHANT_ID
   and rr.rack_id = ra.rack_id
    and 'W'||upper(wa.warehouse_no)||'-'||upper(ra.rack_no) ||'-R'||upper(rr.row_no) = '$rack_name'";
    $rackqry = $this->db->query($rackqry)->result_array();
    if(count($rackqry) > 0){
        return array('status' => true, 'RACK_ROW_ID' => $rackqry[0]['RACK_ROW_ID']);
    }else{
        return array('status' => false, 'RACK_ROW_ID' => '');
    }
        
    }
    public function Update_Bin_Location_app()
    {

        $bin_name = $this->input->post('bin_name');
        //$bin_remarks = $this->input->post('bin_remarks');
        $rack_row_id = $this->input->post('rack_row_id');
        //$new_rack = $this->input->post('new_rack');

        // $new_rack = trim($this->input->post('new_rack'));
        // $new_rack = str_replace("  ", " ", $new_rack);
        // $new_rack = str_replace("'", "''", $new_rack);

        $bin_remarks = trim($this->input->post('bin_remarks'));
        $bin_remark = str_replace("  ", " ", $bin_remarks);
        $bin_remarks = str_replace("'", "''", $bin_remark);
        date_default_timezone_set("America/Chicago");
        $date = date('Y-m-d H:i:s');
        $transfer_date = "TO_DATE('" . $date . "', 'YYYY-MM-DD HH24:MI:SS')";
        $transfer_by_id = $this->input->post('user_id');

        $bindId = $this->db->query("SELECT BIN_ID, BIN_NAME, CURRENT_RACK_ROW_ID
  FROM (SELECT B.BIN_ID,
                      upper(B.BIN_TYPE) || '-' || B.BIN_NO BIN_NAME,
               CURRENT_RACK_ROW_ID
          FROM BIN_MT B)
 WHERE upper(BIN_NAME)= upper('$bin_name') ")->result_array();

        if (count($bindId) > 0) {
            $new_bin_id = $bindId[0]['BIN_ID'];
            $current_row_id = $bindId[0]['CURRENT_RACK_ROW_ID'];

           // $rack_na = $this->db->query(" SELECT * FROM (SELECT RO.RACK_ROW_ID, 'W' || '' || WA.WAREHOUSE_NO || '-' || RA.RACK_NO || '-' || 'R' || RO.ROW_NO RACK_NAME FROM RACK_MT RA, WAREHOUSE_MT WA, LZ_RACK_ROWS RO WHERE /*RA.RACK_ID = 10000000013 AND */RA.WAREHOUSE_ID = WA.WAREHOUSE_ID AND RA.RACK_ID = RO.RACK_ID AND RO.RACK_ROW_ID <> 0) WHERE RACK_NAME  = '$new_rack' ")->result_array();
            //  $rack_row_id = $rack_na[0]['RACK_ROW_ID'];

            // var_dump($new_bin_id);
            // EXIT;
            //$bindId = $this->db->query("UPDATE LZ_BARCODE_MT SET BIN_ID = '$new_bin_id' WHERE BARCODE_NO = $current_barcode");
            //if (!empty($rack_na)) {
                //$rack_row_id = $rack_na[0]['RACK_ROW_ID'];
                $qry = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_BIN_TRANS_LOG','BIN_TRANS_LOG_ID') ID FROM DUAL");
                $rs = $qry->result_array();
                $bin_trans_id = $rs[0]['ID'];
                $updateBin = $this->db->query("INSERT INTO LZ_BIN_TRANS_LOG (BIN_TRANS_LOG_ID, BIN_ID,OLD_RACK_ROW_ID,NEW_RACK_ROW_ID,TRANSFER_DATE,TRANSFER_BY,REMARKS) VALUES($bin_trans_id, $new_bin_id,
      '$current_row_id',$rack_row_id,$transfer_date,$transfer_by_id,'$bin_remarks')");
                if ($updateBin) {
                    $this->db->query(" UPDATE BIN_MT BK SET BK.CURRENT_RACK_ROW_ID = '$rack_row_id' WHERE BK.BIN_ID  =$new_bin_id  ");

                    return array('status' => true, 'message' => 'Location Updated Successfully');
                } else {
                    return array('status' => false, 'message' => 'Location Not Updated Successfully');
                }

            // } else {
            //     return array('status' => false, 'message' => 'No Rows In This Rack');
            // }
        } else {
            return array('status' => false, 'message' => 'No Bin Found | ' . $bin_name . '');

        }
    }

    public function FilterBinMobile()
    {

        $bin_type_search = $this->input->post('Bin_type'); //ID
    
        $print_sta = $this->input->post('PrintStatus');

        $merchant_id = $this->input->post('merchant_id');
        // if (!empty($merchant_id)) {
        //     $merchant_id = $merchant_id['value'];
        // }
        // var_dump($bin_type_search, $print_sta);
        // exit;
        // $qry = $this->db->query("SELECT WAREHOUSE_NO,WAREHOUSE_DESC,LOCATION FROM WAREHOUSE_MT order by WAREHOUSE_ID asc ")->result_array();

        // $rackqry = $this->db->query("SELECT RA.RACK_ID,RA.RACK_NO,WA.WAREHOUSE_DESC,RA.WIDTH,RA.HEIGHT,RA.NO_OF_ROWS,DECODE(RA.RACK_TYPE,1,'CAGE',2,'RACK') RACK_TYPE FROM RACK_MT RA ,WAREHOUSE_MT WA WHERE RA.WAREHOUSE_ID = WA.WAREHOUSE_ID and RA.RACK_ID <>0 ORDER BY RACK_ID ASC ")->result_array();

        $binqry = "SELECT B.PRINT_STATUS,
        B.BIN_ID,
        B.BIN_NO || '-' || B.BIN_TYPE BIN_NAME,
        DECODE(BIN_TYPE,
               'NA',
               'W' || WA.WAREHOUSE_NO || '-' || M.RACK_NO || '-R' || ROW_NO || '-' ||
               B.BIN_TYPE || '-' || B.BIN_NO,
               B.BIN_TYPE || '-' || B.BIN_NO) BIN_NO,
               B.BIN_TYPE,
               MM.BUISNESS_NAME
    FROM BIN_MT B, LZ_RACK_ROWS RA, RACK_MT M, WAREHOUSE_MT WA, LZ_MERCHANT_MT MM
    WHERE BIN_TYPE NOT IN ('No Bin')
    AND B.CURRENT_RACK_ROW_ID = RA.RACK_ROW_ID
    AND RA.RACK_ID = M.RACK_ID
    AND M.WAREHOUSE_ID = WA.WAREHOUSE_ID
    AND MM.MERCHANT_ID = B.MERCHANT_ID ";

        if (!empty($bin_type_search) && $bin_type_search !== 'ALL') {

            $binqry .= " AND  B.BIN_TYPE = '$bin_type_search' ";
        }
        if (!empty($print_sta)) {
            if ($print_sta != 0) {
                $binqry .= " AND  B.PRINT_STATUS = '$print_sta'";
            }
        }
        if (!empty($merchant_id)) {
            $binqry .= " AND B.MERCHANT_ID = '$merchant_id'";
        }

        $binqry .= " ORDER BY BIN_TYPE,BIN_NO ";

        $binqry = $this->db->query($binqry)->result_array();

        // var_dump($this->db->last_query());
        // exit;

        return $binqry;
    }
    public function qc_barcode_detail()
    {
        $barcode_no = trim($this->input->post('barcode_no'));
        $user_id = trim($this->input->post('user_id'));

        $barcode_detail = $this->db->query("SELECT DT.BARCODE_NO,
                   DT.BARCODE_NO FOLDER_NAME,
                   DT.MPN,
                   DT.UPC,
                   CD.COND_NAME CONDITION,
                   CD.ID CONDITION_ID,
                   DT.REMARKS,
                   'LZ_MERCHAT_BARCODE_DT' BARCODE_FROM,
                   DT.SERIAL_NO,
                   DT.IMEI,
                   DT.SERVICE_CHARGES,
                   DT.SERVICE_REMARKS
              FROM LZ_MERCHANT_BARCODE_DT DT,
                   LZ_ITEM_COND_MT  CD
             WHERE DT.COND_ID = CD.ID(+)
               AND DT.BARCODE_NO = '$barcode_no'
               AND ROWNUM = 1")->result_array();

        $user_det = $this->db->query("SELECT E.EMPLOYEE_ID FROM EMPLOYEE_MT E WHERE E.EMPLOYEE_ID = '$user_id' AND E.ROLE_ID = 1")->result_array();
        $service_charges_enabled = false;
        if(count($user_det) > 0){
            $service_charges_enabled = true;
        }
        $barcode_detail[0]['SERVICE_CHARGES_ENABLED']=$service_charges_enabled;
        //array_push($barcode_detail, $service_charges_enabled);
        //var_dump($barcode_detail);exit;
        if (count($barcode_detail) > 0) {
            return $barcode_detail[0];
        } else {
            header('HTTP/1.1 422 Unprocessable Entity');
            //header( 'HTTP/1.1 422 BAD REQUEST' );
            return array('status' => false,
                'message' => 'Barcode Not Found',
                'data' => '');
        }
    }

    public function qc_barcode_detail_update()
    {
        $barcode_no_arr = json_decode(trim($this->input->post('barcode_no')));
        //$barcode_no = ["256868","256868","256868"];
        //var_dump($barcode_no_arr);exit;
        $barcode_no = implode(',', $barcode_no_arr);
         $update_status = $this->db->query("SELECT BARCODE_NO FROM LZ_MERCHANT_BARCODE_DT WHERE BARCODE_NO in ($barcode_no)")->result_array();
         $check_arr=array();
         foreach ($update_status as $value) {
             array_push($check_arr, $value['BARCODE_NO']);
         }
         //var_dump($check_arr);exit;
         $result=array_diff($barcode_no_arr,$check_arr);
         //var_dump($update_status,$result);exit;
         if(count($result) > 0){
            $count_barcode = count($result);
            $result = implode(',', $result);
             header('HTTP/1.1 422 Unprocessable Entity.');
            return array('status' => false,
                'message' => $count_barcode.' Barcode Invalid :'.$result);
         }

        //var_dump($barcode_no,$tags);exit;

        $condition_id = trim($this->input->post('condition_id'));
        $upc = trim($this->input->post('upc')); 
        $mpn = trim($this->input->post('mpn'));
        $mpn = trim(str_replace(array("'"), "''", $mpn));
        $serial_no = trim($this->input->post('serial'));
        $imei = trim($this->input->post('imei'));
        $remarks = trim($this->input->post('remarks'));
        $remarks = trim(str_replace(array("'"), "''", $remarks));
        $updated_by = trim($this->input->post('user_id'));
        $service_charges = trim($this->input->post('service_charges'));
        $service_remarks = trim($this->input->post('service_remarks'));
        $service_remarks = trim(str_replace(array("'"), "''", $service_remarks));

        $update_status = $this->db->query("UPDATE LZ_MERCHANT_BARCODE_DT SET REMARKS = '$remarks' , UPC = '$upc' , MPN = '$mpn' , COND_ID = '$condition_id' , UPDATED_BY = '$updated_by' , UPDATED_AT = sysdate , imei = '$imei' ,SERIAL_NO = '$serial_no', SERVICE_CHARGES = '$service_charges' , SERVICE_REMARKS = '$service_remarks' WHERE BARCODE_NO in ($barcode_no)");
        if ($this->db->affected_rows() > 0) {
            return array('status' => true,
                'message' => 'Success! Barcode Updated.');
        } else {
            header('HTTP/1.1 422 Unprocessable Entity.');
            return array('status' => false,
                'message' => 'Error! Barcode Not Updated.');
        }
    }
}
