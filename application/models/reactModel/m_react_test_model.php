<?php
class M_react_test_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
    }

    /********************************
     *  start Screen services
     *********************************/
    public function getData()
    {
        $get_data = $this->db->query("SELECT CASE
WHEN DE.SALES_RECORD_NUMBER IS NOT NULL AND
DE.TRACKING_NUMBER IS NOT NULL THEN
'SOLD || SHIPPED'
WHEN DE.SALES_RECORD_NUMBER IS NOT NULL AND
DE.TRACKING_NUMBER IS NULL THEN
'SOLD || NOT SHIPPED'
ELSE
'AVAILABLE'
END SOLD_STAT,
LS.SEED_ID,
LS.LZ_MANIFEST_ID,
E.STATUS,
E.LISTER_ID,
E.LIST_ID,
TO_CHAR(E.LIST_DATE, 'MM-DD-YYYY HH24:MI:SS') AS LIST_DATE,
E.LZ_SELLER_ACCT_ID,
LS.EBAY_PRICE,
LM.LOADING_NO,
LM.LOADING_DATE,
LM.PURCH_REF_NO,
I.ITEM_ID,
I.ITEM_CODE LAPTOP_ITEM_CODE,
LS.ITEM_TITLE ITEM_MT_DESC,
BM.BIN_TYPE BI_TYP,
I.ITEM_MT_MANUFACTURE MANUFACTURER,
I.ITEM_MT_UPC UPC,
I.ITEM_MT_MFG_PART_NO MFG_PART_NO,
BCD.CONDITION_ID ITEM_CONDITION,
BCD.EBAY_ITEM_ID,
1 QUANTITY,
BCD.BARCODE_NO,
BCD.BIN_ID,
BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME
FROM LZ_ITEM_SEED LS,
LZ_MANIFEST_MT LM,
ITEMS_MT I,
EBAY_LIST_MT E,
BIN_MT BM,
LZ_BARCODE_MT BCD,
LZ_SALESLOAD_DET DE
WHERE LS.ITEM_ID = I.ITEM_ID
AND E.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
AND E.ITEM_ID = I.ITEM_ID
AND E.SEED_ID = LS.SEED_ID
AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID
AND LS.ITEM_ID = BCD.ITEM_ID
AND LS.DEFAULT_COND = BCD.CONDITION_ID
AND BCD.SALE_RECORD_NO = DE.SALES_RECORD_NUMBER(+)
AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
AND E.EBAY_ITEM_ID = BCD.EBAY_ITEM_ID
and lm.manifest_type not in (3, 4)
AND BCD.EBAY_STICKER = 0
AND BCD.BIN_ID = BM.BIN_ID
ORDER BY LS.ITEM_ID DESC
FETCH FIRST 100 ROWS ONLY")->result_array();

        return $get_data;
        // return array("get_data"=>$get_data, "exist"=>true);
    }

    public function insertService($name, $created_by)
    {
        // $PACKING_NAME = $this->input->post('PACKING_NAME');
        $name = $this->input->post('name');
        $created_by = $this->input->post('created_by');
        $name = trim(str_replace("  ", ' ', $name));
        $name = trim(str_replace(array("'"), "''", $name));
        $qr = "SELECT service_desc from lj_services where service_desc = '$name'";
        $result = $this->db->query($qr)->result_array();
        if ($result) {
            $data = array("status" => false, "response" => "Your Service Name Already Exist", "data" => $result);
        } else {
            $get_User = $this->db->query("INSERT INTO lj_services (service_id ,service_desc,created_date,created_by)
values (get_single_primary_key('lj_services','service_id'),'$name',sysdate,'$created_by')");

            $services = $this->db->query("SELECT l.service_id,l.service_desc, TO_CHAR(l.created_date, 'DD-MM-YYYY HH24:MI:SS') created_date, B.USER_NAME, l.CREATED_BY FROM LJ_SERVICES l,EMPLOYEE_MT B where l.SERVICE_DESC = '$name' and B.EMPLOYEE_ID = l.CREATED_BY order by service_id DESC ")->result_array();
            $service_desc = $services[0]['SERVICE_DESC'];
            $data = array("status" => true, "response" => $get_User, "services" => $services, "service_desc" => $service_desc);
        }
        return $data;
    }

    public function getService()
    {
        $get_user_from_database = $this->db->query("SELECT A.service_id, A.service_desc, TO_CHAR(A.created_date, 'DD-MM-YYYY HH24:MI:SS') created_date ,A.created_by , B.USER_NAME from lj_services A, EMPLOYEE_MT B WHERE B.EMPLOYEE_ID = A.CREATED_BY order by service_id DESC")->result_array();
        return $get_user_from_database;
    }

    public function deleteService()
    {
        $SERVICE_ID = $this->input->post('SERVICE_ID');
        $delData = $this->db->query("DELETE lj_services where SERVICE_ID='$SERVICE_ID'");
        return $delData;
    }
    /********************************
     *   end Screen services
     *********************************/

    /********************************
     *   start Screen services rate
     *********************************/
    public function insertServiceRate()
    {
        // $name = $this->input->post('name');
        // $created_by = $this->input->post('userId'); lj_service_rate

        $selectServiceName = $this->input->post('selectServiceName');
        $selectServiceName = trim(str_replace("  ", ' ', $selectServiceName));
        $selectServiceName = trim(str_replace(array("'"), "''", $selectServiceName));

        $service_type = $this->input->post('service_type');
        $service_Charges = $this->input->post('service_Charges');
        $created_by = $this->input->post('created_by');

        $Set_data = $this->db->query("INSERT INTO lj_service_rate A ( a.ser_rate_id,a.service_id, A.service_type, A.Charges, A.created_by, A.created_date)
values (get_single_primary_key('lj_service_rate','SER_RATE_ID'), '$selectServiceName', '$service_type','$service_Charges','$created_by',sysdate)");
        $tableData = $Set_data;
        $get_data = $this->db->query("SELECT ser_rate_id FROM lj_service_rate where ser_rate_id = (SELECT max(ser_rate_id) from lj_service_rate)")->result_array();
        //$data = $this->db->query($get_data)->;
        $tableData = $get_data[0]['SER_RATE_ID'];
        // print_r($tableData);
        // exit;
        // $tableData = $this->db->query(" SELECT A.ser_rate_id , A.service_id, A.service_type, A.Charges, A.created_by, TO_CHAR(A.created_date, 'DD-MM-YYYY HH24:MI:SS') created_date, c.service_desc, B.USER_NAME from lj_services c, lj_service_rate A , EMPLOYEE_MT B WHERE B.EMPLOYEE_ID = A.CREATED_BY and c.service_id=A.service_id and A.SER_RATE_ID = '$tableData'")->result_array();
        $tableData = $this->db->query(" SELECT A.ser_rate_id , A.service_id, A.service_type, '$' || A.Charges Charges, A.created_by, TO_CHAR(A.created_date, 'DD-MM-YYYY HH24:MI:SS') created_date, c.service_desc, B.USER_NAME from lj_services c, lj_service_rate A , EMPLOYEE_MT B WHERE B.EMPLOYEE_ID = A.CREATED_BY and c.service_id=A.service_id and A.SER_RATE_ID = '$tableData' order by ser_rate_id DESC ")->result_array();
        return array("status" => true, "tableData" => $tableData);
        // return array("status"=>true, "tableData" => $tableData);

    }
    // public function getServiceRate()
    // {
    //     $tableData = $this->db->query(" SELECT A.ser_rate_id , A.service_id, A.service_type, '$' || A.Charges Charges, A.created_by, TO_CHAR(A.created_date, 'DD-MM-YYYY HH24:MI:SS') created_date, c.service_desc, B.USER_NAME from lj_services c, lj_service_rate A , EMPLOYEE_MT B WHERE B.EMPLOYEE_ID = A.CREATED_BY and c.service_id=A.service_id order by ser_rate_id DESC ")->result_array();
    //     return array("status" => true, "tableData" => $tableData);
    // }
    public function getServiceRate()
    {
        $tableData = $this->db->query("SELECT s.service_id,
               r.ser_rate_id,
               s.service_desc,
               r.service_type,

               decode(r.service_type,
                      '1',
                      'per barcde',
                      '2',
                      'hourly',
                      '3',
                      'per_order',
                      '4',
                      'per_bin',
                      '5',
                      'per_pallet',
                      '6',
                      'per_cubic_feet') ser_type,
                      '$' || r.charges rate,
                      '$' || r.excess_qty_rate excess_qty_rate,
               r.created_date
          from lj_services s, lj_service_rate r
         where s.service_id = r.service_id
         order by s.service_id asc")->result_array();
        return array("status" => true, "tableData" => $tableData);
    }

    public function deleteServiceRate()
    {
        //$id=$this->input->post('service_id');
        $ser_rate_id = $this->input->post('ser_rate_id');
        $delData = $this->db->query("DELETE lj_service_rate where ser_rate_id='$ser_rate_id' ");
        return $delData;
    }

    public function upDateSerViceRate()
    {

        $ser_rate_id = $this->input->post('id');
        $service_Charges = $this->input->post('service_Charges');

        $Update_data = $this->db->query("UPDATE lj_service_rate set CHARGES='$service_Charges' where SER_RATE_ID='$ser_rate_id'");
        return $Update_data;
    }

    /********************************
     *   end Screen services rate
     *********************************/

    //data from lz_packing_type_mt

    /********************************
     *   start Screen packing and order packing
     *********************************/

    public function InsertPacking()
    {
        // var_dump($PACKING_BY);

        // exit;
        // $value = json_decode(file_get_contents('php://input'), true);
        // if(!empty($value)){
        // //var_dump($value);exit;
        // foreach( $Data as $value){
        $PACKING_NAME = $this->input->post('PACKING_NAME');
        // $value['PACKING_NAME'];
        $PACKING_TYPE = $this->input->post('PACKING_TYPE');
        // $value['PACKING_TYPE'];
        $PACKING_CODE = $this->input->post('PACKING_CODE');
        // $value['PACKING_CODE'];
        $PACKING_LENGTH = $this->input->post('PACKING_LENGTH');
        // $value['PACKING_LENGTH'];
        $PACKING_WIDTH = $this->input->post('PACKING_WIDTH');
        // $value['PACKING_WIDTH'];
        $PACKING_HEIGTH = $this->input->post('PACKING_HEIGTH');
        // $value['PACKING_HEIGTH'];
        $PACKING_WEIGTH = $this->input->post('PACKING_WEIGTH');
        // $value['PACKING_WEIGTH'];
        $PACKING_BY = $this->input->post('created_by');
        // $value['created_by'];
        $PACKING_COST = $this->input->post('PACKING_COST');
        // $value['PACKING_COST'];

        //}
        //var_dump($PACKING_LENGTH);exit;
        $PACKING_NAME = trim(str_replace(" ", ' ', $PACKING_NAME));
        $PACKING_NAME = trim(str_replace(array("'"), "''", $PACKING_NAME));

        $insertPackinData = $this->db->query("INSERT INTO lz_packing_type_mt (PACKING_ID,PACKING_NAME, PACKING_TYPE, PACKING_CODE, PACKING_LENGTH ,PACKING_WIDTH, PACKING_HEIGTH, PACKING_WEIGTH, PACKING_DATE, PACKING_BY, PACKING_COST)
values (get_single_primary_key('lz_packing_type_mt','PACKING_ID'),'$PACKING_NAME', '$PACKING_TYPE' ,'$PACKING_CODE', '$PACKING_LENGTH' , '$PACKING_WIDTH' , '$PACKING_HEIGTH', '$PACKING_WEIGTH',sysdate, '$PACKING_BY' , '$PACKING_COST')");
        $getPackingData = $insertPackinData;
        $get_data = $this->db->query("SELECT PACKING_ID FROM lz_packing_type_mt where PACKING_ID = (SELECT max(PACKING_ID) from lz_packing_type_mt)")->result_array();
        //$data = $this->db->query($get_data)->;
        $getPackingData = $get_data[0]['PACKING_ID'];
        // print_r($tableData);
        // var_dump($getPackingData); exit;
        $getPackingData = $this->db->query("SELECT p.packing_id,
p.packing_name||'|'||p.packing_type ||'|'||
p.packing_length||'x'||p.packing_width ||'x'||
p.packing_heigth packing_name
from lz_packing_type_mt p WHERE p.PACKING_ID ='$getPackingData'")->result_array();

        //$getPackingData = $this->db->query("SELECT PACKING_ID,PACKING_NAME, PACKING_TYPE, PACKING_CODE, PACKING_LENGTH ,PACKING_WIDTH, PACKING_HEIGTH, PACKING_WEIGTH, TO_CHAR(PACKING_DATE, 'DD-MM-YYYY HH24:MI:SS') PACKING_DATE, PACKING_BY, '$' || PACKING_COST PACKING_COST, B.USER_NAME FROM lz_packing_type_mt, EMPLOYEE_MT B WHERE B.EMPLOYEE_ID = PACKING_BY and PACKING_ID = '$getPackingData' ORDER BY PACKING_ID DESC")->result_array();
        return array("status" => true, 'getPackingData' => $getPackingData);
    }

    public function InsertPacking22()
    {
        // var_dump($PACKING_BY);

        // exit;
        // $value = json_decode(file_get_contents('php://input'), true);
        // if(!empty($value)){
        // //var_dump($value);exit;
        // foreach( $Data as $value){
        $PACKING_NAME = $this->input->post('PACKING_NAME');
        // $value['PACKING_NAME'];
        $PACKING_TYPE = $this->input->post('PACKING_TYPE');
        // $value['PACKING_TYPE'];
        $PACKING_CODE = $this->input->post('PACKING_CODE');
        // $value['PACKING_CODE'];
        $PACKING_LENGTH = $this->input->post('PACKING_LENGTH');
        // $value['PACKING_LENGTH'];
        $PACKING_WIDTH = $this->input->post('PACKING_WIDTH');
        // $value['PACKING_WIDTH'];
        $PACKING_HEIGTH = $this->input->post('PACKING_HEIGTH');
        // $value['PACKING_HEIGTH'];
        $PACKING_WEIGTH = $this->input->post('PACKING_WEIGTH');
        // $value['PACKING_WEIGTH'];
        $PACKING_BY = $this->input->post('created_by');
        // $value['created_by'];
        $PACKING_COST = $this->input->post('PACKING_COST');
        // $value['PACKING_COST'];

        //}
        //var_dump($PACKING_LENGTH);exit;
        $PACKING_NAME = trim(str_replace(" ", ' ', $PACKING_NAME));
        $PACKING_NAME = trim(str_replace(array("'"), "''", $PACKING_NAME));

        $insertPackinData = $this->db->query("INSERT INTO lz_packing_type_mt (PACKING_ID,PACKING_NAME, PACKING_TYPE, PACKING_CODE, PACKING_LENGTH ,PACKING_WIDTH, PACKING_HEIGTH, PACKING_WEIGTH, PACKING_DATE, PACKING_BY, PACKING_COST)
values (get_single_primary_key('lz_packing_type_mt','PACKING_ID'),'$PACKING_NAME', '$PACKING_TYPE' ,'$PACKING_CODE', '$PACKING_LENGTH' , '$PACKING_WIDTH' , '$PACKING_HEIGTH', '$PACKING_WEIGTH',sysdate, '$PACKING_BY' , '$PACKING_COST')");
        $getPackingData = $insertPackinData;
        $get_data = $this->db->query("SELECT PACKING_ID FROM lz_packing_type_mt where PACKING_ID = (SELECT max(PACKING_ID) from lz_packing_type_mt)")->result_array();
        //$data = $this->db->query($get_data)->;
        $getPackingData = $get_data[0]['PACKING_ID'];
        // print_r($tableData);
        // var_dump($getPackingData); exit;
        //         $getPackingData = $this->db->query("SELECT p.packing_id,
        // p.packing_name||'|'||p.packing_type ||'|'||
        // p.packing_length||'x'||p.packing_width ||'x'||
        // p.packing_heigth packing_name
        // from lz_packing_type_mt p WHERE p.PACKING_ID ='$getPackingData'")->result_array();

        $getPackingData = $this->db->query("SELECT PACKING_ID,PACKING_NAME, PACKING_TYPE, PACKING_CODE, PACKING_LENGTH ,PACKING_WIDTH, PACKING_HEIGTH, PACKING_WEIGTH, TO_CHAR(PACKING_DATE, 'DD-MM-YYYY HH24:MI:SS') PACKING_DATE, PACKING_BY, '$' || PACKING_COST PACKING_COST, B.USER_NAME FROM lz_packing_type_mt, EMPLOYEE_MT B WHERE B.EMPLOYEE_ID = PACKING_BY and PACKING_ID = '$getPackingData' ORDER BY PACKING_ID DESC")->result_array();
        return array("status" => true, 'getPackingData' => $getPackingData);
    }

    public function GetPacking()
    {
        $getPackingData = $this->db->query("SELECT PACKING_ID,PACKING_NAME, PACKING_TYPE, PACKING_CODE, PACKING_LENGTH ,PACKING_WIDTH, PACKING_HEIGTH, PACKING_WEIGTH, TO_CHAR(PACKING_DATE, 'DD-MM-YYYY HH24:MI:SS') PACKING_DATE, PACKING_BY, '$' || PACKING_COST PACKING_COST, B.USER_NAME FROM lz_packing_type_mt, EMPLOYEE_MT B WHERE B.EMPLOYEE_ID = PACKING_BY ORDER BY PACKING_ID DESC")->result_array();
        return array("status" => true, 'getPackingData' => $getPackingData);
    }

    public function DeletePacking()
    {
        $PACKING_ID = $this->input->post('PACKING_ID');
        $deletePack = $this->db->query("DELETE lz_packing_type_mt where PACKING_ID='$PACKING_ID'");
        return $deletePack;
    }

    public function UpdatePacking()
    {

        $PACKING_ID = $this->input->post('PACKING_ID');
        $PACKING_NAME = $this->input->post('PackingNameModel');
        $PACKING_TYPE = $this->input->post('radioModel');
        $PACKING_CODE = $this->input->post('PackingCodeNameModel');
        $PACKING_LENGTH = $this->input->post('LengthNameModel');
        $PACKING_WIDTH = $this->input->post('widthNameModel');
        $PACKING_HEIGTH = $this->input->post('HeightNameModel');
        $PACKING_WEIGTH = $this->input->post('PackingWeigthNameModel');
        $PACKING_COST = $this->input->post('PackingCostNameModel');

        $PACKING_NAME = trim(str_replace("  ", ' ', $PACKING_NAME));
        $PACKING_NAME = trim(str_replace(array("'"), "''", $PACKING_NAME));
        $updateData = $this->db->query(" UPDATE lz_packing_type_mt set PACKING_NAME='$PACKING_NAME', PACKING_TYPE='$PACKING_TYPE' , PACKING_CODE='$PACKING_CODE', PACKING_LENGTH='$PACKING_LENGTH' , PACKING_WIDTH='$PACKING_WIDTH' ,PACKING_HEIGTH='$PACKING_HEIGTH', PACKING_WEIGTH='$PACKING_WEIGTH' , PACKING_COST ='$PACKING_COST' where PACKING_ID='$PACKING_ID'");
        return $updateData;
    }

    public function PackingOrderDrop()
    {
        $getPackOrder = $this->db->query("SELECT p.packing_id,
-- p.packing_name||'|'||p.packing_type ||'|'||
p.packing_length||'x'||p.packing_width ||'x'||
p.packing_heigth ||'|'|| p.packing_name||'|'||p.packing_type  packing_name
from lz_packing_type_mt p")->result_array();
        return $getPackOrder;
    }

    public function MarchantDrop()
    {
        $getmarchantdrop = $this->db->query("SELECT MERCHANT_ID, BUISNESS_NAME from lz_merchant_mt")->result_array();
        return $getmarchantdrop;
    }

    public function GetPackingOrderDetail()
    {
        $MERCHANT_ID = $this->input->post('id');

        $getmarchantdrop = $this->db->query("SELECT max(d.sales_record_number) sales_record_number,
        d.order_id, max(i.item_id) ITEMS_ID,
        max(d.lz_salesload_det_id) lz_salesload_det_id,
        max(d.item_id) item_id,
        max(d.item_title) item_title,
        TO_CHAR(max(d.sale_date),'DD-MM-YYYY HH24:MI:SS') selling_date,
        max(d.quantity) quantity,
        '$' ||max(d.sale_price) sale_price,
         '$' || max(d.shippinglabelrate) postage,
        /*'$' || max(d.stamps_shipping_rate) postage,*/
        max(o.order_packing_id) order_packing_id,
        max(m.account_name) account_name,
        max(EM.USER_NAME) USER_NAME,
        max(d.shipping_service) shipping_service,
        max(i.item_length) ||'x'|| max(i.item_width) ||'x'||max(i.item_height) LWH,
        max(d.tracking_number) tracking_number,
          max((select '$ ' || round( sum(nvl(o.packing_cost,0)),2) from lj_order_packing_dt o where o.order_packing_id = dt.order_packing_id)) pack_cost,
       '$ ' || max(o.service_cost) service_cost,
       max(dt.packing_id) det_avail
        from ebay_list_mt e,
        lz_salesload_det d,
        lj_merhcant_acc_dt m,
        lj_order_packing_mt o,
        lj_order_packing_dt dt,
        EMPLOYEE_MT EM,
        items_mt i
        where d.item_id = e.ebay_item_id
        and o.PACKING_BY = EM.EMPLOYEE_ID(+)
        and e.lz_seller_acct_id = m.acct_id
        and d.order_id = o.order_id(+)
        and o.order_packing_id = dt.order_packing_id(+)
        and e.item_id = i.item_id
        and d.orderstatus = 'Completed'
        and d.order_id is not null
        and m.merchant_id = '$MERCHANT_ID'
        group by d.order_id ORDER BY d.order_id DESC")->result_array();
        return $getmarchantdrop;
    }

    public function UpDatePostage()
    {
        $POSTAGE = trim($this->input->post('POSTAGE'));

        // $value['PACKING_NAME'];
        $LZ_SALESLOAD_DET_ID = trim($this->input->post('LZ_SALESLOAD_DET_ID'));
        $SERVICE_COST = trim($this->input->post('SERVICE_COST'));

        $Data = $this->db->query("UPDATE lz_salesload_det set STAMPS_SHIPPING_RATE='$POSTAGE',SHIPPINGLABELRATE='$POSTAGE' where LZ_SALESLOAD_DET_ID='$LZ_SALESLOAD_DET_ID'");
        if ($Data) {
            $quer = $this->db->query("UPDATE LJ_ORDER_PACKING_MT M SET M.SHIPING_LABEL_RATE = '$POSTAGE',M.SERVICE_COST = '$SERVICE_COST' WHERE M.ORDER_ID IN (SELECT D.ORDER_ID FROM LZ_SALESLOAD_DET D WHERE D.LZ_SALESLOAD_DET_ID ='$LZ_SALESLOAD_DET_ID' )");
            return $Data;
        } else {
            $Data = false;
            return $Data;
        }
    }

    public function InsertPackingDetail()
    {
        $PACKING_ID = $this->input->post('PACKING_ID');
        //var_dump($PACKING_ID); exit;
        $PACKING_BY = $this->input->post('PACKING_BY');

        $ITEMS_ID = $this->input->post('ITEMS_ID');
        $QUANTITY = $this->input->post('QUANTITY');
        // var_dump($QUANTITY,$ITEMS_ID); exit;
        $ORDER_ID = $this->input->post('ORDER_ID');
        $MERCHANT_ID = $this->input->post('MERCHANT_ID');
        $PACKING_COST = $this->input->post('PACKING_COST');

        /*==========================================================
        = check same item order exist or not =
        ==========================================================*/
        $getSameOrder = $this->db->query("SELECT
d.order_id,
max(d.item_id) item_id,
max(d.item_title) item_title,
max(o.order_packing_id) order_packing_id
from ebay_list_mt e,
lz_salesload_det d,
lj_merhcant_acc_dt m,
lj_order_packing_mt o,
lj_order_packing_dt dt,
items_mt i
where d.item_id = e.ebay_item_id
and e.lz_seller_acct_id = m.acct_id
and d.order_id = o.order_id(+)
and o.order_packing_id = dt.order_packing_id(+)
and e.item_id = i.item_id
and d.orderstatus = 'Completed'
and d.order_id is not null
and m.merchant_id = '$MERCHANT_ID'
and i.item_id = '$ITEMS_ID'
and d.quantity = '$QUANTITY'

group by d.order_id")->result_array();

        //if(count($getSameOrder) > 0){
        foreach ($getSameOrder as $value) {
            $order_id_id = $value['ORDER_ID'];
            $ebay_id = $value['ITEM_ID'];
            $item_title = $value['ITEM_TITLE'];
            $item_title = trim(str_replace(" ", ' ', $item_title));
            $item_title = trim(str_replace(array("'"), "''", $item_title));
            $order_packing_id = $value['ORDER_PACKING_ID'];
            //var_dump($order_id_id, $ebay_id,$item_title, $order_packing_id); exit;

            /*======================================
            = update packing =
            ======================================*/

            if (empty($order_packing_id)) {
                $get_pk = $this->db->query("SELECT get_single_primary_key('lj_order_packing_mt','ORDER_PACKING_ID') PK from duAL")->result_array();
                $mt_pk = $get_pk[0]['PK'];
                $insertPackinData = $this->db->query("INSERT INTO lj_order_packing_mt (ORDER_PACKING_ID, ORDER_ID, MERCHANT_ID,SER_RATE_ID, PACKING_DATE, PACKING_BY,EBAY_ID, ITEM_TITLE) values ('$mt_pk', '$order_id_id', '$MERCHANT_ID' , '5',sysdate, '$PACKING_BY','$ebay_id','$item_title')");
            } else {
                $mt_pk = $order_packing_id;
            }

            foreach ($PACKING_ID as $value) {
                $pack_id = $value['value'];

                $insertPackinDetData = $this->db->query("INSERT INTO lj_order_packing_dt (ORDER_PACKING_DT_ID, ORDER_PACKING_ID, PACKING_ID, PACKING_COST) values (get_single_primary_key('lj_order_packing_dt','ORDER_PACKING_DT_ID') ,'$mt_pk', '$pack_id', (SELECT nvl(p.packing_cost,0) from lz_packing_type_mt p where p.packing_id ='$pack_id'))");
            }

            /*===== End of update packing ======*/
        } // end main foreach
        //}
        $total = count($PACKING_ID);
        $result = $this->db->query("select *
        from (SELECT d.*, p.packing_name || '|' || p.packing_type || '|' ||
                     p.packing_length || 'x' || p.packing_width || 'x' ||
                     p.packing_heigth packing_name
                from lj_order_packing_dt d, lz_packing_type_mt p
               where d.packing_id = p.packing_id

               order by d.order_packing_dt_id desc)
       where rownum <= $total")->result_array();

        $get_data = $this->db->query("SELECT ORDER_PACKING_DT_ID FROM lj_order_packing_dt where ORDER_PACKING_DT_ID = (SELECT max(ORDER_PACKING_DT_ID) from lj_order_packing_dt)")->result_array();
        //$data = $this->db->query($get_data)->;
        $getPackingData = $get_data[0]['ORDER_PACKING_DT_ID'];
        $data = $this->db->query("SELECT d.*, p.packing_id,
p.packing_name||'|'|| p.packing_type ||'|'||
p.packing_length||'x'||p.packing_width||'x'||
p.packing_heigth packing_name from lj_order_packing_dt d, lz_packing_type_mt p where d.packing_id = p.packing_id AND d.order_packing_dt_id ='$getPackingData'")->result_array();
        /*===== End of check same item order exist or not ======*/
        // var_dump($insertPackinDetData); exit;
        return array("status" => true, 'getPackingData' => $result, 'order_packing_id' => $data);
    }

    public function DetailInsertPackingName()
    {
        // $result = array();
        $PACKING_ID = $this->input->post('pakingId');
        $total = count($PACKING_ID);
        $ORDER_PACKING_ID = $this->input->post('ORDER_PACKING_ID');
        // var_dump($PACKING_ID[0]);
        for ($i = 0; $i < $total; $i++) {
            // var_dump($i);
            $PACKING_data = $PACKING_ID[$i]['value'];

            $quers = $this->db->query("SELECT NVL(OO.PACKING_COST,0) PCOST FROM LZ_PACKING_TYPE_MT OO WHERE OO.PACKING_ID  ='$PACKING_data'")->result_array();
            $cost = $quers[0]['PCOST'];
            // var_dump($PACKING_data);
            $data = $this->db->query("INSERT INTO lj_order_packing_dt (ORDER_PACKING_DT_ID,PACKING_ID, ORDER_PACKING_ID,PACKING_COST)
        values (get_single_primary_key('lj_order_packing_dt','ORDER_PACKING_DT_ID'),'$PACKING_data','$ORDER_PACKING_ID','$cost')");
        }

        $get_Data = $this->db->query("SELECT * FROM (SELECT ORDER_PACKING_DT_ID FROM lj_order_packing_dt
        ORDER BY ORDER_PACKING_DT_ID DESC ) WHERE ROWNUM <=$total ")->result_array();

        $result = $this->db->query("select *
            from (SELECT d.*, p.packing_name || '|' || p.packing_type || '|' ||
                         p.packing_length || 'x' || p.packing_width || 'x' ||
                         p.packing_heigth || '    cost(' ||
                        nvl(p.packing_cost, 0) || ')' packing_name
                    from lj_order_packing_dt d, lz_packing_type_mt p
                   where d.packing_id = p.packing_id

                   order by d.order_packing_dt_id desc)
           where rownum <= $total")->result_array();

        return $result;
    }

    public function UpDateDemension()
    {
        $LWH = $this->input->post('LWH');
        $ITEM_ID = $this->input->post('item_id');
        // $ENTERED_BY = $this->input->post('user_id');
        $LWH = (explode("x", $LWH));
        $total = count($LWH);
        $qr = "UPDATE items_mt SET ITEM_LENGTH = '$LWH[0]', ITEM_WIDTH = '$LWH[1]', ITEM_HEIGHT = '$LWH[2]' WHERE ITEM_ID = '$ITEM_ID'";
        $update = $this->db->query($qr);

        if ($update == true) {
            return array("status" => true, "message" => "Reacord Update");
        } else {
            return array("status" => false, "message" => "Not Update");
        }
    }
    public function DeleteListItem()
    {
        $ORDER_PACKING_DT_ID = $this->input->post('id');

        // var_dump($ORDER_PACKING_DT_ID);

        $deletelist = $this->db->query("SELECT d.order_packing_dt_id
from lj_order_packing_dt d
where d.order_packing_id in
     (select d.order_packing_id
        from lj_order_packing_dt d
       where d.order_packing_dt_id = '$ORDER_PACKING_DT_ID')")->result_array();
        //  var_dump($deletelist->num_rows());
        if (count($deletelist) > 1) {
            $deletelist = $this->db->query("DELETE lj_order_packing_dt where ORDER_PACKING_DT_ID='$ORDER_PACKING_DT_ID'");
            return array("status" => true, "remove" => false, 'deletelist' => $deletelist);
            //var_dump("det delete");
            // exit;
        } else {
            $get_mt_id = $this->db->query("select d.order_packing_id from lj_order_packing_dt d where d.order_packing_dt_id = '$ORDER_PACKING_DT_ID'")->result_array();
            $order_packing_id = @$get_mt_id[0]['ORDER_PACKING_ID'];
            $deletelist = $this->db->query("DELETE from lj_order_packing_dt where ORDER_PACKING_DT_ID='$ORDER_PACKING_DT_ID'");
            //$deletelist = $this->db->query("DELETE from lj_order_packing_mt m where m.order_packing_id ='$order_packing_id'");
            // var_dump("master + det delete");
            //exit;
            return array("status" => true, "remove" => true, 'deletelist' => $deletelist);
        }

        // return $deletelist;
    }

    public function ListViewPackingName()
    {
        $ORDER_PACKING_ID = $this->input->post('id');

        $data = $this->db->query("SELECT d.*, p.packing_id,
            p.packing_name||'|'||p.packing_type||'|'||
            p.packing_length||'x'||p.packing_width||'x'||
            p.packing_heigth || '    cost(' ||
       nvl(p.packing_cost, 0)  || ')' packing_name from lj_order_packing_dt d, lz_packing_type_mt p where d.packing_id = p.packing_id AND d.order_packing_id ='$ORDER_PACKING_ID'")->result_array();

        return $data;
    }

    /********************************
     *   end Screen packing and order packing
     *********************************/

    /********************************
     *   start Screen Tempdata
     *********************************/
    public function GetTempdata()
    {
        $get_data = $this->db->query("SELECT * from LZ_ITEM_TEMPLATE");
        //  var_dump( $get_data->result_array()); exit;
        return $get_data->result_array();
    }

    public function InsetTemplatedata()
    { // for template form
        $TEMPLATE_NAME = $this->input->post('TEMPLATE_NAME');
        $TEMPLATE_NAME = trim(str_replace("  ", ' ', $TEMPLATE_NAME));
        $EBAY_LOCAL = $this->input->post('EBAY_LOCAL');
        $SHIP_FROM_LOC = $this->input->post('SHIP_FROM_LOC');
        $CURRENCY = $this->input->post('CURRENCY');
        $LIST_TYPE = $this->input->post('LIST_TYPE');
        $SHIP_FROM_ZIP_CODE = $this->input->post('SHIP_FROM_ZIP_CODE');
        $PAYMENT_METHOD = $this->input->post('PAYMENT_METHOD');
        $PAYPAL_EMAIL = $this->input->post('PAYPAL_EMAIL');
        $DISPATCH_TIME_MAX = $this->input->post('DISPATCH_TIME_MAX');
        $SHIPPING_SERVICE = $this->input->post('SHIPPING_SERVICE');
        $SHIPPING_COST = $this->input->post('SHIPPING_COST');
        $ADDITIONAL_COST = $this->input->post('ADDITIONAL_COST');
        $RETURN_OPTION = $this->input->post('RETURN_OPTION');
        $RETURN_DAYS = $this->input->post('RETURN_DAYS');
        $SHIPPING_PAID_BY = $this->input->post('SHIPPING_PAID_BY');
        $ENTERED_BY = $this->input->post('ENTERED_BY');

        $ACCOUNT_ID = $this->input->post('ACCOUNT_ID');
        $MERCHANT_ID = $this->input->post('MERCHANT_ID');

        $temp_data = $this->db->query("INSERT INTO LZ_ITEM_TEMPLATE (TEMPLATE_ID, TEMPLATE_NAME, EBAY_LOCAL, CURRENCY, LIST_TYPE, SHIP_FROM_ZIP_CODE, PAYMENT_METHOD, SHIP_FROM_LOC,  PAYPAL_EMAIL, DISPATCH_TIME_MAX, SHIPPING_SERVICE, SHIPPING_COST, ADDITIONAL_COST, RETURN_OPTION, RETURN_DAYS, SHIPPING_PAID_BY, ENTERED_BY, MERCHANT_ID,ACCOUNT_ID)
      values (get_single_primary_key('LZ_ITEM_TEMPLATE','TEMPLATE_ID'),'$TEMPLATE_NAME', '$EBAY_LOCAL','$CURRENCY', '$LIST_TYPE','$SHIP_FROM_ZIP_CODE','$SHIP_FROM_LOC', '$PAYMENT_METHOD', '$PAYPAL_EMAIL', '$DISPATCH_TIME_MAX', '$SHIPPING_SERVICE', '$SHIPPING_COST', '$ADDITIONAL_COST', '$RETURN_OPTION', '$RETURN_DAYS', '$SHIPPING_PAID_BY', '$MERCHANT_ID','$ACCOUNT_ID', '$ENTERED_BY')");
        $tableData = $temp_data;
        $get_data = $this->db->query("SELECT TEMPLATE_ID FROM LZ_ITEM_TEMPLATE where TEMPLATE_ID = (SELECT max(TEMPLATE_ID) from LZ_ITEM_TEMPLATE)")->result_array();
        //$data = $this->db->query($get_data)->;
        $tableData = $get_data[0]['TEMPLATE_ID'];
        // print_r($tableData);

        $tableData = $this->db->query("SELECT TEMPLATE_ID,TEMPLATE_NAME, EBAY_LOCAL, CURRENCY, LIST_TYPE, SHIP_FROM_ZIP_CODE, PAYMENT_METHOD, SHIP_FROM_LOC,  PAYPAL_EMAIL, DISPATCH_TIME_MAX, SHIPPING_SERVICE, SHIPPING_COST, ADDITIONAL_COST, RETURN_OPTION, RETURN_DAYS, SHIPPING_PAID_BY, ENTERED_BY, MERCHANT_ID,ACCOUNT_ID from LZ_ITEM_TEMPLATE where TEMPLATE_ID = '$tableData' ORDER BY TEMPLATE_ID DESC ")->result_array();
        return array("status" => true, 'tableData' => $tableData);
    }

    public function DeleteTamplateData()
    { // for template form
        $TEMPLATE_ID = $this->input->post('id');
        $deleteTemp = $this->db->query("DELETE LZ_ITEM_TEMPLATE where TEMPLATE_ID='$TEMPLATE_ID'");
        return $deleteTemp;
    }

    public function ShipingServiceDrowp()
    { // for template form
        $shipData = $this->db->query("SELECT * from lz_shiping_name");
        return $shipData->result_array();
    }

    public function UpDateTamplateData()
    { // for template form

        $TEMPLATE_ID = $this->input->post('TEMPLATE_ID');
        $TemplateNameupdate = $this->input->post('TemplateNameupdate');
        $TemplateNameupdate = trim(str_replace("  ", ' ', $TemplateNameupdate));
        $EBAY_LOCAL = $this->input->post('SiteIDupdate');
        $SHIP_FROM_LOC = $this->input->post('ShipFromCountryupdate');
        $CURRENCY = $this->input->post('Currencyupdate');
        $LIST_TYPE = $this->input->post('selectListingTypeupdate');
        $SHIP_FROM_ZIP_CODE = $this->input->post('ZipCodeupdate');
        $PAYMENT_METHOD = $this->input->post('PaymentMethodupdate');
        $PAYPAL_EMAIL = $this->input->post('PaypalEmailAddressupdate');
        $DISPATCH_TIME_MAX = $this->input->post('DispatchTimeMaxupdate');
        $SHIPPING_SERVICE = $this->input->post('selectshippingserviceupdate');
        $SHIPPING_COST = $this->input->post('ShppingServiceCostupdate');
        $ADDITIONAL_COST = $this->input->post('ShppingServiceAdditionalCostupdate');
        $RETURN_OPTION = $this->input->post('selectreturnOptionupdate');
        $RETURN_DAYS = $this->input->post('ReturnsWithinOptionupdate');
        $SHIPPING_PAID_BY = $this->input->post('selectshingPaidupdate');

        $Data = $this->db->query("UPDATE LZ_ITEM_TEMPLATE set   TEMPLATE_NAME='$TemplateNameupdate' ,  EBAY_LOCAL='$EBAY_LOCAL' ,SHIP_FROM_LOC='$SHIP_FROM_LOC', CURRENCY='$CURRENCY',
LIST_TYPE='$LIST_TYPE', SHIP_FROM_ZIP_CODE='$SHIP_FROM_ZIP_CODE',PAYMENT_METHOD='$PAYMENT_METHOD',PAYPAL_EMAIL='$PAYPAL_EMAIL',DISPATCH_TIME_MAX='$DISPATCH_TIME_MAX',
SHIPPING_SERVICE='$SHIPPING_SERVICE',SHIPPING_COST='$SHIPPING_COST',ADDITIONAL_COST='$ADDITIONAL_COST',RETURN_OPTION='$RETURN_OPTION',
RETURN_DAYS='$RETURN_DAYS',SHIPPING_PAID_BY='$SHIPPING_PAID_BY'
 where TEMPLATE_ID='$TEMPLATE_ID'");
        return $Data;
    }

    /********************************
     *   end Screen Tempdata
     *********************************/

    /********************************
     *   start Screen TotalBarcode
     *********************************/
    public function TotalBarcode()
    {
        $merChant_ID = $this->input->post('id');
        $tBarcode = $this->db->query("SELECT D.BARCODE_NO,
        L.BARCODE_PRV_NO,
        L.PIC_DATE_TIME,
        L.LZ_MANIFEST_DET_ID,
        B.EBAY_ITEM_ID,
        B.SALE_RECORD_NO,
        B.ITEM_ID,
        I.ITEM_DESC,
        I.ITEM_MT_MANUFACTURE,
        I.ITEM_MT_MFG_PART_NO,
        I.ITEM_MT_UPC,
        I.ITEM_CONDITION,
        C.COND_NAME CONDITION_ID
   FROM LZ_MERCHANT_BARCODE_MT M,
        LZ_ITEM_COND_MT        C,
        LZ_MERCHANT_BARCODE_DT D,
        LZ_SPECIAL_LOTS        L,
        LZ_BARCODE_MT          B,
        ITEMS_MT               I
  WHERE M.MT_ID = D.MT_ID
    AND B.CONDITION_ID = C.ID
    AND D.BARCODE_NO = L.BARCODE_PRV_NO(+)
    AND L.BARCODE_PRV_NO = B.BARCODE_NO(+)
    AND B.ITEM_ID = I.ITEM_ID(+)
    AND B.EBAY_ITEM_ID IS NULL
    and l.lz_manifest_det_id is not null
    AND M.MERCHANT_ID = $merChant_ID
  order by BARCODE_NO desc");
        return $tBarcode->result_array();
    }
    /********************************
     *   end Screen TotalBarcode
     *********************************/

    /********************************
     *  Start Screen PictureDone
     *********************************/
    public function PictureDone()
    {
        $merChant_ID = $this->input->post('id');
        $tpicture = $this->db->query("SELECT B.EBAY_ITEM_ID,
        s.seed_id seed_id,
        '$'|| s.ebay_price pric,
        B.EBAY_ITEM_ID QTY,
        S.ITEM_TITLE ITEM_DESC,
        S.F_MANUFACTURE ITEM_MT_MANUFACTURE,
        S.F_MPN ITEM_MT_MFG_PART_NO,
        S.F_UPC ITEM_MT_UPC,
        COND_NAME CONDITION_ID,
        CA.CATEGORY_NAME NAME
        FROM LZ_MERCHANT_BARCODE_MT M,
        LZ_ITEM_COND_MT C,
        LZ_MERCHANT_BARCODE_DT D,
        LZ_SPECIAL_LOTS L,
        LZ_BARCODE_MT B,
        LZ_ITEM_SEED S,
        LZ_BD_CATEGORY CA
        WHERE M.MT_ID = D.MT_ID
        AND B.CONDITION_ID = C.ID(+)
        AND D.BARCODE_NO = L.BARCODE_PRV_NO(+)
        AND L.BARCODE_PRV_NO = B.BARCODE_NO(+)
        AND B.ITEM_ID = S.ITEM_ID(+)
        AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+)
        AND S.CATEGORY_ID = CA.CATEGORY_ID(+)
        AND B.CONDITION_ID = S.DEFAULT_COND(+)
        AND B.EBAY_ITEM_ID IS NOT NULL
        AND B.SALE_RECORD_NO IS NULL
        AND M.MERCHANT_ID = $merChant_ID
        --GROUP BY B.EBAY_ITEM_ID");
        return $tpicture->result_array();
    }
    /********************************
     *  end Screen PictureDone
     *********************************/

    /********************************
     *   start Screen barcodeProces
     *********************************/

    public function GetBarcodeProcess()
    {
        $merChant_ID = $this->input->post('id');
        $tBarcode = $this->db->query("SELECT B.EBAY_ITEM_ID,
        s.seed_id seed_id,
        '$'|| s.ebay_price pric,
        B.EBAY_ITEM_ID QTY,
        S.ITEM_TITLE ITEM_DESC,
        S.F_MANUFACTURE ITEM_MT_MANUFACTURE,
        S.F_MPN ITEM_MT_MFG_PART_NO,
        S.F_UPC ITEM_MT_UPC,
        COND_NAME CONDITION_ID,
        CA.CATEGORY_NAME NAME
        FROM LZ_MERCHANT_BARCODE_MT M,
        LZ_ITEM_COND_MT C,
        LZ_MERCHANT_BARCODE_DT D,
        LZ_SPECIAL_LOTS L,
        LZ_BARCODE_MT B,
        LZ_ITEM_SEED S,
        LZ_BD_CATEGORY CA
        WHERE M.MT_ID = D.MT_ID
        AND B.CONDITION_ID = C.ID(+)
        AND D.BARCODE_NO = L.BARCODE_PRV_NO(+)
        AND L.BARCODE_PRV_NO = B.BARCODE_NO(+)
        AND B.ITEM_ID = S.ITEM_ID(+)
        AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+)
        AND S.CATEGORY_ID = CA.CATEGORY_ID(+)
        AND B.CONDITION_ID = S.DEFAULT_COND(+)
        AND B.EBAY_ITEM_ID IS NOT NULL
        AND B.SALE_RECORD_NO IS NULL
        AND M.MERCHANT_ID = $merChant_ID
       -- GROUP BY B.EBAY_ITEM_ID");
        return $tBarcode->result_array();
    }
    /********************************
     *  end Screen barcodeProces
     *********************************/

    /********************************
     *  start Screen SoldeItem
     *********************************/
    public function GetSoldItem()
    {
        $merChant_ID = $this->input->post('id');
        $sold = $this->db->query("SELECT B.EBAY_ITEM_ID,
    max(s.seed_id) seed_id,
    '$'|| max(s.ebay_price) pric,
    COUNT(B.EBAY_ITEM_ID) QTY,
    MAX(S.ITEM_TITLE) ITEM_DESC,
    MAX(S.F_MANUFACTURE) ITEM_MT_MANUFACTURE,
    MAX(S.F_MPN) ITEM_MT_MFG_PART_NO,
    MAX(S.F_UPC) ITEM_MT_UPC, /*MAX(I.ITEM_DESC) ITEM_DESC, MAX(I.ITEM_MT_MANUFACTURE) ITEM_MT_MANUFACTURE, MAX(I.ITEM_MT_MFG_PART_NO) ITEM_MT_MFG_PART_NO, MAX(I.ITEM_MT_UPC) ITEM_MT_UPC,*/
    MAX(COND_NAME) CONDITION_ID,
    MAX(CA.CATEGORY_NAME) NAME
FROM LZ_MERCHANT_BARCODE_MT M,
    LZ_ITEM_COND_MT        C,
    LZ_MERCHANT_BARCODE_DT D,
    LZ_SPECIAL_LOTS        L,
    LZ_BARCODE_MT          B, /*ITEMS_MT I,*/
    LZ_ITEM_SEED           S,
    LZ_BD_CATEGORY         CA
WHERE M.MT_ID = D.MT_ID
AND B.CONDITION_ID = C.ID
AND D.BARCODE_NO = L.BARCODE_PRV_NO(+)
AND L.BARCODE_PRV_NO = B.BARCODE_NO(+)
AND B.ITEM_ID = S.ITEM_ID
AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID
AND S.CATEGORY_ID = CA.CATEGORY_ID(+)
AND B.CONDITION_ID = S.DEFAULT_COND /*AND B.ITEM_ID = I.ITEM_ID(+)*/
AND B.EBAY_ITEM_ID IS NOT NULL
AND B.SALE_RECORD_NO IS not NULL
AND M.MERCHANT_ID = $merChant_ID
GROUP BY B.EBAY_ITEM_ID");
        return $sold->result_array();
    }
    /********************************
     *  end Screen SoldeItem
     *********************************/

    /********************************
     *  Screen ActiveNotList
     *********************************/
    public function GetActiveNotListed()
    {
        $merChant_ID = $this->input->post('id');
        $tBarcode = $this->db->query("SELECT D.BARCODE_NO,
               L.BARCODE_PRV_NO,
               L.PIC_DATE_TIME,
               L.LZ_MANIFEST_DET_ID,
               B.EBAY_ITEM_ID,
               B.SALE_RECORD_NO,
               B.ITEM_ID,
               I.ITEM_DESC,
               I.ITEM_MT_MANUFACTURE,
               I.ITEM_MT_MFG_PART_NO,
               I.ITEM_MT_UPC,
               I.ITEM_CONDITION,
               C.COND_NAME CONDITION_ID
          FROM LZ_MERCHANT_BARCODE_MT M,
               LZ_ITEM_COND_MT        C,
               LZ_MERCHANT_BARCODE_DT D,
               LZ_SPECIAL_LOTS        L,
               LZ_BARCODE_MT          B,
               ITEMS_MT               I
         WHERE M.MT_ID = D.MT_ID
           AND B.CONDITION_ID = C.ID
           AND D.BARCODE_NO = L.BARCODE_PRV_NO(+)
           AND L.BARCODE_PRV_NO = B.BARCODE_NO(+)
           AND B.ITEM_ID = I.ITEM_ID(+)
           AND B.EBAY_ITEM_ID IS NULL
           and l.lz_manifest_det_id is not null
           AND M.MERCHANT_ID = $merChant_ID
         order by BARCODE_NO desc");
        return $tBarcode->result_array();
    }

    /********************************
     *  Screen ActiveNotList
     *********************************/

    /********************************
     *  Screen UserList
     *********************************/
    public function Get_Users_List()
    {
        $user_Data = $this->db->query("SELECT E.*, d.Merchant_Id, m.buisness_name from emp_merchant_det d, EMPLOYEE_MT E, lz_merchant_mt m WHERE d.Employee_Id = E.EMPLOYEE_ID AND m.merchant_id = d.merchant_id");
        return $user_Data->result_array();
    }

    public function disable_And_Anable_Users_List()
    {
        $EMPLOYEE_ID = $this->input->post('id');
        $status = $this->input->post('status');

        if (!empty($EMPLOYEE_ID)) {
            $query = $this->db->query("UPDATE EMPLOYEE_MT SET STATUS ='$status' WHERE EMPLOYEE_ID ='$EMPLOYEE_ID'");
            if ($query) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function Update_Users_List()
    {
        $EMPLOYEE_ID = $this->input->post('EMPLOYEE_ID');
        $FIRST_NAME = $this->input->post('FirstName_update');
        $FIRST_NAME = trim(str_replace("  ", ' ', $FIRST_NAME));
        $FIRST_NAME = trim(str_replace(array("'"), "''", $FIRST_NAME));
        $LAST_NAME = $this->input->post('LastName_update');
        $LAST_NAME = trim(str_replace("  ", ' ', $LAST_NAME));
        $LAST_NAME = trim(str_replace(array("'"), "''", $LAST_NAME));
        $USER_NAME = $this->input->post('UserName_update');
        $USER_NAME = trim(str_replace("  ", ' ', $USER_NAME));
        $USER_NAME = trim(str_replace(array("'"), "''", $USER_NAME));
        $PASS_WORD = $this->input->post('Password_update');
        $E_MAIL_ADDRESS = $this->input->post('UserEmail_Update');
        $LOCATION = $this->input->post('selectLocation_update');
        // var_dump( $EMPLOYEE_ID); exit;
        // $merchant = $this->input->post('selectMerchant');

        $query = $this->db->query("UPDATE EMPLOYEE_MT SET FIRST_NAME='$FIRST_NAME', LAST_NAME='$LAST_NAME', USER_NAME='$USER_NAME', PASS_WORD='$PASS_WORD', E_MAIL_ADDRESS='$E_MAIL_ADDRESS', LOCATION='$LOCATION' WHERE EMPLOYEE_ID ='$EMPLOYEE_ID'");
        if ($query) {
            $merchant = $this->input->post('selectMerchant_update');
            if ($merchant > 0) {
                $check = $this->db->query("SELECT EMP_DET_ID FROM EMP_MERCHANT_DET WHERE EMPLOYEE_ID = '$EMPLOYEE_ID' and MERCHANT_ID = '$merchant'");
                if ($check->num_rows() == 0) {

                    $this->db->query("INSERT INTO EMP_MERCHANT_DET (EMP_DET_ID,EMPLOYEE_ID,MERCHANT_ID) VALUES (GET_SINGLE_PRIMARY_KEY('EMP_MERCHANT_DET','EMP_DET_ID'),'$EMPLOYEE_ID','$merchant')");
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function Insert_Users_List()
    {
        // $EMPLOYEE_ID = $this->input->post('EMPLOYEE_ID');
        $FIRST_NAME = $this->input->post('FirstName');
        $FIRST_NAME = trim(str_replace("  ", ' ', $FIRST_NAME));
        $FIRST_NAME = trim(str_replace(array("'"), "''", $FIRST_NAME));
        $LAST_NAME = $this->input->post('LastName');
        $LAST_NAME = trim(str_replace("  ", ' ', $LAST_NAME));
        $LAST_NAME = trim(str_replace(array("'"), "''", $LAST_NAME));
        $USER_NAME = $this->input->post('UserName');
        $USER_NAME = trim(str_replace("  ", ' ', $USER_NAME));
        $USER_NAME = trim(str_replace(array("'"), "''", $USER_NAME));
        $PASS_WORD = $this->input->post('Password');
        $E_MAIL_ADDRESS = $this->input->post('UserEmail');
        $LOCATION = $this->input->post('selectLocation');
        if (!empty($LOCATION)) {
            $LOCATION = $LOCATION['label'];
        }
        // var_dump($FIRST_NAME); exit;

        $usename = $this->db->query("SELECT USER_NAME from EMPLOYEE_MT where USER_NAME ='$USER_NAME'");
        if ($usename->num_rows() > 0) {
            $data = array("status" => false, "message" => "User Name Already Exist");
            return $data;
        } else {
            $user_Data = $this->db->query("INSERT INTO EMPLOYEE_MT(EMPLOYEE_ID, FIRST_NAME, LAST_NAME, JOIN_DATE, BIRTH_DATE, HOME_ADDRESS, RES_PHONE_NO, CELL_PHONE_NO, MARITAL_STATUS, ID_CARD_NUMBER, GENDER, REMARKS, TITLE, APPL_USER_YN, USER_NAME, PASS_WORD, E_MAIL_ADDRESS, EMP_STATUS, DEPT_ID, DESIG_ID, EMP_CODE, PASSWORD_DATE, EBAY_ITEM_ID, LOCATION, SHOW_UN_ASSIGNED, AUTH_PASSWORD) VALUES((get_single_primary_key('EMPLOYEE_MT','EMPLOYEE_ID')),'$FIRST_NAME','$LAST_NAME','01-JAN-90',NULL,NULL,NULL,NULL,'2',NULL,'M',NULL,'TITLE','2','$USER_NAME','$PASS_WORD','$E_MAIL_ADDRESS','OKY',NULL,NULL,NULL,NULL,NULL,'$LOCATION',NULL,NULL)");

            if ($user_Data) {
                $merchant = $this->input->post('selectMerchant');
                if (!empty($merchant)) {
                    $merchant = $merchant['value'];
                }
                $get_data = $this->db->query("SELECT EMPLOYEE_ID FROM EMPLOYEE_MT where EMPLOYEE_ID = (SELECT max(EMPLOYEE_ID) from EMPLOYEE_MT)")->result_array();
                //$data = $this->db->query($get_data)->;
                $getUserData = $get_data[0]['EMPLOYEE_ID'];
                // var_dump($getUserData); exit;
                $data = $this->db->query("SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME,USER_NAME,PASS_WORD,E_MAIL_ADDRESS,LOCATION FROM EMPLOYEE_MT where EMPLOYEE_ID=' $getUserData'")->result_array();
                //$data = array("status" => true, "response" => $getUserData);

                if ($merchant > 0) {
                    $checkdata = $this->db->query("SELECT EMP_DET_ID FROM EMP_MERCHANT_DET WHERE EMPLOYEE_ID = '$getUserData' and MERCHANT_ID = '$merchant'");

                    if ($checkdata->num_rows() == 0) {

                        $this->db->query("INSERT INTO EMP_MERCHANT_DET (EMP_DET_ID,EMPLOYEE_ID,MERCHANT_ID) VALUES (GET_SINGLE_PRIMARY_KEY('EMP_MERCHANT_DET','EMP_DET_ID'),'$getUserData','$merchant')");
                    }
                }
                return array("status" => true, 'tableData' => $data);
            } else {
                return false;
            }
        }
    }
    /********************************
     *  Screen UserList
     *********************************/

    /********************************
     *  Screen itemReturned
     *********************************/

    public function GetitemReturned()
    {
        $merChant_ID = $this->input->post('id');
        $itemReturned = $this->db->query("SELECT D.BARCODE_NO,
               L.BARCODE_PRV_NO,
               L.PIC_DATE_TIME,
               L.LZ_MANIFEST_DET_ID,
               B.EBAY_ITEM_ID,
               B.SALE_RECORD_NO,
               B.ITEM_ID,
               I.ITEM_DESC,
               I.ITEM_MT_MANUFACTURE,
               I.ITEM_MT_MFG_PART_NO,
               I.ITEM_MT_UPC,
               I.ITEM_CONDITION,
               C.COND_NAME CONDITION_ID
          FROM LZ_MERCHANT_BARCODE_MT M,
               LZ_ITEM_COND_MT        C,
               LZ_MERCHANT_BARCODE_DT D,
               LZ_SPECIAL_LOTS        L,
               LZ_BARCODE_MT          B,
               ITEMS_MT               I
         WHERE M.MT_ID = D.MT_ID
           AND B.CONDITION_ID = C.ID
           AND D.BARCODE_NO = L.BARCODE_PRV_NO(+)
           AND L.BARCODE_PRV_NO = B.BARCODE_NO(+)
           AND B.ITEM_ID = I.ITEM_ID(+)
           AND B.EBAY_ITEM_ID IS NULL
           and l.lz_manifest_det_id is not null
           AND M.MERCHANT_ID = $merChant_ID
         order by BARCODE_NO desc");
        return $itemReturned->result_array();
    }
    /********************************
     *  Screen itemReturned
     *********************************/

    /********************************
     *  Screen AddMerchant
     *********************************/

    public function Get_merchant_detail()
    { // for template form
        $shipData = $this->db->query("SELECT M.MERCHANT_ID,
        M.CONTACT_PERSON,
        M.MERCHANT_CODE,
        M.BUISNESS_NAME,
        s.service_desc SERVICE_TYPE,
        C.CITY_DESC,
        ST.STATE_DESC,
        M.CONTACT_NO,
        M.ADDRESS,
        M.ACTIVE_FROM,
        M.ACTIVE_TO,
        M.ACTIVE_STATUS,
        C.CITY_ID,
        s.service_id
   FROM LZ_MERCHANT_MT M, WIZ_CITY_MT C, WIZ_STATE_MT ST , lj_services s
  WHERE M.STATE_ID = C.CITY_ID(+)
    AND C.STATE_ID = ST.STATE_ID(+)
    and m.service_type = s.service_id
  ORDER BY M.MERCHANT_ID asc");
        return $shipData->result_array();
    }

    public function Delete_merchant_detail()
    {
        $MERCHANT_ID = $this->input->post('id');

        $query = $this->db->query("DELETE LZ_MERCHANT_MT where MERCHANT_ID ='$MERCHANT_ID'");

        //$query=$this->db->query("DELETE lj_merchant_service where MERCHANT_SER_ID='$MERCHANT_ID'");
        return $query;
    }
    public function Get_merchant_Services_Type()
    { // for template form
        $stat_query = $this->db->query("SELECT SERVICE_ID, SERVICE_DESC FROM lj_services");
        return $stat_query->result_array();
    }

    public function Update_merchant_detail()
    {

        $MERCHANT_ID = $this->input->post('MERCHANT_ID');
        $murch_name = $this->input->post('MerchantNameUpdate');
        $murch_name = trim(str_replace("  ", ' ', $murch_name));
        $murch_name = trim(str_replace(array("'"), "''", $murch_name));
        $buis_name = $this->input->post('BuisnessNameUpdate');
        $buis_name = trim(str_replace("  ", ' ', $buis_name));
        $buis_name = trim(str_replace(array("'"), "''", $buis_name));
        $merch_add = $this->input->post('MerchantAddressUpdate');
        $merch_add = trim(str_replace("  ", ' ', $merch_add));
        $merch_add = trim(str_replace(array("'"), "''", $merch_add));
        $merch_phon = $this->input->post('MerchantPhoneUpdate');
        $merch_act_from = $this->input->post('dateFromUpdate');
        $merch_act_to = $this->input->post('datetoUpdate');
        $merch_servic = $this->input->post('SelectServiceTypeUpdate');
        $stat_id = $this->input->post('SelectCityUpdate');
        $fromdate = new DateTime($merch_act_from);
        $startDate = date_format($fromdate, 'y-m-d');
        $todate = new DateTime($merch_act_to);
        $endDate = date_format($todate, 'y-m-d');
        $query = $this->db->query("UPDATE LZ_MERCHANT_MT M SET M.BUISNESS_NAME = '$buis_name', M.CONTACT_NO = '$merch_phon',M.CONTACT_PERSON ='$murch_name', M.ADDRESS ='$merch_add',M.STATE_ID ='$stat_id', M.SERVICE_TYPE = '$merch_servic',M.ACTIVE_FROM = TO_DATE('$startDate','yyyy-mm-dd HH24:MI:SS') ,M.ACTIVE_TO = TO_DATE('$endDate', 'yyyy-mm-dd HH24:MI:SS')  WHERE M.MERCHANT_ID = '$MERCHANT_ID' ");
        $query = $this->db->query("UPDATE lj_merchant_service SET SERVICE_ID ='$merch_servic' WHERE MERCHANT_ID ='$MERCHANT_ID'");
        if ($query) {
            return true;
        } else {

            return false;
        }
    }

    public function Insert_merchant_detail()
    {

        $murch_name = $this->input->post('MerchantName');
        $murch_name = trim(str_replace("  ", ' ', $murch_name));
        $murch_name = trim(str_replace(array("'"), "''", $murch_name));
        $buis_name = $this->input->post('BuisnessName');
        $buis_name = trim(str_replace("  ", ' ', $buis_name));
        $buis_name = trim(str_replace(array("'"), "''", $buis_name));
        $merch_add = $this->input->post('MerchantAddress');
        $merch_add = trim(str_replace("  ", ' ', $merch_add));
        $merch_add = trim(str_replace(array("'"), "''", $merch_add));
        $merch_phon = $this->input->post('MerchantPhone');
        $merch_act_from = $this->input->post('dateFrom');
        $merch_act_to = $this->input->post('dateto');
        $merch_servic = $this->input->post('SelectServiceType');
        $stat_id = $this->input->post('SelectCity');
        $created_by = $this->input->post('created_by');

        $fromdate = new DateTime($merch_act_from);
        $startDate = date_format($fromdate, 'y-m-d');
        $todate = new DateTime($merch_act_to);
        $endDate = date_format($todate, 'y-m-d');

        //var_dump( $murch_name,  $buis_name, $merch_add, $merch_phon,$startDate,  $endDate, $merch_servic, $stat_id);
        $merch_code = $this->db->query("SELECT lpad(NVL(MAX(Merchant_code + 1), 1),5,0) MERCH_CODE FROM LZ_MERCHANT_MT ")->result_array();
        $gmerch_code = $merch_code[0]['MERCH_CODE'];
        foreach ($merch_servic as $value) {
            $service_id = $value['value'];

            $get_data = $this->db->query("INSERT INTO LZ_MERCHANT_MT (MERCHANT_ID, MERCHANT_CODE, BUISNESS_NAME, CONTACT_NO, CONTACT_PERSON, ADDRESS, STATE_ID, SERVICE_TYPE, ACTIVE_FROM, ACTIVE_TO) VALUES (get_single_primary_key('LZ_MERCHANT_MT','MERCHANT_ID'), '$gmerch_code', '$buis_name', '$merch_phon', '$murch_name', '$merch_add', '$stat_id', '$service_id',TO_DATE('$startDate','yyyy-mm-dd HH24:MI:SS') ,TO_DATE('$endDate','yyyy-mm-dd HH24:MI:SS'))");
        }

        $get_merchant_id = $this->db->query("SELECT MERCHANT_ID FROM LZ_MERCHANT_MT where MERCHANT_ID = (SELECT max(MERCHANT_ID) from LZ_MERCHANT_MT)")->result_array();

        $getUserData = $get_merchant_id[0]['MERCHANT_ID'];

        $get_data = $this->db->query("INSERT INTO lj_merchant_service (MERCHANT_SER_ID, MERCHANT_ID,SERVICE_ID, CREATED_BY,DATED) values(get_single_primary_key('lj_merchant_service','MERCHANT_SER_ID'),'$getUserData','$service_id','$created_by', sysdate)");

        $get_data = $this->db->query("SELECT m.MERCHANT_ID,
         m.MERCHANT_CODE,
         m.BUISNESS_NAME,
         m.CONTACT_NO,
         m.CONTACT_PERSON,
         m.ADDRESS,
         c.city_desc,
         st.state_desc,
         m.ACTIVE_FROM,
         m.ACTIVE_TO,
         s.SERVICE_DESC SERVICE_TYPE
         FROM LZ_MERCHANT_MT m, lj_services s , wiz_city_mt c , wiz_state_mt st
         WHERE MERCHANT_ID = '$getUserData'
         and SERVICE_ID = '$service_id'
         and m.state_id = c.city_id(+)
         and c.state_id = st.state_id(+)")->result_array();

        return array("status" => true, 'tableData' => $get_data);
    }

    public function Get_merchant_City()
    { // for template form
        $stat_query = $this->db->query("SELECT  C.CITY_ID,CITY_DESC FROM WIZ_CITY_MT C WHERE C.STATE_ID > 1004 order by CITY_DESC ");
        return $stat_query->result_array();
    }
    /********************************
     *  Screen AddMerchant
     *********************************/

    /********************************
     *  Screen MyProfile
     *********************************/

    public function Get_MyProfile()
    {
        $merchant_id = $this->input->post('merchant_id');
        // if ($merchant_id) {
        $directory = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c  where c.path_id = 6")->result_array();
        //var_dump($directory[0]["MASTER_PATH"] . $merchant_id);

        //get master path and save in directory
        $pic_path = $directory[0]["MASTER_PATH"] . $merchant_id . '/';
        $qry = $this->db->query("SELECT
                        '$pic_path'|| LOGO img
                FROM lz_merchant_dt d, WIZ_CITY_MT c
                WHERE MERCHANT_ID = '$merchant_id'
                and d.city = c.city_id")->result_array();
        $pic_path = $qry[0]['IMG'];

        $img = @file_get_contents($pic_path);
        $img = base64_encode($img);
        //return array('image'=>$img);

        $qry = $this->db->query("SELECT
             F_NAME,
            L_NAME,
            ADDRESS1,
            ADDRESS2,
            CITY,
            ZIP_CODE,
            CONTACT_NO,
            PAYPAL_EMAIL,
            DISPLAY_NAME,
            LOGO,
            MERCH_DT_ID,
            MERCHANT_ID,
            c.city_desc,
            c.CITY_ID
       FROM lz_merchant_dt d, WIZ_CITY_MT c
      WHERE MERCHANT_ID = '$merchant_id'
      and d.city = c.city_id");
        if ($img) {

            return array('data' => $qry->result_array(), 'image' => $img);
        } else {
            return array('data' => $qry->result_array(), 'image' => '');
        }
    }
    public function Update_MyProfile()
    {

        $FirstName = $this->input->post('FirstName');
        $FirstName = trim(str_replace("  ", ' ', $FirstName));
        $FirstName = trim(str_replace(array("'"), "''", $FirstName));
        $image = $this->input->post('image');
        $LastName = $this->input->post('LastName');
        $LastName = trim(str_replace("  ", ' ', $LastName));
        $LastName = trim(str_replace(array("'"), "''", $LastName));
        $PaypalEmail = $this->input->post('PaypalEmail');
        $PaypalEmail = trim(str_replace("  ", ' ', $PaypalEmail));
        $PaypalEmail = trim(str_replace(array("'"), "''", $PaypalEmail));
        $ContactNo = $this->input->post('ContactNo');
        $ContactNo = trim(str_replace("  ", ' ', $ContactNo));
        $ContactNo = trim(str_replace(array("'"), "''", $ContactNo));
        $ZipCode = $this->input->post('ZipCode');
        $ZipCode = trim(str_replace("  ", ' ', $ZipCode));
        $ZipCode = trim(str_replace(array("'"), "''", $ZipCode));
        $Address1 = $this->input->post('Address1');
        $Address1 = trim(str_replace("  ", ' ', $Address1));
        $Address1 = trim(str_replace(array("'"), "''", $Address1));
        $Address2 = $this->input->post('Address2');
        $Address2 = trim(str_replace("  ", ' ', $Address2));
        $Address2 = trim(str_replace(array("'"), "''", $Address2));
        $DisplayName = $this->input->post('DisplayName');
        $DisplayName = trim(str_replace("  ", ' ', $DisplayName));
        $DisplayName = trim(str_replace(array("'"), "''", $DisplayName));
        $SelectCity = $this->input->post('SelectCity');
        $merchant_id = $this->input->post('merchant_id');
        $image = "";
        // var_dump(($_FILES['image']['name']));exit;

        $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 6");
        $specific_qry = $query->result_array();
        $specific_path = $specific_qry[0]['MASTER_PATH'];
        $main_dir = $specific_path . $merchant_id; //. $_FILES['image']['name'];

        array_map('unlink', glob("$main_dir/*.*"));

        if (!empty($_FILES['image']['name'])) {

            $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 6");
            $specific_qry = $query->result_array();
            $specific_path = $specific_qry[0]['MASTER_PATH'];

            $main_dir = $specific_path . $merchant_id;
            if (is_dir($main_dir) === false) {
                mkdir($main_dir);
            }
            $config['charset'] = 'charset=utf-8';
            $config['upload_path'] = $main_dir;
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name'] = $_FILES['image']['name'];

            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $uploadData = $this->upload->data();
            // var_dump($uploadData);exit;
            if ($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data();
                $image = $uploadData['file_name'];
            } else {
                $image = "";
            }
        }

        $query = $this->db->query("UPDATE lz_merchant_dt SET F_NAME='$FirstName', L_NAME='$LastName', ADDRESS1='$Address1', ADDRESS2='$Address2', CITY='$SelectCity', ZIP_CODE='$ZipCode',CONTACT_NO='$ContactNo', PAYPAL_EMAIL='$PaypalEmail', DISPLAY_NAME='$DisplayName', LOGO='$image', INSERT_DATE= sysdate WHERE MERCHANT_ID ='$merchant_id'");
        if ($query) {
            return true;
        } else {

            return false;
        }
    }

    public function Insert_MyProfile()
    { // for MyProfile
        // var_dump($_FILES["image"]["tmp_name"]);//exit;
        $FirstName = $this->input->post('FirstName');
        $FirstName = trim(str_replace("  ", ' ', $FirstName));
        $FirstName = trim(str_replace(array("'"), "''", $FirstName));
        $image = $this->input->post('image');
        $LastName = $this->input->post('LastName');
        $LastName = trim(str_replace("  ", ' ', $LastName));
        $LastName = trim(str_replace(array("'"), "''", $LastName));
        $PaypalEmail = $this->input->post('PaypalEmail');
        $PaypalEmail = trim(str_replace("  ", ' ', $PaypalEmail));
        $PaypalEmail = trim(str_replace(array("'"), "''", $PaypalEmail));
        $ContactNo = $this->input->post('ContactNo');
        $ContactNo = trim(str_replace("  ", ' ', $ContactNo));
        $ContactNo = trim(str_replace(array("'"), "''", $ContactNo));
        $ZipCode = $this->input->post('ZipCode');
        $ZipCode = trim(str_replace("  ", ' ', $ZipCode));
        $ZipCode = trim(str_replace(array("'"), "''", $ZipCode));
        $Address1 = $this->input->post('Address1');
        $Address1 = trim(str_replace("  ", ' ', $Address1));
        $Address1 = trim(str_replace(array("'"), "''", $Address1));
        $Address2 = $this->input->post('Address2');
        $Address2 = trim(str_replace("  ", ' ', $Address2));
        $Address2 = trim(str_replace(array("'"), "''", $Address2));
        $DisplayName = $this->input->post('DisplayName');
        $DisplayName = trim(str_replace("  ", ' ', $DisplayName));
        $DisplayName = trim(str_replace(array("'"), "''", $DisplayName));
        $SelectCity = $this->input->post('SelectCity');
        $created_by = $this->input->post('created_by');
        $merchant_id = $this->input->post('merchant_id');

        $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 6");
        $specific_qry = $query->result_array();
        $specific_path = $specific_qry[0]['MASTER_PATH'];
        $main_dir = $specific_path . $merchant_id; //. $_FILES['image']['name'];

        array_map('unlink', glob("$main_dir/*.*"));

        $image = "";
        // var_dump(($_FILES['image']['name']));exit;
        if (!empty($_FILES['image']['name'])) {

            $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 6");
            $specific_qry = $query->result_array();
            $specific_path = $specific_qry[0]['MASTER_PATH'];

            $main_dir = $specific_path . $merchant_id;
            if (is_dir($main_dir) === false) {
                mkdir($main_dir);
            }

            $config['charset'] = 'charset=utf-8';
            $config['upload_path'] = $main_dir;
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name'] = $_FILES['image']['name'];

            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $uploadData = $this->upload->data();
            // var_dump($uploadData);exit;
            if ($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data();
                $image = $uploadData['file_name'];
            } else {
                $image = "";
            }
        }
        $stat_query = $this->db->query("INSERT INTO  lz_merchant_dt(MERCH_DT_ID,  MERCHANT_ID, F_NAME, L_NAME, ADDRESS1, ADDRESS2, CITY, ZIP_CODE,CONTACT_NO, PAYPAL_EMAIL, DISPLAY_NAME, LOGO, INSERT_DATE, INSERTED_BY) values (get_single_primary_key('lz_merchant_dt','MERCH_DT_ID'),'$merchant_id','$FirstName','$LastName','$Address1','$Address2','$SelectCity','$ZipCode','$ContactNo','$PaypalEmail','$DisplayName','$image',sysdate,'$created_by')");
        //var_dump( $stat_query );exit;
        return $stat_query;
        //  }
        //}
    }

    public function check_merchant_id($merchant_id)
    {
        $mrQuery = "SELECT MERCHANT_ID from lz_merchant_dt  where MERCHANT_ID = '$merchant_id'";

        $result = $this->db->query($mrQuery)->result_array();

        return $result;
    }
    /********************************
     *  Screen MyProfile
     *********************************/

    /********************************
     *  Screen US-PK Non Listed Items
     *********************************/
    public function Get_employee() // employee dropDown

    {

        $qyer = $this->db->query("SELECT M.EMPLOYEE_ID,M.USER_NAME FROM EMPLOYEE_MT M WHERE M.LOCATION = 'PK' AND M.STATUS =1 ")->result_array();

        return $qyer;
    }

    public function Get_nonListedItems()
    {

        $employeeName = $this->input->post('emp');
        $radioselect = $this->input->post('radioselect');

        $qyer = "SELECT * from ( SELECT Q.*, C.BUISNESS_NAME
            FROM LZ_MERCHANT_BARCODE_MT M,
                 LZ_MERCHANT_BARCODE_DT D,
                 LZ_MERCHANT_MT C,
                 (SELECT *
                    FROM (SELECT LS.SEED_ID,
                                 (SELECT M.USER_NAME
                                    FROM EMPLOYEE_MT M
                                   WHERE M.EMPLOYEE_ID = DET.IDENTIFIED_BY) VERIFY_BY,
                                 DECODE(((SELECT *
                                            FROM (SELECT M.USER_NAME
                                                    FROM LZ_LISTING_ALLOC L,
                                                         EMPLOYEE_MT      M
                                                   WHERE L.SEED_ID =
                                                         LS.SEED_ID
                                                     AND L.LISTER_ID =
                                                         M.EMPLOYEE_ID
                                                   ORDER BY L.ALLOC_ID DESC)
                                           WHERE ROWNUM <= 1)),
                                        NULL,
                                        'NOT ASSIGNED',
                                        ((SELECT *
                                            FROM (SELECT M.USER_NAME
                                                    FROM LZ_LISTING_ALLOC L,
                                                         EMPLOYEE_MT      M
                                                   WHERE L.SEED_ID =
                                                         LS.SEED_ID
                                                     AND L.LISTER_ID =
                                                         M.EMPLOYEE_ID
                                                   ORDER BY L.ALLOC_ID DESC)
                                           WHERE ROWNUM <= 1))) ASSIGN_TO,
                                 BB.BARCODE_NO,
                                 DET.BARCODE_PRV_NO,
                                 LS.ENTERED_BY,
                                 E.USER_NAME,
                                 LS.SHIPPING_SERVICE,
                                 LS.OTHER_NOTES,
                                 LS.LZ_MANIFEST_ID,
                                 LM.LOADING_DATE,
                                 LM.PURCH_REF_NO,
                                 LS.ITEM_TITLE ITEM_MT_DESC,
                                 I.ITEM_MT_MANUFACTURE MANUFACTURER,
                                 I.ITEM_ID,
                                 I.ITEM_CODE,
                                 I.ITEM_MT_MFG_PART_NO MFG_PART_NO,
                                 I.ITEM_MT_UPC UPC,
                                 BB.BIN_ID,
                                 BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME,
                                 O.OBJECT_NAME OBJECT_DESCRIP,
                                 C.COND_NAME ITEM_CONDITION,
                                 '1' QUANTITY,
                                 DET.BARCODE_PRV_NO FOLDER_NAME
                            FROM LZ_BARCODE_MT    BB,
                                 LZ_DEKIT_US_DT   DET,
                                 LZ_MANIFEST_DET  MDET,
                                 ITEMS_MT         I,
                                 LZ_ITEM_SEED     LS,
                                 LZ_MANIFEST_MT   LM,
                                 BIN_MT           BM,
                                 LZ_ITEM_COND_MT  C,
                                 LZ_BD_OBJECTS_MT O,
                                 EMPLOYEE_MT      E
                           WHERE BB.BARCODE_NO = DET.BARCODE_PRV_NO
                             AND DET.LZ_MANIFEST_DET_ID =
                                 MDET.LAPTOP_ZONE_ID
                             AND LM.LZ_MANIFEST_ID = MDET.LZ_MANIFEST_ID
                             AND LS.DEFAULT_COND = BB.CONDITION_ID
                             AND LS.ITEM_ID = BB.ITEM_ID
                             AND LS.LZ_MANIFEST_ID = BB.LZ_MANIFEST_ID
                             AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
                             AND C.ID = DET.CONDITION_ID
                             AND BB.ITEM_ID = I.ITEM_ID
                             AND O.OBJECT_ID = DET.OBJECT_ID
                             AND LS.ENTERED_BY = E.EMPLOYEE_ID(+)
                             AND BB.EBAY_ITEM_ID IS NULL
                             AND BB.BIN_ID = BM.BIN_ID
                             AND BB.DISCARD = 0)
                  UNION ALL
                  SELECT *
                    FROM (SELECT LS.SEED_ID,
                                 (SELECT M.USER_NAME
                                    FROM EMPLOYEE_MT M
                                   WHERE M.EMPLOYEE_ID = L.UPDATED_BY) VERIFY_BY,
                                 DECODE(((SELECT *
                                            FROM (SELECT M.USER_NAME
                                                    FROM LZ_LISTING_ALLOC L,
                                                         EMPLOYEE_MT      M
                                                   WHERE L.SEED_ID =
                                                         LS.SEED_ID
                                                     AND L.LISTER_ID =
                                                         M.EMPLOYEE_ID
                                                   ORDER BY L.ALLOC_ID DESC)
                                           WHERE ROWNUM <= 1)),
                                        NULL,
                                        'NOT ASSIGNED',
                                        ((SELECT *
                                            FROM (SELECT M.USER_NAME
                                                    FROM LZ_LISTING_ALLOC L,
                                                         EMPLOYEE_MT      M
                                                   WHERE L.SEED_ID =
                                                         LS.SEED_ID
                                                     AND L.LISTER_ID =
                                                         M.EMPLOYEE_ID
                                                   ORDER BY L.ALLOC_ID DESC)
                                           WHERE ROWNUM <= 1))) ASSIGN_TO,
                                 BB.BARCODE_NO,
                                 L.BARCODE_PRV_NO,
                                 LS.ENTERED_BY,
                                 E.USER_NAME,
                                 LS.SHIPPING_SERVICE,
                                 LS.OTHER_NOTES,
                                 LS.LZ_MANIFEST_ID,
                                 LM.LOADING_DATE,
                                 LM.PURCH_REF_NO,
                                 LS.ITEM_TITLE ITEM_MT_DESC,
                                 I.ITEM_MT_MANUFACTURE MANUFACTURER,
                                 I.ITEM_ID,
                                 I.ITEM_CODE,
                                 I.ITEM_MT_MFG_PART_NO MFG_PART_NO,
                                 I.ITEM_MT_UPC UPC,
                                 BB.BIN_ID,
                                 BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME,
                                 O.OBJECT_NAME OBJECT_DESCRIP,
                                 C.COND_NAME ITEM_CONDITION,
                                 '1' QUANTITY,
                                 L.FOLDER_NAME
                            FROM LZ_BARCODE_MT    BB,
                                 LZ_SPECIAL_LOTS  L,
                                 LZ_MANIFEST_DET  MDET,
                                 ITEMS_MT         I,
                                 LZ_ITEM_SEED     LS,
                                 LZ_MANIFEST_MT   LM,
                                 BIN_MT           BM,
                                 LZ_ITEM_COND_MT  C,
                                 LZ_BD_OBJECTS_MT O,
                                 EMPLOYEE_MT      E
                           WHERE BB.BARCODE_NO = L.BARCODE_PRV_NO
                             AND L.LZ_MANIFEST_DET_ID =
                                 MDET.LAPTOP_ZONE_ID
                             AND LM.LZ_MANIFEST_ID = MDET.LZ_MANIFEST_ID
                             AND LS.DEFAULT_COND = BB.CONDITION_ID
                             AND LS.ITEM_ID = BB.ITEM_ID
                             AND LS.LZ_MANIFEST_ID = BB.LZ_MANIFEST_ID
                             AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
                             AND BB.ITEM_ID = I.ITEM_ID
                             AND C.ID = L.CONDITION_ID
                             AND O.OBJECT_ID = L.OBJECT_ID
                             AND LS.ENTERED_BY = E.EMPLOYEE_ID(+)
                             AND BB.EBAY_ITEM_ID IS NULL
                             AND BB.BIN_ID = BM.BIN_ID
                             AND BB.DISCARD = 0)) Q
           WHERE M.MT_ID = D.MT_ID
             AND C.MERCHANT_ID = M.MERCHANT_ID
             AND D.BARCODE_NO = Q.BARCODE_PRV_NO)";

        $qyer = $this->db->query($qyer)->result_array();

        if (count($qyer) >= 1) {

            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_identiti_bar_pics($qyer, $conditions);
            $images = $uri['uri'];

            foreach ($qyer as $key => $value) {

                $qyer[$key]['MFG_PART_NO'] = str_replace('-', ' ', $value['MFG_PART_NO']);
            }
            return array("images" => $images, 'query' => $qyer);
        } else {
            return array('images' => [], 'query' => [], 'exist' => false);
        }
    }

    public function Get_SearchData() // get All nonlistedData at load timme

    {

        $employeeName = $this->input->post('emp');
        $radioselect = $this->input->post('radioselect');

        if ($radioselect == 'All') {
            $qyer = "SELECT * from ( SELECT Q.*, C.BUISNESS_NAME
            FROM LZ_MERCHANT_BARCODE_MT M,
                 LZ_MERCHANT_BARCODE_DT D,
                 LZ_MERCHANT_MT C,
                 (SELECT *
                    FROM (SELECT LS.SEED_ID,
                                 (SELECT M.USER_NAME
                                    FROM EMPLOYEE_MT M
                                   WHERE M.EMPLOYEE_ID = DET.IDENTIFIED_BY) VERIFY_BY,
                                 DECODE(((SELECT *
                                            FROM (SELECT M.USER_NAME
                                                    FROM LZ_LISTING_ALLOC L,
                                                         EMPLOYEE_MT      M
                                                   WHERE L.SEED_ID =
                                                         LS.SEED_ID
                                                     AND L.LISTER_ID =
                                                         M.EMPLOYEE_ID
                                                   ORDER BY L.ALLOC_ID DESC)
                                           WHERE ROWNUM <= 1)),
                                        NULL,
                                        'NOT ASSIGNED',
                                        ((SELECT *
                                            FROM (SELECT M.USER_NAME
                                                    FROM LZ_LISTING_ALLOC L,
                                                         EMPLOYEE_MT      M
                                                   WHERE L.SEED_ID =
                                                         LS.SEED_ID
                                                     AND L.LISTER_ID =
                                                         M.EMPLOYEE_ID
                                                   ORDER BY L.ALLOC_ID DESC)
                                           WHERE ROWNUM <= 1))) ASSIGN_TO,
                                 BB.BARCODE_NO,
                                 DET.BARCODE_PRV_NO,
                                 LS.ENTERED_BY,
                                 E.USER_NAME,
                                 LS.SHIPPING_SERVICE,
                                 LS.OTHER_NOTES,
                                 LS.LZ_MANIFEST_ID,
                                 LM.LOADING_DATE,
                                 LM.PURCH_REF_NO,
                                 LS.ITEM_TITLE ITEM_MT_DESC,
                                 I.ITEM_MT_MANUFACTURE MANUFACTURER,
                                 I.ITEM_ID,
                                 I.ITEM_CODE,
                                 I.ITEM_MT_MFG_PART_NO MFG_PART_NO,
                                 I.ITEM_MT_UPC UPC,
                                 BB.BIN_ID,
                                 BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME,
                                 O.OBJECT_NAME OBJECT_DESCRIP,
                                 C.COND_NAME ITEM_CONDITION,
                                 '1' QUANTITY,
                                 DET.BARCODE_PRV_NO FOLDER_NAME
                            FROM LZ_BARCODE_MT    BB,
                                 LZ_DEKIT_US_DT   DET,
                                 LZ_MANIFEST_DET  MDET,
                                 ITEMS_MT         I,
                                 LZ_ITEM_SEED     LS,
                                 LZ_MANIFEST_MT   LM,
                                 BIN_MT           BM,
                                 LZ_ITEM_COND_MT  C,
                                 LZ_BD_OBJECTS_MT O,
                                 EMPLOYEE_MT      E
                           WHERE BB.BARCODE_NO = DET.BARCODE_PRV_NO
                             AND DET.LZ_MANIFEST_DET_ID =
                                 MDET.LAPTOP_ZONE_ID
                             AND LM.LZ_MANIFEST_ID = MDET.LZ_MANIFEST_ID
                             AND LS.DEFAULT_COND = BB.CONDITION_ID
                             AND LS.ITEM_ID = BB.ITEM_ID
                             AND LS.LZ_MANIFEST_ID = BB.LZ_MANIFEST_ID
                             AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
                             AND C.ID = DET.CONDITION_ID
                             AND BB.ITEM_ID = I.ITEM_ID
                             AND O.OBJECT_ID = DET.OBJECT_ID
                             AND LS.ENTERED_BY = E.EMPLOYEE_ID(+)
                             AND BB.EBAY_ITEM_ID IS NULL
                             AND BB.BIN_ID = BM.BIN_ID
                             AND BB.DISCARD = 0)
                  UNION ALL
                  SELECT *
                    FROM (SELECT LS.SEED_ID,
                                 (SELECT M.USER_NAME
                                    FROM EMPLOYEE_MT M
                                   WHERE M.EMPLOYEE_ID = L.UPDATED_BY) VERIFY_BY,
                                 DECODE(((SELECT *
                                            FROM (SELECT M.USER_NAME
                                                    FROM LZ_LISTING_ALLOC L,
                                                         EMPLOYEE_MT      M
                                                   WHERE L.SEED_ID =
                                                         LS.SEED_ID
                                                     AND L.LISTER_ID =
                                                         M.EMPLOYEE_ID
                                                   ORDER BY L.ALLOC_ID DESC)
                                           WHERE ROWNUM <= 1)),
                                        NULL,
                                        'NOT ASSIGNED',
                                        ((SELECT *
                                            FROM (SELECT M.USER_NAME
                                                    FROM LZ_LISTING_ALLOC L,
                                                         EMPLOYEE_MT      M
                                                   WHERE L.SEED_ID =
                                                         LS.SEED_ID
                                                     AND L.LISTER_ID =
                                                         M.EMPLOYEE_ID
                                                   ORDER BY L.ALLOC_ID DESC)
                                           WHERE ROWNUM <= 1))) ASSIGN_TO,
                                 BB.BARCODE_NO,
                                 L.BARCODE_PRV_NO,
                                 LS.ENTERED_BY,
                                 E.USER_NAME,
                                 LS.SHIPPING_SERVICE,
                                 LS.OTHER_NOTES,
                                 LS.LZ_MANIFEST_ID,
                                 LM.LOADING_DATE,
                                 LM.PURCH_REF_NO,
                                 LS.ITEM_TITLE ITEM_MT_DESC,
                                 I.ITEM_MT_MANUFACTURE MANUFACTURER,
                                 I.ITEM_ID,
                                 I.ITEM_CODE,
                                 I.ITEM_MT_MFG_PART_NO MFG_PART_NO,
                                 I.ITEM_MT_UPC UPC,
                                 BB.BIN_ID,
                                 BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME,
                                 O.OBJECT_NAME OBJECT_DESCRIP,
                                 C.COND_NAME ITEM_CONDITION,
                                 '1' QUANTITY,
                                 L.FOLDER_NAME
                            FROM LZ_BARCODE_MT    BB,
                                 LZ_SPECIAL_LOTS  L,
                                 LZ_MANIFEST_DET  MDET,
                                 ITEMS_MT         I,
                                 LZ_ITEM_SEED     LS,
                                 LZ_MANIFEST_MT   LM,
                                 BIN_MT           BM,
                                 LZ_ITEM_COND_MT  C,
                                 LZ_BD_OBJECTS_MT O,
                                 EMPLOYEE_MT      E
                           WHERE BB.BARCODE_NO = L.BARCODE_PRV_NO
                             AND L.LZ_MANIFEST_DET_ID =
                                 MDET.LAPTOP_ZONE_ID
                             AND LM.LZ_MANIFEST_ID = MDET.LZ_MANIFEST_ID
                             AND LS.DEFAULT_COND = BB.CONDITION_ID
                             AND LS.ITEM_ID = BB.ITEM_ID
                             AND LS.LZ_MANIFEST_ID = BB.LZ_MANIFEST_ID
                             AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
                             AND BB.ITEM_ID = I.ITEM_ID
                             AND C.ID = L.CONDITION_ID
                             AND O.OBJECT_ID = L.OBJECT_ID
                             AND LS.ENTERED_BY = E.EMPLOYEE_ID(+)
                             AND BB.EBAY_ITEM_ID IS NULL
                             AND BB.BIN_ID = BM.BIN_ID
                             AND BB.DISCARD = 0)) Q
           WHERE M.MT_ID = D.MT_ID
             AND C.MERCHANT_ID = M.MERCHANT_ID
             AND D.BARCODE_NO = Q.BARCODE_PRV_NO)";

            if (!empty($employeeName)) {
                $qyer .= "WHERE upper(ASSIGN_TO)=UPPER('$employeeName')";
            }
            $qyer = $this->db->query($qyer)->result_array();

            if (count($qyer) >= 1) {

                $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
                $uri = $this->get_identiti_bar_pics($qyer, $conditions);
                $images = $uri['uri'];
                foreach ($qyer as $key => $value) {

                    $qyer[$key]['MFG_PART_NO'] = str_replace('-', ' ', $value['MFG_PART_NO']);
                }

                return array("images" => $images, 'query' => $qyer);
            } else {
                return array('images' => [], 'query' => [], 'exist' => false);
            }
        } elseif ($radioselect == 'SpecilalLots') {

            $qyer = "SELECT * from (SELECT LS.SEED_ID,
        (SELECT M.USER_NAME
           FROM EMPLOYEE_MT M
          WHERE M.EMPLOYEE_ID = L.UPDATED_BY) VERIFY_BY,
        DECODE(((SELECT *
                   FROM (SELECT M.USER_NAME
                           FROM LZ_LISTING_ALLOC L, EMPLOYEE_MT M
                          WHERE L.SEED_ID = LS.SEED_ID
                            AND L.LISTER_ID = M.EMPLOYEE_ID
                          ORDER BY L.ALLOC_ID DESC)
                  WHERE ROWNUM <= 1)),
               NULL,
               'NOT ASSIGNED',
               ((SELECT *
                   FROM (SELECT M.USER_NAME
                           FROM LZ_LISTING_ALLOC L, EMPLOYEE_MT M
                          WHERE L.SEED_ID = LS.SEED_ID
                            AND L.LISTER_ID = M.EMPLOYEE_ID
                          ORDER BY L.ALLOC_ID DESC)
                  WHERE ROWNUM <= 1))) ASSIGN_TO,
        BB.BARCODE_NO,
        L.BARCODE_PRV_NO,
        LS.ENTERED_BY,
        E.USER_NAME,
        LS.SHIPPING_SERVICE,
        LS.OTHER_NOTES,
        LS.LZ_MANIFEST_ID,
        LM.LOADING_DATE,
        LM.PURCH_REF_NO,
        LS.ITEM_TITLE ITEM_MT_DESC,
        I.ITEM_MT_MANUFACTURE MANUFACTURER,
        I.ITEM_ID,
        I.ITEM_CODE,
        I.ITEM_MT_MFG_PART_NO MFG_PART_NO,
        I.ITEM_MT_UPC UPC,
        BB.BIN_ID,
        BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME,
        O.OBJECT_NAME OBJECT_DESCRIP,
        C.COND_NAME ITEM_CONDITION,
        '1' QUANTITY,
        L.FOLDER_NAME
   FROM LZ_BARCODE_MT    BB,
        LZ_SPECIAL_LOTS  L,
        LZ_MANIFEST_DET  MDET,
        ITEMS_MT         I,
        LZ_ITEM_SEED     LS,
        LZ_MANIFEST_MT   LM,
        BIN_MT           BM,
        LZ_ITEM_COND_MT  C,
        LZ_BD_OBJECTS_MT O,
        EMPLOYEE_MT      E
  WHERE BB.BARCODE_NO = L.BARCODE_PRV_NO
    AND L.LZ_MANIFEST_DET_ID = MDET.LAPTOP_ZONE_ID
    AND LM.LZ_MANIFEST_ID = MDET.LZ_MANIFEST_ID
    AND LS.DEFAULT_COND = BB.CONDITION_ID
    AND LS.ITEM_ID = BB.ITEM_ID
    AND LS.LZ_MANIFEST_ID = BB.LZ_MANIFEST_ID
    AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
    AND BB.ITEM_ID = I.ITEM_ID
    AND C.ID = L.CONDITION_ID
    AND O.OBJECT_ID = L.OBJECT_ID
    AND LS.ENTERED_BY = E.EMPLOYEE_ID(+)
    AND BB.EBAY_ITEM_ID IS NULL
    AND BB.BIN_ID = BM.BIN_ID
    AND BB.DISCARD = 0)";
            if (!empty($employeeName)) {
                $qyer .= "WHERE upper(ASSIGN_TO)=UPPER('$employeeName')";
            }
            $qyer = $this->db->query($qyer)->result_array();

            if (count($qyer) >= 1) {

                $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
                $uri = $this->get_identiti_bar_pics($qyer, $conditions);
                $images = $uri['uri'];
                foreach ($qyer as $key => $value) {

                    $qyer[$key]['MFG_PART_NO'] = str_replace('-', ' ', $value['MFG_PART_NO']);
                }

                return array('images' => $images, 'query' => $qyer);
            } else {
                return array('images' => [], 'query' => [], 'exist' => false);
            }
        } elseif ($radioselect == 'DekittedItems') {
            $qyer = "SELECT * FROM (SELECT LS.SEED_ID,
            (SELECT M.USER_NAME
               FROM EMPLOYEE_mT M
              WHERE M.EMPLOYEE_ID = DET.IDENTIFIED_BY) VERIFY_BY,
            DECODE(((SELECT *
                       FROM (SELECT M.USER_NAME
                               FROM LZ_LISTING_ALLOC L,
                                    EMPLOYEE_MT      M
                              WHERE L.SEED_ID = LS.SEED_ID
                                AND L.LISTER_ID =
                                    M.EMPLOYEE_ID
                              ORDER BY L.ALLOC_ID DESC)
                      WHERE ROWNUM <= 1)),
                   NULL,
                   'NOT ASSIGNED',
                   ((SELECT *
                       FROM (SELECT M.USER_NAME
                               FROM LZ_LISTING_ALLOC L,
                                    EMPLOYEE_MT      M
                              WHERE L.SEED_ID = LS.SEED_ID
                                AND L.LISTER_ID =
                                    M.EMPLOYEE_ID
                              ORDER BY L.ALLOC_ID DESC)
                      WHERE ROWNUM <= 1))) ASSIGN_TO,
            BB.BARCODE_NO,
            DET.BARCODE_PRV_NO,
            LS.ENTERED_BY,
            E.USER_NAME,
            LS.SHIPPING_SERVICE,
            LS.OTHER_NOTES,
            LS.LZ_MANIFEST_ID,
            LM.LOADING_DATE,
            LM.PURCH_REF_NO,
            LS.ITEM_TITLE ITEM_MT_DESC,
            I.ITEM_MT_MANUFACTURE MANUFACTURER,
            I.ITEM_ID,
            I.ITEM_CODE,
            I.ITEM_MT_MFG_PART_NO MFG_PART_NO,
            I.ITEM_MT_UPC UPC,
            BB.BIN_ID,
            BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME,
            O.OBJECT_NAME OBJECT_DESCRIP,
            C.COND_NAME ITEM_CONDITION,
            '1' QUANTITY,
            DET.BARCODE_PRV_NO FOLDER_NAME
       FROM LZ_BARCODE_MT    BB,
            LZ_DEKIT_US_DT   DET,
            LZ_MANIFEST_DET  MDET,
            ITEMS_MT         I,
            LZ_ITEM_SEED     LS,
            LZ_MANIFEST_MT   LM,
            BIN_MT           BM,
            LZ_ITEM_COND_MT  C,
            LZ_BD_OBJECTS_MT O,
            EMPLOYEE_MT      E
      WHERE BB.BARCODE_NO = DET.BARCODE_PRV_NO
        AND DET.LZ_MANIFEST_DET_ID = MDET.LAPTOP_ZONE_ID
        AND LM.LZ_MANIFEST_ID = MDET.LZ_MANIFEST_ID
        AND LS.DEFAULT_COND = BB.CONDITION_ID
        AND LS.ITEM_ID = BB.ITEM_ID
        AND LS.LZ_MANIFEST_ID = BB.LZ_MANIFEST_ID
        AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
        AND C.ID = DET.CONDITION_ID
        AND BB.ITEM_ID = I.ITEM_ID
        AND O.OBJECT_ID = DET.OBJECT_ID
        AND LS.ENTERED_BY = E.EMPLOYEE_ID(+)
        AND BB.EBAY_ITEM_ID IS NULL
        AND BB.BIN_ID = BM.BIN_ID
        AND BB.DISCARD = 0)";

            if (!empty($employeeName)) {
                $qyer .= "WHERE upper(ASSIGN_TO)=UPPER('$employeeName')";
            }
            $qyer = $this->db->query($qyer)->result_array();
            if (count($qyer) >= 1) {

                $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();

                $uri = $this->get_identiti_bar_pics($qyer, $conditions);
                $images = $uri['uri'];
                foreach ($qyer as $key => $value) {

                    $qyer[$key]['MFG_PART_NO'] = str_replace('-', ' ', $value['MFG_PART_NO']);
                }

                return array("images" => $images, 'query' => $qyer);
            } else {
                return array('images' => [], 'query' => [], 'exist' => false);
            }
        }
    }

    public function get_identiti_bar_pics($barcodes, $conditions)
    {

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();

        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        // $base_url = 'http://71.78.236.20/';
        foreach ($barcodes as $barcode) {

            $bar = $barcode['BARCODE_PRV_NO'];

            if (!empty($bar)) {

                $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE lot.barcode_prv_no = '$bar' and rownum <= 1  ")->result_array();
            }

            $folderName = "";
            if ($getFolder) {
                $folderName = $getFolder[0]['FOLDER_NAME'];
            } else {
                $folderName = $bar;
            }

            $dir = "";
            $barcodePictures = $master_path . $folderName . "/";
            $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

            if (is_dir($barcodePictures)) {
                $dir = $barcodePictures;
            } else if (is_dir($barcodePicturesThumb)) {
                $dir = $barcodePicturesThumb;
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);

            if (is_dir($dir)) {
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

                if ($images) {
                    $j = 0;
                    foreach ($images as $image) {

                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                        $uri[$bar][$j] = $base_url . $withoutMasterPartUri;
                        // if($uri[$barcode['LZ_barcode_ID']]){
                        //     break;
                        // }

                        $j++;
                    }
                } else {
                    $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar][1] = false;
                }
            } else {
                $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar][1] = false;
            }
        }
        return array('uri' => $uri);
    }

    /********************************
     *  end Screen US-PK Non Listed Items
     *********************************/

    /********************************
     * start Screen US-PK Non Listed Items
     *********************************/

    public function Last_ten_barcode()
    {

        $last_ten = $this->db->query("SELECT * FROM (SELECT D.BARCODE_NO,I.ITEM_DESC,C.COND_NAME,I.ITEM_MT_MANUFACTURE,I.ITEM_MT_MFG_PART_NO,I.ITEM_MT_UPC FROM LZ_DEKIT_US_MT D, LZ_BARCODE_MT B,ITEMS_MT I,LZ_ITEM_COND_MT C WHERE D.BARCODE_NO = B.BARCODE_NO AND B.ITEM_ID = I.ITEM_ID AND B.CONDITION_ID = C.ID ORDER BY D.BARCODE_NO DESC) WHERE ROWNUM <= 10 ")->result_array();

        return $last_ten;
    }

    public function Get_master_Barcode()
    {
        $barcode = $this->input->post('bardcode');
        $master = $this->db->query("SELECT DE.ITEM_MT_DESC, DE.ITEM_MT_MFG_PART_NO, DE.CONDITIONS_SEG5
        FROM LZ_BARCODE_MT B, LZ_MANIFEST_DET DE, ITEMS_MT I
       WHERE B.BARCODE_NO = '$barcode'
         AND B.ITEM_ID = I.ITEM_ID
         AND I.ITEM_CODE = DE.LAPTOP_ITEM_CODE
         AND B.LZ_MANIFEST_ID = DE.LZ_MANIFEST_ID
         AND ROWNUM <= 1")->result_array();
        return $master;
    }

    public function Get_master_detail()
    {
        $barcode = $this->input->post('bardcode');
        $master = $this->db->query("SELECT D.LZ_DEKIT_US_DT_ID,
        C.COND_NAME,
        DECODE(D.PRINT_STATUS, 0, 'False', 1, 'True', D.PRINT_STATUS) PRINT_STATUS,
        D.BARCODE_PRV_NO,
        B.BIN_TYPE || '-' || B.BIN_NO BIN_NO,
        B.BIN_ID,
        O.OBJECT_NAME,
        O.OBJECT_ID,
        D.DEKIT_REMARKS,
        D.PIC_NOTES,
        D.WEIGHT,
        D.CONDITION_ID
   FROM LZ_DEKIT_US_MT   M,
        LZ_DEKIT_US_DT   D,
        LZ_BD_OBJECTS_MT O,
        BIN_MT           B,
        LZ_ITEM_COND_MT  C
  WHERE M.BARCODE_NO = '$barcode'
    AND M.LZ_DEKIT_US_MT_ID = D.LZ_DEKIT_US_MT_ID
    AND D.OBJECT_ID = O.OBJECT_ID(+)
    AND B.BIN_ID(+) = D.BIN_ID
    AND D.CONDITION_ID = C.ID(+)

  ORDER BY D.BARCODE_PRV_NO desc")->result_array();
        return $master;
    }

    public function Get_object_DrowpDown()
    {
        $master = $this->db->query("SELECT DISTINCT O.OBJECT_NAME , O.OBJECT_ID FROM LZ_BD_CAT_GROUP_DET DT,LZ_BD_OBJECTS_MT O WHERE DT.LZ_BD_GROUP_ID = 3 AND DT.CATEGORY_ID = O.CATEGORY_ID   ORDER BY O.OBJECT_ID DESC")->result_array();
        return $master;
    }

    public function Get_condition_DrowpDown()
    {
        // $master = $this->db->query("SELECT c.COND_NAME  , d.CONDITION_ID from LZ_ITEM_COND_MT c , LZ_DEKIT_US_DT d  where d.CONDITION_ID = c.ID(+)")->result_array();
        $master = $this->db->query("select c.id,c.cond_name from lz_item_cond_mt c")->result_array();

        return $master;
    }

    public function Get_bin_DrowpDown()
    {
        $master = $this->db->query("SELECT B.BIN_ID, B.BIN_TYPE ||'-'|| B.BIN_NO BIN_NO FROM BIN_MT B WHERE BIN_TYPE <> 'NA'AND BIN_TYPE IN( 'TC','NB') ORDER BY BIN_NO ASC")->result_array();
        return $master;
    }

    public function UpdateWeight()
    {
        $WEIGHT = $this->input->post('WEIGHT');
        // $value['PACKING_NAME'];
        $LZ_DEKIT_US_DT_ID = $this->input->post('LZ_DEKIT_US_DT_ID');

        $Data = $this->db->query("UPDATE LZ_DEKIT_US_DT set WEIGHT='$WEIGHT' where LZ_DEKIT_US_DT_ID='$LZ_DEKIT_US_DT_ID'");
        return $Data;
    }

    public function UpdateDekittingRemarks()
    {
        $DEKIT_REMARKS = $this->input->post('DEKIT_REMARKS');
        $DEKIT_REMARKS = trim(str_replace("  ", ' ', $DEKIT_REMARKS));
        $DEKIT_REMARKS = trim(str_replace(array("'"), "''", $DEKIT_REMARKS));
        // $value['PACKING_NAME'];
        $LZ_DEKIT_US_DT_ID = $this->input->post('LZ_DEKIT_US_DT_ID');

        $Data = $this->db->query("UPDATE LZ_DEKIT_US_DT set DEKIT_REMARKS='$DEKIT_REMARKS' where LZ_DEKIT_US_DT_ID='$LZ_DEKIT_US_DT_ID'");
        return $Data;
    }

    public function UpdateMasterDetial()
    {
        $objectdata = $this->input->post('objectdata');
        $conditiondata = $this->input->post('conditiondata');
        $LZ_DEKIT_US_DT_ID = $this->input->post('LZ_DEKIT_US_DT_ID');
        $Weight = $this->input->post('Weight');
        $selectBin = $this->input->post('selectBin');
        $DekittingRemarks = $this->input->post('DekittingRemarks');
        $DekittingRemarks = trim(str_replace("  ", ' ', $DekittingRemarks));
        $DekittingRemarks = trim(str_replace(array("'"), "''", $DekittingRemarks));

        $Data = $this->db->query("UPDATE LZ_DEKIT_US_DT set OBJECT_ID='$objectdata' ,CONDITION_ID='$conditiondata', BIN_ID='$selectBin'
        , WEIGHT='$Weight', DEKIT_REMARKS='$DekittingRemarks'  where LZ_DEKIT_US_DT_ID='$LZ_DEKIT_US_DT_ID'");
        return $Data;
    }

    public function DeleteMasterDetail()
    { // for template form
        $LZ_DEKIT_US_DT_ID = $this->input->post('id');
        $deleteTemp = $this->db->query("DELETE LZ_DEKIT_US_DT where LZ_DEKIT_US_DT_ID='$LZ_DEKIT_US_DT_ID'");
        return $deleteTemp;
    }

    public function SaveMasterDetail()
    {
        $master_barcode = $this->input->post('barcode');
        // var_dump( $master_barcode); exit;
        $objectdataUpdate = $this->input->post('objectdataUpdate');
        $conditiondataUpdate = $this->input->post('conditiondataUpdate');
        $WeightUpdate = $this->input->post('WeightUpdate');
        $selectBinUpdate = $this->input->post('BinUpdate');
        // var_dump($selectBinUpdate); exit;
        $DekittingRemarksUpdate = $this->input->post('DekittingRemarksUpdate');
        $DekittingRemarksUpdate = trim(str_replace("  ", ' ', $DekittingRemarksUpdate));
        $DekittingRemarksUpdate = trim(str_replace(array("'"), "''", $DekittingRemarksUpdate));
        //$barcode = $this->input->post('bardcode');
        $user_id = $this->input->post('created_by');
        date_default_timezone_set("America/Chicago");
        $date = date('Y-m-d H:i:s');
        $dekit_date = "TO_DATE('" . $date . "', 'YYYY-MM-DD HH24:MI:SS')";

        // check for items is alredy dekited
        //**********************************
        $load_dekit_check = $this->db->query("SELECT B.BARCODE_NO,B.ITEM_ID FROM LZ_BARCODE_MT B, LZ_MANIFEST_DET DE,ITEMS_MT I WHERE B.BARCODE_NO = $master_barcode AND B.ITEM_ID = I.ITEM_ID AND I.ITEM_CODE = DE.LAPTOP_ITEM_CODE AND B.LZ_MANIFEST_ID = DE.LZ_MANIFEST_ID AND ROWNUM <=1 ");
        // var_dump($load_dekit_check);exit;
        if ($load_dekit_check->num_rows() > 0) {
            $get_item = $load_dekit_check->result_array();
            $item_id = $get_item[0]['ITEM_ID'];
            $gl_gen = $this->db->query(" SELECT LZ_ITEM_ADJ_BOOK_ID  FROM GL_GEN_PREFERENCES GD ")->result_array();
            $gen_id = $gl_gen[0]['LZ_ITEM_ADJ_BOOK_ID'];

            $inv_book_id = $this->db->query(" SELECT S.DEF_LOCATOR_CODE_ID  FROM INV_BOOKS_MT B, SUB_INVENTORY_MT S WHERE INV_BOOK_ID = $gen_id AND B.SUB_INV_ID = S.SUB_INV_ID ")->result_array();
            $def_loc_id = $inv_book_id[0]['DEF_LOCATOR_CODE_ID'];

            $adjus_no = $this->db->query(" SELECT TO_CHAR(SYSDATE,'YY')||'-'|| LPAD('8',4,'0') ADJUST_REF_NO   FROM DUAL ")->result_array();
            $adjus = $adjus_no[0]['ADJUST_REF_NO'];

            $inv_book = $this->db->query(" SELECT DOC_SEQ_ID FROM   INV_SEQUENCE_ASSIGNMENT WHERE  inv_book_id = 8 ")->result_array();
            $seq_id = $inv_book[0]['DOC_SEQ_ID'];

            $last = $this->db->query("SELECT LAST_NO +1 LAST_NO, DOC_DET_SEQ_ID FROM   DOC_SEQUENCE_DETAIL WHERE  DOC_DET_SEQ_ID = (SELECT DOC_DET_SEQ_ID FROM   DOC_SEQUENCE_DETAIL WHERE  DOC_SEQ_ID = $seq_id AND TO_DATE('3/1/2017','DD-MM-YYYY') >= FROM_DATE AND TO_DATE('3/1/2017','DD-MM-YYYY') <= TO_DATE AND ROWNUM = 1)")->result_array();
            $last_no = $last[0]['LAST_NO'];
            $doc_det_seq_id = $last[0]['DOC_DET_SEQ_ID'];
            // **** variable declaration for insertion into ITEM_ADJUSTMENT_MT,ITEM_ADJUSTMENT_det end*
            //******************************************************************************************

            // **** code for insertion into ITEM_ADJUSTMENT_MT,ITEM_ADJUSTMENT_det start ****
            //**********************************************************************
            $adjs_mt = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('ITEM_ADJUSTMENT_MT', 'ITEM_ADJUSTMENT_ID')ID FROM DUAL")->result_array();
            $adjs_mt_pk = $adjs_mt[0]['ID'];

            $insert_adjus_mt = "INSERT INTO ITEM_ADJUSTMENT_MT(ITEM_ADJUSTMENT_ID, INV_BOOK_ID, ADJUSTMENT_NO, ADJUSTMENT_DATE, STOCK_TRANS_YN, REMARKS,INV_TRANSACTION_NO, JOURNAL_ID, POST_TO_GL, ENTERED_BY, AUTH_ID, AUTHORIZED_YN, SEND_FOR_AUTH, AUTH_STATUS_ID,                              ADJUSTMENT_REF_NO)
                        VALUES('$adjs_mt_pk', 8, '$last_no', to_date(sysdate), 0, NULL, NULL, NULL, 0, '$user_id', null, 0, 0, 0, '$adjus')";
            $this->db->query($insert_adjus_mt);
            // var_dump($insert_adjus_mt);exit;

            $adjs_det = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('ITEM_ADJUSTMENT_DET ', 'ITEM_ADJUSTMENT_DET_ID')ID FROM DUAL")->result_array();
            $adjs_det_pk = $adjs_det[0]['ID'];

            $insert_adjus_det = "INSERT INTO ITEM_ADJUSTMENT_DET(ITEM_ADJUSTMENT_DET_ID, ITEM_ADJUSTMENT_ID, ITEM_ID, SR_NO, LOC_CODE_COMB_ID, PRIMARY_QTY,SECONDARY_QTY, LINE_AMOUNT, CONTRA_ACC_CODE_COMB_ID, REMARKS )
                        VALUES('$adjs_det_pk', '$adjs_mt_pk', '$item_id', 1, '$def_loc_id', -1, NULL, 99, NULL, NULL )"; /// $cost variable query
            $this->db->query($insert_adjus_det);

            $this->db->query("UPDATE DOC_SEQUENCE_DETAIL  SET LAST_NO =$last_no where DOC_DET_SEQ_ID =$doc_det_seq_id");
            $this->db->query("UPDATE LZ_BARCODE_MT SET ITEM_ADJ_DET_ID_FOR_OUT = $adjs_mt_pk WHERE BARCODE_NO = $master_barcode");
            // **** code for insertion into ITEM_ADJUSTMENT_MT,ITEM_ADJUSTMENT_DET end
            //************************************************************************

            // **** code for insertion into lz_manifest_mt start
            //************************************************************************
            $max_query = $this->db->query("SELECT get_single_primary_key('LZ_MANIFEST_MT','LOADING_NO') ID FROM DUAL");
            $rs = $max_query->result_array();
            $loading_no = $rs[0]['ID'];

            // date_default_timezone_set("America/Chicago");
            // $loading_date = date("Y-m-d H:i:s");
            // $loading_date = "TO_DATE('".$loading_date."', 'YYYY-MM-DD HH24:MI:SS')";
            date_default_timezone_set("America/Chicago");
            $loading_date = date("Y-m-d H:i:s");
            $load_date = "TO_DATE('$loading_date', 'YYYY-MM-DD HH24:MI:SS')";

            $purch_ref_no = 'dk_' . $loading_no;
            $supplier_id = 'null';
            $remarks = $this->input->post('DekittingRemarksUpdate');
            $remarks = trim(str_replace("  ", ' ', $remarks));
            $remarks = trim(str_replace(array("'"), "''", $remarks));
            $doc_seq_id = 30;
            $purchase_date = 'null';
            $posted = 'Posted';
            $excel_file_name = 'null';
            $grn_id = 'null';
            $purchase_invoice_id = 'null';
            $single_entry_id = 'null';
            $total_excel_rows = 'null';
            $manifest_name = 'null';
            $manifest_status = 'null';
            $sold_price = 'null';
            $end_date = 'null';
            $lz_offer = 'null';
            $manifest_type = 3; // for dekit-us-pk manifest generation
            $est_mt_id = 'null';

            /*--- Get Single Primary Key for LZ_MANIFEST_MT start---*/
            $get_mt_pk = $this->db->query("SELECT get_single_primary_key('LZ_MANIFEST_MT','LZ_MANIFEST_ID') LZ_MANIFEST_ID FROM DUAL");
            $get_mt_pk = $get_mt_pk->result_array();
            $lz_manifest_id = $get_mt_pk[0]['LZ_MANIFEST_ID'];
            /*--- Get Single Primary Key for LZ_MANIFEST_MT end---*/

            /*--- Insertion Query for LZ_MANIFEST_MT start---*/
            $mt_qry = "INSERT INTO LZ_MANIFEST_MT
    (LZ_MANIFEST_ID,
     LOADING_NO,
     LOADING_DATE,
     PURCH_REF_NO,
     SUPPLIER_ID,
    REMARKS,
     DOC_SEQ_ID,
     PURCHASE_DATE,
     POSTED,
     EXCEL_FILE_NAME,
     GRN_ID,
     PURCHASE_INVOICE_ID,
     SINGLE_ENTRY_ID,
     TOTAL_EXCEL_ROWS,
     MANIFEST_NAME,
     MANIFEST_STATUS,
     SOLD_PRICE,
     END_DATE,
     LZ_OFFER,
     MANIFEST_TYPE,
     EST_MT_ID)
  VALUES
    ($lz_manifest_id,
     $loading_no,
     sysdate,
     '$purch_ref_no',
     $supplier_id,
     '$remarks',
     $doc_seq_id,
     $purchase_date,
     '$posted',
     $excel_file_name,
     $grn_id,
     $purchase_invoice_id,
     $single_entry_id,
     $total_excel_rows,
     $manifest_name,
     $manifest_status,
     $sold_price,
     $end_date,
     $lz_offer,
     $manifest_type,
     $est_mt_id)";
            $mt_qry = $this->db->query($mt_qry);
            /*--- Insertion Query for LZ_MANIFEST_MT end---*/

            // **** code for insertion into lz_manifest_mt end
            //************************************************************************

            $lz_dekit_us_mt_id = $this->db->query("SELECT M.LZ_DEKIT_US_MT_ID  FROM LZ_DEKIT_US_MT M WHERE M.BARCODE_NO = $master_barcode")->result_array();
            //$lz_dekit_us_mt_id = $lz_dekit_us_mt_id[0]['LZ_DEKIT_US_MT_ID'];
            // var_dump( $lz_dekit_us_mt_id ); exit;
            if (count($lz_dekit_us_mt_id) > 0) {
                $lz_dekit_us_mt_id = $lz_dekit_us_mt_id[0]['LZ_DEKIT_US_MT_ID'];
            } else {
                $query = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_DEKIT_US_MT', 'LZ_DEKIT_US_MT_ID')ID FROM DUAL")->result_array();

                $lz_dekit_us_mt_id = $query[0]['ID'];

                $save_lz_dekit_us_mt = ("INSERT INTO LZ_DEKIT_US_MT(LZ_DEKIT_US_MT_ID,BARCODE_NO,DEKIT_BY,DEKIT_DATE_TIME,ADJUST_MT_ID,LZ_MANIFEST_MT_ID)
                                    VALUES('$lz_dekit_us_mt_id','$master_barcode','$user_id',sysdate,null,null)");
                $this->db->query($save_lz_dekit_us_mt);
            }
            $sequn = $this->db->query("SELECT /*GET_SINGLE_PRIMARY_KEY('LZ_BARCODE_MT','BARCODE_NO')*/SEQ_BARCODE_NO.NEXTVAL as ID FROM DUAL")->result_array();
            $bar_sq = $sequn[0]['ID'];
            $qry = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_DEKIT_US_DT', 'LZ_DEKIT_US_DT_ID')ID FROM DUAL");
            $qry = $qry->result_array();
            $lz_dekit_us_dt_id = $qry[0]['ID'];

            $getlatestData = $this->db->query("INSERT INTO LZ_DEKIT_US_DT (LZ_DEKIT_US_DT_ID,LZ_DEKIT_US_MT_ID,BARCODE_PRV_NO,OBJECT_ID,BIN_ID,PIC_DATE_TIME,PIC_BY,CATALOG_MT_ID,IDENT_DATE_TIME,IDENTIFIED_BY,DEKIT_REMARKS,WEIGHT,CONDITION_ID) VALUES('$lz_dekit_us_dt_id','$lz_dekit_us_mt_id','$bar_sq','$objectdataUpdate','$selectBinUpdate',null,null,null,null,null,'$DekittingRemarksUpdate','$WeightUpdate','$conditiondataUpdate')");
            // **** code for insertion into LZ_DEKIT_US_MT,LZ_DEKIT_US_DT end ****
            //**********************************************************************
            $this->db->query("UPDATE LZ_DEKIT_US_MT  SET ADJUST_MT_ID ='$adjs_mt_pk' ,LZ_MANIFEST_MT_ID = $lz_manifest_id  where LZ_DEKIT_US_MT_ID = $lz_dekit_us_mt_id");

            //code section for saving master barcode mpn start
            $item_ids = $this->db->query("SELECT K.ITEM_ID FROM LZ_BARCODE_MT K WHERE K.BARCODE_NO = $master_barcode")->result_array();
            $item_id = $item_ids[0]['ITEM_ID'];

            $mpns = $this->db->query("SELECT T.ITEM_MT_MFG_PART_NO FROM ITEMS_MT T WHERE T.ITEM_ID = $item_id")->result_array();
            $mpn = $mpns[0]['ITEM_MT_MFG_PART_NO'];

            if (!empty($mpn)) {
                $item_mpn = strtoupper($mpn);

                $get_category = $this->db->query("SELECT de.e_bay_cata_id_seg6 CATEGORY_ID FROM LZ_BARCODE_MT B, LZ_MANIFEST_DET DE,ITEMS_MT I WHERE B.BARCODE_NO = $master_barcode AND B.ITEM_ID = I.ITEM_ID AND I.ITEM_CODE = DE.LAPTOP_ITEM_CODE AND B.LZ_MANIFEST_ID = DE.LZ_MANIFEST_ID AND ROWNUM <=1 ")->result_array();

                // old barcode chek$get_category = $this->db->query("SELECT S.CATEGORY_ID FROM LZ_BARCODE_MT B ,LZ_MANIFEST_MT M ,LZ_SINGLE_ENTRY S WHERE B.BARCODE_NO = $master_barcode AND B.LZ_MANIFEST_ID = M.LZ_MANIFEST_ID AND M.SINGLE_ENTRY_ID  = S.ID")->result_array();
                //$get_category->result_array();
                $category_id = $get_category[0]['CATEGORY_ID'];

                if (!empty($category_id)) {

                    $mpn_data = $this->db->query("SELECT MT.CATALOGUE_MT_ID FROM LZ_CATALOGUE_MT MT WHERE UPPER(MT.MPN) = '$item_mpn' AND MT.CATEGORY_ID = $category_id");

                    //if(!empty($master_mpn_id)){
                    if ($mpn_data->num_rows() > 0) {
                        $mpn_data = $mpn_data->result_array();
                        $master_mpn_id = $mpn_data[0]['CATALOGUE_MT_ID'];
                        $this->db->query("UPDATE LZ_DEKIT_US_MT SET MASTER_MPN_ID = $master_mpn_id WHERE LZ_DEKIT_US_MT_ID =$lz_dekit_us_mt_id ");
                    }
                }
            }

            $getlatestData = $this->db->query("SELECT D.LZ_DEKIT_US_DT_ID,
        C.COND_NAME,
        DECODE(D.PRINT_STATUS, 0, 'False', 1, 'True', D.PRINT_STATUS) PRINT_STATUS,
        D.BARCODE_PRV_NO,
        B.BIN_TYPE || '-' || B.BIN_NO BIN_NO,
        B.BIN_ID,
        O.OBJECT_NAME,
        O.OBJECT_ID,
        D.DEKIT_REMARKS,
        D.PIC_NOTES,
        D.WEIGHT,
        D.CONDITION_ID
   FROM
        LZ_DEKIT_US_DT   D,
        LZ_BD_OBJECTS_MT O,
        BIN_MT           B,
        LZ_ITEM_COND_MT  C
  WHERE D.OBJECT_ID = O.OBJECT_ID(+)
    AND B.BIN_ID(+) = D.BIN_ID
    AND D.CONDITION_ID = C.ID(+)
    and D.LZ_DEKIT_US_DT_ID = '$lz_dekit_us_dt_id'

  ORDER BY D.BARCODE_PRV_NO desc")->result_array();

            return array("status" => true, 'getlatestData' => $getlatestData);
        } else {
            return array("status" => false, 'getlatestData' => '');
        }
    }

    public function print_us_pk()
    {

        //$lz_dekit_us_dt_id = $this->input->post('id');
        $lz_dekit_us_dt_id = $_GET['id'];
        //$lz_dekit_us_dt_id = $this->uri->segment(4);
        // var_dump( $lz_dekit_us_dt_id); exit;
        // $manifest_id =    $this->uri->segment(5);
        // $barcode = $this->uri->segment(6);
        $print_qry = $this->db->query("SELECT CO.COND_NAME ITEM_DESC, D.LZ_DEKIT_US_DT_ID, D.BARCODE_PRV_NO BAR_CODE, O.OBJECT_NAME, '1' LOT_NO, 'PO_DETAIL_LOT_REF' PO_DETAIL_LOT_REF, 'UNIT_NO' UNIT_NO, '1' LOT_QTY, 'MB' || '-' || MT.BARCODE_NO BARCODE_NO FROM LZ_DEKIT_US_DT D,LZ_DEKIT_US_MT MT ,LZ_BD_OBJECTS_MT O, BIN_MT B, LZ_ITEM_COND_MT CO WHERE D.CONDITION_ID = CO.ID(+) AND D.OBJECT_ID = O.OBJECT_ID(+) AND D.LZ_DEKIT_US_MT_ID = MT.LZ_DEKIT_US_MT_ID AND D.BIN_ID = B.BIN_ID(+) AND D.LZ_DEKIT_US_DT_ID = $lz_dekit_us_dt_id "); //$query = $this->db->query("UPDATE LZ_BARCODE_MT SET PRINT_STATUS = 1 WHERE BARCODE_NO = $barcode");
        //var_dump($print_qry);exit;
        return $print_qry->result_array();
    }
    public function print_all_us_pk()
    {
        //$master_barcode = $this->session->userdata('ctc_kit_barcode');
        //  $master_barcode = $this->uri->segment(4);
        $master_barcode = $_GET['id'];

        $listing_qry = $this->db->query("SELECT CO.COND_NAME ITEM_DESC ,'MB' || '-' || M.BARCODE_NO BARCODE_NO,D.barcode_prv_no BAR_CODE, o.object_name, '1' LOT_NO, 'PO_DETAIL_LOT_REF' PO_DETAIL_LOT_REF, 'UNIT_NO' UNIT_NO, '1' LOT_QTY FROM LZ_DEKIT_US_MT M, LZ_DEKIT_US_DT D, LZ_BD_OBJECTS_MT O, BIN_MT B,lz_item_cond_mt co WHERE M.BARCODE_NO = $master_barcode AND M.LZ_DEKIT_US_MT_ID = D.LZ_DEKIT_US_MT_ID AND D.PRINT_STATUS = 0 and d.condition_id = co.id(+) AND D.OBJECT_ID = O.OBJECT_ID(+) AND D.BIN_ID = B.BIN_ID (+) order by d.barcode_prv_no Desc")->result_array();

        $this->db->query("UPDATE LZ_DEKIT_US_DT L SET L.PRINT_STATUS = 1 WHERE L.BARCODE_PRV_NO IN ( SELECT D.BARCODE_PRV_NO BAR_CODE FROM LZ_DEKIT_US_MT   M, LZ_DEKIT_US_DT   D, LZ_BD_OBJECTS_MT O, BIN_MT B, LZ_ITEM_COND_MT  CO WHERE M.BARCODE_NO = $master_barcode AND M.LZ_DEKIT_US_MT_ID = D.LZ_DEKIT_US_MT_ID AND D.CONDITION_ID = CO.ID(+) AND D.OBJECT_ID = O.OBJECT_ID(+) AND D.BIN_ID = B.BIN_ID(+) AND D.PRINT_STATUS = 0 ) ");
        return $listing_qry;
    }

    /********************************
     * end Screen DE-Kitting - U.S.
     *********************************/

    /********************************
     * start Screen post Item returns
     *********************************/
    public function displayBarcode()
    {
        $bar = $this->input->post('id');
        // $qry = $this->db->query("select * from lz_barcode_mt b where b.order_id = '$bar'");
        $qry = $this->db->query("SELECT * from lz_barcode_mt b, bin_mt bb where bb.bin_id = b.bin_id and b.order_id = '$bar'");

        return $qry->result_array();
    }

    public function save_data_manualy($merchantArray)
    {
        $buyername = $this->input->post('buyername');
        $reason = $this->input->post('reason');
        $comments = $this->input->post('comments');
        $comments = trim(str_replace("  ", ' ', $comments));
        $comments = trim(str_replace(array("'"), "''", $comments));
        $date = $this->input->post('date');
        $amount = $this->input->post('amount');
        $daItemDescriptiont = $this->input->post('daItemDescriptiont');
        // $selectMarchant2 = $this->input - post('selectMarchant2');
        $returnid = $this->input->post('returnid');
        $TRANSACTION_ID = $this->input->post('TRANSACTION_ID');
        //var_dump($TRANSACTION_ID);
        $ItemId = $this->input->post('ItemId');
        $SelerTotalRefund = $this->input->post('SelerTotalRefund');
        // echo "<pre>";
        // print_r($merchantArray);exit;
        $fromdate = new DateTime($date);
        $checkissuedate2 = date_format($fromdate, 'd-m-y');
        // var_dump($checkissuedate2);exit;
        if (is_array($merchantArray[0])) {
            $merchArrayvalue = $merchantArray[0]['value'];
            $merchArraylable = $merchantArray[0]['label'];
            ///var_dump( $merchArrayvalue,  $merchArraylable, $checkissuedate2); exit;

            $returnURL = 'https://www.ebay.com/rtn/Return/ReturnsDetail?returnId=' . $returnid;

            //var_dump($returnURL); exit;
            $check = $this->db->query("SELECT * FROM LJ_ITEM_RETURNS A WHERE A.RETURNID = '$returnid'")->result_array();
            if (count($check) === 0) {
                $get_pk = $this->db->query("SELECT get_single_primary_key('LJ_ITEM_RETURNS', 'ITEM_RET_ID') ITEM_RET_ID from dual")->result_array();
                $ITEM_RET_ID = $get_pk[0]['ITEM_RET_ID'];
                // var_dump( $ITEM_RET_ID); exit;
                if (empty($returnid)) {
                    $returnid = -1 * $ITEM_RET_ID;
                    //var_dump($returnid); exit;
                }
                $qry = $this->db->query("INSERT INTO LJ_ITEM_RETURNS
        (ITEM_RET_ID,
         RETURNID,
         BUYERLOGINNAME,
         SELLERLOGINNAME,
         RETURN_TYPE,
         RETURN_STATE,
         STATUS,
         ITEMID,
         TRANSACTIONID,
         RETURNQUANTITY,
         TYPE,
         REASON,
         COMMENTS,
         CREATIONDATE,
         SELLERTOTALREFUND,
         SELLERCURRENCY,
         BUYERTOTALREFUND,
         BUYERCURRENCY,
         SELLERRESPONSEDUE,
         RESPONDBYDATE,
         BUYERESCALATIONINFO,
         SELLERESCALATIONINFO,
         ACTIONURL,
         BUYERRESPONSEDUE,
         BUYERRESPONDBYDATE,
         lz_seller_acct_id,
         INSERTED_DATE
         )values(
                $ITEM_RET_ID,
               '$returnid',
               '$buyername',
               '$merchArraylable',
               'MONEY_BACK',
                 null,
               null,
               '$ItemId',
               '$TRANSACTION_ID',
               '$amount',
                null,
               '$reason',
               '$comments',
               TO_DATE('$checkissuedate2','DD-MM-YY HH24:MI:SS'),
               --TO_CHAR(DT.STATUS_DATE,'DD-MM-YY HH24:MI:SS') STATUS_DATE,
               '$SelerTotalRefund',
                 null,
               '$SelerTotalRefund',
               null,
                sysdate,
               sysdate,
               null,
                null,
               '$returnURL',
               null,
               sysdate,
               '$merchArrayvalue',
               sysdate
              )");
                // echo $this->db->last_query();
                //exit();
                $data = $qry;
                $get_data = $this->db->query("SELECT ITEM_RET_ID FROM LJ_ITEM_RETURNS where ITEM_RET_ID = '$ITEM_RET_ID'")->result_array();
                //$data = $this->db->query($get_data)->;
                $data1 = $get_data[0]['ITEM_RET_ID'];
                // var_dump($data1); exit;
                $data = $this->db->query("SELECT
               ITEM_RET_ID,
               RETURNID,
               BUYERLOGINNAME,
               SELLERLOGINNAME,
               RETURN_TYPE,
               RETURN_STATE,
               STATUS,
               ITEMID,
               TRANSACTIONID,
               RETURNQUANTITY,
               TYPE,
               REASON,
               COMMENTS,
               CREATIONDATE,
               SELLERTOTALREFUND,
               SELLERCURRENCY,
               BUYERTOTALREFUND,
               BUYERCURRENCY,
               SELLERRESPONSEDUE,
               RESPONDBYDATE,
               BUYERESCALATIONINFO,
               SELLERESCALATIONINFO,
               ACTIONURL,
               BUYERRESPONSEDUE,
               BUYERRESPONDBYDATE,
               lz_seller_acct_id,
               INSERTED_DATE ,
               RETURN_PROCESSED from LJ_ITEM_RETURNS where ITEM_RET_ID ='$data1'")->result_array();
                return array('status' => true, 'data' => $data, 'return_id' => $returnid);
            } else {
                return array('status' => false, 'message' => 'Return Id allready Exist');
            }
        }
    }

    public function Post_item_returns()
    {
        $qry = $this->db->query("SELECT ITEM_RET_ID,
                RETURNID,
                BUYERLOGINNAME,
                SELLERLOGINNAME,
                RETURN_TYPE,
                RETURN_STATE,
                r.STATUS,
                ITEMID,
                TRANSACTIONID,
                ITEMID ||'-'||  TRANSACTIONID ORDER_ID,
                RETURNQUANTITY,
                TYPE,
                REASON,
                COMMENTS,
                TO_CHAR(CREATIONDATE, 'DD-MM-YYYY HH24:MI:SS') CREATIONDATE,
                '$' || SELLERTOTALREFUND SELLERTOTALREFUND,
                SELLERCURRENCY,
                BUYERTOTALREFUND,
                BUYERCURRENCY,
                SELLERRESPONSEDUE,
                RESPONDBYDATE,
                BUYERESCALATIONINFO,
                SELLERESCALATIONINFO,
                ACTIONURL,
                BUYERRESPONSEDUE,
                BUYERRESPONDBYDATE,
                INSERTED_DATE,
                ee.ebay_item_desc,
                RETURN_PROCESSED,
                EM.USER_NAME RETURN_BY,
                TO_CHAR((SELECT REPLACE(REPLACE(XMLAGG(XMLELEMENT(A, RB.BARCODE_NO) ORDER BY RB.BARCODE_NO DESC NULLS LAST)
                                        .GETCLOBVAL(),
                                        '<A>',
                                        ''),
                                '</A>',
                                ', ') AS BARCODE_NO FROM LJ_RETURNED_BARCODE_MT RB , LJ_ITEM_RETURNED_MT rm
                            WHERE Rm.Returned_Id = RB.RETURNED_ID
                            and rm.return_id = r.returnid)) BARCODES


           from LJ_ITEM_RETURNS r,
           lj_item_returned_mt rm,
       employee_mt em,
                (select *
                   from ebay_list_mt e
                  where e.list_id in
                        (select max(list_id) from ebay_list_mt group by ebay_item_id)) ee
          where ee.ebay_item_id = itemid  and r.returnid = rm.return_id(+)
  and rm.return_by = em.employee_id order by ITEM_RET_ID desc");
        // $data = $this->db->query($qry);
        $qry = $qry->result_array();

        foreach ($qry as $key => $value) {
            // $item_title = trim(str_replace(" ", ' ', $item_title));
            $qry[$key]['RETURN_TYPE'] = str_replace('_', ' ', $value['RETURN_TYPE']);
            $qry[$key]['RETURN_STATE'] = str_replace('_', ' ', $value['RETURN_STATE']);
            $qry[$key]['STATUS'] = str_replace('_', ' ', $value['STATUS']);
            $qry[$key]['TYPE'] = str_replace('_', ' ', $value['TYPE']);
            $qry[$key]['REASON'] = str_replace('_', ' ', $value['REASON']);
            $qry[$key]['SELLERRESPONSEDUE'] = str_replace('_', ' ', $value['SELLERRESPONSEDUE']);
            $qry[$key]['BUYERRESPONSEDUE'] = str_replace('_', ' ', $value['BUYERRESPONSEDUE']);
        }

        return $qry;
    }

    public function InsertedDate($dataArray)
    {
        $merChant_ID = $this->input->post('merchant_id');
        if (!empty($merChant_ID)) {

            $merchantQry = "and r.LZ_SELLER_ACCT_ID =" . $merChant_ID;
        } else {
            $merchantQry = "";
        }

        if (is_array($dataArray[0])) {
            $startDate = date_create($dataArray[0][0]);
            //$startDate = date_format(@$startDate, 'd/m/Y');
            $startDate = date_format(@$startDate, 'Y/m/d');
            $endDate = date_create($dataArray[0][1]);
            $endDate = date_format(@$endDate, 'Y/m/d');

            //$dateQry = "and r.creationdate between TO_DATE('$startDate', 'DD/MM/YYYY') and TO_DATE('$endDate', 'DD/MM/YYYY')";
            $dateQry = "and r.creationdate between TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') and TO_DATE('$endDate 23:59:59',  'YYYY/MM/DD HH24:MI:SS')";
            //TO_DATE('2019/05/22 00:00:00', 'YYYY/MM/DD HH24:MI:SS') and TO_DATE('2019/05/22 23:59:59', 'YYYY/MM/DD HH24:MI:SS')

        } else {
            $dateQry = "";
        }
        $qry = $this->db->query("SELECT ITEM_RET_ID,
        RETURNID,
        BUYERLOGINNAME,
        SELLERLOGINNAME,
        RETURN_TYPE,
        RETURN_STATE,
        r.STATUS,
        ITEMID,
        TRANSACTIONID,
        RETURNQUANTITY,
        TYPE,
        REASON,
        COMMENTS,
        TO_CHAR(CREATIONDATE, 'DD-MM-YYYY HH24:MI:SS') CREATIONDATE,
        '$' || SELLERTOTALREFUND SELLERTOTALREFUND,
        SELLERCURRENCY,
        BUYERTOTALREFUND,
        BUYERCURRENCY,
        SELLERRESPONSEDUE,
        RESPONDBYDATE,
        BUYERESCALATIONINFO,
        SELLERESCALATIONINFO,
        ACTIONURL,
        BUYERRESPONSEDUE,
        BUYERRESPONDBYDATE,
        INSERTED_DATE,
        ee.ebay_item_desc,
        RETURN_PROCESSED,
        TO_CHAR((SELECT REPLACE(REPLACE(XMLAGG(XMLELEMENT(A, RB.BARCODE_NO) ORDER BY RB.BARCODE_NO DESC NULLS LAST)
                                .GETCLOBVAL(),
                                '<A>',
                                ''),
                        '</A>',
                        ', ') AS BARCODE_NO FROM LJ_RETURNED_BARCODE_MT RB , LJ_ITEM_RETURNED_MT rm
                    WHERE Rm.Returned_Id = RB.RETURNED_ID
                    and rm.return_id = r.returnid)) BARCODES

   from LJ_ITEM_RETURNS r,
        (select *
           from ebay_list_mt e
          where e.list_id in
                (select max(list_id) from ebay_list_mt group by ebay_item_id)) ee
  where ee.ebay_item_id = itemid $merchantQry $dateQry order by ITEM_RET_ID desc");
        $qry = $qry->result_array();
        foreach ($qry as $key => $value) {
            // $item_title = trim(str_replace(" ", ' ', $item_title));
            $qry[$key]['RETURN_TYPE'] = str_replace('_', ' ', $value['RETURN_TYPE']);
            $qry[$key]['RETURN_STATE'] = str_replace('_', ' ', $value['RETURN_STATE']);
            $qry[$key]['STATUS'] = str_replace('_', ' ', $value['STATUS']);
            $qry[$key]['TYPE'] = str_replace('_', ' ', $value['TYPE']);
            $qry[$key]['REASON'] = str_replace('_', ' ', $value['REASON']);
            $qry[$key]['SELLERRESPONSEDUE'] = str_replace('_', ' ', $value['SELLERRESPONSEDUE']);
            $qry[$key]['BUYERRESPONSEDUE'] = str_replace('_', ' ', $value['BUYERRESPONSEDUE']);
        }
        return $qry;
    }

    public function Process_Return()
    {
        $return_id = trim($this->input->post("return_Id"));
        $return_status = trim($this->input->post("radio_input"));
        $return_by = trim($this->input->post("userId"));
        $remarks = trim($this->input->post("Remarks_input"));
        $remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = trim(str_replace(array("'"), "''", $remarks));
        $bin = trim($this->input->post("location_input"));
        $barcode_no = trim($this->input->post("barcode_no"));
        $barcode = explode(',', $barcode_no);
        // echo $remarks;
        // exit();
        //  var_dump($barcode); exit;
        //$barcodes=explode(',',)
        // var_dump($return_id,  $return_status , $return_by ,  $remarks,$bin); exit;
        if (!empty($bin)) {
            $bin = $this->db->query("SELECT BB.BIN_ID FROM BIN_MT BB WHERE BB.BIN_TYPE||'-'||BB.BIN_NO = '$bin'")->result_array();
            $bin_id = $bin[0]['BIN_ID'];
        } else {
            $bin_id = 0;
        }
        $results = "";

        $OUT_MESSAGE = 0;

        //$OUT_MESSAGE = 0;

        foreach ($barcode as $bar) {

            // $bar = 280292;

            // $stid = oci_parse($this->db->conn_id, 'BEGIN pro_processReturn(:PARM_RETURN,:PARM_STATUS,:PARM_BY,:PARM_REMARK,:PARM_BIN,:PARM_BAR,:OUT_MESSAGE); end;');
            // oci_bind_by_name($stid, ':PARM_RETURN', $return_id, 100, SQLT_INT);
            // oci_bind_by_name($stid, ':PARM_STATUS', $return_status,200, SQLT_CHR);
            // oci_bind_by_name($stid, ':PARM_BY', $return_by, 100, SQLT_INT);
            // oci_bind_by_name($stid, ':PARM_REMARK', $remarks,400, SQLT_CHR);
            // oci_bind_by_name($stid, ':PARM_BIN', $bin_id, 100, SQLT_INT);
            // oci_bind_by_name($stid, ':PARM_BAR', $bar, 100, SQLT_INT);
            // oci_bind_by_name($stid, ':OUT_MESSAGE', $OUT_MESSAGE ,100, SQLT_INT);
            // if(oci_execute($stid,OCI_DEFAULT)){
            // $results = $OUT_MESSAGE;
            // }
            // oci_free_statement($stid);
            //  oci_close($this->db->conn_id);
            // return $results;
            //exit;
            $barcode_print = $this->db->query("Call pro_processReturn('$return_id',
                                                  '$return_status' ,
                                                  '$return_by'   ,
                                                  '$remarks',
                                                  '$bin_id',
                                                  '$bar'

                                                   )");
        }

        // if ($qyer) {
        //     $qyer = $this->db->query("UPDATE  LJ_ITEM_RETURNS set RETURN_PROCESSED = '1' where returnid ='$return_id'");
        // }
        $qyer = $this->db->query("SELECT  RETURN_PROCESSED from LJ_ITEM_RETURNS s where s.returnid ='$return_id'")->result_array();

        if ($return_status == 'release') {
            return array("status" => true, "data" => $qyer, 'barcode' => $barcode);
        } else {
            return array("status" => false);
        }
    }
    public function printReactItemReturn()
    {
        $return_id = $_GET['id'];
        // var_dump($return_id);exit;
        return $this->db->query("SELECT B.BARCODE_NO,
        SUBSTR(s.item_title, 1, 80) ITEM_DESC,
        decode(s.f_upc, null, null, 'UPC: ' || s.f_upc) upc,
        'Printed:' || sysdate dated,
        (select b.barcode_no
           from lz_barcode_mt b
          where b.returned_id =
                (select mm.returned_id
                   from LJ_ITEM_RETURNED_MT mm
                  where mm.return_id = '$return_id') and rownum = 1) previous_barcode,
        'R' r_bar
   FROM LZ_BARCODE_MT B, lz_item_seed s
  WHERE B.ITEM_ID = s.ITEM_ID
    and b.lz_manifest_id = s.lz_manifest_id
    and b.condition_id = s.default_cond
    AND B.RETURNED_ID_FOR_IN =
        (select mm.returned_id
           from LJ_ITEM_RETURNED_MT mm
          where mm.return_id = '$return_id')")->result_array();
    }
    public function printBarcode()
    {
        sleep(1);
        $return_id = $this->uri->segment(4);
        // var_dump($return_id);exit;
        return $this->db->query("SELECT B.BARCODE_NO,
        SUBSTR(s.item_title, 1, 80) ITEM_DESC,
        decode(s.f_upc, null, null, 'UPC: ' || s.f_upc) upc,
        'Printed:' || sysdate dated,
        (select b.barcode_no
           from lz_barcode_mt b
          where b.returned_id =
                (select mm.returned_id
                   from LJ_ITEM_RETURNED_MT mm
                  where mm.return_id = '$return_id') and rownum = 1) previous_barcode,
        'R' r_bar
   FROM LZ_BARCODE_MT B, lz_item_seed s
  WHERE B.ITEM_ID = s.ITEM_ID
    and b.lz_manifest_id = s.lz_manifest_id
    and b.condition_id = s.default_cond
    AND B.RETURNED_ID_FOR_IN =
        (select mm.returned_id
           from LJ_ITEM_RETURNED_MT mm
          where mm.return_id = '$return_id')

 ")->result_array();
    }
    public function Get_location()
    {
        $bin = $this->input->post("location_value");
        if (!empty($bin)) {
            $bin1 = $this->db->query("SELECT BB.BIN_ID FROM BIN_MT BB WHERE BB.BIN_TYPE||'-'||BB.BIN_NO = '$bin'");
            if ($bin1->num_rows() > 0) {
                $bin1 = $bin1->result_array();
                $bin_id = $bin1[0]['BIN_ID'];
                // return $bin_id;
                return array("status" => true, "data" => $bin_id);
            } else {
                return array("status" => false, "message" => "Invalid Location");
            }
        }
    }

    public function SellerDrop()
    {
        $getmarchantdrop = $this->db->query("SELECT  LZ_SELLER_ACCT_ID , SELL_ACCT_DESC from lz_seller_accts")->result_array();
        return $getmarchantdrop;
    }
    public function reasonDrop()
    {
        $reasond = $this->db->query("SELECT s.reason id ,replace(s.reason ,'_',' ') value from lj_item_returns s group by s.reason")->result_array();
        //var_dump($reasond); exit;
        return $reasond;
    }

    public function DownlaodReturns()
    {

        $merchant_id = $this->input->post("merchant_id");
        $return_value = $this->input->post("return_value");

        $result['merchant_id'] = $merchant_id;
        $result['return_value'] = $return_value;
        $result['returnCall'] = 1;
        $this->load->view('ebay/postOrder/01-searchReturn', $result);
        return $result;
    }

    public function FiterDeta_radio()
    {
        $radiovalue = $this->input->post("id");
        if ($radiovalue == 'all') {
            $qry = $this->db->query("SELECT ITEM_RET_ID,
            RETURNID,
            BUYERLOGINNAME,
            SELLERLOGINNAME,
            RETURN_TYPE,
            RETURN_STATE,
            r.STATUS,
            ITEMID,
            TRANSACTIONID,
            RETURNQUANTITY,
            TYPE,
            REASON,
            COMMENTS,
            TO_CHAR(CREATIONDATE, 'DD-MM-YYYY HH24:MI:SS') CREATIONDATE,
            '$' || SELLERTOTALREFUND SELLERTOTALREFUND,
            SELLERCURRENCY,
            BUYERTOTALREFUND,
            BUYERCURRENCY,
            SELLERRESPONSEDUE,
            RESPONDBYDATE,
            BUYERESCALATIONINFO,
            SELLERESCALATIONINFO,
            ACTIONURL,
            BUYERRESPONSEDUE,
            BUYERRESPONDBYDATE,
            INSERTED_DATE,
            ee.ebay_item_desc,
            RETURN_PROCESSED,
            TO_CHAR((SELECT REPLACE(REPLACE(XMLAGG(XMLELEMENT(A, RB.BARCODE_NO) ORDER BY RB.BARCODE_NO DESC NULLS LAST)
                                    .GETCLOBVAL(),
                                    '<A>',
                                    ''),
                            '</A>',
                            ', ') AS BARCODE_NO FROM LJ_RETURNED_BARCODE_MT RB , LJ_ITEM_RETURNED_MT rm
                        WHERE Rm.Returned_Id = RB.RETURNED_ID
                        and rm.return_id = r.returnid)) BARCODES

       from LJ_ITEM_RETURNS r,
            (select *
               from ebay_list_mt e
              where e.list_id in
                    (select max(list_id) from ebay_list_mt group by ebay_item_id)) ee
      where ee.ebay_item_id = itemid order by ITEM_RET_ID desc")->result_array();
        } else {
            $qry = $this->db->query("SELECT ITEM_RET_ID,
            RETURNID,
            BUYERLOGINNAME,
            SELLERLOGINNAME,
            RETURN_TYPE,
            RETURN_STATE,
            r.STATUS,
            ITEMID,
            TRANSACTIONID,
            RETURNQUANTITY,
            TYPE,
            REASON,
            COMMENTS,
            TO_CHAR(CREATIONDATE, 'DD-MM-YYYY HH24:MI:SS') CREATIONDATE,
            '$' || SELLERTOTALREFUND SELLERTOTALREFUND,
            SELLERCURRENCY,
            BUYERTOTALREFUND,
            BUYERCURRENCY,
            SELLERRESPONSEDUE,
            RESPONDBYDATE,
            BUYERESCALATIONINFO,
            SELLERESCALATIONINFO,
            ACTIONURL,
            BUYERRESPONSEDUE,
            BUYERRESPONDBYDATE,
            INSERTED_DATE,
            ee.ebay_item_desc,
            RETURN_PROCESSED,
            TO_CHAR((SELECT REPLACE(REPLACE(XMLAGG(XMLELEMENT(A, RB.BARCODE_NO) ORDER BY RB.BARCODE_NO DESC NULLS LAST)
                                    .GETCLOBVAL(),
                                    '<A>',
                                    ''),
                            '</A>',
                            ', ') AS BARCODE_NO FROM LJ_RETURNED_BARCODE_MT RB , LJ_ITEM_RETURNED_MT rm
                        WHERE Rm.Returned_Id = RB.RETURNED_ID
                        and rm.return_id = r.returnid)) BARCODES

       from LJ_ITEM_RETURNS r,
            (select *
               from ebay_list_mt e
              where e.list_id in
                    (select max(list_id) from ebay_list_mt group by ebay_item_id)) ee
      where ee.ebay_item_id = itemid and RETURN_PROCESSED='$radiovalue' order by ITEM_RET_ID desc")->result_array();
        }
        foreach ($qry as $key => $value) {
            // $item_title = trim(str_replace(" ", ' ', $item_title));
            $qry[$key]['RETURN_TYPE'] = str_replace('_', ' ', $value['RETURN_TYPE']);
            $qry[$key]['RETURN_STATE'] = str_replace('_', ' ', $value['RETURN_STATE']);
            $qry[$key]['STATUS'] = str_replace('_', ' ', $value['STATUS']);
            $qry[$key]['TYPE'] = str_replace('_', ' ', $value['TYPE']);
            $qry[$key]['REASON'] = str_replace('_', ' ', $value['REASON']);
            $qry[$key]['SELLERRESPONSEDUE'] = str_replace('_', ' ', $value['SELLERRESPONSEDUE']);
            $qry[$key]['BUYERRESPONSEDUE'] = str_replace('_', ' ', $value['BUYERRESPONSEDUE']);
        }

        return $qry;
    }

    public function undo_data()
    {

        $return_id = $this->input->post("id");

        $qyer = $this->db->query("Call pro_processreturnundo('$return_id')");
        // if ($qyer) {
        //     $qyer = $this->db->query("UPDATE  LJ_ITEM_RETURNS set RETURN_PROCESSED ='0' where returnid ='$return_id'");
        // }
        $qyer = $this->db->query("SELECT  RETURN_PROCESSED from LJ_ITEM_RETURNS s where s.returnid ='$return_id'")->result_array();
        if ($qyer) {
            return array("status" => true, "data" => $qyer);
        } else {
            return array("status" => false, "data" => 'Not Exist');
        }
        //return  $qyer;
    }
    public function Filter_Data_Item_Return()
    {

        $radio_value = $this->input->post('radio_value');
        $check_box = $this->input->post('filterdata_radio');
        //    print_r($check_box);
        //    exit();
        $search_data = $this->input->post('search_data');
        $show_filter = $this->input->post('show_filter');
        $show_90 = $this->input->post('older_90');
        // if ($check_box) {
        // if ($check_box == '0') {
        //     $retunData = $this->db->query("SELECT g.return_id from lz_salesload_det g  where g.sales_record_number = '$search_data'")->result_array();

        if ($show_90 == 'true') {
            $qry = "select d.*, '$' || total_price total_price, m.lz_seller_acct_id
        from lz_salesload_det d , lz_salesload_mt m
       where d.lz_saleload_id = m.lz_saleload_id
       and d.inserted_date >= sysdate - 90 ";
        } else {
            $qry = "select d.*, '$' || total_price total_price, m.lz_seller_acct_id
        from lz_salesload_det d , lz_salesload_mt m
       where d.lz_saleload_id = m.lz_saleload_id ";
        }
        $cam = '';
        if ($radio_value == 'Like') {
            $cam = 'LIKE';
            $perc = '%';
        } else {
            $cam = '=';
            $perc = '';
        }

        if ($show_filter == 'true') {
            // var_dump(empty($check_box));exit;
            if ($check_box !== null) {

                if ($check_box == '0') {

                    $qry .= "and d.sales_record_number $cam '$perc$search_data$perc'";
                }
                if ($check_box == '1') {

                    $qry .= "and d.extendedorderid $cam '$perc$search_data$perc'";
                }
                if ($check_box == '2') {

                    $qry .= "and d.item_id $cam '$perc$search_data$perc'";
                }

                if ($check_box == '3') {

                    $qry .= "and d.tracking_number  $cam '$perc$search_data$perc'";
                }
            }
        } else {

            $filter_value = $this->input->post('filter_value');

            if ($filter_value['value'] !== null) {
                //var_dump($filter_value['value']); exit;
                $field = $filter_value['value'];
                $data = trim($search_data);
                $str = explode(' ', $data);
                $w = count($str);
                //var_dump($filter_value['value']);exit;
                $i = 1;
                if ($w > 1) {
                    foreach ($str as $val) {
                        $key = strtoupper(trim($val));
                        if ($i === 1) {
                            $startbreket = '(';
                        } else {
                            $startbreket = '';
                        }
                        if ($i === $w) {
                            $endbraket = ')';
                        } else {
                            $endbraket = '';
                        }

                        $qry .= "and  $startbreket upper(d.$field) $cam '$perc$key$perc' $endbraket";

                        if ($i === 1) {
                            $qry .= "and  $startbreket upper(d.$field) $cam '$perc$key$perc' $endbraket";
                        } else {
                            $qry .= " and  $startbreket upper(d.$field) $cam '$perc$key$perc' $endbraket";
                        }
                        // }

                        $i++;
                    }
                } else {
                    if ($cam == 'LIKE') {
                        foreach ($str as $val) {
                            $key = strtoupper(trim($val));
                            if ($field == 'buyer_address1') {

                                $qry .= "and  $startbreket upper(d.buyer_address1) $cam '$perc$key$perc' $endbraket";
                                $qry .= "and  $startbreket upper(d.buyer_address2) $cam '$perc$key$perc' $endbraket";
                                $qry .= "and  $startbreket upper(d.ship_to_address1) $cam '$perc$key$perc' $endbraket";
                                $qry .= "and  $startbreket upper(d.ship_to_address2) $cam '$perc$key$perc' $endbraket";
                            } else {

                                $qry .= "and upper(d.$field) $cam '$perc$key$perc'";
                            }

                            $i++;
                        }
                    } else {
                        if ($field == 'buyer_address1') {

                            $qry .= "and  $startbreket upper(d.buyer_address1) $cam '$perc$str$perc' $endbraket";
                            $qry .= "and  $startbreket upper(d.buyer_address2) $cam '$perc$str$perc' $endbraket";
                        } else {

                            $qry .= "and upper(d.$field) $cam '$perc$str$perc'";
                        }
                    }
                }
            }
        } // end main if else

        $data = $this->db->query($qry)->result_array();
        if ($show_filter == 'true') {
            $group_id = "";
            if ($radio_value !== 'Like') {
                if ($show_90 == 'true') {
                    $qry = "select d.*, '$' || total_price total_price, m.lz_seller_acct_id
                from lz_salesload_det d , lz_salesload_mt m
               where d.lz_saleload_id = m.lz_saleload_id
               and d.inserted_date >= sysdate - 90 ";
                } else {
                    $qry = "select d.*, '$' || total_price total_price, m.lz_seller_acct_id
                from lz_salesload_det d , lz_salesload_mt m
               where d.lz_saleload_id = m.lz_saleload_id ";
                }
                foreach ($data as $key => $value) {
                    $group_id = $value['SALES_RECORD_NO_GROUP'];
                    if (!empty($value['SALES_RECORD_NO_GROUP'])) {

                        $qry .= "and  ( upper(d.SALES_RECORD_NO_GROUP) = '$group_id' )";
                    }
                }
            }
            if (!empty($group_id)) {
                $data = $this->db->query($qry)->result_array();
            }
        }
        // echo "<pre>";
        // print_r($data);
        // exit;
        $mesages = '';
        foreach ($data as $key => $value) {
            if (!empty($value['RETURN_ID'])) {
                $data[$key]['RETURN_ID_STATUS'] = "TRUE";
                $mesages = 'Return Already Processed';
            } else {
                $data[$key]['RETURN_ID_STATUS'] = "FALSE";
                // $mesages = 'No Cancel Or Return Id';
            }
            if (!empty($value['CANCEL_ID'])) {
                $data[$key]['CANCEL_ID_STATUS'] = "TRUE";
                $mesages = 'Item Already Canceled';
            } else {
                $data[$key]['CANCEL_ID_STATUS'] = "FALSE";
                //$mesages = 'No Cancel Or Return Id';
            }
            if (empty($value['CANCEL_ID']) && empty($value['RETURN_ID'])) {
                $mesages = 'No Cancel And No Return iid';
            }
        }

        //else {

        //     $mesages = 'No Return And No Canceled Id';
        // }

        if ($data) {
            return array("data" => $data, 'status' => true, 'mesages' => $mesages);
        } else {
            return array("data" => array(), 'status' => false, 'mesages' => '');
        }
    }
    /********************************
     * end Screen post Item returns
     *********************************/
    /********************************
     * START  Screen invoices by tayyab
     *********************************/

    public function insert_payment_detail()
    {

        $merchantid = $this->input->post('merchantid');
        $RecieptNumber = $this->input->post('RecieptNumber');
        $paymentDate = $this->input->post('paymentDate');

        $fromdate = new DateTime($paymentDate);
        $paymentDate2 = date_format($fromdate, 'y-m-d');

        // $paymentDate1 = date_create($paymentDate);
        // $paymentDate1 = date_format(@$paymentDate1, 'd/m/y H:i');
        $amountPaid = $this->input->post('amountPaid');
        $user_id = $this->input->post('user_id');
        $payment_type = $this->input->post('payment_type');
        $checkNumber = $this->input->post('checkNumber');
        $checkName = $this->input->post('checkName');
        $checkissuedate = $this->input->post('checkissuedate');

        $fromdate = new DateTime($checkissuedate);
        $checkissuedate2 = date_format($fromdate, 'y-m-d');

        $incoive_id = $this->input->post('incoive_id');

        $stat_query = $this->db->query("INSERT INTO  LJ_PAYMENT_RECEIPT(LJ_PAYMENT_ID,  MERCHANT_ID, RECIEPT_NO, PAYMENT_DATE, AMOUNT_PAID, RECIEVED_BY, PAYMENT_TYPE, CHECK_NUMBER,CHECK_NAME, CHECK_ISSUE_DATE, INVOICE_ID) values (get_single_primary_key('LJ_PAYMENT_RECEIPT','LJ_PAYMENT_ID'),'$merchantid','$RecieptNumber', TO_DATE('$paymentDate2','yyyy-mm-dd HH24:MI:SS'),'$amountPaid','$user_id','$payment_type','$checkNumber','$checkName',TO_DATE('$checkissuedate2','yyyy-mm-dd HH24:MI:SS'),'$incoive_id')");
        //var_dump( $stat_query );exit;
        return $stat_query;
    }

    public function get_Receipt_no()
    {
        $merchantid = $this->input->post('id');
        // var_dump( $merchantid); exit;
        $stat_query = $this->db->query(" SELECT Lpad (NVL(MAX(l.reciept_no + 1), 1) , 5, '0') Receipt  FROM lj_payment_receipt l
        where l.merchant_id =$merchantid");
        //var_dump( $stat_query );exit;
        return $stat_query->result_array();
    }

    /********************************
     * end Screen invoices by tayyab
     *********************************/
    /********************************
     *   start Screen barcodeImage
     *********************************/

    public function get_barcode_detail()
    {
        $barccode_no = $this->input->post('id');
        if (!empty($barccode_no)) {
            $tBarcode = $this->db->query("SELECT C.USER_NAME,
            m.buisness_name,
            md.barcode_no,
            L.LOT_DESC,
            L.LOT_ID,
            mb.issued_date
       from employee_mt            C,
            lz_merchant_mt         m,
            lz_merchant_barcode_mt mb,
            lz_merchant_barcode_dt md,
            lot_defination_mt      L
      WHERE md.mt_id = mb.mt_id
        AND m.merchant_id = mb.merchant_id
        AND mb.lot_id = L.LOT_ID
        AND mb.issued_by = c.employee_id
        AND md.barcode_no = '$barccode_no'");

            if ($tBarcode->num_rows() > 0) {
                $tBarcode = $tBarcode->result_array();
                return array("status" => true, "data" => $tBarcode);
            } else {
                return array("status" => false, "message" => "Barcode Detail Not Exist");
            }
        }
    }
    public function get_all_barcode()
    {
        $qry = $this->db->query("SELECT C.USER_NAME,
        m.buisness_name,
        md.barcode_no,
        L.LOT_DESC,
        L.LOT_ID,
        mb.issued_date
   from employee_mt            C,
        lz_merchant_mt         m,
        lz_merchant_barcode_mt mb,
        lz_merchant_barcode_dt md,
        lot_defination_mt      L
  WHERE md.mt_id = mb.mt_id
    AND m.merchant_id = mb.merchant_id
    AND mb.lot_id = L.LOT_ID
    AND mb.issued_by = c.employee_id
    AND md.barcode_no = md.barcode_no")->result_array();
        return $qry;
    }
    // /********************************
    //  *  end Screen barcodeImage
    //  *********************************/

    // /********************************
    //  *  start Screen Lister View
    //  *********************************/
    public function lister_view()
    {
        // $date = $this->input->post('date');
        $Listerdrowp = $this->input->post('Listerdrowp');
        $searchItem = $this->input->post('searchItem');
        $merchant_id = $this->input->post('merchant_id');
        if (!empty($merchant_id) && isset($merchant_id['value'])) {
            $merchant_id = $merchant_id['value'];
        }
        $account_id = $this->input->post('account_id');
        if (!empty($account_id) && isset($account_id['value'])) {
            $account_id = $account_id['value'];
        }
        $searchItem = strtoupper(trim(str_replace("  ", ' ', $searchItem)));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);
        // $startDate = date_create($date);
        // $startDate = date_format(@$startDate, 'Y/m/d');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $qry = "SELECT
T.ITEM_ID,
T.LIST_ID,
T.LZ_MANIFEST_ID,
TO_CHAR(T.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
T.LISTER_ID,
E.USER_NAME,
lu.EBAY_ITEM_DESC,
T.LIST_QTY,
T.EBAY_ITEM_ID,
lu.LIST_PRICE,
T.LZ_SELLER_ACCT_ID,
L.EBAY_URL,
st.list_qty,
st.qty_sold,
st.status,
st.barcode_no,
DECODE(ls.qc_check, null , 0,ls.qc_check ) qc_check,
       ls.qc_remarks,
       ls.qc_by,
EXTRACT(Day FROM(sysdate - T.LIST_DATE) DAY TO SECOND) as Day,
dd.account_name sell_acct_desc,
lu.U_NAME LAST_LISTER,
lu.list_date LAST_DATE
FROM EBAY_LIST_MT T,
lj_merhcant_acc_dt dd,
EMPLOYEE_MT E,
LZ_LISTED_ITEM_URL L,
lz_item_seed  ls,
(select max(br.barcode_no) barcode_no,
max(sd.ebay_item_id) ebay_item_id,
max(sd.list_qty) list_qty,
max(sd.qty_sold) qty_sold,
max(sd.status) status
from lz_barcode_mt br,
(select ebay_item_id,
list_qty,
qty_sold,
case
when li.list_qty > s.qty_sold then
'Partially Sold'
when li.list_qty = s.qty_sold then
'Completely Sold'
when li.list_qty < s.qty_sold then
'Mismatch Sold'
else
'None'
end status
from (select d.item_id, sum(d.quantity) qty_sold
from lz_salesload_det d
where d.orderstatus = 'Completed'
group by d.item_id) s,
(select t.ebay_item_id, sum(t.list_qty) list_qty
from ebay_list_mt t
where t.ebay_item_id is not null
group by t.ebay_item_id) li
where li.ebay_item_id = s.item_id(+)) sd
where br.ebay_item_id = sd.ebay_item_id
group by br.ebay_item_id) st,
(select ee.lister_id,
TO_CHAR(ee.list_date, 'DD-MM-YYYY HH24:MI:SS') list_date,
ee.ebay_item_id eBay_ID,
e.user_name U_NAME,
ee.ebay_item_desc,
ee.list_price
from ebay_list_mt ee,
(select l.ebay_item_id, max(l.list_id) list_id
from ebay_list_mt l
group by ebay_item_id) mx,
employee_mt e
where ee.list_id = mx.list_id
and e.employee_id = ee.lister_id) lu

WHERE T.LISTER_ID = E.EMPLOYEE_ID
AND T.EBAY_ITEM_ID = L.EBAY_ID
and T.DISCARDED_DATE IS NULL
and dd.acct_id = t.lz_seller_acct_id(+)
and t.ebay_item_id = st.ebay_item_id(+)
AND T.SEED_ID = ls.seed_id(+)";
        if (!empty($account_id) && $merchant_id !== 'ALL') {
            $qry .= " AND T.LZ_SELLER_ACCT_ID = '$account_id'";
        } else {
            $qry .= " AND T.LZ_SELLER_ACCT_ID IS NOT NULL";
        }
        if (!empty($merchant_id) && $merchant_id !== 'ALL') {
            $qry .= "  AND dd.merchant_id = '$merchant_id' ";
        }

/*AND T.LZ_SELLER_ACCT_ID IS NOT NULL*/
        $qry .= " AND T.EBAY_ITEM_ID = lu.eBay_ID(+)
        AND T.LIST_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$endDate 23:59:59',  'YYYY/MM/DD HH24:MI:SS') ";
        if (empty($searchItem)) {
            $qry .= " AND T.LISTER_ID IN
    (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = '$Listerdrowp') ";
        }

        if (!empty($searchItem)) {
            $str = explode(' ', $searchItem);
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $qry .= " and (UPPER(lu.EBAY_ITEM_DESC) LIKE '%$key%' ";
                    } else {
                        $qry .= " AND UPPER(lu.EBAY_ITEM_DESC) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $qry .= " and (UPPER(lu.EBAY_ITEM_DESC) LIKE '%$searchItem%' ";
            }

            $qry .= " OR UPPER(T.EBAY_ITEM_ID) LIKE '%$searchItem%'
    OR UPPER(st.barcode_no) LIKE '%$searchItem%'
    OR UPPER(dd.account_name) LIKE '%$searchItem%' )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }

        $qry .= " order by t.list_id desc ";

        $data = $this->db->query($qry)->result_array();
        // return $qry;
        if (count($data) >= 1) {

            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_pictures_by_barcode_listeview($data, $conditions);
            $images = $uri['uri'];
            return array("images" => $images, 'data' => $data, 'status' => true);
        } else {
            return array('images' => [], 'data' => [], 'exist' => false, 'status' => false);
        }
    }

    public function sum_total_listing()
    {
        //$sum_listing = $this->db->query("select count(t.item_id) as TOTAL_LISTING,sum(t.list_price) as TOTAL_PRICE from ebay_list_mt t where  t.lz_seller_acct_id is not null");
        // $date = $this->input->post('data');
        // $startDate = date_create($date);
        // $startDate = date_format(@$startDate, 'Y/m/d');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $sum_listing = $this->db->query("SELECT COUNT(1) AS TOTAL_LISTING, SUM(LIST_AMT) AS TOTAL_PRICE
        FROM (SELECT E.EBAY_ITEM_ID, SUM(E.LIST_QTY * E.LIST_PRICE) LIST_AMT
                FROM EBAY_LIST_MT E
               WHERE E.LZ_SELLER_ACCT_ID IS NOT NULL
                    and E.LIST_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$endDate 23:59:59',  'YYYY/MM/DD HH24:MI:SS')
               GROUP BY E.EBAY_ITEM_ID) IN_QRY")->result_array();

        //var_dump($sum_listing->result_array());exit;

        return $sum_listing;
    }

    public function listerUsers()
    {
        $query = $this->db->query("SELECT T.EMPLOYEE_ID, T.USER_NAME FROM EMPLOYEE_MT T WHERE T.STATUS =1 ");

        //var_dump($query->result_array());exit;
        return $query->result_array();
    }

    public function filter_data($dataArray)
    {
        $radiobtn = $this->input->post('radiobtn');
        $searchItem = $this->input->post('searchItem');
        $searchItem = strtoupper(trim(str_replace("  ", ' ', $searchItem)));
        $searchItem = str_replace(array("`,'"), "", $searchItem);
        $searchItem = str_replace(array("'"), "''", $searchItem);
        if (is_numeric($radiobtn)) {
            $radioqry = "and t.lz_seller_acct_id ='$radiobtn'";
        } else {
            $radioqry = "";
        };
        $ListeruserId = $this->input->post('Listeruservalue');
        //var_dump($dataArray); exit;
        $employeQry = "";
        if ($ListeruserId == "" || $ListeruserId == "PK" && empty($searchItem)) {
            $employeQry = " AND T.LISTER_ID IN (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = 'PK')";
        } elseif ($ListeruserId !== "All" && $ListeruserId !== "PK" && $ListeruserId !== "US" && empty($searchItem)) {
            $employeQry = " AND T.LISTER_ID = $ListeruserId";
        } elseif ($ListeruserId == "US" && empty($searchItem)) {

            $employeQry = " AND T.LISTER_ID IN (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = 'US')";
        }
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $merchant_id = $this->input->post('merchant_id');
        if (!empty($merchant_id) && isset($merchant_id['value'])) {
            $merchant_id = $merchant_id['value'];
        }
        $account_id = $this->input->post('account_id');
        if (!empty($account_id) && isset($account_id['value'])) {
            $account_id = $account_id['value'];
        }
        $qry = "SELECT
        T.ITEM_ID,
        T.LIST_ID,
        T.LZ_MANIFEST_ID,
        TO_CHAR(T.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
        T.LISTER_ID,
        E.USER_NAME,
        lu.EBAY_ITEM_DESC,
        T.LIST_QTY,
        T.EBAY_ITEM_ID,
        lu.LIST_PRICE,
        T.LZ_SELLER_ACCT_ID,
        L.EBAY_URL,
        st.list_qty,
        st.qty_sold,
        st.status,
        st.barcode_no,
        DECODE(ls.qc_check, null , 0,ls.qc_check ) qc_check,
       ls.qc_remarks,
       ls.qc_by,
       (SELECT USER_NAME FROM EMPLOYEE_MT WHERE EMPLOYEE_ID = ls.qc_by) QC_REMARKS_BY,
        EXTRACT(Day FROM(sysdate - T.LIST_DATE) DAY TO SECOND) as Day,
        dd.account_name sell_acct_desc,
        lu.U_NAME LAST_LISTER,
        lu.list_date LAST_DATE,
        st.condition_id,
       st.category_id,
       st.mpn,
       st.upc
        FROM EBAY_LIST_MT T,
        lj_merhcant_acc_dt dd,
        EMPLOYEE_MT E,
        LZ_LISTED_ITEM_URL L,
        lz_item_seed  ls,
(select max(br.barcode_no) barcode_no,

               max(br.condition_id) condition_id,
               max(se.category_id) category_id,
               max(se.f_mpn) mpn,
               max(se.f_upc) upc,
               max(sd.ebay_item_id) ebay_item_id,

               max(sd.list_qty) list_qty,
               max(sd.qty_sold) qty_sold,
               max(sd.status) status
          from lz_barcode_mt br,
               lz_item_seed se,
               (select ebay_item_id,
                       list_qty,
                       qty_sold,
                       case
                         when li.list_qty > s.qty_sold then
                          'Partially Sold'
                         when li.list_qty = s.qty_sold then
                          'Completely Sold'
                         when li.list_qty < s.qty_sold then
                          'Mismatch Sold'
                         else
                          'None'
                       end status
                  from (select d.item_id, sum(d.quantity) qty_sold
                          from lz_salesload_det d
                         where d.orderstatus = 'Completed'
                         group by d.item_id) s,
                       (select t.ebay_item_id, sum(t.list_qty) list_qty
                          from ebay_list_mt t
                         where t.ebay_item_id is not null
                         group by t.ebay_item_id) li
                 where li.ebay_item_id = s.item_id(+)) sd
         where br.ebay_item_id = sd.ebay_item_id
           and br.item_id = se.item_id(+)
           and br.lz_manifest_id = se.lz_manifest_id(+)
           and br.condition_id = se.default_cond(+)
         group by br.ebay_item_id) st,
            (select ee.lister_id,
        TO_CHAR(ee.list_date, 'DD-MM-YYYY HH24:MI:SS') list_date,
        ee.ebay_item_id eBay_ID,
        e.user_name U_NAME,
        ee.ebay_item_desc,
        ee.list_price
        from ebay_list_mt ee,
        (select l.ebay_item_id, max(l.list_id) list_id
        from ebay_list_mt l
        group by ebay_item_id) mx,
        employee_mt e
        where ee.list_id = mx.list_id
        and e.employee_id = ee.lister_id) lu

        WHERE T.LISTER_ID = E.EMPLOYEE_ID
        AND T.EBAY_ITEM_ID = L.EBAY_ID
        and T.DISCARDED_DATE IS NULL
        and dd.acct_id = t.lz_seller_acct_id(+)
        and t.ebay_item_id = st.ebay_item_id(+)
        AND T.SEED_ID = ls.seed_id(+)
        ";
        if (!empty($account_id) && $merchant_id !== 'ALL') {
            $qry .= " AND T.LZ_SELLER_ACCT_ID = '$account_id'";
        } else {
            $qry .= " AND T.LZ_SELLER_ACCT_ID IS NOT NULL";
        }
        if (!empty($merchant_id) && $merchant_id !== 'ALL') {
            $qry .= "  AND dd.merchant_id = '$merchant_id' ";
        }

        /*  AND T.LZ_SELLER_ACCT_ID IS NOT NULL*/
        $qry .= "  AND T.EBAY_ITEM_ID = lu.eBay_ID(+)
    $radioqry
    $employeQry
    AND T.LIST_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$endDate 23:59:59', 'YYYY/MM/DD HH24:MI:SS') ";
        if (!empty($searchItem)) {
            $str = explode(' ', $searchItem);
            if (count($str) > 1) {
                $i = 1;
                foreach ($str as $key) {
                    if ($i === 1) {
                        $qry .= " and (UPPER(lu.EBAY_ITEM_DESC) LIKE '%$key%' ";
                    } else {
                        $qry .= " AND UPPER(lu.EBAY_ITEM_DESC) LIKE '%$key%' ";
                    }
                    $i++;
                }
            } else {
                $qry .= " and (UPPER(lu.EBAY_ITEM_DESC) LIKE '%$searchItem%' ";
            }

            $qry .= " OR UPPER(T.EBAY_ITEM_ID) LIKE '%$searchItem%'
    OR UPPER(st.barcode_no) LIKE '%$searchItem%'
    OR UPPER(dd.account_name) LIKE '%$searchItem%' )";
            //OR UPPER(OM.SALE_PRICE) LIKE '%$getSeach%') ";
        }
        $qry .= " order by t.list_id desc";

        $qyer = $this->db->query($qry)->result_array();

        if (count($qyer) >= 1) {

            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_pictures_by_barcode_listeview($qyer, $conditions);
            $images = $uri['uri'];
            return array("images" => $images, 'data' => $qyer, 'status' => true);
        } else {
            return array('images' => [], 'data' => [], 'exist' => false, 'status' => false);
        }
    }

    public function price_fiter($dataArray)
    {
        $radiobtn = $this->input->post('radiobtn');
        if (is_numeric($radiobtn)) {
            $radioqry = "and t.lz_seller_acct_id ='$radiobtn'";
        } else {
            $radioqry = "";
        }

        $ListeruserId = $this->input->post('ListeruserId');
        $employeQry = "";
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');

        $main_query = "SELECT COUNT(1) AS TOTAL_LISTING, SUM(LIST_AMT) AS  TOTAL_PRICE FROM ( SELECT T.EBAY_ITEM_ID, SUM(T.LIST_QTY * T.LIST_PRICE) LIST_AMT FROM EBAY_LIST_MT T WHERE T.LZ_SELLER_ACCT_ID IS NOT NULL";
        if ($ListeruserId == "" || $ListeruserId == "PK") {
            $employeQry = " AND T.LISTER_ID IN (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = 'PK')";
        } elseif ($ListeruserId !== "All" && $ListeruserId !== "PK" && $ListeruserId !== "US") {
            $employeQry = " AND T.LISTER_ID = $ListeruserId";
        } elseif ($ListeruserId == "US") {

            $employeQry = " AND T.LISTER_ID IN (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = 'US')";
        }

        $query = $this->db->query($main_query . " " . $employeQry . $radioqry . " AND T.LIST_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$endDate 23:59:59', 'YYYY/MM/DD HH24:MI:SS') GROUP BY T.EBAY_ITEM_ID ORDER BY T.EBAY_ITEM_ID DESC)");return $query->result_array();
    }

    public function get_status_sold_unsold($dataArray)
    {
        $ListeruserId = $this->input->post('Listeruservalue');
        $employeQry = "";
        if ($ListeruserId == "" || $ListeruserId == "PK") {
            $employeQry = " AND T.LISTER_ID IN (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = 'PK')";
        } elseif ($ListeruserId !== "All" && $ListeruserId !== "PK" && $ListeruserId !== "US") {
            $employeQry = " AND T.LISTER_ID = $ListeruserId";
        } elseif ($ListeruserId == "US") {

            $employeQry = " AND T.LISTER_ID IN (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = 'US')";
        }
        $radiobtn = $this->input->post('radiobtn');
        if (is_numeric($radiobtn)) {
            $radioqry = "and t.lz_seller_acct_id ='$radiobtn'";
        } else {
            $radioqry = "";
        }
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');

        $qry = $this->db->query("SELECT status,count(1) acount from (SELECT T.ITEM_ID,
         T.LIST_ID,
        T.LZ_MANIFEST_ID,
        TO_CHAR(T.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
        T.LISTER_ID,
        E.USER_NAME,
        lu.EBAY_ITEM_DESC,
        T.LIST_QTY,
        T.EBAY_ITEM_ID,
        lu.LIST_PRICE,
        T.LZ_SELLER_ACCT_ID,
        L.EBAY_URL,
        st.list_qty,
        st.qty_sold,
        st.status,
        EXTRACT(Day FROM(sysdate -
         T.LIST_DATE) DAY TO
          SECOND) as Day,
         dd.account_name sell_acct_desc,
         lu.U_NAME LAST_LISTER,
lu.list_date LAST_DATE

   FROM EBAY_LIST_MT T,
     lj_merhcant_acc_dt dd,
        EMPLOYEE_MT E,
        LZ_LISTED_ITEM_URL L,
        (select ebay_item_id,

                list_qty,
                qty_sold,
                case
                  when li.list_qty > s.qty_sold then
                   'Partially Sold'
                  when li.list_qty = s.qty_sold then
                   'Completely Sold'
                  when li.list_qty < s.qty_sold then
                   'Mismatch Sold'
                  else
                   'None'
                end status

           from (select d.item_id, sum(d.quantity) qty_sold
                   from lz_salesload_det d
                  where d.orderstatus = 'Completed'
                  group by d.item_id) s,

                (select t.ebay_item_id, sum(t.list_qty) list_qty
                   from ebay_list_mt t
                  where t.ebay_item_id is not null
                  group by t.ebay_item_id) li

          where li.ebay_item_id = s.item_id(+)) st,
          (select ee.lister_id,
          TO_CHAR(ee.list_date, 'DD-MM-YYYY HH24:MI:SS') list_date,
ee.ebay_item_id eBay_ID,
e.user_name U_NAME,
ee.ebay_item_desc,
ee.list_price
from ebay_list_mt ee,
(select l.ebay_item_id, max(l.list_id) list_id
from ebay_list_mt l
group by ebay_item_id) mx,
employee_mt e
where ee.list_id = mx.list_id
and e.employee_id = ee.lister_id) lu
  WHERE T.LISTER_ID = E.EMPLOYEE_ID
    AND T.EBAY_ITEM_ID = L.EBAY_ID
    and dd.acct_id = t.lz_seller_acct_id(+)
    and t.ebay_item_id = st.ebay_item_id(+)
    AND T.LZ_SELLER_ACCT_ID IS NOT NULL
    and T.DISCARDED_DATE IS NULL
    AND T.EBAY_ITEM_ID = lu.eBay_ID(+)
    $radioqry
    $employeQry
    AND T.LIST_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$endDate 23:59:59', 'YYYY/MM/DD HH24:MI:SS')
   order by t.list_id desc)  group by status")->result_array();
        if ($qry) {
            return array("data" => $qry, "status" => true);
        } else {
            return array("data" => [], "status" => false);
        }
    }
    public function get_status_sold_unsold_onload()
    {
        $Listeruservalue = $this->input->post('Listerdrowp');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $qry = $this->db->query("SELECT status,count(1) acount from (SELECT T.ITEM_ID,
         T.LIST_ID,
        T.LZ_MANIFEST_ID,
        TO_CHAR(T.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
        T.LISTER_ID,
        E.USER_NAME,
        lu.EBAY_ITEM_DESC,
        T.LIST_QTY,
        T.EBAY_ITEM_ID,
        lu.LIST_PRICE,
        T.LZ_SELLER_ACCT_ID,
        L.EBAY_URL,
        st.list_qty,
        st.qty_sold,
        st.status,
        EXTRACT(Day FROM(sysdate -
         T.LIST_DATE) DAY TO
          SECOND) as Day,
          dd.account_name sell_acct_desc,
          lu.U_NAME LAST_LISTER,
lu.list_date LAST_DATE

   FROM EBAY_LIST_MT T,
        EMPLOYEE_MT E,
        lj_merhcant_acc_dt dd,
        LZ_LISTED_ITEM_URL L,
        (select ebay_item_id,
                list_qty,
                qty_sold,
                case
                  when li.list_qty > s.qty_sold then
                   'Partially Sold'
                  when li.list_qty = s.qty_sold then
                   'Completely Sold'
                  when li.list_qty < s.qty_sold then
                   'Mismatch Sold'
                  else
                   'None'
                end status

           from (select d.item_id, sum(d.quantity) qty_sold
                   from lz_salesload_det d
                  where d.orderstatus = 'Completed'
                  group by d.item_id) s,

                (select t.ebay_item_id, sum(t.list_qty) list_qty
                   from ebay_list_mt t
                  where t.ebay_item_id is not null
                  group by t.ebay_item_id) li

          where li.ebay_item_id = s.item_id(+)) st,
          (select ee.lister_id,
          TO_CHAR(ee.list_date, 'DD-MM-YYYY HH24:MI:SS') list_date,
ee.ebay_item_id eBay_ID,
e.user_name U_NAME,
ee.ebay_item_desc,
ee.list_price
from ebay_list_mt ee,
(select l.ebay_item_id, max(l.list_id) list_id
from ebay_list_mt l
group by ebay_item_id) mx,
employee_mt e
where ee.list_id = mx.list_id
and e.employee_id = ee.lister_id) lu

  WHERE T.LISTER_ID = E.EMPLOYEE_ID
    AND T.EBAY_ITEM_ID = L.EBAY_ID
    and dd.acct_id = t.lz_seller_acct_id(+)
    and t.ebay_item_id = st.ebay_item_id(+)
    AND T.LZ_SELLER_ACCT_ID IS NOT NULL
    and T.DISCARDED_DATE IS NULL
    AND T.EBAY_ITEM_ID = lu.eBay_ID(+)
    AND T.LIST_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$endDate 23:59:59',  'YYYY/MM/DD HH24:MI:SS')
    AND T.LISTER_ID IN
        (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = '$Listeruservalue')
  order by t.list_id desc)
 group by status")->result_array();
        if ($qry) {
            return array("data" => $qry, "status" => true);
        } else {
            return array("data" => [], "status" => false);
        }
    }

    public function filter_as_sold_unsold($dataArray)
    {
        $ListeruserId = $this->input->post('ListeruserId');
        $radiobtn = $this->input->post('radiobtn');
        $partialy_sold = $this->input->post('partialy_sold');
        // var_dump($partialy_sold); exit;

        $employeQry = "";
        if ($ListeruserId == "" || $ListeruserId == "PK") {
            $employeQry = " AND T.LISTER_ID IN (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = 'PK')";
        } elseif ($ListeruserId !== "All" && $ListeruserId !== "PK" && $ListeruserId !== "US") {
            $employeQry = " AND T.LISTER_ID = $ListeruserId";
        } elseif ($ListeruserId == "US") {

            $employeQry = " AND T.LISTER_ID IN (SELECT EMPLOYEE_ID FROM EMPLOYEE_MT WHERE LOCATION = 'US')";
        }
        $radiobtn = $this->input->post('radiobtn');
        if (is_numeric($radiobtn)) {
            $radioqry = "and t.lz_seller_acct_id ='$radiobtn'";
        } else {
            $radioqry = "";
        }
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');

        $qry = "SELECT T.ITEM_ID,
         T.LIST_ID,
        T.LZ_MANIFEST_ID,
        TO_CHAR(T.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
        T.LISTER_ID,
        E.USER_NAME,
        lu.EBAY_ITEM_DESC,
        T.LIST_QTY,
        T.EBAY_ITEM_ID,
        lu.LIST_PRICE,
        T.LZ_SELLER_ACCT_ID,
        L.EBAY_URL,
        st.list_qty,
        st.qty_sold,
        st.status,
        st.barcode_no,
        EXTRACT(Day FROM(sysdate - T.LIST_DATE) DAY TO SECOND) as Day,
        dd.account_name sell_acct_desc,
        lu.U_NAME LAST_LISTER,
lu.list_date LAST_DATE

   FROM EBAY_LIST_MT T,
        lj_merhcant_acc_dt dd,
        EMPLOYEE_MT E,
        LZ_LISTED_ITEM_URL L,
        (select max(br.barcode_no) barcode_no , max(sd.ebay_item_id) ebay_item_id , max(sd.list_qty) list_qty, max(sd.qty_sold) qty_sold, max(sd.status) status
 from lz_barcode_mt br,(
 select ebay_item_id,
                list_qty,
                qty_sold,
                case
                  when li.list_qty > s.qty_sold then
                   'Partially Sold'
                  when li.list_qty = s.qty_sold then
                   'Completely Sold'
                  when li.list_qty < s.qty_sold then
                   'Mismatch Sold'
                  else
                   'None'
                end status
           from (select d.item_id, sum(d.quantity) qty_sold
                   from lz_salesload_det d
                  where d.orderstatus = 'Completed'
                  group by d.item_id) s,
                (select t.ebay_item_id, sum(t.list_qty) list_qty
                   from ebay_list_mt t
                  where t.ebay_item_id is not null
                  group by t.ebay_item_id) li
          where li.ebay_item_id = s.item_id(+)) sd where br.ebay_item_id = sd.ebay_item_id
          group by br.ebay_item_id
    ) st,
    (select ee.lister_id,
    TO_CHAR(ee.list_date, 'DD-MM-YYYY HH24:MI:SS') list_date,
ee.ebay_item_id eBay_ID,
e.user_name U_NAME,
ee.ebay_item_desc,
ee.list_price
from ebay_list_mt ee,
(select l.ebay_item_id, max(l.list_id) list_id
from ebay_list_mt l
group by ebay_item_id) mx,
employee_mt e
where ee.list_id = mx.list_id
and e.employee_id = ee.lister_id) lu

  WHERE T.LISTER_ID = E.EMPLOYEE_ID
    AND T.EBAY_ITEM_ID = L.EBAY_ID
    and dd.acct_id = t.lz_seller_acct_id(+)
    and t.ebay_item_id = st.ebay_item_id(+)
    AND T.LZ_SELLER_ACCT_ID IS NOT NULL
    and T.DISCARDED_DATE IS NULL
    AND T.EBAY_ITEM_ID = lu.eBay_ID(+)
    AND st.STATUS ='$partialy_sold'
    $radioqry
    $employeQry
    AND T.LIST_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$endDate 23:59:59', 'YYYY/MM/DD HH24:MI:SS')
  order by t.list_id desc";

        $qyer = $this->db->query($qry)->result_array();

        if (count($qyer) >= 1) {

            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_pictures_by_barcode_listeview($qyer, $conditions);
            $images = $uri['uri'];
            return array("images" => $images, 'data' => $qyer, 'status' => true);
        } else {
            return array('images' => [], 'data' => [], 'exist' => false, 'status' => false);
        }
    }
    // /********************************
    //  *  end Screen Lister View
    //  *********************************/

    // /********************************
    //  * start Screen Add Info
    //  *********************************/
    public function Filter_Data_Item_Return_add_info()
    {

        $radio_value = $this->input->post('radio_value');
        $check_box = $this->input->post('filterdata_radio');
        //    print_r($check_box);
        //    exit();
        $search_data = $this->input->post('search_data');
        $show_filter = $this->input->post('show_filter');
        $show_90 = $this->input->post('older_90');
        // if ($check_box) {
        // if ($check_box == '0') {
        //     $retunData = $this->db->query("SELECT g.return_id from lz_salesload_det g  where g.sales_record_number = '$search_data'")->result_array();

        if ($show_90 == 'true') {
            $qry = "select d.*, '$' || total_price total_price, m.lz_seller_acct_id
        from lz_salesload_det d , lz_salesload_mt m
       where d.lz_saleload_id = m.lz_saleload_id
       and d.inserted_date >= sysdate - 90 ";
        } else {
            $qry = "select d.*, '$' || total_price total_price, m.lz_seller_acct_id
        from lz_salesload_det d , lz_salesload_mt m
       where d.lz_saleload_id = m.lz_saleload_id ";
        }
        $cam = '';
        if ($radio_value == 'Like') {
            $cam = 'LIKE';
            $perc = '%';
        } else {
            $cam = '=';
            $perc = '';
        }

        if ($show_filter == 'true') {
            // var_dump(empty($check_box));exit;
            if ($check_box !== null) {

                if ($check_box == '0') {

                    $qry .= "and d.sales_record_number $cam '$perc$search_data$perc'";
                }
                if ($check_box == '1') {

                    $qry .= "and d.extendedorderid $cam '$perc$search_data$perc'";
                }
                if ($check_box == '2') {

                    $qry .= "and d.item_id $cam '$perc$search_data$perc'";
                }

                if ($check_box == '3') {

                    $qry .= "and d.tracking_number  $cam '$perc$search_data$perc'";
                }
            }
        } else {

            $filter_value = $this->input->post('filter_value');

            if ($filter_value['value'] !== null) {
                //var_dump($filter_value['value']); exit;
                $field = $filter_value['value'];
                $data = trim($search_data);
                $str = explode(' ', $data);
                $w = count($str);
                //var_dump($filter_value['value']);exit;
                $i = 1;
                if ($w > 1) {
                    foreach ($str as $val) {
                        $key = strtoupper(trim($val));
                        if ($i === 1) {
                            $startbreket = '(';
                        } else {
                            $startbreket = '';
                        }
                        if ($i === $w) {
                            $endbraket = ')';
                        } else {
                            $endbraket = '';
                        }

                        $qry .= "and  $startbreket upper(d.$field) $cam '$perc$key$perc' $endbraket";

                        if ($i === 1) {
                            $qry .= "and  $startbreket upper(d.$field) $cam '$perc$key$perc' $endbraket";
                        } else {
                            $qry .= " and  $startbreket upper(d.$field) $cam '$perc$key$perc' $endbraket";
                        }
                        // }

                        $i++;
                    }
                } else {
                    if ($cam == 'LIKE') {
                        foreach ($str as $val) {
                            $key = strtoupper(trim($val));
                            if ($field == 'buyer_address1') {

                                $qry .= "and  $startbreket upper(d.buyer_address1) $cam '$perc$key$perc' $endbraket";
                                $qry .= "and  $startbreket upper(d.buyer_address2) $cam '$perc$key$perc' $endbraket";
                                $qry .= "and  $startbreket upper(d.ship_to_address1) $cam '$perc$key$perc' $endbraket";
                                $qry .= "and  $startbreket upper(d.ship_to_address2) $cam '$perc$key$perc' $endbraket";
                            } else {

                                $qry .= "and upper(d.$field) $cam '$perc$key$perc'";
                            }

                            $i++;
                        }
                    } else {
                        if ($field == 'buyer_address1') {

                            $qry .= "and  $startbreket upper(d.buyer_address1) $cam '$perc$str$perc' $endbraket";
                            $qry .= "and  $startbreket upper(d.buyer_address2) $cam '$perc$str$perc' $endbraket";
                        } else {

                            $qry .= "and upper(d.$field) $cam '$perc$str$perc'";
                        }
                    }
                }
            }
        }
        $data = $this->db->query($qry)->result_array();
        if ($show_filter == 'true') {
            $group_id = "";
            if ($radio_value !== 'Like') {
                if ($show_90 == 'true') {
                    $qry = "select d.*, '$' || total_price total_price, m.lz_seller_acct_id
                from lz_salesload_det d , lz_salesload_mt m
               where d.lz_saleload_id = m.lz_saleload_id
               and d.inserted_date >= sysdate - 90 ";
                } else {
                    $qry = "select d.*, '$' || total_price total_price, m.lz_seller_acct_id
                from lz_salesload_det d , lz_salesload_mt m
               where d.lz_saleload_id = m.lz_saleload_id ";
                }
                foreach ($data as $key => $value) {
                    $group_id = $value['SALES_RECORD_NO_GROUP'];
                    if (!empty($value['SALES_RECORD_NO_GROUP'])) {

                        $qry .= "and  ( upper(d.SALES_RECORD_NO_GROUP) = '$group_id' )";
                    }
                }
            }
            if (!empty($group_id)) {
                $data = $this->db->query($qry)->result_array();
            }
        }
        // echo "<pre>";
        // print_r($data);
        // exit;
        $mesages = '';
        foreach ($data as $key => $value) {
            if (!empty($value['RETURN_ID'])) {
                $data[$key]['RETURN_ID_STATUS'] = "TRUE";
                $mesages = 'Return Already Processed';
            } else {
                $data[$key]['RETURN_ID_STATUS'] = "FALSE";
                // $mesages = 'No Cancel Or Return Id';
            }
            if (!empty($value['CANCEL_ID'])) {
                $data[$key]['CANCEL_ID_STATUS'] = "TRUE";
                $mesages = 'Item Already Canceled';
            } else {
                $data[$key]['CANCEL_ID_STATUS'] = "FALSE";
                //$mesages = 'No Cancel Or Return Id';
            }
            if (empty($value['CANCEL_ID']) && empty($value['RETURN_ID'])) {
                $mesages = 'No Cancel And No Return iid';
            }
        }

        //else {

        //     $mesages = 'No Return And No Canceled Id';
        // }

        if ($data) {
            return array("data" => $data, 'status' => true, 'mesages' => $mesages);
        } else {
            return array("data" => array(), 'status' => false, 'mesages' => '');
        }
    }

    public function add_tracking_no()
    {
        $tracking = $this->input->post('tracking');
        $orderid = $this->input->post('orderid');
        $user_id = $this->input->post('user_id');
        $data = $this->db->query("UPDATE lz_salesload_det set TRACKING_NUMBER='$tracking',TRACKING_BY='$user_id' , TRACKING_DATE=sysdate  where ORDER_ID='$orderid' ");
        if ($data) {
            return array('data' => $data, 'status' => true, 'message' => 'Inserted Secessfuly');
        } else {
            return array('status' => false);
        }
    }
    public function local_pic_up()
    {
        $oerderid = $this->input->post('oerderid');
        $user_id = $this->input->post('user_id');
        // var_dump( $user_id); exit;

        $data = $this->db->query("UPDATE lz_salesload_det set TRACKING_NUMBER='00000000000000000000000',TRACKING_BY='$user_id' , TRACKING_DATE= sysdate  where ORDER_ID='$oerderid' ");
        $get_data = $this->db->query("SELECT LZ_SALESLOAD_DET_ID FROM lz_salesload_det where LZ_SALESLOAD_DET_ID = (SELECT max(LZ_SALESLOAD_DET_ID) from lz_salesload_det)")->result_array();
        //$data = $this->db->query($get_data)->;
        $tableData = $get_data[0]['LZ_SALESLOAD_DET_ID'];
        // print_r($tableData);
        // exit;
        // $tableData = $this->db->query(" SELECT A.ser_rate_id , A.service_id, A.service_type, A.Charges, A.created_by, TO_CHAR(A.created_date, 'DD-MM-YYYY HH24:MI:SS') created_date, c.service_desc, B.USER_NAME from lj_services c, lj_service_rate A , EMPLOYEE_MT B WHERE B.EMPLOYEE_ID = A.CREATED_BY and c.service_id=A.service_id and A.SER_RATE_ID = '$tableData'")->result_array();
        $tableData = $this->db->query(" SELECT s.*,s.TRACKING_NUMBER from lz_salesload_det s  WHERE  s.LZ_SALESLOAD_DET_ID = '$tableData' order by LZ_SALESLOAD_DET_ID DESC ")->result_array();
        if ($data) {
            return array('data' => $data, 'data1' => $tableData, 'status' => true, 'message' => 'Inserted Secessfuly');
        } else {
            return array('status' => false, 'data' => []);
        }
    }

    // /********************************
    //  * end Screen Add Info
    //  *********************************/

    // /********************************
    //  * start Screen call log
    //  *********************************/

    public function Get_State()
    {
        $city_id = $this->input->post('Citydrop');
        $city_id = $city_id['value'];
        $state_id = $this->db->query("SELECT STATE_ID FROM WIZ_CITY_MT C WHERE C.CITY_ID = '$city_id'")->result_array();
        $state_id = $state_id[0]['STATE_ID'];
        $state_query = $this->db->query("SELECT * FROM WIZ_STATE_MT C WHERE C.STATE_ID = '$state_id'");
        $state_query = $state_query->result_array();
        if (count($state_query) > 0) {
            return array("status" => true, "state" => $state_query);
        } else {
            return array("status" => false, "state" => array());
        }
    }
    public function Get_State_single()
    {

        $state_id = $this->db->query("SELECT d.state_id , d.state_desc FROM WIZ_STATE_MT d")->result_array();

        return array("status" => true, "state" => $state_id);
    }
    public function Get_City_single()
    {

        $state_id = $this->db->query("SELECT f.city_id, f.city_desc from WIZ_CITY_MT f")->result_array();

        return array("status" => true, "state" => $state_id);
    }

    public function Call_log_save()
    {
        $name = $this->input->post('name');
        // $data=explode(" ",$name);
        $name1 = ucwords($name);
        $date = $this->input->post('date');
        $contact = $this->input->post('contact');
        $Sourcedrop = $this->input->post('Sourcedrop');
        $StateDrop = $this->input->post('StateDrop');
        $Type_Drop = $this->input->post('Type_Drop');
        $Citydrop = $this->input->post('Citydrop');
        $Remarks = $this->input->post('Remarks');
        $Description = $this->input->post('Description');
        $userid = $this->input->post('userid');
        $Adress = $this->input->post('Adress');
        $Price = $this->input->post('Price');
        $offer_Price = $this->input->post('offer_Price');
        $fromdate = new DateTime($date);
        $checkissuedate2 = date_format($fromdate, 'Y-m-d H:i');

        $name1 = trim(str_replace("  ", ' ', $name1));
        $name1 = trim(str_replace(array("'"), "''", $name1));

        $Remarks = trim(str_replace("  ", ' ', $Remarks));
        $Remarks = trim(str_replace(array("'"), "''", $Remarks));

        $Description = trim(str_replace("  ", ' ', $Description));
        $Description = trim(str_replace(array("'"), "''", $Description));

        $Adress = trim(str_replace("  ", ' ', $Adress));
        $Adress = trim(str_replace(array("'"), "''", $Adress));

        $qry = $this->db->query("INSERT INTO  lzw_call_log( LOG_ID, CONTACT_NO,CALL_TYPE,CALL_SOURCE, NAME,ITEM_DESC,ADDRESS,CITY_ID, STATE_ID,CALL_DATE, INSERT_DATE,INSERT_BY,REMARKS, PRICE,OFFER_PRICE) values (get_single_primary_key('lzw_call_log','LOG_ID'),'$contact','$Type_Drop','$Sourcedrop','$name1','$Description','$Adress','$Citydrop','$StateDrop',TO_DATE('$checkissuedate2','yyyy-mm-dd HH24:MI:SS'),sysdate,'$userid', '$Remarks', '$Price', '$offer_Price')");
        $tableData = $qry;
        $get_data = $this->db->query("SELECT LOG_ID FROM lzw_call_log where LOG_ID = (SELECT max(LOG_ID) from lzw_call_log)")->result_array();
        //$data = $this->db->query($get_data)->;
        $tableData = $get_data[0]['LOG_ID'];
        // print_r($tableData);
        // exit;
        // $tableData = $this->db->query(" SELECT A.ser_rate_id , A.service_id, A.service_type, A.Charges, A.created_by, TO_CHAR(A.created_date, 'DD-MM-YYYY HH24:MI:SS') created_date, c.service_desc, B.USER_NAME from lj_services c, lj_service_rate A , EMPLOYEE_MT B WHERE B.EMPLOYEE_ID = A.CREATED_BY and c.service_id=A.service_id and A.SER_RATE_ID = '$tableData'")->result_array();
        $tableData1 = $this->db->query(" SELECT a.LOG_ID,
        a.CONTACT_NO,
        a.CALL_TYPE,
        a.CALL_SOURCE,
        a.NAME,
        a.ITEM_DESC,
         a.PURCHASE_PRICE,
         a.PERCHASE_STATUS,
        a.ADDRESS,
        a.CITY_ID,
        a.STATE_ID,
        to_char(a.CALL_DATE,'yyyy-mm-dd') CALL_DATE,
        a.INSERT_DATE,
        a.INSERT_BY,
        a.REMARKS,
         a.PRICE PRICE,
        s.city_desc,
         a.OFFER_PRICE,
        f.state_desc
   from lzw_call_log a, WIZ_CITY_MT s, WIZ_STATE_MT f
  where s.city_id(+) = a.city_id
    and f.state_id(+) = a.state_id and LOG_ID = '$tableData' order by CALL_DATE DESC ")->result_array();
        // foreach ($tableData1 as $key => $value) {
        //     $tableData1[$key]['NAME'] = ucwords($value['NAME']);
        // }
        if ($tableData1) {
            return array('data' => $tableData1, 'state' => true);
        } else {
            return array('data' => [], 'state' => false);
        }
    }

    public function filter_call_log($dataArray)
    {
        $startDate = date_create($dataArray[0][0]);
        //$startDate = date_format(@$startDate, 'd/m/Y');
        $startDate = date_format(@$startDate, 'Y/m/d');
        $endDate = date_create($dataArray[0][1]);
        $endDate = date_format(@$endDate, 'Y/m/d');
        $trade_filter = $this->input->post('trade_radeo');
        $qry = "SELECT a.LOG_ID,
        a.CONTACT_NO,
        a.CALL_TYPE,
        a.CALL_SOURCE,
        a.NAME,
        a.ITEM_DESC,
        a.ADDRESS,
         a.PURCHASE_PRICE ,
         a.PERCHASE_STATUS,
        a.CITY_ID,
        a.STATE_ID,
        to_char(a.CALL_DATE,'yyyy-mm-dd ') CALL_DATE,
        a.INSERT_DATE,
        a.INSERT_BY,
        a.REMARKS,
         a.OFFER_PRICE,
        a.PERCHASE_STATUS,
         a.PRICE PRICE,
        s.city_desc,
        f.state_desc
   from lzw_call_log a, WIZ_CITY_MT s, WIZ_STATE_MT f
  where s.city_id(+) = a.city_id
    and f.state_id(+) = a.state_id
    and a.CALL_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$endDate 23:59:59',  'YYYY/MM/DD HH24:MI:SS') ";

        $check = '';
        if ($trade_filter == '1') {
            $check = '1';
            $qry .= "and a.PERCHASE_STATUS = $check";
        } else if ($trade_filter == '0') {
            $check = '0';
            $qry .= "and a.PERCHASE_STATUS = $check";
        } else {
            $qry = "SELECT a.LOG_ID,
        a.CONTACT_NO,
        a.CALL_TYPE,
        a.CALL_SOURCE,
        a.NAME,
        a.ITEM_DESC,
        a.ADDRESS,
        a.CITY_ID,
         a.PURCHASE_PRICE,
         a.PERCHASE_STATUS,
        a.STATE_ID,
        to_char(a.CALL_DATE,'yyyy-mm-dd') CALL_DATE,
        a.INSERT_DATE,
        a.INSERT_BY,
        a.REMARKS,
         a.OFFER_PRICE,
        a.PERCHASE_STATUS,
        a.PRICE PRICE,
        s.city_desc,
        f.state_desc
   from lzw_call_log a, WIZ_CITY_MT s, WIZ_STATE_MT f
  where s.city_id(+) = a.city_id
    and f.state_id(+) = a.state_id
    and a.CALL_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$endDate 23:59:59',  'YYYY/MM/DD HH24:MI:SS')
    order by CALL_DATE DESC";
        }

        $qry_final = $this->db->query($qry)->result_array();
        if ($qry_final) {
            return array('data' => $qry_final, 'status' => true);
        } else {
            return array('data' => [], 'status' => false);
        }
    }
    public function date_filter($dataArray)
    {
        $startDate = date_create($dataArray[0][0]);
        //$startDate = date_format(@$startDate, 'd/m/Y');
        $startDate = date_format(@$startDate, 'Y/m/d');
        $endDate = date_create($dataArray[0][1]);
        $endDate = date_format(@$endDate, 'Y/m/d');
        // var_dump($startDate,$endDate ); exit;

        $qry = $this->db->query("SELECT a.LOG_ID,
               a.CONTACT_NO,
               a.CALL_TYPE,
               a.CALL_SOURCE,
               a.NAME,
               a.ITEM_DESC,
                a.PURCHASE_PRICE,
                a.PERCHASE_STATUS,
               a.ADDRESS,
               a.CITY_ID,
               a.STATE_ID,
               to_char(a.CALL_DATE, 'yyyy-mm-dd') CALL_DATE,
               a.INSERT_DATE,
               a.INSERT_BY,
               a.REMARKS,
               a.OFFER_PRICE,
               a.PRICE PRICE,
               s.city_desc,
               f.state_desc
                from lzw_call_log a, WIZ_CITY_MT s, WIZ_STATE_MT f
               where s.city_id(+) = a.city_id
                 and f.state_id(+) = a.state_id
                 and a.INSERT_DATE between
                     TO_DATE('$startDate 00:00:00', 'YYYY-MM-DD HH24:MI:SS') and
                     TO_DATE('$endDate 23:59:59', 'YYYY-MM-DD HH24:MI:SS')")->result_array();
        if ($qry) {
            return array('data' => $qry, 'status' => true);
        } else {

            return array('data' => [], 'status' => false);
        }
    }

    public function call_log_save_all()
    {
        $date = $this->input->post('data');
        $startDate = date_create($date);
        $startDate = date_format(@$startDate, 'Y/m/d');
        // var_dump($startDate); exit;
        $tableData = $this->db->query("SELECT a.LOG_ID,
        a.CONTACT_NO,
        a.CALL_TYPE,
        a.CALL_SOURCE,
        a.NAME,
        a.ITEM_DESC,
        a.ADDRESS,
         a.PURCHASE_PRICE,
         a.PERCHASE_STATUS,
        a.CITY_ID,
        a.STATE_ID,
        to_char(a.CALL_DATE,'yyyy-mm-dd') CALL_DATE,
        a.INSERT_DATE,
        a.INSERT_BY,
        a.REMARKS,
         a.OFFER_PRICE,
         a.PRICE PRICE,
        s.city_desc,
        f.state_desc
   from lzw_call_log a, WIZ_CITY_MT s, WIZ_STATE_MT f
  where s.city_id(+) = a.city_id
    and f.state_id(+) = a.state_id
    and a.CALL_DATE BETWEEN TO_DATE('$startDate 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND TO_DATE('$startDate 23:59:59',  'YYYY/MM/DD HH24:MI:SS')  order by CALL_DATE DESC")->result_array();

        if ($tableData) {
            return array('data' => $tableData, 'state' => true);
        } else {
            return array('data' => [], 'state' => false);
        }
    }
    public function delete_log()
    {
        $logId = $this->input->post('id');
        $tableData = $this->db->query(" DELETE  from lzw_call_log  where  LOG_ID='$logId'");
        return $tableData;
    }
    public function update_call_log()
    {

        $logId = $this->input->post('LOG_ID');
        $ContactNumber_update = $this->input->post('ContactNumber_update');
        $name_update = $this->input->post('name_update');
        $Sourcedrop_update = $this->input->post('Sourcedrop_update');
        $Type_Drop_update = $this->input->post('Type_Drop_update');
        $Description_update = $this->input->post('Description_update');
        $Adress_update = $this->input->post('Adress_update');
        $Citydrop_update = $this->input->post('Citydrop_update');

        if (array_key_exists('value', $Citydrop_update)) {
            $Citydrop_update1 = $Citydrop_update['value'];
        } else {
            $Citydrop_update1 = "";
        }
        $StateDrop_update = $this->input->post('StateDrop_update');
        if (array_key_exists('value', $StateDrop_update)) {
            $StateDrop_update1 = $StateDrop_update['value'];
        } else {
            $StateDrop_update1 = "";
        }

        $date_update = $this->input->post('date_update');
        $Remarks_update = $this->input->post('Remarks_update');
        $userid = $this->input->post('userid');
        $Price_update = $this->input->post('Price_update');
        $fromdate = new DateTime($date_update);
        $checkissuedate2 = date_format($fromdate, 'Y-m-d H:i');
        $offer_Price = $this->input->post('Offer_Price_update');

        $name_update = trim(str_replace("  ", ' ', $name_update));
        $name_update = trim(str_replace(array("'"), "''", $name_update));

        $Description_update = trim(str_replace("  ", ' ', $Description_update));
        $Description_update = trim(str_replace(array("'"), "''", $Description_update));

        $Remarks_update = trim(str_replace("  ", ' ', $Remarks_update));
        $Remarks_update = trim(str_replace(array("'"), "''", $Remarks_update));

        $Adress_update = trim(str_replace("  ", ' ', $Adress_update));
        $Adress_update = trim(str_replace(array("'"), "''", $Adress_update));

        //var_dump($Citydrop_update1);exit;
        $data = $this->db->query(" UPDATE lzw_call_log set CONTACT_NO ='$ContactNumber_update',CALL_TYPE='$Type_Drop_update',CALL_SOURCE='$Sourcedrop_update', NAME='$name_update',ITEM_DESC='$Description_update',ADDRESS='$Adress_update',CITY_ID='$Citydrop_update1', STATE_ID='$StateDrop_update1',CALL_DATE= TO_DATE('$checkissuedate2','yyyy-mm-dd HH24:MI:SS'), INSERT_DATE=sysdate,INSERT_BY='$userid',REMARKS='$Remarks_update', PRICE='$Price_update', OFFER_PRICE='$offer_Price' where  LOG_ID='$logId'");

        return $data;
    }

    public function MarkAsPurchase()
    {
        $purchase = $this->input->post('purchase_pice');
        $PERCHASE_STATUS = $this->input->post('PERCHASE_STATUS');
        $log_id = $this->input->post('LOG_ID');

        $qry = $this->db->query("UPDATE lzw_call_log set PURCHASE_PRICE ='$purchase', PERCHASE_STATUS='$PERCHASE_STATUS' where LOG_ID='$log_id'");
        return $qry;
    }
    // /********************************
    //  * end Screen call log
    //  *********************************/
    // /********************************
    //  * start Screen Listed Barcode
    //  *********************************/

    public function get_offerUp()
    {
        $data = $this->db->query("SELECT * from lz_portal_mt p  where p.portal_id IN ('9', '12', '11') ")->result_array();
        return array('data' => $data, 'status' => true);
    }
    public function get_conditionArray()
    {
        $data = $this->db->query("SELECT d.id, d.cond_name from lz_item_cond_mt d")->result_array();
        return array('data' => $data, 'status' => true);
    }
    public function Get_listed_barcode()
    {
        $barcode = $this->input->post('barcodeNo');
        $data = $this->db->query("SELECT *
        FROM (SELECT 'DEKIT ITEM' BAROCDE_TYPE,
                     DECODE(D.LZ_MANIFEST_DET_ID, NULL, 'PENDING', 'POSTED') VERIFY_ITEM,
                     D.LZ_DEKIT_US_DT_ID,
                     D.BARCODE_PRV_NO,
                     C.COND_NAME,
                     C.ID,
                     O.OBJECT_NAME,
                     O.OBJECT_ID,
                     D.PIC_DATE_TIME,
                     D.PIC_BY,
                     D.PIC_NOTES,
                     DECODE(D.MPN_DESCRIPTION,
                            NULL,
                            CA.MPN_DESCRIPTION,
                            D.MPN_DESCRIPTION) MPN_DESC,
                     CA.MPN,
                     CA.UPC,
                     CA.BRAND,
                     TO_CHAR(D.FOLDER_NAME) FOLDER_NAME,
                     D.DEKIT_REMARKS REMARKS,
                     D.LZ_MANIFEST_DET_ID,
                     CA.CATEGORY_ID,
                     TO_CHAR(D.AVG_SELL_PRICE) AVG_PRIC
                FROM LZ_DEKIT_US_DT   D,
                     LZ_BD_OBJECTS_MT O,
                     LZ_CATALOGUE_MT  CA,
                     LZ_ITEM_COND_MT  C
               WHERE D.OBJECT_ID = O.OBJECT_ID(+)
                 AND D.CATALOG_MT_ID = CA.CATALOGUE_MT_ID(+)
                 AND D.CONDITION_ID = C.ID(+)
              UNION ALL
              SELECT 'SPECIAL LOT' BAROCDE_TYPE,
                     DECODE(L.LZ_MANIFEST_DET_ID, NULL, 'PENDING', 'POSTED') VERIFY_ITEM,
                     L.SPECIAL_LOT_ID,
                     L.BARCODE_PRV_NO,
                     C.COND_NAME,
                     C.ID,
                     OB.OBJECT_NAME,
                     OB.OBJECT_ID,
                     L.PIC_DATE_TIME,
                     L.PIC_BY,
                     L.PIC_NOTES,
                     DECODE(L.MPN_DESCRIPTION,
                            NULL,
                            CA.MPN_DESCRIPTION,
                            L.MPN_DESCRIPTION) MPN_DESC,
                     DECODE(L.CARD_MPN, NULL, CA.MPN, L.CARD_MPN) MPN,
                     DECODE(L.CARD_UPC, NULL, CA.UPC, L.CARD_UPC) UPC,
                     DECODE(L.BRAND, NULL, CA.BRAND, L.BRAND) BRAND,
                     TO_CHAR(L.FOLDER_NAME) FOLDER_NAME,
                     L.LOT_REMARKS REMARKS,
                     L.LZ_MANIFEST_DET_ID,
                     CA.CATEGORY_ID,
                     TO_CHAR(l.AVG_SOLD) AVG_PRIC
                FROM LZ_SPECIAL_LOTS  L,
                     LZ_BD_OBJECTS_MT OB,
                     LZ_ITEM_COND_MT  C,
                     LZ_CATALOGUE_MT  CA
               WHERE L.OBJECT_ID = OB.OBJECT_ID(+)
                 AND L.CONDITION_ID = C.ID(+)
                 AND L.CATALOG_MT_ID = CA.CATALOGUE_MT_ID(+)
               ORDER BY LZ_MANIFEST_DET_ID NULLS FIRST)
       WHERE BARCODE_PRV_NO = '$barcode'")->result_array();
        if ($data) {
            return array('data' => $data, 'status' => true);
        } else {
            return array('data' => '', 'status' => false);
        }
    }

    public function Get_Image_DecodeBase64()
    {

        $barocde_no = $this->input->post('barcodeNo');
        $bar_val = $barocde_no;
        // //var_dump($barocde_no['barcodePass']);
        //  var_dump($barocde_no);
        //  exit;
        //$bar_val = $barocde_no['barcodePass'] ;

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();

        $master_path = $path[0]["MASTER_PATH"];

        $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

        $barcode = $qry[0]["FOLDER_NAME"];

        $dir = $master_path . $barcode . "/";

        $dir = preg_replace("/[\r\n]*/", "", $dir);

        $uri = [];

        //var_dump(is_dir($dir));exit;
        if (is_dir($dir)) {
            // var_dump($dir);exit;
            $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
            $i = 0;
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            foreach ($images as $image) {
                $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                $uri[$i] = 'http://71.78.236.20/' . $withoutMasterPartUri;

                $i++;
            }
        }

        //var_dump($dekitted_pics);exit;
        return array('uri' => $uri);
    }

    public function Save_listed_barcode()
    {

        $title = $this->input->post('Title');
        $cond = $this->input->post('Condition');
        $cond_id = $cond['value'];
        $cond_name = $cond['label'];
        $brand = $this->input->post('Brand');
        $mpn = $this->input->post('Mpn');
        $upc = $this->input->post('UPC');
        $barcode_no = $this->input->post('barcode_no');
        $data = $this->db->query("SELECT BARCODE_NO from lj_classified_ad_mt where BARCODE_NO= '$barcode_no'")->result_array();
        // echo "haji fucking fuck";
        // exit();
        if (count($data) == 0) {
            // echo "kjhdsfkjhdsa";
            // exit();
            $qry = $this->db->query("INSERT INTO  lj_classified_ad_mt( AD_ID, ITEM_DESC,CONDITION_ID,CONDITION_NAME, BRAND,MPN,UPC, PIC_DIR,BARCODE_NO) values (get_single_primary_key('lj_classified_ad_mt','AD_ID'),'$title','$cond_id','$cond_name','$brand','$mpn','$upc','$barcode_no','$barcode_no')");
            $tableData = $qry;
            $get_data = $this->db->query("SELECT AD_ID FROM lj_classified_ad_mt where AD_ID = (SELECT max(AD_ID) from lj_classified_ad_mt)")->result_array();
            $tableData = $get_data[0]['AD_ID'];

            $tableData1 = $this->db->query(" SELECT C.AD_ID, C.ITEM_DESC,C.CONDITION_ID,C.CONDITION_NAME, C.BRAND,C.MPN,C.UPC, C.PIC_DIR,C.BARCODE_NO BARCODE_PRV_NO,
            NVL(P.OFFERUP, 0) OFFERUP,
            NVL(P.FB, 0) fb,
            NVL(P.CRAIGLIST, 0) CRAIGLIST
       FROM LJ_CLASSIFIED_AD_MT C, lj_listed_ad_portals P
      WHERE C.AD_ID = P.AD_ID(+)
      and C.AD_ID ='$tableData' order by C.AD_ID DESC ")->result_array();
            // $this-get_listed_barcode_all();

            if ($tableData1) {
                $qyer = "SELECT C.AD_ID, C.ITEM_DESC,C.CONDITION_ID,C.CONDITION_NAME, C.BRAND,C.MPN,C.UPC, C.PIC_DIR,C.BARCODE_NO BARCODE_PRV_NO,
                NVL(P.OFFERUP, 0) OFFERUP,
                NVL(P.FB, 0) fb,
                NVL(P.CRAIGLIST, 0) CRAIGLIST
           FROM LJ_CLASSIFIED_AD_MT C, lj_listed_ad_portals P
          WHERE C.AD_ID = P.AD_ID(+) order by C.AD_ID DESC";

                $qyer = $this->db->query($qyer)->result_array();

                if (count($qyer) >= 1) {

                    $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
                    $uri = $this->get_identiti_bar_pics($qyer, $conditions);
                    $images = $uri['uri'];

                    foreach ($qyer as $key => $value) {

                        $qyer[$key]['MPN'] = str_replace('-', ' ', $value['MPN']);
                    }
                    // return array("images" => $images, 'query' => $qyer);
                }
                return array('data' => $qyer, "images" => $images, 'state' => true);
            } else {
                return array('data' => $qyer, 'state' => false);
            }
        } else {
            return array('data' => '', 'state' => false, 'message' => 'Barcode Already Exist');
        }
    }

    public function get_listed_barcode_all()
    {
        $qyer = "SELECT C.AD_ID, C.ITEM_DESC,C.CONDITION_ID,C.CONDITION_NAME, C.BRAND,C.MPN,C.UPC, C.PIC_DIR,C.BARCODE_NO BARCODE_PRV_NO,
        NVL(P.OFFERUP, 0) OFFERUP,
        NVL(P.FB, 0) fb,
        NVL(P.CRAIGLIST, 0) CRAIGLIST
   FROM LJ_CLASSIFIED_AD_MT C, lj_listed_ad_portals P
  WHERE C.AD_ID = P.AD_ID(+) order by C.AD_ID DESC";

        $qyer = $this->db->query($qyer)->result_array();

        if (count($qyer) >= 1) {

            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_pictures_by_barcode($qyer, $conditions);
            $images = $uri['uri'];

            foreach ($qyer as $key => $value) {

                $qyer[$key]['MPN'] = str_replace('-', ' ', $value['MPN']);
            }
            return array("images" => $images, 'query' => $qyer);
        } else {
            return array('images' => [], 'query' => [], 'exist' => false);
        }
    }

    public function get_pictures_by_barcode_listeview($barcodes, $conditions)
    {

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = 2");
        $path = $path->result_array();

        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        foreach ($barcodes as $barcode) {

            $bar = $barcode['BARCODE_NO'];

            $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE LOT.BARCODE_PRV_NO = '$bar' ")->result_array();
            $folderName = "";
            if ($getFolder) {
                $folderName = $getFolder[0]['FOLDER_NAME'];
            } else {
                $folderName = $bar;
            }

            $dir = "";
            $barcodePictures = $master_path . $folderName . "/";
            $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

            if (is_dir($barcodePictures)) {
                $dir = $barcodePictures;
            } else if (is_dir($barcodePicturesThumb)) {
                $dir = $barcodePicturesThumb;
            }

            if (!is_dir($dir)) {
                $getBarcodeMt = $this->db->query("SELECT SD.F_UPC UPC, SD.F_MPN MPN
        FROM LZ_BARCODE_MT BM, LZ_ITEM_SEED SD
        WHERE BM.ITEM_ID = SD.ITEM_ID
        AND BM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID
        AND BM.CONDITION_ID = SD.DEFAULT_COND
        AND BM.BARCODE_NO = '$bar'
        ")->result_array();
                $upc = "";
                $mpn = "";
                if ($getBarcodeMt) {
                    $upc = $getBarcodeMt[0]['UPC'];
                    $mpn = $getBarcodeMt[0]['MPN'];
                }

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = 1");
                $path = $path->result_array();

                $upc_mpn_master_path = $path[0]["MASTER_PATH"];

                $mpn = str_replace("/", "_", $mpn);
                foreach ($conditions as $cond) {
                    $dir = $upc_mpn_master_path . $upc . "~" . $mpn . "/" . $cond['COND_NAME'] . "/";
                    $dirWithMpn = $upc_mpn_master_path . "~" . $mpn . "/" . $cond['COND_NAME'] . "/";
                    $dirWithUpc = $upc_mpn_master_path . $upc . "~/" . $cond['COND_NAME'] . "/";
                    if (is_dir($dir)) {
                        $dir = $dir;
                        break;
                    } else if (is_dir($dirWithMpn)) {
                        $dir = $dirWithMpn;
                        break;
                    } else if (is_dir($dirWithUpc)) {
                        $dir = $dirWithUpc;
                        break;
                    } else {
                        $dir = $upc_mpn_master_path . "/" . $cond['COND_NAME'] . "/";
                    }
                }
                if ($dir == $upc_mpn_master_path . "~/" . $cond['COND_NAME'] . "/") {
                    $dir = $upc_mpn_master_path . "/" . $cond['COND_NAME'] . "/";
                }
            }
            $dir = preg_replace("/[\r\n]*/", "", $dir);

            if (is_dir($dir)) {
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

                if ($images) {
                    $j = 0;
                    $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $images[0]);
                    $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                    $withoutMasterPartUri = $this->convert_text($withoutMasterPartUri);
                    $uri[$bar][$j] = $base_url . $withoutMasterPartUri;
                    // foreach ($images as $image) {

                    //     $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                    //     $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                    //     $uri[$bar][$j] = $base_url . $withoutMasterPartUri;

                    //     $j++;
                    // }
                } else {
                    $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar][1] = false;
                }
            } else {
                $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar][1] = false;
            }
        }
        return array('uri' => $uri);
    }
    public function get_pictures_by_barcode($barcodes, $conditions)
    {

        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = 2");
        $path = $path->result_array();

        $master_path = $path[0]["MASTER_PATH"];
        $uri = array();
        $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        foreach ($barcodes as $barcode) {

            $bar = $barcode['BARCODE_NO'];

            $getFolder = $this->db->query("SELECT LOT.FOLDER_NAME FROM LZ_SPECIAL_LOTS LOT WHERE LOT.BARCODE_PRV_NO = '$bar' ")->result_array();
            $folderName = "";
            if ($getFolder) {
                $folderName = $getFolder[0]['FOLDER_NAME'];
            } else {
                $folderName = $bar;
            }

            $dir = "";
            $barcodePictures = $master_path . $folderName . "/";
            $barcodePicturesThumb = $master_path . $folderName . "/thumb" . "/";

            if (is_dir($barcodePictures)) {
                $dir = $barcodePictures;
            } else if (is_dir($barcodePicturesThumb)) {
                $dir = $barcodePicturesThumb;
            }

            if (!is_dir($dir)) {
                $getBarcodeMt = $this->db->query("SELECT SD.F_UPC UPC, SD.F_MPN MPN
        FROM LZ_BARCODE_MT BM, LZ_ITEM_SEED SD
        WHERE BM.ITEM_ID = SD.ITEM_ID
        AND BM.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID
        AND BM.CONDITION_ID = SD.DEFAULT_COND
        AND BM.BARCODE_NO = '$bar'
        ")->result_array();
                $upc = "";
                $mpn = "";
                if ($getBarcodeMt) {
                    $upc = $getBarcodeMt[0]['UPC'];
                    $mpn = $getBarcodeMt[0]['MPN'];
                }

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = 1");
                $path = $path->result_array();

                $upc_mpn_master_path = $path[0]["MASTER_PATH"];

                $mpn = str_replace("/", "_", $mpn);
                foreach ($conditions as $cond) {
                    $dir = $upc_mpn_master_path . $upc . "~" . $mpn . "/" . $cond['COND_NAME'] . "/";
                    $dirWithMpn = $upc_mpn_master_path . "~" . $mpn . "/" . $cond['COND_NAME'] . "/";
                    $dirWithUpc = $upc_mpn_master_path . $upc . "~/" . $cond['COND_NAME'] . "/";
                    if (is_dir($dir)) {
                        $dir = $dir;
                        break;
                    } else if (is_dir($dirWithMpn)) {
                        $dir = $dirWithMpn;
                        break;
                    } else if (is_dir($dirWithUpc)) {
                        $dir = $dirWithUpc;
                        break;
                    } else {
                        $dir = $upc_mpn_master_path . "/" . $cond['COND_NAME'] . "/";
                    }
                }
                if ($dir == $upc_mpn_master_path . "~/" . $cond['COND_NAME'] . "/") {
                    $dir = $upc_mpn_master_path . "/" . $cond['COND_NAME'] . "/";
                }
            }
            $dir = preg_replace("/[\r\n]*/", "", $dir);

            if (is_dir($dir)) {
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);

                if ($images) {
                    $j = 0;
                    // $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $images[0]);
                    // $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                    // $uri[$bar][$j] = $base_url . $withoutMasterPartUri;
                    foreach ($images as $image) {

                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                        $uri[$bar][$j] = $base_url . $withoutMasterPartUri;

                        $j++;
                    }
                } else {
                    $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                    $uri[$bar][1] = false;
                }
            } else {
                $uri[$bar][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar][1] = false;
            }
        }
        return array('uri' => $uri);
    }

    public function Save_marcket_place()
    {
        $id_add = $this->input->post('id_add');
        $port_id = $this->input->post('port_id');
        $url = $this->input->post('url');
        $price = $this->input->post('price');
        $dty = $this->input->post('dty');
        $listed_by = $this->input->post('listed_by');
        $LIST_ID = $this->input->post('LIST_ID');
        // var_dump($id_add,$port_id); exit;
        $ckeck_list_id = $this->db->query("SELECT LIST_ID from lj_classified_ad_listing l where l.LIST_ID='$LIST_ID' AND l.AD_ID = '$id_add'and l.portal_id='$port_id'")->result_array();
        if (count($ckeck_list_id) == 0) {

            $ckeck = $this->db->query("SELECT * from lj_classified_ad_listing l where l.ad_id='$id_add' and l.portal_id='$port_id' and l.list_url='$url'")->result_array();
            if (count($ckeck) == 0) {
                $qry = $this->db->query("INSERT INTO  lj_classified_ad_listing( LIST_ID, AD_ID, PORTAL_ID,LISTING_ID,LIST_URL,LIST_DATE,LIST_QTY,LIST_BY,LIST_PRICE ,LIST_STATUS) values (get_single_primary_key('lj_classified_ad_listing','LIST_ID'),'$id_add','$port_id','0','$url',sysdate,'$dty','$listed_by','$price', '1')");

                if ($qry) {
                    return array("data" => $qry, 'status' => true);
                } else {
                    return array("data" => '', 'status' => false);
                }
            } else {
                return array("data" => '', 'status' => false);
            }
        } else {
            $qry = $this->db->query("UPDATE lj_classified_ad_listing set LIST_URL ='$url',LIST_PRICE='$price' where LIST_ID='$LIST_ID'");
            return array("data" => $qry, 'status' => true);
        }
    }
    public function Save_marcket_place_as_sold()
    {
        $Price = $this->input->post('Price');
        $dty = $this->input->post('dty');
        $date = $this->input->post('date');
        $listed_by = $this->input->post('listed_by');
        $LIST_ID = $this->input->post('LIST_ID');
        // var_dump($LIST_ID);exit;
        $fromdate = new DateTime($date);
        $checkissuedate2 = date_format($fromdate, 'Y-m-d H:i');
        $ckeck = $this->db->query("SELECT LIST_ID from lj_classified_ad_sold l where l.LIST_ID='$LIST_ID'")->result_array();
        if (count($ckeck) == 0) {
            $qry = $this->db->query("INSERT INTO  lj_classified_ad_sold( SALE_ID, LIST_ID, SALE_PRICE,SALE_QTY,SALE_DATE,INSERTED_DATE,INSERTED_BY) values (get_single_primary_key('lj_classified_ad_sold','SALE_ID'),'$LIST_ID','$Price','$dty',TO_DATE('$checkissuedate2','yyyy-mm-dd HH24:MI:SS'),sysdate,'$listed_by')");
            if ($qry) {
                return array("data" => $qry, 'status' => true);
            } else {
                return array("data" => '', 'status' => false);
            }
        } else {
            return array("data" => '', 'status' => false);
        }
    }

    public function to_check_list_id()
    {
        $portal_id = $this->input->post('portal_id');
        $add_id = $this->input->post('add_id');
        $qry = $this->db->query("SELECT d.list_id , d.list_url, d.list_price from lj_classified_ad_listing d where d.ad_id='$add_id'and d.portal_id='$portal_id'")->result_array();
        if ($qry) {
            return array("data" => $qry, 'status' => true);
        } else {
            return array("data" => '', 'status' => false);
        }
    }
    public function Get_filter_barcode()
    {
        $barcode = $this->input->post('barcode');
        $Item = $this->input->post('Item');
        // var_dump($Item); exit;
        if ($Item == '2') {
            $qry = "SELECT B.BARCODE_NO BARCODE_PRV_NO,
            TO_CHAR(l.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
            l.LIST_ID,
            l.STATUS,
            b.HOLD_STATUS,
            l.LIST_PRICE LIST_PRICE,
            e.user_name lister_name,
            S.SEED_ID,
            M.SINGLE_ENTRY_ID,
            M.LZ_MANIFEST_ID,
            M.LOADING_NO,
            M.LOADING_DATE,
            M.PURCH_REF_NO,
            B.ITEM_ID,
            d.laptop_item_code,
            S.ITEM_TITLE ITEM_MT_DESC,
            s.f_manufacture MANUFACTURER,
            l.list_qty,
            S.F_MPN MFG_PART_NO,
            S.F_UPC UPC,
            S.QC_CHECK,
       S.QC_REMARKS,
       S.QC_BY,
            BCD.EBAY_ITEM_ID,
            BCD.CONDITION_ID ITEM_CONDITION,
            BCD.QTY QUANTITY,
            BCD.COST_PRICE COST_PRICE,
            C.COND_NAME,
            R.BUISNESS_NAME,
            b.K2BAY_LIST_ID
       from lz_barcode_mt b,
            ebay_list_mt l,
            employee_mt e,
            lz_manifest_mt m,
            lz_manifest_det d,
            items_mt i,
            lz_item_seed s,
            LZ_ITEM_COND_MT c,
            lj_merhcant_acc_dt a,
            lz_merchant_mt r,
            (SELECT BC.EBAY_ITEM_ID,
                    BC.LZ_MANIFEST_ID,
                    BC.ITEM_ID,
                    BC.CONDITION_ID,
                    COUNT(1) QTY,
                    max(dt.po_detail_retial_price) COST_PRICE
               FROM LZ_BARCODE_MT BC,
                    items_mt i,
                    lz_manifest_det dt,
                    (select item_id, lz_manifest_id, condition_id
                       from lz_barcode_mt
                      where EBAY_ITEM_ID = '$barcode') bb
              WHERE BC.Item_Id = bb.item_id
                and bc.lz_manifest_id = bb.lz_manifest_id
                and bc.condition_id = bb.condition_id
                and dt.lz_manifest_id = bc.lz_manifest_id
                and dt.laptop_item_code = i.item_code
                and to_char(dt.conditions_seg5) = to_char(bc.condition_id)
                and i.item_id = bc.item_id
              GROUP BY BC.LZ_MANIFEST_ID,
                       BC.ITEM_ID,
                       BC.CONDITION_ID,
                       BC.EBAY_ITEM_ID) bcd
      where b.list_id = l.list_id
        and e.employee_id = l.lister_id
        and m.lz_manifest_id = b.lz_manifest_id
        and d.laptop_item_code = i.item_code
        and d.lz_manifest_id = m.lz_manifest_id
        and b.item_id = i.item_id
        and s.item_id = b.item_id
        and s.lz_manifest_id = b.lz_manifest_id
        and s.default_cond = b.condition_id
        and b.condition_id = c.id
        AND R.MERCHANT_ID = A.MERCHANT_ID
        AND l.LZ_SELLER_ACCT_ID = A.ACCT_ID
        and bcd.item_id = b.item_id
        and bcd.lz_manifest_id = b.lz_manifest_id
        and b.EBAY_ITEM_ID = '$barcode'";
        } else {
            $qry = "SELECT B.BARCODE_NO BARCODE_PRV_NO,
            TO_CHAR(l.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
            l.LIST_ID,
            l.STATUS,
            b.HOLD_STATUS,
            l.LIST_PRICE LIST_PRICE,
            e.user_name lister_name,
            S.SEED_ID,
            M.SINGLE_ENTRY_ID,
            M.LZ_MANIFEST_ID,
            M.LOADING_NO,
            M.LOADING_DATE,
            M.PURCH_REF_NO,
            B.ITEM_ID,
            d.laptop_item_code,
            S.ITEM_TITLE ITEM_MT_DESC,
            s.f_manufacture MANUFACTURER,
            l.list_qty,
            S.F_MPN MFG_PART_NO,
            S.F_UPC UPC,
            S.QC_CHECK,
       S.QC_REMARKS,
       S.QC_BY,
            BCD.EBAY_ITEM_ID,
            BCD.CONDITION_ID ITEM_CONDITION,
            BCD.QTY QUANTITY,
            BCD.COST_PRICE COST_PRICE,
            C.COND_NAME,
            (select m.buisness_name
               from lz_merchant_barcode_mt mt,
                    lz_merchant_barcode_dt dt,
                    lz_merchant_mt         m
              where mt.mt_id = dt.mt_id
                and m.merchant_id = mt.merchant_id
                and dt.barcode_no = b.barcode_no) BUISNESS_NAME,
                b.K2BAY_LIST_ID
       from lz_barcode_mt b,
            ebay_list_mt l,
            employee_mt e,
            lz_manifest_mt m,
            lz_manifest_det d,
            items_mt i,
            lz_item_seed s,
            LZ_ITEM_COND_MT c,
            (SELECT BC.EBAY_ITEM_ID,
                    BC.LZ_MANIFEST_ID,
                    BC.ITEM_ID,
                    BC.CONDITION_ID,
                    COUNT(1) QTY,
                    max(dt.po_detail_retial_price) COST_PRICE
               FROM LZ_BARCODE_MT BC,
                    items_mt i,
                    lz_manifest_det dt,
                    (select item_id, lz_manifest_id, condition_id
                       from lz_barcode_mt
                      where barcode_no = '$barcode') bb
              WHERE BC.Item_Id = bb.item_id
                and bc.lz_manifest_id = bb.lz_manifest_id
                and bc.condition_id = bb.condition_id
                and dt.lz_manifest_id = bc.lz_manifest_id
                and dt.laptop_item_code = i.item_code
                and to_char(dt.conditions_seg5) = to_char(bc.condition_id)
                and i.item_id = bc.item_id
              GROUP BY BC.LZ_MANIFEST_ID,
                       BC.ITEM_ID,
                       BC.CONDITION_ID,
                       BC.EBAY_ITEM_ID) bcd
      where b.list_id = l.list_id(+)
        and e.employee_id(+) = l.lister_id
        and m.lz_manifest_id = b.lz_manifest_id
        and d.laptop_item_code = i.item_code
        and d.lz_manifest_id = m.lz_manifest_id
        and b.item_id = i.item_id
        and s.item_id = b.item_id
        and s.lz_manifest_id = b.lz_manifest_id
        and s.default_cond = b.condition_id
        and b.condition_id = c.id
        and bcd.item_id = b.item_id
        and bcd.lz_manifest_id = b.lz_manifest_id
        and bcd.condition_id = b.condition_id
        and b.barcode_no = '$barcode'
     ";
        }

        $qyer = $this->db->query($qry)->result_array();

        if (count($qyer) >= 1) {

            $conditions = $this->db->query("SELECT * FROM LZ_ITEM_COND_MT A where A.COND_DESCRIPTION is not null order by a.id")->result_array();
            $uri = $this->get_identiti_bar_pics($qyer, $conditions);
            $images = $uri['uri'];

            foreach ($qyer as $key => $value) {

                $qyer[$key]['MFG_PART_NO'] = str_replace('-', ' ', $value['MFG_PART_NO']);
                $current_timestamp = date('m/d/Y');
                $purchase_date = $value['LOADING_DATE'];
                $date1 = date_create($current_timestamp);
                $date2 = date_create($purchase_date);
                $diff = date_diff($date1, $date2);
                $date_rslt = $diff->format("%R%a days");
                $qyer[$key]['LOADING_DATE'] = abs($date_rslt) . " Days";
            }
            //   print_r($qyer);
            return array("images" => $images, 'query' => $qyer, 'status' => true);
        } else {
            return array('images' => [], 'query' => [], 'status' => false);
        }
    }

    public function deleteItemfromShopify($ebay_id)
    {

        $qry = $this->db->query("SELECT B.SHOPIFY_LIST_ID FROM LZ_BARCODE_MT B WHERE B.EBAY_ITEM_ID = '$ebay_id' AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND ROWNUM = 1")->result_array();
        $list_id = @$qry[0]["SHOPIFY_LIST_ID"];

        $qry2 = $this->db->query("SELECT S.SHOPIFY_ID FROM SHOPIFY_LIST_MT S WHERE S.LIST_ID = '$list_id' ")->result_array();
        $shopify_id = @$qry2[0]["SHOPIFY_ID"];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://c084db0733b8db2aef9f2a3a37b7314e:d19b1f6eece15313ce289b80e206286f@k2bay.myshopify.com/admin/products/$shopify_id.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Postman-Token: aeb8d737-1fa8-4ef8-b4e9-312f675d0d2e",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "Shopify Item Delete Error #:" . $err;
        } else {

            $check_sold = $this->db->query("SELECT S.SHOPIFY_ID FROM SHOPIFY_ORDERS S WHERE S.LINE_ITEMS_PRODUCT_ID =  '$shopify_id'");
            if ($check_sold->num_rows() > 0) {
                $barcode_qry = $this->db->query("UPDATE LZ_BARCODE_MT B SET B.SHOPIFY_LIST_ID = '' WHERE B.SHOPIFY_LIST_ID = '$shopify_id' ");
            } else {
                $barcode_qry = $this->db->query("UPDATE LZ_BARCODE_MT B SET B.SHOPIFY_LIST_ID = '' WHERE B.SHOPIFY_LIST_ID = '$shopify_id' ");
                $delete_qry = $this->db->query("DELETE FROM SHOPIFY_LIST_MT E WHERE E.SHOPIFY_ID = '$shopify_id'");
                return array("data" => $delete_qry);
            }
            //echo $response;
        }
    }

    public function endItem()
    {
        $ebay_id = $this->input->post('ebay_id');
        // check if item sold or not
        $check_sold = $this->db->query("SELECT * FROM LZ_SALESLOAD_DET D WHERE D.ITEM_ID = '$ebay_id'");
        if ($check_sold->num_rows() > 0) {
            $barcode_qry = $this->db->query("UPDATE LZ_BARCODE_MT B SET B.LIST_ID = '',B.EBAY_ITEM_ID='' WHERE B.EBAY_ITEM_ID = '$ebay_id'AND B.SALE_RECORD_NO IS NULL AND B.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND B.LZ_PART_ISSUE_MT_ID IS NULL AND B.LZ_POS_MT_ID IS NULL AND B.PULLING_ID IS NULL");
        } else {
            $barcode_qry = $this->db->query("UPDATE LZ_BARCODE_MT B SET B.LIST_ID = '',B.EBAY_ITEM_ID='' WHERE B.EBAY_ITEM_ID = '$ebay_id'AND B.SALE_RECORD_NO IS NULL AND B.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND B.LZ_PART_ISSUE_MT_ID IS NULL AND B.LZ_POS_MT_ID IS NULL AND B.PULLING_ID IS NULL");

            $delete_qry = $this->db->query("DELETE FROM LZ_LISTING_ALLOC WHERE LIST_ID IN (SELECT LIST_ID FROM EBAY_LIST_MT E WHERE E.EBAY_ITEM_ID = '$ebay_id')");

            $delete_qry = $this->db->query("DELETE FROM EBAY_LIST_MT E WHERE E.EBAY_ITEM_ID = '$ebay_id'");

            $delete_url = $this->db->query("DELETE FROM LZ_LISTED_ITEM_URL U WHERE U.EBAY_ID =  '$ebay_id'");
        }

        return 1;
    }

    public function holdBarcode()
    {
        $barcode = $this->input->post('barcode');
        $user_id = $this->input->post('user_id');
        $barcodeStatus = 1;
        date_default_timezone_set("America/Chicago");
        $current_date = date("Y-m-d H:i:s");
        $current_date = "TO_DATE('" . $current_date . "', 'YYYY-MM-DD HH24:MI:SS')";
        $comma = ',';

        //foreach($hold_barcode as $barcode){
        $check_status = $this->db->query("SELECT * FROM LZ_BARCODE_MT WHERE BARCODE_NO =$barcode AND ITEM_ADJ_DET_ID_FOR_IN IS NULL AND LIST_ID IS NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND EBAY_ITEM_ID IS NULL");

        if ($check_status->num_rows() > 0) {
            $get_pk = $this->db->query("SELECT get_single_primary_key('LZ_BARCODE_HOLD_LOG','LZ_HOLD_ID') LZ_HOLD_ID FROM DUAL");
            $get_pk = $get_pk->result_array();
            $lz_hold_id = $get_pk[0]['LZ_HOLD_ID'];

            $qry = "INSERT INTO LZ_BARCODE_HOLD_LOG VALUES ($lz_hold_id $comma $barcode $comma $current_date $comma $barcodeStatus $comma $user_id)";
            $this->db->query($qry);

            $hold_qry = "UPDATE LZ_BARCODE_MT SET HOLD_STATUS = $barcodeStatus WHERE BARCODE_NO = $barcode ";
            $hold_status = $this->db->query($hold_qry);
        } else {
            $hold_status = true;
            $barcodeStatus = 2;
        }
        //}//barcode foreach
        if ($hold_status) {
            return array('status' => true, 'message' => "Barcode Successfully Hold");
        } else {
            return array('status' => true, 'message' => "Failed To Hold Barcode");
        }
    }

    public function unHoldBarcode()
    {
        $barcode = $this->input->post('barcode');
        $barcodeStatus = 0;
        $user_id = $this->input->post('user_id');
        date_default_timezone_set("America/Chicago");
        $current_date = date("Y-m-d H:i:s");
        $current_date = "TO_DATE('" . $current_date . "', 'YYYY-MM-DD HH24:MI:SS')";
        $comma = ',';

        //foreach($hold_barcode as $barcode){
        // $check_status = $this->db->query("SELECT * FROM LZ_BARCODE_MT WHERE BARCODE_NO =$barcode AND HOLD_STATUS = 0");

        // if($check_status->num_rows()>0){
        $get_pk = $this->db->query("SELECT get_single_primary_key('LZ_BARCODE_HOLD_LOG','LZ_HOLD_ID') LZ_HOLD_ID FROM DUAL");
        $get_pk = $get_pk->result_array();
        $lz_hold_id = $get_pk[0]['LZ_HOLD_ID'];

        $qry = "INSERT INTO LZ_BARCODE_HOLD_LOG VALUES ($lz_hold_id $comma $barcode $comma $current_date $comma $barcodeStatus $comma $user_id)";
        $this->db->query($qry);

        $hold_qry = "UPDATE LZ_BARCODE_MT SET HOLD_STATUS = $barcodeStatus WHERE BARCODE_NO = $barcode ";
        $hold_status = $this->db->query($hold_qry);

        // }else{
        //     $hold_status = true;
        // }
        //}//barcode foreach
        if ($hold_status) {
            return array('status' => true, 'message' => "Barcode Successfully UN-Hold");
        } else {
            return array('status' => false, 'message' => "Failed To UN-Hold Barcode");
        }
    }

    // /********************************
    //  * end Screen Listed Barcode
    //  *********************************/
    // /********************************
    //  * start Screen merchant Account
    //  *********************************/

    public function merchant_drowp()
    {
        $data = $this->db->query("select d.merchant_id, d.buisness_name from lz_merchant_mt d")->result_array();
        return array('data' => $data, 'status' => true);
    }
    public function merchant_Account_drowp()
    {
        $data = $this->db->query("select f.lz_seller_acct_id, f.sell_acct_desc from lz_seller_accts f where ROWNUM <= 2")->result_array();
        return array('data' => $data, 'status' => true);
    }

    public function save_merchant_acount()
    {
        $mercaht = $this->input->post('MerchantName');
        $merch_id = $mercaht['value'];
        $merchant_name = $mercaht['label'];
        $acountName = $this->input->post('merchant_account_drop');

        $merchacount_id = $acountName[0]['value'];
        $merchantacount_name = $acountName[0]['label'];
        // var_dump($merchantacount_name); exit;
        $insertedBy = $this->input->post('created_by');
        $paypal_address = $this->input->post('paypal_address');
        $paypal_address = trim(str_replace("  ", ' ', $paypal_address));
        $paypal_address = trim(str_replace(array("'"), "''", $paypal_address));
        $token = $this->db->query("SELECT h.SITE_TOKEN from lz_seller_accts h where h.lz_seller_acct_id='$merchacount_id'")->row();
        $token_data = $token->SITE_TOKEN;

        $qry_merchant_dt = $this->db->query("select get_single_primary_key('lj_merhcant_acc_dt','ACCT_ID') ACCT_ID from dual")->row();
        $acct_id = $qry_merchant_dt->ACCT_ID;

        $merchat_dt = $this->db->query("INSERT INTO  lj_merhcant_acc_dt (ACCT_ID, MERCHANT_ID, ACCOUNT_NAME,PORTAL_ID,TOKEN,TOKEN_EXPIRY,INSERTED_DATE,INSERTED_BY,TOKEN_PRIORITY,MERCHANT_STATUS, DEFAULT_MERCHANT)
                values ('$acct_id', '$merch_id', '$merchantacount_name','1','$token_data',null, sysdate, '$insertedBy', null, null, '1')");

        $qry_paypal = $this->db->query("select get_single_primary_key('lj_paypal_mt','PAYPAL_MT_ID') PAYPAL_MT_ID from dual")->row();
        $paypal_id = $qry_paypal->PAYPAL_MT_ID;

        $paypal_mt = $this->db->query("INSERT INTO  lj_paypal_mt (PAYPAL_MT_ID,ACCOUNT_ID,PAYPAL_EMAIL,TOKEN,TOKEN_EXPIRY,INSERTED_DATE,INSERTED_BY)
                values ('$paypal_id', '$acct_id', '$paypal_address','', null, sysdate, '$insertedBy')");
        //var_dump($paypal_mt);exit;

        $qry = $this->db->query("SELECT M.merchant_id,
                  t.buisness_name merchant_name,
                  M.ACCOUNT_NAME,
                  M.DEFAULT_MERCHANT,
                  p.paypal_email,
                  m.inserted_date,
                  p.paypal_mt_id
             from lj_merhcant_acc_dt M, lz_merchant_mt t, lj_paypal_mt p
            where t.merchant_id = M.MERCHANT_ID
              and m.acct_id = p.account_id
              and M.ACCT_ID= '$acct_id'
              and p.PAYPAL_MT_ID='$paypal_id'")->result_array();

        if ($qry) {
            return array('paypal_id' => $paypal_id, 'acct_id' => $acct_id, 'data' => $qry, 'status' => true);
        } else {
            return array('paypal_id' => $paypal_id, 'acct_id' => $acct_id, 'data' => [], 'status' => false);
        }
    }

    public function onSelectMerchatDetail()
    {
        $id = $this->input->post('merchant_id');
        $qry = $this->db->query("SELECT M.merchant_id,
      t.buisness_name merchant_name,
      M.ACCOUNT_NAME,
      M.DEFAULT_MERCHANT,
      p.paypal_email,
      m.inserted_date,
      p.paypal_mt_id
 from lj_merhcant_acc_dt M, lz_merchant_mt t, lj_paypal_mt p
where t.merchant_id = M.MERCHANT_ID
  and m.acct_id = p.account_id
  and m.merchant_id =$id")->result_array();
        if (count($qry) > 0) {

            return array('data' => $qry, 'status' => true);
        } else {
            return array('data' => [], 'status' => true);
        }
    }

    public function update_email_payPal()
    {
        $email_adress = $this->input->post('email_adress');
        $email_adress = trim(str_replace("  ", ' ', $email_adress));
        $email_adress = trim(str_replace(array("'"), "''", $email_adress));
        $email_adres_id = $this->input->post('email_adres_id');
        $qry = $this->db->query("UPDATE lj_paypal_mt set PAYPAL_EMAIL= '$email_adress' where PAYPAL_MT_ID='$email_adres_id'");
        if ($qry) {
            return array('data' => $qry, 'status' => true);
        } else {
            return array('data' => [], 'status' => false);
        }
    }
    public function onSelectAcount()
    {
        $id = $this->input->post('merchant_id');
        $qry = $this->db->query("SELECT * from lz_seller_accts a where a.sell_acct_desc not in (
            select dd.account_name from lj_merhcant_acc_dt dd where dd.merchant_id = $id)
            and a.lz_seller_acct_id in(1,2) ")->result_array();
        if (count($qry) > 0) {

            return array('data' => $qry, 'status' => true);
        } else {
            return array('data' => [], 'status' => true);
        }
    }

    // /********************************
    //  * end Screen merchant Account
    //  *********************************/
    // /********************************
    //  * start Screen BuyerSell
    //  *********************************/

    public function buyerSell()
    {

        $qry = $this->db->query("SELECT *
    FROM (SELECT 'SELL-' || LPAD(NVL(RM.LZW_TRADE_ID, 0), 6, '0') trade_id,RM.remarks,
                 (SELECT OM.OBJECT_NAME
                    FROM LZW_OBJECT_MT OM
                   WHERE OM.OBJECT_ID = RM.OBJECT_ID) PRODUCT_NAME,
                 DECODE((SELECT BMM.DESCRIPTION
                          FROM LZW_BRANDS_DT BT, LZW_BRANDS_MT BMM
                         WHERE BMM.BRAND_ID = BT.BRAND_ID
                           AND TO_CHAR(BT.BRAND_DT_ID) = RM.BRAND_INPUT),
                        NULL,
                        RM.BRAND_INPUT,
                        (SELECT BMM.DESCRIPTION
                           FROM LZW_BRANDS_DT BT, LZW_BRANDS_MT BMM
                          WHERE BMM.BRAND_ID = BT.BRAND_ID
                            AND TO_CHAR(BT.BRAND_DT_ID) = RM.BRAND_INPUT)) BRAND_NAME,

                 DECODE((SELECT SM.DESCRIPTION
                          FROM LZW_SERIES_MT SM, LZW_SERIES_DT ST
                         WHERE SM.SERIES_ID = ST.SERIES_ID
                           AND TO_CHAR(ST.SERIES_DT_ID) = RM.SERIES_INPUT),
                        NULL,
                        RM.SERIES_INPUT,
                        (SELECT SM.DESCRIPTION
                           FROM LZW_SERIES_MT SM, LZW_SERIES_DT ST
                          WHERE SM.SERIES_ID = ST.SERIES_ID
                            AND TO_CHAR(ST.SERIES_DT_ID) = RM.SERIES_INPUT)) SERIES_NAME,
                 DECODE((SELECT MOM.DESCRIPTION
                          FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT
                         WHERE MOM.MODEL_ID = MDT.MODEL_ID
                           AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT),
                        NULL,
                        RM.MODAL_INPUT,
                        (SELECT MOM.DESCRIPTION
                           FROM LZW_MODEL_MT MOM, LZW_MODEL_DT MDT
                          WHERE MOM.MODEL_ID = MDT.MODEL_ID
                            AND TO_CHAR(MDT.MODEL_DT_ID) = RM.MODAL_INPUT)) MODEL_NAME,

                 (select c.carrier_name
                    from Lzw_Carrier c, Lzw_Carrier_dt cd
                   where c.carrier_id = cd.carrier_id
                     and cd.lzw_carrier_dt_id = rm.carrier_input) carrier_name,

                 (select k.storage_desc
                    from lzw_storage_mt k, lzw_storage_dt kd
                   where k.storage_mt_id = kd.storage_mt_id
                     and kd.LZW_STORAGE_DT_ID = rm.storage_input) storage_name,
                 NVL(given_offer, 0) given_offer,

                 CASE
                   WHEN nvl(given_offer, 0) >= 1 then
                    'Approved'
                   else
                    'Pending'
                 end status
            FROM LZW_trade_request RM
           ORDER BY LZW_TRADE_ID DESC)")->result_array();
        if ($qry) {
            return array('data' => $qry, 'status' => true);
        } else {
            return array('data' => [], 'status' => false);
        }
    }
    // /********************************
    //  * end Screen BuyerSell
    //  *********************************/

    // /********************************
    //  * start Screen lzwconfig
    //  *********************************/

    public function get_disable()
    {
        $lzw_storage_dt_id = $this->input->post('id');
        $status = $this->input->post('status');

        if (!empty($lzw_storage_dt_id)) {

            $qry = $this->db->query("UPDATE LZW_STORAGE_DT SD SET SD.ACTIVE ='$status' WHERE SD.LZW_STORAGE_DT_ID = '$lzw_storage_dt_id'");

            $get_car = $this->db->query("SELECT D.LZW_CARRIER_DT_ID FROM LZW_STORAGE_DT D WHERE D.LZW_STORAGE_DT_ID =$lzw_storage_dt_id ")->result_array();
            $lzw_carrier_dt_id = $get_car[0]['LZW_CARRIER_DT_ID'];

            $this->db->query("UPDATE LZW_CARRIER_DT SET ACTIVE ='$status' where LZW_CARRIER_DT_ID = $lzw_carrier_dt_id");

            $get_modl = $this->db->query("SELECT D.MODEL_DT_ID FROM LZW_CARRIER_DT D WHERE D.LZW_CARRIER_DT_ID =$lzw_carrier_dt_id ")->result_array();
            $model_dt_id = $get_modl[0]['MODEL_DT_ID'];

            $this->db->query("UPDATE lzw_model_dt SET ACTIVE ='$status' where model_dt_id = $model_dt_id");

            // $get_ser = $this->db->query("SELECT D.SERIES_DT_ID FROM LZW_MODEL_DT D WHERE D.MODEL_DT_ID =$model_dt_id ")->result_array();
            // $series_dt_id = $get_ser[0]['SERIES_DT_ID'];

            // $this->db->query("UPDATE lzw_series_dt SET ACTIVE ='$status' where SERIES_DT_ID = $series_dt_id" );

            if ($get_modl) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function Save_hot_products()
    {
        $BRAND_ID = $this->input->post('BRAND_ID');
        $OBJECT_ID = $this->input->post('OBJECT_ID');
        $CARRIER_ID = $this->input->post('CARRIER_ID');
        $CARRIER_NAME = $this->input->post('CARRIER_NAME');
        $BRAND_NAME = $this->input->post('BRAND_NAME');
        $LZW_STORAGE_DT_ID = $this->input->post('LZW_STORAGE_DT_ID');
        $MODEL_ID = $this->input->post('MODEL_ID');
        $MODEL_NAME = $this->input->post('MODEL_NAME');
        $PRODUCT_NAME = $this->input->post('PRODUCT_NAME');
        $SALE_PRICE = $this->input->post('SALE_PRICE');
        $SERIES_ID = $this->input->post('SERIES_ID');
        $SERIES_NAME = $this->input->post('SERIES_NAME');
        $STORAGE = $this->input->post('STORAGE');
        $STORAGE_MT_ID = $this->input->post('STORAGE_MT_ID');
        $enter_by = $this->input->post('enter_by');
        $check = $this->db->query("SELECT j.* from lzw_hot_product_mt j where j.brand_name='$BRAND_NAME' and j.model_name='$MODEL_NAME' and j.storage_name='$STORAGE'  and j.carrier_id='$CARRIER_ID'")->result_array();
        if (empty($check)) {
            $qry_hot = $this->db->query("SELECT get_single_primary_key('lzw_hot_product_mt','HOT_PRO_ID') HOT_PRO_ID from dual")->row();
            $hot_id = $qry_hot->HOT_PRO_ID;
            $this->db->query("INSERT INTO  lzw_hot_product_mt (HOT_PRO_ID, PRODUCT_ID, BRAND_ID,BRAND_NAME,SERIES_ID,SERIES_NAME,MODAL_ID,MODEL_NAME,MODEL_LOGO,CARRIER_ID, CARRIER_NAME,CARRIER_LOGO,STORAGE_ID,STORAGE_NAME,OFFER_PRICE,INSERTED_DATE,INSERTED_BY,ACTIVE_YN)
                values ('$hot_id', '$OBJECT_ID','$BRAND_ID', '$BRAND_NAME','$SERIES_ID','$SERIES_NAME','$MODEL_ID', '$MODEL_NAME','', '$CARRIER_ID','$CARRIER_NAME','', '$STORAGE_MT_ID',
                '$STORAGE','$SALE_PRICE',sysdate,'$enter_by', '1')");
            $qry = $this->db->query("SELECT h.hot_pro_id,
           h.product_id,
           h.brand_id,
           h.brand_name,
           h.series_id,
           h.series_name,
           h.modal_id,
           h.model_name,
           h.model_logo,
           h.carrier_id,
           h.carrier_name,
           h.carrier_logo,
           h.storage_id,
           h.storage_name,
           h.offer_price,
           h.inserted_date,
           h.inserted_by,
           h.active_yn from lzw_hot_product_mt h
       where h.HOT_PRO_ID= '$hot_id'")->result_array();

            if ($qry) {
                return array('data' => $qry, 'status' => true);
            } else {
                return array('data' => [], 'status' => false);
            }
        } else {
            return array('data' => [], 'status' => false);
        }
    }
    public function delSeries()
    {
        $SERIES_ID = $this->input->post('id');

        $delSeries = $this->db->query("update lzw_series_mt set active = 1 where SERIES_ID = $SERIES_ID");

        if ($delSeries) {
            return true;
        } else {
            return false;
        }
    }
    public function filter_Acitve_item()
    {
        $filter_data = $this->input->post('filter_data');
        $qry = "SELECT OM.OBJECT_NAME  PRODUCT_NAME,
        BM.DESCRIPTION  BRAND_NAME,
        SM.DESCRIPTION  SERIES_NAME,
        SM.SERIES_ID,
        --md.model_dt_id,
        MO.DESCRIPTION  MODEL_NAME,
        CM.CARRIER_NAME CARRIER_NAME,
        MT.STORAGE_DESC STORAGE,
        dt.SALE_PRICE,
        dt.LZW_STORAGE_DT_ID LZW_STORAGE_DT_ID,
        DT.ACTIVE

   FROM lzw_storage_mt MT,
        lzw_storage_DT DT,
        LZW_CARRIER_DT CD,
        LZW_CARRIER    CM,
        LZW_MODEL_DT   MD,
        LZW_MODEL_MT   MO,
        LZW_SERIES_MT  SM,
        LZW_SERIES_DT  SD,
        LZW_BRANDS_MT  BM,
        LZW_BRANDS_DT  BD,
        LZW_OBJECT_MT  OM

  WHERE MT.STORAGE_MT_ID = DT.STORAGE_MT_ID
    AND DT.LZW_CARRIER_DT_ID = CD.LZW_CARRIER_DT_ID
    AND CD.CARRIER_ID = CM.CARRIER_ID
    AND CD.MODEL_DT_ID = MD.MODEL_DT_ID
    AND MD.MODEL_ID = MO.MODEL_ID
    AND MD.SERIES_DT_ID = SD.SERIES_DT_ID
    AND SD.SERIES_ID = SM.SERIES_ID
    AND SD.BRAND_DT_ID = BD.BRAND_DT_ID
    AND BD.BRAND_ID = BM.BRAND_ID
    AND BD.OBJECT_ID = OM.OBJECT_ID
    and sm.active = 0";
        $check = '';
        if ($filter_data == '1') {
            $check = '1';
            $qry .= "and DT.ACTIVE = $check";
        } else if ($filter_data == '0') {
            $check = '0';
            $qry .= "and DT.ACTIVE = $check";
        } else {
            $qry = " SELECT OM.OBJECT_NAME  PRODUCT_NAME,
            BM.DESCRIPTION  BRAND_NAME,
            SM.DESCRIPTION  SERIES_NAME,
            SM.SERIES_ID,
            --md.model_dt_id,
            MO.DESCRIPTION  MODEL_NAME,
            CM.CARRIER_NAME CARRIER_NAME,
            MT.STORAGE_DESC STORAGE,
            dt.SALE_PRICE,
            dt.LZW_STORAGE_DT_ID LZW_STORAGE_DT_ID,
            DT.ACTIVE

       FROM lzw_storage_mt MT,
            lzw_storage_DT DT,
            LZW_CARRIER_DT CD,
            LZW_CARRIER    CM,
            LZW_MODEL_DT   MD,
            LZW_MODEL_MT   MO,
            LZW_SERIES_MT  SM,
            LZW_SERIES_DT  SD,
            LZW_BRANDS_MT  BM,
            LZW_BRANDS_DT  BD,
            LZW_OBJECT_MT  OM

      WHERE MT.STORAGE_MT_ID = DT.STORAGE_MT_ID
        AND DT.LZW_CARRIER_DT_ID = CD.LZW_CARRIER_DT_ID
        AND CD.CARRIER_ID = CM.CARRIER_ID
        AND CD.MODEL_DT_ID = MD.MODEL_DT_ID
        AND MD.MODEL_ID = MO.MODEL_ID
        AND MD.SERIES_DT_ID = SD.SERIES_DT_ID
        AND SD.SERIES_ID = SM.SERIES_ID
        AND SD.BRAND_DT_ID = BD.BRAND_DT_ID
        AND BD.BRAND_ID = BM.BRAND_ID
        AND BD.OBJECT_ID = OM.OBJECT_ID
         and sm.active = 0 ";
        }

        $qry_final = $this->db->query($qry)->result_array();
        if ($qry_final) {
            return array('data' => $qry_final, 'status' => true);
        } else {
            return array('data' => [], 'status' => false);
        }
    }

    public function save_artcle_post()
    {
        $title = $this->input->post('tilte_editor');
        $title = trim(str_replace("  ", ' ', $title));
        $title = trim(str_replace(array("'"), "''", $title));

        $short_desc = $this->input->post('sort_desc');
        $short_desc = trim(str_replace("  ", ' ', $short_desc));
        $short_desc = trim(str_replace(array("'"), "''", $short_desc));

        $long_desc = $this->input->post('data_editor');
        $Enter_by = $this->input->post('entr_by');
        $tag = $this->input->post('tag');
        $slug = $this->input->post('slug');
        $portal_id = $this->input->post('portal_id');
        $slug = trim(str_replace("  ", ' ', $slug));
        $slug = trim(str_replace(array("'"), "''", $slug));
        $this->form_validation->set_rules('slug', 'slug url required', 'trim|required');
        if ($this->form_validation->run() == false) {
            return array('status' => false, 'message' => "SLug/URL required");
        }

        $image = $_FILES['image'];
        $slug = str_replace(' ', '-', $slug);
        $imageName = $slug . '.png';

        $check = $this->db->query("SELECT slug FROM lzw_article_mt where slug ='$slug'")->row();
        if (count($check) > 0) {
            return array('status' => false, 'message' => "SLug/URL already exist ");
        } else {

            $qry_art_id = $this->db->query("SELECT get_single_primary_key('lzw_article_mt','ART_ID') ART_ID from dual")->row();
            $art_id = $qry_art_id->ART_ID;

            // var_dump($final);
            // exit;

            $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 12");
            $specific_qry = $query->result_array();
            $specific_path = $specific_qry[0]['MASTER_PATH'];

            // $specific_path = "E:/wamp/www/item_pictures/lzw_articles/";
            $main_dir = $specific_path . $art_id;
            //var_dump($main_dir); exit;
            if (is_dir($main_dir) === false) {
                mkdir($main_dir);
            }
            //var_dump($_SERVER['SERVER_NAME']);exit;
            //$temp = 'http://' . $_SERVER['SERVER_NAME'] . $main_dir . $value['BANNER_IMG'];
            $config['upload_path'] = $main_dir;
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] = '100000'; // max_size in kb
            $config['file_name'] = $imageName;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
                return false;
            } else {
                $qry = $this->db->query("INSERT INTO lzw_article_mt(ART_ID,TILTE,SHORT_DESC,CREATED_BY,CREATED_DATE,EXPIRY_DATE,ACTIVE_YN,BANNER_IMG,TAGS,SLUG,PORTAL_ID) values('$art_id','$title','$short_desc','$Enter_by',sysdate, sysdate,'1','$imageName','$tag','$slug','$portal_id')");

                $output = str_split($long_desc, 4000);
                foreach ($output as $value) {
                    $this->db->query("call pro_clob_insert('$value')");
                }
                $this->db->query("UPDATE LZW_ARTICLE_MT M SET M.LONG_DESC = (SELECT CLOB_DATA FROM  TEMP_CLOB) WHERE M.ART_ID = '$art_id'");
                $this->db->query("TRUNCATE TABLE TEMP_CLOB");
                $qry = $this->db->query("SELECT h.art_id,tilte,h.slug, short_desc,(h.long_desc) long_desc, h.created_by,h.created_date,h.portal_id,pr.portal_desc, h.expiry_date,h.active_yn,
        h.banner_img,h.publish_active,h.TAGS,g.first_name from  employee_mt g , lzw_article_mt h, Lj_blog_portals pr where h.created_by= g.employee_id and h.portal_id = pr.portal_id order by h.art_id desc ")->result_array();
                if ($qry) {
                    $images_arr = array();
                    $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 12");
                    $specific_qry = $query->result_array();
                    $specific_path = $specific_qry[0]['MASTER_PATH'];

                    $baseUrl = 'http://' . $_SERVER['SERVER_NAME'];
                    foreach ($qry as $key => $value) {

                        $image = $specific_path . $value['ART_ID'] . '/' . $value['BANNER_IMG'];
                        // $temp = $baseUrl . '/' . $specific_path . $value['ART_ID'] . '/';
                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                        $qry[$key]['BANNER_IMG'] = $baseUrl . '/' . $withoutMasterPartUri;
                        $qry[$key]['BANNER_IMG_NAME'] = $value['BANNER_IMG'];
                        if (!empty($value['LONG_DESC'])) {
                            $qry[$key]['LONG_DESC'] = $this->Read_Clob($value['LONG_DESC']);
                        }
                    }

                    return array('result' => $qry, 'images_arr' => [], 'status' => true);
                } else {
                    return array('result' => [], 'images_arr' => [], 'status' => false);
                }
            }
        }
    }
    public function delete_artcle_post()
    {
        $art_id = $this->input->post('id_cell');
        $quer = $this->db->query("DELETE LZW_ARTICLE_MT WHERE ART_ID= $art_id");
        if ($quer) {
            return array('status' => true, 'message' => "Blog Deleted Successfully");
        } else {
            return array('status' => false, 'message' => "Failed To Delete Blog");

        }

    }
    public function save_artcle_post_as_publish()
    {
        $title = $this->input->post('tilte_editor');
        $title = trim(str_replace("  ", ' ', $title));
        $title = trim(str_replace(array("'"), "''", $title));

        $short_desc = $this->input->post('sort_desc');
        $short_desc = trim(str_replace("  ", ' ', $short_desc));
        $short_desc = trim(str_replace(array("'"), "''", $short_desc));

        $long_desc = $this->input->post('data_editor');
        $Enter_by = $this->input->post('entr_by');
        $tag = $this->input->post('tag');
        $slug = $this->input->post('slug');
        $portal_id = $this->input->post('portal_id');
        $slug = trim(str_replace("  ", ' ', $slug));
        $slug = trim(str_replace(array("'"), "''", $slug));

        $this->form_validation->set_rules('slug', 'slug url required', 'trim|required');
        if ($this->form_validation->run() == false) {
            return array('status' => false, 'message' => "SLug/URL required");
        }
        $image = $_FILES['image'];
        $slug = str_replace(' ', '-', $slug);
        $imageName = $slug . '.png';

        $check = $this->db->query("SELECT slug FROM lzw_article_mt where slug ='$slug'")->row();
        if (count($check) > 0) {
            return array('status' => false, 'message' => "SLug/URL already exist ");
        } else {
            $qry_art_id = $this->db->query("SELECT get_single_primary_key('lzw_article_mt','ART_ID') ART_ID from dual")->row();
            $art_id = $qry_art_id->ART_ID;

            $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 12");
            $specific_qry = $query->result_array();
            $specific_path = $specific_qry[0]['MASTER_PATH'];

            // $specific_path = "E:/wamp/www/item_pictures/lzw_articles/";

            $main_dir = $specific_path . $art_id;
            if (is_dir($main_dir) === false) {
                mkdir($main_dir);
            }

            $config['upload_path'] = $main_dir;
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] = '100000'; // max_size in kb
            $config['file_name'] = $imageName;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
                return false;
            } else {
                $qry = $this->db->query("INSERT INTO lzw_article_mt(ART_ID,TILTE,SHORT_DESC,CREATED_BY,CREATED_DATE,EXPIRY_DATE,ACTIVE_YN,BANNER_IMG,PUBLISH_ACTIVE,TAGS,SLUG,PORTAL_ID) values('$art_id','$title','$short_desc','$Enter_by',sysdate, sysdate,'1','$imageName','1','$tag','$slug','$portal_id')");
                //$loop_count = strlen($long_desc);
                $output = str_split($long_desc, 4000);
                foreach ($output as $value) {
                    $this->db->query("call pro_clob_insert('$value')");
                }
                $this->db->query("UPDATE LZW_ARTICLE_MT M SET M.LONG_DESC = (SELECT CLOB_DATA FROM  TEMP_CLOB) WHERE M.ART_ID = '$art_id'");
                $this->db->query("TRUNCATE TABLE TEMP_CLOB");

                $qry = $this->db->query("SELECT h.art_id,tilte,h.slug, short_desc,(h.long_desc) long_desc, h.created_by,h.created_date,h.portal_id,pr.portal_desc, h.expiry_date,h.active_yn,h.banner_img,h.publish_active,h.TAGS,g.first_name from  employee_mt g , lzw_article_mt h,Lj_blog_portals pr where h.created_by= g.employee_id and h.portal_id = pr.portal_id order by h.art_id desc")->result_array();

                if ($qry) {
                    $images_arr = array();
                    $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 12");
                    $specific_qry = $query->result_array();
                    $specific_path = $specific_qry[0]['MASTER_PATH'];

                    $baseUrl = 'http://' . $_SERVER['SERVER_NAME'];
                    foreach ($qry as $key => $value) {

                        $image = $specific_path . $value['ART_ID'] . '/' . $value['BANNER_IMG'];
                        // $temp = $baseUrl . '/' . $specific_path . $value['ART_ID'] . '/';
                        $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                        $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                        $qry[$key]['BANNER_IMG'] = $baseUrl . '/' . $withoutMasterPartUri;
                        $qry[$key]['BANNER_IMG_NAME'] = $value['BANNER_IMG'];
                        if (!empty($value['LONG_DESC'])) {
                            $qry[$key]['LONG_DESC'] = $this->Read_Clob($value['LONG_DESC']);
                        }
                    }

                    return array('result' => $qry, 'images_arr' => [], 'status' => true);
                } else {
                    return array('result' => [], 'images_arr' => [], 'status' => false);
                }
            }
        }
    }
    public function Read_Clob($long_desc)
    {
        return $long_desc->read($long_desc->size());
    }

    public function get_lzw_article()
    {
        $qry = $this->db->query("SELECT h.art_id,tilte,h.slug, short_desc,(h.long_desc) long_desc,h.portal_id, h.created_by,h.created_date,pr.PORTAL_DESC, h.expiry_date,h.active_yn,
        h.banner_img,h.publish_active,h.TAGS,g.first_name from  employee_mt g , lzw_article_mt h, Lj_blog_portals pr where h.created_by= g.employee_id and h.portal_id = pr.portal_id order by h.art_id desc")->result_array();
        if ($qry) {
            $images_arr = array();
            $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 12");
            $specific_qry = $query->result_array();
            $specific_path = $specific_qry[0]['MASTER_PATH'];

            $baseUrl = 'http://' . $_SERVER['SERVER_NAME'];
            foreach ($qry as $key => $value) {

                $image = $specific_path . $value['ART_ID'] . '/' . $value['BANNER_IMG'];
                // $temp = $baseUrl . '/' . $specific_path . $value['ART_ID'] . '/';
                $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                $qry[$key]['BANNER_IMG'] = $baseUrl . '/' . $withoutMasterPartUri;
                $qry[$key]['BANNER_IMG_NAME'] = $value['BANNER_IMG'];
                if (!empty($value['LONG_DESC'])) {
                    $qry[$key]['LONG_DESC'] = $this->Read_Clob($value['LONG_DESC']);
                }
            }

            return array('result' => $qry, 'images_arr' => [], 'status' => true);
        } else {
            return array('result' => [], 'images_arr' => [], 'status' => false);
        }
    }

    public function get_publish_newsEvent()
    {
        // print_r($this->input->post('data'));
        // exit;
        $art_id = $this->input->post('id');
        $publish = $this->input->post('status');
        // var_dump($art_id, $publish);
        // exit;
        if (!empty($art_id)) {

            $qry = $this->db->query("UPDATE lzw_article_mt sd set sd.PUBLISH_ACTIVE ='$publish' where sd.ART_ID ='$art_id'");
            $qry = $this->db->query("SELECT h.art_id,tilte,h.slug, short_desc,(h.long_desc) long_desc, h.created_by,h.created_date, h.expiry_date,h.active_yn,
        h.banner_img,h.publish_active,h.TAGS,g.first_name from  employee_mt g , lzw_article_mt h where h.created_by= g.employee_id order by h.art_id desc")->result_array();
        }
        if ($qry) {
            $images_arr = array();
            $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 12");
            $specific_qry = $query->result_array();
            $specific_path = $specific_qry[0]['MASTER_PATH'];

            $baseUrl = 'http://' . $_SERVER['SERVER_NAME'];
            foreach ($qry as $key => $value) {

                $image = $specific_path . $value['ART_ID'] . '/' . $value['BANNER_IMG'];
                // $temp = $baseUrl . '/' . $specific_path . $value['ART_ID'] . '/';
                $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                $qry[$key]['BANNER_IMG'] = $baseUrl . '/' . $withoutMasterPartUri;
                $qry[$key]['BANNER_IMG_NAME'] = $value['BANNER_IMG'];
                if (!empty($value['LONG_DESC'])) {
                    $qry[$key]['LONG_DESC'] = $this->Read_Clob($value['LONG_DESC']);
                }
            }

            return array('result' => $qry, 'images_arr' => [], 'status' => true);
        }
        return false;
    }
    public function get_active_newsEvent()
    {
        $id = $this->input->post('id_cell');
        $active = $this->input->post('active');

        if (!empty($id)) {

            $qry = $this->db->query("UPDATE lzw_article_mt sd set sd.ACTIVE_YN ='$active' where sd.ART_ID ='$id'");
            $qry = $this->db->query("SELECT h.art_id,tilte,h.slug, short_desc,(h.long_desc) long_desc, h.created_by,h.created_date, h.expiry_date,h.active_yn,
        h.banner_img,h.publish_active,h.TAGS,g.first_name from  employee_mt g , lzw_article_mt h where h.created_by= g.employee_id order by h.art_id desc")->result_array();
        }
        if ($qry) {
            $images_arr = array();
            $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 12");
            $specific_qry = $query->result_array();
            $specific_path = $specific_qry[0]['MASTER_PATH'];

            $baseUrl = 'http://' . $_SERVER['SERVER_NAME'];
            foreach ($qry as $key => $value) {

                $image = $specific_path . $value['ART_ID'] . '/' . $value['BANNER_IMG'];
                // $temp = $baseUrl . '/' . $specific_path . $value['ART_ID'] . '/';
                $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                $withoutMasterPartUri = preg_replace("/[\r\n]*/", "", $withoutMasterPartUri);
                $qry[$key]['BANNER_IMG'] = $baseUrl . '/' . $withoutMasterPartUri;
                $qry[$key]['BANNER_IMG_NAME'] = $value['BANNER_IMG'];
                if (!empty($value['LONG_DESC'])) {
                    $qry[$key]['LONG_DESC'] = $this->Read_Clob($value['LONG_DESC']);
                }
            }

            return array('result' => $qry, 'images_arr' => [], 'status' => true);
        }
        return false;
    }
    public function get_portal_info()
    {
        $qry = $this->db->query("SELECT * from Lj_blog_portals")->result_array();
        if (count($qry) > 0) {
            return array('result' => $qry, 'status' => true);
        } else {
            return array('result' => array(), 'status' => false);
        }
    }
    public function update_newsEvent()
    {
        $id = $this->input->post('id');
        $tilte_editor_update = $this->input->post('tilte_editor_update');
        $tilte_editor_update = trim(str_replace("  ", ' ', $tilte_editor_update));
        $tilte_editor_update = trim(str_replace(array("'"), "''", $tilte_editor_update));
        $data_editor_update = $this->input->post('data_editor_update');
        $sort_desc_update = $this->input->post('sort_desc_update');
        $sort_desc_update = trim(str_replace("  ", ' ', $sort_desc_update));
        $sort_desc_update = trim(str_replace(array("'"), "''", $sort_desc_update));
        $tag_update = $this->input->post('tag_update');
        $portal_id = $this->input->post('portal_id');
        $slug = $this->input->post('slug');
        $slug = trim(str_replace("  ", ' ', $slug));
        $slug = trim(str_replace(array("'"), "''", $slug));
        $this->form_validation->set_rules('slug', 'slug url required', 'trim|required');
        if ($this->form_validation->run() == false) {
            return array('status' => false, 'message' => "SLug/URL required");
        }
        $slug = str_replace(' ', '-', $slug);
        $image_name_temp = $slug . ".png";
        $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 12");
        $specific_qry = $query->result_array();
        $specific_path = $specific_qry[0]['MASTER_PATH'];
        // $specific_path = "E:/wamp/www/item_pictures/lzw_articles/";
        $image_name = $this->db->query("SELECT BANNER_IMG FROM lzw_article_mt where art_id = $id")->row();
        $main_dir = $specific_path . $id;
        if (isset($_FILES['image_update'])) {
            $check = $this->db->query("SELECT slug FROM lzw_article_mt where slug ='$slug' and art_id != $id")->row();
            if (count($check) > 0) {

                return array('status' => 'false', 'message' => "SLug/URL already exist ");
            } else {

                if (is_dir($main_dir) === false) {
                    mkdir($main_dir);
                }
                unlink($main_dir . "/" . $image_name->BANNER_IMG);
                $image = $_FILES['image_update'];
                $imageName = $_FILES['image_update']['name'];
                $config['upload_path'] = $main_dir;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['max_size'] = '100000'; // max_size in kb
                $config['file_name'] = $slug . ".png";
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image_update')) {
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                    return false;
                } else {

                    $qry = $this->db->query("UPDATE lzw_article_mt sd set sd.TILTE ='$tilte_editor_update', sd.SHORT_DESC='$sort_desc_update',sd.BANNER_IMG='$image_name_temp', sd.TAGS='$tag_update', sd.portal_id = '$portal_id' where sd.ART_ID ='$id'");
                    $output = str_split($data_editor_update, 4000);
                    foreach ($output as $value) {
                        $this->db->query("call pro_clob_insert('$value')");
                    }
                    $this->db->query("UPDATE LZW_ARTICLE_MT M SET M.LONG_DESC = (SELECT CLOB_DATA FROM  TEMP_CLOB) WHERE M.ART_ID = '$id'");
                    $this->db->query("TRUNCATE TABLE TEMP_CLOB");
                    return array('status' => 'true');

                    return array('status' => 'true');

                }
            }
        } else {

            $check = $this->db->query("SELECT slug FROM lzw_article_mt where slug ='$slug' and art_id != $id")->row();

            if (count($check) > 0) {
                return array('status' => 'false', 'message' => "SLug/URL already exist ");
            } else {

                rename($main_dir . "/" . $image_name->BANNER_IMG, $main_dir . "/" . $slug . ".png");

                $qry = $this->db->query("UPDATE lzw_article_mt sd set sd.TILTE ='$tilte_editor_update', sd.SHORT_DESC='$sort_desc_update',sd.BANNER_IMG='$image_name_temp', sd.TAGS='$tag_update',sd.SLUG = '$slug', sd.portal_id = '$portal_id' where sd.ART_ID ='$id'");
                $output = str_split($data_editor_update, 4000);
                foreach ($output as $value) {
                    $this->db->query("call pro_clob_insert('$value')");
                }
                $this->db->query("UPDATE LZW_ARTICLE_MT M SET M.LONG_DESC = (SELECT CLOB_DATA FROM  TEMP_CLOB) WHERE M.ART_ID = '$id'");
                $this->db->query("TRUNCATE TABLE TEMP_CLOB");
                return array('status' => 'true');

            }
        }
    }

    public function download_Specific_dropdown()
    {
        $qry = $this->db->query("SELECT t.category_id,t.category_id ||' | ' || t.category_name category_name from lz_bd_category_tree t where t.parent_cat_id is not null")->result_array();
        if ($qry) {
            return array('data' => $qry, 'status' => true);
        } else {
            return array('data' => $qry, 'status' => true);
        }
    }

    // /********************************
    //  * end Screen lzwconfig
    // /********************************
    // /********************************
    //  * end Screen recycle
    // /********************************
    public function recycle()
    {
        $qry = $this->db->query("SELECT 'rec-' || LPAD(NVL(r.rec_id, 0), 6, '0') rec_id,
        r.rec_first_name name,
        r.rec_email   email,
        r.rec_phone   phone,
        r.rec_remarks remarks
        from lzw_rec_pickup r")->result_array();
        if ($qry) {
            return array('data' => $qry, 'status' => true);
        } else {
            return array('data' => $qry, 'status' => true);
        }
    }
    public function convert_text($text)
    {

        $t = $text;
        // '\'' => '%27',
        // '~' => '%7E',
        // '_' => '%5F',
        // '/' => '%2F',
        // '\\' => '%5C',
        // '.' => '%2E',
        // '%' => '%25',
        // ':' => '%3A',
        // ' ' => '%20',
        $specChars = array(
            '!' => '%21', '"' => '%22',
            '#' => '%23', '$' => '',
            '&' => '%26', '(' => '%28',
            ')' => '%29', '*' => '%2A', '+' => '%2B',
            ',' => '%2C', '-' => '%2D',
            ';' => '%3B',
            '<' => '%3C', '=' => '%3D', '>' => '%3E',
            '?' => '%3F', '@' => '%40', '[' => '%5B',
            ']' => '%5D', '^' => '%5E',
            '`' => '%60', '{' => '%7B',
            '|' => '%7C', '}' => '%7D',
            ',' => '%E2%80%9A',
        );

        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }

        return $t;
    }
    public function SaveImage_detect()
    {
        $data = $_FILES['image'];
        $query = $this->db->query("SELECT c.MASTER_PATH from lz_pict_path_config c where c.path_id = 13");
        $specific_qry = $query->result_array();
        $specific_path = $specific_qry[0]['MASTER_PATH'];
        // var_dump($query); exit;

        $main_dir = $specific_path;
        if (is_dir($main_dir) === false) {
            mkdir($main_dir);
        }
        $config['upload_path'] = $main_dir;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 100000;
        $config['file_name'] = $this->convert_text($_FILES['image']['name']);
        $file_name = $this->convert_text($_FILES['image']['name']);
        $this->load->library('upload', $config);
        // var_dump($config);
        // var_dump($file_name);
        // var_dump($this->upload->do_upload('image'));
        if (!$this->upload->do_upload('image')) {
            $error = array(
                'error' => $this->upload->display_errors(),
            );
            echo json_encode(array(
                "data" => $error,

            ));
            return json_encode(array(
                "data" => $error,

            ));
        } else {
            $url = $main_dir . $file_name;
            $url2 = "item_pictures/detect_image/" . $file_name;
            $data = array(
                'url' => $url,
                'url2' => $url2,

            );

            return $data;
            // echo json_encode(array("data_img" => $url));
            //  return json_encode(array("data_img" => $url));
        }
    }
    // /********************************
    //  * end Screen recycle
    // /********************************

    // /********************************
    //  * start Screen search item barcode
    //  *********************************/

    public function search_item_barcode()
    {
        $id = $this->input->post('item');
        //var_dump($id); exit;
        $qry = $this->db->query("SELECT B.BARCODE_NO,
        TO_CHAR(l.LIST_DATE, 'DD-MM-YYYY HH24:MI:SS') AS LIST_DATE,
        l.LIST_ID,
        l.STATUS,
        l.LIST_PRICE,
        e.user_name lister_name,
        S.SEED_ID,
        M.SINGLE_ENTRY_ID,
        M.LZ_MANIFEST_ID,
        M.LOADING_NO,
        M.LOADING_DATE,
        M.PURCH_REF_NO,
        B.ITEM_ID,
        d.laptop_item_code,
        S.ITEM_TITLE ITEM_MT_DESC,
        s.f_manufacture MANUFACTURER,
        l.list_qty,
        S.F_MPN MFG_PART_NO,
        S.F_UPC UPC,
        BCD.EBAY_ITEM_ID,
        BCD.CONDITION_ID ITEM_CONDITION,
        BCD.QTY QUANTITY,
        BCD.COST_PRICE,
        C.COND_NAME,
        R.BUISNESS_NAME
   from lz_barcode_mt b,
        ebay_list_mt l,
        employee_mt e,
        lz_manifest_mt m,
        lz_manifest_det d,
        items_mt i,
        lz_item_seed s,
        LZ_ITEM_COND_MT c,
        lj_merhcant_acc_dt a,
        lz_merchant_mt r,
        (SELECT BC.EBAY_ITEM_ID,
                BC.LZ_MANIFEST_ID,
                BC.ITEM_ID,
                BC.CONDITION_ID,
                COUNT(1) QTY,
                max(dt.po_detail_retial_price) COST_PRICE
           FROM LZ_BARCODE_MT BC,
                items_mt i,
                lz_manifest_det dt,
                (select item_id, lz_manifest_id, condition_id
                   from lz_barcode_mt
                  where barcode_no = 249352) bb
          WHERE BC.Item_Id = bb.item_id
            and bc.lz_manifest_id = bb.lz_manifest_id
            and bc.condition_id = bb.condition_id
            and dt.lz_manifest_id = bc.lz_manifest_id
            and dt.laptop_item_code = i.item_code
            and dt.conditions_seg5 = bc.condition_id
            and i.item_id = bc.item_id
          GROUP BY BC.LZ_MANIFEST_ID,
                   BC.ITEM_ID,
                   BC.CONDITION_ID,
                   BC.EBAY_ITEM_ID) bcd
  where b.list_id = l.list_id
    and e.employee_id = l.lister_id
    and m.lz_manifest_id = b.lz_manifest_id
    and d.laptop_item_code = i.item_code
    and d.lz_manifest_id = m.lz_manifest_id
    and b.item_id = i.item_id
    and s.item_id = b.item_id
    and s.lz_manifest_id = b.lz_manifest_id
    and s.default_cond = b.condition_id
    and b.condition_id = c.id
    AND R.MERCHANT_ID = A.MERCHANT_ID
    AND l.LZ_SELLER_ACCT_ID = A.ACCT_ID
    and bcd.item_id = b.item_id
    and bcd.lz_manifest_id = b.lz_manifest_id
    and b.barcode_no =$id")->result_array();

        return array('data' => $qry, 'status' => true);
    }

    public function get_order_item()
    {
        $qry = $this->db->query("SELECT FILTER_ID ,FILTER_DESC from lj_search_filter where FILTER_ID in(1,2)")->result_array();

        return array('data' => $qry, 'status' => true);
    }

    // /********************************
    //  * end Screen search item barcode
    //  *********************************/

    // /********************************
    //  * start Screen search item spcifics
    //  *********************************/

    public function get_barcode_data()
    {
        $perameter = $this->input->post('barcode');
        $item_qry = $this->db->query("select m.item_id,m.lz_manifest_id,m.condition_id from lz_barcode_mt m where m.barcode_no='$perameter'");

        $item_data = $item_qry->result_array();
        if (!empty($item_data)) {
            // var_dump($item_qry); exit;
            // var_dump($item_data); exit;

            if ($item_qry->num_rows() > 0) {
                $lz_manifest_id = $item_data[0]['LZ_MANIFEST_ID'];
                $item_id = $item_data[0]['ITEM_ID'];
                $condition_id = $item_data[0]['CONDITION_ID'];
       //          $item_det = $this->db->query("SELECT B.UNIT_NO, B.BARCODE_NO IT_BARCODE, D.CONDITIONS_SEG5     ITEM_CONDITION, D.ITEM_MT_DESC, I.ITEM_MT_MANUFACTURE MANUFACTURER, I.ITEM_MT_MFG_PART_NO MFG_PART_NO, D.ITEM_MT_UPC         UPC, D.AVAILABLE_QTY       AVAIL_QTY, B.ITEM_ID, B.LZ_MANIFEST_ID, M.PURCH_REF_NO,  B.CONDITION_ID FROM LZ_BARCODE_MT B, LZ_MANIFEST_DET D, ITEMS_MT I, LZ_MANIFEST_MT M,LZ_ITEM_COND_MT C WHERE B.ITEM_ID = I.ITEM_ID AND B.LZ_MANIFEST_ID = D.LZ_MANIFEST_ID AND D.LAPTOP_ITEM_CODE = I.ITEM_CODE AND M.LZ_MANIFEST_ID = D.LZ_MANIFEST_ID
       // AND  UPPER(TRIM(D.CONDITIONS_SEG5)) =UPPER(C.COND_NAME(+)) AND B.LZ_MANIFEST_ID=" . $item_data[0]['LZ_MANIFEST_ID'] . " AND B.ITEM_ID = " . $item_data[0]['ITEM_ID'] . " ORDER BY B.UNIT_NO");
                $item_det = $this->db->query("SELECT B.UNIT_NO,
       B.BARCODE_NO          IT_BARCODE,
       S.DEFAULT_COND     ITEM_CONDITION,
       S.ITEM_TITLE ITEM_MT_DESC,
       S.F_MANUFACTURE MANUFACTURER,
       S.F_MPN MFG_PART_NO,
       S.F_UPC         UPC,
       '1'       AVAIL_QTY,
       B.ITEM_ID,
       B.LZ_MANIFEST_ID,
       '' PURCH_REF_NO,
       B.CONDITION_ID
  FROM LZ_BARCODE_MT   B,
       LZ_ITEM_SEED S
 WHERE B.ITEM_ID = S.ITEM_ID
   AND B.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID
   AND B.CONDITION_ID = S.DEFAULT_COND
   AND B.LZ_MANIFEST_ID = '$lz_manifest_id'
   AND B.ITEM_ID = '$item_id'
   AND B.CONDITION_ID = '$condition_id'
 ORDER BY B.UNIT_NO");
                $item_det = $item_det->result_array();
            }

            // $cat_id = $this->db->query("SELECT DISTINCT DT.E_BAY_CATA_ID_SEG6,DT.BRAND_SEG3 FROM LZ_MANIFEST_DET DT WHERE DT.LAPTOP_ITEM_CODE = (SELECT V.LAPTOP_ITEM_CODE FROM VIEW_LZ_LISTING_REVISED V WHERE V.LZ_MANIFEST_ID = ".$item_det[0]['LZ_MANIFEST_ID']." AND V.ITEM_ID = ".$item_det[0]['ITEM_ID']." AND ROWNUM = 1) AND DT.E_BAY_CATA_ID_SEG6 NOT IN ('N/A', 'Other', 'OTHER', 'other')");

            if (!empty($item_data[0]['CONDITION_ID'])) {
                $condition_id = $item_data[0]['CONDITION_ID'];
            } else {
                $condition_id = $item_det[0]['CONDITION_ID'];
            }
            $cat_id = $this->db->query("SELECT S.CATEGORY_ID E_BAY_CATA_ID_SEG6, S.CATEGORY_NAME BRAND_SEG3 FROM LZ_MANIFEST_DET DT, LZ_ITEM_SEED S , ITEMS_MT I WHERE DT.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID AND DT.LAPTOP_ITEM_CODE = I.ITEM_CODE AND I.ITEM_ID = S.ITEM_ID AND S.ITEM_ID = " . $item_data[0]['ITEM_ID'] . " AND S.LZ_MANIFEST_ID = " . $item_data[0]['LZ_MANIFEST_ID'] . " AND S.DEFAULT_COND = " . $condition_id);

            $cat_id = $cat_id->result_array();
            //var_dump($cat_id);exit;

            if (!empty($cat_id[0]['E_BAY_CATA_ID_SEG6'])) {

                //$mt_id = $this->db->query("SELECT * FROM CATEGORY_SPECIFIC_MT T WHERE T.EBAY_CATEGORY_ID = (SELECT DISTINCT DT.E_BAY_CATA_ID_SEG6 FROM LZ_MANIFEST_DET DT WHERE DT.LAPTOP_ITEM_CODE = (SELECT V.LAPTOP_ITEM_CODE FROM VIEW_LZ_LISTING_REVISED V WHERE V.LZ_MANIFEST_ID = ".$item_det[0]['LZ_MANIFEST_ID']." AND V.ITEM_ID = ".$item_det[0]['ITEM_ID']." AND ROWNUM = 1) AND DT.E_BAY_CATA_ID_SEG6 NOT IN ('N/A', 'Other', 'OTHER', 'other') ) ORDER BY T.SPECIFIC_NAME");
                $mt_id = $this->db->query("SELECT * FROM CATEGORY_SPECIFIC_MT T WHERE T.EBAY_CATEGORY_ID = " . $cat_id[0]['E_BAY_CATA_ID_SEG6'] . " ORDER BY T.SPECIFIC_NAME");
                $mt_id = $mt_id->result_array();

                if (!empty($mt_id[0]['EBAY_CATEGORY_ID'])) {
                    // $specs_qry = $this->db->query("SELECT Q1.EBAY_CATEGORY_ID, Q1.SPECIFIC_NAME, DET.SPECIFIC_VALUE, Q1.MAX_VALUE, Q1.MIN_VALUE, Q1.SELECTION_MODE,Q1.MT_ID FROM (SELECT * FROM CATEGORY_SPECIFIC_MT MT WHERE MT.EBAY_CATEGORY_ID = (SELECT DISTINCT  DT.E_BAY_CATA_ID_SEG6 FROM LZ_MANIFEST_DET DT WHERE DT.LAPTOP_ITEM_CODE = (SELECT V.LAPTOP_ITEM_CODE FROM VIEW_LZ_LISTING_REVISED V WHERE V.LZ_MANIFEST_ID = ".$item_det[0]['LZ_MANIFEST_ID']." AND V.ITEM_ID = ".$item_det[0]['ITEM_ID']." AND ROWNUM = 1) AND DT.E_BAY_CATA_ID_SEG6 NOT IN ('N/A', 'Other', 'OTHER', 'other' ) AND ROWNUM = 1 )) Q1, CATEGORY_SPECIFIC_DET DET WHERE Q1.MT_ID = DET.MT_ID ORDER BY Q1.SPECIFIC_NAME");
                    $mt_id1 = $mt_id[0]['EBAY_CATEGORY_ID'];
                    $specs_qry = $this->db->query("SELECT Q1.EBAY_CATEGORY_ID,
            Q1.SPECIFIC_NAME,
            DET.SPECIFIC_VALUE,
            Q1.MAX_VALUE,
            Q1.MIN_VALUE,
            Q1.SELECTION_MODE,
            Q1.MT_ID
       FROM CATEGORY_SPECIFIC_DET DET, CATEGORY_SPECIFIC_MT Q1
      WHERE Q1.MT_ID = DET.MT_ID
        and q1.ebay_category_id ='$mt_id1'
      ORDER BY Q1.Mt_Id")->result_array();

                    $lables = $this->db->query("SELECT DISTINCT cst.specific_name ,cst.max_value, cst.mt_id from category_specific_det csd, category_specific_mt cst WHERE cst.mt_id = csd.mt_id and cst.ebay_category_id ='$mt_id1'")->result_array();
                    //var_dump($lables); exit;
                    $specs_value = "SELECT MT.SPECIFICS_NAME, DT.SPECIFICS_VALUE,CM.MT_ID FROM LZ_ITEM_SPECIFICS_MT MT, LZ_ITEM_SPECIFICS_DET DT,category_specific_mt cm WHERE MT.SPECIFICS_MT_ID = DT.SPECIFICS_MT_ID and upper(cm.specific_name) = upper(mt.specifics_name) and cm.ebay_category_id = mt.category_id AND MT.ITEM_ID = " . $item_det[0]['ITEM_ID'] . "AND MT.CATEGORY_ID = " . $mt_id[0]['EBAY_CATEGORY_ID'] . "  ORDER BY cm.Mt_Id";
                    $specs_value = $this->db->query($specs_value);
                    $specs_value = $specs_value->result_array();

                    foreach ($specs_qry as $key1 => $value1) {
                        foreach ($specs_value as $key => $value) {
                            if ($value['SPECIFICS_NAME'] === $value1['SPECIFIC_NAME'] and $value['SPECIFICS_VALUE'] === $value1['SPECIFIC_VALUE'] and $value['MT_ID'] === $value1['MT_ID']) {
                                // var_dump($value['SPECIFICS_VALUE']);
                                // var_dump($key1);
                                $specs_qry[$key1]['SELECTEDCHECK'] = "true";
                                break;
                            } else {
                                $specs_qry[$key1]['SELECTEDCHECK'] = "false";
                            }
                        }
                    }
                    // echo "<pre>";
                    // print_r($specs_qry);
                    // exit();
                    if (empty($specs_value)) {
                        return array('specs_qry' => $specs_qry, 'specs_v' => 1, 'mt_id' => $mt_id, 'item_det' => $item_det, 'cat_id' => $cat_id, 'specs_value' => $specs_value, 'item' => $lables, 'status' => true);
                    } else {
                        return array('specs_qry' => $specs_qry, 'mt_id' => $mt_id, 'item_det' => $item_det, 'cat_id' => $cat_id, 'specs_value' => $specs_value, 'item' => $lables, 'status' => true);
                    }
                } else {
                    return array('specs_qry' => [], 'mt_id' => [], 'cat_id' => [], 'specs_value' => [], 'item' => [], 'error_msg' => true, 'item_det' => $item_det, 'status' => false);
                }
            } else { //category id if else
                return array('specs_qry' => [], 'mt_id' => [], 'cat_id' => [], 'specs_value' => [], 'item' => [], 'error_msg' => true, 'item_det' => $item_det, 'status' => false);
            }
        } else {
            return array('status' => false);
        }
    }


    public function save_drop_value()
    {
        $dynamic = $this->input->post('dynamic');

        foreach ($dynamic as $key => $val) {
            $mt_id = $val['value'];
            $custom_attribute = $val['label'];
            $custom_attribute = ucfirst($custom_attribute);
            $custom_attribute = trim(str_replace("  ", ' ', $custom_attribute));
            $custom_attribute = trim(str_replace(array("'"), "''", $custom_attribute));

            $comma = ',';
            $query = $this->db->query("SELECT SPECIFIC_VALUE FROM CATEGORY_SPECIFIC_DET D WHERE D.MT_ID = $mt_id AND upper(SPECIFIC_VALUE) = upper('$custom_attribute')");
            if ($query->num_rows() > 0) {
                return false;
            } else {

                $get_det_pk = $this->db->query("SELECT get_single_primary_key('CATEGORY_SPECIFIC_DET','DET_ID') SPECIFICS_DET_ID FROM DUAL");
                $get_det_pk = $get_det_pk->result_array();
                $specifics_det_id = $get_det_pk[0]['SPECIFICS_DET_ID'];

                $ins_det_qry = "INSERT INTO CATEGORY_SPECIFIC_DET VALUES ($specifics_det_id $comma $mt_id $comma '$custom_attribute')";
                $ins_det_qry = $this->db->query($ins_det_qry);
                return array('data' => $ins_det_qry, 'status' => true);
            }
        }
    }

    public function add_specifics()
    {
        $item_id = $this->input->post('item_id');
        $cat_id = $this->input->post('cat_id');
        $spec_name = $this->input->post('spec_name');
        $spec_value = $this->input->post('spec_value');
        $item_upc =trim($this->input->post('item_upc'));
        $item_mpn = trim($this->input->post('item_mpn'));
        $user_id = $this->input->post('user_id');
        date_default_timezone_set("America/Chicago");
        $current_date = date("Y-m-d H:i:s");
        $current_date = "TO_DATE('" . $current_date . "', 'YYYY-MM-DD HH24:MI:SS')";
        $comma = ',';
        if (!empty($item_upc)) {
                $where_upc = " AND S.UPC = '" . $item_upc . "'";
            } else {
                //$where_upc = ' ';
                $where_upc = " AND S.UPC IS NULL";
            }
            if (!empty($item_mpn)) {
                $where_mpn = " AND UPPER(S.MPN) = UPPER('" . $item_mpn . "')";
            } else {
                //$where_mpn = '';
                $where_mpn = " AND S.MPN IS NULL";
            }

        //$qry_det = $this->db->query("DELETE FROM LZ_ITEM_SPECIFICS_DET D WHERE D.SPECIFICS_MT_ID IN (SELECT S.SPECIFICS_MT_ID FROM LZ_ITEM_SPECIFICS_MT S WHERE S.CATEGORY_ID = $cat_id AND S.ITEM_ID = $item_id )");
        $qry_det = $this->db->query("DELETE FROM LZ_ITEM_SPECIFICS_DET D WHERE D.SPECIFICS_MT_ID IN (SELECT S.SPECIFICS_MT_ID FROM LZ_ITEM_SPECIFICS_MT S WHERE S.CATEGORY_ID = $cat_id $where_mpn $where_upc)");

        //$qry_mt = $this->db->query("DELETE FROM  LZ_ITEM_SPECIFICS_MT S WHERE S.CATEGORY_ID =  $cat_id AND S.ITEM_ID = $item_id");
        $qry_mt = $this->db->query("DELETE FROM  LZ_ITEM_SPECIFICS_MT S WHERE S.CATEGORY_ID =  $cat_id AND UPPER(S.MPN) = UPPER('$item_mpn') AND S.UPC = '$item_upc'");

        foreach ($spec_name as $key => $name) {
            $name = trim(str_replace("  ", ' ', $name));
            $name = trim(str_replace(array("'"), "''", $name));

            if ($spec_value[$key] != '------------Select------------' && !empty($spec_value[$key])) {
                $get_mt_pk = $this->db->query("SELECT get_single_primary_key('LZ_ITEM_SPECIFICS_MT','SPECIFICS_MT_ID') SPECIFICS_MT_ID FROM DUAL");
                $get_pk = $get_mt_pk->result_array();
                $specifics_mt_id = $get_pk[0]['SPECIFICS_MT_ID'];
                $ins_mt_qty = "INSERT INTO LZ_ITEM_SPECIFICS_MT VALUES ($specifics_mt_id $comma $item_id $comma $cat_id $comma $user_id $comma $current_date $comma '$name' $comma '$item_mpn' $comma '$item_upc')";
                $ins_mt_qty = $this->db->query($ins_mt_qty);
                // var_dump($spec_value); exit;
                if (strpos($spec_value[$key], ',')) {
                    $valuesIds = explode(',', $spec_value[$key]);
                    foreach ($valuesIds as $key1 => $value1) {
                        $specific_value = trim(str_replace("  ", ' ', $value1));
                        $specific_value = trim(str_replace(array("'"), "''", $specific_value));

                        if (!empty($specific_value)) {
                            $get_det_pk = $this->db->query("SELECT get_single_primary_key('LZ_ITEM_SPECIFICS_DET','SPECIFICS_DET_ID') SPECIFICS_DET_ID FROM DUAL");
                            $get_det_pk = $get_det_pk->result_array();
                            $specifics_det_id = $get_det_pk[0]['SPECIFICS_DET_ID'];
                            $ins_det_qty = "INSERT INTO LZ_ITEM_SPECIFICS_DET VALUES ($specifics_det_id $comma $specifics_mt_id $comma '$specific_value')";
                            $ins_det_qty = $this->db->query($ins_det_qty);
                        }
                    }
                } else {
                    $specific_value = trim(str_replace("  ", ' ', $spec_value[$key]));
                    $specific_value = trim(str_replace(array("'"), "''", $specific_value));
                    $get_det_pk = $this->db->query("SELECT get_single_primary_key('LZ_ITEM_SPECIFICS_DET','SPECIFICS_DET_ID') SPECIFICS_DET_ID FROM DUAL");
                    $get_det_pk = $get_det_pk->result_array();
                    $specifics_det_id = $get_det_pk[0]['SPECIFICS_DET_ID'];
                    $ins_det_qty = "INSERT INTO LZ_ITEM_SPECIFICS_DET VALUES ($specifics_det_id $comma $specifics_mt_id $comma '$specific_value')";
                    $ins_det_qty = $this->db->query($ins_det_qty);
                }
            }
        }
        return true;
        //  }
    }

    public function get_spcifics()
    {
        $item_id = $this->input->post('Item_ID');
        $cat_id = $this->input->post('Category_ID');
        $qry = $this->db->query("SELECT MT.SPECIFICS_NAME, DT.SPECIFICS_VALUE, CM.MT_ID
        FROM LZ_ITEM_SPECIFICS_MT  MT,
             LZ_ITEM_SPECIFICS_DET DT,
             category_specific_mt  cm
       WHERE MT.SPECIFICS_MT_ID = DT.SPECIFICS_MT_ID
         and upper(cm.specific_name) = upper(mt.specifics_name)
         and cm.ebay_category_id = mt.category_id
         AND MT.ITEM_ID = $item_id
         AND MT.CATEGORY_ID =$cat_id
       ORDER BY MT.SPECIFICS_NAME")->result_array();
        // return $qry;
        return array('result' => $qry, 'status' => true);
    }

    public function update_cat_id()
    {
        $it_barcode = $this->input->post('barcode');
        $category_id = $this->input->post('CategoryId_sugest');
        $main_category = $this->input->post('MainCategory_suggest');
        $main_category = trim(str_replace("  ", ' ', $main_category));
        $main_category = trim(str_replace(array("'"), "''", $main_category));
        $sub_cat = $this->input->post('SubCategory_suggest');
        $sub_cat = trim(str_replace("  ", ' ', $sub_cat));
        $sub_cat = trim(str_replace(array("'"), "''", $sub_cat));
        $category_name = $this->input->post('Category_Name_suggest');
        $category_name = trim(str_replace("  ", ' ', $category_name));
        $category_name = trim(str_replace(array("'"), "''", $category_name));

        $item_qry = $this->db->query("select m.item_id,m.lz_manifest_id , m.condition_id from lz_barcode_mt m where m.barcode_no=$it_barcode");
        $item_det = $item_qry->result_array();
        //B.LZ_MANIFEST_ID=".$item_det[0]['LZ_MANIFEST_ID']." AND B.ITEM_ID = ".$item_det[0]['ITEM_ID']";

        $cat_id = $this->db->query("UPDATE LZ_MANIFEST_DET SET MAIN_CATAGORY_SEG1='$main_category' ,SUB_CATAGORY_SEG2='$sub_cat',E_BAY_CATA_ID_SEG6= '$category_id',BRAND_SEG3 = '$category_name' WHERE LAPTOP_ITEM_CODE = (SELECT I.ITEM_CODE FROM ITEMS_MT I WHERE I.ITEM_ID=" . $item_det[0]['ITEM_ID'] . ")");

        $this->db->query("UPDATE LZ_ITEM_SEED SS SET SS.CATEGORY_ID = '$category_id', SS.CATEGORY_NAME = '$category_name' WHERE SS.ITEM_ID = " . $item_det[0]['ITEM_ID'] . " AND SS.LZ_MANIFEST_ID = " . $item_det[0]['LZ_MANIFEST_ID'] . " AND SS.DEFAULT_COND = " . $item_det[0]['CONDITION_ID']);

        return $cat_id;
    }

    public function attribute_value()
    {
        $cat_id = $this->input->post('Category_ID');
        $spec_name = $this->input->post('Attribute_name');
        //$spec_name = trim(str_replace("  ", ' ', $spec_name));
        $spec_name = trim(str_replace(array("'"), "''", $spec_name));
        //var_dump($spec_name);exit;
        $custom_attribute = ucfirst($this->input->post('YourOwn'));
        $custom_attribute = trim(str_replace("  ", ' ', $custom_attribute));
        $custom_attribute = trim(str_replace(array("'"), "''", $custom_attribute));

        $comma = ',';
        $query = $this->db->query("SELECT SPECIFIC_VALUE FROM CATEGORY_SPECIFIC_DET D WHERE D.MT_ID = (SELECT M.MT_ID FROM CATEGORY_SPECIFIC_MT M WHERE M.EBAY_CATEGORY_ID = $cat_id AND UPPER(M.SPECIFIC_NAME) = UPPER('$spec_name')) AND UPPER(SPECIFIC_VALUE) = UPPER('$custom_attribute')");
        if ($query->num_rows() > 0) {
            return false;
        } else {
            $get_mt_id = $this->db->query("SELECT M.MT_ID FROM CATEGORY_SPECIFIC_MT M WHERE M.EBAY_CATEGORY_ID=$cat_id AND UPPER(M.SPECIFIC_NAME)=UPPER('$spec_name')");
            $get_mt_id = $get_mt_id->result_array();
            $mt_id = $get_mt_id[0]['MT_ID'];

            $get_det_pk = $this->db->query("SELECT get_single_primary_key('CATEGORY_SPECIFIC_DET','DET_ID') SPECIFICS_DET_ID FROM DUAL");
            $get_det_pk = $get_det_pk->result_array();
            $specifics_det_id = $get_det_pk[0]['SPECIFICS_DET_ID'];

            $ins_det_qry = "INSERT INTO CATEGORY_SPECIFIC_DET VALUES ($specifics_det_id $comma $mt_id $comma '$custom_attribute')";
            $ins_det_qry = $this->db->query($ins_det_qry);
        }
    }

    public function custom_specifics()
    {

        $cat_id = $this->input->post('Category_ID');
        $custom_name = ucfirst($this->input->post('test_name'));
        $custom_name = trim(str_replace("  ", ' ', $custom_name));
        $custom_name = trim(str_replace(array("'"), "''", $custom_name));

        $custom_value = ucfirst($this->input->post('test_value'));
        $custom_value = trim(str_replace("  ", ' ', $custom_value));
        $custom_value = trim(str_replace(array("'"), "''", $custom_value));
        $selectionMode = $this->input->post('selectionMode');
        if (empty($selectionMode)) {
            $selectionMode = 'FreeText';
        }

        $catalogue_only = 0;

        $maxValue = $this->input->post('maxValue');

        if (empty($maxValue)) {
            $maxValue = 1;
        }
        $comma = ',';
        date_default_timezone_set("America/Chicago");
        $current_date = date("Y-m-d H:i:s");
        $current_date = "TO_DATE('" . $current_date . "', 'YYYY-MM-DD HH24:MI:SS')";

        $query = $this->db->query("SELECT SPECIFIC_NAME FROM CATEGORY_SPECIFIC_MT WHERE SPECIFIC_NAME = '$custom_name' AND EBAY_CATEGORY_ID = $cat_id");
        if ($query->num_rows() > 0) {
            return false;
        } else {

            $get_mt_pk = $this->db->query("SELECT get_single_primary_key('CATEGORY_SPECIFIC_MT','MT_ID') SPECIFICS_MT_ID FROM DUAL");
            $get_mt_pk = $get_mt_pk->result_array();
            $specifics_mt_id = $get_mt_pk[0]['SPECIFICS_MT_ID'];

            $ins_mt_qry = "INSERT INTO CATEGORY_SPECIFIC_MT(MT_ID, EBAY_CATEGORY_ID, SPECIFIC_NAME, MARKETPLACE_ID, MAX_VALUE, MIN_VALUE, SELECTION_MODE, UPDATE_DATE, CUSTOM, CATALOGUE_ONLY) VALUES ($specifics_mt_id $comma $cat_id $comma '$custom_name' $comma 1 $comma $maxValue $comma 1 $comma '$selectionMode' $comma $current_date $comma 1 $comma $catalogue_only)";
            $ins_mt_qry = $this->db->query($ins_mt_qry);

            $get_det_pk = $this->db->query("SELECT get_single_primary_key('CATEGORY_SPECIFIC_DET','DET_ID') SPECIFICS_DET_ID FROM DUAL");
            $get_det_pk = $get_det_pk->result_array();
            $specifics_det_id = $get_det_pk[0]['SPECIFICS_DET_ID'];

            $ins_det_qry = "INSERT INTO CATEGORY_SPECIFIC_DET VALUES ($specifics_det_id $comma $specifics_mt_id $comma '$custom_value')";
            $ins_det_qry = $this->db->query($ins_det_qry);
        }
    }

    // /********************************
    //  * end Screen search item spcifics
    //  *********************************/

    // /********************************
    //  * start Screen menu
    //  *********************************/
    public function addmenu_main()
    {

        $MainMenu = $this->input->post('MainMenu');
        $enter_by = $this->input->post('enter_by');
        $qry_art_id = $this->db->query("SELECT get_single_primary_key('lz_main_menu','MENU_ID') MENU_ID from dual")->row();
        $menu_id = $qry_art_id->MENU_ID;
        $qry = $this->db->query("INSERT INTO lz_main_menu(MENU_ID,DESCRIPTION,DISPLAY_ORDER,INSERTED_DATE,INSERTED_BY) values('$menu_id','$MainMenu','$menu_id',sysdate,'$enter_by')");

        $get_data = $this->db->query("SELECT menu_id FROM lz_main_menu where menu_id = (SELECT max(menu_id) from lz_main_menu)")->result_array();
        //$data = $this->db->query($get_data)->;
        $tableData = $get_data[0]['MENU_ID'];

        $result = $this->db->query("SELECT f.menu_id,
        f.description,
        f.display_order,
        f.inserted_date,
        f.inserted_by from lz_main_menu f where f.menu_id='$tableData'")->result_array();
        if (!empty($result)) {
            return array('data' => $result, 'status' => true);
        } else {
            return array('data' => [], 'status' => false);
        }
    }
    public function add_sub_menu()
    {

        $SelectMainManu = $this->input->post('SelectMainManu');
        $SubMenu = $this->input->post('SubMenu');
        $LatestOrder = $this->input->post('LatestOrder');
        $enter_by = $this->input->post('enter_by');
        $qry_art_id = $this->db->query("SELECT get_single_primary_key('lz_sub_menu','SUB_MENU_ID') SUB_MENU_ID from dual")->row();
        $menu_Sub = $qry_art_id->SUB_MENU_ID;
        $qry = $this->db->query("INSERT INTO lz_sub_menu(SUB_MENU_ID,MENU_ID,DESCRIPTION,ICON,DISPLAY_ORDER,INSETED_DATE,INSERTED_BY) values('$menu_Sub','$SelectMainManu','$SubMenu','', '$LatestOrder',sysdate,'$enter_by')");

        $get_data = $this->db->query("SELECT SUB_MENU_ID FROM lz_sub_menu where SUB_MENU_ID = (SELECT max(SUB_MENU_ID) from lz_sub_menu)")->result_array();
        //$data = $this->db->query($get_data)->;
        $tableData = $get_data[0]['SUB_MENU_ID'];

        $result = $this->db->query("SELECT h.sub_menu_id,
        h.menu_id,
        h.description,
        h.icon,
        h.display_order,
        h.inseted_date,
        h.inserted_by from lz_sub_menu h where h.sub_menu_id='$tableData'")->result_array();
        if (!empty($result)) {
            return array('data' => $result, 'status' => true);
        } else {
            return array('data' => [], 'status' => false);
        }
    }
    public function Add_Url_data()
    {

        $SelectSubManu = $this->input->post('SelectSubManu');
        $Url = $this->input->post('Url');
        $Component = $this->input->post('Component');
        $sub_menu_url = $this->input->post('sub_menu_url');
        $sub_menu_url = trim(str_replace("  ", ' ', $sub_menu_url));
        $sub_menu_url = trim(str_replace(array("'"), "''", $sub_menu_url));
        $enter_by = $this->input->post('enter_by');
        $qry_art_id = $this->db->query("SELECT get_single_primary_key('lz_sub_menu_url','URL_ID') URL_ID from dual")->row();
        $url_id = $qry_art_id->URL_ID;
        $qry = $this->db->query("INSERT INTO lz_sub_menu_url(URL_ID,SUB_MENU_ID,URL,APP_TYPE,INSERTED_DATE,INSERTED_BY,COMPONENT_NAME) values('$url_id','$SelectSubManu',
        '$sub_menu_url','2',sysdate,'$enter_by','$Component')");
        return $qry;
    }

    public function get_mainDrop()
    {
        $qry = $this->db->query("SELECT h.menu_id, h.description , h.display_order from lz_main_menu h order by display_order asc")->result_array();
        return array('data' => $qry, 'status' => true);
    }
    public function get_main_dispaly()
    {
        $qry = $this->db->query("SELECT  h.description,h.menu_id,h.display_order from lz_main_menu h order by display_order asc")->result_array();
        return array('data' => $qry, 'status' => true);
    }
    public function update_sub_menu()
    {
        $Menu_id = $this->input->post('menu_id');
        $qry = $this->db->query("SELECT h.sub_menu_id , h.description, h.menu_id ,h.display_order from lz_sub_menu h where h.menu_id = $Menu_id order by display_order asc")->result_array();
        return array('data' => $qry, 'status' => true);
    }
    public function get_SubDrop()
    {
        $qry = $this->db->query("SELECT g.sub_menu_id,g.description from lz_sub_menu g order by display_order asc")->result_array();
        return array('data' => $qry, 'status' => true);
    }

    public function get_display_order()
    {
        $SelectMainManu = $this->input->post('SelectMainManu');
        $auto_we_no = $this->db->query("SELECT nvl(max(display_order +1),1) display_order from lz_sub_menu where menu_id ='$SelectMainManu'")->result_array();
        return array('data' => $auto_we_no, 'status' => true);
    }

    public function update_display_order()
    {
        $Menu_id = $this->input->post('sortData');
        $display_order = $this->input->post('dispaly');
        //var_dump($Menu_id , $display_order); exit;

        foreach ($Menu_id as $key => $value) {
            $index = $key + 1;
            $qry = $this->db->query("UPDATE lz_main_menu SET DISPLAY_ORDER='$index'  WHERE MENU_ID =$value");
        }
        return $qry;
    }
    public function sort_form_sub()
    {
        $Menu_id = $this->input->post('sortData1');
        $display_order = $this->input->post('dispaly1');
        //var_dump($Menu_id , $display_order); exit;

        foreach ($Menu_id as $key => $value) {
            $index = $key + 1;
            // var_dump($index);
            $qry = $this->db->query("UPDATE lz_sub_menu SET DISPLAY_ORDER='$index'  WHERE SUB_MENU_ID =$value");
        }
        return $qry;
    }
    // /********************************
    //  * end Screen menu
    //  *********************************/

    public function save_color_info()
    {
        $color_picker = $this->input->post('color_picker');

        $qry = $this->db->query("INSERT INTO lz_purch_item_dt(COLOR) values('')");
    }

    // /********************************
    //  * start Screen black lis data
    //  *********************************/

    public function get_black_Reason()
    {
        $qry = $this->db->query("SELECT * from lz_blacklist_reason")->result_array();
        if ($qry) {
            return array('data' => $qry, 'status' => true);
        }
    }

    public function save_black_list_data()
    {
        $black_barcode = $this->input->post('black_barcode');
        $Remarks_black = $this->input->post('Remarks_black');
        $Reason_black = $this->input->post('Reason_black');
        $enter_by = $this->input->post('enter_by');

        $qry = $this->db->query("SELECT h.f_upc, d.barcode_no, h.f_mpn, h.default_cond ,C.ID, C.COND_NAME from lz_barcode_mt d , lz_item_seed h , LZ_ITEM_COND_MT C where
        d.lz_manifest_id=h.lz_manifest_id
        and d.item_id=  h.item_id
        and d.condition_id= h.default_cond
        AND C.ID = D.CONDITION_ID
        and d.barcode_no='$black_barcode'")->row();
        $upc = $qry->F_UPC;
        $mpn = $qry->F_MPN;
        $cond = $qry->COND_NAME;
        $cond_id = $qry->ID;
        // var_dump($upc,$mpn,$cond); exit();
        $check = $this->db->query("SELECT * from lz_blacklist_items where MPN='$mpn'")->row();
        //  var_dump($check); exit;
        if (empty($check)) {
            $qry_art_id = $this->db->query("SELECT get_single_primary_key('lz_blacklist_items','LIST_ID') LIST_ID from dual")->row();
            $list_id = $qry_art_id->LIST_ID;
            $this->db->query("INSERT INTO lz_blacklist_items (LIST_ID,UPC, MPN, CONDITION, REASON_ID, INSERTED_BY, INSERTED_DATE, REMARKS, CONDITION_ID) values ('$list_id', '$upc',
                  '$mpn', '$cond', '$Reason_black', '$enter_by', sysdate, '$Remarks_black','$cond_id') ");

            if ($qry) {
                return array('data' => $qry, 'status' => true);
            }
        } else {
            return array('data' => [], 'status' => false);
        }
    }
    // /********************************
    //  * start Screen black lis data
    //  *********************************/

}
