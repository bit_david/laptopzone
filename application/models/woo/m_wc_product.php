<?php  
  class m_wc_product extends CI_Model{

    public function __construct(){
    parent::__construct();
    $this->load->database();
    $this->load->database('woo_db',TRUE);
    }

    
    public function listOnK2bay($barcode_no)
{    	
    	$seed_qry = $this->db->query("SELECT DET.WEIGHT,
        LS.SEED_ID,
       LS.ITEM_ID,
       LS.ITEM_TITLE,
       LS.ITEM_DESC,
       LS.EBAY_PRICE,
       LS.TEMPLATE_ID,
       LS.EBAY_LOCAL,
       LS.CURRENCY,
       LS.LIST_TYPE,
       LS.CATEGORY_ID,
       LS.SHIP_FROM_ZIP_CODE,
       LS.SHIP_FROM_LOC,
       LS.DEFAULT_COND,
       LS.DETAIL_COND,
       LS.PAYMENT_METHOD,
       LS.PAYPAL_EMAIL,
       LS.DISPATCH_TIME_MAX,
       LS.SHIPPING_COST,
       LS.ADDITIONAL_COST,
       LS.RETURN_OPTION,
       LS.RETURN_DAYS,
       LS.SHIPPING_PAID_BY,
       LS.SHIPPING_SERVICE,
       LS.CATEGORY_NAME,
       LS.LZ_MANIFEST_ID,
       LM.LOADING_NO,
       LM.LOADING_DATE,
       LM.PURCH_REF_NO,
       LS.F_MANUFACTURE      MANUFACTURE,
       LS.F_MPN              PART_NO,
       I.ITEM_MT_BBY_SKU     SKU,
       LS.F_UPC              UPC,
       LS.DEFAULT_COND       ITEM_CONDITION,
       NULL                  QUANTITY,
       R.GENERAL_RULE,
       R.SPECIFIC_RULE,
       C.COND_NAME,
       LS.EPID,
       PC.CATEGORY_NAME      PART_CATEGORY
  	   FROM LZ_ITEM_SEED LS,
       LZ_MANIFEST_MT LM,
       LZ_MANIFEST_DET DET,
       ITEMS_MT I,
       LZ_LISTING_RULES R,
       LZ_ITEM_COND_MT C,
       LJ_PARTS_CATEGORY PC,
       (SELECT BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID, COUNT(1) QTY
          FROM LZ_BARCODE_MT BC
         WHERE BC.CONDITION_ID IS NOT NULL
           AND BC.HOLD_STATUS = 0
           AND BC.SALE_RECORD_NO IS NULL
           AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL
           AND BC.LZ_PART_ISSUE_MT_ID IS NULL
           AND BC.LZ_POS_MT_ID IS NULL
           AND BC.PULLING_ID IS NULL
           AND BC.ORDER_DT_ID IS NULL
           AND BC.NOT_FOUND_BY IS NULL
           AND BC.LZ_DEKIT_US_MT_ID_FOR_OUT IS NULL
           AND BC.RETURNED_ID IS NULL
           AND BC.DISCARD = 0
           AND BC.CONDITION_ID <> -1
         GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID) BCD,
         LZ_BARCODE_MT B
       WHERE LM.LZ_MANIFEST_ID = DET.LZ_MANIFEST_ID
		AND LS.ITEM_ID = I.ITEM_ID
		AND LS.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID
		AND LS.ITEM_ID = BCD.ITEM_ID
		AND LS.DEFAULT_COND = BCD.CONDITION_ID
		AND LS.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
		AND R.ITEM_CONDITION = LS.DEFAULT_COND
		AND LS.DEFAULT_COND = C.ID
		AND LS.CATEGORY_ID = PC.CATEGORY_ID(+)
		AND B.ITEM_ID = BCD.ITEM_ID
		AND B.LZ_MANIFEST_ID = BCD.LZ_MANIFEST_ID
		AND B.CONDITION_ID = BCD.CONDITION_ID
		AND B.BARCODE_NO = '$barcode_no'
		FETCH FIRST 1 ROWS ONLY")->result_array();

      $condition_id = $seed_qry[0]['ITEM_CONDITION'];
      $lz_manifest_id = $seed_qry[0]['LZ_MANIFEST_ID'];
      $item_id = $seed_qry[0]['ITEM_ID'];

      $list_qty = $this->db->query("SELECT COUNT(1) QTY FROM LZ_BARCODE_MT BC WHERE BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0  and bc.K2BAY_LIST_ID is null AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL AND BC.ORDER_DT_ID IS NULL AND BC.NOT_FOUND_BY IS NULL AND BC.LZ_DEKIT_US_MT_ID_FOR_OUT IS NULL AND BC.RETURNED_ID IS NULL AND BC.DISCARD = 0 AND BC.CONDITION_ID <> -1 AND BC.LZ_MANIFEST_ID = '$lz_manifest_id' AND BC.ITEM_ID = '$item_id'  AND BC.CONDITION_ID = '$condition_id' GROUP BY BC.LZ_MANIFEST_ID, BC.ITEM_ID, BC.CONDITION_ID ")->result_array();

 //      $img_qry = $this->db->query("select 
 //       jd.local_url live_url,jd.PIC_ORDER
 //  from lj_barcode_pic_mt jm, lj_barcode_pic_dt jd, lz_pict_path_config pc
 // where jm.img_mt_id = jd.img_mt_id
 //   and jm.base_url = pc.path_id
 //   and jm.folder_name = '046396027146~OP40401'
 // order by IMG_DT_ID asc")->result_array();

		return array("seed_qry"=>$seed_qry,"list_qty" => $list_qty);

    }
    public function insertK2bayListMt($list_data,$status){

		$productId = $list_data->id;
		$title = $list_data->name;
		$slug = $list_data->slug;
		$permalink = $list_data->permalink;
		$date_created = $list_data->date_created;
		$date_created_gmt = $list_data->date_created_gmt;
		$date_modified_gmt = $list_data->date_modified_gmt;
		$list_type = $list_data->type;
		$list_status = $list_data->status;
		$featured = $list_data->featured;
		$catalog_visibility = $list_data->catalog_visibility;
		$description = $list_data->description;
		$short_description = $list_data->short_description;
		$sku = $list_data->sku;
		$price = $list_data->price;
		$regular_price = $list_data->regular_price;
		$sale_price = $list_data->sale_price;
		$stock_quantity = $list_data->stock_quantity;
		$weight = $list_data->weight;
		$length = $list_data->dimensions->length;
		$width = $list_data->dimensions->width;
		$height = $list_data->dimensions->height;
		$seed_id = $list_data->seed_id;
		$list_qty = $list_data->list_qty;
		$listed_by = $list_data->listed_by;
		$condition_id = $list_data->condition_id;
		$lz_manifest_id = $list_data->lz_manifest_id;
		$item_id = $list_data->item_id;
		$qry = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('K2BAY_LIST_MT','LIST_ID') ID FROM DUAL")->result_array();
        $list_id = $qry[0]['ID'];
		$this->db->query("INSERT INTO K2BAY_LIST_MT (
						list_id,
						product_id,
						seed_id,
						status,
						qty,
						title,
						slug,
						list_price,
						listed_by,
						listed_date,
            PROD_URL
						)VALUES(
						'$list_id',
						'$productId',
						'$seed_id',
						'$status',
						'$list_qty',".
                        $this->db->escape($title).",".
                        $this->db->escape($slug).",
						'$price',
						'$listed_by',
						sysdate,
            '$permalink'						
						)");
		$update_barcode_qry = "UPDATE LZ_BARCODE_MT SET K2BAY_LIST_ID='$list_id',K2BAY_PRODUCT_ID='$productId' WHERE ITEM_ID= '$item_id' AND LZ_MANIFEST_ID = '$lz_manifest_id' AND CONDITION_ID = '$condition_id' AND K2BAY_LIST_ID IS NULL AND SALE_RECORD_NO IS NULL AND ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND LZ_PART_ISSUE_MT_ID IS NULL AND LZ_POS_MT_ID IS NULL AND PULLING_ID IS NULL AND HOLD_STATUS = 0 AND ORDER_ID IS NULL AND EXTENDEDORDERID IS NULL AND NOT_FOUND_BY IS NULL
           AND LZ_DEKIT_US_MT_ID_FOR_OUT IS NULL
           AND RETURNED_ID IS NULL
           AND DISCARD = 0
           AND CONDITION_ID <> -1";
           $this->db->query($update_barcode_qry);

          $k2bay_list_query = $this->db->query("SELECT HH.PRODUCT_ID, TO_CHAR(HH.LISTED_DATE, 'YYYY-MM-DD HH24:MI:SS') LIST_DATE, 'K2BAY' LZ_SELLER_ACCT_ID, HH.PROD_URL URLL, HH.QTY LIST_QTY, HH.LIST_PRICE, TRIM(HH.STATUS) STATUS, 'K2BAY' CCOUNT_NAME FROM K2BAY_LIST_MT HH WHERE HH.LIST_ID = '$list_id'")->result_array();

          if(count($k2bay_list_query) >0){            
          return array('k2bay_list_query'=>$k2bay_list_query,'ckdata'  => 'true' );
          }else{
            return array('k2bay_list_query'=>$k2bay_list_query,'ckdata' => 'false');
          }

           

    } 

    
    public function endItemWoo($product_id,$remarks,$user_id)
    {

      
       //$product_id =$this->input->post('product_id') ;
        // $remarks = $this->input->post('Remarks');
        // $user_id = $this->input->post('user_id');
        //$ebay_id = $this->input->post('ebay_id');
        // check if item sold or not
        $check_sold = $this->db->query("select * from lz_k2bay_order_det where PRODUCT_ID ='$product_id' ");
        if ($check_sold->num_rows() > 0) {
           return 2;
        } else {
            // $endDate = $response->Timestamp->format('Y-m-d H:i:s');
            // $message = 'Item Ended Successfully.';
            // $endDate = "TO_DATE('" . $endDate . "', 'YYYY-MM-DD HH24:MI:SS')";
           
            date_default_timezone_set("America/Chicago");
            $current_date = date("Y-m-d H:i:s");
            $current_date = "TO_DATE('" . $current_date . "', 'YYYY-MM-DD HH24:MI:SS')";

            $pk_query = $this->db->query("SELECT  GET_SINGLE_PRIMARY_KEY('LZ_ENDITEM_LOG','LOG_ID') ID FROM DUAL")->result_array();

            $log_id = $pk_query[0]['ID'];

            $ins = $this->db->query("INSERT INTO LZ_ENDITEM_LOG (LOG_ID, EBAY_ID, ENDED_BY, ENDED_DATE, INSERT_DATE,REMARKS,SOURCE) VALUES ($log_id,'$product_id',$user_id,sysdate,$current_date,'$remarks','k2bay')");

            if ($ins) {

              $this->db->query("call  pro_insert_ebay_log($product_id,$log_id)");

               $barcode_qry = $this->db->query("UPDATE LZ_BARCODE_MT B SET B.K2BAY_LIST_ID = '',K2BAY_PRODUCT_ID = '',B.EBAY_STICKER = 0 WHERE B.K2BAY_PRODUCT_ID = '$product_id' AND B.SALE_RECORD_NO IS NULL AND B.ORDER_ID IS NULL AND B.EXTENDEDORDERID IS NULL AND B.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND B.LZ_PART_ISSUE_MT_ID IS NULL AND B.LZ_POS_MT_ID IS NULL AND B.PULLING_ID IS NULL");

            // $delete_qry = $this->db->query("DELETE FROM LZ_LISTING_ALLOC WHERE LIST_ID IN (SELECT LIST_ID FROM EBAY_LIST_MT E WHERE E.EBAY_ITEM_ID = '$ebay_id')");

            $delete_qry = $this->db->query("DELETE FROM K2BAY_LIST_MT E WHERE E.PRODUCT_ID = '$product_id'");

                    // $delete_url = $this->db->query("DELETE FROM LZ_LISTED_ITEM_URL U WHERE U.EBAY_ID =  '$ebay_id'");

                }

        return 1;
      } 

  }
}
