<?php  

class Common_Model extends CI_Model{

    public $wooDB;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->wooDB = $this->load->database('woo_db',TRUE);
    }

    public function termExists($term, $taxonomy = '', $parent = null){
        $this->wooDB->select('tt.term_id, tt.term_taxonomy_id,tt.count as taxonomy_count');
        $this->wooDB->from('wp_terms t');
        $this->wooDB->join('wp_term_taxonomy tt','tt.term_id = t.term_id','inner');
        $where = array(
            't.slug' => $term,
            //'tt.parent' => $parent,
            'tt.taxonomy' => $taxonomy
        );
        $this->wooDB->where($where);
        $this->wooDB->limit(1);
        $this->wooDB->order_by('t.term_id','ASC');
        $query = $this->wooDB->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function catTree($cat_id){
        $query = $this->db->query('select fun_cat_tree('.$cat_id.') as cat_tre from dual');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function createTerm($term,$slug,$taxonomy,$description,$parent,$object_id){
        $data1 = [
            'name' => $term,
            'slug' => $slug,
        ];
        $id = $this->wooDB->insert('wp_terms',$data1);
        $wp_term_id = $this->wooDB->insert_id();

        if($wp_term_id > 0){

            //Insert data into term_taxonomy table
            $wp_term_taxonomy_data = [
                'term_id' => $wp_term_id,
                'taxonomy' => $taxonomy,
                'description' => $description,
                'parent'    => $parent,
                'count' => 1
            ];
            $this->wooDB->insert('wp_term_taxonomy',$wp_term_taxonomy_data);
            $term_taxonomy_id = $this->wooDB->insert_id();
            if($term_taxonomy_id > 0){

                //Attach brand with product in wp_term_relationships
                $data = [
                    'object_id' => $object_id,
                    'term_taxonomy_id' => $term_taxonomy_id,
                ];
                $this->makeTermRelation($data);

                //Add term data inside wp_termmeta meta_value
                $termmeta_data = [
                    'term_id' => $wp_term_id,
                    'meta_key' => 'product_count_product_brand',
                    'meta_value' => 1
                ];
                $this->insert('wp_termmeta',$termmeta_data);

                $response = ['Success' => 'true','Status' => 'Brand created ,add term relationships and add termmeta', 'term_id' => $wp_term_id, 'term_taxonomy_id' => $term_taxonomy_id];
                return $response;
            }else{
                return false;
            }    
        }else{
            return false;
        }
       
    }

    public function makeTermRelation($data){
        $this->wooDB->insert('wp_term_relationships',$data);
    }

    public function update($table,$update,$where){
        $this->wooDB->update($table,$update,$where);
        return $this->wooDB->affected_rows();
    }

    public function insert($table, $data){
        $this->wooDB->insert($table, $data);
        return $this->wooDB->insert_id();
    }

    public function insertBatch($table, $dataBatch) {
        $this->wooDB->insert_batch($table, $dataBatch);
        return $this->wooDB->affected_rows();
    }

    public function getWooMenuMaxOrder($where){
        $this->wooDB->select('max(p.menu_order) as max_order');
        $this->wooDB->from('wp_posts p');
        $this->wooDB->join('wp_term_relationships tr','tr.object_id = p.ID');
        if (!empty($where))
            $this->wooDB->where($where);
        $query = $this->wooDB->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getRecord($select, $table, $where = '') {
        $this->wooDB->select($select);
        $this->wooDB->from($table);
        if (!empty($where))
            $this->wooDB->where($where);
        $query = $this->wooDB->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /**
     * Condition : Item_id & category_id
     * response  : Item atrributes with values
     */
    public function getItemAttributesFromListing($item_id,$lz_manifest_id,$condition_id){
        
        // $item_id = 18786;
        // $manifest_id = 13827;
        // $query = $this->db->query("SELECT s.f_upc  I.ITEM_MT_UPC, s.f_mpn I.ITEM_MT_MFG_PART_NO, S.CATEGORY_ID FROM ITEMS_MT I, LZ_ITEM_SEED S WHERE I.ITEM_ID = $item_id AND I.ITEM_ID = S.ITEM_ID AND S.LZ_MANIFEST_ID = $manifest_id AND S.DEFAULT_COND = $condition_id AND ROWNUM = 1");
        $query = $this->db->query("SELECT s.f_upc  ITEM_MT_UPC, s.f_mpn ITEM_MT_MFG_PART_NO, S.CATEGORY_ID FROM ITEMS_MT I, LZ_ITEM_SEED S WHERE I.ITEM_ID = $item_id AND I.ITEM_ID = S.ITEM_ID AND S.LZ_MANIFEST_ID = $lz_manifest_id AND S.DEFAULT_COND = $condition_id AND ROWNUM = 1");

        $result = $query->result_array();

        if ($query->num_rows() > 0) {

            if (!empty($result[0]['ITEM_MT_UPC'])) {
                $where_upc = " AND MT.UPC = '" . $result[0]['ITEM_MT_UPC'] . "'";
            } else {
                //$where_upc = ' ';
                $where_upc = " AND MT.UPC IS NULL";
            }
            if (!empty($result[0]['ITEM_MT_MFG_PART_NO'])) {
                $where_mpn = " AND MT.MPN = '" . $result[0]['ITEM_MT_MFG_PART_NO'] . "'";
            } else {
                //$where_mpn = '';
                $where_mpn = " AND MT.MPN IS NULL";
            }

            $spec_query = $this->db->query("SELECT MT.SPECIFICS_NAME, DT.SPECIFICS_VALUE FROM LZ_ITEM_SPECIFICS_MT MT, LZ_ITEM_SPECIFICS_DET DT WHERE DT.SPECIFICS_MT_ID = MT.SPECIFICS_MT_ID  AND MT.CATEGORY_ID = " . $result[0]['CATEGORY_ID'] . $where_upc . $where_mpn);
            $spec_query = $spec_query->result_array();

        } else {
            $spec_query = "";
        }

        return $spec_query;

        //var_dump($spec_query);exit ;

    }

    public function createOrderInListing($order_array){
        $list_rslt = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lz_k2bay_order_mt','LZ_K2BAY_ORDER_MT_ID') lz_k2bay_order_mt_id FROM DUAL");
        $lz_k2bay_order_mt_id = $list_rslt->result_array();
        $lz_k2bay_order_mt_id = $lz_k2bay_order_mt_id[0]['LZ_K2BAY_ORDER_MT_ID'];
        $date_created = $order_array['date_created'];
        $date_paid= $order_array['date_paid'];

        $date_created = str_replace('T'," ",$date_created); 
        $date_created= "TO_DATE('$date_created', 'YYYY-MM-DD HH24:MI:SS')";

        $date_paid = str_replace('T'," ",$date_paid); 
        $date_paid= "TO_DATE('$date_paid', 'YYYY-MM-DD HH24:MI:SS')";


        //$date_created = ''; 
        //var_dump($date_created);exit(1);
        $data = [
            'LZ_K2BAY_ORDER_MT_ID' => $lz_k2bay_order_mt_id,
            'ORDER_ID' =>   '03-'.$order_array['id'],
            'ORDER_NUMBER' => '03-'.$order_array['id'],
            'CREATED_VIA' => $order_array['created_via'], 
            'STATUS' => $order_array['status'],
            //'DATE_CREATED' =>  $date_created,
            'DISCOUNT_TOTAL' => $order_array['discount_total'],
            'DISCOUNT_TAX' => $order_array['discount_tax'],
            'SHIPPING_TOTAL' => $order_array['shipping_total'],
            'SHIPPING_TAX' => $order_array['shipping_tax'],
            'TOTAL' => $order_array['total'],
            'TOTAL_TAX' => $order_array['total_tax'],
            'PRICES_INCLUDE_TAX' => $order_array['prices_include_tax'],
            'CUSTOMER_ID' => $order_array['customer_id'],
            'CUSTOMER_NOTE' => $order_array['customer_note'],
            'FIRST_NAME' => $order_array['billing']['first_name'],
            'LAST_NAME' => $order_array['billing']['last_name'],
            'COMPANY' => $order_array['billing']['company'],
            'ADDRESS_1' => $order_array['billing']['address_1'],
            'ADDRESS_2' => $order_array['billing']['address_2'],
            'CITY' => $order_array['billing']['city'],
            'STATE' => $order_array['billing']['state'],
            'POSTCODE' => $order_array['billing']['postcode'],
            'COUNTRY' => $order_array['billing']['country'],
            'EMAIL' => $order_array['billing']['email'],
            'PHONE' => $order_array['billing']['phone'],
            'SHIP_FIRST_NAME' => $order_array['billing']['first_name'],
            'SHIP_LAST_NAME' => $order_array['billing']['last_name'],
            'SHIP_ADDRESS_1' => $order_array['billing']['address_1'],
            'SHIP_ADDRESS_2' => $order_array['billing']['address_2'],
            'SHIP_CITY' => $order_array['billing']['city'],
            'SHIP_STATE' => $order_array['billing']['state'],
            'SHIP_POSTCODE' => $order_array['billing']['postcode'],
            'SHIP_COUNTRY' => $order_array['billing']['country'],
            'PAYMENT_METHOD' => $order_array['payment_method'],
            'PAYMENT_METHOD_TITLE' => $order_array['payment_method_title']
            //'DATE_PAID'  => ''
        ];
        //$this->db->set($data,FALSE);
        $this->db->set('DATE_CREATED',$date_created,FALSE);
        $this->db->set('DATE_PAID',$date_paid,FALSE);
        //Table name should be in uppercase incase of oracle db insertion 
        $this->db->insert('LZ_K2BAY_ORDER_MT',$data);
        $itemsDataBatch = array();
        for ($i = 0; $i < sizeof($order_array['line_items']); $i++) {
            
            $list_rslt = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('lz_k2bay_order_det','LZ_K2BAY_ORDER_DET_ID') lz_k2bay_order_det_id FROM DUAL");
            $lz_k2bay_order_det_id = $list_rslt->result_array();
            $lz_k2bay_order_det_id = $lz_k2bay_order_det_id[0]['LZ_K2BAY_ORDER_DET_ID'];
    
            $itemsPushBatch = array(
                'LZ_K2BAY_ORDER_DET_ID' => $lz_k2bay_order_det_id,
                'LZ_K2BAY_ORDER_MT_ID' => $lz_k2bay_order_mt_id,
                'ITEM_TITLE' => $order_array['line_items'][$i]['name'],
                'PRODUCT_ID'=> $order_array['line_items'][$i]['product_id'],
                'QUANTITY' => $order_array['line_items'][$i]['quantity'],
                'SUBTOTAL' => $order_array['line_items'][$i]['subtotal'],
                'SUBTOTAL_TAX' => $order_array['line_items'][$i]['subtotal_tax'],
                'TOTAL' =>$order_array['line_items'][$i]['total'],
                'TOTAL_TAX' => $order_array['line_items'][$i]['total_tax'],
                'K2BAY_ORDER_ID' => '03-'.$order_array['id']
            );
            
            array_push($itemsDataBatch, $itemsPushBatch);
        }
        
        $this->db->insert_batch('LZ_K2BAY_ORDER_DET',$itemsDataBatch);
    }

    public function updateOrderListing($table,$update,$where){
        $this->db->update($table,$update,$where);
        return $this->db->affected_rows();
    }

    public function getRecordFromListing($select, $table, $where = '', $orderbyCol, $orderby){
        $query = $this->db->query('select  CATEGORY_ID,CATEGORY_NAME from lz_bd_category_tree  where PARENT_CAT_ID is null order by CATEGORY_ID desc')->result_array();return $query;
        // $this->db->select($select);
        // $this->db->from($table);
        // if (!empty($where))
        //     $this->db->where($where);
        // if (!empty($orderbyCol && $orderby))   
        //     $this->db->order_by($orderbyCol, $orderby);    
        // $query = $this->db->get();
        // if ($query->num_rows() > 0) {
        //     return $query->row_array();
        // } else {
        //     return false;
        // }
    }
}   

?>

