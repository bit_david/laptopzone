<?php
class c_firebase extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->database();
    //$this->load->model('rssFeed/m_rssfeed');
   // $this->load->model('firebase/m_firebase');
    $this->load->helper('security');
    /*=============================================
    =  Section lz_bigData db connection block  =
    =============================================*/
    //$CI = &get_instance();
    //setting the second parameter to TRUE (Boolean) the function will return the database object.
   // $this->db2 = $CI->load->database('bigData', TRUE);
    /*======= End Section lz_bigData db connection block=====*/

    /*==============================================================
    =            get distinct category for rss feed url            =
    ==============================================================*/
    // $get_cat = $this->db2->query("SELECT DISTINCT CATEGORY_ID from LZ_BD_RSS_FEED_URL")->result_array();
    // foreach ($get_cat as $cat) {
    //   $this->cat_rss_feed($cat['CATEGORY_ID']);
    // }

    /*=====  End of get distinct category for rss feed url  ======*/
      
  }
public function loadview()
  {
      //$verify_pro = "CALL PRO_RECOG_DATA_JOB()";
    $this->load->view('firebase/downloadData');
  }
public function getFbData()
  {
    $fb_data = json_decode($_GET['parm_order']);
    $barcode = $_GET['barcode'];
    //var_dump($barcode);exit;
    $upc = @$fb_data->UPC;
    $ean = @$fb_data->EAN;
    $enteredBy = @$fb_data->enteredBy;
    $enteredDate = @$fb_data->enteredDate;
    $object = @$fb_data->object;
    $remarks = @$fb_data->Remarks;
    $bin = @$fb_data->bin;
    $condition = @$fb_data->condition;
    $conditionRemarks = @$fb_data->conditionRemarks;
    $folderName = @$fb_data->folderName;
    //var_dump($upc , $ean , $enteredBy , $enteredDate , $object , $remarks , $bin , $condition , $conditionRemarks , $folderName);
    $enteredDate = "TO_DATE('".$enteredDate."', 'DD/MM/YYYY HH24:MI:SS')";  

    $qry = $this->db->query("SELECT SPECIAL_LOT_ID FROM LZ_SPECIAL_LOTS WHERE BARCODE_PRV_NO = '$barcode'");
    if($qry->num_rows() == 0){

      $query = $this->db->query("SELECT B.BIN_ID FROM BIN_MT B WHERE B.BIN_TYPE || '-' || B.BIN_NO = UPPER('$bin')");
      if($query->num_rows() == 0){
        $bin_id = 0;
      }else{
        $query = $query->result_array();
        $bin_id = $query[0]['BIN_ID'];
      }

      $this->db->query("INSERT INTO LZ_SPECIAL_LOTS (SPECIAL_LOT_ID, BARCODE_PRV_NO, BIN_ID, PIC_DATE_TIME, PIC_BY, LOT_REMARKS, CONDITION_ID, CARD_UPC, INSERTED_AT, INSERTED_BY, FOLDER_NAME, CONDITION_REMRAKS,EAN,FB_OBJECT_NAME) VALUES( get_single_primary_key('LZ_SPECIAL_LOTS','SPECIAL_LOT_ID'),'$barcode','$bin_id',$enteredDate,'$enteredBy','$remarks','$condition','$upc',SYSDATE,'$enteredBy','$folderName','$conditionRemarks','$ean','$object')");
    }else{
      echo "Already inserted" . "<br>";
    }
    /*=======================================
    =            save pic to dir            =
    =======================================*/
    $query = $this->db->query("SELECT C.MASTER_PATH FROM LZ_PICT_PATH_CONFIG C WHERE C.PATH_ID = 2")->result_array();
    $pic_dir = $query[0]['MASTER_PATH'];
    
    $folder_dir = $pic_dir.$folderName;
    $thumb_dir = $pic_dir.$folderName.'\thumb';

    if(!is_dir($folder_dir)){
        mkdir($folder_dir);
        if(!is_dir($thumb_dir)){
          mkdir($thumb_dir);
        }
      $i=0;
      $azRange = range('A', 'Z');
      $imgUrls = @$fb_data->imgUrl;
      foreach (@$imgUrls as $key => $url) {
        //var_dump($url->url);//exit;
        $imageUrl = @$url->url;
        $thumbUrl = @$url->thumbUrl;
        //$imageTime = $url->imageTime;
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789'; 
        $img_name = '';
        $max = strlen($characters) - 1;
        for ($k = 0; $k < 10; $k++) {
            $img_name .= $characters[mt_rand(0, $max)];
        }

        copy(@$imageUrl, $folder_dir.'\\'.$azRange[$i]."_fb_".$img_name.'.jpg');
        copy(@$thumbUrl, $thumb_dir.'\\'.$azRange[$i]."_fb_".$img_name.'.jpg');

        $i++;
      }
    }else{
      echo $folder_dir . " already exist";
    }
    
    
    
    /*=====  End of save pic to dir  ======*/
  }

  public function getFirebaseData()
  {
    $data = $this->input->post('data');
    //var_dump($data);//exit;
    foreach ($data as $barcode => $value) {
      //echo $barcode."<br>";
//var_dump($value['imgUrl'],@$value['UPC']);exit;
//var_dump($value);exit;
      $upc = @$value['UPC'];
      $ean = @$value['EAN'];
      $enteredBy = @$value['enteredBy'];
      $enteredDate = @$value['enteredDate'];
      $object = @$value['object'];
      $remarks = @$value['Remarks'];
      $bin = @$value['bin'];
      $condition = @$value['condition'];
      $conditionRemarks = @$value['conditionRemarks'];
      $folderName = @$value['folderName'];
  //var_dump($upc,$ean,$enteredBy,$enteredDate,$object,$remarks,$bin,$condition,$conditionRemarks);exit;    
      //var_dump(expression)
      // foreach ($value['imgUrl'] as $key => $url) {
      //   var_dump($key,$url);
      // }
      
      $enteredDate = "TO_DATE('".$enteredDate."', 'DD/MM/YYYY HH24:MI:SS')";  

      $qry = $this->db->query("SELECT SPECIAL_LOT_ID FROM LZ_SPECIAL_LOTS WHERE BARCODE_PRV_NO = '$barcode'");
      if($qry->num_rows() == 0){

        $query = $this->db->query("SELECT B.BIN_ID FROM BIN_MT B WHERE B.BIN_TYPE || '-' || B.BIN_NO = UPPER('$bin')");
        if($query->num_rows() == 0){
          $bin_id = 0;
        }else{
          $query = $query->result_array();
          $bin_id = $query[0]['BIN_ID'];
        }

        $this->db->query("INSERT INTO LZ_SPECIAL_LOTS (SPECIAL_LOT_ID, BARCODE_PRV_NO, BIN_ID, PIC_DATE_TIME, PIC_BY, LOT_REMARKS, CONDITION_ID, CARD_UPC, INSERTED_AT, INSERTED_BY, FOLDER_NAME, CONDITION_REMRAKS,EAN,FB_OBJECT_NAME) VALUES( get_single_primary_key('LZ_SPECIAL_LOTS','SPECIAL_LOT_ID'),'$barcode','$bin_id',$enteredDate,'$enteredBy','$remarks','$condition','$upc',SYSDATE,'$enteredBy','$folderName','$conditionRemarks','$ean','$object')");
      }else{
        echo "Already inserted" . "<br>";
      }
          
    }

  }
}
