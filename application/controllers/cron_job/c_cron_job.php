<?php
header("Access-Control-Allow-Origin: *");
use Kreait\Firebase\Configuration;
use Kreait\Firebase\Firebase;
class c_cron_job extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->database();
    $this->load->model('rssFeed/m_rssfeed');
    $this->load->model('cron_job/m_cron_job');
    $this->load->helper('security');
    /*=============================================
    =  Section lz_bigData db connection block  =
    =============================================*/
    $CI = &get_instance();
    //setting the second parameter to TRUE (Boolean) the function will return the database object.
    //$this->db2 = $CI->load->database('bigData', TRUE);
    /*======= End Section lz_bigData db connection block=====*/

    /*==============================================================
    =            get distinct category for rss feed url            =
    ==============================================================*/
    // $get_cat = $this->db2->query("SELECT DISTINCT CATEGORY_ID from LZ_BD_RSS_FEED_URL")->result_array();
    // foreach ($get_cat as $cat) {
    //   $this->cat_rss_feed($cat['CATEGORY_ID']);
    // }

    /*=====  End of get distinct category for rss feed url  ======*/
      
  }
  public function test_ebay()
  {
    $this->load->view('API/GetOrders/GetOrders_test');
  }
  public function getOrders_mod()
  {
    $this->load->view('API/GetOrders/getOrdersAll');
  }
  public function getOrders_ById()
  {
    $this->load->view('API/GetOrders/getOrdersAllById');
  }
  public function GetOrderTransactions()
  {
    $this->load->view('API/GetOrders/GetOrderTransactions_byitemId');
  }
  public function getSingleOrderById()
  {
    $result['LZ_SELLER_ACCT_ID']= $this->input->post("seller_id");
    $result['ORDER_ID']= $this->input->post("order_id");
    $result['print_r']= true;
    //var_dump($result);exit;

    //$result['LZ_SELLER_ACCT_ID']= '2';
    //$result['ORDER_ID']= '254243058755-2473168825015';// '07-03528-19848'
    $this->load->view('API/GetOrders/getSingleOrderById',$result);
    $response = $this->session->has_userdata('response');
    if($response){
      if(@$this->session->userdata('response') === 'error'){
        $data['response'] = @$this->session->userdata('response');
        $data['code'] = @$this->session->userdata('code');
        $data['shortMsg'] = @$this->session->userdata('shortMsg');
        $data['longMsg'] = @$this->session->userdata('longMsg');

        
        @$this->session->unset_userdata('code');
        @$this->session->unset_userdata('shortMsg');
        @$this->session->unset_userdata('longMsg');

      }else{
        $data['response'] = @$this->session->userdata('response');
      }

    }else{
      $data['response'] = 'Network';
    }
    @$this->session->unset_userdata('response');
    echo json_encode($data);
    return json_encode($data);
  }
// public function ebayNotificationListener()
//   {
// //$this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date,NOTIFICATION_TYPE) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'from cron job',sysdate,'')");
//       // $result['data']=$this->m_cron_job->runAllSoldFeed();
//       //$this->load->view('firebase/listener');
//       // echo "All Category Feed Updated".PHP_EOL;

//       //$stdin = $GLOBALS['HTTP_RAW_POST_DATA'];
// $stdin = file_get_contents("php://input");
// file_put_contents('test.xml', $stdin,FILE_APPEND);
// // Loads the XML
// $xml = simplexml_load_string($stdin);

// $mainNode = $xml->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children()->getName();

// $this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'r1:$mainNode',sysdate)");
// $response = $xml->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children()->$mainNode;

// $NotificationEventName = (string) $response->NotificationEventName;
// $ItemID = (string) $response->Item->ItemID;

// $merchant_name = (string) $response->RecipientUserID;
// //$data['Timestamp'] = (string) $response->Timestamp;
// $Timestamp = (string) $response->Timestamp;

// $dTimestamp = (string) $response->Timestamp;

// $Timestamp = str_split($Timestamp,19);
// $Timestamp = str_replace('T'," ",$Timestamp[0]);
// $this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'NotificationEventName:$NotificationEventName , ItemID: $ItemID merchant_name: $merchant_name, ',sysdate)");
// if($mainNode == 'GetItemTransactionsResponse'){
// $Transaction = $response->TransactionArray->Transaction;
// $OrderID = $Transaction->ContainingOrder->OrderID;
// $this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'OrderID:$OrderID',sysdate)");
// $data['Timestamp'] = $Timestamp;
// $data['merchant_name'] = $merchant_name;
// $data['NotificationEventName'] = $NotificationEventName;
// $data['ItemID'] = $ItemID;
// $data['OrderID'] = $OrderID;
// //$this->load->view('API/GetOrders/GetOrders_notification',$data);
// $this->load->view('API/GetOrders/GetOrdersByID_notification',$data);

// }else{
// $this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'from else of mainnode ',sysdate)");
// }
// //7188


// //$this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'test.adil',sysdate)");
// //$qry = $this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'$dTimestamp',sysdate)");
// //$qry = $this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),  'OK',sysdate)");

// //$qry = $this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date,NOTIFICATION_TYPE) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'$OrderID cron_job',sysdate,'$NotificationEventName')");




// // uplaod data to fb
// // if(!empty($NotificationEventName)){
// //   require __DIR__.'/vendor/autoload.php';


// //   use Kreait\Firebase\Configuration;
// //   use Kreait\Firebase\Firebase;

// //   $config = new Configuration();
// //   //$config->setAuthConfigFile(__DIR__.'/secret/fbtest-e7425-8d3d759d5984.json');
// //   $config->setAuthConfigFile(__DIR__.'/secret/plateformnotification-b2648d771b9f.json');

// //   //$firebase = new Firebase('https://fbtest-e7425.firebaseio.com/', $config);
// //   $firebase = new Firebase('https://plateformnotification.firebaseio.com/', $config);

// //   // $firebase->set(['key' => 'value'], 'my/data');
// //   // $firebase->set('new value', 'my/data/key');

// //   $newKey = $firebase->push([
// //       'merchant_name' => $merchant_name,
// //       'Timestamp' => $Timestamp,
// //       'NotificationEventName' => $NotificationEventName,
// //       'ItemID' => $ItemID
// //   ], 'PlateformNotification/ebay/itemSold');
// // }


    
//   }
public function ebayNotificationListener()
{
  //$stdin = $GLOBALS['HTTP_RAW_POST_DATA'];
  $stdin = file_get_contents("php://input");
  file_put_contents('test.xml', $stdin,FILE_APPEND);// save response in file
  // Loads the XML
  $xml = simplexml_load_string($stdin);

  $mainNode = $xml->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children()->getName();
  $response = $xml->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children()->$mainNode;
  $NotificationEventName = (string) $response->NotificationEventName;
  $merchant_name = (string) $response->RecipientUserID;

  $this->db->query("INSERT INTO EBAY_NOTEIFICATION (TEST_PK,E_DATA,INSERT_DATE,NOTIFICATION_TYPE) VALUES (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'merchant_name: $merchant_name',sysdate,'$NotificationEventName')");
  switch ($mainNode) {
    case 'GetItemTransactionsResponse':
      $this->GetItemTransactionsResponse($response);
      break;
    case 'NotificationEvent':
      $this->NotificationEvent($response);
      break;
    case 'GetItemResponse':
      $this->GetItemResponse($response);
      break;
    case 'GetMyMessagesResponse':
      $this->GetMyMessagesResponse($response);
      break;
    case 'GetDisputeResponse':
      $this->GetDisputeResponse($response);
      break;
    case 'GetBestOffersResponse':
      $this->GetBestOffersResponse($response);
      break;
    case 'GetFeedbackResponse':
      $this->GetFeedbackResponse($response);
      break;
    default:
      # code...
      break;
  }  
}

public function notificationTesting()
{
  
  // Loads the XML
  //$xml = simplexml_load_string($stdin);
  //$xml = simplexml_load_file("testorder.xml");
  //$xml = simplexml_load_file("MyMessagesM2MMessage.xml");
  //$xml = simplexml_load_file("ItemMarkedShipped.xml");
  //$xml = simplexml_load_file("FixedPriceTransaction.xml");
  //$xml = simplexml_load_file("ReturnDelivered.xml");
  //$xml = simplexml_load_file("MyMessagesM2MMessageHeader.xml");
  //$xml = simplexml_load_file("OutBid.xml");
  //$xml = simplexml_load_file("BuyerCancelRequested.xml");
  //$xml = simplexml_load_file("ReturnCreated.xml");
  //$xml = simplexml_load_file("BestOffers.xml");
  //$xml = simplexml_load_file("ItemRevised.xml");
  //$xml = simplexml_load_file("ItemListed.xml");
  //$xml = simplexml_load_file("ItemSold.xml");
  $xml = simplexml_load_file("ItemRevised_WithSold.xml");
  //$xml = simplexml_load_file("ItemListed-ebay payment.xml");

  $mainNode = $xml->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children()->getName();


  $response = $xml->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children()->$mainNode;
  // var_dump($mainNode);
  // echo "<pre>";
  // print_r($response);
  // echo "</pre>";exit;
  $NotificationEventName = (string) $response->NotificationEventName;
  $merchant_name = (string) $response->RecipientUserID;

  $this->db->query("INSERT INTO EBAY_NOTEIFICATION (TEST_PK,E_DATA,INSERT_DATE,NOTIFICATION_TYPE) VALUES (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'merchant_name: $merchant_name',sysdate,'$NotificationEventName')");
  switch ($mainNode) {
    case 'GetItemTransactionsResponse':
      $this->GetItemTransactionsResponse($response);
      break;
    case 'NotificationEvent':
      $this->NotificationEvent($response);
      break;
    case 'GetItemResponse':
      $this->GetItemResponse($response);
      break;
    case 'GetMyMessagesResponse':
      $this->GetMyMessagesResponse($response);
      break;
    case 'GetDisputeResponse':
      $this->GetDisputeResponse($response);
      break;
    case 'GetBestOffersResponse':
      $this->GetBestOffersResponse($response);
      break;
    case 'GetFeedbackResponse':
      $this->GetFeedbackResponse($response);
      break;
    default:
      # code...
      break;
  }
  if($NotificationEventName ==='MyMessagesM2MMessage'){
    $this->GetMyMessagesResponse($response);
  }
  exit;
    
}

  public function getOrders_new()
  {

      // $result['data']=$this->m_cron_job->runAllSoldFeed();
      $this->load->view('API/GetOrders/GetAllAccOrders');
      // echo "All Category Feed Updated".PHP_EOL;
    
  }

  public function recognizeDatajob()
  {
    $this->db2->query("CALL PRO_RECOG_DATA_JOB()");
      //$verify_pro = "CALL PRO_RECOG_DATA_JOB()";
  }
  public function testFunction()
  {
    $response = 'ok';
    //$data = $this->db->query("DECLARE RESPONSE VARCHAR2(200); BEGIN pro_processAgingItemSingle('1',RESPONSE); DBMS_OUTPUT.put_line(RESPONSE); END;")->result_array();
    $sql = "call pro_processAgingItemSingle('1',?)";
    $data = $this->db->query($sql,'OK')->result_array();
    var_dump($data);
  }
  public function createActiveListingUrlFile()
  {
    $this->m_cron_job->createActiveListingUrlFile();
    echo "Feed Updated".PHP_EOL;
      //$verify_pro = "CALL PRO_RECOG_DATA_JOB()";
  }
  public function auto_rss_feed()
  {
    //$this->output->nocache();
    $this->load->library('rssparser');
    //for  ($k = 1; $k <=  100000000000000; $k++) {
      
      //$result['pageTitle'] = 'eBay RSS Feed';
      $this->m_rssfeed->auto_rss_feed();
      echo "Feed Updated".PHP_EOL;
     
       $this->auto_rss_feed();
    //}
  }
  public function cat_rss_feed($cat_id = '')
  {
    if(!empty($cat_id)){
      $this->load->library('rssparser');
      $this->m_cron_job->cat_rss_feed($cat_id);
      echo $cat_id ."-Feed Updated".PHP_EOL;
      $this->cat_rss_feed($cat_id);
    }else{
      echo 'category_id not found';
    }
  }

  public function lookup_feed($feed_id = '')
  {
    if(!empty($feed_id)){
      $this->load->library('rssparser');
      $this->m_cron_job->lookup_feed($feed_id);
      echo $feed_id ."-Feed Updated".PHP_EOL;
      $this->lookup_feed($feed_id);
    }else{
      echo 'feed_id not found';
    }
  }
  public function lookup_sold_feed($feed_id = '')
  {
    if(!empty($feed_id)){
      $result['data']=$this->m_cron_job->lookup_sold_feed($feed_id);
      $this->load->view('ebay/async/soldItemWithKeyword',$result);
      echo $feed_id ."-Feed Updated".PHP_EOL;
    }else{
      echo 'feed_id not found';
    }
  }  
  public function runAllSoldFeed()
  {

      $result['data']=$this->m_cron_job->runAllSoldFeed();
      $this->load->view('ebay/async/soldItemWithKeyword',$result);
      echo "All Category Feed Updated".PHP_EOL;
    
  }
  public function runPurchasedSoldFeed()
  {
      $result['data']=$this->m_cron_job->runPurchasedSoldFeed();
      $this->load->view('ebay/async/soldItemWithKeyword',$result);
      echo "All Category Feed Updated".PHP_EOL;
  }   
  public function lookup_feed_chunk()
  {
    //if(!empty($feed_id)){
      $this->load->library('rssparser');
      $this->m_cron_job->lookup_feed_chunk();
      echo $cat_id ."-Feed Updated".PHP_EOL;
      $this->lookup_feed_chunk();
    // }else{
    //   echo 'feed_id not found';
    // }
  }
  public function createLookupStream_chunk()
  {
      $this->m_cron_job->createLookupStream_chunk();
  }
  public function createLocalStream_chunk()
  {
      $this->m_cron_job->createLocalStream_chunk();
  }
  public function createGetordersBatchFile()
  {
      $this->m_cron_job->createGetordersBatchFile();
  }
  public function createCategoryStream_chunk()
  {
      $this->m_cron_job->createCategoryStream_chunk();
  }
  public function updateRSSFeed_chunk()
  {
      $this->m_cron_job->updateRSSFeed_chunk();
  }
  public function runPurchasedItemChunk()
  {
    $this->load->library('rssparser');
    $this->m_cron_job->runPurchasedItemChunk();
    echo "Feed Updated".PHP_EOL;
  }
  public function runLookupStreamChunk($min_url_id,$max_url_id)
  {
    if(!empty($min_url_id) && !empty($max_url_id)){
      $this->load->library('rssparser');
      $this->m_cron_job->runLookupStreamChunk($min_url_id,$max_url_id);
      echo $min_url_id.'-'.$max_url_id ."-Feed Updated".PHP_EOL;
      $this->runLookupStreamChunk($min_url_id,$max_url_id);
     }else{
       echo 'feed_url_id not found';
     }
  }
  public function runAutoBuyStream()
  {
      $this->load->library('rssparser');
      $this->m_cron_job->runAutoBuyStream();
      echo "Feed Updated".PHP_EOL;
      $this->runAutoBuyStream();
  }
  public function runAutoBINStream()
  {
      $this->load->library('rssparser');
      $this->m_cron_job->runAutoBINStream();
      echo "Feed Updated".PHP_EOL;
      $this->runAutoBINStream();
  }
  public function runAutoBuyAuctionStream()
  {
      $this->load->library('rssparser');
      $this->m_cron_job->runAutoBuyAuctionStream();
      echo "Feed Updated".PHP_EOL;
      $this->runAutoBuyAuctionStream();
  }
  public function runCatAuctionStream()
  {
      $this->load->library('rssparser');
      $this->m_cron_job->runCatAuctionStream();
      echo "Feed Updated".PHP_EOL;
      $this->runCatAuctionStream();
  }
  public function runFeedChunktest($min_url_id,$max_url_id)
  {
    if(!empty($min_url_id) && !empty($max_url_id)){
      $this->load->library('rssparser');
      $this->m_cron_job->runFeedChunktest($min_url_id,$max_url_id);
      echo $min_url_id.'-'.$max_url_id ."-Feed Updated".PHP_EOL;
      $this->runFeedChunk($min_url_id,$max_url_id);
     }else{
       echo 'feed_url_id not found';
     }
  }
  //     public function runLocalFeedChunk($min_url_id,$max_url_id)
  // {
  //   if(!empty($min_url_id) && !empty($max_url_id)){
  //     $this->load->library('rssparser');
  //     $this->m_cron_job->runLocalFeedChunk($min_url_id,$max_url_id);
  //     echo $min_url_id.'-'.$max_url_id ."-Feed Updated".PHP_EOL;
  //     $this->runLocalFeedChunk($min_url_id,$max_url_id);
  //    }else{
  //      echo 'feed_url_id not found';
  //    }
  // }
  public function runCategoryStream($cat_id = '')
  {
    if(!empty($cat_id)){
      $this->load->library('rssparser');
      $this->m_cron_job->runCategoryStream($cat_id);
      echo $cat_id ."-Feed Updated".PHP_EOL;
      $this->runCategoryStream($cat_id);
    }else{
      echo 'category_id not found';
    }
  }
  public function runLocalStreamChunk($min_url_id,$max_url_id)
  {
    if(!empty($min_url_id) && !empty($max_url_id)){
      $this->load->library('rssparser');
      $this->m_cron_job->runLocalStreamChunk($min_url_id,$max_url_id);
      echo $min_url_id.'-'.$max_url_id ."-Feed Updated".PHP_EOL;
      $this->runLocalStreamChunk($min_url_id,$max_url_id);
     }else{
       echo 'feed_url_id not found';
     }
  }
  public function getSoldActiveSummary()
  {
    $result['data']=$this->m_cron_job->getSoldActiveSummary();
    $this->load->view('API\get_avg_sold_price',$result);

  }
  public function getSoldActiveSummaryAll()
  {
    $result['data']=$this->m_cron_job->getSoldActiveSummaryAll();
    $this->load->view('API\get_avg_sold_price',$result);

  }
  public function GetMostWatchedItems()
  {

    $result['data']=$this->m_cron_job->GetMostWatchedItems();
    $this->load->view('API\GetMostWatchedItems',$result);

  }
  public function GetEpid($user_id)
  {
    $result['user_id'] = $user_id;
    $this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    $result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/browse/01-search-for-items',$result);

  }
  public function GetEbayCatalogue()
  {

    $result['data']=$this->m_cron_job->GetEbayCatalogue();
    $this->load->view('ebay/catalog/02-getProduct',$result);

  }
  public function updateUserLog()
  {

    $this->m_cron_job->updateUserLog();
    echo "USER LOG UPDATED SUCCESSFULLY".PHP_EOL;
    //$this->load->view('ebay/catalog/02-getProduct',$result);

  }
  public function getAuctionItemPrice()
  {
    $result['data']=$this->m_cron_job->getAuctionItemPrice();
    $this->load->view('API\getAuctionItemPrice',$result);

  } 
  public function uploadBbyfile()
  {
    $this->m_cron_job->uploadBbyfile();
  }
  public function GetCategoryFeatures()
  {
    $result['data'] = $this->m_cron_job->GetCategoryFeatures();
    $this->load->view('ebay/trading/GetCategoryFeatures',$result);
  }
  public function endItemAging()
  {
    $this->load->view('ebay/trading/endItemAging');
  }
  public function GetActiveItemEpid($user_id)
  {
    //var_dump($user_id);exit;
    $result['user_id'] = $user_id;
    $this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    $result['res']=$this->m_cron_job->GetActiveItemEpid($user_id);
    $this->load->view('ebay/browse/getEpid',$result);
    //$this->load->view('ebay/browse/getEpid');

  }
  public function assignEpid($user_id)
  {
    //var_dump($user_id);exit;
    $result['user_id'] = $user_id;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    $result['seed_data']=$this->m_cron_job->assignEpid();
    $this->load->view('ebay/trading/AssignEpidtoActiveListing',$result);
    //$this->load->view('ebay/browse/getEpid');

  }
  public function createCampaign($user_id)
  {
    //var_dump($user_id);exit;
    $result['user_id'] = $user_id;
    $this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$this->load->view('ebay\oauth-tokens\02-get-user-token',$result);
   // $result['seed_data']=$this->m_cron_job->assignEpid();
    //$this->load->view('ebay/trading/AssignEpidtoActiveListing',$result);
    $this->load->view('ebay/Marketing/createCampaign',$result);
    //$this->load->view('ebay/browse/getEpid');

  }
  public function CreateAdByListingID($user_id)
  {
    //var_dump($user_id);exit;
    $result['user_id'] = $user_id;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    $this->load->view('ebay\oauth-tokens\02-get-user-token',$result);
   // $result['seed_data']=$this->m_cron_job->assignEpid();
    //$this->load->view('ebay/trading/AssignEpidtoActiveListing',$result);
    $this->load->view('ebay/Marketing/CreateAdByListingID',$result);
    //$this->load->view('ebay/browse/getEpid');

  }
  public function getCampaigns($user_id)
  {
    //var_dump($user_id);exit;
    $result['user_id'] = $user_id;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$this->load->view('ebay\oauth-tokens\02-get-user-token',$result);
   // $result['seed_data']=$this->m_cron_job->assignEpid();
    //$this->load->view('ebay/trading/AssignEpidtoActiveListing',$result);
    $this->load->view('ebay/Marketing/getCampaigns',$result);
    //$this->load->view('ebay/browse/getEpid');

  }
 
  public function completeSale()
  {
    
    $result['transaction_id'] = $_GET['transaction_id'];
    $result['item_id'] = $_GET['item_id'];
    $result['tracking_number'] = $_GET['tracking_number'];
    $result['acc_id'] = $_GET['acc_id'];
    $result['user_id'] = $_GET['user_id'];
    $result['order_id'] = $_GET['order_id'];
    $result['carrier_name'] = $_GET['carrier_name'];
    //updated by tahir => 8/23/19
    $order_id = $_GET['order_id'];
    $shippingLabelRate = $_GET['shippingLabelRate'];
    $trackingNo = $_GET['tracking_number'];
    $trans_id = $_GET['transaction_id'];
    $acc_id  =$_GET['acc_id'];
    $user_id = $_GET['user_id'];
    // $carrier_name = $_GET['carrier_name'];
    
    

$this->db->query("INSERT INTO TABLE_TRIGGER_LOG (TRIGGER_LOG_ID,TABLE_NAME,MESSAGE,STATUS,CREATED_AT,CREATED_BY,ERROR_MESSAGE,ERROR_CODE,TRANSACTION_ID) VALUES (GET_SINGLE_PRIMARY_KEY('TABLE_TRIGGER_LOG','TRIGGER_LOG_ID'),'OP_CALL_STATUS','".$trackingNo."',2,SYSDATE,".$user_id.",'Before Api Call Account ID is: $acc_id','".$order_id."','".$trans_id."') ");
    //end 
    $data = $this->load->view('API\GetOrders\CompleteSale',$result,true);
    $trackingUpdated =  json_encode( json_decode(htmlspecialchars_decode($data), true));
                        // $jason = json_decode($trackingUpdated,true); for making it as Array
                        $jason = json_decode($trackingUpdated); // For making it as an Object
                        echo json_encode($jason);
                        return json_encode($jason);
  }
  public function getAwaitingShipments($user_id)
  {
    $result['user_id'] = $user_id;
    $this->load->view('ebay/trading/getAwaitingShipments',$result);
  }
  public function getItemSeed($account_id,$user_id)
  {
    //$result['user_id'] = $user_id;
    $result['account_id'] = $account_id;
    $result['user_id'] = $user_id;
    $this->load->view('ebay/trading/getitem_seed',$result);
  }
  public function getAwaitingShipmentsDetails($user_id)
  {
    $result['user_id'] = $user_id;
    $this->load->view('API\GetOrders\getAwaitingShipmentsDetails',$result);

  }
  public function shopifyOrdersReviseCall(){
    $this->m_ordersShopify->reviseQuantityonShopify();
  }
  public function getSoldItems($user_id){
    $result['user_id'] = $user_id;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    $result['data']=$this->m_cron_job->getSoldItems();
    $this->load->view('ebay/finding/getSoldItems',$result);
  }
  public function getActiveItems($user_id){
    $result['user_id'] = $user_id;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    $result['data']=$this->m_cron_job->getSoldItems();
    $this->load->view('ebay/finding/getActiveItems',$result);
  }
  public function findItems($user_id)
  {
    $result['user_id'] = $user_id;
    $this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/browse/findItems',$result);

  }
  public function searchByImage($user_id)
  {
    $result['user_id'] = $user_id;
    $this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/browse/searchByImage',$result);

  }
  public function soldDataBatch_01($user_id)
  {
    $result['user_id'] = $user_id;
    $this->load->view('ebay/async/auto_run_sold',$result);

  }
  public function soldDataBatch_02($user_id)
  {
    $result['user_id'] = $user_id;
    $this->load->view('ebay/async/auto_run_sold_40004_100003',$result);

  }
  public function soldDataBatch_03($user_id)
  {
    $result['user_id'] = $user_id;
    $this->load->view('ebay/async/auto_run_sold_10004_184770',$result);

  }
  public function localSoldData($user_id)
  {
    $result['user_id'] = $user_id;
    $this->load->view('ebay/async/localSoldData',$result);

  }
  public function pullOrders(){
    $data['Timestamp'] = $_GET['Timestamp'];
    $data['merchant_name'] = $_GET['merchantName'];
    $data['NotificationEventName'] = $_GET['NotificationEventName'];
    $data['ItemID'] = $_GET['ItemID'];
    $this->load->view('API/GetOrders/GetOrders_notification',$data);
  }
  public function GetSellerOrders($sellerId)
  {
    $result['sellerId']=$sellerId;
    $result['Days']=3;
    $this->load->view('API/GetOrders/GetSellerOrders',$result);
  }
  public function downloadOrders($sellerId)
  {
    //$sellerId = $this->input->post("accountId");
    //var_dump($sellerId);exit;
    $result['sellerId']=$sellerId;
    $result['Days']=3;
    $this->load->view('API/GetOrders/GetSellerOrders',$result);
    
  }
  public function searchReturn($user_id)
  {
    //$result['user_id'] = $user_id;
    $result['merchant_id'] = $merchant_id;
    $result['returnCall'] = 1;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/postOrder/01-searchReturn',$result);

  }
  public function searchReturnWeb()
  {
    //$user_id = $this->input->post("user_id");
    $merchant_id = $this->input->post("merchant_id");

    //$result['user_id'] = $user_id;
    $result['merchant_id'] = $merchant_id;
    $result['returnCall'] = 1;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/postOrder/01-searchReturn',$result);

  }

  public function decideReturn($user_id)
  {
    $result['user_id'] = $user_id;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/postOrder/02-DecideReturn',$result);

  }
  public function get_pic($upc,$part_no){
//$upc = '678149439229';
//$part_no = '43922-DVD';
  $dekitted_pics = [];
  $parts = [];
  $uri = [];

      $query = $this->db->query("SELECT *
  FROM (SELECT S.LZ_MANIFEST_ID,
               S.ITEM_TITLE ITEM_MT_DESC,
               S.DEFAULT_COND,
               S.F_MPN ITEM_MT_MFG_PART_NO,
               S.F_UPC ITEM_MT_UPC,
               S.F_MANUFACTURE ITEM_MT_MANUFACTURE,
               S.CATEGORY_ID E_BAY_CATA_ID_SEG6,
               '' MAIN_CATAGORY_SEG1,
               '' SUB_CATAGORY_SEG2,
               S.CATEGORY_NAME BRAND_SEG3,
               'US' ORIGIN_SEG4,
               S.SHIPPING_SERVICE,
               C.COND_NAME CONDITIONS_SEG5
          FROM LZ_ITEM_SEED S , LZ_ITEM_COND_MT C
         WHERE (S.F_UPC = '$upc' OR upper(s.f_mpn) = upper('$part_no'))
         AND C.ID = S.DEFAULT_COND
         ORDER BY S.SEED_ID DESC)
WHERE ROWNUM = 1"); 

    if($query->num_rows() === 0){
    $query = $this->db->query("SELECT LZ_MANIFEST_ID,
       ITEM_MT_DESC,
       CONDITIONS_SEG5,
       ITEM_MT_MFG_PART_NO,
       ITEM_MT_UPC,
       ITEM_MT_MANUFACTURE,
       ITEM_MT_BBY_SKU,
       E_BAY_CATA_ID_SEG6,
       MAIN_CATAGORY_SEG1,
       SUB_CATAGORY_SEG2,
       BRAND_SEG3,
       ORIGIN_SEG4,
       '' SHIPPING_SERVICE
  FROM (SELECT LZ_MANIFEST_ID,
       ITEM_MT_DESC,
       trim(ITEM_MT_MFG_PART_NO) ITEM_MT_MFG_PART_NO,
       trim(ITEM_MT_UPC) ITEM_MT_UPC,
       ITEM_MT_MANUFACTURE,
       ITEM_MT_BBY_SKU,
       E_BAY_CATA_ID_SEG6,
       MAIN_CATAGORY_SEG1,
       SUB_CATAGORY_SEG2,
       BRAND_SEG3,
       ORIGIN_SEG4,
       NVL(C.COND_NAME, DT.E_BAY_CATA_ID_SEG6) CONDITIONS_SEG5
          FROM LZ_MANIFEST_DET DT , lz_item_cond_mt c
         WHERE (DT.ITEM_MT_UPC = '$upc' or dt.item_mt_mfg_part_no = upper('$part_no'))
           AND DT.E_BAY_CATA_ID_SEG6 NOT IN
               ('N/A', 'other', 'Other', 'OTHER', '123456')
               AND C.ID = DT.CONDITIONS_SEG5(+)
           AND DT.E_BAY_CATA_ID_SEG6 IS NOT NULL
           AND DT.LAPTOP_ITEM_CODE IS NOT NULL
         ORDER BY DT.LZ_MANIFEST_ID DESC)
 WHERE ROWNUM = 1"); 
    }
    // $query = $this->db->query("SELECT * FROM (SELECT DTT.LZ_MANIFEST_ID, C.COND_NAME CONDITIONS_SEG5, DTT.F_MPN ITEM_MT_MFG_PART_NO, TRIM(DTT.F_UPC) ITEM_MT_UPC, DTT.F_MANUFACTURE ITEM_MT_MANUFACTURE, '' ITEM_MT_BBY_SKU, DTT.CATEGORY_ID E_BAY_CATA_ID_SEG6 FROM LZ_ITEM_SEED DTT,LZ_ITEM_COND_MT C WHERE DTT.DEFAULT_COND = C.ID(+) AND UPPER(DTT.F_UPC) = '$upc' AND   DTT.CATEGORY_ID IS NOT NULL ORDER BY DTT.SEED_ID DESC) WHERE ROWNUM =1 ");
    //$data = $query->result();

    if($query->num_rows()>0){
      $manifest_id = $query->result_array();
      //var_dump($purch_ref);exit;
      //$lz_manifest_id = $manifest_id[0]['LZ_MANIFEST_ID'];
      $condid = $manifest_id[0]['CONDITIONS_SEG5'];

      $item_mt_mfg_part_no = str_replace('/', '_', @$manifest_id[0]['ITEM_MT_MFG_PART_NO']);
      

      $item_mt_upc = $manifest_id[0]['ITEM_MT_UPC'];
    //picture code start

    $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
    $path = $path->result_array();    
    $master_path = $path[0]["MASTER_PATH"];

    //$master_path.@$row['UPC'].'~'.$mpn.'/'.@$it_condition
    $dir1 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no;
    $dir2 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid;
    $dir3 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';
    $dir = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';
    // if(!empty($item_mt_upc) && !empty($item_mt_mfg_part_no)){
    //  $dir = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';

    // }elseif(empty($item_mt_upc) && !empty($item_mt_mfg_part_no)){
    //  $dir = $master_path."~".@$item_mt_mfg_part_no."/".@$condid.'/thumb/';

    // }elseif(!empty($item_mt_upc) && empty($item_mt_mfg_part_no)){
    //  $dir = $master_path.@$item_mt_upc."~/".@$condid.'/thumb/';
    // }

    $dir = preg_replace("/[\r\n]*/","",$dir);

  
    $dekitted_pics = [];
    $parts = [];
    $uri = [];
    if (is_dir($dir)) {


    }else{  // end if (is_dir($dir)) {
      $dekit = $this->db->query("SELECT *
                    FROM (SELECT L.FOLDER_NAME
                            FROM LZ_SPECIAL_LOTS L
                           WHERE (L.CARD_UPC = '$upc' OR
                                 UPPER(L.CARD_MPN) = UPPER('$part_no'))
                             AND L.FOLDER_NAME IS NOT NULL
                           ORDER BY L.SPECIAL_LOT_ID DESC)
                   WHERE ROWNUM = 1")->result_array();
      if(count($dekit)>0){
        $folder_name = $dekit[0]['FOLDER_NAME'];
        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();    
        $master_path = $path[0]["MASTER_PATH"];
        //$master_path.@$row['UPC'].'~'.$mpn.'/'.@$it_condition
        $dir = $master_path.$folder_name.'/thumb/';
        $src1 = $master_path.$folder_name;
        $src2 = $master_path.$folder_name.'/thumb/';
        @mkdir($dir1); 
        @mkdir($dir2); 
        @mkdir($dir3); 
        $this->custom_copy($src1,$dir2);
        $this->custom_copy($src2,$dir3);
      }
    }
      if(is_dir($dir)){
        $images = glob($dir."\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}",GLOB_BRACE);
        $i=0 ;
        
        foreach($images as $image) {

          $uri[$i] = $image;

          $parts = explode(".", $image);
          $img_name = explode("/",$image);

          $img_n = explode(".",$img_name[4]);
          $str = preg_replace('/[^A-Za-z0-9\. -]/', '', $img_name[4]);
          $new_string = substr($str,0,1) . "_" . substr($str,1,strlen($str)-1);

                if (is_array($parts) && count($parts) > 1){
                    $extension = end($parts);
                    if(!empty($extension)){

                      if(count($parts)>2){
                      $img_concat = '';

                      for($k=0; $k<count($parts)-1; $k++){
                        if($k==0){
                          $img_concat .= $parts[$k];

                        }else{
                          $img_concat .='.'. $parts[$k];

                        }
                        
                      }

                      $url = $img_concat.'.'.$extension;
                      }
                      else{
                       $url = $parts['0'].'.'.$extension;
                      }

                        $url = preg_replace("/[\r\n]*/","",$url);
                         //var_dump($url);exit;
                        $uri[$i] = $url;
                        $img = file_get_contents($url);
                         //var_dump($url);exit;
                        $img =base64_encode($img);
                        $dekitted_pics[$i] = $img;
                         $i++;
                      }//if(!empty($extension)){
                  }//if (is_array($parts) && count($parts) > 1){
          }//foreach($images as $image) {
      }else{//if(is_dir($dir)){
        $object_query = 0;
        $manifest_id = 0;
        $weight_qry = 0;
      }
      //picture code end

    }else{//if($query->num_rows()>0){
      $object_query = 0;
      $manifest_id = 0;
      $weight_qry = 0;
    }
    //picture code end
  
    return array('dekitted_pics'=>$dekitted_pics,'parts'=>$parts,'uri'=>$uri);
} 
public function get_img($upc,$part_no,$cond){
//$upc = '678149439229';
//$part_no = '43922-DVD';
      $query = $this->db->query("SELECT * FROM (SELECT S.LZ_MANIFEST_ID, S.ITEM_TITLE ITEM_MT_DESC, S.DEFAULT_COND, S.F_MPN ITEM_MT_MFG_PART_NO, S.F_UPC ITEM_MT_UPC, S.F_MANUFACTURE ITEM_MT_MANUFACTURE, S.CATEGORY_ID E_BAY_CATA_ID_SEG6, '' MAIN_CATAGORY_SEG1, '' SUB_CATAGORY_SEG2, S.CATEGORY_NAME BRAND_SEG3, 'US' ORIGIN_SEG4, S.SHIPPING_SERVICE, C.COND_NAME CONDITIONS_SEG5 FROM LZ_ITEM_SEED S , LZ_ITEM_COND_MT C WHERE (S.F_UPC = '$upc' OR upper(s.f_mpn) = upper('$part_no')) AND C.ID = S.DEFAULT_COND AND S.DEFAULT_COND = '$cond' ORDER BY S.SEED_ID DESC) WHERE ROWNUM = 1");
    if($query->num_rows() === 0){
      $query = $this->db->query("SELECT LZ_MANIFEST_ID, ITEM_MT_DESC, CONDITIONS_SEG5, ITEM_MT_MFG_PART_NO, ITEM_MT_UPC, ITEM_MT_MANUFACTURE, ITEM_MT_BBY_SKU, E_BAY_CATA_ID_SEG6, MAIN_CATAGORY_SEG1, SUB_CATAGORY_SEG2, BRAND_SEG3, ORIGIN_SEG4, '' SHIPPING_SERVICE FROM (SELECT LZ_MANIFEST_ID, ITEM_MT_DESC, trim(ITEM_MT_MFG_PART_NO) ITEM_MT_MFG_PART_NO, trim(ITEM_MT_UPC) ITEM_MT_UPC, ITEM_MT_MANUFACTURE, ITEM_MT_BBY_SKU, E_BAY_CATA_ID_SEG6, MAIN_CATAGORY_SEG1, SUB_CATAGORY_SEG2, BRAND_SEG3, ORIGIN_SEG4, NVL(C.COND_NAME, DT.E_BAY_CATA_ID_SEG6) CONDITIONS_SEG5 FROM LZ_MANIFEST_DET DT , lz_item_cond_mt c WHERE (DT.ITEM_MT_UPC = '$upc' or dt.item_mt_mfg_part_no = upper('$part_no')) AND DT.E_BAY_CATA_ID_SEG6 NOT IN ('N/A', 'other', 'Other', 'OTHER', '123456') AND C.ID = DT.CONDITIONS_SEG5(+) AND C.ID = '$cond' AND DT.E_BAY_CATA_ID_SEG6 IS NOT NULL AND DT.LAPTOP_ITEM_CODE IS NOT NULL ORDER BY DT.LZ_MANIFEST_ID DESC) WHERE ROWNUM = 1"); 
    }

    if($query->num_rows()>0){
      $manifest_id = $query->result_array();
      $condid = $manifest_id[0]['CONDITIONS_SEG5'];

      $item_mt_mfg_part_no = str_replace('/', '_', @$manifest_id[0]['ITEM_MT_MFG_PART_NO']);
      

      $item_mt_upc = $manifest_id[0]['ITEM_MT_UPC'];
    //picture code start

    $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
    $path = $path->result_array();    
    $master_path = $path[0]["MASTER_PATH"];

    $dir1 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no;
    $dir2 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid;
    $dir3 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';
    $dir = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';

    $dir = preg_replace("/[\r\n]*/","",$dir);
    if (is_dir($dir)) {


    }else{  // end if (is_dir($dir)) {
      $dekit = $this->db->query("SELECT *
                    FROM (SELECT L.FOLDER_NAME
                            FROM LZ_SPECIAL_LOTS L
                           WHERE (L.CARD_UPC = '$upc' OR
                                 UPPER(L.CARD_MPN) = UPPER('$part_no'))
                             AND L.FOLDER_NAME IS NOT NULL
                             AND L.CONDITION_ID = '$cond'
                           ORDER BY L.SPECIAL_LOT_ID DESC)
                   WHERE ROWNUM = 1")->result_array();
      if(count($dekit)>0){
        $folder_name = $dekit[0]['FOLDER_NAME'];
        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();    
        $master_path = $path[0]["MASTER_PATH"];
        //$master_path.@$row['UPC'].'~'.$mpn.'/'.@$it_condition
        $dir = $master_path.$folder_name.'/thumb/';
        $src1 = $master_path.$folder_name;
        $src2 = $master_path.$folder_name.'/thumb/';
        @mkdir($dir1); 
        @mkdir($dir2); 
        @mkdir($dir3); 
        $this->custom_copy($src1,$dir2);
        $this->custom_copy($src2,$dir3);
      }
    }
      

    }else{//if($query->num_rows()>0){
     echo "No histroy found";
    }
    //picture code end
  
    //return array('dekitted_pics'=>$dekitted_pics,'parts'=>$parts,'uri'=>$uri);
} 
function custom_copy($src, $dst) {  
  
    // open the source directory 
    $dir = opendir($src);  
  
    // Make the destination directory if not exist 
    @mkdir($dst);  
  
    // Loop through the files in source directory 
    while( $file = readdir($dir) ) {  
  
        if (( $file != '.' ) && ( $file != '..' )) {  
            if ( is_dir($src . '/' . $file) )  
            {  
  
                // Recursively calling custom copy function 
                // for sub directory  
                $this->custom_copy($src . '/' . $file, $dst . '/' . $file);  
  
            }  
            else {  
                copy($src . '/' . $file, $dst . '/' . $file);  
            }  
        }  
    }  
  
    closedir($dir); 
}
public function insertMasterImgURLinDB()//FOR master dir PIC ONLY
    {
        $path_id = 1;
        $query = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = '$path_id'");
        $master_qry = $query->result_array();
        $base_url = $master_qry[0]['MASTER_PATH'];
        $directories = glob($base_url . '*', GLOB_ONLYDIR);
        foreach ($directories as $sub_dir) {
            $sub_directories = glob($sub_dir . '/*', GLOB_ONLYDIR);
            $folder_name = basename($sub_dir);
            //$sub_dir ='D:/wamp/www/item_pictures/master_pictures/678149439229~43922-DVD/abc.jpg';
            //$pathinfo = date("m/d/Y H:i:s", fileatime($sub_dir));
            //$pathinfo = pathinfo($sub_dir,PATHINFO_FILENAME );
            //$pathinfo = pathinfo($sub_dir,PATHINFO_EXTENSION  );
            //$fileatime = date("m/d/Y H:i:s", fileatime($sub_dir));
            //$filesize = filesize($file_dir); // bytes
            //$filetype = filetype($sub_dir);// dir or file
            //$is_file = is_file($sub_dir); // return bool
            //var_dump($pathinfo);exit;
            foreach ($sub_directories as $dir) {

            if (is_dir($dir)) {
                $condition_name = basename($dir);
                $folder_dir = $folder_name.'/'.$condition_name;
                echo $folder_dir.PHP_EOL;
                //var_dump($folder_dir);//exit;
                /*========================================================
                =            insert data in lz_barcode_pic_mt            =
                ========================================================*/
                $check_mt_qry = $this->db->query("SELECT IMG_MT_ID FROM LJ_BARCODE_PIC_MT WHERE FOLDER_NAME = '$folder_dir'")->result_array();
                if(count($check_mt_qry) === 0){
                    $get_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_BARCODE_PIC_MT','IMG_MT_ID') IMG_MT_ID FROM DUAL")->result_array();
                    $img_mt_id = $get_pk[0]['IMG_MT_ID'];
                    $this->db->query("INSERT INTO LJ_BARCODE_PIC_MT
                                      (IMG_MT_ID, FOLDER_NAME, INSERTED_BY, INSERTED_DATE, BASE_URL)
                                    values
                                      ('$img_mt_id', '$folder_dir', 81, SYSDATE, '$path_id')");
                }else{
                    $img_mt_id = $check_mt_qry[0]['IMG_MT_ID'];
                }
                /*=====  End of insert data in lz_barcode_pic_mt  ======*/
                if ($dh = opendir($dir)) {
                  //var_dump(readdir($dh));exit;
                  $i=0;
                  while (($image_name = readdir($dh)) !== false) {
                      if (is_file($dir . '/' . $image_name)) {
                          $i++;
                              $local_url = $dir . '/' . $image_name;
                              /*========================================================
                              =            insert data in lj_barcode_pic_dt            =
                              ========================================================*/
                                  $check_dt_qry = $this->db->query("SELECT IMG_DT_ID FROM LJ_BARCODE_PIC_DT WHERE LOCAL_URL = '$local_url'")->result_array();
                                  if(count($check_dt_qry) === 0){
                                      
                                      $get_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_BARCODE_PIC_DT','IMG_DT_ID') IMG_DT_ID FROM DUAL")->result_array();
                                      $img_dt_id = $get_pk[0]['IMG_DT_ID'];

                                      $this->db->query("INSERT INTO LJ_BARCODE_PIC_DT
                                                        (IMG_DT_ID,
                                                         IMG_MT_ID,
                                                         LOCAL_URL,
                                                         PIC_ORDER,
                                                         INSERTED_BY,
                                                         INSERTED_DATE,
                                                         IMAGE_NAME)
                                                      values
                                                        ('$img_dt_id',
                                                         '$img_mt_id',
                                                         '$local_url',
                                                         '$i',
                                                         81,
                                                         SYSDATE,
                                                         '$image_name')");

                                  }else{
                                      $img_dt_id = $check_dt_qry[0]['IMG_DT_ID'];
                                      $this->db->query("UPDATE LJ_BARCODE_PIC_DT SET
                                                         IMG_MT_ID='$img_mt_id',
                                                         LOCAL_URL='$local_url',
                                                         PIC_ORDER='$i',
                                                         INSERTED_BY =81,
                                                         INSERTED_DATE = SYSDATE,
                                                         IMAGE_NAME = '$image_name'
                                                         where img_dt_id = '$img_dt_id'");

                                  }
                              /*=====  End of insert data in lj_barcode_pic_dt  ======*/
                      }//if (is_file($dir . '/' . $image_name)) {
                  } //while close
                  closedir($dh);
                } //opendir if ($dh = opendir($dir)) {
            }//if (is_dir($dir)) {
            //}//foreach ($leaf_dir as $dir) {
            }//foreach ($sub_directories as $dir) {
        } //foreach ($directories as $dir) {
    }
    public function insertDekitImgURLinDB()//FOR DEKITTED PIC ONLY
    {
        $path_id = 2;
        $query = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = '$path_id'");
        $master_qry = $query->result_array();
        $base_url = $master_qry[0]['MASTER_PATH'];
        $directories = glob($base_url . '*', GLOB_ONLYDIR);
        foreach ($directories as $dir) {
          if (is_dir($dir)) {
              $folder_name = basename($dir);
              echo $folder_name.PHP_EOL;
              /*========================================================
              =            insert data in lz_barcode_pic_mt            =
              ========================================================*/
              $check_mt_qry = $this->db->query("SELECT IMG_MT_ID FROM LJ_BARCODE_PIC_MT WHERE FOLDER_NAME = '$folder_name'")->result_array();
              if(count($check_mt_qry) === 0){
                  $get_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_BARCODE_PIC_MT','IMG_MT_ID') IMG_MT_ID FROM DUAL")->result_array();
                  $img_mt_id = $get_pk[0]['IMG_MT_ID'];
                  $this->db->query("INSERT INTO LJ_BARCODE_PIC_MT
                                    (IMG_MT_ID, FOLDER_NAME, INSERTED_BY, INSERTED_DATE, BASE_URL)
                                  values
                                    ('$img_mt_id', '$folder_name', 81, SYSDATE, '$path_id')");
              }else{
                  $img_mt_id = $check_mt_qry[0]['IMG_MT_ID'];
              }
              /*=====  End of insert data in lz_barcode_pic_mt  ======*/
              if ($dh = opendir($dir)) {
                  //var_dump(readdir($dh));exit;
                $i=0;
                while (($image_name = readdir($dh)) !== false) {
                    if (is_file($dir . '/' . $image_name)) {
                        $i++;
                            $local_url = $dir . '/' . $image_name;
                            /*========================================================
                            =            insert data in lj_barcode_pic_dt            =
                            ========================================================*/
                                $check_dt_qry = $this->db->query("SELECT IMG_DT_ID FROM LJ_BARCODE_PIC_DT WHERE LOCAL_URL = '$local_url'")->result_array();
                                if(count($check_dt_qry) === 0){
                                    
                                    $get_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_BARCODE_PIC_DT','IMG_DT_ID') IMG_DT_ID FROM DUAL")->result_array();
                                    $img_dt_id = $get_pk[0]['IMG_DT_ID'];

                                    $this->db->query("INSERT INTO LJ_BARCODE_PIC_DT
                                                      (IMG_DT_ID,
                                                       IMG_MT_ID,
                                                       LOCAL_URL,
                                                       PIC_ORDER,
                                                       INSERTED_BY,
                                                       INSERTED_DATE,
                                                       IMAGE_NAME)
                                                    values
                                                      ('$img_dt_id',
                                                       '$img_mt_id',
                                                       '$local_url',
                                                       '$i',
                                                       81,
                                                       SYSDATE,
                                                       '$image_name')");

                                }else{
                                    $img_dt_id = $check_dt_qry[0]['IMG_DT_ID'];
                                    $this->db->query("UPDATE LJ_BARCODE_PIC_DT SET
                                                       IMG_MT_ID='$img_mt_id',
                                                       LOCAL_URL='$local_url',
                                                       PIC_ORDER='$i',
                                                       INSERTED_BY =81,
                                                       INSERTED_DATE = SYSDATE,
                                                       IMAGE_NAME = '$image_name'
                                                       where img_dt_id = '$img_dt_id'");

                                }
                            /*=====  End of insert data in lj_barcode_pic_dt  ======*/
                    }//if (is_file($dir . '/' . $image_name)) {
                } //while close
                closedir($dh);
              } //opendir if ($dh = opendir($dir)) {
          }//if (is_dir($dir)) {
        } //foreach ($directories as $dir) {
    }
  public function test_qry(){//FOR testing purpose
    // $this->db->query("UPDATE LZ_BARCODE_MT B SET B.ADMIN_AT      = SYSDATE, B.ADMIN_ID      = '2', B.ADMIN_REMARKS = 'test'WHERE B.BARCODE_NO = '1223032'");
    // if($this->db->affected_rows() === 0){
    //   echo "error";
    //   }else{
    //     echo "SUCCESSFULLY";
    //   }
      $data['account_id'] = 12;
      $data['ebay_id'] = '174207768796';
      $this->load->view('ebay/trading/getItemQty',$data);
  }
  public function GetBestOffers()
  {
    //$result['account_name'] = 'dfwonline';
    $result['account_name'] = '2';
    $result['site_id'] = 0;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/trading/GetBestOffers',$result);

  }
  public function endItems()
  {
    //$result['account_name'] = 'dfwonline';
    $result['account_name'] = '2';
    $result['site_id'] = 0;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/trading/endItems',$result);

  }
  public function RelistItem()
  {
    //$result['account_name'] = 'dfwonline';
    $result['account_name'] = '2';
    $result['site_id'] = 0;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/trading/RelistItem',$result);

  }
  public function RespondToBestOffer()
  { 
    // echo "here is tge ";
    // $res  = array('ack'=>false,"message" => 'CounterOfferPrice and CounterOfferQuantity is Mandatory');
    // echo json_encode($res);
    //     return json_encode($res);
    //  exit();
    $action = $this->input->post('action');
    $BestOfferID = $this->input->post('BestOfferID');
    $sellerresponse = $this->input->post('sellerresponse');
    $counterofferprice = $this->input->post('counterofferprice');
    $counterofferquantity = $this->input->post('counterofferquantity');
    $user_id = $this->input->post('user_id');
    if(empty($action)){
      $res  = array('ack'=>'Failure','errorcode'=>'',"message" => 'Please Select a Action');
      echo json_encode($res);
        return json_encode($res);
    }
    if($action === 'Counter'){
      if(empty($counterofferprice) OR empty($counterofferquantity)){
        $res  = array('ack'=>'Failure','errorcode'=>'',"message" => 'CounterOfferPrice and CounterOfferQuantity is Mandatory');
        echo json_encode($res);
        return json_encode($res);
      }
    }
    

    $data = $this->m_cron_job->RespondToBestOffer($BestOfferID);
    $result['account_name'] = $data[0]['ACCOUNT_ID'];
    $result['site_id'] = $data[0]['SITE_ID'];
    $result['action'] = $action;//Accept , Counter , Decline
    $result['BestOfferID'] = $BestOfferID;
    $result['ItemID'] = $data[0]['EBAY_ID'];
    $result['CounterOfferPrice'] = $counterofferprice;
    $result['CounterOfferQuantity'] = $counterofferquantity;
    $result['SellerResponse'] = $sellerresponse;
    $result['user_id'] = $user_id;

    //$this->load->view('ebay/trading/RespondToBestOffer',$result);
    $dataa = $this->load->view('ebay/trading/RespondToBestOffer',$result,true);
    $res =  json_encode( json_decode(htmlspecialchars_decode($dataa), true));
    $json_res = json_decode($res); // For making it as an Object
    //var_dump($json_res->current_qty);exit;
    $res  = array('ack'=>$json_res->ack,'errorcode'=>$json_res->errorcode,"message" => $json_res->message);
    echo json_encode($res);
    return json_encode($res);
  }
  public function GetMyMessages()
  {
    //$result['account_name'] = 'dfwonline';
    $result['account_name'] = '2';
    $result['site_id'] = 0;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/trading/GetMyMessages',$result);

  }
  public function AddMemberMessageAAQToPartner()
  {
    //$result['account_name'] = 'dfwonline';
    $result['account_name'] = '2';
    $result['site_id'] = 0;

    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $dataa = $this->load->view('ebay/trading/AddMemberMessageAAQToPartner',$result,true);
    $res =  json_encode( json_decode(htmlspecialchars_decode($dataa), true));
    $json_res = json_decode($res); // For making it as an Object
    //var_dump($json_res->current_qty);exit;
    $res  = array('ack'=>$json_res->ack,'errorcode'=>$json_res->errorcode,"message" => $json_res->message);
    echo json_encode($res);
    return json_encode($res);

  }
  public function AddMemberMessageRTQ()
  {
    //$result['account_name'] = 'dfwonline';
    $ItemID = $this->input->post('ItemID');
    $MessageID = $this->input->post('MessageID');
    $ParentMessageID = $this->input->post('ParentMessageID');
    $replied_text = $this->input->post('replied_text');
    $RecipientID = $this->input->post('RecipientID');
    $SenderID = $this->input->post('SenderID');
    $Subject = $this->input->post('Subject');
    $replied_by = $this->input->post('replied_by');
    $images = @$_FILES['images'];

    $result['ItemID'] = $ItemID;
    $result['images'] = $images;
    $result['MessageID'] = $MessageID;
    $result['ParentMessageID'] = $ParentMessageID;
    $result['replied_text'] = $replied_text;
    $result['RecipientID'] = $RecipientID;
    $result['Subject'] = $Subject;
    $result['replied_by'] = $replied_by;
    $result['SenderID'] = $SenderID;
    $NotificationEventName = 'AddMemberMessageRTQ';
    $accountId = $this->getAccountId($RecipientID,$NotificationEventName);
    $result['account_name'] = $accountId;
    $result['site_id'] = 0;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    //$this->load->view('ebay/trading/AddMemberMessageRTQ',$result);
    $dataa = $this->load->view('ebay/trading/AddMemberMessageRTQ',$result,true);
    $res =  json_encode( json_decode(htmlspecialchars_decode($dataa), true));
    $json_res = json_decode($res); // For making it as an Object
    //var_dump($json_res->current_qty);exit;
    $res  = array('ack'=>$json_res->ack,'errorcode'=>$json_res->errorcode,"message" => $json_res->message);
    echo json_encode($res);
    return json_encode($res);

  }
  public function GetTokenStatus()
  {
    //$result['account_name'] = 'dfwonline';
    $result['account_name'] = '1';
    $result['site_id'] = 0;
    //$this->load->view('ebay\oauth-tokens\01-get-app-token',$result);
    //$result['res']=$this->m_cron_job->GetEpid($user_id);

    $this->load->view('ebay/trading/GetTokenStatus',$result);

  }
  public function GetMyMessagesResponse($response)
  {
    $NotificationEventName = (string) @$response->NotificationEventName;
    //var_dump($NotificationEventName);exit;
    $merchant_name = (string) @$response->RecipientUserID;
    $Timestamp = (string) @$response->Timestamp;
    $Timestamp = str_split($Timestamp,19);
    $Timestamp = str_replace('T'," ",$Timestamp[0]);
    $Timestamp = "TO_DATE('".$Timestamp."', 'YYYY-MM-DD HH24:MI:SS')";
    $Sender = (string) @$response->Messages->Message->Sender;
    $SendingUserID = (string) @$response->Messages->Message->SendingUserID;
    $RecipientUserID = (string) @$response->Messages->Message->RecipientUserID;
    $SendToName = (string) @$response->Messages->Message->SendToName;
    $Subject = (string) @$response->Messages->Message->Subject;
    $MessageID = (string) @$response->Messages->Message->MessageID;
    $ExternalMessageID = (string) @$response->Messages->Message->ExternalMessageID;
    $Text = @$response->Messages->Message->Text;
    $doc = new DomDocument();
    @$doc->loadHTML($Text);
    $msg_Text = $doc->getElementById('UserInputtedText')->nodeValue;
    $xpath = new DOMXPath($doc);
    $query = '//img[@class="product-image"]/@src';
    $entries = $xpath->query($query);
    $img_url = $entries[0]->nodeValue;
    //var_dump($entries[0]->nodeValue); exit;
    $Flagged = (string) @$response->Messages->Message->Flagged;
    $ReceiveDate = (string) @$response->Messages->Message->ReceiveDate;
    $ReceiveDate = str_split($ReceiveDate,19);
    $ReceiveDate = str_replace('T'," ",$ReceiveDate[0]);
    $ReceiveDate = "TO_DATE('".$ReceiveDate."', 'YYYY-MM-DD HH24:MI:SS')";
    $ExpirationDate = (string) @$response->Messages->Message->ExpirationDate;
    $ExpirationDate = str_split($ExpirationDate,19);
    $ExpirationDate = str_replace('T'," ",$ExpirationDate[0]);
    $ExpirationDate = "TO_DATE('".$ExpirationDate."', 'YYYY-MM-DD HH24:MI:SS')";
    $ItemID = (string) @$response->Messages->Message->ItemID;
    $ResponseEnabled = (string) @$response->Messages->Message->ResponseDetails->ResponseEnabled;
    $ResponseURL = (string) @$response->Messages->Message->ResponseDetails->ResponseURL;
    $FolderID = (string) @$response->Messages->Message->Folder->FolderID;
    //$Content = (string) @$response->Messages->Message->Content;
    $MessageType = (string) @$response->Messages->Message->MessageType;
    $Replied = (string) @$response->Messages->Message->Replied;
    $ItemEndTime = (string) @$response->Messages->Message->ItemEndTime;
    $ItemEndTime = str_split($ItemEndTime,19);
    $ItemEndTime = str_replace('T'," ",$ItemEndTime[0]);
    $ItemEndTime = "TO_DATE('".$ItemEndTime."', 'YYYY-MM-DD HH24:MI:SS')";
    $ItemTitle = (string) @$response->Messages->Message->ItemTitle;
   // var_dump(@$response->Timestamp,$Timestamp,@$response->Messages->Message->ReceiveDate,$ReceiveDate,@$response->Messages->Message->ExpirationDate,$ExpirationDate);
    $check_msg = $this->db->query("select LJ_MSG_ID from LJ_MSG_MT where MESSAGE_ID = '$MessageID'")->result_array();
    if(count($check_msg) === 0){
      $this->db->query("INSERT INTO LJ_MSG_MT (LJ_MSG_ID,
      MESSAGE_ID,
      EXTERNALMESSAGEID,
      SENDER,
      SENDINGUSERID,
      RECIPIENTUSERID,
      SENDTONAME,
      SUBJECT,
      MSG_TEXT,
      FLAGGED,
      RECEIVEDATE,
      EXPIRATIONDATE,
      ITEMID,
      RESPONSEENABLED,
      RESPONSEURL,
      FOLDERID,
      MESSAGETYPE,
      REPLIED,
      ITEMENDTIME,
      ITEMTITLE,
      NOTIFICATIONEVENTNAME,
      MERCHANT_NAME,
      IMG_URL)
      VALUES(
      get_single_primary_key('LJ_MSG_MT','LJ_MSG_ID'),
      '$MessageID',
      '$ExternalMessageID',
      '$Sender',
      '$SendingUserID',
      '$RecipientUserID',
      '$SendToName',".
      $this->db->escape($Subject).','.
      $this->db->escape($msg_Text).",
      '$Flagged',
      $ReceiveDate,
      $ExpirationDate,
      '$ItemID',
      '$ResponseEnabled',".
      $this->db->escape($ResponseURL).",
      '$FolderID',
      '$MessageType',
      '$Replied',
      $ItemEndTime,".
      $this->db->escape($ItemTitle).",
      '$NotificationEventName',
      '$merchant_name',
      '$img_url'
      )");
    }else{
      $this->db->query("UPDATE LJ_MSG_MT SET
      SUBJECT =". $this->db->escape($Subject).",
      MSG_TEXT = ". $this->db->escape($msg_Text).",
      FLAGGED = '$Flagged',
      RECEIVEDATE = $ReceiveDate,
      EXPIRATIONDATE = $ExpirationDate,
      ITEMID = '$ItemID',
      RESPONSEENABLED = '$ResponseEnabled',
      RESPONSEURL = ". $this->db->escape($ResponseURL).",
      FOLDERID = '$FolderID',
      MESSAGETYPE = '$MessageType',
      REPLIED = '$Replied',
      ITEMENDTIME = $ItemEndTime,
      ITEMTITLE = ". $this->db->escape($ItemTitle).",
      NOTIFICATIONEVENTNAME = '$NotificationEventName'"
      );
    }
    if(isset($response->Messages->Message->MessageMedia)){
      $MessageMedia =$response->Messages->Message->MessageMedia;
      foreach ($MessageMedia as $Media) {
        $MediaURL = $Media->MediaURL;
        $MediaName = $Media->MediaName;
        $check_msg = $this->db->query("SELECT LJ_MSG_DT_ID FROM LJ_MSG_DT WHERE MEDIA_URL = '$MediaURL'")->result_array();
        if(count($check_msg) === 0){
          $this->db->query("INSERT INTO LJ_MSG_DT (LJ_MSG_DT_ID,
          MESSAGE_ID,
          MEDIA_URL,
          MEDIA_NAME
          )
          VALUES(
          get_single_primary_key('LJ_MSG_DT','LJ_MSG_DT_ID'),
          '$MessageID',
          '$MediaURL',
          '$MediaName'
          )");
        }// end if(count($check_msg) === 0){
      }// end foreach ($MessageMedia as $Media) {
    }// end if(isset($response->Messages->Message->MessageMedia)){

  }  
  public function GetItemTransactionsResponse($response)
  {
    $NotificationEventName = (string) @$response->NotificationEventName;
    if($NotificationEventName === 'ItemMarkedShipped'){
      $Transaction = $response->TransactionArray->Transaction;
      $extendedorderid = $Transaction->ContainingOrder->ExtendedOrderID;
      $ShippingCarrierUsed = $Transaction->ShippingDetails->ShipmentTrackingDetails->ShippingCarrierUsed;
      $ShipmentTrackingNumber = $Transaction->ShippingDetails->ShipmentTrackingDetails->ShipmentTrackingNumber;
      $this->db->query("UPDATE LZ_SALESLOAD_DET D
                          SET D.TRACKING_NUMBER = '$ShipmentTrackingNumber', D.SHIPPINGCARRIER = '$ShippingCarrierUsed'
                        WHERE D.EXTENDEDORDERID = '$extendedorderid'");
    }else if($NotificationEventName === 'FixedPriceTransaction'){
      $merchant_name = (string) @$response->RecipientUserID;
      $Timestamp = (string) @$response->Timestamp;
      $Timestamp = str_split($Timestamp,19);
      $Timestamp = str_replace('T'," ",$Timestamp[0]);
      $Timestamp = "TO_DATE('".$Timestamp."', 'YYYY-MM-DD HH24:MI:SS')";
        //if($mainNode == 'GetItemTransactionsResponse'){
    //file_put_contents('test.xml', $stdin,FILE_APPEND);// save response in file
     // exit;die();
      $currency = (string) $response->Item->Currency;//<Currency>USD</Currency>
      $itemid = (string) $response->Item->ItemID;//<ItemID>264298358737</ItemID>
      $listingtype = (string) $response->Item->ListingType;//<ListingType>FixedPriceItem</ListingType>
      $paymentmethods = (string) $response->Item->PaymentMethods;//<PaymentMethods>PayPal</PaymentMethods>
      $categoryid = (string) $response->Item->PrimaryCategory->CategoryID;//<CategoryID>PayPal</CategoryID>
      $convertedcurrentprice = (string) $response->Item->SellingStatus->ConvertedCurrentPrice;//<ConvertedCurrentPrice currencyID="USD">104.99</ConvertedCurrentPrice>
      $quantitysold = (string) $response->Item->SellingStatus->QuantitySold;
      $listingstatus = (string) $response->Item->SellingStatus->ListingStatus;
      $site = (string) $response->Item->Site;
      $title = (string) $response->Item->Title;
      $title = trim(str_replace("  ", '', $title));
      $title = trim(str_replace(array("'", '"'), "''", $title));
      $conditionid = (string) $response->Item->ConditionID;
      $conditiondisplayname = (string) $response->Item->ConditionDisplayName;
      $timestamp = (string) $response->Timestamp;
      /*=========================================
      =            transaction array            =
      =========================================*/
      $Transaction = $response->TransactionArray->Transaction;
      $amountpaid = $Transaction->AmountPaid;
      $ShippingCarrierUsed = $Transaction->ShippingDetails->ShipmentTrackingDetails->ShippingCarrierUsed;
      $ShipmentTrackingNumber = $Transaction->ShippingDetails->ShipmentTrackingDetails->ShipmentTrackingNumber;
      //var_dump($amountpaid,$ShipmentTrackingNumber);exit;
      $adjustmentamount = $Transaction->AdjustmentAmount;
      $convertedadjustmentamount = $Transaction->ConvertedAdjustmentAmount;

      $Buyer = $Transaction->Buyer;
      $email = $Buyer->Email;
      $feedbackscore = $Buyer->FeedbackScore;
      $positivefeedbackpercent = $Buyer->PositiveFeedbackPercent;
      $feedbackratingstar = $Buyer->FeedbackRatingStar;
      $userid = $Buyer->UserID;
      $staticalias = $Buyer->StaticAlias;
      $userfirstname = $Buyer->UserFirstName;
      $userfirstname = trim(str_replace("  ", '', $userfirstname));
      $userfirstname = trim(str_replace(array("'", '"'), "''", $userfirstname));
      $userlastname = $Buyer->UserLastName;
      $userlastname = trim(str_replace("  ", '', $userlastname));
      $userlastname = trim(str_replace(array("'", '"'), "''", $userlastname));



      $BuyerInfo = $Buyer->BuyerInfo;
      $shipname = $BuyerInfo->ShippingAddress->Name;//shipName
      $shipname = trim(str_replace("  ", '', $shipname));
      $shipname = trim(str_replace(array("'", '"'), "''", $shipname));
      $street1 = $BuyerInfo->ShippingAddress->Street1;
      $street1 = trim(str_replace("  ", '', $street1));
      $street1 = trim(str_replace(array("'", '"'), "''", $street1));
      $street2 = $BuyerInfo->ShippingAddress->Street2;
      $street2 = trim(str_replace("  ", '', $street2));
      $street2 = trim(str_replace(array("'", '"'), "''", $street2));
      $cityname = $BuyerInfo->ShippingAddress->CityName;
      $cityname = trim(str_replace("  ", '', $cityname));
      $cityname = trim(str_replace(array("'", '"'), "''", $cityname));
      $stateorprovince = $BuyerInfo->ShippingAddress->StateOrProvince;
      $stateorprovince = trim(str_replace("  ", '', $stateorprovince));
      $stateorprovince = trim(str_replace(array("'", '"'), "''", $stateorprovince));
      $country = $BuyerInfo->ShippingAddress->Country;//US //CountryCode
      $countryname = $BuyerInfo->ShippingAddress->CountryName; // United States
      $countryname = trim(str_replace("  ", '', $countryname));
      $countryname = trim(str_replace(array("'", '"'), "''", $countryname));
      $phone = $BuyerInfo->ShippingAddress->Phone; 
      $postalcode = $BuyerInfo->ShippingAddress->PostalCode; 
      $addressid = $BuyerInfo->ShippingAddress->AddressID; 
      $addressowner = $BuyerInfo->ShippingAddress->AddressOwner; 
      $addressusage = $BuyerInfo->ShippingAddress->AddressUsage; //<AddressUsage>DefaultShipping</AddressUsage>

      $salestaxpercent = $Transaction->ShippingDetails->SalesTax->SalesTaxPercent;
      $salestaxamount = $Transaction->ShippingDetails->SalesTax->SalesTaxAmount;
      $shippingservice = $Transaction->ShippingDetails->ShippingServiceOptions->ShippingService;
      $shippingservicecost = $Transaction->ShippingDetails->ShippingServiceOptions->ShippingServiceCost;
      $shippingtype = $Transaction->ShippingDetails->ShippingType;
      $salesrecordnumber = $Transaction->ShippingDetails->SellingManagerSalesRecordNumber;//SalesRecordNumber

      $convertedamountpaid = $Transaction->ConvertedAmountPaid;
      $convertedtransactionprice = $Transaction->ConvertedTransactionPrice;
      $createddate = $Transaction->CreatedDate;
      $quantitypurchased = $Transaction->QuantityPurchased;
      $checkoutstatus = $Transaction->Status->CheckoutStatus;
      $lasttimemodified = $Transaction->Status->LastTimeModified;
      $paymentmethodused = $Transaction->Status->PaymentMethodUsed;
      $completestatus = $Transaction->Status->CompleteStatus;//<CompleteStatus>Complete</CompleteStatus>
      $buyerselectedshipping = $Transaction->Status->BuyerSelectedShipping;
      $paymentholdstatus = $Transaction->Status->PaymentHoldStatus;
      $inquirystatus = $Transaction->Status->InquiryStatus;
      $returnstatus = $Transaction->Status->ReturnStatus;
      $transactionid = $Transaction->TransactionID;
      $transactionprice = $Transaction->TransactionPrice;
      $externaltransactionid = $Transaction->ExternalTransaction->ExternalTransactionID;
      $externaltransactiontime = $Transaction->ExternalTransaction->ExternalTransactionTime;
      $feeorcreditamount = $Transaction->ExternalTransaction->FeeOrCreditAmount;
      $paymentorrefundamount = $Transaction->ExternalTransaction->PaymentOrRefundAmount;
      $externaltransactionstatus = $Transaction->ExternalTransaction->ExternalTransactionStatus;
      $shippingserviceselected = $Transaction->ShippingServiceSelected->ShippingService;
      $paidtime = $Transaction->PaidTime;
      $orderid = $Transaction->ContainingOrder->OrderID;
      $orderlineitemid = $Transaction->OrderLineItemID;
      $orderstatus = $Transaction->ContainingOrder->OrderStatus;
      $sales_record_no_group = $Transaction->ContainingOrder->ShippingDetails->SellingManagerSalesRecordNumber;//combine order only
      $cancelstatus = $Transaction->ContainingOrder->CancelStatus;
      $extendedorderid = $Transaction->ContainingOrder->ExtendedOrderID;
      $containsebayplustransaction = $Transaction->ContainingOrder->ContainseBayPlusTransaction;
      $finalvaluefee = $Transaction->FinalValueFee;
      $transactionsiteid = $Transaction->TransactionSiteID;
      $platform = $Transaction->Platform;
      $paypalemailaddress = $Transaction->PayPalEmailAddress;
      $buyercheckoutmessage = @$Transaction->BuyerCheckoutMessage;
      $buyercheckoutmessage = trim(str_replace("  ", '', $buyercheckoutmessage));
      $buyercheckoutmessage = trim(str_replace(array("'", '"'), "''", $buyercheckoutmessage));
      $actualshippingcost = $Transaction->ActualShippingCost;
      $actualhandlingcost = $Transaction->ActualHandlingCost;
      $ismultilegshipping = $Transaction->IsMultiLegShipping;// check for combine order <IsMultiLegShipping>false</IsMultiLegShipping>
      $intangibleitem = $Transaction->IntangibleItem;
      
      $paymentstatus = $Transaction->MonetaryDetails->Payments->Payment->PaymentStatus;
      $payer = $Transaction->MonetaryDetails->Payments->Payment->Payer;//<Payer type="eBayUser">02slva02</Payer>
      $payer = trim(str_replace("  ", '', $payer));
      $payer = trim(str_replace(array("'", '"'), "''", $payer));
      $payee = $Transaction->MonetaryDetails->Payments->Payment->Payee;//<Payee type="eBayUser">dfwonline</Payee>
      $payee = trim(str_replace("  ", '', $payee));
      $payee = trim(str_replace(array("'", '"'), "''", $payee));
      $paymenttime = $Transaction->MonetaryDetails->Payments->Payment->PaymentTime;//<Payee type="eBayUser">dfwonline</Payee>
      $paymentamount = $Transaction->MonetaryDetails->Payments->Payment->PaymentAmount;
      $referenceid = $Transaction->MonetaryDetails->Payments->Payment->ReferenceID;
      //$FeeOrCreditAmount = $Transaction->MonetaryDetails->Payments->Payment->FeeOrCreditAmount;//duplicate
      
      /*=====  End of transaction array  ======*/
      $item_id = $itemid;
      $countrycode = $country;
      $notificationeventname = $NotificationEventName;
      $timestamp = str_split($timestamp,19);
      $timestamp = str_replace('T'," ",$timestamp[0]); 

      $createddate = str_split($createddate,19);
      $createddate = str_replace('T'," ",$createddate[0]); 

      $externaltransactiontime = str_split($externaltransactiontime,19);
      $externaltransactiontime = str_replace('T'," ",$externaltransactiontime[0]); 
      
      $paidtime = str_split($paidtime,19);
      $paidtime = str_replace('T'," ",$paidtime[0]); 
      
      $paymenttime = str_split($paymenttime,19);
      $paymenttime = str_replace('T'," ",$paymenttime[0]); 
      

      $timestamp = "TO_DATE('".$timestamp."', 'YYYY-MM-DD HH24:MI:SS')";
      $createddate = "TO_DATE('".$createddate."', 'YYYY-MM-DD HH24:MI:SS')";
      $externaltransactiontime = "TO_DATE('".$externaltransactiontime."', 'YYYY-MM-DD HH24:MI:SS')";
      $paidtime = "TO_DATE('".$paidtime."', 'YYYY-MM-DD HH24:MI:SS')";
      $paymenttime = "TO_DATE('".$paymenttime."', 'YYYY-MM-DD HH24:MI:SS')";
     
    /*========================================================
    =            check if orderstatus = completed            =
    ========================================================*/
    if($orderstatus == 'Completed'){


    if(strpos($orderid, '-') !== false)//if order id contain - its mean its a single order not combine
    {
      $combine_order_id = '';
    }else{
      $combine_order_id = $orderid;
    }
     $select_query = "select * from LJ_ITEMSOLD_NOTIFICATION d where d.orderid = '$orderlineitemid'";
        $row = $this->db->query($select_query)->result_array();

      // ===================== End Checking if record already exist ===============================

       if(count($row) == 0){
      $this->db->query("insert into LJ_ITEMSOLD_NOTIFICATION (
      notification_id,
      notificationeventname,
      merchant_name,
      timestamp,
      currency,
      item_id,
      listingtype,
      paymentmethods,
      categoryid,
      quantitysold,
      listingstatus,
      site,
      title,
      conditionid,
      conditiondisplayname,
      convertedcurrentprice,
      amountpaid,
      adjustmentamount,
      convertedadjustmentamount,
      email,
      feedbackscore,
      positivefeedbackpercent,
      userid,
      staticalias,
      userfirstname,
      userlastname,
      shipname,
      street1,
      cityname,
      stateorprovince,
      countrycode,
      countryname,
      phone,
      postalcode,
      addressid,
      addressowner,
      addressusage,
      salestaxpercent,
      salestaxamount,
      shippingservice,
      shippingservicecost,
      salesrecordnumber,
      convertedamountpaid,
      convertedtransactionprice,
      createddate,
      quantitypurchased,
      checkoutstatus,
      buyerselectedshipping,
      paymentholdstatus,
      inquirystatus,
      returnstatus,
      transactionid,
      transactionprice,
      externaltransactionid,
      externaltransactiontime,
      feeorcreditamount,
      paymentorrefundamount,
      externaltransactionstatus,
      shippingserviceselected,
      paidtime,
      orderid,
      orderstatus,
      cancelstatus,
      extendedorderid,
      containsebayplustransaction,
      finalvaluefee,
      transactionsiteid,
      platform,
      paypalemailaddress,
      buyercheckoutmessage,
      actualshippingcost,
      actualhandlingcost,
      ismultilegshipping,
      intangibleitem,
      paymentstatus,
      payer,
      payee,
      paymenttime,
      paymentamount,
      referenceid,
      sales_record_no_group,
      combine_order_id
      )
      values(
      get_single_primary_key('LJ_ITEMSOLD_NOTIFICATION','notification_id'),
      '$notificationeventname',
      '$merchant_name',
      $timestamp,
      '$currency',
      '$item_id',
      '$listingtype',
      '$paymentmethods',
      '$categoryid',
      '$quantitysold',
      '$listingstatus',
      '$site',
      '$title',
      '$conditionid',
      '$conditiondisplayname',
      '$convertedcurrentprice',
      '$amountpaid',
      '$adjustmentamount',
      '$convertedadjustmentamount',
      '$email',
      '$feedbackscore',
      '$positivefeedbackpercent',
      '$userid',
      '$staticalias',
      '$userfirstname',
      '$userlastname',
      '$shipname',
      '$street1',
      '$cityname',
      '$stateorprovince',
      '$countrycode',
      '$countryname',
      '$phone',
      '$postalcode',
      '$addressid',
      '$addressowner',
      '$addressusage',
      '$salestaxpercent',
      '$salestaxamount',
      '$shippingservice',
      '$shippingservicecost',
      '$salesrecordnumber',
      '$convertedamountpaid',
      '$convertedtransactionprice',
      $createddate,
      '$quantitypurchased',
      '$checkoutstatus',
      '$buyerselectedshipping',
      '$paymentholdstatus',
      '$inquirystatus',
      '$returnstatus',
      '$transactionid',
      '$transactionprice',
      '$externaltransactionid',
      $externaltransactiontime,
      '$feeorcreditamount',
      '$paymentorrefundamount',
      '$externaltransactionstatus',
      '$shippingserviceselected',
      $paidtime,
      '$orderlineitemid',
      '$orderstatus',
      '$cancelstatus',
      '$extendedorderid',
      '$containsebayplustransaction',
      '$finalvaluefee',
      '$transactionsiteid',
      '$platform',
      '$paypalemailaddress',
      '$buyercheckoutmessage',
      '$actualshippingcost',
      '$actualhandlingcost',
      '$ismultilegshipping',
      '$intangibleitem',
      '$paymentstatus',
      '$payer',
      '$payee',
      $paymenttime,
      '$paymentamount',
      '$referenceid',
      '$sales_record_no_group',
      '$combine_order_id'

      )");
    } //close if(count($row) == 0){
     // $this->db->query("insert into EBAY_NOTEIFICATION (test_pk,E_data,insert_date) values (get_single_primary_key('EBAY_NOTEIFICATION','test_pk'),'OrderID:$orderid',sysdate)");

    /*=================================================================
    =            upload data to oracle db lz_salesload_det            =
    =================================================================*/
      $LZ_SELLER_ACCT_ID = $this->getAccountId($merchant_name,$NotificationEventName);
      if(empty($LZ_SELLER_ACCT_ID)){
        exit;
      }
      // $get_seller = $this->db->query("SELECT S.LZ_SELLER_ACCT_ID FROM LZ_SELLER_ACCTS S, LJ_MERHCANT_ACC_DT D WHERE UPPER(TRIM(S.SELL_ACCT_DESC)) = UPPER(TRIM(D.ACCOUNT_NAME)) AND UPPER(TRIM(S.SELL_ACCT_DESC)) = UPPER('$merchant_name') AND ROWNUM = 1")->result_array();

      // $LZ_SELLER_ACCT_ID = @$get_seller[0]['LZ_SELLER_ACCT_ID'];

    /*==========================================================
    =            insert data into lz_sales_load_det            =
    ==========================================================*/
     // =============== Start Checking if record already exist in lz_salesload_mt table ===============
          $current_date=date('Y-m-d');
          $current_date= "TO_DATE('".$current_date."', 'YYYY-MM-DD')";
          $q ="SELECT * FROM (select * from LZ_SALESLOAD_MT where loading_date like $current_date and LZ_SELLER_ACCT_ID=$LZ_SELLER_ACCT_ID ORDER BY LZ_SALELOAD_ID DESC) WHERE ROWNUM = 1";
          $row = $this->db->query($q);//->result_array();
    // =============== End Checking if record already exist in lz_salesload_mt table ===============

        // ========================== Starts Insertion in MT table ===============================
        if($row->num_rows() == 0){
            $max_id = "SELECT GET_SINGLE_PRIMARY_KEY('LZ_SALESLOAD_MT','LZ_SALELOAD_ID') LZ_SALELOAD_ID FROM DUAL";
            $row = $this->db->query($max_id)->result_array();
            $lz_saleload_id = $row[0]['LZ_SALELOAD_ID'];
            $max_id = "SELECT GET_SINGLE_PRIMARY_KEY('LZ_SALESLOAD_MT','LOADING_NO') LOADING_NO FROM DUAL";
            $row = $this->db->query($max_id)->result_array();
            $LOADING_NO = $row[0]['LOADING_NO'];
            $LOADING_DATE=date('m/d/Y');
            $SALES_REF_NO=date('m/d/Y');
            $EXCEL_FILE_NAME=date('m/d/Y');
            $EXCEL_FILE_NAME=$EXCEL_FILE_NAME."_".$LZ_SELLER_ACCT_ID;
            $POSTED='Post in ERP';
            $comma = ',';
            $insert_qry = "INSERT INTO lz_salesload_mt VALUES($lz_saleload_id $comma $LOADING_NO $comma TO_DATE('$LOADING_DATE', 'mm/dd/yyyy') $comma '$SALES_REF_NO' $comma NULL $comma '$EXCEL_FILE_NAME' $comma '$POSTED' $comma $LZ_SELLER_ACCT_ID )";
            $this->db->query($insert_qry);
        }else{
            $row = $this->db->query($q)->result_array();
            $lz_saleload_id = $row[0]['LZ_SALELOAD_ID'];
        }
        // ========================== End Insertion in MT table ===============================


      // working Dat format = TO_DATE('2016-08-30 20:49:08', 'YYYY-MM-DD HH24:MI:SS')

      // ===================== Start Checking if record already exist ==============================
        $select_query = "select * from LZ_SALESLOAD_det d where d.order_id = '$orderlineitemid'";
        $row = $this->db->query($select_query)->result_array();

      // ===================== End Checking if record already exist ===============================

       if(count($row) == 0){
        /*==============================================
    =            insertion in det table            =
    ==============================================*/
    $max_id = "SELECT GET_SINGLE_PRIMARY_KEY('LZ_SALESLOAD_DET','LZ_SALESLOAD_DET_ID') LZ_SALESLOAD_DET_ID FROM DUAL";
    $row = $this->db->query($max_id)->result_array();
    $lz_salesload_det_id = $row[0]['LZ_SALESLOAD_DET_ID'];

            $qry = "INSERT INTO LZ_SALESLOAD_DET (
    sales_record_number,
    user_id,
    buyer_fullname,
    buyer_phone_number,
    buyer_email,
    buyer_address1,
    BUYER_ADDRESS2,
    buyer_city,
    buyer_state,
    buyer_zip,
    buyer_country,
    item_id,
    transaction_id,
    item_title,
    quantity,
    sale_price,
    shipping_and_handling,
    sales_tax,
    total_price,
    payment_method,
    paypal_transaction_id,
    sale_date,
    checkout_date,
    paid_on_date,
    shipping_service,
    listed_on,
    sold_on,
    lz_saleload_id,
    lz_salesload_det_id,
    ebay_fee_perc,
    paypal_per_trans_fee,
    posted,
    ship_to_address1,
    SHIP_TO_ADDRESS2,
    ship_to_city,
    ship_to_state,
    ship_to_zip,
    ship_to_country,
    mannual_yn,
    order_id,
    buyer_checkout_msg,
    buyer_street1,
    BUYER_STREET2,
    buyer_address_id,
    orderstatus,
    sales_record_no_group,
    ship_to_name,
    ship_to_phone,
    combine_order_id,
    inserted_date,
    extendedorderid,
    conditionid,
    conditiondisplayname)
    values(
    '$salesrecordnumber',
    '$userid',
    '$userfirstname $userlastname',
    '$phone',
    '$email',
    '$street1',
    '$street2',
    '$cityname',
    '$stateorprovince',
    '$postalcode',
    '$countryname',
    '$item_id',
    '$transactionid',
    '$title',
    '$quantitypurchased',
    '$convertedcurrentprice',
    '$actualhandlingcost',
    '$salestaxamount',
    '$convertedamountpaid',
    '$paymentmethods',
    '$externaltransactionid',
     $createddate,
     $externaltransactiontime,
     $paidtime,
    '$shippingservice',
    '$platform',
    '$platform',
    '$lz_saleload_id',
    '$lz_salesload_det_id',
    '$finalvaluefee',
    '$feeorcreditamount',
    'post in erp',
    '$street1',
    '$street2',
    '$cityname',
    '$stateorprovince',
    '$postalcode',
    '$countryname',
    '2',
    '$orderlineitemid',
    '$buyercheckoutmessage',
    '$street1',
    '$street2',
    '$addressid',
    '$orderstatus',
    '$sales_record_no_group',
    '$shipname',
    '$phone',
    '$combine_order_id',
    sysdate,
    '$extendedorderid',
    '$conditionid',
    '$conditiondisplayname'
    )";
    //$this->db->query("CALL PRO_ORDERPACKING('$orderlineitemid')");
        }else{
            $qry = "UPDATE LZ_SALESLOAD_DET SET ORDERSTATUS='$orderstatus' , EXTENDEDORDERID = '$extendedorderid' ,SALES_RECORD_NO_GROUP = '$sales_record_no_group' ,COMBINE_ORDER_ID = '$combine_order_id' WHERE ORDER_ID = '$orderlineitemid'";
        }
            $this->db->query($qry);

    /*=====  End of insertion in det table  ======*/



        /*=============================================
        =            lz_awaiting_shipment insertion           =
        =============================================*/
        if($orderstatus == 'Completed')
        {
          $lz_seller_account_id = $LZ_SELLER_ACCT_ID;
         
          $this->db->query("INSERT INTO LZ_AWAITING_SHIPMENT (TRANSACTIONID, SALERECORDID, ORDERLINEITEMID, LZ_SELLER_ACCOUNT_ID) SELECT '$transactionid', '$salesrecordnumber', '$orderlineitemid', '$lz_seller_account_id' FROM DUAL WHERE NOT EXISTS (SELECT * FROM LZ_AWAITING_SHIPMENT A WHERE A.ORDERLINEITEMID = '$orderlineitemid') ");
           
        }
                                    
        /*=====  End of lz_awaiting_shipment insertion ======*/   
          
      }// if(count($data) > 0 ){

    //end if orderstatus = 'Completed'


    /*=====  End of check if orderstatus = completed  ======*/


    }//else if($NotificationEventName === 'FixedPriceTransaction'){
  
  
  }
public function NotificationEvent($response)
  {
    $NotificationEventName = (string) @$response->NotificationEventName;
    // var_dump($NotificationEventName);exit;
    //$merchant_name = (string) @$response->RecipientUserID;
    switch ($NotificationEventName) {
      case 'BuyerCancelRequested':
        $this->BuyerCancelRequested($response);
        break;
      case 'ReturnCreated':
        $this->ReturnHandler($response);
        break;
      case 'ReturnShipped':
        $this->ReturnHandler($response);
        break;
      case 'ReturnClosed':
        $this->ReturnHandler($response);
        break;
      case 'ReturnDelivered':
        $this->ReturnHandler($response);
        break;
      default:
        # code...
        break;
    }
  }
  public function BuyerCancelRequested($response)
  {
    $NotificationEventName = (string) @$response->NotificationEventName;
    // var_dump($NotificationEventName);exit;
    $merchant_name = (string) @$response->RecipientUserID;
    // $CancelId = (string) @$response->CancelId;
    // $CreationDate = (string) @$response->CreationDate;
    // $OtherPartyId = (string) @$response->OtherPartyId;
    // $OtherPartyRole = (string) @$response->OtherPartyRole;
    // $CancelStatus = (string) @$response->CancelStatus;
    // $CancelGlobalId = (string) @$response->CancelGlobalId;
    // $Priority = (string) @$response->Priority;
    // $CancelLink = (string) @$response->CancelLink;
    $ItemId = (string) @$response->TransactionIdentity->ItemId;
    $TransactionId = (string) @$response->TransactionIdentity->TransactionId;
    $accountId = $this->getAccountId($merchant_name,$NotificationEventName);
    if(!empty($accountId)){
      $this->getCancellations(81,$accountId);
      $result['LZ_SELLER_ACCT_ID']= $accountId;
      $result['ORDER_ID']= $ItemId.'-'.$TransactionId;
      $result['print_r']= false;
      $this->load->view('API/GetOrders/getSingleOrderById',$result);
    }
    // $get_account = $this->db->query("SELECT S.LZ_SELLER_ACCT_ID FROM LZ_SELLER_ACCTS S WHERE UPPER(S.SELL_ACCT_DESC) = UPPER('$merchant_name')")->result_array();
    // if(count($get_account) > 0){
    //   $accountId = $get_account[0]['LZ_SELLER_ACCT_ID'];
    //   $this->getCancellations(81,$accountId);
    //   $result['LZ_SELLER_ACCT_ID']= $accountId;
    //   $result['ORDER_ID']= $ItemId.'-'.$TransactionId;
    //   $result['print_r']= false;
    //   $this->load->view('API/GetOrders/getSingleOrderById',$result);
    // }else{
    //   $this->db->query("CALL PRO_TABLE_TRIGGER_LOG('BuyerCancelRequested', 'Account Name :$merchant_name Not found in LZ_SELLER_ACCTS table , '0', '81')");
    // }
    

  }
  public function getCancellations($userId,$accountId){
    //var_dump($userId,$accountId);exit;
    $data['merchant_id'] = $accountId;
    $data['userId'] = $userId;
    $data['cancelCall'] = 1;
    $this->load->view('ebay/postOrder/02-searchCancellation',$data,true);
    
  }
  public function ReturnHandler($response)
  {
    $NotificationEventName = (string) @$response->NotificationEventName;
    // var_dump($NotificationEventName);exit;
    switch ($NotificationEventName) {
      case 'ReturnCreated':
        $return_value = 'RETURN_STARTED';
        break;
      case 'ReturnShipped':
        $return_value = 'ITEM_SHIPPED';
        break;
      case 'ReturnClosed':
        $return_value = 'CLOSED';
        break;
      case 'ReturnDelivered':
        $return_value = 'ITEM_DELIVERED';
        break;
      default:
        $return_value = 'ALL_OPEN';
        break;
    }
    $merchant_name = (string) @$response->RecipientUserID;
    // $ReturnId = (string) @$response->ReturnId;
    // $CreationDate = (string) @$response->CreationDate;
    // $OtherPartyId = (string) @$response->OtherPartyId;
    // $OtherPartyRole = (string) @$response->OtherPartyRole;
    // $ReturnStatus = (string) @$response->ReturnStatus;
    // $ReturnGlobalId = (string) @$response->ReturnGlobalId;
    // $OrderId = (string) @$response->OrderId;
    // $Priority = (string) @$response->Priority;
    // $ReturnTrackingId = (string) @$response->ReturnTrackingId;
    // $ReturnLink = (string) @$response->ReturnLink;
    // $ItemId = (string) @$response->TransactionIdentity->ItemId;
    // $TransactionId = (string) @$response->TransactionIdentity->TransactionId;
    $accountId = $this->getAccountId($merchant_name,$NotificationEventName);
    if(!empty($accountId)){
      $this->downlaodReturns($accountId,$return_value);
    }
    

  }
  public function downlaodReturns($accountId,$return_value)//RETURN_STARTED,ITEM_DELIVERED,ITEM_SHIPPED,CLOSED
    {
      $result['merchant_id'] = $accountId;
      $result['return_value'] = $return_value;
      $result['returnCall'] = 1;
      $this->load->view('ebay/postOrder/01-searchReturn', $result);
      //return $result;
    }
  public function GetBestOffersResponse($response)
  {
    $NotificationEventName = (string) @$response->NotificationEventName;
    // var_dump($NotificationEventName);exit;
    $merchant_name = (string) @$response->RecipientUserID;
    $accountId = $this->getAccountId($merchant_name,$NotificationEventName);
    //var_dump($accountId);exit;
    //if(!empty($accountId)){
      // $result['account_name'] = $accountId;
      // $result['site_id'] = 0;
      // $result['BestOfferID'] = $BestOfferID;
      // $result['ItemID'] = $ItemID;
      // $this->load->view('ebay/trading/GetBestOffers',$result);
    //}
    /*===================================================
    =            copy from getbestoffers.php            =
    ===================================================*/
      $ItemID = (string) @$response->Item->ItemID;
      $BuyItNowPrice = (string) @$response->Item->BuyItNowPrice;
      $currencyID = "USD";
      $Location = (string) @$response->Item->Location;
      $Title = (string) @$response->Item->Title;
      $ConditionID = (string) @$response->Item->ConditionID;
      $ConditionDisplayName = (string) @$response->Item->ConditionDisplayName;
      $EndTime = $response->Item->ListingDetails->EndTime;
      $EndTime = str_split($EndTime,19);
      $EndTime = str_replace('T'," ",$EndTime[0]);
      $EndTime = "TO_DATE('".$EndTime."', 'YYYY-MM-DD HH24:MI:SS')";
        /*=========================================
        =            insert data in db            =
        =========================================*/
        $check_qry = $this->db->query("SELECT LJ_OFFER_ID FROM LJ_BEST_OFFER_ITEMS_MT WHERE EBAY_ID = '$ItemID'")->result_array();
        if(count($check_qry) === 0){
            $account_id = $accountId;
            $seller_name = $merchant_name;
            $get_pk = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_BEST_OFFER_ITEMS_MT','LJ_OFFER_ID') LJ_OFFER_ID FROM DUAL")->result_array();
            $lj_offer_id = $get_pk[0]['LJ_OFFER_ID'];
            $this->db->query("INSERT INTO LJ_BEST_OFFER_ITEMS_MT (
            LJ_OFFER_ID,
            EBAY_ID,
            BUYITNOWPRICE,
            ITEM_TITLE,
            CONDITIONID,
            CONDITIONDISPLAYNAME,
            ACCOUNT_ID,
            CURRENCYID,
            SELLER_NAME)VALUES(
            '$lj_offer_id',
            '$ItemID',
            '$BuyItNowPrice',
            '$Title',
            '$ConditionID',
            '$ConditionDisplayName',
            '$account_id',
            '$currencyID',
            '$seller_name'
            )");
        }else{
            $lj_offer_id = $check_qry[0]['LJ_OFFER_ID'];
        }
        

        /*=====  End of insert data in db  ======*/
    //$offer = (string) @$response->BestOfferArray->BestOffer;
    $BestOfferID = @$response->BestOfferArray->BestOffer->BestOfferID;
    $ExpirationTime = @$response->BestOfferArray->BestOffer->ExpirationTime;
    $ExpirationTime = str_split($ExpirationTime,19);
    $ExpirationTime = str_replace('T'," ",$ExpirationTime[0]);
    $ExpirationTime = "TO_DATE('".$ExpirationTime."', 'YYYY-MM-DD HH24:MI:SS')";
    $Buyer_Email = @$response->BestOfferArray->BestOffer->Buyer->Email;
    $Buyer_FeedbackScore = @$response->BestOfferArray->BestOffer->Buyer->FeedbackScore;
    $Buyer_RegistrationDate = @$response->BestOfferArray->BestOffer->Buyer->RegistrationDate;
    $Buyer_RegistrationDate = str_split($Buyer_RegistrationDate,19);
    $Buyer_RegistrationDate = str_replace('T'," ",$Buyer_RegistrationDate[0]);
    $Buyer_RegistrationDate = "TO_DATE('".$Buyer_RegistrationDate."', 'YYYY-MM-DD HH24:MI:SS')";
    $Buyer_UserID = @$response->BestOfferArray->BestOffer->Buyer->UserID;
    $Buyer_StateOrProvince = @$response->BestOfferArray->BestOffer->Buyer->ShippingAddress->StateOrProvince;
    $Buyer_CountryName = @$response->BestOfferArray->BestOffer->Buyer->ShippingAddress->CountryName;
    $Buyer_PostalCode = @$response->BestOfferArray->BestOffer->Buyer->ShippingAddress->PostalCode;
    $buyer_shippingaddress = $Buyer_PostalCode .' | '. $Buyer_StateOrProvince .' | '.$Buyer_CountryName;
    $offer_price = @$response->BestOfferArray->BestOffer->Price;
    $offer_currency = $currencyID;
    $offer_Status = @$response->BestOfferArray->BestOffer->Status;
    $offer_Quantity = @$response->BestOfferArray->BestOffer->Quantity;
    $offer_BestOfferCodeType = @$response->BestOfferArray->BestOffer->BestOfferCodeType;
    /*=========================================
    =            insert data in db            =
    =========================================*/
     $check_qry = $this->db->query("SELECT OFFER_DT_ID FROM LJ_BEST_OFFER_ITEMS_DT WHERE BESTOFFERID = '$BestOfferID'")->result_array();
    if(count($check_qry) === 0){
        $this->db->query("INSERT INTO LJ_BEST_OFFER_ITEMS_DT (
                        OFFER_DT_ID,
                        LJ_OFFER_ID,
                        BESTOFFERID,
                        EXPIRATIONTIME,
                        BUYER_EMAIL,
                        BUYER_FEEDBACKSCORE,
                        BUYER_REGISTRATIONDATE,
                        BUYER_USERID,
                        BUYER_SHIPPINGADDRESS,
                        OFFER_PRICE,
                        OFFER_STATUS,
                        QUANTITY,
                        BESTOFFERCODETYPE
                        )values(
                        GET_SINGLE_PRIMARY_KEY('LJ_BEST_OFFER_ITEMS_DT','OFFER_DT_ID'),
                        '$lj_offer_id',
                        '$BestOfferID',
                        $ExpirationTime,
                        '$Buyer_Email',
                        '$Buyer_FeedbackScore',
                        $Buyer_RegistrationDate,
                        '$Buyer_UserID',
                        '$buyer_shippingaddress',
                        '$offer_price',
                        '$offer_Status',
                        '$offer_Quantity',
                        '$offer_BestOfferCodeType'
                        )");
    }else{
        $this->db->query("UPDATE LJ_BEST_OFFER_ITEMS_DT SET OFFER_STATUS = '$offer_Status' WHERE BESTOFFERID = '$BestOfferID'");
    }
    /*=====  End of insert data in db  ======*/
    /*=====  End of copy from getbestoffers.php  ======*/
  }
  public function getAccountId($merchant_name,$NotificationEventName){
    //$NotificationEventName='BestOffers';
    $get_account = $this->db->query("SELECT S.LZ_SELLER_ACCT_ID FROM LZ_SELLER_ACCTS S WHERE UPPER(S.SELL_ACCT_DESC) = UPPER('$merchant_name')")->result_array();
    $accountId = '';
    if(count($get_account) > 0){
      $accountId = $get_account[0]['LZ_SELLER_ACCT_ID'];
      return $accountId;
      
    }else{
      $this->db->query("CALL PRO_TABLE_TRIGGER_LOG('$NotificationEventName', 'ACCOUNT NAME :$merchant_name NOT FOUND IN LZ_SELLER_ACCTS TABLE', '0', '81')");
      return $accountId;
    }
  }
  public function GetItemResponse($response){
    $NotificationEventName = (string) @$response->NotificationEventName;
    $Timestamp = (string) @$response->Timestamp;
    $Timestamp = str_split($Timestamp,19);
    $Timestamp = str_replace('T'," ",$Timestamp[0]);
    $Timestamp = "TO_DATE('".$Timestamp."', 'YYYY-MM-DD HH24:MI:SS')";
    $merchant_name = (string) @$response->RecipientUserID;
    // $PayPalEmailAddress = (string) @$response->Item->PayPalEmailAddress;
    $ItemID = (string) @$response->Item->ItemID;
    $AutoPay = (string) @$response->Item->AutoPay;
    $BuyerProtection = (string) @$response->Item->BuyerProtection;
    $BuyItNowPrice = (string) @$response->Item->BuyItNowPrice;
    $Country = (string) @$response->Item->Country;
    $Currency = (string) @$response->Item->Currency;
    $Description = (string) @$response->Item->Description;
    $GiftIcon = (string) @$response->Item->GiftIcon;
    $HitCounter = (string) @$response->Item->HitCounter;
    $Adult = (string) @$response->Item->ListingDetails->Adult;
    $BindingAuction = (string) @$response->Item->ListingDetails->BindingAuction;
    $CheckoutEnabled = (string) @$response->Item->ListingDetails->CheckoutEnabled;
    $ConvertedBuyItNowPrice = (string) @$response->Item->ListingDetails->ConvertedBuyItNowPrice;
    $ConvertedStartPrice = (string) @$response->Item->ListingDetails->ConvertedStartPrice;
    $ConvertedReservePrice = (string) @$response->Item->ListingDetails->ConvertedReservePrice;
    $HasReservePrice = (string) @$response->Item->ListingDetails->HasReservePrice;
    $StartTime = (string) @$response->Item->ListingDetails->StartTime;
    $StartTime = str_split($StartTime,19);
    $StartTime = str_replace('T'," ",$StartTime[0]);
    $StartTime = "TO_DATE('".$StartTime."', 'YYYY-MM-DD HH24:MI:SS')";
    $EndTime = (string) @$response->Item->ListingDetails->EndTime;
    $EndTime = str_split($EndTime,19);
    $EndTime = str_replace('T'," ",$EndTime[0]);
    $EndTime = "TO_DATE('".$EndTime."', 'YYYY-MM-DD HH24:MI:SS')";
    $ViewItemURL = (string) @$response->Item->ListingDetails->ViewItemURL;
    $HasUnansweredQuestions = (string) @$response->Item->ListingDetails->HasUnansweredQuestions;
    $HasPublicMessages = (string) @$response->Item->ListingDetails->HasPublicMessages;
    $ViewItemURLForNaturalSearch = (string) @$response->Item->ListingDetails->ViewItemURLForNaturalSearch;
    $LayoutID = (string) @$response->Item->ListingDesigner->LayoutID;
    $ThemeID = (string) @$response->Item->ListingDesigner->ThemeID;
    $ListingDuration = (string) @$response->Item->ListingDuration;
    $ListingType = (string) @$response->Item->ListingType;
    $Location = (string) @$response->Item->Location;
    $PaymentMethods = (string) @$response->Item->PaymentMethods;
    $PayPalEmailAddress = (string) @$response->Item->PayPalEmailAddress;
    $CategoryID = (string) @$response->Item->PrimaryCategory->CategoryID;
    $CategoryName = (string) @$response->Item->PrimaryCategory->CategoryName;
    $PrivateListing = (string) @$response->Item->PrivateListing;
    $IncludeStockPhotoURL = (string) @$response->Item->ProductListingDetails->IncludeStockPhotoURL;
    $IncludePrefilledItemInformation = (string) @$response->Item->ProductListingDetails->IncludePrefilledItemInformation;
    $UseStockPhotoURLAsGallery = (string) @$response->Item->ProductListingDetails->UseStockPhotoURLAsGallery;
    $ProductReferenceID = (string) @$response->Item->ProductListingDetails->ProductReferenceID;
    $UPC = (string) @$response->Item->ProductListingDetails->UPC;
    $EAN = (string) @$response->Item->ProductListingDetails->EAN;
    $Brand = (string) @$response->Item->ProductListingDetails->BrandMPN->Brand;
    $MPN = (string) @$response->Item->ProductListingDetails->BrandMPN->MPN;
    $IncludeeBayProductDetails = (string) @$response->Item->ProductListingDetails->IncludeeBayProductDetails;
    $Quantity = (string) @$response->Item->Quantity;
    $ReservePrice = (string) @$response->Item->ReservePrice;
    $ItemRevised = (string) @$response->Item->ReviseStatus->ItemRevised;
    $AboutMePage = (string) @$response->Item->Seller->AboutMePage;
    $Email = (string) @$response->Item->Seller->Email;
    $FeedbackScore = (string) @$response->Item->Seller->FeedbackScore;
    $PositiveFeedbackPercent = (string) @$response->Item->Seller->PositiveFeedbackPercent;
    $FeedbackPrivate = (string) @$response->Item->Seller->FeedbackPrivate;
    $IDVerified = (string) @$response->Item->Seller->IDVerified;
    $eBayGoodStanding = (string) @$response->Item->Seller->eBayGoodStanding;
    $NewUser = (string) @$response->Item->Seller->NewUser;
    $RegistrationDate = (string) @$response->Item->Seller->RegistrationDate;
    $RegistrationDate = str_split($RegistrationDate,19);
    $RegistrationDate = str_replace('T'," ",$RegistrationDate[0]);
    $RegistrationDate = "TO_DATE('".$RegistrationDate."', 'YYYY-MM-DD HH24:MI:SS')";
    $Site = (string) @$response->Item->Seller->Site;
    $Status = (string) @$response->Item->Seller->Status;
    $UserID = (string) @$response->Item->Seller->UserID;
    $UserIDChanged = (string) @$response->Item->Seller->UserIDChanged;
    $UserIDLastChanged = (string) @$response->Item->Seller->UserIDLastChanged;
    $UserIDLastChanged = str_split($UserIDLastChanged,19);
    $UserIDLastChanged = str_replace('T'," ",$UserIDLastChanged[0]);
    $UserIDLastChanged = "TO_DATE('".$UserIDLastChanged."', 'YYYY-MM-DD HH24:MI:SS')";
    $VATStatus = (string) @$response->Item->Seller->VATStatus;
    $AllowPaymentEdit = (string) @$response->Item->Seller->SellerInfo->AllowPaymentEdit;
    $Seller_CheckoutEnabled = (string) @$response->Item->Seller->SellerInfo->CheckoutEnabled;
    $CIPBankAccountStored = (string) @$response->Item->Seller->SellerInfo->CIPBankAccountStored;
    $GoodStanding = (string) @$response->Item->Seller->SellerInfo->GoodStanding;
    $LiveAuctionAuthorized = (string) @$response->Item->Seller->SellerInfo->LiveAuctionAuthorized;
    $MerchandizingPref = (string) @$response->Item->Seller->SellerInfo->MerchandizingPref;
    $QualifiesForB2BVAT = (string) @$response->Item->Seller->SellerInfo->QualifiesForB2BVAT;
    $StoreOwner = (string) @$response->Item->Seller->SellerInfo->StoreOwner;
    $StoreURL = (string) @$response->Item->Seller->SellerInfo->StoreURL;
    $SafePaymentExempt = (string) @$response->Item->Seller->SellerInfo->SafePaymentExempt;
    $MotorsDealer = (string) @$response->Item->Seller->MotorsDealer;
    $BidCount = (string) @$response->Item->SellingStatus->BidCount;
    $BidIncrement = (string) @$response->Item->SellingStatus->BidIncrement;
    $SellerConvertedCurrentPrice = (string) @$response->Item->SellingStatus->ConvertedCurrentPrice;
    $CurrentPrice = (string) @$response->Item->SellingStatus->CurrentPrice;
    $LeadCount = (string) @$response->Item->SellingStatus->LeadCount;
    $MinimumToBid = (string) @$response->Item->SellingStatus->MinimumToBid;
    $QuantitySold = (string) @$response->Item->SellingStatus->QuantitySold;
    $ReserveMet = (string) @$response->Item->SellingStatus->ReserveMet;
    $SecondChanceEligible = (string) @$response->Item->SellingStatus->SecondChanceEligible;
    $ListingStatus = (string) @$response->Item->SellingStatus->ListingStatus;
    $QuantitySoldByPickupInStore = (string) @$response->Item->SellingStatus->QuantitySoldByPickupInStore;
    $ApplyShippingDiscount = (string) @$response->Item->ShippingDetails->ApplyShippingDiscount;
    $WeightMajor = (string) @$response->Item->ShippingDetails->CalculatedShippingRate->WeightMajor;
    $WeightMinor = (string) @$response->Item->ShippingDetails->CalculatedShippingRate->WeightMinor;
    $SalesTaxPercent = (string) @$response->Item->ShippingDetails->SalesTax->SalesTaxPercent;
    $ShippingIncludedInTax = (string) @$response->Item->ShippingDetails->SalesTax->ShippingIncludedInTax;
    $ShippingService = (string) @$response->Item->ShippingDetails->ShippingServiceOptions->ShippingService;
    $ShippingServiceCost = (string) @$response->Item->ShippingDetails->ShippingServiceOptions->ShippingServiceCost;
    $ShippingServicePriority = (string) @$response->Item->ShippingDetails->ShippingServiceOptions->ShippingServicePriority;
    $ExpeditedService = (string) @$response->Item->ShippingDetails->ShippingServiceOptions->ExpeditedService;
    $ShippingTimeMin = (string) @$response->Item->ShippingDetails->ShippingServiceOptions->ShippingTimeMin;
    $ShippingTimeMax = (string) @$response->Item->ShippingDetails->ShippingServiceOptions->ShippingTimeMax;
    $FreeShipping = (string) @$response->Item->ShippingDetails->ShippingServiceOptions->FreeShipping;
    $ShippingType = (string) @$response->Item->ShippingDetails->ShippingType;
    $ThirdPartyCheckout = (string) @$response->Item->ShippingDetails->ThirdPartyCheckout;
    $ShippingDiscountProfileID = (string) @$response->Item->ShippingDetails->ShippingDiscountProfileID;
    $InternationalShippingDiscountProfileID = (string) @$response->Item->ShippingDetails->InternationalShippingDiscountProfileID;
    $ShipToLocations = (string) @$response->Item->ShipToLocations;
    $Seller_Site = (string) @$response->Item->Site;
    $Title = (string) @$response->Item->Title;
    $HitCount = (string) @$response->Item->HitCount;
    $GalleryType = (string) @$response->Item->PictureDetails->GalleryType;
    $GalleryURL = (string) @$response->Item->PictureDetails->GalleryURL;
    $PhotoDisplay = (string) @$response->Item->PictureDetails->PhotoDisplay;
    $PictureSource = (string) @$response->Item->PictureDetails->PictureSource;
    $DispatchTimeMax = (string) @$response->Item->DispatchTimeMax;
    $IntangibleItem = (string) @$response->Item->IntangibleItem;
    $RefundOption = (string) @$response->Item->ReturnPolicy->RefundOption;
    $Refund = (string) @$response->Item->ReturnPolicy->Refund;
    $ReturnsWithinOption = (string) @$response->Item->ReturnPolicy->ReturnsWithinOption;
    $ReturnsWithin = (string) @$response->Item->ReturnPolicy->ReturnsWithin;
    $ReturnsAcceptedOption = (string) @$response->Item->ReturnPolicy->ReturnsAcceptedOption;
    $ReturnsAccepted = (string) @$response->Item->ReturnPolicy->ReturnsAccepted;
    $ShippingCostPaidByOption = (string) @$response->Item->ReturnPolicy->ShippingCostPaidByOption;
    $ShippingCostPaidBy = (string) @$response->Item->ReturnPolicy->ShippingCostPaidBy;
    $ConditionID = (string) @$response->Item->ConditionID;
    $ConditionDisplayName = (string) @$response->Item->ConditionDisplayName;
    $ShippingProfileID = (string) @$response->Item->SellerProfiles->SellerShippingProfile->ShippingProfileID;
    $ReturnProfileID = (string) @$response->Item->SellerProfiles->SellerReturnProfile->ReturnProfileID;
    $ReturnProfileName = (string) @$response->Item->SellerProfiles->SellerReturnProfile->ReturnProfileName;
    $PaymentProfileID = (string) @$response->Item->SellerProfiles->SellerPaymentProfile->PaymentProfileID;
    $PaymentProfileName = (string) @$response->Item->SellerProfiles->SellerPaymentProfile->PaymentProfileName;
    $ShippingIrregular = (string) @$response->Item->ShippingPackageDetails->ShippingIrregular;
    $ShippingPackage = (string) @$response->Item->ShippingPackageDetails->ShippingPackage;
    $ShipWeightMajor = (string) @$response->Item->ShippingPackageDetails->WeightMajor;
    $ShipWeightMinor = (string) @$response->Item->ShippingPackageDetails->WeightMinor;
    $HideFromSearch = (string) @$response->Item->HideFromSearch;
    $eBayPlus = (string) @$response->Item->eBayPlus;
    $RelistParentID = (string) @$response->Item->RelistParentID;
    $eBayPlusEligible = (string) @$response->Item->eBayPlusEligible;
    $IsSecureDescription = (string) @$response->Item->IsSecureDescription;
    //var_dump($ItemID);exit;
    //$accountId = $this->getAccountId($merchant_name,$NotificationEventName);
    //if(!empty($PayPalEmailAddress)){
    /*=====================================
    =            update paypal            =
    =====================================*/
    
    $this->db->query("UPDATE EBAY_LIST_MT E SET E.PAYPAL_EMAIL = '$PayPalEmailAddress' WHERE E.EBAY_ITEM_ID = '$ItemID'");
      $this->db->query("CALL PRO_TABLE_TRIGGER_LOG('$NotificationEventName', 'PayPalEmailAddress :$PayPalEmailAddress , ItemID = $ItemID ', '0', '81')");
    
    /*=====  End of update paypal  ======*/
if($merchant_name == 'dfwonline' OR $merchant_name == 'techbargains2015'){
    $get_pk =$this->db->query("SELECT get_single_primary_key('LJ_LISTING_NOTIFICATION','NOTIFICATION_ID') NOTIFICATION_ID FROM DUAL")->result_array();
    $notification_id = $get_pk[0]['NOTIFICATION_ID'];
      $this->db->query("insert into LJ_LISTING_NOTIFICATION (NOTIFICATION_ID,
NOTIFICATIONEVENTNAME,
TIMESTAMP,
MERCHANT_NAME,
PAYPALEMAILADDRESS,
ITEMID,
AUTOPAY,
BUYERPROTECTION,
BUYITNOWPRICE,
COUNTRY,
CURRENCY,
DESCRIPTION,
GIFTICON,
HITCOUNTER,
ADULT,
BINDINGAUCTION,
CHECKOUTENABLED,
CONVERTEDBUYITNOWPRICE,
CONVERTEDSTARTPRICE,
CONVERTEDRESERVEPRICE,
HASRESERVEPRICE,
STARTTIME,
ENDTIME,
VIEWITEMURL,
HASUNANSWEREDQUESTIONS,
HASPUBLICMESSAGES,
VIEWITEMURLFORNATURALSEARCH,
LAYOUTID,
THEMEID,
LISTINGDURATION,
LISTINGTYPE,
LOCATION,
PAYMENTMETHODS,
CATEGORYID,
CATEGORYNAME,
PRIVATELISTING,
INCLUDESTOCKPHOTOURL,
INCLUDEPREFILLEDITEMINFORMATION,
USESTOCKPHOTOURLASGALLERY,
PRODUCTREFERENCEID,
UPC,
EAN,
BRAND,
MPN,
INCLUDEEBAYPRODUCTDETAILS,
QUANTITY,
RESERVEPRICE,
ITEMREVISED,
ABOUTMEPAGE,
EMAIL,
FEEDBACKSCORE,
POSITIVEFEEDBACKPERCENT,
FEEDBACKPRIVATE,
IDVERIFIED,
EBAYGOODSTANDING,
NEWUSER,
REGISTRATIONDATE,
SITE,
STATUS,
USERID,
USERIDCHANGED,
USERIDLASTCHANGED,
VATSTATUS,
ALLOWPAYMENTEDIT,
SELLER_CHECKOUTENABLED,
CIPBANKACCOUNTSTORED,
GOODSTANDING,
LIVEAUCTIONAUTHORIZED,
MERCHANDIZINGPREF,
QUALIFIESFORB2BVAT,
STOREOWNER,
STOREURL,
SAFEPAYMENTEXEMPT,
MOTORSDEALER,
BIDCOUNT,
BIDINCREMENT,
SELLERCONVERTEDCURRENTPRICE,
CURRENTPRICE,
LEADCOUNT,
MINIMUMTOBID,
QUANTITYSOLD,
RESERVEMET,
SECONDCHANCEELIGIBLE,
LISTINGSTATUS,
QUANTITYSOLDBYPICKUPINSTORE,
APPLYSHIPPINGDISCOUNT,
WEIGHTMAJOR,
WEIGHTMINOR,
SALESTAXPERCENT,
SHIPPINGINCLUDEDINTAX,
SHIPPINGSERVICE,
SHIPPINGSERVICECOST,
SHIPPINGSERVICEPRIORITY,
EXPEDITEDSERVICE,
SHIPPINGTIMEMIN,
SHIPPINGTIMEMAX,
FREESHIPPING,
SHIPPINGTYPE,
THIRDPARTYCHECKOUT,
SHIPPINGDISCOUNTPROFILEID,
INTERNATIONALSHIPPINGDISCOUNTPROFILEID,
SHIPTOLOCATIONS,
SELLER_SITE,
TITLE,
HITCOUNT,
GALLERYTYPE,
GALLERYURL,
PHOTODISPLAY,
PICTURESOURCE,
DISPATCHTIMEMAX,
INTANGIBLEITEM,
REFUNDOPTION,
REFUND,
RETURNSWITHINOPTION,
RETURNSWITHIN,
RETURNSACCEPTEDOPTION,
RETURNSACCEPTED,
SHIPPINGCOSTPAIDBYOPTION,
SHIPPINGCOSTPAIDBY,
CONDITIONID,
CONDITIONDISPLAYNAME,
SHIPPINGPROFILEID,
RETURNPROFILEID,
RETURNPROFILENAME,
PAYMENTPROFILEID,
PAYMENTPROFILENAME,
SHIPPINGIRREGULAR,
SHIPPINGPACKAGE,
SHIPWEIGHTMAJOR,
SHIPWEIGHTMINOR,
HIDEFROMSEARCH,
EBAYPLUS,
EBAYPLUSELIGIBLE,
ISSECUREDESCRIPTION,
RELISTPARENTID)values
(
'$notification_id',
'$NotificationEventName',
$Timestamp,
'$merchant_name',
'$PayPalEmailAddress',
'$ItemID',
'$AutoPay',
'$BuyerProtection',
'$BuyItNowPrice',
'$Country',
'$Currency',
'$Description',
'$GiftIcon',
'$HitCounter',
'$Adult',
'$BindingAuction',
'$CheckoutEnabled',
'$ConvertedBuyItNowPrice',
'$ConvertedStartPrice',
'$ConvertedReservePrice',
'$HasReservePrice',
$StartTime,
$EndTime,
'$ViewItemURL',
'$HasUnansweredQuestions',
'$HasPublicMessages',
'$ViewItemURLForNaturalSearch ',
'$LayoutID',
'$ThemeID',
'$ListingDuration',
'$ListingType',
'$Location',
'$PaymentMethods',
'$CategoryID',
'$CategoryName',
'$PrivateListing',
'$IncludeStockPhotoURL',
'$IncludePrefilledItemInformation',
'$UseStockPhotoURLAsGallery',
'$ProductReferenceID',
'$UPC',
'$EAN',
'$Brand',
'$MPN',
'$IncludeeBayProductDetails',
'$Quantity',
'$ReservePrice',
'$ItemRevised',
'$AboutMePage',
'$Email',
'$FeedbackScore',
'$PositiveFeedbackPercent',
'$FeedbackPrivate',
'$IDVerified',
'$eBayGoodStanding',
'$NewUser',
$RegistrationDate,
'$Site',
'$Status',
'$UserID',
'$UserIDChanged',
$UserIDLastChanged,
'$VATStatus',
'$AllowPaymentEdit',
'$Seller_CheckoutEnabled',
'$CIPBankAccountStored',
'$GoodStanding',
'$LiveAuctionAuthorized',
'$MerchandizingPref',
'$QualifiesForB2BVAT',
'$StoreOwner',
'$StoreURL',
'$SafePaymentExempt',
'$MotorsDealer',
'$BidCount',
'$BidIncrement',
'$SellerConvertedCurrentPrice',
'$CurrentPrice',
'$LeadCount',
'$MinimumToBid',
'$QuantitySold',
'$ReserveMet',
'$SecondChanceEligible',
'$ListingStatus',
'$QuantitySoldByPickupInStore',
'$ApplyShippingDiscount',
'$WeightMajor',
'$WeightMinor',
'$SalesTaxPercent',
'$ShippingIncludedInTax',
'$ShippingService',
'$ShippingServiceCost',
'$ShippingServicePriority',
'$ExpeditedService',
'$ShippingTimeMin',
'$ShippingTimeMax',
'$FreeShipping',
'$ShippingType',
'$ThirdPartyCheckout',
'$ShippingDiscountProfileID',
'$InternationalShippingDiscountProfileID',
'$ShipToLocations',
'$Seller_Site',
'$Title',
'$HitCount',
'$GalleryType',
'$GalleryURL',
'$PhotoDisplay',
'$PictureSource',
'$DispatchTimeMax',
'$IntangibleItem',
'$RefundOption',
'$Refund',
'$ReturnsWithinOption',
'$ReturnsWithin',
'$ReturnsAcceptedOption',
'$ReturnsAccepted',
'$ShippingCostPaidByOption',
'$ShippingCostPaidBy',
'$ConditionID',
'$ConditionDisplayName',
'$ShippingProfileID',
'$ReturnProfileID',
'$ReturnProfileName',
'$PaymentProfileID',
'$PaymentProfileName',
'$ShippingIrregular',
'$ShippingPackage',
'$ShipWeightMajor',
'$ShipWeightMinor',
'$HideFromSearch',
'$eBayPlus',
'$eBayPlusEligible',
'$IsSecureDescription',
'$RelistParentID'
)");
    $PictureURL =  @$response->Item->PictureDetails->PictureURL;
      foreach ($PictureURL as $URL) {
        $PicURL = $URL;
        //$MediaName = $Media->MediaName;
        $check_msg = $this->db->query("SELECT PIC_ID FROM LJ_NOTIFICATION_PIC WHERE PIC_URL = '$PicURL'")->result_array();
        if(count($check_msg) === 0){
          $this->db->query("INSERT INTO LJ_NOTIFICATION_PIC (
            PIC_ID,
            NOTIFICATION_ID,
            EBAY_ID,
            PIC_URL
          )
          VALUES(
          get_single_primary_key('LJ_NOTIFICATION_PIC','PIC_ID'),
          '$notification_id',
          '$ItemID',
          '$PicURL'
          )");
        }// end if(count($check_msg) === 0){
      }// end foreach ($MessageMedia as $Media) {

/*=====================================================
=            taking action on notification            =
=====================================================*/
if($NotificationEventName === 'ItemRevised' OR $NotificationEventName === 'ItemListed'){
  $check_sys_qty = $this->db->query("SELECT COUNT(1) LIST_QTY
        FROM LZ_BARCODE_MT B
       WHERE B.EBAY_ITEM_ID = '$ItemID'
         AND B.EXTENDEDORDERID IS NULL
         AND B.ORDER_ID IS NULL
         AND B.SALE_RECORD_NO IS NULL
         AND B.LZ_POS_MT_ID IS NULL
         AND B.LZ_PART_ISSUE_MT_ID IS NULL
         AND B.SHOPIFY_ORDER_ID IS NULL
         AND B.PULLING_ID IS NULL
         AND B.LZ_PART_ISSUE_MT_ID IS NULL
         AND B.ITEM_ADJ_DET_ID_FOR_OUT IS NULL")->result_array();
  $sys_qty = (int)$check_sys_qty[0]['LIST_QTY'];
  $active_qty = (int)$Quantity -( (int)$QuantitySold+(int)$QuantitySoldByPickupInStore);
  if($active_qty === $sys_qty){
    $this->db->query("UPDATE LJ_LISTING_NOTIFICATION SET VERIFIED = 1 ,SYS_QTY = '$sys_qty' WHERE NOTIFICATION_ID = '$notification_id'");
  }elseif ($active_qty > $sys_qty) {
    $this->db->query("UPDATE LJ_LISTING_NOTIFICATION SET VERIFIED = 2 ,SYS_QTY = '$sys_qty' WHERE NOTIFICATION_ID = '$notification_id'");
    $remarks  = 'Adjusted Through Notification Call';
    $this->reviseEbayQty('$ItemID',$sys_qty,'$remarks',81,'$notification_id');
  }elseif ($active_qty < $sys_qty) {
    $this->db->query("UPDATE LJ_LISTING_NOTIFICATION SET VERIFIED = 3 ,SYS_QTY = '$sys_qty' WHERE NOTIFICATION_ID = '$notification_id'");
  }else{
    $this->db->query("UPDATE LJ_LISTING_NOTIFICATION SET VERIFIED = 4 ,SYS_QTY = '$sys_qty' WHERE NOTIFICATION_ID = '$notification_id'");
  }
}//if($NotificationEventName != 'ItemSold'){
/*=====  End of taking action on notification  ======*/
}//end if($merchant_name == 'dfwonline' OR $merchant_name == 'techbargains2015'){
    
  }
  public function reviseEbayQty($ebay_id,$quantity,$remarks='',$user_id=81,$notification_id){
  //public function reviseEbayQty($ebay_id = '264714384323',$quantity=0,$remarks='',$user_id=81,$notification_id=3358){

    $result['ebay_id'] = $ebay_id;
    $result['remarks'] = $remarks;
    $result['user_id'] = $user_id;
    $result['quantity'] = $quantity;
    $result['notification_id'] = $notification_id;

    $get_seller = $this->db->query("SELECT E.LZ_SELLER_ACCT_ID, A.ACCOUNT_NAME SELL_ACCT_DESC , S.EBAY_LOCAL FROM EBAY_LIST_MT E, LJ_MERHCANT_ACC_DT A , LZ_ITEM_SEED S WHERE A.ACCT_ID = E.LZ_SELLER_ACCT_ID AND S.SEED_ID = E.SEED_ID AND E.EBAY_ITEM_ID =  '$ebay_id' AND ROWNUM = 1")->result_array(); 
    if(count($get_seller) === 0){
      $get_seller = $this->db->query("SELECT N.MERCHANT_NAME SELL_ACCT_DESC, A.LZ_SELLER_ACCT_ID  , DECODE(N.SELLER_SITE,'US','0','MOTOR','100',N.SELLER_SITE) EBAY_LOCAL
  FROM LJ_LISTING_NOTIFICATION N, LZ_SELLER_ACCTS A
 WHERE N.MERCHANT_NAME = A.SELL_ACCT_DESC
   AND N.ITEMID = '$ebay_id' AND ROWNUM = 1")->result_array(); 
    }
    if(count($get_seller) === 0){
      $message = 'Ebay Id not found in System';
      $this->db->query("CALL PRO_TABLE_TRIGGER_LOG('LJ_LISTING_NOTIFICATION PK: $notification_id', '$message', '0', '81')");
      return array('Ack'=>'Failure','message'=>$message);

    }

    //var_dump($get_seller);exit;
    $account_name = @$get_seller[0]['LZ_SELLER_ACCT_ID'];
    
    if(!empty(@$get_seller[0]['EBAY_LOCAL'])){
      $site_id = @$get_seller[0]['EBAY_LOCAL'];
    }else{
      $site_id = 0;
    }
    $result['site_id'] = $site_id;
    if(!empty(@$account_name)){
      $result['account_name'] = $account_name;
    }
    if((int)$quantity> 0){
      $data = $this->load->view('ebay/trading/reviseQtyNotification',$result,true);
    }else{
      $data = $this->load->view('ebay/trading/endItemNotification',$result,true);
    }
    
    $revItemResponse =  json_encode( json_decode(htmlspecialchars_decode($data), true));
    //$jason = json_decode($trackingUpdated,true); for making it as Array
    $json = json_decode($revItemResponse); // For making it as an Object
    //var_dump($json,$json->ack);
    if($json->ack !== 'Failure'){
      //echo json_encode(array('Ack'=>'Success','message'=>'Item Qty Revised Sucessfully'));
      return array('Ack'=>'Success','message'=>$json->message);
    }else{
      //echo json_encode(array('Ack'=>'Failure','message'=>$json->message));
      return array('Ack'=>'Failure','message'=>$json->message);
    }
  }
  public function reviseQtyDirect(){
    $this->load->view('ebay/trading/reviseQtyDirect');
  }
  public function getMerchantList(){
    $res = $this->m_cron_job->getMerchantList();
    echo json_encode($res);
    return $res;
  }
  public function GetSellerOrdersApp()
  {
    $sellerId = $this->uri->segment(4);
    $Days = $this->uri->segment(5);
    $result['sellerId']=$sellerId;
    $result['Days']=$Days;
    //var_dump($sellerId,$Days);exit;
    $this->load->view('API/GetOrders/GetSellerOrders',$result);
  }
  public function getSingleOrderByApp()
  {
    $result['LZ_SELLER_ACCT_ID']= $this->input->post("SELLER_ID");
    $result['ORDER_ID']= $this->input->post("ORDER_ID");
    
    //var_dump($result);exit;

    //$result['LZ_SELLER_ACCT_ID']= '2';
    //$result['ORDER_ID']= '254243058755-2473168825015';// '07-03528-19848'
    $data = $this->load->view('API/GetOrders/getSingleOrderByIdApp',$result,true);

    $res =  json_encode( json_decode(htmlspecialchars_decode($data), true));
    $json_res = json_decode($res); // For making it as an Object
    //var_dump($json_res->ack[0],$json_res->message);exit;
    $res  = array('ack'=>$json_res->ack[0],"message" => $json_res->message);
    echo json_encode($res);
    return json_encode($res);
  }
  public function getEbayTime()
  {
    $result['account_name'] = 1;
    $this->load->view('ebay/trading/01-get-ebay-official-time',$result);
  }
  public function DelEmptyFolderInMasterDir()//FOR master dir PIC ONLY
    {
        $path_id = 1;
        $i = 1;
        $query = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = '$path_id'");
        $master_qry = $query->result_array();
        $base_url = $master_qry[0]['MASTER_PATH'];
        //$base_url = "D:/wamp/www/item_pictures/test_del/";
        $directories = glob($base_url . '*', GLOB_ONLYDIR);
        foreach ($directories as $sub_dir) {
            $sub_directories = glob($sub_dir . '/*', GLOB_ONLYDIR);
            // var_dump("Sajid" . count($sub_directories));
            // if(count($sub_directories) === 0){
            //   echo $i. '-' . $sub_dir.PHP_EOL;
            //       rmdir($sub_dir);
            //       $i++;
            //   continue;
            // }
            // $sub_directories = glob($sub_directories[0] . '/*', GLOB_ONLYDIR);
            // //var_dump("Sajid" . count($sub_directories));
            // if(count($sub_directories) === 0){
            //   echo $i. '-' . $sub_directories[0].PHP_EOL;
            //       rmdir($sub_directories[0]);
            //       $i++;
            //   continue;
            // }
                        $dir = $sub_directories[0];
            //foreach ($sub_directories as $dir) {

            if (is_dir($dir)) {
                $condition_name = basename($dir);
                //$folder_dir = $folder_name.'/'.$condition_name;
                //var_dump($dir);
                $folder_dir = $dir;
                if (is_dir(@$folder_dir)) {
                    $iterator = new \FilesystemIterator(@$folder_dir);
                    if (@$iterator->valid()) {
                        $m_flag = true;
                    } else {
                        $m_flag = false;
                    }
                } else {
                    $m_flag = false;
                }
                if($m_flag === true){
                  //echo $i. '-' . $folder_dir.PHP_EOL;
                  //rmdir($folder_dir);
                  //@rmdir($sub_dir);
                  //$i++;
                }else{
echo $i. '-' . $folder_dir.PHP_EOL;
//echo $i. '-' . $sub_dir.PHP_EOL;
@rmdir($folder_dir);
//sleep(5);
//if(rmdir($folder_dir)){
  //rmdir($sub_dir);
//}

$i++;
                }
              }
        }  
foreach ($directories as $sub_dir) {
            // $sub_directories = glob($sub_dir . '/*', GLOB_ONLYDIR);
            // var_dump($sub_dir . ' count: '.count($sub_directories));
            // if(count($sub_directories) === 0){
              echo $i. '-' . $sub_dir.PHP_EOL;
                  @rmdir($sub_dir);
                  $i++;
              //continue;
           // }
            // $sub_directories = glob($sub_directories[0] . '/*', GLOB_ONLYDIR);
            // //var_dump("Sajid" . count($sub_directories));
            // if(count($sub_directories) === 0){
            //   echo $i. '-' . $sub_directories[0].PHP_EOL;
            //       rmdir($sub_directories[0]);
            //       $i++;
            //   continue;
            // }
          }
          exit;
//             $folder_name = basename($sub_dir);
//             //$sub_dir ='D:/wamp/www/item_pictures/master_pictures/678149439229~43922-DVD/abc.jpg';
//             //$pathinfo = date("m/d/Y H:i:s", fileatime($sub_dir));
//             //$pathinfo = pathinfo($sub_dir,PATHINFO_FILENAME );
//             //$pathinfo = pathinfo($sub_dir,PATHINFO_EXTENSION  );
//             //$fileatime = date("m/d/Y H:i:s", fileatime($sub_dir));
//             //$filesize = filesize($file_dir); // bytes
//             //$filetype = filetype($sub_dir);// dir or file
//             //$is_file = is_file($sub_dir); // return bool
//             //var_dump($pathinfo);exit;
//             $dir = $sub_directories[0];
//             //foreach ($sub_directories as $dir) {

//             if (is_dir($dir)) {
//                 $condition_name = basename($dir);
//                 //$folder_dir = $folder_name.'/'.$condition_name;
//                 //var_dump($dir);
//                 $folder_dir = $dir;
//                 if (is_dir(@$folder_dir)) {
//                     $iterator = new \FilesystemIterator(@$folder_dir);
//                     if (@$iterator->valid()) {
//                         $m_flag = true;
//                     } else {
//                         $m_flag = false;
//                     }
//                 } else {
//                     $m_flag = false;
//                 }
//                 if($m_flag === true){
//                   //echo $i. '-' . $folder_dir.PHP_EOL;
//                   //rmdir($folder_dir);
//                   //@rmdir($sub_dir);
//                   //$i++;
//                 }else{
// echo $i. '-' . $folder_dir.PHP_EOL;
// echo $i. '-' . $sub_dir.PHP_EOL;
// rmdir($folder_dir);
// //sleep(5);
// //if(rmdir($folder_dir)){
//   rmdir($sub_dir);
// //}

// $i++;
//                 }
                
//                 //var_dump($folder_dir);//exit;
                
//             }//if (is_dir($dir)) {
//             //}//foreach ($leaf_dir as $dir) {
//             //}//foreach ($sub_directories as $dir) {
//         } //foreach ($directories as $dir) {
    }
    public function copyImgOfPurchaseItem()//FOR master dir PIC ONLY
    {
      $query = $this->db->query("SELECT BARCODE_NO , M.UPC , M.MPN , M.HISTORY_STATUS ,M.CONDITION , C.COND_NAME FROM LZ_PURCH_ITEM_DT D, LZ_PURCH_ITEM_MT M , LZ_ITEM_COND_MT C
WHERE M.PURCH_MT_ID = D.PURCH_MT_ID
AND D.BARCODE_NO NOT IN (SELECT BARCODE_NO  FROM LZ_BARCODE_MT BB WHERE BB.EBAY_ITEM_ID IS NOT NULL)
AND D.BARCODE_NO NOT IN (SELECT BARCODE_PRV_NO FROM LZ_SPECIAL_LOTS L)
AND M.CONDITION = C.ID")->result_array();
      foreach ($query as $val) {
        $s_upc = $val['UPC'];
        $s_mpn = $val['MPN'];
        $d_mpn = $val['MPN'];
        //$barcode_no = $val['BARCODE_NO'];
        $cond = $val['CONDITION'];
        $cond_name = $val['COND_NAME'];

        $seed_qry = $this->db->query("SELECT S.F_UPC , S.F_MPN
                                  FROM LZ_ITEM_SEED S
                                  WHERE (S.F_UPC = '$s_upc' OR UPPER(S.F_MPN) = UPPER('$s_mpn'))
                                   AND S.DEFAULT_COND = '$cond'
                                  ORDER BY S.DATE_TIME DESC FETCH FIRST 1 ROW ONLY
                                  ")->result_array();
        if(count($seed_qry) > 0){
            $s_upc = $seed_qry[0]['F_UPC'];
            $s_mpn = $seed_qry[0]['F_MPN'];
            $d_mpn = $seed_qry[0]['F_MPN'];
        }        

       // $folder_name = $s_upc .'~'. $s_mpn;
        //$cond_folder = $s_upc .'~'. $s_mpn . '/'.$cond_name;

        $path_id = 1;
        //$i = 1;
        $query = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = '$path_id'");
        $master_qry = $query->result_array();
        $master_path = $master_qry[0]['MASTER_PATH'];
$thumb_dir = false;
// -------------
        $s_mpn = str_replace('/', '_', @$s_mpn);
    $dir1 = $master_path.@$s_upc.'~'.$s_mpn;
    $dir2 = $master_path.@$s_upc.'~'.$s_mpn.'/'.@$cond_name;
    $dir3 = $master_path.@$s_upc.'~'.$s_mpn.'/'.@$cond_name.'/thumb/';
    $dir = $master_path.@$s_upc.'~'.$s_mpn.'/'.@$cond_name.'/thumb/';
    $dir = preg_replace("/[\r\n]*/","",$dir);
    $copy_img = false;
    $copy_thumb_dir = false;
              if (is_dir(@$dir)) {
                $thumb_dir = true;
                  $iterator = new \FilesystemIterator(@$dir);
                  if (@$iterator->valid()) {
                      $m_flag = true;
                  } else {
                      $m_flag = false;
                  }
              } else {
                  $m_flag = false;
              }
              if($m_flag === true){
                //echo $i. '-' . $folder_dir.PHP_EOL;
                //rmdir($folder_dir);
                //@rmdir($sub_dir);
                //$i++;
                
              }else{
                  if($thumb_dir){
                    $iterator = new \FilesystemIterator(@$dir2);
                  if (@$iterator->valid()) {
                      $m_flag = true;
                      $copy_thumb_dir = true;
                  } else {
                      $m_flag = false;
                  }
                }//if($thumb_dir){
                  
                
                $copy_img = true;
              }

    if ($copy_img) {

  // end if (is_dir($dir)) {
      $dekit = $this->db->query("SELECT *
  FROM (SELECT L.FOLDER_NAME, L.SPECIAL_LOT_ID ORDER_ID
          FROM LZ_SPECIAL_LOTS L
         WHERE (L.CARD_UPC = '$s_upc' OR
               UPPER(L.CARD_MPN) = UPPER('$d_mpn'))
           AND L.FOLDER_NAME IS NOT NULL
        UNION ALL
        SELECT DT.FOLDER_NAME, DT.LZ_DEKIT_US_DT_ID ORDER_ID
          FROM LZ_DEKIT_US_DT DT, LZ_CATALOGUE_MT C
         WHERE C.CATALOGUE_MT_ID = DT.CATALOG_MT_ID
           AND (C.UPC = '$s_upc' OR UPPER(C.MPN) = UPPER('$d_mpn'))
           AND DT.FOLDER_NAME IS NOT NULL
        
        )
 ORDER BY ORDER_ID DESC fetch first 1 row only
")->result_array();
      if(count($dekit)>0){
        $folder_name = $dekit[0]['FOLDER_NAME'];
        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
        $path = $path->result_array();    
        $master_path = $path[0]["MASTER_PATH"];
        //$master_path.@$row['UPC'].'~'.$mpn.'/'.@$it_condition
        $dir = $master_path.$folder_name.'/thumb/';
        $src1 = $master_path.$folder_name;
        $src2 = $master_path.$folder_name.'/thumb/';
        if(!$thumb_dir){
          @mkdir($dir1); 
          @mkdir($dir2); 
          @mkdir($dir3); 
        }
        if($copy_thumb_dir){
          $this->custom_copy($src2,$dir3);
        }else{
        $this->custom_copy($src1,$dir2);
        $this->custom_copy($src2,$dir3);
        }
        
      }
    }
// ---------------





      }//foreach ($query as $val) {
      
    }
 public function testhttp(){
  echo file_get_contents('https://71.78.236.20/laptopzone/tolist/c_tolist/reviseItemPrice', false, stream_context_create([
    'http' => [
        'method' => 'POST',
        'header'  => "Content-type: application/x-www-form-urlencoded",
        'content' => http_build_query([
            'ebay_id' => '264738693755', 'revise_price' => '1998'
        ])
    ]
]));
 }
 public function testimg(){
  $dir = "E:\\test_img\\163377";
  if (is_dir($dir)) {
        }else{
          mkdir($dir);
        }
 //var_dump(getimagesize("https://i.ebayimg.com/00/s/NjY3WDEwMDA=/z/~zoAAOSwWa1b2ZqC/\$_1.JPG?set_id=2"));
$pic_url = "https://i.ebayimg.com/00/s/NjY3WDEwMDA=/z/~zoAAOSwWa1b2ZqC/\$_1.JPG?set_id=2";

// $url = 'http://example.com/image.php';
// $img = '/my/folder/flower.gif';
//file_put_contents($dir, file_get_contents($pic_url));

// $ch = curl_init($pic_url);
// //$fp = fopen($dir, 'wb');
// $fp = fopen($dir, 'a+');
// curl_setopt($ch, CURLOPT_FILE, $fp);
// curl_setopt($ch, CURLOPT_HEADER, 0);
// curl_exec($ch);
// curl_close($ch);
// fclose($fp);

// exit;

   list($width, $height, $type, $attr) = getimagesize($pic_url);

echo "Width: " .$width. "<br />";
echo "Height: " .$height. "<br />";
echo "Type: " .$type. "<br />";
echo "Attribute: " .$attr. "<br />";

if($width > 100 and $height > 100){
  copy($pic_url, $dir."/image.jpg");
}else{
 echo "else <br />" ; 
}

$pic_url = "https://i.ebayimg.com/00/s/OTYwWDcyMA==/z/y2oAAOSweMJfTfFB/\$_1.JPG?set_id=2";
list($width, $height, $type, $attr) = getimagesize($pic_url);

echo "Width: " .$width. "<br />";
echo "Height: " .$height. "<br />";
echo "Type: " .$type. "<br />";
echo "Attribute: " .$attr. "<br />";

if($width > 100 and $height > 100){
  copy($pic_url, $dir."/image.jpg");
}else{
 echo "2nd <br />" ;
}

// Type of image consider like -

// 1 = GIF
// 2 = JPG
// 3 = PNG
// 4 = SWF
// 5 = PSD
// 6 = BMP
// 7 = TIFF(intel byte order)
// 8 = TIFF(motorola byte order)
// 9 = JPC
// 10 = JP2
// 11 = JPX
// 12 = JB2
// 13 = SWC
// 14 = IFF
// 15 = WBMP
// 16 = XBM
//Using array
$arr = array('h' => $height, 'w' => $width, 't' => $type, 'a' => $attr);
 }
 public function fetchImagefromEbayURL(){
//   $qry = $this->db->query("
// select distinct * from (select l.folder_name, 
// dd.*
//   from lz_special_lots l, lz_barcode_mt b ,(select mt.item_id,

//        dt.pic_url
//        from site_hosted_pic_mt mt , site_hosted_pic_dt dt
// where mt.pic_mt_id = dt.pic_mt_id
// order by mt.pic_mt_id asc)dd
//  where b.barcode_no = l.barcode_prv_no
//   and b.item_id = dd.item_id

// union all

// select   l.folder_name,
// dd.*
//   from lz_dekit_us_dt l, lz_barcode_mt b ,(select mt.item_id,

//        dt.pic_url
//        from site_hosted_pic_mt mt , site_hosted_pic_dt dt
// where mt.pic_mt_id = dt.pic_mt_id
// order by mt.pic_mt_id asc)dd
//  where b.barcode_no = l.barcode_prv_no
//  and b.item_id = dd.item_id
//  )
//  where folder_name is not null
//  and length(folder_name) <= 6
//  order by folder_name desc  
// ")->result_array();  
//   $qry = $this->db->query("select t.folder_name,
//        'https://i.ebayimg.com/images/g/' ||
//        substr(t.pic_url, instr(t.pic_url, '/', 1, 7) + 1, 16) ||
//        '/s-l1600.jpg' full_url,
//        'https://i.ebayimg.com/images/g/' ||
//        substr(t.pic_url, instr(t.pic_url, '/', 1, 7) + 1, 16) ||
//        '/s-l100.jpg' thumb_url
//   from dekit_img_temp t
//  where t.attempted = 0
//  order by t.folder_name desc;
// ")->result_array(); 
$qry = $this->db->query("select * from dekit_img_temp t where t.attempted = 0 ORDER BY T.FOLDER_NAME DESC")->result_array();
//$qry = $this->db->query("select * from dekit_img_temp t where T.FOLDER_NAME = 123027")->result_array();
$azRange = range('A', 'Z');
$this->load->library('image_lib');
foreach ($qry as $value) {
  # code...


$item_id = $value['ITEM_ID'];
$condition_id = $value['CONDITION_ID'];
$folder_name = $value['FOLDER_NAME'];
$sub_qry = $this->db->query("select dt.pic_dt_id , 'https://i.ebayimg.com/images/g/' ||
       substr(dt.pic_url, instr(dt.pic_url, '/', 1, 7) + 1, 16) ||
       '/s-l1600.jpg' full_url,
       'https://i.ebayimg.com/images/g/' ||
       substr(dt.pic_url, instr(dt.pic_url, '/', 1, 7) + 1, 16) ||
       '/s-l100.jpg' thumb_url
       from site_hosted_pic_mt mt , site_hosted_pic_dt dt
where mt.pic_mt_id = dt.pic_mt_id
and mt.item_id = '$item_id'
and mt.condition_id = '$condition_id'
order by dt.pic_dt_id asc")->result_array();

if(count($sub_qry) > 0){
  $i = 0;
  foreach ($sub_qry as $val) {
   $full_url = $val['FULL_URL'];
   $thumb_url = $val['THUMB_URL'];
   list($width, $height, $type, $attr) = getimagesize($full_url);
    if($width > 150 and $height > 150){
      $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
      $img_name = '';
      $extension = 'jpg';
      $max = strlen($characters) - 1;
      for ($k = 0; $k < 10; $k++) {
          $img_name .= $characters[mt_rand(0, $max)];

      }
      $img_complete_name = $azRange[$i] . '_' . $img_name . '.' . $extension;

      $dir = "E:\\test_img\\".$folder_name;
      $thumb_dir = "E:\\test_img\\".$folder_name."\\thumb";
      if (is_dir($dir)) {
      }else{
        mkdir($dir);
      }
      if (is_dir($thumb_dir)) {
      }else{
        mkdir($thumb_dir);
      }
      copy($full_url, $dir.'\\'.$img_complete_name);
      //copy($thumb_url, $thumb_dir.'\\'.$img_complete_name);
      /*====================================
      =            image thumbnail creation            =
      ====================================*/
      $config['image_library'] = 'GD2';
      $config['source_image'] = $dir . "/" . $img_complete_name;
      $config['new_image'] = $thumb_dir . "/" . $azRange[$i] . '_' . $img_name . '.' . $extension;
      $config['maintain_ratio'] = true;
      $config['width'] = 100;
      $config['height'] = 100;

      //$config['quality']     = 50; this filter doesnt work
      $in = $this->image_lib->initialize($config);
      $result = $this->image_lib->resize($in);
      $this->image_lib->clear();

      /*=====  End of image thumbnail creation  ======*/

      
      $i++;
      $this->db->query("UPDATE DEKIT_IMG_TEMP T SET T.DOWNLOADED = 1 WHERE T.ITEM_ID = '$item_id' AND T.CONDITION_ID = '$condition_id' AND T.FOLDER_NAME = '$folder_name'");
    }else{
      $this->db->query("UPDATE DEKIT_IMG_TEMP T SET T.ATTEMPTED = 1 WHERE T.ITEM_ID = '$item_id' AND T.CONDITION_ID = '$condition_id' AND T.FOLDER_NAME = '$folder_name'");
      continue 1;
    }


}// end foreach ($sub_qry as $val) {
}else{//if(count() > 0){
  
}

$this->db->query("UPDATE DEKIT_IMG_TEMP T SET T.ATTEMPTED = 1 WHERE T.ITEM_ID = '$item_id' AND T.CONDITION_ID = '$condition_id' AND T.FOLDER_NAME = '$folder_name'");
}//end foreach ($qry as $value) {
/*=====  End of copy code from item_pictures  ======*/



 }
  public function fetchImagefromEbayURL_AIO(){

$qry = $this->db->query("select * from dekit_img_temp_AIO t where t.attempted = 0 ORDER BY T.PIC_DT_ID DESC")->result_array();

$i=0;
$j=0;
$dir = "E:\\test_img_aio";
if (is_dir($dir)) {
}else{
  mkdir($dir);
}
foreach ($qry as $value) {
    # code...
  $rest_j = 0;
  $PIC_DT_ID = $value['PIC_DT_ID'];
  $PIC_MT_ID = $value['PIC_MT_ID'];
  $FULL_URL = $value['FULL_URL'];
  $extension = 'jpg';
  if($PIC_MT_ID != $qry[$i+1]['PIC_MT_ID']){
    $rest_j = 1;
  }
  $j++;
  $img_complete_name = $PIC_MT_ID. '_' . $j . '.' . $extension;
  if(copy($FULL_URL, $dir.'\\'.$img_complete_name)){
    $this->db->query("UPDATE dekit_img_temp_AIO T SET T.DOWNLOADED = 1,T.ATTEMPTED = 1 , UPDATED = sysdate WHERE T.PIC_DT_ID = '$PIC_DT_ID'");
  }else{
    $this->db->query("UPDATE dekit_img_temp_AIO T SET T.ATTEMPTED = 1 WHERE T.PIC_DT_ID = '$PIC_DT_ID'");
  }

  if($rest_j){
    $j=0;
  }
  $i++;
}//foreach ($qry as $value) {


 }
public function runfetchImagefromEbayURL_AIO_Chunk($min_url_id,$max_url_id)
  {
    if(!empty($min_url_id) && !empty($max_url_id)){
      $qry = $this->db->query("select * from dekit_img_temp_AIO t where t.attempted = 0 and PIC_DT_ID BETWEEN $min_url_id and $max_url_id   ORDER BY T.PIC_DT_ID DESC")->result_array();

      $i=0;
      $j=0;
      $dir = "E:\\test_img_aio";
      if (is_dir($dir)) {
      }else{
        mkdir($dir);
      }
      foreach ($qry as $value) {
          # code...
        $rest_j = 0;
        $PIC_DT_ID = $value['PIC_DT_ID'];
        $PIC_MT_ID = $value['PIC_MT_ID'];
        $FULL_URL = $value['FULL_URL'];
        $extension = 'jpg';
        if($PIC_MT_ID != $qry[$i+1]['PIC_MT_ID']){
          $rest_j = 1;
        }
        $j++;
        $img_complete_name = $PIC_MT_ID. '_' . $j . '.' . $extension;
        if(copy($FULL_URL, $dir.'\\'.$img_complete_name)){
          $this->db->query("UPDATE dekit_img_temp_AIO T SET T.DOWNLOADED = 1,T.ATTEMPTED = 1 , UPDATED = sysdate WHERE T.PIC_DT_ID = '$PIC_DT_ID'");
        }else{
          $this->db->query("UPDATE dekit_img_temp_AIO T SET T.ATTEMPTED = 1 WHERE T.PIC_DT_ID = '$PIC_DT_ID'");
        }

        if($rest_j){
          $j=0;
        }
        $i++;
      }//foreach ($qry as $value) {
     }else{
       echo 'feed_url_id not found';
     }
  }
    public function createImageDownloadfromEbay_chunk()
  {
      $this->m_cron_job->createImageDownloadfromEbay_chunk();
  }
    public function createImageDownloadEbay_chunk()
  {
      $this->m_cron_job->createImageDownloadEbay_chunk();
  }
  public function fetchImagefromEbayURL_chunk($min_url_id,$max_url_id){
//   $qry = $this->db->query("
// select distinct * from (select l.folder_name, 
// dd.*
//   from lz_special_lots l, lz_barcode_mt b ,(select mt.item_id,

//        dt.pic_url
//        from site_hosted_pic_mt mt , site_hosted_pic_dt dt
// where mt.pic_mt_id = dt.pic_mt_id
// order by mt.pic_mt_id asc)dd
//  where b.barcode_no = l.barcode_prv_no
//   and b.item_id = dd.item_id

// union all

// select   l.folder_name,
// dd.*
//   from lz_dekit_us_dt l, lz_barcode_mt b ,(select mt.item_id,

//        dt.pic_url
//        from site_hosted_pic_mt mt , site_hosted_pic_dt dt
// where mt.pic_mt_id = dt.pic_mt_id
// order by mt.pic_mt_id asc)dd
//  where b.barcode_no = l.barcode_prv_no
//  and b.item_id = dd.item_id
//  )
//  where folder_name is not null
//  and length(folder_name) <= 6
//  order by folder_name desc  
// ")->result_array();  
//   $qry = $this->db->query("select t.folder_name,
//        'https://i.ebayimg.com/images/g/' ||
//        substr(t.pic_url, instr(t.pic_url, '/', 1, 7) + 1, 16) ||
//        '/s-l1600.jpg' full_url,
//        'https://i.ebayimg.com/images/g/' ||
//        substr(t.pic_url, instr(t.pic_url, '/', 1, 7) + 1, 16) ||
//        '/s-l100.jpg' thumb_url
//   from dekit_img_temp t
//  where t.attempted = 0
//  order by t.folder_name desc;
// ")->result_array(); 
$qry = $this->db->query("SELECT * FROM DEKIT_IMG_TEMP T WHERE T.ATTEMPTED = 0 AND T.FOLDER_NAME BETWEEN '$min_url_id' and '$max_url_id' ORDER BY T.FOLDER_NAME ASC")->result_array();
//$qry = $this->db->query("select * from dekit_img_temp t where T.FOLDER_NAME = 123027")->result_array();
$azRange = range('A', 'Z');
$this->load->library('image_lib');
foreach ($qry as $value) {
  # code...


$item_id = $value['ITEM_ID'];
$condition_id = $value['CONDITION_ID'];
$folder_name = $value['FOLDER_NAME'];
$sub_qry = $this->db->query("select dt.pic_dt_id , 'https://i.ebayimg.com/images/g/' ||
       substr(dt.pic_url, instr(dt.pic_url, '/', 1, 7) + 1, 16) ||
       '/s-l1600.jpg' full_url,
       'https://i.ebayimg.com/images/g/' ||
       substr(dt.pic_url, instr(dt.pic_url, '/', 1, 7) + 1, 16) ||
       '/s-l100.jpg' thumb_url
       from site_hosted_pic_mt mt , site_hosted_pic_dt dt
where mt.pic_mt_id = dt.pic_mt_id
and mt.item_id = '$item_id'
and mt.condition_id = '$condition_id'
order by dt.pic_dt_id asc")->result_array();

if(count($sub_qry) > 0){
  $i = 0;
  foreach ($sub_qry as $val) {
   $full_url = $val['FULL_URL'];
   $thumb_url = $val['THUMB_URL'];
   list($width, $height, $type, $attr) = getimagesize($full_url);
    if($width > 150 and $height > 150){
      $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
      $img_name = '';
      $extension = 'jpg';
      $max = strlen($characters) - 1;
      for ($k = 0; $k < 10; $k++) {
          $img_name .= $characters[mt_rand(0, $max)];

      }
      $img_complete_name = $azRange[$i] . '_' . $img_name . '.' . $extension;

      $dir = "D:\\test_img\\".$folder_name;
      $thumb_dir = "E:\\test_img\\".$folder_name."\\thumb";
      if (is_dir($dir)) {
      }else{
        mkdir($dir);
      }
      if (is_dir($thumb_dir)) {
      }else{
        mkdir($thumb_dir);
      }
      copy($full_url, $dir.'\\'.$img_complete_name);
      //copy($thumb_url, $thumb_dir.'\\'.$img_complete_name);
      /*====================================
      =            image thumbnail creation            =
      ====================================*/
      $config['image_library'] = 'GD2';
      $config['source_image'] = $dir . "/" . $img_complete_name;
      $config['new_image'] = $thumb_dir . "/" . $azRange[$i] . '_' . $img_name . '.' . $extension;
      $config['maintain_ratio'] = true;
      $config['width'] = 100;
      $config['height'] = 100;

      //$config['quality']     = 50; this filter doesnt work
      $in = $this->image_lib->initialize($config);
      $result = $this->image_lib->resize($in);
      $this->image_lib->clear();

      /*=====  End of image thumbnail creation  ======*/

      
      $i++;
      $this->db->query("UPDATE DEKIT_IMG_TEMP T SET T.DOWNLOADED = 1 WHERE T.ITEM_ID = '$item_id' AND T.CONDITION_ID = '$condition_id' AND T.FOLDER_NAME = '$folder_name'");
    }else{
      $this->db->query("UPDATE DEKIT_IMG_TEMP T SET T.ATTEMPTED = 1 WHERE T.ITEM_ID = '$item_id' AND T.CONDITION_ID = '$condition_id' AND T.FOLDER_NAME = '$folder_name'");
      continue 1;
    }


}// end foreach ($sub_qry as $val) {
}else{//if(count() > 0){
  
}

$this->db->query("UPDATE DEKIT_IMG_TEMP T SET T.ATTEMPTED = 1 WHERE T.ITEM_ID = '$item_id' AND T.CONDITION_ID = '$condition_id' AND T.FOLDER_NAME = '$folder_name'");
}//end foreach ($qry as $value) {
/*=====  End of copy code from item_pictures  ======*/



 }
public function fetchMasterImagefromEbayURL(){
//   $qry = $this->db->query("
// select distinct * from (select l.folder_name, 
// dd.*
//   from lz_special_lots l, lz_barcode_mt b ,(select mt.item_id,

//        dt.pic_url
//        from site_hosted_pic_mt mt , site_hosted_pic_dt dt
// where mt.pic_mt_id = dt.pic_mt_id
// order by mt.pic_mt_id asc)dd
//  where b.barcode_no = l.barcode_prv_no
//   and b.item_id = dd.item_id

// union all

// select   l.folder_name,
// dd.*
//   from lz_dekit_us_dt l, lz_barcode_mt b ,(select mt.item_id,

//        dt.pic_url
//        from site_hosted_pic_mt mt , site_hosted_pic_dt dt
// where mt.pic_mt_id = dt.pic_mt_id
// order by mt.pic_mt_id asc)dd
//  where b.barcode_no = l.barcode_prv_no
//  and b.item_id = dd.item_id
//  )
//  where folder_name is not null
//  and length(folder_name) <= 6
//  order by folder_name desc  
// ")->result_array();  
//   $qry = $this->db->query("select t.folder_name,
//        'https://i.ebayimg.com/images/g/' ||
//        substr(t.pic_url, instr(t.pic_url, '/', 1, 7) + 1, 16) ||
//        '/s-l1600.jpg' full_url,
//        'https://i.ebayimg.com/images/g/' ||
//        substr(t.pic_url, instr(t.pic_url, '/', 1, 7) + 1, 16) ||
//        '/s-l100.jpg' thumb_url
//   from dekit_img_temp t
//  where t.attempted = 0
//  order by t.folder_name desc;
// ")->result_array(); 
$qry = $this->db->query("SELECT * FROM MASTER_IMG_TEMP T WHERE T.ATTEMPTED = 0 ")->result_array();
//$qry = $this->db->query("select * from MASTER_IMG_TEMP t where T.FOLDER_NAME = 123027")->result_array();
$azRange = range('A', 'Z');
$this->load->library('image_lib');
   if (is_dir("D:\\test_master_img\\")) {
      }else{
        mkdir("D:\\test_master_img\\");
      }
foreach ($qry as $value) {
  # code...


$item_id = $value['ITEM_ID'];
$condition_id = $value['CONDITION_ID'];
$folder_name = $value['FOLDER_NAME'];
$cond_name = $value['COND_NAME'];
$folder_dir = $value['FOLDER_DIR'];
$sub_qry = $this->db->query("select dt.pic_dt_id , 'https://i.ebayimg.com/images/g/' ||
       substr(dt.pic_url, instr(dt.pic_url, '/', 1, 7) + 1, 16) ||
       '/s-l1600.jpg' full_url,
       'https://i.ebayimg.com/images/g/' ||
       substr(dt.pic_url, instr(dt.pic_url, '/', 1, 7) + 1, 16) ||
       '/s-l100.jpg' thumb_url
       from site_hosted_pic_mt mt , site_hosted_pic_dt dt
where mt.pic_mt_id = dt.pic_mt_id
and mt.item_id = '$item_id'
and mt.condition_id = '$condition_id'
order by dt.pic_dt_id asc")->result_array();

if(count($sub_qry) > 0){
  $i = 0;
  foreach ($sub_qry as $val) {
   $full_url = $val['FULL_URL'];
   $thumb_url = $val['THUMB_URL'];
   list($width, $height, $type, $attr) = getimagesize($full_url);
    if($width > 150 and $height > 150){
      $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
      $img_name = '';
      $extension = 'jpg';
      $max = strlen($characters) - 1;
      for ($k = 0; $k < 10; $k++) {
          $img_name .= $characters[mt_rand(0, $max)];

      }
      $img_complete_name = $azRange[$i] . '_' . $img_name . '.' . $extension;

      $dir = "D:\\test_master_img\\".$folder_name;
      if (is_dir($dir)) {
      }else{
        mkdir($dir);
      }
      $dir_cond = "D:\\test_master_img\\".$folder_name."\\".$cond_name;
      if (is_dir($dir_cond)) {
      }else{
        mkdir($dir_cond);
      }
      $thumb_dir = $dir_cond."\\thumb";
      
      if (is_dir($thumb_dir)) {
      }else{
        mkdir($thumb_dir);
      }
      copy($full_url, $dir_cond.'\\'.$img_complete_name);
      //copy($thumb_url, $thumb_dir.'\\'.$img_complete_name);
      /*====================================
      =            image thumbnail creation            =
      ====================================*/
      $config['image_library'] = 'GD2';
      $config['source_image'] = $dir_cond . "/" . $img_complete_name;
      $config['new_image'] = $thumb_dir . "/" . $azRange[$i] . '_' . $img_name . '.' . $extension;
      $config['maintain_ratio'] = true;
      $config['width'] = 100;
      $config['height'] = 100;

      //$config['quality']     = 50; this filter doesnt work
      $in = $this->image_lib->initialize($config);
      $result = $this->image_lib->resize($in);
      $this->image_lib->clear();

      /*=====  End of image thumbnail creation  ======*/

      
      $i++;
      $this->db->query("UPDATE MASTER_IMG_TEMP T SET T.DOWNLOADED = 1 WHERE T.ITEM_ID = '$item_id' AND T.CONDITION_ID = '$condition_id' AND T.FOLDER_DIR = '$folder_dir'");
    }else{
      $this->db->query("UPDATE MASTER_IMG_TEMP T SET T.ATTEMPTED = 1 WHERE T.ITEM_ID = '$item_id' AND T.CONDITION_ID = '$condition_id' AND T.FOLDER_DIR = '$folder_dir'");
      continue 1;
    }


}// end foreach ($sub_qry as $val) {
}else{//if(count() > 0){
  
}

$this->db->query("UPDATE MASTER_IMG_TEMP T SET T.ATTEMPTED = 1 WHERE T.ITEM_ID = '$item_id' AND T.CONDITION_ID = '$condition_id' AND T.FOLDER_NAME = '$folder_name'");
}//end foreach ($qry as $value) {
/*=====  End of copy code from item_pictures  ======*/



 }
 public function create_thumb_from_image()
    {
        $result = $this->m_cron_job->Create_Thumb_From_Image();
        echo json_encode($result);
        return json_encode($result);
    }
    public function remove_duplicate_images()
    {
        $result = $this->m_cron_job->Remove_Duplicate_Images();
        echo json_encode($result);
        return json_encode($result);
    }
}