<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

class c_retail_screen extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    public function Save_Store_Genrate_Barcode()
    {
        $desc = $this->input->post('barcode_desc');
        $desc = trim(str_replace("  ", ' ', $desc));
        $desc = trim(str_replace(array("'"), "''", $desc));
        $label_desc = str_replace(',', ' ', $this->input->post('label_desc'));
        $label_desc = trim(str_replace("  ", ' ', $label_desc));
        $label_desc = trim(str_replace(array("'"), "''", $label_desc));
        $brand = $this->input->post('barcode_brand');
        $brand = trim(str_replace("  ", ' ', $brand));
        $brand = trim(str_replace(array("'"), "''", $brand));
        $mpn = $this->input->post('barcode_mpn');
        $mpn = trim(str_replace("  ", ' ', $mpn));
        $mpn = trim(str_replace(array("'"), "''", $mpn));
        $upc = $this->input->post('barcode_upc');
        $upc = trim(str_replace("  ", ' ', $upc));
        $upc = trim(str_replace(array("'"), "''", $upc));
        $barcode = trim($this->input->post('barcode'));
        $barcodes = explode('-', $barcode);
        $barcode = explode('@', $barcode);
        // var_dump(empty($barcode[1]));

        if (!empty($barcode[1])) {
            $barcode = $barcode[1];
            $sendBarcode = $barcode;
        } else if (!empty($barcodes[1])) {
            return array('status' => false, 'message' => 'This Barcode is Use For Repair POS');
        } else {
            $barcode = $this->input->post('barcode');
            $sendBarcode = $barcode;
        }
        if (empty($barcode)) {
            $sendBarcode = 'NULL';
        }
        // var_dump($barcode);
        $cond_id = $this->input->post('condition_id');
        // if (!empty($condition)) {
        //     $cond_id = $condition['value'];
        //     $cond_name = $condition['label'];
        // }
        $cost = trim($this->input->post('barcode_cost'));
        $price = trim($this->input->post('barcode_price'));
        $user_id = $this->input->post('user_id');
        $merchant_id = $this->input->post('merchant_id');
        $no_of_barcode = $this->input->post('quantity');
        // $bin_name = trim($this->input->post('bin_name'));
        $bin_id = trim($this->input->post('bin_name'));//$this->db->query("select * from bin_mt b where b.bin_no = '$bin_name'");
        $pos_status = 1;

        if (!empty($bin_id)) {
            $qr = $this->db->query("SELECT BIN_ID FROM BIN_MT WHERE BIN_NO = '$bin_id'");
            if ($qr->num_rows() > 0) {
                $bin_id = $qr[0]['BIN_ID'];
            } else {
                echo json_encode(array('status' => false, 'message' => 'This Bin Is Not Valid'));
                return array('status' => false, 'message' => 'This Bin Is Not Valid');
            }
        } else {
            $bin_id = 0;
        }

        $check_barcode_exist = $this->db->query("SELECT BARCODE_NO FROM LZ_MERCHANT_BARCODE_DT WHERE BARCODE_NO = '$barcode'");

        if ($check_barcode_exist->num_rows() === 0) {
            $mt_id = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_MERCHANT_BARCODE_MT', 'MT_ID') ID FROM DUAL")->result_array();
            $mt_id = $mt_id[0]['ID'];
            $qr = "INSERT INTO LZ_MERCHANT_BARCODE_MT (MT_ID, MERCHANT_ID, ISSUED_DATE, ISSUED_BY, NO_OF_BARCODE,LOT_ID,RANGE_ID, POS_STATUS) VALUES ($mt_id, '$merchant_id', sysdate, $user_id, $no_of_barcode, null, 0, $pos_status)";
            $result = $this->db->query($qr);
            // return $result;
            if ($result) {
                $get_mer_acc = $this->db->query("SELECT MERCHANT_ID, ACCOUNT_NAME, ACCT_ID, DEFAULT_MERCHANT FROM lj_merhcant_acc_dt WHERE MERCHANT_ID = '$merchant_id' ")->result_array();
                $seller_account_id = $get_mer_acc[0]['ACCT_ID'];
                for ($i = 1; $i <= $no_of_barcode; $i++) {
                    if (empty($barcode)) {
                        $bar = $this->db->query('SELECT seq_barcode_no.nextval barcode FROM dual')->result_array();
                        $bar_code = $bar[0]['BARCODE'];
                    } else {
                        $bar_code = $barcode;
                        // die("ok");
                    }
                    $this->db->query("INSERT INTO LZ_MERCHANT_BARCODE_DT (DT_ID, MT_ID, BARCODE_NO, ACCOUNT_ID,COST, SALE_PRICE, BIN_ID,ITEM_DESC, COND_ID, UPC, MPN, BRAND, LABEL_DESC , CREATED_BY, CREATED_AT) VALUES (GET_SINGLE_PRIMARY_KEY('LZ_MERCHANT_BARCODE_DT', 'DT_ID'), '$mt_id', '$bar_code', '$seller_account_id', '$cost', '$price', '$bin_id', '$desc', '$cond_id', '$upc', '$mpn', '$brand', '$label_desc', $user_id, sysdate)");
                }
            }
        } else {
            $mt_id = $this->db->query("SELECT MT_ID FROM LZ_MERCHANT_BARCODE_DT WHERE BARCODE_NO = '$barcode'")->result_array();
            $mt_id = $mt_id[0]['MT_ID'];
            $this->db->query("UPDATE LZ_MERCHANT_BARCODE_DT SET COST = '$cost', SALE_PRICE = '$price', BIN_ID = '$bin_id',ITEM_DESC = '$desc', COND_ID = '$cond_id', UPC = '$upc', MPN = '$mpn', BRAND = '$brand', UPDATED_BY = '$user_id', UPDATED_AT = sysdate, LABEL_DESC = '$label_desc' WHERE MT_ID = '$mt_id' AND  BARCODE_NO = '$barcode' ");
        }

        // $bin_Id = $this->db->query("SELECT BIN_ID from lz_merchant_barcode_dt d where d.barcode_no = '$barcode'")->result_array();

        // if (count($bin_Id) > 0) {
        //     $bin_id = $bin_Id[0]['BIN_ID'];
        // } else {
        //     $bin_id = $this->input->post('bin_id');
        // }
        $barcodeEx = $this->db->query("SELECT BARCODE_NO, LZ_MANIFEST_ID FROM LZ_BARCODE_MT WHERE BARCODE_No = '$barcode'");
        if ($barcodeEx->num_rows() <= 0) {
            $qry2 = $this->db->query("SELECT get_single_primary_key('LZ_MANIFEST_MT','LZ_MANIFEST_ID') LZ_MANIFEST_ID FROM DUAL")->result_array();
            $lz_menifest_id = $qry2[0]['LZ_MANIFEST_ID'];
            $loadingNo = $this->db->query('SELECT max(LOADING_NO) LOADING_NO from LZ_MANIFEST_MT')->result_array();
            $loading_no = $loadingNo[0]['LOADING_NO'] + 1;
            $pruch_ref_no = 'POS_' . $loading_no;
            $supplier_no = 7;
            $doc_seq_id = 30;
            $excel_file_name = 'POS FORM';
            $manifiest_type = 5;
            $insert = $this->db->query("INSERT INTO LZ_MANIFEST_MT
        (LZ_MANIFEST_ID,
         LOADING_NO,
         LOADING_DATE,
         PURCH_REF_NO,
         SUPPLIER_ID,
         REMARKS,
         DOC_SEQ_ID,
         PURCHASE_DATE,
         POSTED,
         EXCEL_FILE_NAME,
         GRN_ID,
         PURCHASE_INVOICE_ID,
         SINGLE_ENTRY_ID,
         TOTAL_EXCEL_ROWS,
         MANIFEST_NAME,
         MANIFEST_STATUS,
         SOLD_PRICE,
         END_DATE,
         LZ_OFFER,
         MANIFEST_TYPE,
         EST_MT_ID,
         POSTED_DATE,
         POSTED_BY)
      VALUES
        ('$lz_menifest_id',
         '$loading_no',
         sysdate,
        '$pruch_ref_no',
         $supplier_no,
         null,
         $doc_seq_id,
         sysdate,
         'POSTED',
         'RETAIL FORM',
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         $manifiest_type,
         null,
         sysdate,
         $user_id)
      ");
            if ($insert == true) {
                $qry2 = $this->db->query("SELECT get_single_primary_key('lz_manifest_det','LAPTOP_ZONE_ID') LAPTOP_ZONE_ID FROM DUAL")->result_array();
                $LAPTOP_ZONE_ID = $qry2[0]['LAPTOP_ZONE_ID'];
                $insertDt = $this->db->query("INSERT into lz_manifest_det
            (po_mt_auction_no,
             po_detail_lot_ref,
             po_mt_ref_no,
             item_mt_manufacture,
             item_mt_mfg_part_no,
             item_mt_desc,
             item_mt_bby_sku,
             item_mt_upc,
             po_detail_retial_price,
             main_catagory_seg1,
             sub_catagory_seg2,
             brand_seg3,
             origin_seg4,
             conditions_seg5,
             e_bay_cata_id_seg6,
             laptop_zone_id,
             laptop_item_code,
             available_qty,
             price,
             lz_manifest_id,
             category_name_seg7,
             s_price,
             v_price,
             ship_fee,
             sticker_print,
             manual_update,
             est_det_id,
             weight,
             bin_id)
          VALUES (null,null,null, '$brand', '$mpn', '$desc', null, '$upc','$cost',null,null, null , null, '$cond_id' , null, '$LAPTOP_ZONE_ID',null,1,'$price','$lz_menifest_id', null, null, null,
          null, null, 0, null, null, '$bin_id')");
                if ($insertDt == true) {

                    $this->db->query("CALL Pro_Items_Mt($lz_menifest_id)");
                    $item_code = $this->db->query("SELECT LAPTOP_ITEM_CODE FROM LZ_MANIFEST_DET WHERE LZ_MANIFEST_ID = '$lz_menifest_id'")->result_array();
                    $item_code = $item_code[0]['LAPTOP_ITEM_CODE'];

                    $item_id = $this->db->query("SELECT ITEM_ID FROM ITEMS_MT WHERE ITEM_CODE = '$item_code'")->result_array();
                    $item_id = $item_id[0]['ITEM_ID'];

                    $Barcode_nos = $this->db->query("SELECT BARCODE_NO FROM LZ_MERCHANT_BARCODE_DT WHERE MT_ID= '$mt_id'");
                    if ($Barcode_nos->num_rows() > 0) {
                        $barcode = $Barcode_nos->result_array();
                    } else {
                        $barcode = array('BARCODE_NO' => $barcode);
                    }
                    for ($i = 0; $i < $no_of_barcode; $i++) {
                        $qry2 = $this->db->query("SELECT get_single_primary_key('LZ_BARCODE_MT','LZ_BARCODE_MT_ID') LZ_BARCODE_MT_ID FROM DUAL")->result_array();
                        $lz_barcode_mt_id = $qry2[0]['LZ_BARCODE_MT_ID'];

                        $get_unit_no = $this->db->query("select nvl(max(unit_no + 1), 1)
                 UNIT_NO
                from lz_barcode_mt
               where lz_manifest_id = $lz_menifest_id
                 and item_id = $item_id")->result_array();
                        $unit_no = $get_unit_no[0]['UNIT_NO'];

                        $barcode_no = $barcode[$i]['BARCODE_NO'];

                        $insertBarcode = $this->db->query("INSERT INTO LZ_BARCODE_MT
                (LZ_BARCODE_MT_ID,
                 Lz_Manifest_Id,
                 item_id,
                 unit_no,
                 barcode_no,
                 condition_id,
                 print_status,
                 hold_status,
                 ebay_item_id,
                 pulling_id,
                 ebay_sticker,
                 lz_pos_mt_id,
                 po_detail_lot_ref,
                 lz_part_issue_mt_id,
                 item_adj_det_id_for_out,
                 bin_id,
                 audit_datetime,
                 audit_by,
                 sale_record_no,
                 packing_id,
                 packing_by,
                 packing_date,
                 list_id,
                 created_trhough,
                 auto_list_id,
                 item_adj_det_id_for_in,
                 shopify_list_id,
                 salvage_reason,
                 pulling_print_yn,
                 barcode_notes,
                 discard_date,
                 discard_by,
                 discard,
                 order_id,
                 discard_remarks,
                 repair_id,
                 ended_barcode,
                 extendedorderid,
                 pulling_print_date, pos_only, inserted_date)
              VALUES ('$lz_barcode_mt_id', '$lz_menifest_id', ' $item_id' , '$unit_no', '$barcode_no' , '$cond_id',
              0,
              0,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              '$bin_id',
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,null,null,null,null,null,null,null,null,null,null, 1, sysdate)");
                    }
                    if ($insertBarcode == true) {
                        // echo json_encode(array("status" => true, 'message' => 'Barcode Genrated Successfuly', "MT_ID" => $mt_id, 'manifest_id' => $lz_menifest_id, 'barcode' => $sendBarcode));
                        $this->Genrate_Store_Barcode_print($lz_menifest_id, $mt_id, $sendBarcode);
                    } else {
                        echo json_encode( array("status" => false, 'message' => 'Not Inserted In Barcode Mt'));
                        return array("status" => false, 'message' => 'Not Inserted In Barcode Mt');
                    }
                } else {
                    echo json_encode( array("status" => false, 'message' => 'Not Inserted In Manifest Det'));
                    return array("status" => false, 'message' => 'Not Inserted In Manifest Det');
                }
            } else {
                echo json_encode(array("status" => false, 'message' => 'Not Inserted In Manifest Mt'));
                return array("status" => false, 'message' => 'Not Inserted In Manifest Mt');
            }
        } else {
            //  ManiFest DET
            $barcodeEx = $barcodeEx->result_array();
            $lz_manifest_id = $barcodeEx[0]['LZ_MANIFEST_ID'];
            $updateDt = $this->db->query("UPDATE lz_manifest_det
            SET item_mt_manufacture    = '$brand',
                item_mt_mfg_part_no    = '$mpn',
                item_mt_desc           = '$desc',
                item_mt_upc            = '$upc',
                po_detail_retial_price = '$cost',
                conditions_seg5        = '$cond_id',
                price                  = '$price'
          WHERE lz_manifest_id = '$lz_manifest_id'");

            $updateMer = $this->db->query("UPDATE LZ_BARCODE_MT SET condition_id = '$cond_id' WHERE Lz_Manifest_Id = '$lz_manifest_id'");
            // $this->db->query("UPDATE LZ_MANIFEST_MT SET LOADING_NO = '$loadingNo' AND LOADING_DATE = sysdate AND PURCH_REF_NO = '$pruch_ref_no', AND SUPPLIER_ID = '$supplier_no'
            // AND DOC_SEQ_ID = '$doc_seq_id' AND PURCHASE_DATE = sysdate AND POSTED= 'POSTED' AND EXCEL_FILE_NAME = ");
            // echo json_encode(array('status' => true, 'message' => 'Barcode Already Exist And Updated Successfully', "MT_ID" => $mt_id, 'manifest_id' => $lz_manifest_id, 'barcode' => $sendBarcode));
            // return array('status' => true, 'message' => 'Barcode Already Exist And Updated Successfully', "MT_ID" => $mt_id, 'manifest_id' => $lz_manifest_id, 'barcode' => $sendBarcode);
            $this->Genrate_Store_Barcode_print($lz_manifest_id, $mt_id, $sendBarcode);
        }
    }

    public function Search_Barcode_For_Store()
    {
        $barcode = $this->input->post('barcode');

        $barcodes = explode('-', $barcode);
        $barcode = explode('@', $barcode);
        // var_dump(empty($barcode[1]));

        if (!empty($barcode[1])) {
            $barcode = $barcode[1];
        } else if (!empty($barcodes[1])) {
            echo json_encode(array('status' => false, 'message' => 'This Barcode is Use For Repair POS'));
            return array('status' => false, 'message' => 'This Barcode is Use For Repair POS');
        } else {
            $barcode = $this->input->post('barcode');
        }
        $result = $this->db->query("SELECT *
        FROM LZ_BARCODE_MT b
       WHERE b.BARCODE_NO = '$barcode'");
        $error = [];
        if ($result->num_rows() > 0) {
            $data = $result->result_array();
            if ($data[0]['PULLING_ID'] != null) {
                $sale_record_no = $data[0]['SALE_RECORD_NO'];
                $error[] = array("status" => false, 'message' => "Barcode Ship Aganist sale Record $sale_record_no ", "model" => false);
            } else if ($data[0]['PULLING_ID'] == null && $data[0]['SALE_RECORD_NO'] != null) {
                $sale_record_no = $data[0]['SALE_RECORD_NO'];
                $error[] = array("status" => false, 'message' => "Barcode Already Sold Aganist sale Record  $sale_record_no ", "model" => false);
            } else if ($data[0]['PULLING_ID'] == null && $data[0]['SALE_RECORD_NO'] == null && $data[0]['EBAY_ITEM_ID'] != null) {
                $ebay_id = $data[0]['EBAY_ITEM_ID'];
                $error[] = array("status" => false, "message" => "Barcode Already Listed Aganist ebay id $ebay_id ", "model" => true, 'barcode' => $barcode, 'ebay_id' => $ebay_id);
            } else if ($data[0]['ITEM_ADJ_DET_ID_FOR_IN'] != null || $data[0]['ITEM_ADJ_DET_ID_FOR_OUT'] != null || $data[0]['LZ_PART_ISSUE_MT_ID'] != null) {
                // Remove this validation
                // || $data[0]['HOLD_STATUS'] != 0
                $error[] = array("status" => false, "message" => "Barcode Already Consumed", "model" => false);
            } elseif ($data[0]['LZ_POS_MT_ID'] != null) {
                $id = $data[0]['LZ_POS_MT_ID'];
                $error[] = array("status" => false, "message" => "Barcode Already Consumed By POS $id", "model" => false);
            } else if ($data[0]['REPAIR_ID'] !== null) {
                $id = $data[0]['REPAIR_ID'];
                $error[] = array("status" => false, "message" => "Barcode Already Consumed By Repair  $id", "model" => false);
            }
            $detail_query = $this->db->query("SELECT S.SEED_ID,
            LM.LZ_MANIFEST_ID,
            LM.PURCH_REF_NO,
            NVL(S.ITEM_TITLE, I.ITEM_DESC) ITEM_MT_DESC,
            I.ITEM_MT_MANUFACTURE MANUFACTURER,
            I.ITEM_MT_MFG_PART_NO MFG_PART_NO,
            I.ITEM_MT_UPC UPC,
            BCD.CONDITION_ID ITEM_CONDITION,
            (SELECT BIN_ID FROM LZ_MERCHANT_BARCODE_DT WHERE BARCODE_NO = '$barcode') BIN_ID,
            (SELECT LABEL_DESC FROM LZ_MERCHANT_BARCODE_DT WHERE BARCODE_NO = '$barcode') LABEL_DESC,
            (SELECT COND_NAME FROM LZ_ITEM_COND_MT WHERE ID = BCD.CONDITION_ID ) ITEM_CONDITION_NAME,
            BCD.QTY QUANTITY,
            -- S.EBAY_PRICE COST_PRICE,
            QRY_PRICE.COST_PRICE COST_PRICE,
            QRY_PRICE.RETAIL_PRICE SALE_PRICE,
            BCD.BARCODE_NO,
            --SALE_PRICE.PRICE,
            null LINE_TYPE,
            null DISCOUNT_PER,
            null DISCOUNT_AMOUNT,
            null LZ_POS_DET_ID,
            DECODE(S.EBAY_PRICE, 0, 0 , S.EBAY_PRICE) NET_PRICE,
            0 ADVANCE_PAYMENT,
            null REPAIRE_ID
            FROM LZ_MANIFEST_MT LM,
            ITEMS_MT I,
            LZ_ITEM_SEED S,
            (SELECT BC.EBAY_ITEM_ID,
            BC.LZ_MANIFEST_ID,
            BC.ITEM_ID,
            BC.BARCODE_NO,
            BC.CONDITION_ID,
            COUNT(1) QTY
            FROM LZ_BARCODE_MT BC
            WHERE BC.BARCODE_NO = '$barcode'
            and bc.pulling_id is null
            GROUP BY BC.LZ_MANIFEST_ID,
            BC.ITEM_ID,
            BC.CONDITION_ID,
            BC.BARCODE_NO,
            BC.EBAY_ITEM_ID) BCD,
            (SELECT D.LZ_MANIFEST_ID,
            I.ITEM_ID,
            MAX(D.PO_DETAIL_RETIAL_PRICE) COST_PRICE,
            MAX(D.Price) RETAIL_PRICE
            FROM LZ_MANIFEST_DET D, ITEMS_MT I
            WHERE D.LAPTOP_ITEM_CODE = I.ITEM_CODE
            GROUP BY D.LZ_MANIFEST_ID, I.ITEM_ID) QRY_PRICE
            WHERE BCD.ITEM_ID = I.ITEM_ID
            AND BCD.LZ_MANIFEST_ID = LM.LZ_MANIFEST_ID
            AND BCD.ITEM_ID = QRY_PRICE.ITEM_ID
            AND BCD.LZ_MANIFEST_ID = QRY_PRICE.LZ_MANIFEST_ID
            AND S.ITEM_ID(+) = BCD.ITEM_ID
            AND S.LZ_MANIFEST_ID(+) = BCD.LZ_MANIFEST_ID
            AND S.DEFAULT_COND(+) = BCD.CONDITION_ID")->result_array();
            array_walk_recursive($detail_query, function (&$item) {$item = strval($item);});
            echo json_encode(array("status" => true, "data" => $detail_query, 'error' => $error));
            return array("status" => true, "data" => $detail_query, 'error' => $error);
        } else {
            $res = $this->db->query("select mb.*
        from lz_merchant_barcode_dt dm, lz_merchant_barcode_mt mb
       WHERE dm.barcode_no = '$barcode'
         and mb.pos_status = 1
         and dm.mt_id = mb.mt_id
         and mb.LZ_POS_MT_ID is null
        ");
            if ($res->num_rows() > 0) {
                $data = $this->db->query(" SELECT dm.barcode_no BARCODE_NO,
            dm.item_desc ITEM_MT_DESC,
            dm.mpn MFG_PART_NO,
            dm.brand MANUFACTURER,
            dm.LABEL_DESC ,
            dm.upc UPC,
            dm.BIN_ID,
            dm.cond_id ITEM_CONDITION,
            DECODE(dm.COST, 0, 0, dm.COST) COST_PRICE,
            DECODE(dm.sale_price, 0, 0, dm.sale_price) SALE_PRICE,
            (SELECT COND_NAME FROM LZ_ITEM_COND_MT WHERE ID = dm.cond_id) ITEM_CONDITION_NAME
       from lz_merchant_barcode_mt mb, lz_merchant_barcode_dt dm
      where dm.barcode_no = '$barcode'
        and mb.mt_id = dm.mt_id
        and mb.POS_STATUS = 1")->result_array();
        array_walk_recursive($data, function (&$item) {$item = strval($item);});
                echo json_encode(array("status" => true, "data" => $data));
                return array("status" => true, "data" => $data);
            } else {
                echo json_encode(array("status" => false, "message" => "No Barcode Found", "modelBarcode" => true));
                return array("status" => false, "message" => "No Barcode Found", "modelBarcode" => true);
            }
        }
    }

    public function Genrate_Store_Barcode_print($lz_manifest_id, $mt_id, $barcode)
    {
        // $lz_manifest_id = $_GET['lz_manifest_id'];
        // $mt_id = $_GET['mt_id'];
        // $lz_manifest_id = $_GET['lz_manifest_id'];
        // $mt_id = $_GET['mt_id'];
        // $barcode = $_GET['barcode'];
        $qr = "SELECT TO_CHAR(B.ISSUED_DATE, 'MM/DD/YY HH24:MI:SS') ISSUED_DATE,
        B.NO_OF_BARCODE,
        B.MERCHANT_ID,
        DECODE(B.POS_STATUS, '0', 'NOT FOR POS', '1', 'USE FOR POS') POS_STATUS,
        B.RANGE_ID,
        D.BARCODE_NO,
        MM.BUISNESS_NAME,
        L.LOT_ID,
        D.LABEL_DESC DES,
        L.REF_NO,
        DECODE(MD.ITEM_MT_UPC, '', MD.ITEM_MT_MFG_PART_NO, MD.ITEM_MT_UPC) UPC_MPN,
        MD.Price PRICE,
        (SELECT MIN(DD.BARCODE_NO) || '-' || MAX(DD.BARCODE_NO)
           FROM LZ_MERCHANT_BARCODE_DT DD
          WHERE DD.MT_ID = B.MT_ID) RANGE_BARCODE
   FROM LZ_MERCHANT_BARCODE_MT B,
        LZ_MERCHANT_BARCODE_DT D,
        LZ_MERCHANT_MT         MM,
        LZ_BARCODE_MT          LM,
        LZ_MANIFEST_DET        MD,
        items_mt               I,
        LOT_DEFINATION_MT L
  WHERE B.MERCHANT_ID = MM.MERCHANT_ID
    AND B.MT_ID = D.MT_ID
    AND B.LOT_ID = L.LOT_ID(+)
    AND I.item_code = md.laptop_item_code
AND I.item_id = lm.item_id
AND LM.BARCODE_NO = D.BARCODE_NO
    AND LM.LZ_MANIFEST_ID = MD.LZ_MANIFEST_ID
    AND LM.LZ_MANIFEST_ID = '$lz_manifest_id'
    AND D.MT_ID = '$mt_id'

";

        if ($barcode !== 'NULL') {
            $qr .= "AND D.BARCODE_NO = '$barcode'";
        }

        $qr .= " ORDER BY BARCODE_NO ASC";
        $result = $this->db->query($qr)->result_array();
        if ($result) {
            array_walk_recursive($result, function (&$item) {$item = strval($item);});
            echo json_encode($result);
        } else {
            echo array('status' => false, 'message' => 'No Print');
        }
    }

    /**
     *
     * End Genrate Store Barcode
     * Assign Barcodes
     *
     *
     */

}