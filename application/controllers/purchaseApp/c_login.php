<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_login extends CI_Controller {
	public function __construct()
  {
     parent::__construct();  
    $this->load->database(); 
    // $this->load->model('login/m_login','Mlogin');
  }

	public function index()
	{

		$data = $this->m_login->login();
		echo json_encode($data);

	}
	public function test()
	{
		$response = "";
		$response['success'] = 0;
		$response['message'] = "Image Uploaded Failed";
		// $data = $this->m_login->login();
		echo '{ "Pascal" : [ 
			{ "Name"  : "Pascal Made Simple", "price" : 700 },
			{ "Name"  : "Guide to Pascal", "price" : 400 }],  
	
			"Scala"  : [
				 { "Name"  : "Scala for the Impatient", "price" : 1000 }, 
				 { "Name"  : "Scala in Depth", "price" : 1300 }]    
	 }';

	}
}
