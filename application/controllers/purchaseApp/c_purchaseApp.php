<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

class c_purchaseApp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {

        $barcodes_array = array();

        $this->load->library('image_lib');
        $this->load->model('purchaseApp/m_sync_oracle', 'handler');

        $json_object_data = $_POST['dataObject'];
        $app_version = $_POST['app_version'];
        $images = $_POST['imagesArray'];
        // $images = "[]";
        // $app_version = "POSTMAN_VERSION";
        $myFile = "data.json";
        file_put_contents($myFile, $json_object_data);
        // $json_object_data = file_get_contents($myFile);
        // exit;
        $object_data = json_decode($json_object_data);

        // echo $object_data->MPN;
        // exit;
        $images = json_decode($images, true);
        $mpn = $object_data->MPN;
        $upc = $object_data->UPC;
        $imei1 = $object_data->IMEI1;
        $imei2 = "";// it is eliminated from app
        $cost = $object_data->cost;
        $condId = $object_data->condId;
        $qty = $object_data->qty;
        $userId = $object_data->userId;
        $remarks = $object_data->remarks;
        $historyCheck = $object_data->historyCheck;
        $bin = $object_data->bin;
        $exp_date = $object_data->expDate;
        $exp_date = "TO_DATE('".$exp_date."', 'DD/MM/YYYY HH:MI:SS am')";
        $dir_path_id = 14;
        $imei_path = "";
        $mpn_path = "";
//var_dump($images);exit;
        $MT_ID = $this->db->query("SELECT get_single_primary_key('LZ_PURCH_ITEM_MT', 'PURCH_MT_ID') mt_id FROM DUAL")->result_array();
        $MT_ID = $MT_ID[0]["MT_ID"];
        $query = "INSERT INTO LZ_PURCH_ITEM_MT (PURCH_MT_ID, PURCHASE_NO, MPN, UPC, IMEI1, IMEI2, COST, QTY, REMARKS, INSERTED_BY, INSERTED_DATE, CONDITION, ANDROID_APP_VERSION, BIN_NAME, EXP_DATE)
        VALUES('$MT_ID',
                '1', '$mpn', '$upc', '$imei1', '$imei2', '$cost', '$qty', '$remarks', '$userId', sysdate, '$condId', '$app_version', '$bin', $exp_date)";
        $this->db->query($query);
        $this->db->query("INSERT INTO LZ_PURCH_LOG (LOG_ID, PURCH_MT_ID, SCOPE) VALUES(get_single_primary_key('LZ_PURCH_LOG','LOG_ID'), '$MT_ID', 'LZ_PURCH_ITEM_MT')");
if(count($images) > 0){
    $this->db->query("INSERT INTO LZ_PURCH_LOG (LOG_ID, PURCH_MT_ID, SCOPE) VALUES(get_single_primary_key('LZ_PURCH_LOG','LOG_ID'), '$MT_ID', 'PICTURES_UPLOAD')");

        $query = $this->db->query("SELECT C.MASTER_PATH FROM LZ_PICT_PATH_CONFIG C WHERE C.PATH_ID = '$dir_path_id'")->result_array();
        $pic_dir = $query[0]['MASTER_PATH'];

        $folder_dir = $pic_dir . $MT_ID;
        $thumb_dir = $pic_dir . $MT_ID . '/thumb';
        if (!is_dir($folder_dir)) {
            mkdir($folder_dir);
        }
        if (!is_dir($thumb_dir)) {
            mkdir($thumb_dir);
        }
        $azRange = range('A', 'Z');
        for ($i = 0; $i < count($images); $i++) {
            $imageFile = $images[$i]->image;
            $fileName = $images[$i]->file_name;
            $decodedImage = base64_decode($imageFile);
            if (strpos($fileName, "product") != -1) {
                $return = file_put_contents($folder_dir . "/" . $azRange[$i] . "_" . $fileName, $decodedImage);
                $PIC_PATH = $folder_dir . "/" . $azRange[$i] . "_" . $fileName;
                $config['image_library'] = 'GD2';
                // $config['source_image']  = $sub_dir2."/".$azRange[$i].'_'.$img_name.'.'.$extension;
                $config['source_image'] = $folder_dir . "/" . $azRange[$i] . "_" . $fileName;
                if (!is_dir($folder_dir . "/thumb")) {
                    mkdir($folder_dir . "/thumb");
                }
                $config['new_image'] = $thumb_dir . "/" . $azRange[$i] . "_" . $fileName . ".jpg";
                $config['maintain_ratio'] = true;
                $config['width'] = 100;
                $config['height'] = 100;

                //$config['quality']     = 50; this filter doesnt work
                $in = $this->image_lib->initialize($config);
                $result = $this->image_lib->resize($in);
                $this->image_lib->clear();
                $queryPicPath = $this->db->query("INSERT INTO LZ_PURCH_ITEM_PIC (PIC_ID, PURCH_MT_ID, PIC_PATCH) VALUES
                (get_single_primary_key('LZ_PURCH_ITEM_PIC', 'PIC_ID'), '$MT_ID', '$PIC_PATH')");
            } else {
                $return = file_put_contents($folder_dir . "/" . $fileName, $decodedImage);
                if (strpos($fileName, "mpn")) {
                    $mpn_path = $folder_dir . "/" . $fileName;
                } else {
                    $imei_path = $folder_dir . "/" . $fileName;
                }
            }
        }
    }//if(count($images) > 0){
    
        for ($j = 0; $j < $qty; $j++) {
            $barcode_count = $j+1;
            $queryGenerateBarcode = $this->db->query("select seq_barcode_no.nextval barcode from dual")->result_array();
            $barcode = $queryGenerateBarcode[0]['BARCODE'];
            $barcodes_array[$j] = $barcode;
            $queryPurchDet = $this->db->query("INSERT INTO LZ_PURCH_ITEM_DT ( PURCH_DT_ID, PURCH_MT_ID, BARCODE_NO, INSERTED_BY, INSETED_DATE, IMEI_PATH, MPN_PIC, BARCODE_COUNT)
            VALUES(get_single_primary_key('LZ_PURCH_ITEM_DT', 'PURCH_DT_ID'), '$MT_ID', '$barcode', '$userId', sysdate, '$imei_path', '$mpn_path' , '$barcode_count')");
            $this->db->query("INSERT INTO LZ_PURCH_LOG (LOG_ID, PURCH_MT_ID, SCOPE) VALUES(get_single_primary_key('LZ_PURCH_LOG','LOG_ID'), '$MT_ID', '$barcode')");
        }
        $result['barcode_array'] = $barcodes_array;

        //$query = "SELECT ITEM_TITLE FROM (SELECT * FROM LZ_ITEM_SEED S WHERE (S.F_UPC = '$upc' OR UPPER(S.F_MPN) = UPPER('$mpn')) ORDER BY S.DATE_TIME DESC )WHERE ROWNUM =1";
        $query = "SELECT ITEM_TITLE
        FROM (SELECT *
                FROM LZ_ITEM_SEED S
               WHERE (S.F_UPC = '$upc'
                  or upper(s.f_mpn) = upper('$mpn'))
                  and s.date_time is not null
                  and s.default_cond = '$condId'
               order by s.date_time desc)
       where rownum = 1";
        $queryUpcHistory = $this->db->query($query)->result_array();

        if(count($queryUpcHistory) > 0)
        {
            $historyCheck = "1" ;
          
        }else{
            $historyCheck = "0" ;
        }

        $this->db->query("UPDATE LZ_PURCH_ITEM_MT SET HISTORY_STATUS = '$historyCheck' WHERE  PURCH_MT_ID='$MT_ID'");
        $this->db->query("INSERT INTO LZ_PURCH_LOG (LOG_ID, PURCH_MT_ID, SCOPE) VALUES(get_single_primary_key('LZ_PURCH_LOG','LOG_ID'), '$MT_ID', 'HISTORY_UPDATE')");

        $cond_abrivation = $this->db->query("SELECT CM.COND_ABRIVATION CA FROM LZ_ITEM_COND_MT CM WHERE CM.ID = '$condId'")->result_array();
        $cond_abrivation = $cond_abrivation[0]["CA"];
        if($historyCheck == "1" ){
            $this->get_img($barcodes_array[0]);
        }
        
        $result['cond'] = $cond_abrivation;

        echo json_encode($result);
        return json_encode($result);


    }

    public function getUPCHistory()
    {
        $UPC = $_POST['upc'];
        $MPN = $_POST['mpn'];

        $query = "select ITEM_TITLE from (select * from lz_item_seed s where (s.f_upc = '$UPC') or (upper(s.f_mpn) = upper('$MPN')) order by s.seed_id desc )where rownum = 1";
        $queryUpcHistory = $this->db->query($query)->result_array();
        // echo json_encode($query); exit;
        if ($queryUpcHistory) {
            $ITEM_TITLE = $queryUpcHistory[0]['ITEM_TITLE'];
        } else {
            $ITEM_TITLE = "";
        }

        echo json_encode($ITEM_TITLE);
    }

    //   function printAllStickers(){
    //     $data = $this->m_locations->printAllStickers();
    //     echo json_encode($data);
    //     return json_encode($data);
    //   }
public function validatebin()
    {
      $bin_name = $_POST['bin_name'];
        $bin_qry = $this->db->query("SELECT B.BIN_ID FROM BIN_MT B WHERE UPPER(B.BIN_TYPE || '-' || B.BIN_NO) = UPPER('$bin_name')")->result_array();
        if(count($bin_qry)> 0){
          $valid_bin = 1;
          $bin_id = $bin_qry[0]['BIN_ID'];
        }else{
          $bin_id = '';
          $valid_bin = 0;
        }
        echo json_encode(array('valid_bin'=>$valid_bin,'bin_id'=>$bin_id));
    }

    public function printAllStickers()
    {
        $barcode = $_POST['barcode'];
        // $print_qry = $this->db->query("SELECT MT.EBAY_ITEM_ID, MT.BARCODE_NO,'R#' || TO_DATE(I.ENTERED_DATE_TIME,'YYYY-MM-DD') REF_DATE,ROWNUM AS UNIT_NO,I.ITEM_DESC, (SELECT COUNT(B.EBAY_ITEM_ID) FROM LZ_BARCODE_MT B WHERE B.EBAY_ITEM_ID = (SELECT
        // C.EBAY_ITEM_ID FROM LZ_BARCODE_MT C WHERE C.BARCODE_NO = $barcode) ) NO_OF_BARCODE,  I.ITEM_MT_UPC CARD_UPC
        // FROM LZ_BARCODE_MT MT,
        // ITEMS_MT I
        // WHERE MT.ITEM_ID = I.ITEM_ID
        // AND MT.BARCODE_NO = $barcode")->result_array();
        $print_qry = $this->db->query("SELECT MT.EBAY_ITEM_ID,
               MT.BARCODE_NO,
               'R#' || TO_DATE(sd.date_time, 'YYYY-MM-DD') REF_DATE,
               ROWNUM AS UNIT_NO,
               sd.item_title ITEM_DESC,
               (SELECT COUNT(B.EBAY_ITEM_ID)
                  FROM LZ_BARCODE_MT B
                 WHERE B.EBAY_ITEM_ID =
                       (SELECT C.EBAY_ITEM_ID
                          FROM LZ_BARCODE_MT C
                         WHERE C.BARCODE_NO = $barcode)) NO_OF_BARCODE,
               sd.f_upc CARD_UPC
          FROM LZ_BARCODE_MT MT, lz_item_seed sd
         WHERE MT.ITEM_ID = sd.ITEM_ID
         and mt.lz_manifest_id = sd.lz_manifest_id
         and mt.condition_id = sd.default_cond
           AND MT.BARCODE_NO = $barcode")->result_array();
        if ($print_qry) {
            $this->db->query("UPDATE LJ_LOG_EBAY_DT SET PRINT_YN = 1 WHERE BARCODE_NO = $barcode");
            $this->db->query("UPDATE LZ_BARCODE_MT SET EBAY_STICKER = 1 WHERE BARCODE_NO = $barcode");
        }
        array_walk_recursive($print_qry, function (&$item) {$item = strval($item);});
        echo json_encode($print_qry);
    }

    public function printAllSticker()
    {
        $barcode = $_POST['barcode'];
        $listed_bin = $_POST['listed_bin'];
        $not_listed_bin = $_POST['not_listed_bin'];
        $user_id = $_POST['user_id'];
        $folder_name =NULL;
        $img_found = 0;
        $img_url = 'http://71.78.236.20/item_pictures/master_pictures/image_not_available.jpg';
        // $print_qry = $this->db->query("SELECT MT.EBAY_ITEM_ID, MT.BARCODE_NO,'R#' || TO_DATE(I.ENTERED_DATE_TIME,'YYYY-MM-DD') REF_DATE,ROWNUM AS UNIT_NO,I.ITEM_DESC, (SELECT COUNT(B.EBAY_ITEM_ID) FROM LZ_BARCODE_MT B WHERE B.EBAY_ITEM_ID = (SELECT
        // C.EBAY_ITEM_ID FROM LZ_BARCODE_MT C WHERE C.BARCODE_NO = $barcode) ) NO_OF_BARCODE,  I.ITEM_MT_UPC CARD_UPC
        // FROM LZ_BARCODE_MT MT,
        // ITEMS_MT I
        // WHERE MT.ITEM_ID = I.ITEM_ID
        // AND MT.BARCODE_NO = $barcode")->result_array();
        // $print_qry = $this->db->query("SELECT MT.EBAY_ITEM_ID,
        //        MT.BARCODE_NO,
        //        'R#' || TO_DATE(sd.date_time, 'YYYY-MM-DD') REF_DATE,
        //        ROWNUM AS UNIT_NO,
        //        sd.item_title ITEM_DESC,
        //        (SELECT COUNT(B.EBAY_ITEM_ID)
        //           FROM LZ_BARCODE_MT B
        //          WHERE B.EBAY_ITEM_ID =
        //                (SELECT C.EBAY_ITEM_ID
        //                   FROM LZ_BARCODE_MT C
        //                  WHERE C.BARCODE_NO = $barcode)) NO_OF_BARCODE,
        //        sd.f_upc CARD_UPC
        //   FROM LZ_BARCODE_MT MT, lz_item_seed sd
        //  WHERE MT.ITEM_ID = sd.ITEM_ID
        //  and mt.lz_manifest_id = sd.lz_manifest_id
        //  and mt.condition_id = sd.default_cond
        //    AND MT.BARCODE_NO = $barcode")->result_array();
        $print_qry = $this->db->query("SELECT MT.EBAY_ITEM_ID,
               MT.BARCODE_NO,
               'R#' || TO_DATE(sd.date_time, 'YYYY-MM-DD') REF_DATE,
               ROWNUM AS UNIT_NO,
               sd.item_title ITEM_DESC,
               (SELECT COUNT(B.EBAY_ITEM_ID)
                  FROM LZ_BARCODE_MT B
                 WHERE B.EBAY_ITEM_ID =
                       (SELECT C.EBAY_ITEM_ID
                          FROM LZ_BARCODE_MT C
                         WHERE C.BARCODE_NO = '$barcode')) NO_OF_BARCODE,
               sd.f_upc CARD_UPC,
               SD.F_UPC ||'~'||REPLACE(SD.F_MPN,'/','_')||'/'||CD.COND_NAME FOLDER_NAME
          FROM LZ_BARCODE_MT MT, LZ_ITEM_SEED SD , LZ_ITEM_COND_MT CD
         WHERE MT.ITEM_ID = SD.ITEM_ID
         AND MT.LZ_MANIFEST_ID = SD.LZ_MANIFEST_ID
         AND MT.CONDITION_ID = SD.DEFAULT_COND
         AND SD.DEFAULT_COND = CD.ID
           AND MT.BARCODE_NO = '$barcode'")->result_array();
        $ebay_item_id = '';
        $bin_update = 0;
        if(count($print_qry) > 0){
          // check pic exist or not
          $folder_name = $print_qry[0]['FOLDER_NAME'];
          $ebay_item_id = $print_qry[0]['EBAY_ITEM_ID'];
          if(!empty($ebay_item_id)){
            if(!empty($listed_bin)){
              
              $this->db->query("INSERT INTO LJ_BARCODE_LOG_MT
                                (LOG_ID,
                                 BARCODE_NO,
                                 EBAY_ITEM_ID,
                                 BIN_ID,
                                 ENTRY_POINT,
                                 INSERTED_DATE,
                                 INSERTED_BY)
                              VALUES
                                (GET_SINGLE_PRIMARY_KEY('LJ_BARCODE_LOG_MT', 'LOG_ID'),
                                 '$barcode',
                                 '$ebay_item_id',
                                 '$listed_bin',
                                 'PURCHASE APP',
                                 SYSDATE,
                                 $user_id)");
              $bin_update = 1;
            }
            $this->db->query("UPDATE LJ_LOG_EBAY_DT SET PRINT_YN = 1 WHERE BARCODE_NO = $barcode");
            $this->db->query("UPDATE LZ_BARCODE_MT SET EBAY_STICKER = 1,AUDIT_BY = '$user_id',AUDIT_DATETIME = SYSDATE WHERE BARCODE_NO = $barcode");
          }else{
            if(!empty($not_listed_bin)){
              $this->db->query("UPDATE LZ_BARCODE_MT SET BIN_ID = '$not_listed_bin' WHERE BARCODE_NO = $barcode");
            }
          }
        }
        //else{
        $folder_qry = $this->db->query("SELECT DT.FOLDER_NAME
                                      FROM LZ_DEKIT_US_DT DT
                                      WHERE DT.BARCODE_PRV_NO = '$barcode'
                                      UNION ALL
                                      SELECT L.FOLDER_NAME
                                      FROM LZ_SPECIAL_LOTS L
                                      WHERE L.BARCODE_PRV_NO = '$barcode'
                                      ")->result_array();
        
          if(count($folder_qry) > 0){
            // have pic
            if(!empty(@$folder_qry[0]['FOLDER_NAME'])){
              $folder_name = $folder_qry[0]['FOLDER_NAME'];
            }
            if(!empty($ebay_item_id)){
              if(!empty($listed_bin)){
                if(!$bin_update){
                // $this->db->query("UPDATE LZ_SPECIAL_LOTS SET BIN_ID = '$listed_bin' WHERE BARCODE_PRV_NO = '$barcode'");  
                // $this->db->query("UPDATE LZ_DEKIT_US_DT SET BIN_ID = '$listed_bin'  WHERE BARCODE_PRV_NO = '$barcode'");  
                  $this->db->query("INSERT INTO LJ_BARCODE_LOG_MT
                                  (LOG_ID,
                                   BARCODE_NO,
                                   EBAY_ITEM_ID,
                                   BIN_ID,
                                   ENTRY_POINT,
                                   INSERTED_DATE,
                                   INSERTED_BY)
                                VALUES
                                  (GET_SINGLE_PRIMARY_KEY('LJ_BARCODE_LOG_MT', 'LOG_ID'),
                                   '$barcode',
                                   '$ebay_item_id',
                                   '$listed_bin',
                                   'PURCHASE APP',
                                   SYSDATE,
                                   $user_id)");
                }
              }
            }else{
              if(!empty($not_listed_bin)){
                // $this->db->query("UPDATE LZ_SPECIAL_LOTS SET BIN_ID = '$not_listed_bin' WHERE BARCODE_PRV_NO = '$barcode'");  
                // $this->db->query("UPDATE LZ_DEKIT_US_DT SET BIN_ID = '$not_listed_bin'  WHERE BARCODE_PRV_NO = '$barcode'");  
                $this->db->query("INSERT INTO LJ_BARCODE_LOG_MT
                                (LOG_ID,
                                 BARCODE_NO,
                                 EBAY_ITEM_ID,
                                 BIN_ID,
                                 ENTRY_POINT,
                                 INSERTED_DATE,
                                 INSERTED_BY)
                              VALUES
                                (GET_SINGLE_PRIMARY_KEY('LJ_BARCODE_LOG_MT', 'LOG_ID'),
                                 '$barcode',
                                 '$ebay_item_id',
                                 '$not_listed_bin',
                                 'PURCHASE APP',
                                 SYSDATE,
                                 $user_id)");
              }
            }
            
          }
      //  }//if(count($print_qry) > 0){
          if(!empty($folder_name)){
            $pic_qry = $this->db->query("SELECT *
                                        FROM (SELECT REPLACE(PD.LOCAL_URL,'D:/wamp/www','http://71.78.236.20') IMG_URL
                                                FROM LJ_BARCODE_PIC_MT PM, LJ_BARCODE_PIC_DT PD
                                               WHERE PM.IMG_MT_ID = PD.IMG_MT_ID
                                                 AND PM.FOLDER_NAME = '$folder_name'
                                               ORDER BY PD.PIC_ORDER ASC)
                                       WHERE ROWNUM = 1")->result_array();
            if(count($pic_qry) > 0){
              if(!empty($pic_qry[0]['IMG_URL'])){
                $img_found = 1;
                $img_url = $pic_qry[0]['IMG_URL'];
              }              

            }
          }
          
        // if ($print_qry) {
        //     $this->db->query("UPDATE LJ_LOG_EBAY_DT SET PRINT_YN = 1 WHERE BARCODE_NO = $barcode");
        //     $this->db->query("UPDATE LZ_BARCODE_MT SET EBAY_STICKER = 1 WHERE BARCODE_NO = $barcode");
        // }
        array_walk_recursive($print_qry, function (&$item) {$item = strval($item);});
        echo json_encode(array('print_qry'=>$print_qry,'img_found'=>$img_found,'img_url'=>$img_url));
    }

    public function get_pictures($barcodes)
    {
        $uri = [];
        $base64 = [];

        // $barocde_no = $this->input->post('barocde_no');
        foreach ($barcodes as $barco) {
            $bar = explode(',', $barco['BARCODE_NO']);
            $bar_val = $bar[0];
            $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

            if (count($qry) >= 1) {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $barcode = $qry[0]["FOLDER_NAME"];
                $dir = $master_path . $barcode . "/";
                // var_dump($dir);
                // exit;

            } else {

                $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
                $path = $path->result_array();
                $master_path = $path[0]["MASTER_PATH"];

                $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar_val' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();

                //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

                $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
                $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
                $cond = @$get_params[0]['COND_NAME'];

                $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
            }

            $dir = preg_replace("/[\r\n]*/", "", $dir);
            // var_dump($dir);
            // exit;

            //var_dump(is_dir($dir));exit;
            $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            if (is_dir($dir)) {
                // var_dump($dir);exit;
                $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
                $i = 0;
                $pathinfo = pathinfo($images[0]);
                $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $images[0]);
                $uri[$bar_val][$i] = $base_url . $withoutMasterPartUri;
                // foreach ($images as $image) {
                //     $pathinfo = pathinfo($image);
                //     $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
                //     $uri[$bar_val][$i] = $base_url . $withoutMasterPartUri;
                //     // $base64[$i]['filename'] = $pathinfo['filename'];
                //     // $base64[$i]['image64'] = base64_encode(file_get_contents($base_url . $withoutMasterPartUri));
                //     $i++;
                // }
            } else {
                $uri[$bar_val][0] = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
                $uri[$bar_val][1] = $bar_val;
            }

            //var_dump($dekitted_pics);exit;

        }
        // var_dump($uri);
        // exit;
        return array('uri' => $uri);

    }

    public function getEbayItemList()
    {
      //and e.list_date between TO_DATE('$from_date', 'YYYY-MM-DD HH24:MI:SS') and TO_DATE('$to_date', 'YYYY-MM-DD HH24:MI:SS')
      $from_date = $this->input->post('from_date');
      
      $to_date = $this->input->post('to_date');

      if(strlen($from_date) > 0){
        $from_date = $from_date.' 00:00:00';
        $to_date = $to_date.' 23:59:59';
        $date_filter = " AND E.LIST_DATE BETWEEN TO_DATE('$from_date', 'MM/DD/YYYY HH24:MI:SS') AND
       TO_DATE('$to_date', 'MM/DD/YYYY HH24:MI:SS')";
       //$date_filter = " AND E.LIST_DATE BETWEEN TO_DATE('$from_date 00:00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND
       //TO_DATE('$to_date 23:59:59:59', 'MM/DD/YYYY HH24:MI:SS')";
      }else{
        $date_filter = ' ';
      }
      $print_qry = $this->db->query("SELECT DECODE(PP.PIC_URL,
              null,
              'http://71.78.236.20/item_pictures/master_pictures/image_not_available.jpg',
              PP.PIC_URL) PIC_URL,
       E.EBAY_ITEM_DESC,
       E.EBAY_ITEM_ID,
       NVL(S.F_UPC, ' ') UPC,
       B.BARCODE_NO,
       E.LIST_QTY,
       E.LIST_DATE,
       E.LIST_PRICE,
       E.LISTER_ID,
       B.EBAY_STICKER,
       B.BIN_ID,
       BM.BIN_TYPE || '-' || BM.BIN_NO BIN_NAME,
       EM.USER_NAME LISTER_NAME,
       C.COND_NAME,
       E.LIST_DATE
  FROM LZ_BARCODE_MT b,
       EBAY_LIST_MT E,
       BIN_MT BM,
       EMPLOYEE_MT EM,
       LZ_ITEM_COND_MT C,
       LZ_ITEM_SEED S,
       (SELECT P.EBAY_ID,
               DECODE(P.IMG_KEY,
                      NULL,
                      'http://71.78.236.20/item_pictures/master_pictures/image_not_available.jpg',
                      'https://i.ebayimg.com/images/g/' || p.img_key ||
                      '/s-l110.jpg') PIC_URL,
               P.LOCAL_URL
          FROM LZ_LISTED_ITEM_PIC P
         WHERE P.PIC_ID IN (SELECT MIN(PIC_ID) PIC_ID
                              FROM LZ_LISTED_ITEM_PIC P
                             WHERE P.EBAY_ID IS NOT NULL
                               AND P.IMG_KEY IS NOT NULL
                             GROUP BY EBAY_ID)) PP
 WHERE B.LIST_ID = E.LIST_ID
   AND E.EBAY_ITEM_ID = PP.EBAY_ID(+)
   $date_filter
   AND B.BIN_ID = BM.BIN_ID
   AND EM.EMPLOYEE_ID = E.LISTER_ID
   AND E.SEED_ID = S.SEED_ID
   AND C.ID = B.CONDITION_ID
   AND B.EBAY_STICKER = 0
   AND BM.BIN_TYPE <> 'WB'
   AND B.NOT_FOUND_BY IS NULL
   AND B.EXTENDEDORDERID IS NULL
   AND B.SALE_RECORD_NO IS NULL
   AND B.ORDER_ID IS NULL
   AND B.ITEM_ADJ_DET_ID_FOR_OUT IS NULL
   AND B.ITEM_ADJ_DET_ID_FOR_IN IS NULL
   AND B.LZ_POS_MT_ID IS NULL
   AND B.LZ_PART_ISSUE_MT_ID IS NULL
   AND B.BARCODE_NO NOT IN
       (SELECT DT.BARCODE_NO
          FROM LZ_MERCHANT_BARCODE_MT MT, LZ_MERCHANT_BARCODE_DT DT
         WHERE MT.MT_ID = DT.MT_ID
           AND MT.MERCHANT_ID IN (9))
   AND B.LZ_MANIFEST_ID NOT IN
       (SELECT LZ_MANIFEST_ID
          FROM LZ_MANIFEST_MT MM
         WHERE MM.SINGLE_ENTRY_ID IS NOT NULL)
   AND B.BARCODE_NO NOT IN
       (SELECT DISTINCT L.BARCODE_NO FROM LJ_AGINIG_ITEM_LOG L)
   AND E.LIST_DATE > SYSDATE - 60
 ORDER BY E.LIST_DATE DESC
")->result_array();
      // $j = 0; item_pictures/master_pictures/image_not_available.jpg
      // foreach($print_qry as $record){
      //   //$bar = explode(',', $record['BARCODE_NO']);
      //   $bar_val = $record['BARCODE_NO'];
      //       //$bar_val = $bar[0];
      //       $qry = $this->db->query("SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_DEKIT_US_DT WHERE BARCODE_PRV_NO = '$bar_val' UNION ALL SELECT TO_CHAR(FOLDER_NAME) FOLDER_NAME FROM LZ_SPECIAL_LOTS L WHERE L.BARCODE_PRV_NO =  '$bar_val' ")->result_array();

      //       if (count($qry) >= 1) {

      //           $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
      //           $path = $path->result_array();
      //           $master_path = $path[0]["MASTER_PATH"];

      //           $barcode = $qry[0]["FOLDER_NAME"];
      //           $dir = $master_path . $barcode . "/";
      //           // var_dump($dir);
      //           // exit;

      //       } else {

      //           $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
      //           $path = $path->result_array();
      //           $master_path = $path[0]["MASTER_PATH"];

      //           $get_params = $this->db->query("SELECT BB.BARCODE_NO, S.F_UPC, S.F_MPN, C.COND_NAME FROM LZ_BARCODE_MT BB, LZ_ITEM_SEED S, LZ_ITEM_COND_MT C WHERE BB.BARCODE_NO = '$bar_val' AND BB.ITEM_ID = S.ITEM_ID(+) AND BB.LZ_MANIFEST_ID = S.LZ_MANIFEST_ID(+) AND BB.CONDITION_ID = S.DEFAULT_COND AND C.ID = S.DEFAULT_COND(+) ")->result_array();

      //           //$master_path . @$data_get['result'][0]->UPC . "~" . @$mpn . "/" . @$it_condition . "/";

      //           $upc = @str_replace('/', '_', @$get_params[0]['F_UPC']);
      //           $mpn = @str_replace('/', '_', @$get_params[0]['F_MPN']);
      //           $cond = @$get_params[0]['COND_NAME'];

      //           $dir = $master_path . @$upc . "~" . @$mpn . "/" . @$cond . "/";
      //       }

      //       $dir = preg_replace("/[\r\n]*/", "", $dir);
      //       // var_dump($dir);
      //       // exit;

      //       //var_dump(is_dir($dir));exit;
      //       $base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
      //       if (is_dir($dir)) {
      //           // var_dump($dir);exit;
      //           $images = glob($dir . "\*.{JPG,jpg,GIF,gif,PNG,png,BMP,bmp,JPEG,jpeg}", GLOB_BRACE);
      //           $i = 0;
      //           $pathinfo = pathinfo($images[0]);
      //           $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $images[0]);
      //           $withoutMasterPartUri = str_replace("\\", "", $withoutMasterPartUri);
      //           // $withoutMasterPartUri = preg_replace("/\s+|[[:^print:]]/", "", $withoutMasterPartUri);

      //           $uri = $base_url . $withoutMasterPartUri;
      //           // foreach ($images as $image) {
      //           //     $pathinfo = pathinfo($image);
      //           //     $withoutMasterPartUri = str_replace("D:/wamp/www/", "", $image);
      //           //     $uri[$bar_val][$i] = $base_url . $withoutMasterPartUri;
      //           //     // $base64[$i]['filename'] = $pathinfo['filename'];
      //           //     // $base64[$i]['image64'] = base64_encode(file_get_contents($base_url . $withoutMasterPartUri));
      //           //     $i++;
      //           // }
      //       } else {
      //           $uri = $base_url . "item_pictures/master_pictures/image_not_available.jpg";
      //           // $uri[$bar_val][1] = $bar_val;
      //       }
      //       $print_qry[$j]['image_url'] = $uri;
      //       $j++;
      // }
      //array_walk_recursive($print_qry, function (&$item) {$item = strval($item);});
     // echo json_encode(array($print_qry));
        echo json_encode($print_qry);
    }

    public function get_img($barcode_no){
        //$upc = '678149439229';
        //$part_no = '43922-DVD';
        
        // $qy = $this->db->query("SELECT B.ITEM_ID, B.LZ_MANIFEST_ID, B.CONDITION_ID,S.F_UPC,S.F_MPN , S.SEED_ID , C.COND_NAME
        // FROM LZ_BARCODE_MT B, LZ_ITEM_SEED S , LZ_ITEM_COND_MT C
        // WHERE S.ITEM_ID = B.ITEM_ID
        // AND S.LZ_MANIFEST_ID = B.LZ_MANIFEST_ID
        // AND S.DEFAULT_COND = B.CONDITION_ID
        // AND S.DEFAULT_COND = C.ID
        // AND B.BARCODE_NO = '$barcode_no'")->result_array();
        $qy = $this->db->query("SELECT B.ITEM_ID,
               B.LZ_MANIFEST_ID,
               B.CONDITION_ID,
               DECODE(MT.UPC, '', S.F_UPC, MT.UPC) F_UPC,
               DECODE(MT.MPN, '', S.F_MPN, MT.MPN) F_MPN,
               S.SEED_ID,
               C.COND_NAME
          FROM LZ_BARCODE_MT    B,
               LZ_ITEM_SEED     S,
               LZ_ITEM_COND_MT  C,
               LZ_PURCH_ITEM_MT MT,
               LZ_PURCH_ITEM_DT DT
         WHERE S.ITEM_ID = B.ITEM_ID
           AND MT.PURCH_MT_ID = DT.PURCH_MT_ID
           AND DT.BARCODE_NO = B.BARCODE_NO
           AND S.LZ_MANIFEST_ID = B.LZ_MANIFEST_ID
           AND S.DEFAULT_COND = B.CONDITION_ID
           AND S.DEFAULT_COND = C.ID
           AND B.BARCODE_NO = '$barcode_no'")->result_array();
        if(count($qy) > 0){
        $upc = $qy[0]['F_UPC'];
        $part_no = $qy[0]['F_MPN'];
        $condid = $qy[0]['COND_NAME'];
        $seed_id = $qy[0]['SEED_ID'];
        $cond = $qy[0]['CONDITION_ID'];
        
        }else{
        return false;
        }
        
        $query = $this->db->query("SELECT * FROM 
        (SELECT S.DEFAULT_COND,
        S.F_MPN ITEM_MT_MFG_PART_NO,
        S.F_UPC ITEM_MT_UPC,
        C.COND_NAME CONDITIONS_SEG5,
        MAX(S.DATE_TIME) DATE_TIME
        FROM LZ_ITEM_SEED S, LZ_ITEM_COND_MT C
        WHERE (S.F_UPC = '$upc' OR UPPER(S.F_MPN) = UPPER('$part_no'))
        AND C.ID = S.DEFAULT_COND
        AND S.DEFAULT_COND = '$cond'
        GROUP BY S.DEFAULT_COND,
        S.F_MPN ,
        S.F_UPC ,
        C.COND_NAME 
        ) 
        ORDER BY DATE_TIME DESC
        ");
        if($query->num_rows() === 0){
        $query = $this->db->query("SELECT LZ_MANIFEST_ID, ITEM_MT_DESC, CONDITIONS_SEG5, ITEM_MT_MFG_PART_NO, ITEM_MT_UPC, ITEM_MT_MANUFACTURE, ITEM_MT_BBY_SKU, E_BAY_CATA_ID_SEG6, MAIN_CATAGORY_SEG1, SUB_CATAGORY_SEG2, BRAND_SEG3, ORIGIN_SEG4, '' SHIPPING_SERVICE FROM (SELECT LZ_MANIFEST_ID, ITEM_MT_DESC, trim(ITEM_MT_MFG_PART_NO) ITEM_MT_MFG_PART_NO, trim(ITEM_MT_UPC) ITEM_MT_UPC, ITEM_MT_MANUFACTURE, ITEM_MT_BBY_SKU, E_BAY_CATA_ID_SEG6, MAIN_CATAGORY_SEG1, SUB_CATAGORY_SEG2, BRAND_SEG3, ORIGIN_SEG4, NVL(C.COND_NAME, DT.E_BAY_CATA_ID_SEG6) CONDITIONS_SEG5 FROM LZ_MANIFEST_DET DT , lz_item_cond_mt c WHERE (DT.ITEM_MT_UPC = '$upc' or dt.item_mt_mfg_part_no = upper('$part_no')) AND DT.E_BAY_CATA_ID_SEG6 NOT IN ('N/A', 'other', 'Other', 'OTHER', '123456') AND C.ID = DT.CONDITIONS_SEG5(+) AND C.ID = '$cond' AND DT.E_BAY_CATA_ID_SEG6 IS NOT NULL AND DT.LAPTOP_ITEM_CODE IS NOT NULL ORDER BY DT.LZ_MANIFEST_ID DESC) WHERE ROWNUM = 1"); 
        }
        
        if($query->num_rows()>0){
        $manifest_id = $query->result_array();
        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = 1");
        $path = $path->result_array(); 
        $master_path = $path[0]["MASTER_PATH"];
        foreach ($manifest_id as $value) {
        $condid = $value['CONDITIONS_SEG5'];
        $f_mpn = @$value['ITEM_MT_MFG_PART_NO'];
        $item_mt_mfg_part_no = str_replace('/', '_', @$value['ITEM_MT_MFG_PART_NO']);
        $item_mt_upc = $value['ITEM_MT_UPC'];
        //picture code start
        $dir1 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no;
        $dir2 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid;
        $dir3 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';
        $dir = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';
        $dir = preg_replace("/[\r\n]*/","",$dir);
        if (is_dir($dir)) {
            $path = $this->db->query("UPDATE LZ_ITEM_SEED S SET S.F_UPC = '$item_mt_upc' , S.F_MPN = '$f_mpn' WHERE S.SEED_ID = '$seed_id'");
        break;
        }
        
        }// end foreach ($manifest_id as $value) { 
        // $condid = $manifest_id[0]['CONDITIONS_SEG5'];
        // $item_mt_mfg_part_no = str_replace('/', '_', @$manifest_id[0]['ITEM_MT_MFG_PART_NO']);
        // $item_mt_upc = $manifest_id[0]['ITEM_MT_UPC'];
        // //picture code start
        
        // $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = 1");
        // $path = $path->result_array(); 
        // $master_path = $path[0]["MASTER_PATH"];
        
        // $dir1 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no;
        // $dir2 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid;
        // $dir3 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';
        // $dir = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';
        
        // $dir = preg_replace("/[\r\n]*/","",$dir);
        if (is_dir($dir)) {
        
        
        }else{ // end if (is_dir($dir)) {
        // $dekit = $this->db->query("SELECT *
        // FROM (SELECT L.FOLDER_NAME
        // FROM LZ_SPECIAL_LOTS L
        // WHERE (L.CARD_UPC = '$upc' OR
        // UPPER(L.CARD_MPN) = UPPER('$part_no'))
        // AND L.FOLDER_NAME IS NOT NULL
        // AND L.CONDITION_ID = '$cond'
        // ORDER BY L.SPECIAL_LOT_ID DESC)
        // WHERE ROWNUM = 1")->result_array();
        $dekit = $this->db->query("SELECT *
        FROM (SELECT L.FOLDER_NAME
        FROM LZ_SPECIAL_LOTS L
        WHERE (L.CARD_UPC = '$upc' OR
        UPPER(L.CARD_MPN) = UPPER('$part_no'))
        AND L.FOLDER_NAME IS NOT NULL
        AND L.CONDITION_ID = '$cond'
        ORDER BY L.SPECIAL_LOT_ID DESC)
        WHERE ROWNUM = 1
        
        union all
        SELECT *
        FROM (SELECT Ld.FOLDER_NAME
        FROM lz_dekit_us_dt Ld, lz_catalogue_mt ct
        WHERE (ct.upc = '$upc' OR
        UPPER(ct.mpn) = UPPER('$part_no'))
        and ct.catalogue_mt_id = ld.catalog_mt_id
        AND Ld.FOLDER_NAME IS NOT NULL
        AND Ld.CONDITION_ID = '$cond'
        ORDER BY Ld.Lz_Dekit_Us_Dt_Id DESC)
        WHERE ROWNUM = 1")->result_array();
        if(count($dekit)>0){
        $folder_name = $dekit[0]['FOLDER_NAME'];
        $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG WHERE PATH_ID = 2");
        $path = $path->result_array(); 
        $master_path = $path[0]["MASTER_PATH"];
        //$master_path.@$row['UPC'].'~'.$mpn.'/'.@$it_condition
        $dir = $master_path.$folder_name.'/thumb/';
        $src1 = $master_path.$folder_name;
        $src2 = $master_path.$folder_name.'/thumb/';
        @mkdir($dir1); 
        @mkdir($dir2); 
        @mkdir($dir3); 
        $this->custom_copy($src1,$dir2);
        $this->custom_copy($src2,$dir3);
        }
         if (is_dir($dir)) {
            $path = $this->db->query("UPDATE LZ_ITEM_SEED S SET S.F_UPC = '$upc' , S.F_MPN = '$part_no' WHERE S.SEED_ID = '$seed_id'");
        break;
        }
        }
        
        
        }else{//if($query->num_rows()>0){
        echo "No histroy found";
        }
        //picture code end
        
        //return array('dekitted_pics'=>$dekitted_pics,'parts'=>$parts,'uri'=>$uri);
        }

    // public function get_img($upc,$part_no,$cond){
    //     //$upc = '678149439229';
    //     //$part_no = '43922-DVD';
    //           $query = $this->db->query("SELECT * FROM (SELECT S.LZ_MANIFEST_ID, S.ITEM_TITLE ITEM_MT_DESC, S.DEFAULT_COND, S.F_MPN ITEM_MT_MFG_PART_NO, S.F_UPC ITEM_MT_UPC, S.F_MANUFACTURE ITEM_MT_MANUFACTURE, S.CATEGORY_ID E_BAY_CATA_ID_SEG6, '' MAIN_CATAGORY_SEG1, '' SUB_CATAGORY_SEG2, S.CATEGORY_NAME BRAND_SEG3, 'US' ORIGIN_SEG4, S.SHIPPING_SERVICE, C.COND_NAME CONDITIONS_SEG5 FROM LZ_ITEM_SEED S , LZ_ITEM_COND_MT C WHERE (S.F_UPC = '$upc' OR upper(s.f_mpn) = upper('$part_no')) AND C.ID = S.DEFAULT_COND AND S.DEFAULT_COND = '$cond' ORDER BY S.SEED_ID DESC) WHERE ROWNUM = 1");
    //         if($query->num_rows() === 0){
    //           $query = $this->db->query("SELECT LZ_MANIFEST_ID, ITEM_MT_DESC, CONDITIONS_SEG5, ITEM_MT_MFG_PART_NO, ITEM_MT_UPC, ITEM_MT_MANUFACTURE, ITEM_MT_BBY_SKU, E_BAY_CATA_ID_SEG6, MAIN_CATAGORY_SEG1, SUB_CATAGORY_SEG2, BRAND_SEG3, ORIGIN_SEG4, '' SHIPPING_SERVICE FROM (SELECT LZ_MANIFEST_ID, ITEM_MT_DESC, trim(ITEM_MT_MFG_PART_NO) ITEM_MT_MFG_PART_NO, trim(ITEM_MT_UPC) ITEM_MT_UPC, ITEM_MT_MANUFACTURE, ITEM_MT_BBY_SKU, E_BAY_CATA_ID_SEG6, MAIN_CATAGORY_SEG1, SUB_CATAGORY_SEG2, BRAND_SEG3, ORIGIN_SEG4, NVL(C.COND_NAME, DT.E_BAY_CATA_ID_SEG6) CONDITIONS_SEG5 FROM LZ_MANIFEST_DET DT , lz_item_cond_mt c WHERE (DT.ITEM_MT_UPC = '$upc' or dt.item_mt_mfg_part_no = upper('$part_no')) AND DT.E_BAY_CATA_ID_SEG6 NOT IN ('N/A', 'other', 'Other', 'OTHER', '123456') AND C.ID = DT.CONDITIONS_SEG5(+) AND C.ID = '$cond' AND DT.E_BAY_CATA_ID_SEG6 IS NOT NULL AND DT.LAPTOP_ITEM_CODE IS NOT NULL ORDER BY DT.LZ_MANIFEST_ID DESC) WHERE ROWNUM = 1"); 
    //         }
        
    //         if($query->num_rows()>0){
    //           $manifest_id = $query->result_array();
    //           $condid = $manifest_id[0]['CONDITIONS_SEG5'];
        
    //           $item_mt_mfg_part_no = str_replace('/', '_', @$manifest_id[0]['ITEM_MT_MFG_PART_NO']);
              
        
    //           $item_mt_upc = $manifest_id[0]['ITEM_MT_UPC'];
    //         //picture code start
        
    //         $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 1");
    //         $path = $path->result_array();    
    //         $master_path = $path[0]["MASTER_PATH"];
        
    //         $dir1 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no;
    //         $dir2 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid;
    //         $dir3 = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';
    //         $dir = $master_path.@$item_mt_upc.'~'.$item_mt_mfg_part_no.'/'.@$condid.'/thumb/';
        
    //         $dir = preg_replace("/[\r\n]*/","",$dir);
    //         if (is_dir($dir)) {
        
        
    //         }else{  // end if (is_dir($dir)) {
    //           $dekit = $this->db->query("SELECT *
    //                         FROM (SELECT L.FOLDER_NAME
    //                                 FROM LZ_SPECIAL_LOTS L
    //                                WHERE (L.CARD_UPC = '$upc' OR
    //                                      UPPER(L.CARD_MPN) = UPPER('$part_no'))
    //                                  AND L.FOLDER_NAME IS NOT NULL
    //                                  AND L.CONDITION_ID = '$cond'
    //                                ORDER BY L.SPECIAL_LOT_ID DESC)
    //                        WHERE ROWNUM = 1")->result_array();
    //           if(count($dekit)>0){
    //             $folder_name = $dekit[0]['FOLDER_NAME'];
    //             $path = $this->db->query("SELECT MASTER_PATH FROM LZ_PICT_PATH_CONFIG  WHERE PATH_ID = 2");
    //             $path = $path->result_array();    
    //             $master_path = $path[0]["MASTER_PATH"];
    //             //$master_path.@$row['UPC'].'~'.$mpn.'/'.@$it_condition
    //             $dir = $master_path.$folder_name.'/thumb/';
    //             $src1 = $master_path.$folder_name;
    //             $src2 = $master_path.$folder_name.'/thumb/';
    //             @mkdir($dir1); 
    //             @mkdir($dir2); 
    //             @mkdir($dir3); 
    //             $this->custom_copy($src1,$dir2);
    //             $this->custom_copy($src2,$dir3);
    //           }
    //         }
              
        
    //         }else{//if($query->num_rows()>0){
    //          echo "No histroy found";
    //         }
    //         //picture code end
          
    //         //return array('dekitted_pics'=>$dekitted_pics,'parts'=>$parts,'uri'=>$uri);
    //     } 
        function custom_copy($src, $dst) { 
        
        // open the source directory 
        $dir = opendir($src); 
        
        // Make the destination directory if not exist 
        @mkdir($dst); 
        
        // Loop through the files in source directory 
        while( $file = readdir($dir) ) { 
        
        if (( $file != '.' ) && ( $file != '..' )) { 
        if ( is_dir($src . '/' . $file) ) 
        { 
        
        // Recursively calling custom copy function 
        // for sub directory 
        $this->custom_copy($src . '/' . $file, $dst . '/' . $file); 
        
        } 
        else { 
        copy($src . '/' . $file, $dst . '/' . $file); 
        } 
        } 
        } 
        
        closedir($dir); 
        }

        public function get_cond_result()
  {

    $this->load->model('purchaseApp/m_sync_oracle','handler');
    
    $data = $this->handler->get_cond();

    echo json_encode(array('Results'=>($data)));

  }
    public function get_sync()
    {
        $date = $_POST["date"];

        $this->load->model('purchaseApp/m_sync_oracle','handler');
        
        $data = $this->handler->get_sync($date);
        
        array_walk_recursive($data, function (&$item) {$item = strval($item);});

        echo json_encode(array('Results'=>($data)));

    }

    public function delete_barcode()
    {
        $barcode = $_POST["barcode"];

        $this->load->model('purchaseApp/m_sync_oracle','handler');
        
        $data = $this->handler->delete_barcode($barcode);
        
        // array_walk_recursive($data, function (&$item) {$item = strval($item);});

        $result['success'] = $data;

        echo json_encode($result);

    }
    public function add_remarks()
    {
        $barcode = $_POST["barcode"];
        $remarks = $_POST["remarks"];
        $user_id = $_POST["user_id"];
        
        $this->db->query("UPDATE LZ_MERCHANT_BARCODE_DT DT SET DT.ADMIN_AT =SYSDATE, DT.ADMIN_ID ='$user_id', DT.ADMIN_REMARKS = '$remarks' WHERE DT.BARCODE_NO ='$barcode'"); 
        if($this->db->affected_rows() > 0){
          $result['success'] = true;
        }else{
          $result['success'] = false;
        }

        echo json_encode($result);

    }
  public function getCondtions()
  {

    $this->load->model('purchaseApp/m_sync_oracle','handler');
    
    $data = $this->handler->get_cond();

    echo json_encode(array('Results'=>($data)));

  }
  public function getListedNotFound()
  {

    $this->load->model('purchaseApp/m_sync_oracle','handler');
    
    $data = $this->handler->getListedNotFound();

    array_walk_recursive($data, function (&$item) {$item = strval($item);});

    echo json_encode(array('Results'=>($data)));

  }

  public function getItemActiveQty(){

    $barcode = trim($this->input->post('barcode_no'));
    //$remarks = trim(@$this->input->post('remarks'));
    $remarks = '';
    $user_id = trim($this->input->post('user_id'));

    //$user_id=2;
    //$barcode_no = '12032';//active item
    //$ebay_id = '254518518289';//active item
    $getEbayId = $this->db->query("SELECT EBAY_ITEM_ID FROM LZ_BARCODE_MT WHERE BARCODE_NO = '$barcode'")->result_array();
      if(!empty(@$getEbayId[0]['EBAY_ITEM_ID'])){
        $ebay_id = @$getEbayId[0]['EBAY_ITEM_ID'];
      }else{
        echo json_encode(array('Ack'=>'Failure','message'=>'Barcode Not listed'));
        return array('Ack'=>'Failure','message'=>'Barcode Not listed');  
      }
    
    $get_seller_acct = $this->db->query("SELECT * FROM (SELECT E.LZ_SELLER_ACCT_ID,S.EBAY_LOCAL FROM EBAY_LIST_MT E, LZ_ITEM_SEED S WHERE E.EBAY_ITEM_ID = '$ebay_id'AND S.SEED_ID = E.SEED_ID AND UPPER(E.STATUS) = 'ADD' ORDER BY E.LIST_ID DESC) WHERE  ROWNUM = 1")->result_array();
    if(count($get_seller_acct) > 0){
      $account_id = @$get_seller_acct[0]['LZ_SELLER_ACCT_ID'];
      $site_id = @$get_seller_acct[0]['EBAY_LOCAL'];
    }else{
      $get_seller = $this->db->query("SELECT * FROM (SELECT E.LZ_SELLER_ACCT_ID,S.EBAY_LOCAL FROM EBAY_LIST_MT E, LZ_ITEM_SEED S WHERE E.EBAY_ITEM_ID = '$ebay_id' AND S.SEED_ID = E.SEED_ID ORDER BY E.LIST_ID DESC) WHERE  ROWNUM = 1")->result_array(); 
      $account_id = @$get_seller[0]['LZ_SELLER_ACCT_ID'];
      $site_id = @$get_seller[0]['EBAY_LOCAL'];      
    }


    if(empty($account_id)){
      $get_seller = $this->db->query("SELECT E.LZ_SELLER_ACCT_ID FROM EBAY_LIST_MT E WHERE E.EBAY_ITEM_ID = '$ebay_id' AND ROWNUM=1")->result_array(); 
      $account_id = @$get_seller[0]['LZ_SELLER_ACCT_ID'];
      $site_id = 0;      
    }
    if(empty($account_id)){
       $data = "Account id against this ebay Id:".$ebay_id. " is Not Found in system.";
       //$data = 1;
       // echo json_encode($data);
       // return json_encode($data);
       echo json_encode(array('Ack'=>'Failure','message'=>$data));
        return array('Ack'=>'Failure','message'=>$data); 
      
    }
    $data['ebay_id'] = $ebay_id;
    $data['site_id'] = $site_id;
    $data['account_name'] = $account_id;// used in configuration.php
    //$data['forceRevise'] = $forceRevise;
    //$this->session->unset_userdata('active_listing');
   $result = $this->load->view('ebay/trading/getItemActiveQty',$data,true);
   $active_qty =  json_encode( json_decode(htmlspecialchars_decode($result), true));
   $json_res = json_decode($active_qty); // For making it as an Object
   //var_dump($json_res->current_qty);exit;
   if($json_res->listingstatus === 'Active' AND $json_res->current_qty === 1 ){
    //end item on ebay
      $endRes = $this->endEbayItem($ebay_id,$remarks,$user_id);
      if($endRes['Ack'] !== 'Failure'){
        $this->m_tolist->discardListedBarcode($barcode,$user_id);
      }
      echo json_encode(array('Ack'=>$endRes['Ack'],'message'=>$endRes['message']));
      return array('Ack'=>$endRes['Ack'],'message'=>$endRes['message']);
      //following is the var_dump($endRes) result
      // array (size=2)
      // 'Ack' => string 'Success' (length=7)
      // 'message' => string 'Item Ended Sucessfully' (length=22)
   }else if($json_res->listingstatus === 'Active' AND $json_res->current_qty > 1){// less 1 qty
    //less one qty on ebay
      $quantity = $json_res->current_qty - 1;// less 1 qty
      $revRes = $this->reviseEbayQty($ebay_id,$quantity,$remarks,$user_id);
      if($revRes['Ack'] !== 'Failure'){
        $this->m_tolist->discardListedBarcode($barcode,$user_id);
      }
      echo json_encode(array('Ack'=>$revRes['Ack'],'message'=>$revRes['message']));
      return array('Ack'=>$revRes['Ack'],'message'=>$revRes['message']);

   }else if($json_res->listingstatus === 'Completed'){
    // item is sold

    echo json_encode(array('Ack'=>'Failure','message'=>'Item Sold on eBay'));
    return array('Ack'=>'Failure','message'=>'Item Sold on eBay');
   }else if($json_res->listingstatus === 'Ended'){
    // item is ended
    echo json_encode(array('Ack'=>'Failure','message'=>'Item Ended on eBay'));
    return array('Ack'=>'Failure','message'=>'Item Ended on eBay');
   }else{
    echo json_encode(array('Ack'=>'Failure','message'=>'Some eror occur. Please Contact Your Administartor'));
    return array('Ack'=>'Failure','message'=>'Some eror occur. Please Contact Your Administartor');
   }

  }
  public function endEbayItem($ebay_id,$remarks='',$user_id=81){

    $result['ebay_id'] = $ebay_id;
    $result['remarks'] = $remarks;
    $result['user_id'] = $user_id;

    $get_seller = $this->db->query("SELECT E.LZ_SELLER_ACCT_ID, A.ACCOUNT_NAME SELL_ACCT_DESC , S.EBAY_LOCAL FROM EBAY_LIST_MT E, LJ_MERHCANT_ACC_DT A , LZ_ITEM_SEED S WHERE A.ACCT_ID = E.LZ_SELLER_ACCT_ID AND S.SEED_ID = E.SEED_ID AND E.EBAY_ITEM_ID =  '$ebay_id' AND ROWNUM = 1")->result_array(); 
    $account_name = @$get_seller[0]['LZ_SELLER_ACCT_ID'];
    
    if(!empty(@$get_seller[0]['EBAY_LOCAL'])){
      $site_id = @$get_seller[0]['EBAY_LOCAL'];
    }else{
      $site_id = 0;
    }
    $result['site_id'] = $site_id;
    if(!empty(@$account_name)){
      $result['account_name'] = $account_name;
    }
    $data = $this->load->view('ebay/trading/endEbayItem',$result,true);
    $endItemResponse =  json_encode( json_decode(htmlspecialchars_decode($data), true));
    //$jason = json_decode($trackingUpdated,true); for making it as Array
    $json = json_decode($endItemResponse); // For making it as an Object
    //var_dump($json->ack);
    if($json->ack !== 'Failure'){
      $endResponse = $this->m_tolist->endEbayItem($ebay_id);
      //$result['response'] = $this->m_ordersShopify->deleteItemfromShopify($ebay_id);
      //End item from Shopify
      //echo json_encode(array('Ack'=>'Success','message'=>'Item Ended Sucessfully'));
      return array('Ack'=>'Success','message'=>'Item Ended Sucessfully');
    }else{
      //echo json_encode(array('Ack'=>'Failure','message'=>$json->message));
      return array('Ack'=>'Failure','message'=>$json->message);
    }
 
  }
  public function reviseEbayQty($ebay_id,$quantity,$remarks='',$user_id=81){

    $result['ebay_id'] = $ebay_id;
    $result['remarks'] = $remarks;
    $result['user_id'] = $user_id;
    $result['quantity'] = $quantity;

    $get_seller = $this->db->query("SELECT E.LZ_SELLER_ACCT_ID, A.ACCOUNT_NAME SELL_ACCT_DESC , S.EBAY_LOCAL FROM EBAY_LIST_MT E, LJ_MERHCANT_ACC_DT A , LZ_ITEM_SEED S WHERE A.ACCT_ID = E.LZ_SELLER_ACCT_ID AND S.SEED_ID = E.SEED_ID AND E.EBAY_ITEM_ID =  '$ebay_id' AND ROWNUM = 1")->result_array(); 
    $account_name = @$get_seller[0]['LZ_SELLER_ACCT_ID'];
    
    if(!empty(@$get_seller[0]['EBAY_LOCAL'])){
      $site_id = @$get_seller[0]['EBAY_LOCAL'];
    }else{
      $site_id = 0;
    }
    $result['site_id'] = $site_id;
    if(!empty(@$account_name)){
      $result['account_name'] = $account_name;
    }
    $data = $this->load->view('ebay/trading/reviseItemQuantity',$result,true);
    $revItemResponse =  json_encode( json_decode(htmlspecialchars_decode($data), true));
    //$jason = json_decode($trackingUpdated,true); for making it as Array
    $json = json_decode($revItemResponse); // For making it as an Object
    //var_dump($json,$json->ack);
    if($json->ack !== 'Failure'){
      //echo json_encode(array('Ack'=>'Success','message'=>'Item Qty Revised Sucessfully'));
      return array('Ack'=>'Success','message'=>$json->message);
    }else{
      //echo json_encode(array('Ack'=>'Failure','message'=>$json->message));
      return array('Ack'=>'Failure','message'=>$json->message);
    }
  }
  public function foundBarcode()
  {
    $this->load->model('purchaseApp/m_sync_oracle','handler');
    $data = $this->handler->foundBarcode();
    if($data){
      echo json_encode(array('Ack'=>'Success','message'=>'Item Marked As Found'));
    }else{
      echo json_encode(array('Ack'=>'Failure','message'=>'Item Not Marked As Found'));
    }
  }

}
