<?php
// Load Composer autoloader.
// @link https://github.com/brightnucleus/jasper-client/blob/master/tests/bootstrap.php#L55-L59
$autoloader = dirname( __FILE__ ) . '/vendor/autoload.php';
if ( is_readable( $autoloader ) ) {
	require_once $autoloader;
}
//require __DIR__ . '/vendor/autoload.php';
use Automattic\WooCommerce\Client;
use Automattic\WooCommerce\HttpClient\HttpClient;

$woocommerce = new Client(
	'http://localhost/wordpress-5.4.1/wordpress/',
	'ck_97bdb0b023c0e219690640cd52adf1d35e3f47c7',
	'cs_5ba29a913438118100ddb48b8c95e089a606d1b7',
	[
		'wp_api'  => true,
		'version' => 'wc/v2',
	]
);
// var_dump($woocommerce->get( 'orders'));
// exit;
$prod_data = [
	'name'          => 'A great he product',
	'type'          => 'simple',
	'regular_price' => '15.00',
	'description'   => 'A very meaningful product description',
	'images'        => [
		[
			'src'      => 'https://images.squarespace-cdn.com/content/v1/59d2bea58a02c78793a95114/1589398875141-QL8O2W7QS3B4MZPFWHJV/ke17ZwdGBToddI8pDm48kBVDUY_ojHUJPbTAKvjNhBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmmV5_8-bAHr7cY_ioNsJS_wbCc47fY_dUiPbsewqOAk2CqqlDyATm2OxkJ1_5B47U/image-asset.jpeg',
			'position' => 0,
		],
	],
	'categories'    => [
		[
			'id' => 1,
		],
	],
];

var_dump($woocommerce->post( 'products', $prod_data ));
?>
