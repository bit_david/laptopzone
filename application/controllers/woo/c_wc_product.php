<?php

//require 'E:wamp64/www/laptopzone/woo_vendor/autoload.php';

// use Automattic\WooCommerce\Client;
// use Automattic\WooCommerce\HttpClient\HttpClient;
defined('BASEPATH') or exit('No direct script access allowed');

header("Access-Control-Allow-Origin: *");



// $wc_conf = dirname( __FILE__ ). '/include/wc_config.php';
// 	    if ( is_readable( $wc_conf ) ) {
// 			require_once $wc_conf;
// 			var_dump($wc_url);
// 		}
class c_wc_product extends CI_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->database();
		$this->load->helper('wc_config');
		$this->load->model('woo/m_wc_product');
		$this->load->model('woo/common_model');
		//$autoloader = __DIR__ . '/../woo_vendor/autoload.php';
		//$autoloader = dirname( __FILE__ ) . '/vendor/autoload.php';
		// if ( is_readable( $autoloader ) ) {
		// 	require_once $autoloader;
		// }

		//Helper variable
		$this->woocommerce= wc_config();
		// var_dump('HELOOO');
		// var_dump($this->woocommerce);
		// exit;
		//var_dump($wc['wc_url'] ,$wc['wc_ck'] ,$wc['wc_cs'] ,$wc['wp_api'] ,$wc['wc_version']);exit;
		
		//Make object of woocomerce client
		// $this->woocommerce = new Client(
		// 	$this->wc['wc_url'],
		// 	$this->wc['wc_ck'],
		// 	$this->wc['wc_cs'],
		// 	[
		// 		'wp_api'  => $this->wc['wp_api'],
		// 		'version' => $this->wc['wc_version'],
		// 	]
		// );

	}

	public function addProduct( )
	{

		//exit;

		$barcode_no = $this->input->post('list_barcode');//366225;//$param_barcode_no;
		$listed_by = $this->input->post('userId');//366225;//$param_barcode_no;
		$getMyImageUri = $this->input->post('getMyImageUri');

		
		// $listed_by = $this->input->post("user_id");
		//$listed_by = 81;
		$data = $this->m_wc_product->listOnK2bay($barcode_no);
		//if(count($data['seed_qry']) > 0 AND count($data['img_qry']) > 0 ){
		if(count($data['seed_qry']) > 0 AND count($getMyImageUri) > 0 AND count($data['list_qty']) > 0 ){
		// 	var_dump('asd');
		// exit;
			$item_title = $data['seed_qry'][0]['ITEM_TITLE'];
	    	$currency = $data['seed_qry'][0]['CURRENCY'];
	    	$category_name = $data['seed_qry'][0]['CATEGORY_NAME'];
	    	$category_id = $data['seed_qry'][0]['CATEGORY_ID'];
	    	$item_desc = $data['seed_qry'][0]['ITEM_DESC'];
	    	$upc = $data['seed_qry'][0]['UPC'];
	    	$mpn = $data['seed_qry'][0]['PART_NO'];
	    	$cond_name = $data['seed_qry'][0]['COND_NAME'];
	    	$quantity =  $data['list_qty'][0]['QTY'];//$data['seed_qry'][0]['QUANTITY']; 
	    	$price = $data['seed_qry'][0]['EBAY_PRICE'];
	    	$brand_name = $data['seed_qry'][0]['MANUFACTURE'];
	    	$weight = $data['seed_qry'][0]['WEIGHT'];
	    	$seed_id = $data['seed_qry'][0]['SEED_ID'];
	    	$condition_id = $data['seed_qry'][0]['DEFAULT_COND'];
	    	$lz_manifest_id = $data['seed_qry'][0]['LZ_MANIFEST_ID'];
	    	$item_id = $data['seed_qry'][0]['ITEM_ID'];
	    	$short_desc = $data['seed_qry'][0]['DETAIL_COND'];
	    	$weight = $data['seed_qry'][0]['WEIGHT'];

	    	
	    	//Check Sku in woo db if product exist or not
	    	$generate_sku = $upc.'-'.$mpn;
	    	$params = [
		    'sku' => strtoupper(trim($generate_sku)),
			];
		
			$sku_exist = stdClassObjectToArray($this->woocommerce->get('products', $params));

			// echo "<pre>";
			// print_r($sku_exist);
			// /exit;

			if(!empty($sku_exist)){
				$sku_exist_flag = true;

				$exist_prod_data_arr=[];
				$exist_prod_data = [
						'id' => $sku_exist[0]['id'],
						'stock_quantity' => $sku_exist[0]['stock_quantity'],
						'regular_price' => '$'.$sku_exist[0]['regular_price'],
						'name' => $sku_exist[0]['name'],
						'permalink' => $sku_exist[0]['permalink'],
						'sku' => $sku_exist[0]['sku'],
						'short_description' => str_replace('Condition:','',$sku_exist[0]['short_description'])
					];
					

				
				array_push($exist_prod_data_arr, $exist_prod_data);

				$message = "Item Exist on K2bay";
			    $function_url = 'woo/c_wc_product/addProduct';
				$error = array('Ack'=>'success','message'=>$message,'status' => 'ItemExist','function_url' =>$function_url,'sedd'=>$data['seed_qry'],'Check_exist_sku' => $exist_prod_data_arr);
				echo json_encode($error);
				return json_encode($error);
				//return $error;

			}else{
				$sku_exist_flag = false;
			}
			
			if($sku_exist_flag == false){//if Check Sku in woo db if product exist or not
			// var_dump($sku_exist_flag);
			// exit;
	    	//$this->productRevise();



		    	//create image array  
		    	// $img_array =[];
		    	// $i=0;
		    	// foreach ($data['img_qry']  as $val) {
		    	// 	   	$img_array[]=['src' =>  $val['LIVE_URL'], 'position' => $val['PIC_ORDER']];
		    		   	
		    	// 	$i++;
		    	// } 
		    	$img_array =[];
		    	$i=0;
		    	foreach ($getMyImageUri  as $val) {
		    		   	$img_array[]=['src' =>  $val, 'position' => $i];
		    		   	
		    		$i++;
		    	}    		    	
		    	
		    	//  End of create image array 	    	
		    	/*============================================
		    	=            List item on k2bay            =
		    	============================================*/
		    	//get woo category id 
		    	$get_woo_cat = $this->addCategory($category_id,$category_name);
		    	$category_id = $get_woo_cat['category_id'];

			    $Title = trim(str_replace("  ", ' ', $item_title));
			    $Title = str_replace('"',"\\\"", $Title);
			    
			    $Description = strip_tags($item_desc); //Remove HTML Tags		    
			    $a = $Description;
			    if (strpos($a, 'Please read listing carefully') !== false) {
			        $pos = strpos($a, 'Please read listing carefully');
			        $Description = substr( $Description, 0, $pos);
			        $Description = trim(str_replace("  ", ' ', $Description));
			        $Description = str_replace('"',"\\\"", $Description);        
			        //var_dump("afterstr <br>".$Description);
			    }elseif(strpos($a, 'Holidays') == false){
			        $Description = strip_tags($item_desc);
			        $Description = trim(str_replace("  ", ' ', $Description));
			        $Description = str_replace('"',"\\\"", $Description);
			    }

			    // https://stackoverflow.com/questions/1252693/using-str-replace-so-that-it-only-acts-on-the-first-match
			    // Remove First Occurance of string function
			    function str_replace_first($from, $to, $content)
			    {
			        $from = '/'.preg_quote($from, '/').'/';

			        return preg_replace($from, $to, $content, 1);
			    }

			    $Description = str_replace_first($Title, '', $Description);
			    //var_dump ($Description);exit;
			    $currencyID = trim(str_replace("  ", ' ', $currency));
			    $quantity = trim(str_replace("  ", ' ', $quantity));
			    //$Quantity = 1;
			    $priceValue = trim(str_replace("  ", ' ', $price));
			    //$PictureURL = $all_url;

			    $ConditionDisplayName = trim(str_replace("  ", ' ', $cond_name)); //Condition Name: Used, New etc
			    $ConditionDisplayName = trim(str_replace(array("'"), "''", $cond_name));

			    $brand_name = trim(str_replace("  ", ' ', $brand_name));
			    $brand_name = trim(str_replace(array("'"), "''", $brand_name));

			    $WeightUnit = @$weight;
			    //$WeightUnit = 2.5;
			    
			    $category_name = trim(str_replace("  ", ' ', $category_name));
			    $category_name = trim(str_replace(array("'"), "''", $category_name));

			    	
			    //$barcode_sku = 1234;
			    // if(empty($barcode_sku)){
			    //     $barcode_sku = 'null';
			    // }
			    // if(!is_numeric($upc)){
			    //     $upc = $barcode_sku;
			    // }
			    //Empty MPN Check
			    // ----------------------------------------------------
			    
			    $sys_mpn = $mpn;
			    if(!empty(@$sys_mpn)){
			        @$check_mpn = "";
			    }else if(@$sys_mpn == false){
			        $check_mpn = "<strong>MPN:</strong>  $mpn";
			    }
			    if(empty(@$mpn) && @$mpn == ""){
			        @$check_mpn = "";
			    }else{
			        $check_mpn = "<strong>MPN:</strong>  $mpn";        
			    }
			    //Empty brand_name Check
			    // ----------------------------------------------------
			    if(empty(@$brand_name) && @$brand_name == ""){
			        @$check_brand_name = "";
			    }else{
			        $check_brand = "<strong> Brand:</strong>  $brand_name";    
			    }
			    //Empty Condition Display Name
			    // -----------------------------------------------------
			    if(empty($ConditionDisplayName) && $ConditionDisplayName == ""){
			        @$check_condition = "";
			    }else{
			        $check_condition = "<strong> Condition:</strong>  $ConditionDisplayName";    
			    }

				$prod_data = [
					'name'          => $Title,
					'type'          => 'simple',
					'regular_price' => $priceValue,
					'description'   =>  $Description,
					'short_description' => $check_condition ,
					'stock_quantity' => $quantity,	
					'manage_stock' => true,
					'weight' => $weight,	
					'sku' => strtoupper(trim($generate_sku)),
					'categories' => [
						[
							'id' => $category_id
						]
					],
					
					'dimensions'    => [
						'length' => '12',
						'width' => '10',
						'height' => '10'
					],
					'images'        =>  $img_array,
					'attributes'  => 
						(!empty($this->createProductAttributes($item_id,$lz_manifest_id,$condition_id))) ? $this->createProductAttributes($item_id,$lz_manifest_id,$condition_id) : '',				

				];
				
				// echo "<pre>";
				// print_r(@$prod_data);
				// echo "</pre>";
				// exit;
				$list_data = $this->woocommerce->post( 'products', $prod_data );
				$list_data->seed_id = $seed_id;// append seed_id to object
				$list_data->list_qty = $quantity;// append list_qty to object
				$list_data->listed_by = $listed_by;// append listed_by to object
				$list_data->condition_id = $condition_id;// append condition_id to object
				$list_data->lz_manifest_id = $lz_manifest_id;// append lz_manifest_id to object
				$list_data->item_id = $item_id;// append item_id to object
				$get_prod_id= $list_data->id;
				$product_link = $list_data->permalink;
				$status = 'ADD';

				// update woo brand 
				if(!empty($get_prod_id)){
					// /($brand_name,$object_id){/
					if(!empty($brand_name)){
					$updaye_woo_brand = $this->createBrand($brand_name,$get_prod_id);
					}	    						
					
					$data = $this->m_wc_product->insertK2bayListMt($list_data,$status);
					if($data['ckdata'] ===  'true'){


						$message = "Item Listed On K2bay";
					    $function_url = 'woo/c_wc_product/addProduct';
						$error = array('Ack'=>'success','status' => 'ItemListed','message'=>$message,'function_url' =>$function_url,'k2bay_link' => $product_link,'k2bay_list_query' => $data['k2bay_list_query']);
						echo json_encode($error);
						return json_encode($error);
						//return $error;
						
					}else{
						$message = " Inventory not updated in ebay db";
						$function_url = 'woo/c_wc_product/addProduct';
					 	$error = array('Ack'=>'Failure','message'=>$message,'function_url' =>$function_url);
						// also remove product from woo db if not insert in oracle db
						echo json_encode($error);
						return json_encode($error);
						//return $error;
					}

				}else{
						$message = " Inventory not updated in woo db and ebay db";
						$function_url = 'woo/c_wc_product/addProduct';
						$error = array('Ack'=>'Failure','message'=>$message,'function_url' =>$function_url);
						echo json_encode($error);
						return json_encode($error);
						//return $error;
				}
							 
				

			}//end if Check Sku in woo db if product exist or not
		}else{
 
			if(count($data['seed_qry']) <1){
				$err_mesg = 'Seed Not Avaiable';
			}else if(count($getMyImageUri) <1){
				$err_mesg = 'Image Not Avaiable in Db';

			}else if(count($data['list_qty']) <1){
				$err_mesg = 'Qty Not Avaiable to LisT on k2bay';
			}

						$message = " Data incomplete. ". $err_mesg ;
						// //seed_qry count =".count($data['seed_qry']).
						// " img_qry count =".count($data['img_qry']).
						// " available quantity count =".count($data['list_qty']);
						$function_url = 'woo/c_wc_product/addProduct';
						$error = array('Ack'=>'Failure','message'=>$message,'function_url' =>$function_url);
						echo json_encode($error);
						return json_encode($error);
						//return $error;
					
		}//end if(count($data['seed_qry']) > 0 AND count($data['img_qry']) > 0){
		
    	
	}
	
	public function addCategory($cat_id,$cat_name)
	{
		$parent = 0;
		//$cat_id	= '177659';
		//$cat_name = 'Ink Cartridges';
		$json_response = [];
		$tree = $this->common_model->catTree($cat_id);
		$tree = stdClassObjectToArray($tree);
		$tree = $tree[0]['CAT_TRE'];
		if($tree){
		//echo $tree.'<br>';
		$tree_array = explode("\\",$tree);
		//echo '<pre>';
		//print_r($tree_array);
		$term = $tree_array[sizeof($tree_array) - 1];


		$leaf_category_exits = $this->categoryExistByApi($term);
		$leaf_category_exits = stdClassObjectToArray($leaf_category_exits);
		
		if(isset($leaf_category_exits) && !empty($leaf_category_exits)){
			
			//Leaf category exist 
			$json_response = ['success' => 'true' ,'status' => 'Leaf Exist', 'message' => 'Leaf category exist','Category' => $leaf_category_exits[0]['name'] , 'category_id' => $leaf_category_exits[0]['id']];
			}else{
				for($i = 0; $i < sizeof($tree_array); $i++){
					$category_exist = $this->categoryExistByApi($tree_array[$i],$parent);
					if($category_exist){						
						$parent = stdClassObjectToArray($category_exist);
						$parent = $parent[0]['id'];					
						
						}else{
							$data = [
								'name' => $tree_array[$i],
								'parent' => $parent
								// 'image' => [
								//     'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg'
								// ]
								//'slug' => 'computer-s-networking-ok-kafd'
							];
			
							$categories = $this->woocommerce->post( 'products/categories',$data);
							$category = json_decode(json_encode($categories), true);
							$parent 	=	$category['id'];
							//print_r($categories);
						}
					}
					$json_response = ['success' => 'true' ,'status' => 'Generate tree', 'message' => 'Leaf category created','Category' => $category['name'] , 'category_id' => $category['id']];
				}	
			}
		else{
		$leaf_category_exits = $this->categoryExistByApi($cat_name);
		$leaf_category_exits = stdClassObjectToArray($leaf_category_exits);
		//print_r($leaf_category_exits);die;
			if(empty($leaf_category_exits) || $leaf_category_exits == ''){
				$data = [
					'name' => $cat_name,
					'parent' => $parent
					// 'image' => [
					//     'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg'
					// ]
					//'slug' => 'computer-s-networking-ok-kafd'
				];
				$category = $this->woocommerce->post( 'products/categories',$data);
				$category = stdClassObjectToArray($category);
				if(isset($category) || !empty($category) || $category != ''){
					$json_response = ['success' => 'true' ,'status' => 'Parent category created', 'message' => 'Category created successfully','Category' => $category['name'] , 'category_id' => $category['id']];

				}else{
					$json_response = ['success' => 'false', 'message' => 'Category creation failed or category already exist'];
				}
			}else{
				$json_response = ['success' => 'failed' ,'status' => 'Parent category exist', 'message' => 'Category already exists','Category' => $leaf_category_exits[0]['name'] , 'category_id' => $leaf_category_exits[0]['id']];
			}	
		}

		return $json_response;
		//exit(1);
	}

	// public function categoryExist(){
	// 	$term = 'samsung';//samsung,clothing
	// 	$taxonomy = 'product_brand'; //product_cat ,product_brand 
	// 	$parent = 0;
	// 	$isExist = $this->common_model->termExists($term, $taxonomy, $parent);
	// 	return $isExist;
	// }
	public function termExist($term,$taxonomy){
		//Sample Terms e.g samsung,clothing
		//Sample Taxonomy e.g product_cat ,product_brand 
		$parent = 0;
		$slugy_fy_term = slugify($term);
		//echo $slugy_fy_term." ".$taxonomy;die;
		$isExist = $this->common_model->termExists($slugy_fy_term, $taxonomy, $parent);
		return $isExist;
	}


	public function categoryExistByApi($term,$parent = false){
		$text = $term;
		$slugy_fy = slugify($text);
		if($parent){
			$data = [
				'slug' => $slugy_fy,
				'parent' => $parent
			];
		}else{
			$data = [
				'slug' => $slugy_fy
			];
		}
		$result = $this->woocommerce->get( 'products/categories',$data);
		if(!empty($result)){
			return $result;
		}else{
			return false;
		}
	}

	public function createBrand($brand_name,$object_id){//object_id which is product id 
		 //$brand_name = 'Ryobi';
		 $taxonomy = 'product_brand';
		//$object_id = '5069'; //Object id with reference of term_taxonomy_id insert in wp_term_relationships
		$brand_description = '';
		$brand_parent = 0;
		//Check brand if already exists
		$brand_exist = $this->termExist($brand_name,$taxonomy);
		
		if(!$brand_exist){

			$result	= $this->common_model->createTerm($brand_name,slugify($brand_name),$taxonomy,$brand_description,$brand_parent,$object_id);
			if($result){
				return $result;
			}
		}
		else {
			$term_id = $brand_exist[0]['term_id'];
			$term_taxonomy_id = $brand_exist[0]['term_taxonomy_id'];
			$taxonomy_count = $brand_exist[0]['taxonomy_count'];

			//Attach brand with product
			$data = [
				'object_id' => $object_id,
				'term_taxonomy_id' => $brand_exist[0]['term_taxonomy_id']
			];
			$this->common_model->makeTermRelation($data);

			//Update wp_term_taxonomy count
			$taxonomy_count = $taxonomy_count + 1;
			$taxonomy_update = [
				'count' => $taxonomy_count 
			];

			$taxonomy_where = [
				'term_taxonomy_id' => $term_taxonomy_id
			];

			$this->common_model->update('wp_term_taxonomy',$taxonomy_update,$taxonomy_where);

			//Update wp_termmeta meta_value 
			$termmeta_where = [
				'term_id' => $term_id
			];
			$result = $this->common_model->getRecord('meta_value','wp_termmeta',$termmeta_where);
			
			$termmeta_update = [
				'meta_value' => $result['meta_value'] + 1
			];
			
			$this->common_model->update('wp_termmeta',$termmeta_update,$termmeta_where);

			$response = ['Success' => 'true','Status' => 'Brand assign ,update term relationships,taxonomy count updated and update termmeta', 'term_id' => $term_id, 'term_taxonomy_id' => $term_taxonomy_id];
			
			return $response;
		} 
	}

	public function createProductAttributes($item_id,$lz_manifest_id,$condition_id){
		//$item_id = 82708;
		//$category_id = 54257;
		$attribut_array = [];
		$brand_flag = false;
		$mpn_flag = false;
		$upc_flag = false;

		$attributes = $this->common_model->getItemAttributesFromListing($item_id,$lz_manifest_id,$condition_id);
		if(isset($attributes) && !empty($attributes)){
			foreach($attributes as $atr){
				$atr['SPECIFICS_NAME'] = strtoupper(trim($atr['SPECIFICS_NAME']," "));
				if($atr['SPECIFICS_NAME'] == 'UPC'){
					$upc_flag = true;
					$upc_value = $atr['SPECIFICS_VALUE'];
				}else if($atr['SPECIFICS_NAME'] == 'BRAND'){
					$brand_flag = true;
					$brand_value = $atr['SPECIFICS_VALUE'];
				}else if($atr['SPECIFICS_NAME'] == 'MPN'){
					$mpn_flag = true;
					$mpn_value = $atr['SPECIFICS_VALUE'];
				}else{
					$data = [
						'name' => $atr['SPECIFICS_NAME'],
						'slug' => slugify($atr['SPECIFICS_NAME']),
						'type' => 'select',
						'order_by' => 'menu_order',
						'has_archives' => true,
						'variation' => true,
						'visible'   => true,
						'has_archives' => true,
						'options' => [''. $atr['SPECIFICS_VALUE'].''],
					];
					array_push($attribut_array, $data);
				}
				
				// try{
				// 	$result = $this->woocommerce->post( 'products/attributes/',$data);
				// 	if(!empty($result)){
				// 		print_r($result);
				// 	}else{
				// 		echo false;
				// 	}
				// }catch(\Exception $e){
				// 	echo $e->getMessage() .'<br>';
				// }	
			}
		}

		if($upc_flag){
			$upc = [
				'name' => 'UPC',
				'slug' => slugify('UPC'),
				'type' => 'select',
				'order_by' => 'menu_order',
				'has_archives' => true,
				'variation' => true,
				'visible'   => true,
				'has_archives' => true,
				'options' => [''. $upc_value .''],
			];
			array_push($attribut_array, $upc);
		}
		
		if($brand_flag){
			$brand = [
				'name' => 'BRAND',
				'slug' => slugify('Brand'),
				'type' => 'select',
				'order_by' => 'menu_order',
				'has_archives' => true,
				'variation' => true,
				'visible'   => true,
				'has_archives' => true,
				'options' => [''. $brand_value .''],
			];
			array_push($attribut_array, $brand);
		}
		
		if($mpn_flag){
			$mpn = [
				'name' => 'MPN',
				'slug' => slugify('MPN'),
				'type' => 'select',
				'order_by' => 'menu_order',
				'has_archives' => true,
				'variation' => true,
				'visible'   => true,
				'has_archives' => true,
				'options' => [''. $mpn_value .''],
			];
			array_push($attribut_array, $mpn);
		}
		// echo '<pre>';
		// print_R($attribut_array);
		return $attribut_array;
	}
 	

 	public function productReviseK2bay(){
 		$param = $this->input->post('param');
        // var_dump($param);
        // exit;

 	    $barcode_no = $this->input->post('list_barcode');//366225;//$param_barcode_no;
		$listed_by = $this->input->post('userId');//366225;//$param_barcode_no;
		$param = $this->input->post('param');

		$data = $this->m_wc_product->listOnK2bay($barcode_no);
		//if(count($data['seed_qry']) > 0 AND count($data['img_qry']) > 0 ){
		
		if(count($data['seed_qry']) > 0 AND count($data['list_qty']) > 0 || $param ==='without_qty' ){
		// 	var_dump('asd');
		// exit;
			$item_title = $data['seed_qry'][0]['ITEM_TITLE'];
	    	$currency = $data['seed_qry'][0]['CURRENCY'];
	    	$category_name = $data['seed_qry'][0]['CATEGORY_NAME'];
	    	$category_id = $data['seed_qry'][0]['CATEGORY_ID'];
	    	$item_desc = $data['seed_qry'][0]['ITEM_DESC'];
	    	$upc = $data['seed_qry'][0]['UPC'];
	    	$mpn = $data['seed_qry'][0]['PART_NO'];
	    	$cond_name = $data['seed_qry'][0]['COND_NAME'];

	    	if(isset($data['list_qty'][0]['QTY'])){

	    		$quantity =  @$data['list_qty'][0]['QTY'];
	    	}else{
	    		$quantity = 0;
	    	}
	    	
	    	//$data['seed_qry'][0]['QUANTITY']; 
	    	$price = $data['seed_qry'][0]['EBAY_PRICE'];
	    	$brand_name = $data['seed_qry'][0]['MANUFACTURE'];
	    	$weight = $data['seed_qry'][0]['WEIGHT'];
	    	$seed_id = $data['seed_qry'][0]['SEED_ID'];
	    	$condition_id = $data['seed_qry'][0]['DEFAULT_COND'];
	    	$lz_manifest_id = $data['seed_qry'][0]['LZ_MANIFEST_ID'];
	    	$item_id = $data['seed_qry'][0]['ITEM_ID'];
	    	$short_desc = $data['seed_qry'][0]['DETAIL_COND'];
	    	$weight = $data['seed_qry'][0]['WEIGHT'];

	    	
	    	//Check Sku in woo db if product exist or not
	    	$generate_sku = $upc.'-'.$mpn;
	    	$params = [
		    'sku' => strtoupper(trim($generate_sku)),
			];
		
			$sku_exist = stdClassObjectToArray($this->woocommerce->get('products', $params));

			if(!empty($sku_exist)){
				//$sku_exist_flag = true;
				$sku_exist_flag = false;
				$product_id = $sku_exist[0]['id'];

			}else{
				$sku_exist_flag = false;
			}
			
			if($sku_exist_flag == false){//if Check Sku in woo db if product exist or not			
		    	
		    	//  End of create image array 	    	
		    	/*============================================
		    	=            List item on k2bay            =
		    	============================================*/
		    	//get woo category id 
		    	$get_woo_cat = $this->addCategory($category_id,$category_name);
		    	$category_id = $get_woo_cat['category_id'];

			    $Title = trim(str_replace("  ", ' ', $item_title));
			    $Title = str_replace('"',"\\\"", $Title);
			    
			    $Description = strip_tags($item_desc); //Remove HTML Tags		    
			    $a = $Description;
			    if (strpos($a, 'Please read listing carefully') !== false) {
			        $pos = strpos($a, 'Please read listing carefully');
			        $Description = substr( $Description, 0, $pos);
			        $Description = trim(str_replace("  ", ' ', $Description));
			        $Description = str_replace('"',"\\\"", $Description);        
			        //var_dump("afterstr <br>".$Description);
			    }elseif(strpos($a, 'Holidays') == false){
			        $Description = strip_tags($item_desc);
			        $Description = trim(str_replace("  ", ' ', $Description));
			        $Description = str_replace('"',"\\\"", $Description);
			    }

			    // https://stackoverflow.com/questions/1252693/using-str-replace-so-that-it-only-acts-on-the-first-match
			    // Remove First Occurance of string function
			    function str_replace_first($from, $to, $content)
			    {
			        $from = '/'.preg_quote($from, '/').'/';

			        return preg_replace($from, $to, $content, 1);
			    }

			    $Description = str_replace_first($Title, '', $Description);
			    //var_dump ($Description);exit;
			    $currencyID = trim(str_replace("  ", ' ', $currency));
			    $quantity = trim(str_replace("  ", ' ', $quantity));
			    //$Quantity = 1;
			    $priceValue = trim(str_replace("  ", ' ', $price));
			    //$PictureURL = $all_url;

			    $ConditionDisplayName = trim(str_replace("  ", ' ', $cond_name)); //Condition Name: Used, New etc
			    $ConditionDisplayName = trim(str_replace(array("'"), "''", $cond_name));

			    $brand_name = trim(str_replace("  ", ' ', $brand_name));
			    $brand_name = trim(str_replace(array("'"), "''", $brand_name));

			    $WeightUnit = @$weight;
			    //$WeightUnit = 2.5;
			    
			    $category_name = trim(str_replace("  ", ' ', $category_name));
			    $category_name = trim(str_replace(array("'"), "''", $category_name));

			    	
			   
			    //Empty MPN Check
			    // ----------------------------------------------------
			    
			    $sys_mpn = $mpn;
			    if(!empty(@$sys_mpn)){
			        @$check_mpn = "";
			    }else if(@$sys_mpn == false){
			        $check_mpn = "<strong>MPN:</strong>  $mpn";
			    }
			    if(empty(@$mpn) && @$mpn == ""){
			        @$check_mpn = "";
			    }else{
			        $check_mpn = "<strong>MPN:</strong>  $mpn";        
			    }
			    //Empty brand_name Check
			    // ----------------------------------------------------
			    if(empty(@$brand_name) && @$brand_name == ""){
			        @$check_brand_name = "";
			    }else{
			        $check_brand = "<strong> Brand:</strong>  $brand_name";    
			    }
			    //Empty Condition Display Name
			    // -----------------------------------------------------
			    if(empty($ConditionDisplayName) && $ConditionDisplayName == ""){
			        @$check_condition = "";
			    }else{
			        $check_condition = "<strong> Condition:</strong>  $ConditionDisplayName";    
			    }

			    if($param === 'without_qty'){
					$item_qty =   $sku_exist[0]['stock_quantity'];
					$quantity = 0;
					$status = 'update';
				}else{
					$item_qty = $quantity + $sku_exist[0]['stock_quantity'];
					$status = 'REVISE';
				}


				$prod_data = [
					'name'          => $Title,
					'type'          => 'simple',
					'regular_price' => $priceValue,
					'description'   =>  $Description,
					'short_description' => $check_condition,					
					'stock_quantity' => $item_qty,					
					'manage_stock' => true,
					'weight' => $weight,	
					'sku' => strtoupper(trim($generate_sku)),
					'categories' => [
						[
							'id' => $category_id
						]
					],
					
					'dimensions'    => [
						'length' => '12',
						'width' => '10',
						'height' => '10'
					],
					//'images'        =>  $img_array,
					'attributes'  => 
						(!empty($this->createProductAttributes($item_id,$lz_manifest_id,$condition_id))) ? $this->createProductAttributes($item_id,$lz_manifest_id,$condition_id) : '',				

				];
				
				// echo "<pre>";
				// print_r(@$prod_data);
				// echo "</pre>";
				// exit;
				$list_data = $this->woocommerce->post('products/'.$product_id.'', $prod_data );
				$list_data->seed_id = $seed_id;// append seed_id to object
				$list_data->list_qty = $quantity;// append list_qty to object
				$list_data->listed_by = $listed_by;// append listed_by to object
				$list_data->condition_id = $condition_id;// append condition_id to object
				$list_data->lz_manifest_id = $lz_manifest_id;// append lz_manifest_id to object
				$list_data->item_id = $item_id;// append item_id to object
				$get_prod_id= $list_data->id;
				$product_link = $list_data->permalink;
				

				// update woo brand 
				if(!empty($get_prod_id)){
					// /($brand_name,$object_id){/
					// do not create brand in case of revise
					// if(!empty($brand_name)){
					// $updaye_woo_brand = $this->createBrand($brand_name,$get_prod_id);
					// }	    						
					
					$data = $this->m_wc_product->insertK2bayListMt($list_data,$status);
					// echo "<pre>";
					// print_r($data);

					// echo json_encode($data);
					// 	return json_encode($data);
					// 	exit;
					if($data['ckdata'] ===  'true'){
						

						$message = "Item  added On k2bay";
					    $function_url = 'woo/c_wc_product/productReviseK2bay';
						$error = array('Ack'=>'success','message'=>$message,'function_url' =>$function_url,'k2bay_link' => $product_link,'k2bay_list_query' => $data['k2bay_list_query']);
						echo json_encode($error);
						return json_encode($error);
						//return $error;
						
					}else{
						$message = " Inventory not updated in K2bay db";
						$function_url = 'woo/c_wc_product/productReviseK2bay';
					 	$error = array('Ack'=>'Failure','message'=>$message,'function_url' =>$function_url);
						// also remove product from woo db if not insert in oracle db
						echo json_encode($error);
						return json_encode($error);
						//return $error;
					}

				}else{
						$message = " Inventory not updated in woo db and ebay db";
						$function_url = 'woo/c_wc_product/addProduct';
						$error = array('Ack'=>'Failure','message'=>$message,'function_url' =>$function_url);
						echo json_encode($error);
						return json_encode($error);
						//return $error;
				}
							 
				

			}//end if Check Sku in woo db if product exist or not
		}else{		

					if(count($data['seed_qry']) <1){
						$err_mesg = 'Seed Not Avaiable';
					}else if(count($data['list_qty']) <1){
						$err_mesg = 'Qty Not Avaiable to LisT on k2bay';
					}

						$message = " Data incomplete. ". $err_mesg ;

						// $message = " Data incomplete. seed_qry count =".count($data['seed_qry']).
						// " img_qry count =".count($data['img_qry']).
						// " available quantity count =".count($data['list_qty']);
						$function_url = 'woo/c_wc_product/addProduct';
						$error = array('Ack'=>'Failure','message'=>$message,'function_url' =>$function_url);
						echo json_encode($error);
						return json_encode($error);
						//return $error;
					
		}//end if(count($data['seed_qry']) > 0 AND count($data['img_qry']) > 0){


 	}
	public function productRevise(){

var_dump('revise k2bay');
		exit;
		$price = $this->input->post('price');
		$product_id = $this->input->post('k2bay_prod_id');
		$k2bay_prod_name = $this->input->post('k2bay_prod_name');
		$k2bay_prod_regular_price = $this->input->post('k2bay_prod_regular_price');
		$k2bay_prod_stock_quantity = $this->input->post('k2bay_prod_stock_quantity');
		$permalink = $this->input->post('k2bay_prod_permalink');
// echo 'asdsa';
// 		var_dump($price);
// 		var_dump($product_id);
// 		exit;

		
		$data = [];
		$title = '';
		$description = '';
		//$price = '5';
		$ConditionDisplayName = '';
		$cat_id	= '';
		$cat_name = '';		
		$quantity = '';//$stock_quantity + 5;

		if(isset($cat_id) && !empty($cat_id) ){	
			$get_woo_cat = $this->addCategory($cat_id,$cat_name);
			$category = $get_woo_cat['category_id'];
		}else{
			$category = '';//$get_woo_cat['category_id'];
		}


		if($title != '' && $title != NULL){
			$data['name'] = $title;				
			$revise_data['name'] = 'Title Revise ' .'old Title = '.$k2bay_prod_name.' New Title  = '.$title;	
			$revise_data['Item Link'] = $permalink;
		}

		if($description != '' && $description != NULL){
			$data['description'] = $description;	
			$revise_data['description'] = 'Description Revise';
			$revise_data['Item Link'] = $permalink;
		}

		if($price != '' && $price != NULL){
			$data['regular_price'] = $price;			
			$revise_data['Price'] = 'Price  Revise '.'old Price = '.$k2bay_prod_regular_price.' New Price  = '.$price;
			$revise_data['Item Link'] = $permalink;	
		}
		if($quantity != '' && $quantity != NULL){
			$data['stock_quantity'] = $quantity;
			$revise_data['quantity'] = 'Qty Revise '.'old Qty = '.$k2bay_prod_stock_quantity.' New QTY  = '.$quantity;	
			$revise_data['Item Link'] = $permalink;
		}


		if($category != '' && $category != NULL){
			$data['categories'] = 
			[
				[
					'id' => $category, 
				]
			];
			$revise_data['categories'] = 'category Id Revise';	
			$revise_data['Item Link'] = $permalink;
		}

		if($ConditionDisplayName != '' && $ConditionDisplayName != NULL){
			$check_condition = "<strong> Condition:</strong>  $ConditionDisplayName";
			$data['short_description'] = $check_condition;
			$revise_data['Condition'] = 'Condition Revise';
			$revise_data['Item Link'] = $permalink;		
		}

		//print_r($data);
		$result = $this->woocommerce->put('products/'.$product_id.'', $data);
		$result = stdClassObjectToArray($result); 
		if(isset($result) && !empty($result)){
			$response = ['Ack'=>'true', 'message' => 'Produt Updated', 'product_id' => $result['id'],'revise data' => $revise_data];
		}
		echo json_encode($response);
		return json_encode($response);
		
	}

	public function deleteProductK2bay(){
		
		$k2bay_list_id = $this->input->post('product_id') ;
		$remarks = $this->input->post('Remarks');
		$remarks = trim(str_replace("  ", ' ', $remarks));
        $remarks = str_replace(array("`,'"), "", $remarks);
        $remarks = str_replace(array("'"), "''", $remarks);
		
        $user_id = $this->input->post('user_id');

		$get_prod_id = $this->db->query("select hh.PRODUCT_ID from K2BAY_LIST_MT hh where hh.list_id = '$k2bay_list_id'")->result_array();
		if(count($get_prod_id) >0){
			$product_id = $get_prod_id[0]['PRODUCT_ID'];
			$response = [];
		$result = $this->woocommerce->delete('products/'.$product_id.'',['force' => true]);
		$result = stdClassObjectToArray($result);
		if(isset($result) && !empty($result)){
			$this->m_wc_product->endItemWoo($product_id,$remarks,$user_id);
			$response = ['Success' => 'true','Status' => 'Product Deleted','Product_id' => $result['id'] ,'Product_name' => $result['name']];
		}
		// echo '<pre>';
		// print_r($response);
		echo json_encode($response);
        return json_encode($response);    
    
		//return $response;

		}else{
			return 1;

		}
		
		

	}

	public function getOrders(){
		//echo 'ZEESHAN';
		$input = @file_get_contents( 'php://input' ); 
		$json  = json_decode( $input );
		$order_array = stdClassObjectToArray($json); 
		$string = json_encode($order_array)."\n\n";
		$current_year =  date('Y');
		$current_date = date('d-m-Y');
		$path = 'k2bay_orders/'.$current_year.'/';
		if (!file_exists($path)) {
			mkdir($path, 0777, true);
		}
		$file_name = $path.'k2bay_orders_'.$current_date.'.text';	
		file_put_contents($file_name, $string,FILE_APPEND | LOCK_EX);
		
		//  echo '<pre>';
		//  $order_array = $this->woocommerce->get('orders/4845');
		// print_r($order_array);
		$order_array = stdClassObjectToArray($order_array); 
		$create_order = $this->common_model->createOrderInListing($order_array);
		if($create_order){
			$k2bay_order_id =  $order_array['id'];
			$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('asd','$k2bay_order_id')");

			$get_order_qty = $this->db->query("SELECT MAX(OM.ORDER_ID)ORD_ID , SUM(DL.QUANTITY) ORD_QTY,PRODUCT_ID FROM LZ_K2BAY_ORDER_MT OM,LZ_K2BAY_ORDER_DET DL WHERE OM.LZ_K2BAY_ORDER_MT_ID = DL.LZ_K2BAY_ORDER_MT_ID AND OM.ORDER_ID = '$k2bay_order_id' GROUP BY DL.PRODUCT_ID")->result_array();


			if(count($get_order_qty) >0){

				foreach ($get_order_qty as $k2bay_get_order_qty ) {

					$order_qty = $k2bay_get_order_qty['ORD_QTY'];
					$order_prd_Id = $k2bay_get_order_qty['PRODUCT_ID'];
					$get_order_id_k2bay = $k2bay_get_order_qty['ORD_ID'];				

					$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('$get_order_id_k2bay','$order_prd_Id')");

				 	// $result['data'] = $this->db->query("SELECT BC.EBAY_ITEM_ID, BC.BARCODE_NO, MD.ACCOUNT_ID FROM LZ_BARCODE_MT BC, LZ_MERCHANT_BARCODE_DT MD WHERE BC.K2BAY_PRODUCT_ID = '$order_prd_Id'  and bc.K2BAY_ORDER_ID = '$get_order_id_k2bay'  AND BC.BARCODE_NO = MD.BARCODE_NO AND BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 AND BC.EBAY_ITEM_ID IS NOT NULL AND BC.K2BAY_LIST_ID IS NOT NULL AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL AND BC.ORDER_DT_ID IS NULL AND BC.NOT_FOUND_BY IS NULL AND BC.LZ_DEKIT_US_MT_ID_FOR_OUT IS NULL AND BC.RETURNED_ID IS NULL AND BC.DISCARD = 0 AND BC.CONDITION_ID <> -1 AND ROWNUM <= 1 ")->result_array();

				 	$result['data'] = $this->db->query("SELECT BC.EBAY_ITEM_ID,count(BC.EBAY_ITEM_ID) eb_qty,(select max(jj.lz_seller_acct_id) from ebay_list_mt jj where jj.ebay_item_id = BC.EBAY_ITEM_ID)  ACCOUNT_ID FROM LZ_BARCODE_MT BC WHERE /*BC.K2BAY_PRODUCT_ID = '$order_prd_Id'and */ bc.K2BAY_ORDER_ID = '$get_order_id_k2bay' and BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 /*AND BC.EBAY_ITEM_ID IS NOT NULL*/ AND BC.K2BAY_LIST_ID IS NOT NULL AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL AND BC.ORDER_DT_ID IS NULL AND BC.NOT_FOUND_BY IS NULL AND BC.LZ_DEKIT_US_MT_ID_FOR_OUT IS NULL AND BC.RETURNED_ID IS NULL AND BC.DISCARD = 0 AND BC.CONDITION_ID <> -1 group by BC.EBAY_ITEM_ID order by BC.EBAY_ITEM_ID asc ")->result_array();
				 	foreach ($result['data'] as $all_ebay_qty) {

				 		$result['account_name'] =  $result['data'][0]['ACCOUNT_ID'];
				 		$result['order_qty'] 	=  $all_ebay_qty['EB_QTY'];
				 		$result['ebay_item_id'] =  $all_ebay_qty['EBAY_ITEM_ID'];
				 		$result['prod_id'] =  $order_prd_Id;
				 		//$result['order_qty'] 	=  $order_qty;

				 		// order syncing call
						$data_get =  $this->load->view('ebay/finding/k2bay_ebay_order_sync', $result,true);

						$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('after call','k2bay_ebay_order_sync')");

				 		# code...
				 	}
				 	// $result['account_name'] = $result['data'][0]['ACCOUNT_ID'];
				 	// $result['order_qty'] =  $order_qty;			 	
				 	
					
				}
			}else{
				$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('1','can not find order db')");
			}

		}else{

			$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('asd','else')");
		}
	}

	public function getOrdersTest(){
		$create_order = true;


		if($create_order){
			$k2bay_order_id =  '4932';//$order_array['id'];
			$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('asd','$k2bay_order_id')");

			$get_order_qty = $this->db->query("SELECT MAX(OM.ORDER_ID)ORD_ID , SUM(DL.QUANTITY) ORD_QTY,PRODUCT_ID FROM LZ_K2BAY_ORDER_MT OM,LZ_K2BAY_ORDER_DET DL WHERE OM.LZ_K2BAY_ORDER_MT_ID = DL.LZ_K2BAY_ORDER_MT_ID AND OM.ORDER_ID = '$k2bay_order_id' GROUP BY DL.PRODUCT_ID")->result_array();


			if(count($get_order_qty) >0){

				foreach ($get_order_qty as $k2bay_get_order_qty ) {

					$order_qty = $k2bay_get_order_qty['ORD_QTY'];
					$order_prd_Id = $k2bay_get_order_qty['PRODUCT_ID'];
					$get_order_id_k2bay = $k2bay_get_order_qty['ORD_ID'];				

					$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('$get_order_id_k2bay','$order_prd_Id')");

				 	// $result['data'] = $this->db->query("SELECT BC.EBAY_ITEM_ID, BC.BARCODE_NO, MD.ACCOUNT_ID FROM LZ_BARCODE_MT BC, LZ_MERCHANT_BARCODE_DT MD WHERE BC.K2BAY_PRODUCT_ID = '$order_prd_Id'  and bc.K2BAY_ORDER_ID = '$get_order_id_k2bay'  AND BC.BARCODE_NO = MD.BARCODE_NO AND BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 AND BC.EBAY_ITEM_ID IS NOT NULL AND BC.K2BAY_LIST_ID IS NOT NULL AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL AND BC.ORDER_DT_ID IS NULL AND BC.NOT_FOUND_BY IS NULL AND BC.LZ_DEKIT_US_MT_ID_FOR_OUT IS NULL AND BC.RETURNED_ID IS NULL AND BC.DISCARD = 0 AND BC.CONDITION_ID <> -1 AND ROWNUM <= 1 ")->result_array();

				 	$result['data'] = $this->db->query("SELECT BC.EBAY_ITEM_ID,count(BC.EBAY_ITEM_ID) eb_qty,max(el.lz_seller_acct_id) ACCOUNT_ID FROM LZ_BARCODE_MT BC,ebay_list_mt el WHERE /*BC.K2BAY_PRODUCT_ID = '$order_prd_Id'and */ bc.K2BAY_ORDER_ID = '$get_order_id_k2bay'and bc.ebay_item_id = el.EBAY_ITEM_ID and BC.CONDITION_ID IS NOT NULL AND BC.HOLD_STATUS = 0 /*AND BC.EBAY_ITEM_ID IS NOT NULL*/ AND BC.K2BAY_LIST_ID IS NOT NULL AND BC.SALE_RECORD_NO IS NULL AND BC.ITEM_ADJ_DET_ID_FOR_OUT IS NULL AND BC.LZ_PART_ISSUE_MT_ID IS NULL AND BC.LZ_POS_MT_ID IS NULL AND BC.PULLING_ID IS NULL AND BC.ORDER_DT_ID IS NULL AND BC.NOT_FOUND_BY IS NULL AND BC.LZ_DEKIT_US_MT_ID_FOR_OUT IS NULL AND BC.RETURNED_ID IS NULL AND BC.DISCARD = 0 AND BC.CONDITION_ID <> -1 group by BC.EBAY_ITEM_ID order by BC.EBAY_ITEM_ID asc ")->result_array();

				 	foreach ($result['data'] as $all_ebay_qty) {

				 		$result['account_name'] =  $result['data'][0]['ACCOUNT_ID'];
				 		$result['order_qty'] 	=  $all_ebay_qty['EB_QTY'];
				 		$result['ebay_item_id'] =  $all_ebay_qty['EBAY_ITEM_ID'];
				 		$result['prod_id'] =  $order_prd_Id;
				 		//$result['order_qty'] 	=  $order_qty;

				 		// order syncing call
						$data_get =  $this->load->view('ebay/finding/k2bay_ebay_order_sync', $result,true);

						$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('after call','k2bay_ebay_order_sync')");

				 		# code...
				 	}
				 	// $result['account_name'] = $result['data'][0]['ACCOUNT_ID'];
				 	// $result['order_qty'] =  $order_qty;			 	
				 	
					
				}
			}else{
				$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('1','can not find order db')");
			}

		}else{

			$this->db->query("INSERT INTO TEST_K2BAY_ORDER(ORDER_ID,MESSAGE)values('asd','else')");
		}
	}
	
	public function orderUpdateNotify(){
		$input = @file_get_contents( 'php://input' ); 
		$json  = json_decode( $input );
		$order_array = stdClassObjectToArray($json);
		$table = 'LZ_K2BAY_ORDER_MT';
		$update = ['STATUS' =>  $order_array['status']];
		$where = ['ORDER_ID' => $order_array['number']];
		$this->common_model->updateOrderListing($table,$update,$where);
	}

	public function orderStatus(){
		$order_id = $this->uri->segment(4);
		
		$table = 'LZ_K2BAY_ORDER_MT';
		$where = ['ORDER_ID' => $order_id];
		echo '<pre>';
		//print_R($this->common_model->getRecordFromListing('*',$table,$where));
		$data = [
					'tracking_provider' => 'ups-global',
					'tracking_number' => 'zxcvbndasdm',
					'date_shipped' => '1600646400',
					'tracking_url' => '',
		];

		$trackingDataBatch = array();
		foreach($data as $key => $value){
			$trackingPushBatch = array(
				'meta_id' => '',
				'post_id' => $order_id,
				'meta_key' => $key,
				'meta_value' =>  $value
			);
			
			array_push($trackingDataBatch, $trackingPushBatch);
		}
		
		$this->common_model->insert_batch('wp_postmeta',$trackingDataBatch);
		//$res = $this->woocommerce->put('products/'.$order_id, $data);
		//print_r($res);
		$order_array = $this->woocommerce->get('orders/'.$order_id);
		print_r($order_array);
	}	

	public function getTermPostId($term_id){
        $table = 'wp_postmeta';
        $where = [
            'meta_key' => '_menu_item_object_id',
            'meta_value' => $term_id  
        ];
        $result = $this->common_model->getRecord(['post_id'],$table,$where);
        return $result['post_id'];
    }

	public function getParentCategory(){
		$CATEGORES = $this->common_model->getRecordFromListing(['CATEGORY_ID,CATEGORY_NAME'],'lz_bd_category_tree',['PARENT_CAT_ID' => ''],'CATEGORY_ID','DESC');
		ECHO '<PRE>';
		PRINT_R($CATEGORES); 
		foreach($CATEGORES as $key){
			$cat_name = $key['CATEGORY_NAME'];
			$cat_id = $key['CATEGORY_ID'];
			$result = $this->addCategory($cat_id,$cat_name);
			$array[]['term_id'] = $result['category_id'];
			print_r($result);
			// if($result){
			// 	print_r($result);
			// 	$this->createDynamiCategoryMenu($result['category_id']);
			// }
			
		}
		print_r($array);

		foreach($array as $key){
			$this->createDynamiCategoryMenu($key['term_id']);
		}
		die;
	}
	public function createDynamiCategoryMenu($term_id){

		//$term_id = $term_id;//294;
		$term_taxonomy_id = 201;
		$parent_term_id = 0;
		$menu_item_parent_post_id = '';
		if($parent_term_id > 0 ){
		$menu_item_parent_post_id = getTermPostId($parent_term_id); //wp_posts ID of parent menu item using this function getTermPostId($term_id)
		}
 
		$menu_max_order = $this->common_model->getWooMenuMaxOrder(['tr.term_taxonomy_id' => $term_taxonomy_id]);
		$menu_order = $menu_max_order['max_order'] + 1;
		//$current_date  = date_default_timezone_set('America/Chicago');
		date_default_timezone_set("America/Chicago");
        $current_date = date("Y-m-d H:i:s");

		// echo $current_date;
		// exit;

		// date_default_timezone_set("America/Chicago");
  //       $current_date = date("Y-m-d H:i:s");

		$wp_posts_data = [
		'post_author' => 1,
		'post_Date' => $current_date,
		'post_date_gmt' => $current_date,
		'post_title' => '',
		'post_status' => 'publish',
		'comment_Status' => 'closed',
		'ping_status' => 'closed',
		'post_name' => '',
		'post_parent' => '0',
		'menu_order' => $menu_order,
		'post_type' => 'nav_menu_item',
		'post_content' => '',
		'post_excerpt' => '',
		'to_ping' => '',
		'pinged' => '',
		'post_content_filtered' => '',
		'post_modified' => $current_date,
		'post_modified_gmt' => $current_date
		];

		$post_id = $this->common_model->insert('wp_posts',$wp_posts_data);
		$this->common_model->update('wp_posts',['post_name' => $post_id],['ID' => $post_id]);

		$wp_term_relationship_data = [
		'object_id' => $post_id,
		'term_taxonomy_id' => $term_taxonomy_id,
		'term_order' => 0
		];
		$this->common_model->insert('wp_term_relationships',$wp_term_relationship_data);

		$wp_post_meta = [
		'tamm_menu_item_background' => '',
		'tamm_menu_item_icon' => '',//ion-android-hand
		'tamm_menu_item_icon_color' => '',
		'tamm_menu_item_mega_width' => '', //100% , 50%
		'tamm_menu_item_mega' => '', // 1 = for mega menu
		'_menu_item_orphaned' => '',
		'_menu_item_url' => '',
		'_menu_item_classes' => serialize(array(0 => '')),
		'_menu_item_xfn' => '',
		'_menu_item_target' => '',
		'_menu_item_object' => 'product_cat', // taxonomy name
		'_menu_item_object_id' => $term_id, // term id
		'_menu_item_menu_item_parent' => $menu_item_parent_post_id, //wp_posts ID of parent menu item
		'_menu_item_type' => 'taxonomy',

		];

		$postMetaDataBatch = array();
		foreach($wp_post_meta as $key => $value){
		$postMetaPushBatch = array(
		'post_id' => $post_id,
		'meta_key' => $key,
		'meta_value' => $value
		);

		array_push($postMetaDataBatch, $postMetaPushBatch);
		}
		// echo '<pre>';
		// print_r($postMetaDataBatch);die;
		$this->common_model->insertBatch('wp_postmeta',$postMetaDataBatch);
		}
}


?>



