<?php
use Automattic\WooCommerce\Client;
use Automattic\WooCommerce\HttpClient\HttpClient;
defined('BASEPATH') OR exit('No direct script access allowed');

class c_wc_category extends CI_Controller {

	//Define Variables
	public $woocommerce;
	public $wc;
	public $wooDB;

    public function __construct(){

		parent::__construct();
		$this->load->database();
		$this->load->helper('wc_config');
		$this->load->model('woo/common_model');
		$this->wooDB = $this->load->database('woo_db',TRUE);
		$autoloader = dirname( __FILE__ ) . '/vendor/autoload.php';
		if ( is_readable( $autoloader ) ) {
			require_once $autoloader;
		}

		//Helper variable
		$this->wc = wc_config();
		//var_dump($wc['wc_url'] ,$wc['wc_ck'] ,$wc['wc_cs'] ,$wc['wp_api'] ,$wc['wc_version']);exit;
		
		//Make object of woocomerce client
		$this->woocommerce = new Client(
			$this->wc['wc_url'],
			$this->wc['wc_ck'],
			$this->wc['wc_cs'],
			[
				'wp_api'  => $this->wc['wp_api'],
				'version' => $this->wc['wc_version'],
			]
		);
	}
	public function addCategory()
	{
		$parent = 0;
		$cat_id	= '177659';
		$cat_name = 'Ink Cartridges';
		$json_response = [];
		$tree = $this->common_model->catTree($cat_id);
		$tree = stdClassObjectToArray($tree);
		$tree = $tree[0]['CAT_TRE'];
		if($tree){
		//echo $tree.'<br>';
		$tree_array = explode("\\",$tree);
		//echo '<pre>';
		//print_r($tree_array);
		$term = $tree_array[sizeof($tree_array) - 1];


		$leaf_category_exits = $this->categoryExistByApi($term);
		$leaf_category_exits = stdClassObjectToArray($leaf_category_exits);
		
		if(isset($leaf_category_exits) && !empty($leaf_category_exits)){
			
			//Leaf category exist 
			$json_response = ['success' => 'true' ,'status' => 'Leaf Exist', 'message' => 'Leaf category exist','Category' => $leaf_category_exits[0]['name'] , 'category_id' => $leaf_category_exits[0]['id']];
			}else{
				for($i = 0; $i < sizeof($tree_array); $i++){
					$category_exist = $this->categoryExistByApi($tree_array[$i],$parent);
					if($category_exist){
						$parent = $category_exist[0]['id'];
						}else{
							$data = [
								'name' => $tree_array[$i],
								'parent' => $parent
								// 'image' => [
								//     'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg'
								// ]
								//'slug' => 'computer-s-networking-ok-kafd'
							];
			
							$categories = $this->woocommerce->post( 'products/categories',$data);
							$category = json_decode(json_encode($categories), true);
							$parent 	=	$category['id'];
							//print_r($categories);
						}
					}
					$json_response = ['success' => 'true' ,'status' => 'Generate tree', 'message' => 'Leaf category created','Category' => $category['name'] , 'category_id' => $category['id']];
				}	
			}
		else{
		$leaf_category_exits = $this->categoryExistByApi($cat_name);
		$leaf_category_exits = stdClassObjectToArray($leaf_category_exits);
		//print_r($leaf_category_exits);die;
			if(empty($leaf_category_exits) || $leaf_category_exits == ''){
				$data = [
					'name' => $cat_name,
					'parent' => $parent
					// 'image' => [
					//     'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg'
					// ]
					//'slug' => 'computer-s-networking-ok-kafd'
				];
				$category = $this->woocommerce->post( 'products/categories',$data);
				$category = stdClassObjectToArray($category);
				if(isset($category) || !empty($category) || $category != ''){
					$json_response = ['success' => 'true' ,'status' => 'Parent category created', 'message' => 'Category created successfully','Category' => $category['name'] , 'category_id' => $category['id']];

				}else{
					$json_response = ['success' => 'false', 'message' => 'Category creation failed or category already exist'];
				}
			}else{
				$json_response = ['success' => 'failed' ,'status' => 'Parent category exist', 'message' => 'Category already exists','Category' => $leaf_category_exits[0]['name'] , 'category_id' => $leaf_category_exits[0]['id']];
			}	
		}

		

		//print_r($categories);
		//$categories = json_decode(json_encode($categories), true);
		//echo "</pre>";

		// foreach($categories as $category){
		// 	if($category['slug'] == 'computers-technologies') {
		// 		echo 'computers-technologies Slug already Exit';
		// 	break;
		// 	}
		// }
		//echo "</pre>";

		echo json_encode($json_response);
		exit(1);
	}

	public function termExist($term,$taxonomy){
		//Sample Terms e.g samsung,clothing
		//Sample Taxonomy e.g product_cat ,product_brand 
		$parent = 0;
		$slugy_fy_term = slugify($term);
		//echo $slugy_fy_term." ".$taxonomy;die;
		$isExist = $this->common_model->termExists($slugy_fy_term, $taxonomy, $parent);
		return $isExist;
	}

	public function categoryExistByApi($term,$parent = false){
		$text = $term;
		$slugy_fy = slugify($text);
		if($parent){
			$data = [
				'slug' => $slugy_fy,
				'parent' => $parent
			];
		}else{
			$data = [
				'slug' => $slugy_fy
			];
		}
		$result = $this->woocommerce->get( 'products/categories',$data);
		if(!empty($result)){
			return $result;
		}else{
			return false;
		}
	}

	public function createBrand(){
		$brand_name = 'zeeshutech';
		$taxonomy = 'product_brand';
		$object_id = '4783'; //Object id with reference of term_taxonomy_id insert in wp_term_relationships
		$brand_description = '';
		$brand_parent = 0;

		//Check brand if already exists
		$brand_exist = $this->termExist($brand_name,$taxonomy);
		//print_r($brand_exist);die;
		if(!$brand_exist){
			$result	= $this->common_model->createTerm($brand_name,slugify($brand_name),$taxonomy,$brand_description,$brand_parent,$object_id);
			if($result){
				return $result;
			}
		}
		else {
			$term_id = $brand_exist[0]['term_id'];
			$term_taxonomy_id = $brand_exist[0]['term_taxonomy_id'];
			$taxonomy_count = $brand_exist[0]['taxonomy_count'];

			//Attach brand with product
			$data = [
				'object_id' => $object_id,
				'term_taxonomy_id' => $brand_exist[0]['term_taxonomy_id']
			];
			$this->common_model->makeTermRelation($data);

			//Update wp_term_taxonomy count
			$taxonomy_count = $taxonomy_count + 1;
			$taxonomy_update = [
				'count' => $taxonomy_count 
			];

			$taxonomy_where = [
				'term_taxonomy_id' => $term_taxonomy_id
			];

			$this->common_model->update('wp_term_taxonomy',$taxonomy_update,$taxonomy_where);

			//Update wp_termmeta meta_value 
			$termmeta_where = [
				'term_id' => $term_id
			];
			$result = $this->common_model->getRecord('meta_value','wp_termmeta',$termmeta_where);
			
			$termmeta_update = [
				'meta_value' => $result['meta_value'] + 1
			];

			
			$this->common_model->update('wp_termmeta',$termmeta_update,$termmeta_where);

			$response = ['Success' => 'true','Status' => 'Brand assign ,update term relationships,taxonomy count updated and update termmeta', 'term_id' => $term_id, 'term_taxonomy_id' => $term_taxonomy_id];
			
			return $response;
		} 
	}

	public function deleteProduct(){
		$product_id = 4941  ;
		$response = [];
		$result = $this->woocommerce->delete('products/'.$product_id.'',['force' => true]);
		$result = stdClassObjectToArray($result);
		if(isset($result) && !empty($result)){
			$this->m_wc_product->endItemWoo($product_id);
			$response = ['Success' => 'true','Status' => 'Product Deleted','Product_id' => $result['id'] ,'Product_name' => $result['name']];
		}
		// echo '<pre>';
		// print_r($response);
		return $response;

	}
}


?>
