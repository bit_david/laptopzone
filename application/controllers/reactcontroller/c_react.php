<?php
defined('BASEPATH') or exit('No direct script access allowed');

header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
/**
 * Listing Controller
 */
// use \DTS\eBaySDK\Constants;
// use \DTS\eBaySDK\Finding\Services;
// use \DTS\eBaySDK\Finding\Types;
// use \DTS\eBaySDK\Finding\Enums;

class c_react extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('reactcontroller/m_react');
        $this->load->model('tolist/m_tolist');
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2048M');

    }

    public function login()
    {

        $data = $this->m_react->login();
        echo json_encode($data);
        return json_encode($data);

    }
    public function getMacro()
    {

        $data = $this->m_tolist->getMacro();
        echo json_encode($data);
        return json_encode($data);

    }
    public function DeleteBarcodeReact()
    {

        $data = $this->m_react->DeleteBarcodeReact();
        echo json_encode($data);
        return json_encode($data);

    }
    public function verify_item()
    {

        $data = $this->m_react->verify_item();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_obj_drop_sugestion()
    {

        $data = $this->m_react->get_obj_drop_sugestion();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_unposted_item()
    {

        $data = $this->m_react->get_unposted_item();
        echo json_encode($data);
        return json_encode($data);
    }

    public function get_verify_item()
    {

        $data = $this->m_react->get_verify_item();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_dropdowns()
    {

        $data = $this->m_react->get_dropdowns();
        echo json_encode($data);
        return json_encode($data);
    }
    public function serch_mpn_sys()
    {

        $data = $this->m_react->serch_mpn_sys();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_upc_title()
    {

        $data = $this->m_react->get_upc_title();
        echo json_encode($data);
        return json_encode($data);
    }
    public function serch_desc_sys()
    {

        $data = $this->m_react->serch_desc_sys();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_avail_cond()
    {

        $data = $this->m_react->get_avail_cond();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_obj_drop()
    {

        $data = $this->m_react->get_obj_drop();
        echo json_encode($data);
        return json_encode($data);
    }

    public function update_seed()
    {

        $data = $this->m_react->update_seed();
        echo json_encode($data);
        return json_encode($data);
    }

    public function get_pictures()
    {

        $data = $this->m_react->get_pictures();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_pictures_from_dir()
    {

        $data = $this->m_react->get_pictures_from_dir();
        echo json_encode($data);
        return json_encode($data);
    }
    public function queryData()
    {

        $data = $this->m_react->queryData();
        echo json_encode($data);
        return json_encode($data);
    }
    public function add_specifics()
    {
        $data = $this->m_react->add_specifics();
        echo json_encode($data);
        return json_encode($data);
    }
    public function add_custom_specifics()
    {
        $data = $this->m_react->add_custom_specifics();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_cond_desc()
    {

        $data = $this->m_react->get_cond_desc();
        echo json_encode($data);
        return json_encode($data);
    }
    public function mer_active_listed()
    {

        $data = $this->m_react->mer_active_listed();
        echo json_encode($data);
        return json_encode($data);
    }
    public function mer_active_not_listed()
    {

        $data = $this->m_react->mer_active_not_listed();
        echo json_encode($data);
        return json_encode($data);
    }

    public function merchant_dashboard()
    {

        $data = $this->m_react->merchant_dashboard();
        echo json_encode($data);
        return json_encode($data);
    }
    public function update_tag_remarks()
    {

        $data = $this->m_react->update_tag_remarks();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_tag_data()
    {

        $data = $this->m_react->get_tag_data();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_tag_remarks_data()
    {

        $data = $this->m_react->get_tag_remarks_data();
        echo json_encode($data);
        return json_encode($data);
    }
    public function save_admin_remarks()
    {

        $data = $this->m_react->save_admin_remarks();
        echo json_encode($data);
        return json_encode($data);
    }
    public function delete_tag_barcode_remarks()
    {

        $data = $this->m_react->delete_tag_barcode_remarks();
        echo json_encode($data);
        return json_encode($data);
    }

    public function attribute_value()
    {
        // if($this->input->post('save_attr')){
        $cat_id = $this->input->post('cat_id');
        $barcode = $this->input->post('bar_code');
        $item_mpn = $this->input->post('item_mpn');
        $item_upc = $this->input->post('item_upc');
        $spec_name = $this->input->post('spec_name');
        //$spec_name = trim(str_replace("  ", ' ', $spec_name));
        $spec_name = trim(str_replace(array("'"), "''", $spec_name));
        //var_dump($spec_name);exit;
        $custom_attribute = ucfirst($this->input->post('custom_attribute'));
        $custom_attribute = trim(str_replace("  ", ' ', $custom_attribute));
        $custom_attribute = trim(str_replace(array("'"), "''", $custom_attribute));

        $data = $this->m_react->attribute_value($cat_id, $barcode, $item_mpn, $item_upc, $spec_name, $custom_attribute);
        echo json_encode($data);
        return json_encode($data);

        // }
    }

// function chekalreadyitem($result){
    //   $account_name =2;

// //$baseurl=site_url();

// //$base_url = 'http://' . $_SERVER['SCRIPT_NAME'] . '/';
    // //var_dump($baseurl);
    //   $asdkas = APPPATH;
    //   //var_dump($asdkas);

//  require APPPATH.'/../vendor/autoload.php';
    //  $config  = require APPPATH.'views\ebay/configuration.php';

//  $service = new Services\FindingService();

//  exit;
    //  // /require APPPATH.'/../configuration.php';
    //  //$config = require APPPATH.'/../configuration.php';
    // // var_dump($baseurl);
    //   //require_once('E:\wamp\www\laptopzone\application\views\ebay/configuration.php');
    //   //require_once('E:\wamp\www\laptopzone\vendor/autoload.php');
    //   //require __DIR__.'/../vendor/autoload.php';
    //   //E:\wamp\www\laptopzone\vendor

//   //E:\wamp\www\laptopzone
    //   var_dump('asdsad');

// }
    /// actual function to list item on new db
    public function list_item_merg_copy()
    {

       // exit;
        // $this->load->model('tolist/m_tolist');

        //$bestOfferCheckbox = @$this->uri->segment(8);
        $seed_id = $this->input->post('seed_id');
        $get_price = $this->input->post('price');

        $bestOfferCheckbox = $this->input->post('bestOfferCheckbox');
        //$this->session->set_userdata('shopifyCheckbox', $shopifyCheckbox); // this session is for shopify checkbox value
        if (!empty($seed_id)) {

            $this->db->query("UPDATE LZ_ITEM_SEED SET EBAY_PRICE = '$get_price' WHERE SEED_ID = '$seed_id'");

            $check_btn = $this->input->post('clicked_btn');
            $list_barcode = $this->input->post('list_barcode');
            $shopifyCheckbox = $this->input->post('shopifyCheckbox');
            $listonK2bay = $this->input->post('listonK2bay');
            $revise_ebay_id = $this->input->post('ebay_id');
            $force_ebay_id = $this->input->post('revise_ebay_id');
            $accountId = $this->input->post('accountId');
            $acct_id = $this->input->post('accountId');
            $forceRevise = $this->input->post('forceRevise');

            $userId = $this->input->post('userId');
            $userName = $this->input->post('userName');
            $addQtyOnly = $this->input->post('addQtyOnly');

            if (!is_numeric($list_barcode)) {
                $list_barcode = '';
                //$account_id = $this->session->userdata('account_type');
                $account_id = $accountId; /// tempray variable assign
            } else {
                /*======================================================
                =            get account_id against barcode            =
                ======================================================*/
                //$account_id = $this->session->userdata('account_type');

                $account_id = $accountId; /// tempray variable assign
                if (!empty($list_barcode)) {
                    $query = $this->db->query("SELECT AC.TOKEN, P.PAYPAL_EMAIL, AC.MERCHANT_ID,AC.ACCT_ID FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LJ_MERHCANT_ACC_DT     AC, LJ_PAYPAL_MT           P WHERE M.MT_ID = D.MT_ID AND D.ACCOUNT_ID = AC.ACCT_ID AND P.ACCOUNT_ID = D.ACCOUNT_ID AND D.BARCODE_NO = '$list_barcode'")->result_array();
                    if (count($query) > 0) {
                        $merchant_id = $query[0]['MERCHANT_ID'];
                        $account_id = $query[0]['ACCT_ID'];
                        /*========================================================
                        =            check if our laptopzone merchant            =
                        ========================================================*/
                        if ((int) $merchant_id === 1) {
                            //var_dump("form else of account name");
                            $account_id = $accountId; //$this->session->userdata('account_type');
                            //$account_id = $this->session->userdata('account_type');
                            //var_dump("form else of account name: ". $get_acc);
                        }

                        /*=====  End of check if our laptopzone merchant  ======*/

                    }

                }
                /*=====  End of get account_id against barcode  ======*/

            } // else is_numeric barcode closing
            if (!is_numeric($revise_ebay_id)) {
                $revise_ebay_id = '';
            }

            $query = $this->db->query("SELECT S.ITEM_ID, S.LZ_MANIFEST_ID, S.DEFAULT_COND, S.CATEGORY_ID,s.f_upc UPC,s.f_mpn MPN,s.F_MANUFACTURE ITEM_MT_MANUFACTURE,s.s_mpn FROM LZ_ITEM_SEED S WHERE S.SEED_ID = $seed_id");
            $result['data'] = $query->result_array();
            $item_id = $result['data'][0]['ITEM_ID'];
            $manifest_id = $result['data'][0]['LZ_MANIFEST_ID'];
            $condition_id = $result['data'][0]['DEFAULT_COND'];
            $category_id = $result['data'][0]['CATEGORY_ID'];
            $upc = $result['data'][0]['UPC'];
            $mpn = $result['data'][0]['MPN'];
            $result['seed_id'] = $seed_id;
            $result['check_btn'] = $check_btn;
            $result['revise_ebay_id'] = $revise_ebay_id;
            $result['list_barcode'] = $list_barcode;
            $result['account_name'] = $account_id; // used in configuration.php

            $result['shopifyCheckbox'] = $shopifyCheckbox;
            $result['listonK2bay'] = $listonK2bay;
            $result['bestOfferCheckbox'] = $bestOfferCheckbox;
            $result['uer_id'] = $userId;
            $result['userName'] = $userName;
            $result['addQtyOnly'] = $addQtyOnly;

            // var_dump($check_btn);
            // exit;
            

            if ($forceRevise == 0) {

                $checkItemOnEbay = false;
                if ($check_btn == "add") {
                    // check item is listed or not
                    $checkItemOnEbay = false;

                }else if($check_btn == "list"){
                    $checkItemOnEbay = true;
                }


                // if item found on ebay for revise or add item
                //if($checkItemOnEbay){
                    //  if($checkItemOnEbay){ 

                if($checkItemOnEbay){

                $data_get =  $this->load->view('ebay/finding/finditemadvance_merg_copy', $result,true);
                //3rd permetr condition
                $check = $this->session->userdata('check_item_id');

                }else{
                    $check = false;
                }

                

                if ($check == true) {

                    $end_res =  json_encode( json_decode(htmlspecialchars_decode($data_get), true));
                    $json_res = json_decode($end_res);
                    $exist_prod_data_arr=[];

                    $j =0;
                    foreach ($json_res->ebay_id as $ebay_item_id) { 

                         $dataa = [
                        'ebayId' => $ebay_item_id,
                        'title' => $json_res->title[$j],
                        'cond_name' => $json_res->condition_Name[$j],
                        'curent_price' => '$'.$json_res->curent_price[$j]
                        ];                           
                        array_push($exist_prod_data_arr, $dataa);                          
                        $j++;
                    }


                    $response = array('Ack'=>'success','status' => 'ItemExist','ebayId'=> $exist_prod_data_arr);
                    echo json_encode($response);
                    return json_encode($response);


                } else if ($check_btn == "list" || $check_btn == "add" ) {  
                    


                    $chek_b = $this->db->query("SELECT * FROM LZ_BARCODE_MT B WHERE B.BARCODE_NO NOT IN (SELECT O.BARCODE_PRV_NO FROM LZ_SPECIAL_LOTS O UNION ALL SELECT D.BARCODE_PRV_NO FROM LZ_DEKIT_US_DT D) AND B.BARCODE_NO = '$list_barcode' ")->result_array();

                    if (count($chek_b) >= 1) {
                        $list_barcode = '';
                    }

                    $forceRevise = 0;
                    $data['seed_data'] = $this->m_react->uplaod_seed($item_id, $manifest_id, $condition_id, $check_btn, $forceRevise);
                    if (empty($list_barcode)) {
                        $data['pic_data'] = $this->m_react->uplaod_seed_pic($item_id, $manifest_id, $condition_id, $seed_id);
                    }
                    $data['specific_data'] = $this->m_react->item_specifics($item_id, $manifest_id, $condition_id);
                    $qty = @$data['seed_data'][0]['QUANTITY'];

                    $data['list_barcode'] = $list_barcode;
                    $data['bestOfferCheckbox'] = $bestOfferCheckbox;
                    $this->session->unset_userdata('ebay_item_id');

                    $get_list_resp = $this->load->view('ebay/trading/04-add-fixed-price-item-merg', $data,true);
                    $data_list=  json_encode( json_decode(htmlspecialchars_decode($get_list_resp), true));
                    $data_list_resp = json_decode($data_list);                  

                    if ($this->session->userdata('ebay_item_id')) {
                        $status = "ADD";
                        $result = $this->m_react->insert_ebay_id($item_id, $manifest_id, $seed_id, $condition_id, $status, $check_btn, $forceRevise, $account_id, $userId);
                        //$list_id = $result['list_id'];
                        //$this->load->view('ebay/trading/getitem');
                        $this->m_react->insert_ebay_url($userId);
                        //List on Shopify Code
                        if ($shopifyCheckbox == 1 and (int) $account_id < 3 and !empty($account_id)) {

                            $get_user_id = $userId; //@$this->session->userdata('user_id');

                            $result['shopify_data'] = $this->m_listItemtoShopify->listOnShopify($seed_id, $item_id, $qty, $get_user_id);
                        }
                        $list_id = $result['list_id'];

                        $list_qry = $this->db->query("SELECT E.EBAY_ITEM_ID, TO_CHAR(E.LIST_DATE, 'YYYY-MM-DD HH24:MI:SS') LIST_DATE, E.LZ_SELLER_ACCT_ID, U.EBAY_URL, E.LIST_QTY, E.LIST_PRICE, TRIM(E.STATUS) STATUS, D.ACCOUNT_NAME FROM EBAY_LIST_MT E, LZ_LISTED_ITEM_URL U , LJ_MERHCANT_ACC_DT D WHERE E.EBAY_ITEM_ID = U.EBAY_ID(+) AND E.LZ_SELLER_ACCT_ID = D.ACCT_ID(+) AND E.LIST_ID ='$list_id'")->result_array();
                        $ebay_id = $list_qry[0]['EBAY_ITEM_ID'];
                        $list_date = $list_qry[0]['LIST_DATE'];
                        //$lz_seller_acct_id = $list_qry[0]['LZ_SELLER_ACCT_ID'];
                        $account_type = $list_qry[0]['ACCOUNT_NAME'];
                        $ebay_url = $list_qry[0]['EBAY_URL'];
                        $list_qty = $list_qry[0]['LIST_QTY'];
                        if (@$check_btn == 'revise') {
                            $list_qty = 0;
                        }
                        $list_price = $list_qry[0]['LIST_PRICE'];
                        $emp_name = $userName; //$this->session->userdata('employee_name');
                        // var_dump($emp_name);
                        // exit;
                        $status = $list_qry[0]['STATUS'];


                        // $this->load->view('tolist_view/v_post_list',$result);
                        $this->session->unset_userdata('ebay_item_id');
                        $this->session->unset_userdata('check_item_id');

                    $response = array('Ack'=>'success','status' => 'ItemListed','ebayId'=> $list_qry,'session_err'=> @$data_list_resp->LongMessage);
                    //$response = array('Ack'=>'success','status' => 'ItemListed','ebayId'=> $ebay_id);
                    echo json_encode($response);
                    return json_encode($response);
                    }else{

                    $response = array('Ack'=>'failure','status' => 'item not listed','session_err'=> @$data_list_resp->LongMessage);
                    //$response = array('Ack'=>'success','status' => 'ItemListed','ebayId'=> $ebay_id);
                    echo json_encode($response);
                    return json_encode($response);

                    }
                } 
                
                else {
                    die("Item not found on eBay.");
                } // end if else

            }
        } else {
            echo "<script>alert('Error! Item not listed');return false;</script>";
        }
    }

    public function list_item_confirm_merg()
    {
        $param = $this->input->post('param');
        $check_btn = $this->input->post('check_btn');
        $list_barcode = $this->input->post('list_barcode');
        $seed_id = $this->input->post('seed_id');
        $price = $this->input->post('price');
        $price = trim(str_replace("  ", '', $price));
        $price = trim(str_replace(array("'"), "''", $price));

        
                //$shopifyCheckbox = $this->input->post('shopifyCheckbox');
        $bestOfferCheckbox = $this->input->post('bestOfferCheckbox');
        //$account_id = $this->session->userdata('account_type');
        $user_id = $this->input->post('user_id');
        $userName = $this->input->post('userName');
        //$addQtyOnly = 1;//$this->input->post('addQtyOnly');
        
       
         //  var_dump($forceRevise,$check_btn,$addQtyOnly);
         // exit;
           
        if ($check_btn === 'revise_item') {
        


        
            
            $query = $this->db->query("SELECT S.ITEM_ID, S.LZ_MANIFEST_ID, S.DEFAULT_COND, S.CATEGORY_ID,I.ITEM_MT_UPC UPC,I.ITEM_MT_MFG_PART_NO MPN,I.ITEM_MT_MANUFACTURE FROM LZ_ITEM_SEED S, ITEMS_MT I WHERE S.SEED_ID = $seed_id AND I.ITEM_ID = S.ITEM_ID");
            $result['data'] = $query->result_array();
            $item_id = $result['data'][0]['ITEM_ID'];
            $manifest_id = $result['data'][0]['LZ_MANIFEST_ID'];
            $condition_id = $result['data'][0]['DEFAULT_COND'];
            $category_id = $result['data'][0]['CATEGORY_ID'];
            $upc = $result['data'][0]['UPC'];
            $mpn = $result['data'][0]['MPN'];
            //3rd permetr condition
            $check = $this->session->userdata('check_item_id');

            // if($check == true)
            // {
            //$ebay_id = $this->session->userdata('ebay_item_id');
            $ebay_id = trim($this->input->post('getreviseEbayId'));
            //$ebay_id = trim($this->input->post('reviseEbayId'));
            // if(!isset($ebay_id)){
            //     echo "asdasd";
            //     $getebay_id = $ebay_id;
            // }else{
            //      echo "123";
            //     $getebay_id = $reviseEbayId;
            // }

            // var_dump($ebay_id);
            // exit;
            $query = $this->db->query("SELECT * FROM (SELECT E.ITEM_ID,E.LZ_SELLER_ACCT_ID FROM EBAY_LIST_MT E WHERE E.EBAY_ITEM_ID = $ebay_id ORDER BY E.LIST_ID DESC) WHERE  ROWNUM = 1");
            $old_item = $query->result_array();
            if ($query->num_rows() > 0) {
                if ($old_item[0]['ITEM_ID'] != $item_id) {
                    $old_item_id = $old_item[0]['ITEM_ID'];
                    $new_item_id = $item_id;
                    $replace_itm = "call lz_repl_item($old_item_id,$new_item_id,$manifest_id,$condition_id)";
                    $replace_itm = $this->db->query($replace_itm);
                }
                //$account_id = $old_item[0]['LZ_SELLER_ACCT_ID'];
            }
            $get_seller_acct = $this->db->query("SELECT E.LZ_SELLER_ACCT_ID FROM EBAY_LIST_MT E WHERE E.EBAY_ITEM_ID = '$ebay_id' AND UPPER(E.STATUS) = 'ADD' AND ROWNUM=1 ")->result_array();
            if (count($get_seller_acct) > 0) {
                $account_id = @$get_seller_acct[0]['LZ_SELLER_ACCT_ID'];
            } else {
                $get_seller = $this->db->query("SELECT E.LZ_SELLER_ACCT_ID FROM EBAY_LIST_MT E WHERE E.EBAY_ITEM_ID = '$ebay_id' AND ROWNUM=1")->result_array();
                $account_id = @$get_seller[0]['LZ_SELLER_ACCT_ID'];
            }

            if($param === 'without_qty'){
                    //$item_qty =   $sku_exist[0]['stock_quantity'];
                    $qty = 0;
                    $addQtyOnly = 0;
                    $forceRevise = 0;
            }else if($param === 'add_qty'){

                    $this->db->query("UPDATE LZ_ITEM_SEED SS set SS.EBAY_PRICE = '$price'  WHERE SS.SEED_ID = '$seed_id' ");

                //add qty
                    $qty = @$data['seed_data'][0]['QUANTITY'];
                    $addQtyOnly = 1;
                    $forceRevise = 1;
            }else if($param === 'add_qty_with_seed'){

                $qty = @$data['seed_data'][0]['QUANTITY'];
                $addQtyOnly = 0;
                $forceRevise = 1;
            }

            // var_dump($param);
            // exit;

           
            $data['seed_data'] = $this->m_react->uplaod_seed($item_id, $manifest_id, $condition_id, $check_btn, $forceRevise);            


            
            //$qty = $data['seed_data'][0]['QUANTITY'];
            $data['specific_data'] = $this->m_react->item_specifics($item_id, $manifest_id, $condition_id);
            $data['ebay_id'] = $ebay_id;
            $data['check_btn'] = $check_btn;
            $data['list_barcode'] = $list_barcode;
            $data['account_name'] = $account_id;
            $data['bestOfferCheckbox'] = $bestOfferCheckbox;
            $data['addQtyOnly'] = $addQtyOnly;
            $data['param'] = $param;
            //$data['account_name'] = $account_name;// used in configuration.php


            $data['forceRevise'] = $forceRevise;
           
            //$this->session->unset_userdata('active_listing');
            //$this->load->view('ebay/trading/Revisefixedpriceitem', $data);
            $get_revise_resp = $this->load->view('ebay/trading/Revisefixedpriceitem', $data,true);
            $data_revise=  json_encode( json_decode(htmlspecialchars_decode($get_revise_resp), true));
            $data_revise_resp = json_decode($data_revise);


            // echo "<pre>";
            // print_r($data_revise_resp);
            // exit;

            



            // var_dump('helooo');
            // exit;
            if (!empty($this->session->userdata('ebay_item_id'))) {

                if($param === 'without_qty'){
                    //$item_qty =   $sku_exist[0]['stock_quantity'];
                    $check_btn = 'revise_item';
                }else{
                    $check_btn = 'list';// $data['seed_data'][0]['QUANTITY'];
                }

                $status = "REVISED";
                $result = $this->m_react->insert_ebay_id($item_id, $manifest_id, $seed_id, $condition_id, $status, $check_btn, $forceRevise, $account_id, $user_id);
                $data['list_id'] = $result['list_id'];

                $list_id = $data['list_id'];

                $list_qry = $this->db->query("SELECT E.EBAY_ITEM_ID, TO_CHAR(E.LIST_DATE, 'YYYY-MM-DD HH24:MI:SS') LIST_DATE, E.LZ_SELLER_ACCT_ID, U.EBAY_URL, E.LIST_QTY, E.LIST_PRICE, TRIM(E.STATUS) STATUS, D.ACCOUNT_NAME FROM EBAY_LIST_MT E, LZ_LISTED_ITEM_URL U , LJ_MERHCANT_ACC_DT D WHERE E.EBAY_ITEM_ID = U.EBAY_ID(+) AND E.LZ_SELLER_ACCT_ID = D.ACCT_ID(+) AND E.LIST_ID = $list_id")->result_array();
                $ebay_id = $list_qry[0]['EBAY_ITEM_ID'];
                $list_date = $list_qry[0]['LIST_DATE'];
                //$lz_seller_acct_id = $list_qry[0]['LZ_SELLER_ACCT_ID'];
                $account_type = $list_qry[0]['ACCOUNT_NAME'];
                $ebay_url = $list_qry[0]['EBAY_URL'];
                $list_qty = $list_qry[0]['LIST_QTY'];
                if (@$check_btn == 'revise') {
                    $list_qty = 0;
                }
                $list_price = $list_qry[0]['LIST_PRICE'];
                $emp_name = $userName; //$this->session->userdata('employee_name');
                // var_dump($emp_name);
                // exit;
                $status = $list_qry[0]['STATUS'];
                $current_qty = $this->session->userdata('current_qty');
                

                $this->session->unset_userdata('ebay_error');
                $this->session->unset_userdata('ebay_item_id');
                $this->session->unset_userdata('check_item_id');


                $response = array('Ack'=>'success','status' => 'ItemListed','ebayId'=> $list_qry,'session_err'=> @$data_revise_resp->LongMessage);
                echo json_encode($response);
                return json_encode($response);

                } else {
                $response = array('Ack'=>'failure','status' => 'Item not Revised','session_err'=> @$data_revise_resp->LongMessage);
                echo json_encode($response);
                return json_encode($response);

            }

               

            // }else{
            //   die('Unable to Revise an item');
            // }

        } else {
            echo "<script>alert('Error! Item not listed');return false;</script>";
        }
    }
    
// function to list the item from  add button
    public function list_item_confirm_merg_add_item()
    {

        // $this->load->model('tolist/m_tolist');

        //$bestOfferCheckbox = @$this->uri->segment(8);
        $seed_id = $this->input->post('seed_id');

        $bestOfferCheckbox = 0;
        //$this->session->set_userdata('shopifyCheckbox', $shopifyCheckbox); // this session is for shopify checkbox value
        if (!empty($seed_id)) {
            $check_btn = $this->input->post('check_btn');
            $list_barcode = $this->input->post('list_barcode');

            $shopifyCheckbox = $this->input->post('shopifyCheckbox');
            $revise_ebay_id = $this->input->post('ebay_id');
            $accountId = $this->input->post('accountId');

            $user_id = $this->input->post('user_id');
            $userName = $this->input->post('userName');

            // $userId = $this->input->post('userId');
            // $userName = $this->input->post('userName');

            if (!is_numeric($list_barcode)) {
                $list_barcode = '';
                //$account_id = $this->session->userdata('account_type');
            } else {
                /*======================================================
                =            get account_id against barcode            =
                ======================================================*/
                //$account_id = $this->session->userdata('account_type');

                if (!empty($list_barcode)) {
                    $query = $this->db->query("SELECT AC.TOKEN, P.PAYPAL_EMAIL, AC.MERCHANT_ID,AC.ACCT_ID FROM LZ_MERCHANT_BARCODE_MT M, LZ_MERCHANT_BARCODE_DT D, LJ_MERHCANT_ACC_DT     AC, LJ_PAYPAL_MT           P WHERE M.MT_ID = D.MT_ID AND D.ACCOUNT_ID = AC.ACCT_ID AND P.ACCOUNT_ID = D.ACCOUNT_ID AND D.BARCODE_NO = '$list_barcode'")->result_array();
                    if (count($query) > 0) {
                        $merchant_id = $query[0]['MERCHANT_ID'];
                        $account_id = $query[0]['ACCT_ID'];
                        /*========================================================
                        =            check if our laptopzone merchant            =
                        ========================================================*/
                        if ((int) $merchant_id === 1) {
                            //var_dump("form else of account name");
                            $account_id = $accountId; //$this->session->userdata('account_type');
                            //$account_id = $this->session->userdata('account_type');
                            //var_dump("form else of account name: ". $get_acc);
                        }

                        /*=====  End of check if our laptopzone merchant  ======*/

                    }
                    $account_id = $accountId; /// tempray variable assign
                }
                /*=====  End of get account_id against barcode  ======*/

            } // else is_numeric barcode closing
            if (!is_numeric($revise_ebay_id)) {
                $revise_ebay_id = '';
            }
            // var_dump($accountId);
            // exit;

            $query = $this->db->query("SELECT S.ITEM_ID, S.LZ_MANIFEST_ID, S.DEFAULT_COND, S.CATEGORY_ID,I.ITEM_MT_UPC UPC,I.ITEM_MT_MFG_PART_NO MPN,I.ITEM_MT_MANUFACTURE FROM LZ_ITEM_SEED S, ITEMS_MT I WHERE S.SEED_ID = $seed_id AND I.ITEM_ID = S.ITEM_ID");
            $result['data'] = $query->result_array();
            $item_id = $result['data'][0]['ITEM_ID'];
            $manifest_id = $result['data'][0]['LZ_MANIFEST_ID'];
            $condition_id = $result['data'][0]['DEFAULT_COND'];
            $category_id = $result['data'][0]['CATEGORY_ID'];
            $upc = $result['data'][0]['UPC'];
            $mpn = $result['data'][0]['MPN'];
            $result['seed_id'] = $seed_id;
            $result['check_btn'] = $check_btn;
            $result['revise_ebay_id'] = $revise_ebay_id;
            $result['list_barcode'] = $list_barcode;
            $result['account_name'] = $account_id; // used in configuration.php

            $result['shopifyCheckbox'] = $shopifyCheckbox;
            $result['bestOfferCheckbox'] = $bestOfferCheckbox;
            $result['uer_id'] = $user_id;
            $result['userName'] = $userName;

            // var_dump($mpn);exit;
            //$this->load->view('ebay/finding/finditembykeyword',$result);
            //     $this->load->view('ebay/finding/finditemadvance_merg_copy',$result);
            // //     var_dump('asasd');
            // // exit;
            //    //3rd permetr condition
            //    $check = $this->session->userdata('check_item_id');

            //    if($check == true)
            //    {

            //      exit;
            //    }else
            if ($check_btn == "add_item") {
                //  $data['account_name'] = $account_id;// used in configuration.php
                // exit;
                // var_dump($account_id);
                // exit;
                $forceRevise = 0;
                $data['seed_data'] = $this->m_react->uplaod_seed($item_id, $manifest_id, $condition_id, $check_btn, $forceRevise);

                // adde by adil asad  6-jun-2020
                $chk_itm_sourc = $this->db->query("select * from (select l.barcode_prv_no from lz_special_lots l where l.barcode_prv_no ='$list_barcode' union all select op.barcode_prv_no from lz_dekit_us_dt op where op.barcode_prv_no ='$list_barcode')  where rownum<=1")->result_array();
                $chk_itm_sourc = @$chk_itm_sourc[0]['BARCODE_PRV_NO'];

                if (!empty($chk_itm_sourc)) {
                    $list_barcode = $list_barcode;
                } else {
                    $list_barcode = '';
                }
                // adde by adil asad  6-jun-2020

                if (empty($list_barcode)) {
                    $data['pic_data'] = $this->m_react->uplaod_seed_pic($item_id, $manifest_id, $condition_id, $seed_id);
                }

                $data['specific_data'] = $this->m_react->item_specifics($item_id, $manifest_id, $condition_id);
                $qty = @$data['seed_data'][0]['QUANTITY'];

                $data['list_barcode'] = $list_barcode;
                $data['bestOfferCheckbox'] = $bestOfferCheckbox;
                $data['account_name'] = $account_id;
                $this->session->unset_userdata('ebay_item_id');

                if (@$account_id == 'dfwonline') {
                    $account_id = 2;
                    //$seller_name = 'dfwonline';
                } elseif (@$account_id == 'techbargains2015') {
                    $account_id = 1;
                    //$account_id = 'techbargains2015';
                }

                // $status = "ADD";
                // $result = $this->m_react->insert_ebay_id($item_id,$manifest_id,$seed_id,$condition_id,$status,$check_btn,$forceRevise,$account_id, $user_id);
                //     $data['list_id'] = $result['list_id'];
                //     $data['affected_barcode'] = $result['affected_barcode'];

                /// api call for listing item
                $this->load->view('ebay/trading/04-add-fixed-price-item-merg', $data);
                if ($this->session->userdata('ebay_item_id')) {
                    $status = "ADD";
                    $result = $this->m_react->insert_ebay_id($item_id, $manifest_id, $seed_id, $condition_id, $status, $check_btn, $forceRevise, $account_id, $user_id);
                    // $data['list_id'] = $result['list_id'];
                    // $data['affected_barcode'] = $result['affected_barcode'];

                    $list_id = $result['list_id'];

                    // $status = "ADD";
                    // $result['list_id'] = $this->m_react->insert_ebay_id($item_id, $manifest_id, $seed_id, $condition_id, $status, $check_btn, $forceRevise, $account_id, $user_id);
                    // $list_id = $result['list_id'];
                    //$this->load->view('ebay/trading/getitem');
                    $this->m_react->insert_ebay_url($user_id);
                    //List on Shopify Code
                    if ($shopifyCheckbox == 1 and (int) $account_id < 3 and !empty($account_id)) {

                        $get_user_id = $userId;

                        $result['shopify_data'] = $this->m_listItemtoShopify->listOnShopify($seed_id, $item_id, $qty, $get_user_id);
                    }

                    $list_qry = $this->db->query("SELECT E.EBAY_ITEM_ID, TO_CHAR(E.LIST_DATE, 'YYYY-MM-DD HH24:MI:SS') LIST_DATE, E.LZ_SELLER_ACCT_ID, U.EBAY_URL, E.LIST_QTY, E.LIST_PRICE, TRIM(E.STATUS) STATUS, D.ACCOUNT_NAME FROM EBAY_LIST_MT E, LZ_LISTED_ITEM_URL U , LJ_MERHCANT_ACC_DT D WHERE E.EBAY_ITEM_ID = U.EBAY_ID(+) AND E.LZ_SELLER_ACCT_ID = D.ACCT_ID(+) AND E.LIST_ID = $list_id")->result_array();
                    $ebay_id = $list_qry[0]['EBAY_ITEM_ID'];
                    $list_date = $list_qry[0]['LIST_DATE'];
                    //$lz_seller_acct_id = $list_qry[0]['LZ_SELLER_ACCT_ID'];
                    $account_type = $list_qry[0]['ACCOUNT_NAME'];
                    $ebay_url = $list_qry[0]['EBAY_URL'];
                    $list_qty = $list_qry[0]['LIST_QTY'];
                    if (@$check_btn == 'revise') {
                        $list_qty = 0;
                    }
                    $list_price = $list_qry[0]['LIST_PRICE'];
                    $emp_name = $userName; //$this->session->userdata('employee_name');
                    // var_dump($emp_name);
                    // exit;
                    $status = $list_qry[0]['STATUS'];
                    $current_qty = $this->session->userdata('current_qty');
                    echo "<div class='col-sm-12'>

                <div id='errorMsgs' >
                    The item is listed on eBay With the Following Details:<br>
                    <ul>

                        <li>Ebay Id: <a href='https://www.ebay.com/itm/" . $ebay_id . "' target='_blank' >" . $ebay_id . "</a></li>
                        <li>Ebay Account: " . $account_type . "</li>
                        <li>Listed By: " . $emp_name . " </li>
                        <li>Timestamp: " . $list_date . " </li>
                        <li>List Qty: " . $list_qty . " </li>
                        <li>List Price: $ " . $list_price . " </li>
                    </ul>
                </div>

            </div>";

                    // $this->load->view('tolist_view/v_post_list',$result);
                    $this->session->unset_userdata('ebay_item_id');
                    $this->session->unset_userdata('check_item_id');
                }

            } else {
                echo "<script>alert('Error! Item not listed');return false;</script>";
            }

        }

    }

    public function reviseItemPrice()
    {
        $ebay_id = trim($this->input->post('ebay_id'));
        //$price = trim($this->input->post('revise_price'));

        $price = trim(str_replace('$', '', $this->input->post('revise_price')));
        $user_id = trim($this->input->post('user_id'));

        // var_dump($ebay_id,$price,$user_id);
        // exit;
        //$seed_id = trim($this->input->post('seed_id'));
        $query = $this->db->query("SELECT * FROM (SELECT E.LZ_SELLER_ACCT_ID,S.EBAY_LOCAL FROM EBAY_LIST_MT E, LZ_ITEM_SEED S WHERE E.EBAY_ITEM_ID = '$ebay_id'AND S.SEED_ID = E.SEED_ID ORDER BY E.LIST_ID DESC) WHERE  ROWNUM = 1");
        $old_item = $query->result_array();
        if ($query->num_rows() > 0) {
            $account_id = $old_item[0]['LZ_SELLER_ACCT_ID'];
            $site_id = $old_item[0]['EBAY_LOCAL'];
        } else {
            echo "alert('Some error Occur please Contact Your Administartor')";
            exit;
        }
        // $forceRevise = 0;
        // $data['seed_data'] = $this->m_tolist->uplaod_seed($item_id,$manifest_id,$condition_id,$check_btn,$forceRevise);
        // $data['specific_data'] = $this->m_tolist->item_specifics($item_id,$manifest_id,$condition_id);
        $data['ebay_id'] = $ebay_id;
        $data['site_id'] = $site_id;
        $data['price'] = $price;
        $data['account_name'] = $account_id; // used in configuration.php
        $data['addQty'] = 0;
        //$data['forceRevise'] = $forceRevise;
        //$this->session->unset_userdata('active_listing');
        $this->load->view('ebay/trading/reviseItemPrice', $data);
        if (!empty($this->session->userdata('ebay_item_id'))) {
            $status = "REVISED";
            $result['list_id'] = $this->m_tolist->updateReviseStatus($ebay_id, $price, $user_id);
            //$result['check_btn'] = $check_btn;
            //$this->load->view('ebay/trading/getitem');
            //$this->m_tolist->insert_ebay_url();
            //$this->load->view('tolist_view/v_post_list',$result);
            $this->session->unset_userdata('ebay_error');
            $this->session->unset_userdata('ebay_item_id');
            $this->session->unset_userdata('check_item_id');
            //$this->session->unset_userdata('ebay_item_url');
            //break;
            $data = true;
            echo json_encode($data);
            return json_encode($data);

        } else {
            die('Unable to Revise an item');
            $data = false;
            echo json_encode($data);
            return json_encode($data);
            // return false;
        }
    }

    public function pic_log()
    {

        $data = $this->m_react->pic_log();
        echo json_encode($data);
        return json_encode($data);
    }
    public function merch_servic_invoice()
    {

        $data = $this->m_react->merch_servic_invoice();
        echo json_encode($data);
        return json_encode($data);
    }

    public function load_merchant()
    {

        $data = $this->m_react->load_merchant();
        echo json_encode($data);
        return json_encode($data);
    }
    public function merch_servic_invoice_barcode()
    {

        $data = $this->m_react->merch_servic_invoice_barcode();
        echo json_encode($data);
        return json_encode($data);
    }
    public function generate_invoic()
    {

        $data = $this->m_react->generate_invoic();
        echo json_encode($data);
        return json_encode($data);
    }
    public function merch_generate_invoice()
    {

        $data = $this->m_react->merch_generate_invoice();
        echo json_encode($data);
        return json_encode($data);
    }
    public function merch_lot_dash()
    {

        $data = $this->m_react->merch_lot_dash();
        echo json_encode($data);
        return json_encode($data);
    }

    public function merch_lot_detail_view()
    {

        $data = $this->m_react->merch_lot_detail_view();
        echo json_encode($data);
        return json_encode($data);
    }
    public function merch_lot_load_detail()
    {

        $data = $this->m_react->merch_lot_load_detail();
        echo json_encode($data);
        return json_encode($data);
    }
    public function merchant_sold_items()
    {

        $data = $this->m_react->merchant_sold_items();
        echo json_encode($data);
        return json_encode($data);
    }
    public function merch_inproces_items()
    {

        $data = $this->m_react->merch_inproces_items();
        echo json_encode($data);
        return json_encode($data);
    }

    public function load_identification_data()
    {

        $data = $this->m_react->load_identification_data();
        echo json_encode($data);
        return json_encode($data);
    }
    public function itemDiscard()
    {

        $data = $this->m_react->itemDiscard();
        echo json_encode($data);
        return json_encode($data);
    }
    /// get lz webiste functions

    public function ljw_Brands()
    {

        $data = $this->m_react->ljw_Brands();
        echo json_encode($data);
        return json_encode($data);
    }
    public function ljw_Series()
    {

        $data = $this->m_react->ljw_Series();
        echo json_encode($data);
        return json_encode($data);
    }

    public function ljw_Model()
    {

        $data = $this->m_react->ljw_Model();
        echo json_encode($data);
        return json_encode($data);
    }
    public function ljw_Issues()
    {

        $data = $this->m_react->ljw_Issues();
        echo json_encode($data);
        return json_encode($data);
    }
    public function ljw_Carrier()
    {

        $data = $this->m_react->ljw_Carrier();
        echo json_encode($data);
        return json_encode($data);
    }
    public function ljw_Storage()
    {

        $data = $this->m_react->ljw_Storage();
        echo json_encode($data);
        return json_encode($data);
    }
    public function ljw_getAnOffer()
    {

        $data = $this->m_react->ljw_getAnOffer();
        echo json_encode($data);
        return json_encode($data);
    }
    public function ljw_getAnOfferAdmin()
    {

        $data = $this->m_react->ljw_getAnOfferAdmin();
        echo json_encode($data);
        return json_encode($data);
    }
    public function ljw_getAnOfferMobile()
    {

        $data = $this->m_react->ljw_getAnOfferMobile();
        echo json_encode($data);
        return json_encode($data);
    }

    public function get_repairs()
    {

        $data = $this->m_react->get_repairs();
        echo json_encode($data);
        return json_encode($data);
    }
    public function ljw_saveBuySellTech()
    {

        $data = $this->m_react->ljw_saveBuySellTech();
        echo json_encode($data);
        return json_encode($data);
    }

    public function ljw_rep_sell_data()
    {

        $data = $this->m_react->ljw_rep_sell_data();
        echo json_encode($data);
        return json_encode($data);
    }

    public function ljw_rep_sell_counts()
    {

        $data = $this->m_react->ljw_rep_sell_counts();
        echo json_encode($data);
        return json_encode($data);
    }
    public function ljw_SaveRequest()
    {
        $this->load->library('email');
        $emailNumb = $this->input->post('emailNumb');
        $phoneNumb = $this->input->post('phoneNumb');
        $LastName = $this->input->post('LastName');
        $yourName = $this->input->post('yourName');
        if ($this->input->post('emailNumb')) {

            $data = $this->m_react->ljw_SaveRequest();

            if ($data['exist'] == true) {
                // echo json_encode(array(
                //   "create" => true,
                //   "message" => " Mail successfully send",
                //   "data" =>$data['get_req_id']
                // ));

                $config['mailtype'] = 'html';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = true;
                $this->email->initialize($config);
                $this->email->from('info@laptopzone.us');
                $this->email->to($emailNumb);
                $this->email->cc('info@laptopzone.us');
                $this->email->subject('Team LaptopZone |  Repair #  ' . $data['get_req_id'] . '');
                $this->email->message('<h1>Welcome to our website and Thank You for connecting with LaptopZone. </h1><br>
          <h3>Dear ' . $yourName . ',</h3></n><p>LaptopZone is most reliable and authentic electronic sellers, stay connected for easy and quick business. <strong style="color :red"> Your REPAIR # is  ' . $data['get_req_id'] . '</strong>. Thank you for placing an order with us. For more queries and questions please contact us.<br> Click on the link <a href="http://laptopzone.us/pullRequests/' . $data['get_req_id'] . '" >http://laptopzone.us/pullRequests/' . $data['get_req_id'] . '</a> to follow your request </p></n><h3>Phone # (214) 427-4496</h3></n><h3>Email : info@laptopzone.us</h3></b><h3>Address:</h3></n><p>2720 Royal Ln #180</p></n><p>Dallas, TX 75229</p></n><p>United States</p></n><h3>Best Regard</h3><p>LaptopZone</p>'
                );
                if ($this->email->send()) {
                    echo json_encode(array(
                        "create" => true,
                        "message" => " Mail successfully send",
                        "data" => $data['get_req_id'],
                    ));

                } else {
                    print_r($this->email->print_debugger());
                    echo json_encode(array(
                        "create" => false,
                        "message" => "Failed to send email",
                        "data" => $data['get_req_id'],
                    ));
                }
            } else {
                echo json_encode(array(
                    "create" => false,
                    "message" => "Request Not Generated",
                ));
            }
        }
    }
    public function ljw_SaveRequestMobile()
    {
        $this->load->library('email');
        $emailNumb = $this->input->post('emailNumb');
        $phoneNumb = $this->input->post('phoneNumb');
        $LastName = $this->input->post('LastName');
        $yourName = $this->input->post('yourName');
        if ($this->input->post('emailNumb')) {

            $data = $this->m_react->ljw_SaveRequestMobile();

            if ($data['exist'] == true) {
                $config['mailtype'] = 'html';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = true;
                $this->email->initialize($config);
                $this->email->from('info@laptopzone.us');
                $this->email->to($emailNumb);
                $this->email->cc('info@laptopzone.us');
                $this->email->subject('Team LaptopZone |  Repair #  ' . $data['get_req_id'] . '');
                $this->email->message('<h1>Welcome to our website and Thank You for connecting with LaptopZone. </h1><br>
          <h3>Dear ' . $yourName . ',</h3></n><p>LaptopZone is most reliable and authentic electronic sellers, stay connected for easy and quick business. <strong style="color :red"> Your REPAIR # is  ' . $data['get_req_id'] . '</strong>. Thank you for placing an order with us. For more queries and questions please contact us.<br> Click on the link <a href="http://laptopzone.us/pullRequests/' . $data['get_req_id'] . '" >http://laptopzone.us/pullRequests/' . $data['get_req_id'] . '</a> to follow your request </p></n><h3>Phone # (214) 427-4496</h3></n><h3>Email : info@laptopzone.us</h3></b><h3>Address:</h3></n><p>2720 Royal Ln #180</p></n><p>Dallas, TX 75229</p></n><p>United States</p></n><h3>Best Regard</h3><p>LaptopZone</p>'
                );
                if ($this->email->send()) {
                    echo json_encode(array(
                        "create" => true,
                        "message" => " Mail successfully send",
                        "data" => $data['get_req_id'],
                    ));

                } else {
                    // print_r($this->email->print_debugger());
                    echo json_encode(array(
                        "create" => false,
                        "message" => "Failed to send email",
                        "data" => $data['get_req_id'],
                    ));
                }
            } else {
                echo json_encode(array(
                    "create" => false,
                    "message" => "Request Not Generated",
                ));
            }
        }
    }
    public function ljw_SaveRequestMobilePics()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('get_req_id', 'Request Id Requried', 'required|trim');
        if ($this->form_validation->run() == false) {
            header('HTTP/1.1 422 Unprocessable Entity', true, 422);
            echo json_encode(array("send" => false, "message" => validation_errors()));
            return json_encode(array("send" => false, "message" => validation_errors()));
        } else {
            $data = $this->m_react->ljw_SaveRequestMobilePics();
            if ($data['exist'] == true) {

                echo json_encode(array(
                    "create" => true,
                    "message" => $data['message'],
                    "data" => $data['get_req_id'],
                ));

            } else {
                header('HTTP/1.1 400 Unprocessable Entity', true, 400);
                echo json_encode(array(
                    "create" => false,
                    "message" => "You did not select any image",
                    "data" => $data['get_req_id'],
                ));
            }
        }
    }
    public function ljw_saveBuySell()
    {
        $this->load->library('email');
        $payment_type = $this->input->post('payment_mode');
        if ($payment_type == "paypal") {
            $email = $this->input->post('paypalEmail');
        } else {
            $email = $this->input->post('email_address');
        }
        if ($email) {
            $data = $this->m_react->ljw_saveBuySell();
            if ($data['exist'] == true) {
                $config['mailtype'] = 'html';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = true;
                $this->email->initialize($config);
                $this->email->from('info@laptopzone.us');
                $this->email->to($email);
                $this->email->cc('info@laptopzone.us');
                $this->email->subject('Team LaptopZone |  Sell #  ' . $data['trade_pk_id'] . '');
                $this->email->message('<h1>Welcome to our website and Thank You for connecting with LaptopZone. </h1><br>
              <h3>Dear customer,</h3></n><p>LaptopZone is most reliable and authentic electronic sellers, stay connected for easy and quick business. <strong style="color :red"> Your Tracking # is  ' . $data['trade_pk_id'] . '</strong>. Thank you for placing an order with us. For more queries and questions please contact us.<br> </p></n><h3>Phone # (214) 427-4496</h3></n><h3>Email : info@laptopzone.us</h3></b><h3>Address:</h3></n><p>2720 Royal Ln #180</p></n><p>Dallas, TX 75229</p></n><p>United States</p></n><h3>Best Regard</h3><p>LaptopZone</p>'
                );
                if ($this->email->send()) {
                    echo json_encode(array(
                        "create" => true,
                        "message" => "Create Successfully",
                        "data" => $data['trade_pk_id'],
                    ));
                    return json_encode(array(
                        "create" => true,
                        "data" => $data['trade_pk_id'],
                        "message" => "Create Successfully",
                    ));

                } else {
                    echo json_encode(array(
                        "create" => false,
                        "message" => "Failed to send email",
                    ));
                    return json_encode(array(
                        "create" => false,
                        "message" => "Failed to send email",
                    ));
                }
            } else {
                echo json_encode(array(
                    "create" => false,
                    "message" => "Request Not Generated",
                ));
                return json_encode(array(
                    "create" => false,
                    "message" => "Request Not Generated",
                ));
            }
        } else {
            echo json_encode(array(
                "create" => false,
                "message" => "Email address required",
            ));
            return json_encode(array(
                "create" => false,
                "message" => "Email address required",
            ));
        }
    }

    //   public function resetpassord()
    // {
    //   $this->load->library('encrypt');
    //   $this->load->library('email');
    //   $this->load->library('form_validation');

    //   if ($this->input->post('username')) {

    //     $totaken = date("d/m/Y h:i:s");
    //     $username = $this->input->post('username');
    //     $tempuser = $this->encrypt->encode($username);
    //     $tempToken = $this->encrypt->encode($totaken);
    //     // print_r($tempuser);
    //     // echo "<br>";
    //     // print_r($this->encrypt->decode($tempuser));
    //     // exit();
    //     $data = $this->m_sigInUp->Resetpassord();
    //     // var_dump($data); exit;
    //     if ($data == true) {

    //       $config['mailtype'] = 'html';
    //       $config['charset']  = 'iso-8859-1';
    //       $config['wordwrap'] = TRUE;
    //       $this->email->initialize($config);
    //       $this->email->from('tayyabchohan7@gmail.com');
    //       $this->email->to($username);

    //       $this->email->subject('Reset Password');
    //       $this->email->message("<a href='http://localhost:3000/resetPassword/~" . $tempuser . "~/~" . $tempToken . "~' target='_blank' >Click here to Reset Password</a>");

    //       if ($this->email->send()) {
    //         echo json_encode(array(
    //           "create" => true,
    //           "message" => " Mail successfully send"
    //         ));
    //         return json_encode(array(
    //           "create" => true,
    //           "message" => " Mail successfully send"
    //         ));
    //       } else {
    //         print_r($this->email->print_debugger());
    //         echo json_encode(array(
    //           "create" => false,
    //           "message" => "Failed to send email"
    //         ));
    //         return json_encode(array(
    //           "create" => false,
    //           "message" => "Failed to send email"
    //         ));
    //       }
    //     } else {
    //       echo json_encode(array(
    //         "create" => false,
    //         "message" => "User Name Not Exist"
    //       ));
    //       // return array('status' => false, 'message' => 'User Name Not Exist');
    //     }
    //   }
    // }

    public function save_image_url()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'id', 'required|trim');
        $this->form_validation->set_rules('type', 'type', 'required|trim');
        if ($this->form_validation->run() == false) {
            header('HTTP/1.1 422 Unprocessable Entity', true, 422);
            echo json_encode(array("update" => false, "message" => validation_errors()));
            return json_encode(array("update" => false, "message" => validation_errors()));
        } else {
            $data = $this->m_react->save_image_url();
            if ($data['exist'] == true) {
                echo json_encode(array(
                    "update" => true,
                    "message" => $data['message'],
                    "data" => $data['id'],
                ));
            } else {
                header('HTTP/1.1 400 Unprocessable Entity', true, 400);
                echo json_encode(array(
                    "update" => false,
                    "message" => $data['message'],
                    "data" => $data['id'],
                ));
            }
        }
    }
    public function saveOptionData()
    {

        $data = $this->m_react->saveOptionData();
        echo json_encode($data);
        return json_encode($data);

    }

    // for usman android app
    public function lz_recyle_form()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('full_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('phone', 'Phone', 'required|trim');
        $this->form_validation->set_rules('remarks', 'Remakrs', 'required|trim');

        if ($this->form_validation->run() == false) {
            echo json_encode(array("insert" => false, "message" => validation_errors()));
            return json_encode(array("insert" => false, "message" => validation_errors()));
        } else {

            if ($this->m_react->lz_recyle_form()) {
                $data = $this->m_react->lz_recyle_form();
                echo json_encode(array("insert" => true, "data" => $data['get_req_id'], "message" => "Thanks for contact us"));
                return json_encode(array("insert" => true, "data" => $data['get_req_id'], "message" => 'Thanks for contact us'));
            } else {
                $data = $this->m_react->lz_recyle_form();
                echo json_encode(array("insert" => false, "data" => $data['get_req_id'], "message" => "SOmething went wrong"));
                return json_encode(array("insert" => false, "data" => $data['get_req_id'], "message" => "SOmething went wrong"));
            }

        }
    }

    public function get_rep_rec_detail()
    {
        $data = $this->m_react->get_rep_rec_detail();
        echo json_encode($data);
        return json_encode($data);

    }
    public function get_rep_rec_detail_mobile()
    {
        $data = $this->m_react->get_rep_rec_detail_mobile();
        echo json_encode($data);
        return json_encode($data);

    }

    public function update_carrier_track()
    {
        $data = $this->m_react->update_carrier_track();
        echo json_encode($data);
        return json_encode($data);

    }
    public function lzw_hot_items()
    {
        $data = $this->m_react->lzw_hot_items();
        echo json_encode($data);
        return json_encode($data);
    }

    public function lzw_cancel_req()
    {

        $data = $this->m_react->lzw_cancel_req();
        echo json_encode($data);
        return json_encode($data);

    }
    public function serch_invoice_monthly()
    {

        $data = $this->m_react->serch_invoice_monthly();
        echo json_encode($data);
        return json_encode($data);

    }

    public function gene_month_invoice()
    {

        $data = $this->m_react->gene_month_invoice();
        echo json_encode($data);
        return json_encode($data);

    }

    public function serach_invoice_monthly_detail()
    {

        $data = $this->m_react->serach_invoice_monthly_detail();
        echo json_encode($data);
        return json_encode($data);

    }

    public function serach_Inv_list_charg_detail()
    {

        $data = $this->m_react->serach_Inv_list_charg_detail();
        echo json_encode($data);
        return json_encode($data);

    }

    public function search_Inv_pic_charg_detail()
    {

        $data = $this->m_react->search_Inv_pic_charg_detail();
        echo json_encode($data);
        return json_encode($data);

    }
    public function pic_invoice_barcode_details()
    {

        $data = $this->m_react->pic_invoice_barcode_details();
        echo json_encode($data);
        return json_encode($data);

    }
    public function list_invoice_barcode_details()
    {

        $data = $this->m_react->list_invoice_barcode_details();
        echo json_encode($data);
        return json_encode($data);

    }

    public function bin_wise_storage_inv_detail()
    {

        $data = $this->m_react->bin_wise_storage_inv_detail();
        echo json_encode($data);
        return json_encode($data);

    }

    public function get_monthly_storage_charge()
    {
        $data = $this->m_react->Get_Monthly_Storage_Charge();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_pl_report()
    {
        $data = $this->m_react->get_pl_report();
        echo json_encode($data);
        return json_encode($data);
    }

    public function save_log_data()
    {

        $endTime = microtime(true);
        $filename = 'rfid_log_' . date('y-m-d') . '.log';
        $dataToLog = 'Time: ' . gmdate("F j, Y, g:i a") . "\r\n";
        $dataToLog .= 'Input: ' . json_encode($this->input->post(null, false)) . "\r\n";
        $dataToLog .= "---------------------------------------------------------------\r\n";

        $specific_path = "D:/wamp/www/item_pictures/rfid_log/" . $filename;

        if (!write_file($specific_path, $dataToLog, 'a')) {
            return 'Unable to write the file';
        } else {
            echo json_encode($dataToLog);
            return json_encode($dataToLog);
        }

    }

    public function merch_invoice_status_detail()
    {

        $data = $this->m_react->merch_invoice_status_detail();
        echo json_encode($data);
        return json_encode($data);

    }
    public function get_payment_recipt_no()
    {

        $data = $this->m_react->get_payment_recipt_no();
        echo json_encode($data);
        return json_encode($data);

    }
    public function save_payment_recipt()
    {

        $data = $this->m_react->save_payment_recipt();
        echo json_encode($data);
        return json_encode($data);

    }
    public function get_payment_recipt_data()
    {

        $data = $this->m_react->get_payment_recipt_data();
        echo json_encode($data);
        return json_encode($data);

    }
    public function delete_payment_recipt()
    {

        $data = $this->m_react->delete_payment_recipt();
        echo json_encode($data);
        return json_encode($data);

    }
    public function update_payment_recipt_status()
    {

        $data = $this->m_react->update_payment_recipt_status();
        echo json_encode($data);
        return json_encode($data);

    }
    public function update_payment_recipt_data()
    {

        $data = $this->m_react->update_payment_recipt_data();
        echo json_encode($data);
        return json_encode($data);

    }
    public function custom_order_barrcode()
    {
        $data = $this->m_react->custom_order_barrcode();
        echo json_encode($data);
        return json_encode($data);
    }
    public function save_custom_order_barrcode()
    {
        $data = $this->m_react->save_custom_order_barrcode();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_lot_all_record()
    {
        $result = $this->m_react->Get_Lot_All_Record();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_best_offers_mt()
    {
        $result = $this->m_react->get_best_offers_mt();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_ebay_messages()
    {
        $result = $this->m_react->get_ebay_messages();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_message_attachment()
    {
        $result = $this->m_react->get_message_attachment();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_sent_messages()
    {
        $result = $this->m_react->get_sent_messages();
        echo json_encode($result);
        return json_encode($result);
    }
    public function delete_ebay_message()
    {
        $result = $this->m_react->delete_ebay_message();
        echo json_encode($result);
        return json_encode($result);
    }
    public function save_refund_amount()
    {
        $result = $this->m_react->save_refund_amount();
        echo json_encode($result);
        return json_encode($result);
    }
    public function update_refund_amount()
    {
        $result = $this->m_react->update_refund_amount();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_partial_refund_data()
    {
        $result = $this->m_react->get_partial_refund_data();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_best_offers_details()
    {
        $result = $this->m_react->get_best_offers_details();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_listing_log()
    {
        $result = $this->m_react->get_listing_log();
        echo json_encode($result);
        return json_encode($result);
    }
    // public function get_merchant_returns(){
    //     $result  = $this->m_react->Get_Merchant_Returns();
    //     echo json_encode($result);
    //     return json_encode($result);
    // }

    public function releaseListedbarcode()
    {
        $barcode_no = trim($this->input->post('barcode_no'));
        $user_id = trim($this->input->post('user_id'));
        if (!empty($barcode_no)) {
            $data = $this->m_tolist->releaseListedbarcode($barcode_no, $user_id);
        } else {
            $data = false;
        }
        echo json_encode($data);
        return json_encode($data);

    }
    public function save_subscriber()
    {
        $result = $this->m_react->save_subscriber();
        echo json_encode($result);
        return json_encode($result);
    }
    public function updateReturnShip()
    {
        $result = $this->m_react->updateReturnShip();
        echo json_encode($result);
        return json_encode($result);
    }
    public function update_qc_info()
    {
        $result = $this->m_react->update_qc_info();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_all_video_files(Type $var = null)
    {
        $dir = 'D:\wamp\www\item_pictures\lz_videos';
        $video_array = scandir($dir);
        $data = array();
        foreach ($video_array as $key => $value) {
            if ($value != '.' && $value != '..') {
                array_push($data, $value);
            }
        }
        echo json_encode(array('data' => $data, 'status' => true));
        return json_encode(array('data' => $data, 'status' => true));

    }

}
