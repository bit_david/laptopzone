<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * c_pl_report Controller
 */
class c_pl_report extends CI_Controller
{

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model("reactcontroller/m_pl_report");
    }
    public function get_summary_report()
    {
        $result = $this->m_pl_report->Get_Summary_Report();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_pl_report()
    {
        $result = $this->m_pl_report->Get_Pl_Report();
        echo json_encode($result);
        return json_encode($result);
    }
    public function update_ship_cost()
    {
        $result = $this->m_pl_report->Update_Ship_Cost();
        echo json_encode($result);
        return json_encode($result);
    }public function update_cost()
    {
        $result = $this->m_pl_report->Update_Cost();
        echo json_encode($result);
        return json_encode($result);
    }

}
