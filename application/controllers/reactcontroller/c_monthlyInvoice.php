<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * c_monthlyInvoice Controller
 */
class c_monthlyInvoice extends CI_Controller
{

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model("reactcontroller/m_monthlyInvoice");
    }

    public function update_ship_cost()
    {
        $result = $this->m_monthlyInvoice->Update_Ship_Cost();
        echo json_encode($result);
        return json_encode($result);
    }

    public function update_service_cost()
    {
        $result = $this->m_monthlyInvoice->Update_Service_Cost();
        echo json_encode($result);
        return json_encode($result);
    }

    public function update_packing_cost()
    {
        $result = $this->m_monthlyInvoice->Update_Packing_Cost();
        echo json_encode($result);
        return json_encode($result);
    }

    public function get_selected_packing()
    {
        $result = $this->m_monthlyInvoice->Get_Selected_Packing();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_packing_order_dropdown()
    {
        $result = $this->m_monthlyInvoice->Get_Packing_Order_DropDown();
        echo json_encode($result);
        return json_encode($result);
    }
    public function delete_packing()
    {
        $result = $this->m_monthlyInvoice->delete_packing();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_invoice_detail()
    {
        $result = $this->m_monthlyInvoice->Get_Invoice_Detail();
        echo json_encode($result);
        return json_encode($result);
    }

    public function delete_invoice_detail()
    {
        $result = $this->m_monthlyInvoice->Delete_Invoice_Detail();
        echo json_encode($result);
        return json_encode($result);
    }
    public function save_invoice_detail()
    {
        $result = $this->m_monthlyInvoice->Save_Invoice_Detail();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_merchant_inventory_prep()
    {
        $result = $this->m_monthlyInvoice->Get_Merchant_Inventory_Prep();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_merchant_services()
    {
        $result = $this->m_monthlyInvoice->Get_Merchant_Services();
        echo json_encode($result);
        return json_encode($result);
    }

    public function save_invoice_services()
    {
        $result = $this->m_monthlyInvoice->Save_Invoice_Services();
        echo json_encode($result);
        return json_encode($result);
    }
    public function delete_invoice_service()
    {
        $result = $this->m_monthlyInvoice->Delete_Invoice_Service();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_invoice_other_services_detail()
    {
        $result = $this->m_monthlyInvoice->Get_Invoice_Other_Services_Detail();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_merchant_pos_invoice()
    {
        $result = $this->m_monthlyInvoice->Get_Merchant_Pos_Invoice();
        echo json_encode($result);
        return json_encode($result);
    }
}
