<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * c_audit_barcode Controller
 */
class c_audit_barcode extends CI_Controller
{

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);
        }
        ini_set('MAX_EXECUTION_TIME', '-1');
        $this->load->model("reactcontroller/m_audit_barcode");
    }

    public function get_audit_barcode_detail()
    {
        $result = $this->m_audit_barcode->Get_Audit_Barcode_detail();
        // foreach ($result['images'] as $image) {
        //     foreach ($result['data'] as $key => $data) {
        //         $barcode = explode(',', $data['BARCODE_NO']);
        //         $bar = $barcode[0];
        //         if ($image[1] == $bar) {
        //             $result['data'][$key]['TOTAL_IMAGE'] = $image[2];
        //             // array_push($result['data']['data'][$key], $data['TOTAL_IMAGE'] = $image[$bar][2]);
        //         }
        //     }
        // }
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_audit_barcode_summary()
    {
        $result = $this->m_audit_barcode->Get_Audit_Barcode_Summary();
        echo json_encode($result);
        return json_encode($result);
    }

    public function update_lister_remarks_audit_barcode()
    {
        $result = $this->m_audit_barcode->Update_Lister_Remarks_Audit_Barcode();
        echo json_encode($result);
        return json_encode($result);
    }

    public function get_tages()
    {
        $result = $this->m_audit_barcode->Get_Tages();
        echo json_encode($result);
        return json_encode($result);
    }
    public function dicard_item_remarks()
    {
        $result = $this->m_audit_barcode->Dicard_Item_Remarks();
        echo json_encode($result);
        return json_encode($result);
    }

}
