<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * c_payouts Controller
 */
class c_payouts extends CI_Controller
{

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model("reactcontroller/m_payouts");
    }
    public function search_ebay_sale_payout_server_side()
    {

        $data = $this->m_payouts->Search_Ebay_Sale_Payout_Server_Side();
        echo json_encode($data);
        return json_encode($data);

    }
    public function get_merchant_pos_invoice_server_side()
    {

        $data = $this->m_payouts->Get_Merchant_Pos_Invoice_Server_Side();
        echo json_encode($data);
        return json_encode($data);

    }
    public function get_sums_for_payouts()
    {
        //$data['POS'] = $this->m_payouts->Get_Merchant_Pos_Invoice_Sum();
        $data['PAYOUTS_CARDS'] = $this->m_payouts->Search_Ebay_Sale_Payout_Sum();
        echo json_encode($data);
        return json_encode($data);
    }
    public function save_payouts()
    {
        $data = $this->m_payouts->Save_Payouts();
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_payouts()
    {
        $data = $this->m_payouts->Get_Payouts();
        echo json_encode($data);
        return json_encode($data);
    }
 public function delete_payout()
    {
        $data = $this->m_payouts->Delete_Payout();
        echo json_encode($data);
        return json_encode($data);
    }
 public function get_payout_detail()
    {
        $data = $this->m_payouts->Get_Payout_Detail();
        echo json_encode($data);
        return json_encode($data);
    }

}
