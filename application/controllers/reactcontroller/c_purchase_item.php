<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * c_purchase_item Controller
 */
class c_purchase_item extends CI_Controller
{

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);
        }
        // $this->load->model('reactcontroller/m_react');
        $this->load->model("reactcontroller/m_purchase_item");
    }
    public function get_purchase_items()
    {
        $result['data'] = $this->m_purchase_item->Get_Purchase_Items();
        $result['images'] = $this->m_purchase_item->Get_Purchase_Images();
        $result['dbImages'] = $this->m_purchase_item->Get_Purchase_Images_DataBase();
        echo json_encode($result);
        return json_encode($result);
    }

    public function date_filter_purch_item()
    {
        $result['data'] = $this->m_purchase_item->Date_Filter_Purch_Item();
        $result['images'] = $this->m_purchase_item->Date_Filter_Purchase_Images();
        // $result['dbImages'] = $this->m_purchase_item->Get_Purchase_Images_DataBase();
        // $this->load->view('API/SearchImagesEbayId', $result);
        foreach ($result['images'] as $image) {
            foreach ($result['data']['data'] as $key => $data) {
                $barcode = explode(',', $data['BARCODE_NO']);
                $bar = $barcode[0];
                if ($image[$bar][1] == $bar) {
                    $result['data']['data'][$key]['TOTAL_IMAGE'] = $image[$bar][2];
                    $result['data']['data'][$key]['PIC_DAYS'] = $image[$bar][3];
                    // array_push($result['data']['data'][$key], $data['TOTAL_IMAGE'] = $image[$bar][2]);
                }
            }
        }

        echo json_encode($result);
        return json_encode($result);
    }

    public function get_images()
    {
        $result = $this->m_purchase_item->Get_Images();
        echo json_encode($result);
        return json_encode($result);
    }

    public function get_purchase_images_dataBase()
    {
        $result = $this->m_purchase_item->Get_Purchase_Images_DataBase();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_db_barcode_image()
    {
        $result = $this->m_purchase_item->Get_Db_Barcode_Image();
        echo json_encode($result);
        return json_encode($result);
    }
    public function update_lister_remarks()
    {
        $result = $this->m_purchase_item->Update_Lister_Remarks();
        echo json_encode($result);
        return json_encode($result);
    }

    public function print_purchase_sticker()
    {
        $result = $this->m_purchase_item->Print_Purchase_Sticker();
        echo json_encode($result);
        return json_encode($result);
    }

    public function save_color_info()
    {
        $result = $this->m_purchase_item->Save_Color_Info();
        echo json_encode($result);
        return json_encode($result);
    }
    public function delete_purchase_barcode()
    {
        $result = $this->m_purchase_item->Delete_Purchase_Barcode();
        echo json_encode($result);
        return json_encode($result);
    }
    public function delete_purchase_all_barcode()
    {
        $result = $this->m_purchase_item->Delete_Purchase_All_Barcode();
        echo json_encode($result);
        return json_encode($result);
    }

    public function un_post_purchase_barcode()
    {
        $result = $this->m_purchase_item->Un_Post_Purchase_Barcode();
        echo json_encode($result);
        return json_encode($result);
    }

    public function edit_purchase_detail()
    {
        $result = $this->m_purchase_item->Edit_Purchase_Detail();
        echo json_encode($result);
        return json_encode($result);
    }

    public function get_all_purchase_record()
    {
        // $result = $this->m_purchase_item->Get_All_Purchase_Record();
        $result['data'] = $this->m_purchase_item->Get_All_Purchase_Record();
        // var_dump($result['data']['images']);
        foreach ($result['data']['images'] as $image) {
            foreach ($result['data']['data'] as $key => $data) {
                $barcode = explode(',', $data['BARCODE_NO']);
                $bar = $barcode[0];
                if ($image[1] == $bar) {
                    $result['data']['data'][$key]['TOTAL_IMAGE'] = $image[2];
                    $result['data']['data'][$key]['PIC_DAYS'] = $image[3];
                    // array_push($result['data']['data'][$key], $data['TOTAL_IMAGE'] = $image[$bar][2]);
                }
            }
        }
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_barcode_marketplace()
    {
        $result = $this->m_purchase_item->Get_Barcode_MarketPlace();
        echo json_encode($result);
        return json_encode($result);
    }
    public function save_Insta_marketPlace()
    {
        $reutrn = $this->m_purchase_item->save_Insta_marketPlace();
        echo json_encode($reutrn);
        return json_encode($reutrn);
    }
    public function Save_Fb_MarketPlace()
    {
        $result = $this->m_purchase_item->save_fb_marketPlace();
        echo json_encode($result);
        return json_encode($result);
    }
    public function save_marketPlace_detail()
    {

        $result = $this->m_purchase_item->Save_MarketPlace_Detail();
        echo json_encode($result);
        return json_encode($result);

    }
    public function get_market_place_detail()
    {
        $result = $this->m_purchase_item->Get_Market_Place_Detail();
        echo json_encode($result);
        return json_encode($result);
    }
    public function delete_market_place_barcode()
    {
        $result = $this->m_purchase_item->Delete_Market_Place_Barcode();
        echo json_encode($result);
        return json_encode($result);
    }
    public function update_market_place()
    {
        $result = $this->m_purchase_item->Update_Market_Place();
        echo json_encode($result);
        return json_encode($result);
    }
  
}
