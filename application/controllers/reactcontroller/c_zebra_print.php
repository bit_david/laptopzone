<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Zebra Controller
 */
class c_zebra_print extends CI_Controller
{
    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);

        }
        // $this->load->model('reactcontroller/m_react');
        $this->load->model("reactcontroller/m_genrateToken");

    }
    public function heelo()
    {
        echo json_encode('hello');
        return json_encode('hello');
    }
    public function print_zebra()
    {
        $this->load->view('zebra_printer/v_zebra_print');
    }
}
