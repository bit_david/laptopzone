<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * c_monthlyInvoice Controller
 */
class c_assignService extends CI_Controller
{

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model("reactcontroller/m_assignService");

    }

    public function get_merchant_service_dropdown()
    {
        $result = $this->m_assignService->Get_Merchant_Service_DropDown();
        echo json_encode($result);
        return json_encode($result);
    }

    public function save_merchant_services()
    {
        $result = $this->m_assignService->Save_Merchant_Services();
        echo json_encode($result);
        return json_encode($result);
    }

    public function get_merchant_service_detail()
    {
        $result = $this->m_assignService->Get_Merchant_Service_Detail();
        echo json_encode($result);
        return json_encode($result);
    }
    public function delete_merchant_service()
    {
        $result = $this->m_assignService->Delete_Merchant_Service();
        echo json_encode($result);
        return json_encode($result);
    }
}
