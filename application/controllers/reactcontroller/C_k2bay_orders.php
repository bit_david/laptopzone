<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * C_k2bay_orders Controller
 */
class C_k2bay_orders extends CI_Controller
{

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model("reactcontroller/M_k2bay_orders");

    }

    public function get_k2bay_orders_detail()
    {
        $result = $this->M_k2bay_orders->Get_K2bay_Orders_Detail();

        echo json_encode($result);
        return json_encode($result);
    }
    public function update_k2bay_order_status()
    {
        $result = $this->M_k2bay_orders->Update_K2bay_Order_Status();

        echo json_encode($result);
        return json_encode($result);
    }

    public function pull_k2bay_order()
    {
        $result = $this->M_k2bay_orders->pull_k2bay_order();

        echo json_encode($result);
        return json_encode($result);
    }
    public function unpull_k2bay_order()
    {
        $result = $this->M_k2bay_orders->unpull_k2bay_order();

        echo json_encode($result);
        return json_encode($result);
    }
    public function k2bay_awaiting_order_android()
    {
        $result = $this->M_k2bay_orders->k2bay_awaiting_order_android();

        echo json_encode($result);
        return json_encode($result);
    }
}