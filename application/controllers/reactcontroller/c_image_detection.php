<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Listing Controller
 */
class c_image_detection extends CI_Controller
{
    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);

        }
        // $this->load->model('reactcontroller/m_react');
        $this->load->model("reactcontroller/m_image_detection");

    }

    public function get_similar_data()
    {

        // $UPC = $this->input->post('UPC');
        // $TITLE = $this->input->post('TITLE');
        // $TITLE = trim(str_replace("  ", ' ', $TITLE));
        //       $TITLE = trim(str_replace(array("'"), "''", $TITLE));
        //$MPN = $this->input->post('MPN');

        // $MPN = "MD101LL/A";
        $CONDITION = "";
        $MPN = "";
        $CATEGORY = "";
        if (($this->input->post('upc'))) {
            $upc = $this->input->post('upc');
            $upc = trim(str_replace("  ", ' ', $upc));
            $upc = trim(str_replace(array("'"), "''", $upc));
            $data['key'] = $upc;
        } else if ($this->input->post('mpn')) {
            $MPN = $this->input->post('mpn');
            $MPN = trim(str_replace("  ", ' ', $MPN));
            $MPN = trim(str_replace(array("'"), "''", $MPN));
            $data['key'] = $MPN;

        } else if ($this->input->post('category_id')) {
            $CATEGORY = $this->input->post('category_id');
        } else if ($this->input->post('cond_id')) {
            $CONDITION = $this->input->post('cond_id');
        } else if ($this->input->post('title')) {
            $title = $this->input->post('title');
            $title = trim(str_replace("  ", ' ', $title));
            $title = trim(str_replace(array("'"), "''", $title));
            $data['key'] = $title;
        }

        // $MPN = trim(str_replace("  ", ' ', $MPN));
        // $MPN = trim(str_replace(array("'"), "''", $MPN));
        //$CATEGORY = $this->input->post('CATEGORY');
        // $CATEGORY = "111422";
        // $CATEGORY = trim(str_replace("  ", ' ', $CATEGORY));
        //       $CATEGORY = trim(str_replace(array("'"), "''", $CATEGORY));
        //$CONDITION = $this->input->post('CONDITION');
       
        // $CONDITION = "";
       
        // if(!empty($UPC) && strtoupper($UPC) != "DOES NOT APPLY")
        // {
        //     $data['key']=$UPC;
        // }elseif(!empty($MPN && strtoupper($MPN) != "DOES NOT APPLY")){
        //     $data['key']=$MPN;
        // }elseif(!empty($TITLE)){
        //     $data['key']=$TITLE;
        // }
        $data['condition'] = $CONDITION;
        $data['category'] = $CATEGORY;
        // $data['key'] = $MPN;
        $data['result'] = $this->load->view('API/searchImagesEbay', $data, true);
        // return $data['result'];
        echo json_encode(json_decode(htmlspecialchars_decode($data['result']), true));
        return json_encode(json_decode(htmlspecialchars_decode($data['result']), true));
        // var_dump($data['result']);
        // $result = $this->m_genrateToken->Get_Similar_Data();
        // echo json_encode($result);
        // return json_encode($result);
    }

    public function save_image_detect()
    {
        $result = $this->m_image_detection->Save_Image_Detect();
        echo json_encode($result);
        return json_encode($result);
    }

}
