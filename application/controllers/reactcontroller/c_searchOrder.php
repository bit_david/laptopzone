<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';
\EasyPost\EasyPost::setApiKey("EZTKac29498d9013471eac177657f1146b86kY1x5gKV0LLv8KwrFaQiig");

header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
	/**
	* Listing Controller
	*/
class c_searchOrder extends CI_Controller
{
		
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('reactcontroller/m_searchOrder');	    		
    }
    public function get_search_order_record()
    {
        $search_extended_order_id = $this->input->post('search_extended_order_id');

        if($search_extended_order_id == 'on'){
            $parameter                = trim($this->input->post('searchOrder'));
            $checkValid = $this->db->query("SELECT mt.LZ_SELLER_ACCT_ID,det.ORDER_ID  FROM   LZ_SALESLOAD_DET DET, LZ_SALESLOAD_MT  MT WHERE MT.LZ_SALELOAD_ID = DET.LZ_SALELOAD_ID  AND EXTENDEDORDERID = '$parameter'")->row();
            if ($checkValid) {
                $account_id  = $checkValid->LZ_SELLER_ACCT_ID;
                $legacy_order_id  = $checkValid->ORDER_ID;
                $accounts   = $this->db->query("SELECT A.SITE_TOKEN FROM LZ_SELLER_ACCTS A WHERE A.LZ_SELLER_ACCT_ID = $account_id")->result_array();
                
                if ($accounts) {
                    $authToken = $accounts[0]['SITE_TOKEN'];
                }
                $post_data = json_encode(array('legacy_order_id' => $parameter));
                
                // ebay search cancellation api 
                    $url    = 'https://api.ebay.com/post-order/v2/cancellation/search?legacy_order_id='.$parameter;
                    $header = array(
                        'Accept: application/json',
                        'Authorization: TOKEN ' . $authToken,
                        'Content-Type: application/json',
                        'X-EBAY-C-MARKETPLACE-ID: EBAY_US'
                    );
                    $ch     = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    // curl_setopt($ch, CURLOPT_POST, 0);
                    // curl_setopt($ch, CURLOPT_GETFIELDS, $post_data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    $response = curl_exec($ch);
                    // $json     = json_decode($response, TRUE);
                    if (curl_errno($ch)) {
                        echo "ERROR:" . curl_error($ch);
                    }
                    // echo "<pre>";
                    curl_close($ch);
                    $json = json_decode($response, TRUE);
                    if($json['total'] > 0){
                        // print_r($json['cancellations'][0]['cancelId']);
                        // exit();
                        $userId  = $this->input->post("userId");
                        $data = $this->m_searchOrder->getCancellationsFromEbaySingle($account_id,$parameter);
                        if($data['found'] == 1){
                            echo json_encode($data);
                            return json_encode($data);
                        }else{
                            $this->getCancellations($userId,$account_id);
                            $data = $this->m_searchOrder->getCancellationsFromEbaySingle($account_id,$parameter);
                            echo json_encode($data);
                            return json_encode($data);
                        }
                        // $data =  array(
                        //     "found" => false,
                        //     "message" => "Cancellation already exist. Cancel Id : " . $json['cancellations'][0]['cancelId']
                        // );
                        // echo json_encode($data);
                        // return json_encode($data);
                    }else{
                        
                        $data = $this->m_searchOrder->get_search_order_record();
                        echo json_encode($data);
                        return json_encode($data);
                    }
                    
            }else{
                $data = $this->m_searchOrder->get_search_order_record();
                echo json_encode($data);
                return json_encode($data);
            }
        }else{
            
             $data = $this->m_searchOrder->get_search_order_record();
             echo json_encode($data);
             return json_encode($data);
        }
    }
	public function getOrderByID(){
		$data = $this->m_searchOrder->getOrderByID();                
        echo json_encode($data);
   		return json_encode($data);
	}
	public function search_barcode_for_assign_market_place(){
		$data = $this->m_searchOrder->search_barcode_for_assign_market_place();                
        echo json_encode($data);
   		return json_encode($data);
	}
	public function assign_market_place_data(){
		$data = $this->m_searchOrder->assign_market_place_data();                
        echo json_encode($data);
   		return json_encode($data);
	}
	public function save_market_place_assign(){
		$data = $this->m_searchOrder->save_market_place_assign();                
        echo json_encode($data);
   		return json_encode($data);
	}

	public function search_by_ebay_id(){
		$data = $this->m_searchOrder->search_by_ebay_id();                
        echo json_encode($data);
   		return json_encode($data);
	}
	public function changeAddress(){
		$data = $this->m_searchOrder->changeAddress();                
        echo json_encode($data);
   		return json_encode($data);
	}
	public function getMerchantAccounts(){
		$data = $this->m_searchOrder->getMerchantAccounts();                
        echo json_encode($data);
   		return json_encode($data);
	}
	public function getCancellationsFromEbay(){
		
		$userId  = $this->input->post("userId");
		$accountId = $this->input->post("accountId");
        $this->getCancellations($userId,$accountId);
		
		$data = $this->m_searchOrder->getCancellationsFromEbay();

		echo json_encode($data);
		return json_encode($data);

        
	}
	function getCancellations($userId,$accountId){
 
    //   $data['account_name'] = "dfwonline";
	  $data['cancelCall'] = 1;
	  if(!$accountId){
		 $accountId = 1; 
	  }
      $data['merchant_id'] = $accountId;
      $data['userId'] = $userId;
      $data['cancelCall'] = 1;
	  $this->load->view('ebay/postOrder/02-searchCancellation',$data,true);
		
	}
	public function approveCancellation(){
		$account_id      = $this->input->post("account_id");
		$cancel_id      = $this->input->post("cancel_id");
		$ebay_cancel_id = $this->input->post("ebay_cancel_id");
		$cancel_status  = $this->input->post("cancel_status");
		$userId         = $this->input->post("userId");
		$bin_id         = $this->input->post("bin_id");

		if($cancel_status == 'CANCEL REQUESTED' || $cancel_status == 'APPROVE PENDING'){

		
		$accounts = $this->db->query("SELECT A.SITE_TOKEN FROM LZ_SELLER_ACCTS A WHERE A.LZ_SELLER_ACCT_ID = $account_id")->result_array();

		if($accounts){
			$authToken = $accounts[0]['SITE_TOKEN'];
		}
		// print_r($authToken);
		// exit();
 // Your ID and token
 //Sandbox TOKEN
		//  $authToken1 = 'AgAAAA**AQAAAA**aAAAAA**4AIgXw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GgDpCGpQqdj6x9nY+seQ**PM8DAA**AAMAAA**Fi+1Cl1ohpYjtarq3rBdJFcy6LK5tLWHV8FWSH9cHhl2R4AfuIhdKScDaQPg6gn0xJpX/m97JRKljBkJ1auLXDA5b2ifKJq51gIFzxVmQ0fXvYfT9QIjWHnfKCZRqMg8x264bw3NouspfZJoGivHEn2G/4689m1/PPZ6cPqt1jaZYHY4EU0s61Cr9y1cimTIrttbTh7JXi2fi5lflqSeLKWELDig63uRfPCs+K7I3dFdAph/kw/4S+C8HNbm1IxL7g0KaS6PC8dAaUG/mH0w//bsnLvlqQNYjUChQc+6tufLNGegPUQonGXMTkRi5lCHrTCcEz6m8UPo9yK+cr/ZHbMa5WNzMWrDhwLQaYtQwtCKZfF9R3p6kb4uXd/jB/Bdnh1ZRZ7B7KXEGXi2SbK37/9DikaBGQcWDRoSWfsAYGsNR5ppn0/zMbq+v1EzgjAthoRKQ0Q2Bfsmh/ATOTY/HiKX/qIVq21PkebgHFZTEhJZ8whS0XUWtXGsPcvioUmXLMaxcgxZJtUaTHzCfHvMeZ1iRhow/tWgKv0fbukugzpHXDlJaTO4UHJIvt2I6okAs+RDFUGWrRMYi95FqDpAbkytCHaFR2tHbBKnCW6l8viyoX8Vicko53XShZhw0YRF7OJ3siWtJwd1BYWwYUq0inYgQJCRhjCYqvAOqyYhGYERYbMH60BihDMf9RHxyQnbKSwB5ph6iZWuWoBLHXgwNqg6QGm0vrGLDEQL8O4ygNxNOPbC2uWzIUz6fu3Ofev5';
		//Actual TOKEN
        //   $authToken1 = 'AgAAAA**AQAAAA**aAAAAA**8AscXg**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AElIGpD5aGqQidj6x9nY+seQ**u0gDAA**AAMAAA**t2/CygLBcdHNnbWAY66y8VieRtDxnpUVrK0V0b0BQevGCixjfmm0Xa5OW6WIM7Yvf79F/ly1jEKikvKA63pPwzi7uKVWg8x9gRSWjUWU8bRccQuTwCcYN37522vjcB3IvBxs9Bvy0Qe8iIwJ94bOgASfxUsaF7BJLITKvSBZUIcii8VhVMm0Z2HXopVcR1Bi7QktwgbwmWn0S0dxXZxXTQJSg9pWkLNRkV4BHxaau629T+g2AthY6KAoXOEcnQM/N9rZYvSKUHzoRc4rW1ZyFoV2HCd4TR1twZhfNGgLdSI43rkNEqvj7LH4XOn5Z5Rk/4Buuak9ujlTAwqPhXwI8HzFymcCzlio2RKCIsQxEd2qn3TyS1mfq7mTqI6zEgANVNV/BZwHGY+ot6ba2BMAcjLzyMGK/dMmb61EeFuOZysl73NDwlv6kp9Z4/qpMm18fMeC2tBlyaSfo6U4cPVVcNnmPwGCJ49BP3d4V1Rcep+akEigXhIpEz5yYVezkHgGZccp5YncEU6gc418UHuNH3rjWOzcfLjd8N1AbU2wUrY9BzIqTfRBOeAztUezWx5UMj0pcCeIDB3iU68fa3Lri1WfdwznSt7M6mYFrUo7ho8lPcyuQg1PoXNOxTH6+aChTJmNEGUOwgCQTCHQFYWiz97Qljc/JieeulWgy0280kUEKZlBVK9o6vhAQl7l8hT+xFXIu3eXa6V4o2i1xE04jnrhxIA5sMHCxn2wFTFQpfL4RpKxaRkOLX7q5CORGyZd';

         // The data to send to the API
         $post_data = "" ;  //json_encode(array("cancelId"=>"5000021216"));
        //  $url = 'https://api.sandbox.ebay.com/post-order/v2/cancellation/5000021216/approve'; 
         $url = 'https://api.ebay.com/post-order/v2/cancellation/'.$ebay_cancel_id.'/approve'; 
         //Setup cURL
         $header = array(
                        'Accept: application/json',
                        'Authorization: TOKEN '.$authToken,
                        'Content-Type: application/json',
                        'X-EBAY-C-MARKETPLACE-ID: EBAY_US'
                         );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		$response = json_decode($response);
		
		if(isset($response->error)){
			echo json_encode(array("status" => false, "message" => $response->error[0]->message));
			return json_encode(array("status" => false, "message" => $response->error[0]->message));
        }else{
			curl_close($ch); 
			$data['responseAPI'] = $response;
			// confirm ebay api call 

			// $url = 'https://api.sandbox.ebay.com/post-order/v2/cancellation/5000021216/confirm'; 
			$url = 'https://api.ebay.com/post-order/v2/cancellation/'.$ebay_cancel_id.'/confirm'; 
			//Setup cURL
			$header = array(
							'Accept: application/json',
							'Authorization: TOKEN '.$authToken,
							'Content-Type: application/json',
							'X-EBAY-C-MARKETPLACE-ID: EBAY_US'
							);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response1 = curl_exec($ch);
			$response1 = json_decode($response1);
			if(isset($response1->error)){
				echo json_encode(array("status" => false, "message" => $response1->error[0]->message));
				return json_encode(array("status" => false, "message" => $response1->error[0]->message));
			}else{
				curl_close($ch); 
				$data['responseAPI'] = $response1;
				 $data = $this->m_searchOrder->processCancel();
				echo json_encode($data);
				return json_encode($data);

			}
			
		}

	}else{
		$data = $this->m_searchOrder->processCancel();
		echo json_encode($data);
		return json_encode($data);
	}

		
		
}
	public function CallEasyPostApi()
	{
		$fromAccount = $this->input->post('fromAccount');
		$fromAccount = trim(str_replace("  ", ' ', $fromAccount));
		$fromAccount = trim(str_replace(array("'"), "''", $fromAccount));

		$fromStreet1 = $this->input->post('fromStreet1');
		$fromStreet1 = trim(str_replace("  ", ' ', $fromStreet1));
		$fromStreet1 = trim(str_replace(array("'"), "''", $fromStreet1));

		$fromStreet2 = $this->input->post('fromStreet2');
		$fromStreet2 = trim(str_replace("  ", ' ', $fromStreet2));
		$fromStreet2 = trim(str_replace(array("'"), "''", $fromStreet2));
		
		$fromCity = $this->input->post('fromCity');
		$fromCity = trim(str_replace("  ", ' ', $fromCity));
		$fromCity = trim(str_replace(array("'"), "''", $fromCity));
		
		$fromState = $this->input->post('fromState');
		$fromState = trim(str_replace("  ", ' ', $fromState));
		$fromState = trim(str_replace(array("'"), "''", $fromState));
		
		$fromZip = $this->input->post('fromZip');
		$fromZip = trim(str_replace("  ", ' ', $fromZip));
		$fromZip = trim(str_replace(array("'"), "''", $fromZip));
		
		$fromPhone = $this->input->post('fromPhone');
		$fromPhone = trim(str_replace("  ", ' ', $fromPhone));
		$fromPhone = trim(str_replace(array("'"), "''", $fromPhone));
		
		$toName = $this->input->post('toName');
		$toName = trim(str_replace("  ", ' ', $toName));
		$toName = trim(str_replace(array("'"), "''", $toName));
		
		$toCompany = $this->input->post('toCompany');
		$toCompany = trim(str_replace("  ", ' ', $toCompany));
		$toCompany = trim(str_replace(array("'"), "''", $toCompany));
		
		$toStreet1 = $this->input->post('toStreet1');
		$toStreet1 = trim(str_replace("  ", ' ', $toStreet1));
		$toStreet1 = trim(str_replace(array("'"), "''", $toStreet1));
		
		$toCity = $this->input->post('toCity');
		$toCity = trim(str_replace("  ", ' ', $toCity));
		$toCity = trim(str_replace(array("'"), "''", $toCity));
		
		$toState = $this->input->post('toState');
		$toState = trim(str_replace("  ", ' ', $toState));
		$toState = trim(str_replace(array("'"), "''", $toState));
	
		$toZip = $this->input->post('toZip');
		$toZip = trim(str_replace("  ", ' ', $toZip));
		$toZip = trim(str_replace(array("'"), "''", $toZip));

		$parcelWeight = $this->input->post('parcelWeight');
		$parcelWeight = trim(str_replace("  ", ' ', $parcelWeight));
		$parcelWeight = trim(str_replace(array("'"), "''", $parcelWeight));
 
		$parcelLength = $this->input->post('parcelLength');
		$parcelLength = trim(str_replace("  ", ' ', $parcelLength));
		$parcelLength = trim(str_replace(array("'"), "''", $parcelLength));
 
		$parcelWidth = $this->input->post('parcelWidth');
		$parcelWidth = trim(str_replace("  ", ' ', $parcelWidth));
		$parcelWidth = trim(str_replace(array("'"), "''", $parcelWidth));
 
		$parcelHeight = $this->input->post('parcelHeight');
		$parcelHeight = trim(str_replace("  ", ' ', $parcelHeight));
		$parcelHeight = trim(str_replace(array("'"), "''", $parcelHeight));

		$sale_record_no = $this->input->post('sale_record_no');
		$lz_salesload_det_id = $this->input->post('lz_salesload_det_id');

		$from_address = \EasyPost\Address::create(array(
			'company' => $fromAccount,
			'street1' => $fromStreet1,
			'street2' => $fromStreet2,
			'city' => $fromCity,
			'state' => $fromState,
			'zip' => $fromZip,
			'phone' => $fromPhone
		));

		$to_address = \EasyPost\Address::create(array(
			'name' => $toName,
			'company' => $toCompany,
			'street1' => $toStreet1,
			'city' => $toCity,
			'state' => $toState,
			'zip' => $toZip
		));

		$parcel = \EasyPost\Parcel::create(array(
			"length" => $parcelLength,
			"width" => $parcelWidth,
			"height" => $parcelHeight,
			"weight" => $parcelWeight
		));


$shipment = \EasyPost\Shipment::create(
    array(
        "to_address"   => $to_address,
        "from_address" => $from_address,
        "parcel"       => $parcel
    )
);
		$arrayData = array();
		$i = 0;
		foreach ($shipment->rates as $rate) {
			$arrayData[$i] = array(
				"carrier" => $rate->carrier,
				"service" => $rate->service,
				"rate" => $rate->rate,
				"id" => $rate->id
			);
			$i++;
		}
				echo json_encode($arrayData);
				return json_encode($arrayData);
		
	}
	public function buyShipmentLabel(){
		$fromAccount = $this->input->post('fromAccount');
		$fromAccount = trim(str_replace("  ", ' ', $fromAccount));
		$fromAccount = trim(str_replace(array("'"), "''", $fromAccount));

		$fromStreet1 = $this->input->post('fromStreet1');
		$fromStreet1 = trim(str_replace("  ", ' ', $fromStreet1));
		$fromStreet1 = trim(str_replace(array("'"), "''", $fromStreet1));

		$fromStreet2 = $this->input->post('fromStreet2');
		$fromStreet2 = trim(str_replace("  ", ' ', $fromStreet2));
		$fromStreet2 = trim(str_replace(array("'"), "''", $fromStreet2));
		
		$fromCity = $this->input->post('fromCity');
		$fromCity = trim(str_replace("  ", ' ', $fromCity));
		$fromCity = trim(str_replace(array("'"), "''", $fromCity));
		
		$fromState = $this->input->post('fromState');
		$fromState = trim(str_replace("  ", ' ', $fromState));
		$fromState = trim(str_replace(array("'"), "''", $fromState));
		
		$fromZip = $this->input->post('fromZip');
		$fromZip = trim(str_replace("  ", ' ', $fromZip));
		$fromZip = trim(str_replace(array("'"), "''", $fromZip));
		
		$fromPhone = $this->input->post('fromPhone');
		$fromPhone = trim(str_replace("  ", ' ', $fromPhone));
		$fromPhone = trim(str_replace(array("'"), "''", $fromPhone));
		
		$toName = $this->input->post('toName');
		$toName = trim(str_replace("  ", ' ', $toName));
		$toName = trim(str_replace(array("'"), "''", $toName));
		
		$toCompany = $this->input->post('toCompany');
		$toCompany = trim(str_replace("  ", ' ', $toCompany));
		$toCompany = trim(str_replace(array("'"), "''", $toCompany));
		
		$toStreet1 = $this->input->post('toStreet1');
		$toStreet1 = trim(str_replace("  ", ' ', $toStreet1));
		$toStreet1 = trim(str_replace(array("'"), "''", $toStreet1));
		
		$toCity = $this->input->post('toCity');
		$toCity = trim(str_replace("  ", ' ', $toCity));
		$toCity = trim(str_replace(array("'"), "''", $toCity));
		
		$toState = $this->input->post('toState');
		$toState = trim(str_replace("  ", ' ', $toState));
		$toState = trim(str_replace(array("'"), "''", $toState));
	
		$toZip = $this->input->post('toZip');
		$toZip = trim(str_replace("  ", ' ', $toZip));
		$toZip = trim(str_replace(array("'"), "''", $toZip));

		$parcelWeight = $this->input->post('parcelWeight');
		$parcelWeight = trim(str_replace("  ", ' ', $parcelWeight));
		$parcelWeight = trim(str_replace(array("'"), "''", $parcelWeight));
 
		$parcelLength = $this->input->post('parcelLength');
		$parcelLength = trim(str_replace("  ", ' ', $parcelLength));
		$parcelLength = trim(str_replace(array("'"), "''", $parcelLength));
 
		$parcelWidth = $this->input->post('parcelWidth');
		$parcelWidth = trim(str_replace("  ", ' ', $parcelWidth));
		$parcelWidth = trim(str_replace(array("'"), "''", $parcelWidth));
 
		$parcelHeight = $this->input->post('parcelHeight');
		$parcelHeight = trim(str_replace("  ", ' ', $parcelHeight));
		$parcelHeight = trim(str_replace(array("'"), "''", $parcelHeight));

		$sale_record_no = $this->input->post('sale_record_no');
		$lz_salesload_det_id = $this->input->post('lz_salesload_det_id');
		$carrier = $this->input->post('carrier');
		$service = $this->input->post('service');
		$userId  = $this->input->post('userId');

		$fromAddress = \EasyPost\Address::create(array(
			'company' => $fromAccount,
			'street1' => $fromStreet1,
			'street2' => $fromStreet2,
			'city' => $fromCity,
			'state' => $fromState,
			'zip' => $fromZip,
			'phone' => $fromPhone
		));

		$toAddress = \EasyPost\Address::create(array(
			'name' => $toName,
			'company' => $toCompany,
			'street1' => $toStreet1,
			'city' => $toCity,
			'state' => $toState,
			'zip' => $toZip
		));

		$parcel = \EasyPost\Parcel::create(array(
			"length" => $parcelLength,
			"width" => $parcelWidth,
			"height" => $parcelHeight,
			"weight" => $parcelWeight
		));

		$shipment = \EasyPost\Shipment::create(array(
			"to_address" => $toAddress,
			"from_address" => $fromAddress,
			"parcel" => $parcel
		));
		$rateId = "";
		foreach ($shipment->rates as $rate) {
			if( $rate->carrier == $carrier && $rate->service == $service){
				$rateId = $rate->id;
			}
		}

		$shipment->buy(array('rate' => array('id' => $rateId)));
		
		$shippingData = array();
		$j = 0;

		$shippingData[$j] = array(
			"label_url" => $shipment->postage_label->label_url,
			"Tracking_No" => $shipment->tracking_code
		);
		$date = date("m/d/Y");
            date_default_timezone_set("America/Chicago");
            $date = date('Y-m-d H:i:s');
			$insert_date = "TO_DATE('".$date."', 'YYYY-MM-DD HH24:MI:SS')";

			$tacking_no = $shipment->tracking_code;
			$label_link = $shipment->postage_label->label_url;

		if($tacking_no){
			$update = $this->db->query("UPDATE LZ_SALESLOAD_DET SET TRACKING_NUMBER = '$tacking_no' WHERE SALES_RECORD_NUMBER = '$sale_record_no' AND LZ_SALESLOAD_DET_ID = $lz_salesload_det_id ");

			$tracking_data = $this->db->query("SELECT TM.TRACKING_ID FROM LZ_TRACKING_MT TM WHERE TM.LZ_SALESLOAD_DET_ID = $lz_salesload_det_id AND TM.ORDER_ID = $sale_record_no")->result_array();
			if($tracking_data){

				$tracking_id = $tracking_data[0]['TRACKING_ID'];

				$this->db->query("INSERT INTO LZ_TRACKING_DET TD (TD.TRACKING_DET_ID,
																TD.TRACKING_ID,
																TD.TRACKING_NO,
																TD.LABEL_LINK) 
																VALUES(GET_SINGLE_PRIMARY_KEY('LZ_TRACKING_DET','TRACKING_DET_ID'),
																$tracking_id,
																'$tacking_no',
																'$label_link')");

			}else{
				$tracking_data = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LZ_TRACKING_MT','TRACKING_ID') TRACKING_ID FROM DUAL ")->result_array();
				$tracking_id = $tracking_data[0]['TRACKING_ID'];
	
				$this->db->query("INSERT INTO LZ_TRACKING_MT TM (TM.TRACKING_ID,
																TM.LZ_SALESLOAD_DET_ID,
																TM.ORDER_ID,
																TM.CREATED_DATE,
																TM.CREATED_BY) 
																VALUES($tracking_id,
																$lz_salesload_det_id,
																$sale_record_no,
																$insert_date,
																$userId)");
				$this->db->query("INSERT INTO LZ_TRACKING_DET TD (TD.TRACKING_DET_ID,
																TD.TRACKING_ID,
																TD.TRACKING_NO,
																TD.LABEL_LINK) 
																VALUES(GET_SINGLE_PRIMARY_KEY('LZ_TRACKING_DET','TRACKING_DET_ID'),
																$tracking_id,
																'$tacking_no',
																'$label_link')");
			}
		}
		echo json_encode($shippingData);
		return json_encode($shippingData);

	}
	public function send_email()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('message', 'Message', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		if ($this->form_validation->run() == FALSE){
					echo json_encode(array('status' => false,'message' => validation_errors()));
					return json_encode(array('status' => false,'message' => validation_errors()));
		}else
		{
			$name =  html_escape($this->input->post('name'));
			$email =  html_escape($this->input->post('email'));
			$message =  html_escape($this->input->post('message'));
			if(isset($_FILES['file'])){	
				$specific_path = "D:/wamp/www/item_pictures/application_resume/";
                $main_dir = $specific_path;
                if (is_dir($main_dir) === false) {
                    mkdir($main_dir);
                }
                $config['upload_path'] = $main_dir;
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '0'; // max_size in kb
                $config['remove_spaces'] = true;
                $config['file_name'] = $_FILES['file']['name'];

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('file')) {
					$error = array('error' => $this->upload->display_errors());
					echo json_encode(array('status' => false,'message' => $error));
					return json_encode(array('status' => false,'message' => $error));
				} else{
					$image_data = $this->upload->data();
					$full_path = $image_data['full_path'];
					
					$this->load->library('email');
					$config['mailtype'] = 'html';
					$config['charset'] = 'iso-8859-1';
					$config['wordwrap'] = true;
					$this->email->initialize($config);
					$this->email->from('info@laptopzone.us');
					$this->email->to('hr@ecologix.us');
					$this->email->subject('Job Application');
					$this->email->message('<strong>Name  : <strong> <span> ' . $name . ' </span><br>
					<strong>Email  : <strong> <span> ' . $email . ' </span><br>
					<p>'. $message .'</p>');
					$this->email->attach($full_path);
					if ($this->email->send()) {
						echo json_encode(array('status' => true));
						return json_encode(array('status'=> true));

					} else {
						echo json_encode(array('status' => false));
						return json_encode(array('status'=> false));
					}
				}
			}else{
				echo json_encode(array('status' => false,'message' => "Please Select Your Recume"));
				return json_encode(array('status' => false,'message' => "Please Select Your Recume"));
			}
		}
	}

	// adilController/c_searchOrder functions start 
	
    public function get_search_order_record_refund()
    {
        $data = $this->m_searchOrder->get_search_order_record_refund();
        
        echo json_encode($data);
        return json_encode($data);
    }
    public function get_search_order_record_account_id()
    {
        $data = $this->m_searchOrder->get_search_order_record_account_id();
        
        echo json_encode($data);
        return json_encode($data);
    }
    public function printBarcode(){
    
      $result = $this->m_searchOrder->printBarcode();

      $this->load->library('m_pdf');
            // to increse or decrese the width of barcode please set size attribute in barcode tag
        $i = 0;
        foreach($result as $data){
          $text = $data["BUISNESS_NAME"];
          $item_desc =implode("<br/>", str_split($text, 40));
          $lot_desc =implode("<br/>", str_split($data['LOT_DESC'], 40));
          $html ='<div style = "margin-left:-35px!important;">
                <div style="width:222px !important;" class="barcodecell"><barcode height="0.75" size="1.18" code="'.@$data["BARCODE_NO"].'" type="C128A" class="barcode" /></div>
            
            <div style="margin-top:6px !important;width:222px;padding:0;font-size:10px;font-family:arial;">
            <span><b>'.
              @$data["BARCODE_NO"].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>C
              </u>&nbsp;&nbsp;<br></b>Release Date: '.
              @$data['RELEASE_DATE'].'</span><br>
              <br></span></div>
                </div>';
          //generate the PDF from the given html
          $this->m_pdf->pdf->WriteHTML($html);
          $i++;
          if(!empty($result[$i])){
            $this->m_pdf->pdf->AddPage();
          }
          
        }//end foreach
        
      //download it.
      $this->m_pdf->pdf->Output($pdfFilePath, "I");
    }
    public function check_eligibility()
    {
        $order_id   = trim($this->input->post('order_id'));
        $account_id = $this->input->post("account_id");
        $accounts   = $this->db->query("SELECT A.SITE_TOKEN FROM LZ_SELLER_ACCTS A WHERE A.LZ_SELLER_ACCT_ID = $account_id")->result_array();
        
        if ($accounts) {
            $authToken = $accounts[0]['SITE_TOKEN'];
        }
        $post_data = json_encode(array(
            'legacyOrderId' => $order_id
            
        ));
        
        $url    = 'https://api.ebay.com/post-order/v2/cancellation/check_eligibility';
        $header = array(
            'Accept: application/json',
            'Authorization: TOKEN ' . $authToken,
            'Content-Type: application/json',
            'X-EBAY-C-MARKETPLACE-ID: EBAY_US'
        );
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        $json     = json_decode($response, TRUE);
        if (curl_errno($ch)) {
            echo "ERROR:" . curl_error($ch);
        }
        curl_close($ch);
        $json     = json_decode($response, TRUE);
        $eligible = $json['eligible'];
        if ($eligible == TRUE) {
            $legecyId = $json['legacyOrderId'];
            foreach ($json['eligibleCancelReason'] as $key => $value) {
                $json['eligibleCancelReason'][$key] = str_replace("_", " ", $value);
            }
            echo json_encode(array(
                'found' => true,
                'legecyId' => $legecyId,
                'reason' => $json['eligibleCancelReason']
            ));
            return json_encode(array(
                'found' => true,
                'legecyId' => $legecyId,
                'reason' => $json['eligibleCancelReason']
            ));
        } else {
          $group_record_number = $this->db->query("SELECT SALES_RECORD_NO_GROUP FROM LZ_SALESLOAD_DET WHERE ORDER_ID ='$order_id'")->row();
          $group_record_number =$group_record_number->SALES_RECORD_NO_GROUP;
          
          $bar_codes =  array();
          if($group_record_number){
              $order_ids = $this->m_searchOrder->get_order_ids_by_group_record($group_record_number);
              
              foreach ($order_ids as $key => $value) {
                  // echo $value['ORDER_ID'];
                  $bar_codes1 = $this->m_searchOrder->get_bar_codes($value['ORDER_ID']);
                  foreach ($bar_codes1 as $key => $value1) {
                      $bar_codes1[$key]['ORDER_ID'] = $value['ORDER_ID'];
                    }
                    array_push($bar_codes ,$bar_codes1);
                }
            }else{
            $bar_codes1 = $this->m_searchOrder->get_bar_codes($order_id);
            foreach ($bar_codes1 as $key => $value1) {
              $bar_codes1[$key]['ORDER_ID'] = $order_id;
            }
            array_push($bar_codes ,$bar_codes1);
            
          }

          // $bar_codes = $this->m_searchOrder->get_bar_codes($order_id);
          
          // echo "<pre>";
          // print_r($bar_codes);
          echo json_encode(array(
                'found' => false,
                'legecyId' => $order_id,
                'barcodes' => $bar_codes,
                'message' => 'Sorry item is not eligible due to : ' 
            ));
            return json_encode(array(
                'found' => false,
                'legecyId' => $order_id,
                'barcodes' => $bar_codes,
                'message' => 'Sorry item is not eligible due to : '
            ));
          
        }
        
    }
    function cancellation_request()
    {
        $legecyId       = trim($this->input->post('legecyId'));
        $remarks        = $this->input->post('remarks');
        $remarks        = str_replace(" ", "_", $remarks);
        $account_id     = $this->input->post("account_id");
        $userId         = $this->input->post("userId");
        $barcode_status = $this->input->post("barcode_status");
        $releaseRemarks = $this->input->post("releaseRemarks");
        $requestType    = $this->input->post("requestType");
        $bin_id    = $this->input->post("bin_id");
        $barcode_no    = $this->input->post("barcode_no");
        
        $accounts       = $this->db->query("SELECT A.SITE_TOKEN FROM LZ_SELLER_ACCTS A WHERE A.LZ_SELLER_ACCT_ID = $account_id")->result_array();
        
        if ($accounts) {
            $authToken = $accounts[0]['SITE_TOKEN'];
        }
        $header = array(
            'Accept: application/json',
            'Authorization: TOKEN ' . $authToken,
            'Content-Type: application/json',
            'X-EBAY-C-MARKETPLACE-ID: EBAY_US'
        );
        if ($requestType == 'CANCEL') {
            $post_data = json_encode(array(
                'legacyOrderId' => $legecyId,
                'cancelReason' => $remarks,
                'buyerPaid' => false
            ));
            $url       = 'https://api.ebay.com/post-order/v2/cancellation/';
            $ch        = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $response = curl_exec($ch);
            $json     = json_decode($response, TRUE);
            if (curl_errno($ch)) {
                echo "ERROR:" . curl_error($ch);
            }
            curl_close($ch);
            $cancelId = $json['cancelId'];
            if ($cancelId) {
                $url = 'https://api.ebay.com/post-order/v2/cancellation/' . $cancelId;
                $ch  = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_POST, 0);
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                $response = curl_exec($ch);
                $json     = json_decode($response, TRUE);
                if (curl_errno($ch)) {
                    echo "ERROR:" . curl_error($ch);
                }
                curl_close($ch);
                date_default_timezone_set("America/Chicago");
                $date                = date('Y-m-d H:i:s');
                $insert_date         = "TO_DATE('" . $date . "', 'YYYY-MM-DD HH24:MI:SS')";
                $ebay_cancel_id      = $json['cancelDetail']['cancelId'];
                $marketplace_id      = $json['cancelDetail']['marketplaceId'];
                $legacy_order_id     = $json['cancelDetail']['legacyOrderId'];
                $requestor_type      = $json['cancelDetail']['requestorType'];
                $cancel_reason       = $json['cancelDetail']['cancelReason'];
                $cancel_state        = $json['cancelDetail']['cancelState'];
                $cancel_status       = $json['cancelDetail']['cancelStatus'];
                $cancel_close_reason = $json['cancelDetail']['cancelReason'];
                $payment_status      = $json['cancelDetail']['paymentStatus'];
                
                $alreadyExit = $this->db->query("SELECT C.EBAY_CANCEL_ID FROM LJ_CANCELLATION_MT C WHERE C.EBAY_CANCEL_ID = $ebay_cancel_id")->result_array();
                if($ebay_cancel_id){
                if (!$alreadyExit) {
                    $cancel_id = $this->db->query("SELECT GET_SINGLE_PRIMARY_KEY('LJ_CANCELLATION_MT','CANCELLATION_ID') CANCELLATION_ID FROM DUAL")->result_array();
                    $cancel_id = $cancel_id[0]['CANCELLATION_ID'];
                    
                    $this->db->query("INSERT INTO LJ_CANCELLATION_MT C (C.CANCELLATION_ID,C.EBAY_CANCEL_ID,C.MARKETPLACE_ID,C.REQUESTOR_TYPE,C.CANCEL_STATE,C.CANCEL_STATUS,C.CANCEL_CLOSE_REASON,C.PAYMENT_STATUS,C.CANCEL_REASON,C.LEGACY_ORDER_ID,C.CREATED_AT,C.CREATED_BY,C.ACCOUNT_ID,C.STATUS)
                    values ($cancel_id,$ebay_cancel_id,'$marketplace_id','$requestor_type','$cancel_state','$cancel_status','$cancel_close_reason','$payment_status','$cancel_reason','$legacy_order_id',$insert_date,$userId,$account_id,'1')");
                    $barcodes = $this->db->query("SELECT B.BARCODE_NO FROM LZ_BARCODE_MT B WHERE B.ORDER_ID = (SELECT DT.ORDER_ID FROM LZ_SALESLOAD_DET DT WHERE (DT.EXTENDEDORDERID =  '$legacy_order_id' OR DT.ORDER_ID = '$legacy_order_id') AND ROWNUM = 1) ")->result_array();
                    $resultSale = $this->db->query("SELECT d.sales_record_number, d.extendedorderid, d.order_id
                        from lz_salesload_det d
                        where (d.order_id = '$legacy_order_id' or d.extendedorderid = '$legacy_order_id')")->row();

                    if ($barcodes) {
                        foreach ($barcodes as $bar) {
                            $barcode = $bar['BARCODE_NO'];
                            $this->db->query("INSERT INTO LJ_CANCELLATION_DT CD (CD.CANCELLATION_DT_ID,CD.CANCELLATION_ID,CD.BARCODE_NO,CD.SALE_ORDER_ID,CD.EXTENDEDORDERID,CD.ORDER_ID)
                                                             values (
                                                                 GET_SINGLE_PRIMARY_KEY('LJ_CANCELLATION_DT','CANCELLATION_DT_ID'),
                                                                 $cancel_id,
                                                                 $barcode,
                                                                 '$resultSale->SALES_RECORD_NUMBER',
                                                                 '$resultSale->EXTENDEDORDERID',
                                                                 '$resultSale->ORDER_ID') ");
                            
                        }
                    }
                } else {
                        $cancellation_id = $alreadyExit[0]['CANCELLATION_ID'];
                        $this->db->query("UPDATE LJ_CANCELLATION_MT C 
                        SET
                        C.EBAY_CANCEL_ID = $ebay_cancel_id,
                        C.MARKETPLACE_ID = '$marketplace_id',
                        C.REQUESTOR_TYPE = '$requestor_type',
                        C.CANCEL_STATE   = '$cancel_state',
                        C.CANCEL_STATUS  = '$cancel_status',
                        C.CANCEL_CLOSE_REASON = '$cancel_close_reason',
                        C.PAYMENT_STATUS = '$payment_status',
                        C.CANCEL_REASON = '$cancel_reason',
                        C.LEGACY_ORDER_ID = '$legacy_order_id',
                        C.ACCOUNT_ID = $merchant_id,
                        C.STATUS = '0'
                        WHERE C.CANCELLATION_ID = $cancellation_id");
                    }
                        echo json_encode(array(
                            'found' => true,
                            'message' => 'Item is cancelled successfully ! Now you can release or discard...!'
                        ));
                        return json_encode(array(
                            'found' => true,
                            'message' => 'Item is cancelled successfully  Now you can release or discard...!'
                        ));
                    }else{
                        echo json_encode(array(
                            'found' => false,
                            'message' => 'Invalid Request'
                        ));
                        return json_encode(array(
                            'found' => false,
                            'message' => 'Invalid Request'
                        ));
                    }       
            } else {
                echo json_encode(array(
                    'found' => false,
                    'message' => 'Invalid Request'
                ));
                return json_encode(array(
                    'found' => false,
                    'message' => 'Invalid Request'
                ));
            }
        }else{
              $data = $this->m_searchOrder->processCreateCancelation($legecyId,$releaseRemarks,$userId,$bin_id,$barcode_no);
              if($data){
                if($data == 'release'){
                echo json_encode(array(
                  'found' => true,
                  'message' => 'Item successfully release!'
              ));
              return json_encode(array(
                  'found' => true,
                  'message' => 'Item successfully release!'
              ));
              }else{
                echo json_encode(array(
                  'found' => true,
                  'message' => 'Item successfully discard!'
              ));
              return json_encode(array(
                  'found' => true,
                  'message' => 'Item successfully discard!'
              ));
              }
          }
        }
    }
    public function get_cancelled_barcode()
    {
        $bar_codes = $this->m_searchOrder->get_cancelled_barcode();
        echo json_encode(array(
            'found' => true,
            'barcodes' => $bar_codes
            
        ));
        return json_encode(array(
            'found' => true,
            'barcodes' => $bar_codes
            
        ));
  
    }
    public function get_cancelled_barcode_records()
    {
        $records = $this->m_searchOrder->get_cancelled_barcode_records();
        echo json_encode(array(
            'found' => true,
            'records' => $records
            
        ));
        return json_encode(array(
            'found' => true,
            'records' => $records
            
        ));
  
	}
	// end of adilController/c_searchOrder 
	
	
}	