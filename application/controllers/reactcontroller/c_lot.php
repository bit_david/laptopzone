<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * c_lot Controller
 */
class c_lot extends CI_Controller
{

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        parent::__construct();
        $this->load->database();
        // $this->Data = json_decode(file_get_contents('php://input'), true);
        if (!empty(json_decode(file_get_contents('php://input'), true))) {
            $this->Data = json_decode(file_get_contents('php://input'), true);
        }
        $this->load->model("reactcontroller/m_lot");
    }

    public function get_lots_detail()
    {
        $result = $this->m_lot->Get_Lots_Detail();
        echo json_encode($result);
        return json_encode($result);
    }
    public function get_specific_lot_detail()
    {
        $result = $this->m_lot->Get_Specific_Lot_Detail();
        echo json_encode($result);
        return json_encode($result);
    }

    public function get_print_lot_barcode_detail()
    {
        $result = $this->m_lot->Get_Print_Lot_Barcode_Detail();
        echo json_encode($result);
        return json_encode($result);
    }
}
